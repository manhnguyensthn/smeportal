var express = require('express');
var config = require('config');
var bodyParser = require('body-parser');
var socketIo = require('socket.io');

var app = express();
//body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//path to controller folder
var controller = require(__dirname + '/apps/Controllers');
app.use(controller);

//config server
var host = config.get('server.host');
var port = config.get('server.port');

var server = app.listen(port, host, function(){
    console.log('Server is running on port ', port);
});

var io = socketIo(server);