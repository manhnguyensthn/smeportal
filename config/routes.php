<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\InflectedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
//Router::defaultRouteClass(DashedRoute::class);
Router::defaultRouteClass('InflectedRoute');
Router::scope('/', function ($routes) {
    // Connect the default home and /pages/* routes.

    $routes->connect('/:language/:controller/:action/*', ['language' => 'en|es|ja']);
    $routes->connect('/', [
        'controller' => 'Projects',
        'action' => 'index'
    ]);
    $routes->connect('/:language', [
        'controller' => 'Projects',
        'action' => 'index'
    ]);
    Router::connect(
            '/projects/edit/:id', array('controller' => 'Projects', 'action' => 'create'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );

    Router::connect(
            '/requests/respond/:id', array('controller' => 'Requests', 'action' => 'respond'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    $routes->connect('/projects/exportProjets',array('controller'=>'Projects','action'=>'exportProjets'));
    $routes->connect('/requests/create', array('controller'=> 'Requests', 'action'=>'create'));
    $routes->connect('/groups', array('controller'=> 'Groups', 'action'=>'index'));
    $routes->connect('/requests/index', array('controller'=> 'Requests', 'action'=>'index'));
    $routes->connect('/request/list',array('controller'=>'Requests','action'=>'list_send'));
    $routes->connect('/projects/list',array('controller'=>'Projects','action'=>'getProjectByFee'));
    Router::connect('/request/view/:id',array('controller'=>'Requests','action'=>'view'),array(
'pass' => array('id'),
'id' => '[0-9]+'
    ));
    Router::connect(
            '/projects/:slug-:id', ['controller' => 'Projects', 'action' => 'detail'], ['slug' => '.*', 'id' => '\d+']
    );
    // Router::connect(
    //         '/projects/list', ['controller' => 'Projects', 'action' => 'getProjectByFee']
    // );
    // router edit project
    Router::connect(
            '/projects/edit/:id', array('controller' => 'Projects', 'action' => 'create'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );

    Router::connect(
            '/requests/respond/:id', array('controller' => 'Requests', 'action' => 'respond'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    // router role project
    Router::connect(
            '/projects/roles/:id', array('controller' => 'Projects', 'action' => 'roles'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    // router del role in project
    Router::connect(
            '/projects/deleterole/:id', array('controller' => 'Projects', 'action' => 'deleterole'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    // router milestone
    Router::connect(
            '/projects/milestones/:id', array('controller' => 'Projects', 'action' => 'milestones'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    // router milestone delete
    Router::connect(
            '/projects/delmilestone/:id', array('controller' => 'Projects', 'action' => 'delmilestone'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    //about you
    Router::connect(
            '/projects/about_you/:id', array('controller' => 'Projects', 'action' => 'about_you'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    //share embeded
    Router::connect(
            '/projectactions/share/:id', array('controller' => 'ProjectActions', 'action' => 'share'), array(
        'pass' => array('id'),
        'id' => '[0-9]+'
            )
    );
    //chat collaborator detail
    Router::connect(
            '/usersprojects/userChatDetail/:projectid/:userid', array('controller' => 'UsersProjects', 'action' => 'userChatDetail'), array(
        'pass' => array('projectid', 'userid'),
        'projectid' => '[0-9]+',
        'userid' => '[0-9]+'
    ));

    //chat room detail
    Router::connect(
            '/chat-room/:projectid/:roomid', array('controller' => 'UsersProjects', 'action' => 'chatRoomDetail'), array(
        'pass' => array('projectid', 'roomid'),
        'projectid' => '[0-9]+',
        'roomid' => '[0-9]+'
    ));

    //Project Metting Update
    Router::connect('/projects/update/:projectid/*', array(
        'controller' => 'Mettings',
        'action' => 'update'
            ), array(
        'pass' => array('projectid'),
        'projectid' => '[0-9]+',
    ));

    //Project Metting Asset
    Router::connect('/projects/asset/:projectid/*', array(
        'controller' => 'Mettings',
        'action' => 'asset'
            ), array(
        'pass' => array('projectid'),
        'projectid' => '[0-9]+',
    ));
    //Project Metting schedule
    Router::connect('/projects/schedule/:projectid/*', array(
        'controller' => 'Mettings',
        'action' => 'schedule'
    ), array(
        'pass' => array('projectid'),
        'projectid' => '[0-9]+',
    ));

    Router::connect('/pages/*', [
        'controller' => 'Pages',
        'action' => 'display'
    ]);

    Router::connect('/about-us', array(
        'controller' => 'Pages',
        'action' => 'about',
    ));

    Router::connect('/what-is-we-the-project', array(
        'controller' => 'Pages',
        'action' => 'WhatIsWeTheProject',
    ));

    Router::connect('/faq', array(
        'controller' => 'Pages',
        'action' => 'faq',
    ));

    Router::connect('/guide', array(
        'controller' => 'Pages',
        'action' => 'guide',
    ));

    Router::connect('/privacy-policy', array(
        'controller' => 'Pages',
        'action' => 'privacypolicy',
    ));

    Router::connect('/support-and-contact', array(
        'controller' => 'Pages',
        'action' => 'Support',
    ));

    Router::connect('/terms-of-user', array(
        'controller' => 'Pages',
        'action' => 'TermsOfUser',
    ));

    // MyFolder
    $routes->connect('/my-folder', array(
        'controller' => 'Myfolder',
        'action' => 'getMyfolder',
    ));
    // Applicants
    Router::connect(
            '/applicant-:project_id', array('controller' => 'Applicant', 'action' => 'getApplication'), array(
        'pass' => array('project_id'),
        'project_id' => '[0-9]+'
            )
    );
    // Applicants detail
    Router::connect(
            '/applicant-detail-:project_id-:role_id', array('controller' => 'Applicant', 'action' => 'ApplicantDetail'), array(
        'pass' => array('project_id', 'role_id'),
        'project_id' => '[0-9]+',
        'role_id' => '[0-9]+'
            )
    );
    // Applicants profile
    Router::connect( '/applicant-profile-:user_id',array(
        'controller' => 'Applicant',
        'action' => 'getProfileApplicant'
    ), array(
        'pass'       => array('user', 'user_id'),
        'user_id'    => '[0-9]+',
    ));
    Router::connect('/applicant-profile-:user_id-:project_id-:role_id', array(
        'controller' => 'Applicant',
        'action' => 'getProfileApplicant'
    ), array(
        'pass'       => array('user', 'user_id', 'project_id', 'role_id'),
        'user_id'    => '[0-9]+',
        'project_id' => '[0-9]+',
        'role_id'    => '[0-9]+'
    ));

    Router::connect( '/applicant-findmore-:project_id',array(
        'controller' => 'Applicant',
        'action' => 'get_findmore'
    )
    , array(
        'pass'       => array('project_id'),
        'project_id'    => '[0-9]+',
    ));
    // Findmore profile
    Router::connect( '/findmore-profile-:project_id-:user_id',array(
        'controller' => 'Applicant',
        'action' => 'getFindMoreProfile'
    ), array(
        'pass'       => array('project_id','user_id'),
        'project_id'    => '[0-9]+',
        'user_id'    => '[0-9]+'
    ));
    // Applicants project
    Router::connect('/applicant-project-:user_id', array(
        'controller' => 'Applicant',
        'action' => 'getProjectApplicant'
    ), array(
        'pass' => array('user', 'user_id'),
        'user_id' => '[0-9]+'
    ));
    Router::connect('/applicant-project-:user_id-:project_id-:role_id', array(
        'controller' => 'Applicant',
        'action' => 'getProjectApplicant'
    ), array(
        'pass'       => array('user', 'user_id', 'project_id', 'role_id'),
        'user_id'    => '[0-9]+',
        'project_id' => '[0-9]+',
        'role_id'    => '[0-9]+'
    ));
    // Applicants folowing
    Router::connect('/applicant-following-:user_id',array(
        'controller' => 'Applicant',
        'action'     => 'getFollowingApplicant'
    ), array(
        'pass'    => array('user', 'user_id'),
        'user_id' => '[0-9]+'
    ));

    Router::connect('/applicant-following-:user_id-:project_id-:role_id', array(
        'controller' => 'Applicant',
        'action'     => 'getFollowingApplicant'
    ), array(
        'pass'       => array('user', 'user_id', 'project_id', 'role_id'),
        'user_id'    => '[0-9]+',
        'project_id' => '[0-9]+',
        'role_id'    => '[0-9]+'
    ));
    // Applicants folower
    Router::connect('/applicant-follower-:user_id', array(
        'controller' => 'Applicant',
        'action'     => 'getFollowerApplicant'
    ), array(
        'pass'    => array('user', 'user_id'),
        'user_id' => '[0-9]+',
    ));
    Router::connect('/applicant-follower-:user_id-:project_id-:role_id', array(
        'controller' => 'Applicant',
        'action'     => 'getFollowerApplicant'
    ), array(
        'pass'       => array('user', 'user_id', 'project_id', 'role_id'),
        'user_id'    => '[0-9]+',
        'project_id' => '[0-9]+',
        'role_id'    => '[0-9]+'
    ));

    // Connect the conventions based default routes.
    $routes->fallbacks('InflectedRoute');
});
Router::scope('/', function ($routes) {
    // Connect the default home and /pages/* routes.
    $routes->connect('/:language/:controller/:action/*', ['language' => 'en|vi']);

    //forgos
    $routes->connect(
            '/users/forgot_pass', ['controller' => 'Users', 'action' => 'forgotPass'], ['_name' => 'forgot_pass']
    );
    $routes->connect(
            '/users/update_pass', ['controller' => 'Users', 'action' => 'updatePass'], ['_name' => 'update_pass']
    );
    $routes->connect(
            '/users/expried_pass', ['controller' => 'Users', 'action' => 'expriedPass'], ['_name' => 'expried_pass']
    );

    // Connect the conventions based default routes.
    $routes->fallbacks('InflectedRoute');
});
//router admin prefix
Router::prefix('admin', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->connect('/', ['controller' => 'Dashboard', 'action' => 'index']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/add', ['controller' => 'Users', 'action' => 'add']);

    $routes->fallbacks('DashedRoute');
});
//router admin prefix
Router::prefix('api', function ($routes) {
    $routes->extensions(['json', 'XML']);
    // All routes here will be prefixed with `/api`
    // And have the prefix => admin route element added.
    // $routes->connect('/:language/:controller/:action/*', ['language' => 'en|es|ja']);
    $routes->connect(
            '/notifications/setting', ['controller' => 'NotificationSetting', 'action' => 'setting']
    );
    $routes->connect(
            '/notifications/update', ['controller' => 'NotificationSetting', 'action' => 'update']
    );
    $routes->fallbacks('DashedRoute');
});
/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
