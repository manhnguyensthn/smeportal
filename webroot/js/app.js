function getLang(key){
    if(oTranslations[key] != null){
        return oTranslations[key];
    }
    return false;
}


// check validate url
function isUrlValid(url) {
    return /^(https?):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}
// get youtube id
function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
}

function isYoutubeURL($url) {
    var regex = /https:\/\/(www\.|m\.){0,1}youtu(\.be|be\.com)\/(watch\?v=|embed\/){0,1}([a-zA-Z0-9_-]{11})/gmi;
    var result = false;
    var matches = regex.exec($.trim($url));
    if (matches != null && matches.length > 4) {
        result = true;
    }
    return result;
}

function countCharTitle(val) {
    var len = val.value.length;
    if (len > 60) {
      val.value = val.value.substring(1, 60);
    } else {
      $('.charTitle').text(60 - len);
    }
};
function countCharDescription(val) {
    var lend = val.value.length;
    //console.log(lend);
    if (lend > 500) {
      val.value = val.value.substring(1, 500);
    } else {
      $('.charDes').text(500 - lend);
    }
};
function ValidateDate(dtValue){
    var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
    return dtRegex.test(dtValue);
}

function comPareDate(first,second ){
    if((new Date(first).getTime() > new Date(second).getTime())){
       return 0; 
    }
    return 1
}

function add_action_to_system(type, project_id){
    $.ajax({
            type: 'POST',
            url: '/projectActions/addActionToSystem',
            data: {
                'type': type,
                'project_id': project_id
            },
            success: function (respone) {
                result = JSON.parse(respone);
                if (result['result'] == '1'){
                    if (type == '1'){
                        if (result['status'] == '1'){
                            $('.love_project_'+project_id).removeClass('dislike');
                            $('.list-follows .btn-like img').attr('src','/img/heart-liked.png');
                            send_notification_for_collaborators(project_id,'7');
                        }else{
                            $('.love_project_'+project_id).addClass('dislike');
                            $('.list-follows .btn-like img').attr('src','/img/heart-dislike.png');
                        }
                    }else{
                        if (result['status'] == '1'){
                            $('.list-follows .btn-default').addClass('active');
                            $('.comment-all-form').show();
                            send_notification_for_collaborators(project_id,'8');
                        }
                        else{
                            $('.list-follows .btn-default').removeClass('active');
                            $('.comment-all-form').hide();
                        }
                    }
                }
                else{
                    alertBox(result['message']);
                }
            }
        });
}

function process_like_to_system(project_id){
    $.ajax({
            type: 'POST',
            url: '/projectActions/addActionToSystem',
            data: {
                'type': '1',
                'project_id': project_id
            },
            success: function (respone) {
                result = JSON.parse(respone);
                if (result['result'] == '1'){
                    if (result['status'] == '1'){
                        $('#like-box-'+project_id+' img').attr('src','/img/heart-liked.png');
                    }else{
                        $('#like-box-'+project_id+' img').attr('src','/img/heart-dislike.png');
                    }
                }
                else{
                    alertBox(respone);
                }
            }
        });
}

function send_notification_for_collaborators(project_id,action_type,comment_id = ''){
    $.ajax({
            type: 'POST',
            url: '/ProjectActions/sendNotificationForCollaborator',
            data: {
                'project_id': project_id,
                'action_type': action_type,
                'comment_id' : comment_id
            },
            success: function (respone) {
                var result = JSON.parse(respone);
                if (result['status'] == '1'){
                    return true;
                }
                else{
                    return false;
                }
            }
        });
}

function get_project_info_by_tab(option,content_id,project_id){
    $.ajax({
            type: 'POST',
            url: '/projects/getProjectInfoByTabAjax',
            data: {
                'option': option,
                'project_id': project_id
            },
            beforeSend: function(){
                $('#' + content_id + ' .tab-info-content').html('<div class="row text-center"><img src="/img/loading.gif" /></div>');
            },
            success: function (respone) {
                $('#' + content_id + ' .tab-info-content').html(respone);
            }
        });
}

function delete_update(id,project_id){
    if (confirm(getLang('are_you_sure_delete_this_update'))){
        $.ajax({
            type: 'POST',
            url: '/projectActions/deleteUpdateById',
            data: {
                'id': id
            },
            success: function () {
                get_project_info_by_tab('updates','updates',project_id);
            }
        });
    }
}

function add_update_to_project(project_id){
    var title = $('.add-update-form input[name=title]').val();
    var content = $('.add-update-form textarea[name=content]').val();
    var token = $('.add-update-form input[name=token]').val();
    $.ajax({
            type: 'POST',
            url: '/projects/addUpdatesAjax',
            data: {
                'project_id': project_id,
                'token': token,
                'title': title,
                'content': content
            },
            success: function (respone) {
                result = JSON.parse(respone);
                if (result['status'] == '1'){
                    get_project_info_by_tab('updates','updates',project_id);
                    $('#updates .box-form-alert').html('<div class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+result['message']+'</div>');
                    $('form[name="frm_add_update"]')[0].reset();
                }
                else{
                    $('#updates .box-form-alert').html('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+result['message']+'</div>');
                }
                return false;
            }
        });
        return false;
}

function post_comment_to_system(project_id,comment_id){
    var comment = $('textarea#comment-id-'+comment_id).val();
    $.ajax({
            type: 'POST',
            url: '/projectActions/addCommentsAjax',
            data: {
                'project_id': project_id,
                'parent_comment_id': comment_id,
                'comment': comment
            },
            success: function (respone) {
                result = JSON.parse(respone);
                if (result['status'] == '1'){
                    get_project_info_by_tab('comments','comments',project_id);
                    send_notification_for_collaborators(project_id,'6',comment_id);
                }
                else{
                    $('#comment-item-'+comment_id+' .box-form-alert').html('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+result['message']+'</div>');
                    return false;
                }
                return false;
            }
        });
}

function add_comments_to_system(project_id){
    var comment = $('.comment-all-form textarea[name=comment]').val();
    $.ajax({
            type: 'POST',
            url: '/projectActions/addCommentsAjax',
            data: {
                'project_id': project_id,
                'parent_comment_id': 0,
                'comment': comment
            },
            success: function (respone) {
                result = JSON.parse(respone);
                if (result['status'] == '1'){
                    $('#comments .box-form-alert').html('<div class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+result['message']+'</div>');
                    get_project_info_by_tab('comments','comments',project_id);
                    $('.comment-all-form textarea[name=comment]').val('');
                    send_notification_for_collaborators(project_id,'2');
                }
                else{
                    $('#comments .box-form-alert').html('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+result['message']+'</div>');
                    return false;
                }
                return false;
            }
        });
        return false;
}

function confirm_detele_role(role_id){
    $('#alert-box .alert-content').html('<p><strong>'+ getLang('are_you_sure_you_want_to_delete_this_role') +'</strong></p>');
    $('#item_id').val(role_id);
    $('#confirm-option').val('delete_role');
    $('#alert-box').modal('show');
}

function process_delete_role(role_id){
    $('#alert-box').modal('hide');
    $.ajax({
        type: 'POST',
        url: '/projects/deleterole',
        data: {
            'role_id': role_id
        },
        success: function () {
            location.reload();
        }
    });
}

function delete_comment(comment_id){
    $('#alert-box .alert-content').html('<p><strong>'+ getLang('are_you_sure_you_want_to_delete_this_comment') +'</strong></p>');
    $('#item_id').val(comment_id);
    $('#confirm-option').val('delete_comment');
    $('#alert-box').modal('show');
}

function process_delete_comment(comment_id){
    $('#alert-box').modal('hide');
    var project_id = $('#project_id').val();
    $.ajax({
        type: 'POST',
        url: '/projectActions/deleteCommentAjax',
        data: {
            'comment_id': comment_id
        },
        success: function () {
            get_project_info_by_tab('comments','comments',project_id);
            return false;
        }
    });
}

// Show iframe code
function show_iframe_code(){
    $('#frm-copy-iframe').toggle();
}
// milestone page
function confirmRemoveMileStone(id,opt){
    $('#delete_opt').val(opt);
    $('#item_id').val(id);
    $('#alert-box').modal('show');
}



function process_remove_mistone(c,opt) {
    $('#alert-box').modal('hide');
    if (opt == '1'){
        window.location = '/projects/delmilestone/' + c;
    }
    else{
        $("#MileList li#mile-"+c).remove();
    }
}
// validate check html
function isHTML(str) {
    var a = document.createElement('div');
    a.innerHTML = str;
    for (var c = a.childNodes, i = c.length; i--; ) {
        if (c[i].nodeType == 1) return true; 
    }
    return false;
}
// validate title and description of milestone
function validMileStone(title,description){
    var flag = 0;
    if(title.trim().length == 0 || title.trim().length > 60) {
        $('#msgTit').html(getLang('title_is_required_max_length_of_chars_html_or_script_tags_is_not_allowed'));
        flag = 1;
    }else{
        $('#msgTit').html('');
        if(isHTML(title)) {
            $('#msgTit').html(getLang('title_is_required_max_length_of_chars_html_or_script_tags_is_not_allowed'));
            flag = 1;
        }else{
            $('#msgTit').html('');
        }
        
    }
    if(description.trim().length == 0 || description.trim().length > 120) {
        $('#msgDes').html(getLang('content_is_required_max_length_of_chars_html_or_script_tags_is_not_allowed'));
        flag = 1;
    }else{
        $('#msgDes').html('');
        if(isHTML(description)) {
            $('#msgDes').html(getLang('content_is_required_max_length_of_chars_html_or_script_tags_is_not_allowed'));
            flag = 1;
        }else{
            $('#msgDes').html('');
        }
    }
    if(flag == 1) {
        return true;
    }
}

function check_role_in_project(project_id, role_length){
    if(role_length == 0){
        alertBox(getLang('please_create_at_least_one_role'));
    }else{
        $.ajax({
                type: 'POST',
                url: '/projects/checkRoleInProjectAjax',
                data: {
                    'project_id': project_id
                },
                success: function (respone) {
                    result = JSON.parse(respone);
                    if (result['status'] == '1'){
                        window.location = '/projects/milestones/'+project_id;
                    }
                    else{
                        location.reload();
                    }
                }
        });
    }
}

function delete_user_work(work_id,opt){
     if (opt == 'delete'){
   confirmBoxwork(work_id,getLang('are_you_sure_you_want_to_delete_this_work'),1);
     }
     else
     {
          $('#work-'+work_id).remove(); 
     }
}
function accept_delete_user_work(work_id)
{
      $.ajax({
                    type: 'POST',
                    url: '/profile/deleteUserWork',
                    data: {
                        'work_id': work_id
                    },
                    success: function (respone) {
                        location.reload();
                    }
            });
}
function show_list_notification(){
    $.ajax({
                    type: 'POST',
                    url: '/notifications/getListNotificationsAjax',
                    data: {
                        'is_ajax': true
                    },
                    success: function (respone) {
                        $('.box-list-notifications').html(respone);
                    }
            });
}
var loop = 0
function process_notifications(notification_id,user_id){
if(loop == 0)
{
    
    if(notification_id == 1)
    {
        loop = 1;
        window.location.href = '/applicant-profile-'+ user_id;
        return false;
    }
    else
    {
    $.ajax({
                    type: 'POST',
                    url: '/notifications/processNotificationAjax',
                    data: {
                        'notification_id': notification_id
                    },
                    beforeSend: function(){
                        $('#box-loading').modal('show');
                    },
                    success: function (respone) {
                        $('#box-loading').modal('hide');
                        result = JSON.parse(respone);
                        if (result['status'] == 'true'){
                            if (result['action'] == 'reload'){
                                location.reload();
                            }
                            else if(result['action'] == 'confirm'){
                                process_accept_join_project(notification_id);
                            }
                            else{
                                window.location = result['link'];
                            }
                        }
                        else{
                            alertBox(result['message']);
                        }
                    }
            });
    
    }
}
}

function process_accept_join_project(notification_id){
   confirmBoxwork(notification_id,getLang('are_you_sure_you_want_to_accept_this_request'),2);  
}
function accept_join_project(notification_id)
{
    $('.modal-backdrop.fade.in').hide();
    $('.modal.fade.confirm.alert-box.in').hide();
            $.ajax({
                    type: 'POST',
                    url: '/notifications/processAcceptJoinRoleAjax',
                    data: {
                        'notification_id': notification_id
                    },
                    beforeSend: function(){
                        $('#box-loading').modal('show');
                    },
                    success: function (respone) {
                        $('#box-loading').modal('hide');
                        alertBox(respone);
                    }
            });
    }
function process_read_notification(notification_id){
    $.ajax({
                    type: 'POST',
                    url: '/notifications/processReadNotificationAjax',
                    data: {
                        'notification_id': notification_id
                    },
                    success: function () {
                        location.reload();
                    }
            });
}

function load_more_content_by_page(action,boxAppend){
    var current_page = $('#current-page').val();
    var page = parseInt(current_page) + 1;
    $('#current-page').val(page);
    $.ajax({
                    type: 'POST',
                    url: action,
                    data: {
                        'page': page
                    },
                    success: function (respone) {
                        $(boxAppend).append(respone);
                    }
            });
}

function process_confirm_box(){
    var confirm_option = $('#confirm-option').val();
    if (confirm_option == 'join_project'){
        var role_id = $('#role_id').val();
        var project_id = $('#project_id').val();
        var project_role_id = $('#project_role_id').val();
        var message = $('#message-confirm').val();
        send_request_join_project(role_id,project_id,project_role_id,message);
    }
    if (confirm_option == 'delete_comment'){
        var comment_id = $('#item_id').val();
        process_delete_comment(comment_id);
    }
    if (confirm_option == 'delete_role'){
        var role_id = $('#item_id').val();
        process_delete_role(role_id);
    }
    if (confirm_option == 'delete_milestone'){
        var milestone_id = $('#item_id').val();
        var delete_opt = $('#delete_opt').val();
        process_remove_mistone(milestone_id,delete_opt);
    }
}

function send_request_join_project(role_id,project_id,project_role_id, message){
    $('#alert-box').modal('hide');
    $.ajax({
        type: 'POST',
        url: '/usersProjects/joinProject',
        data: {
            'role_id': role_id,
            'project_id': project_id,
            'project_role_id': project_role_id,
            'message': message
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (rs) {
            //console.log(rs);
            location.reload();
        }
    });
}


$(window).load(function () {
    $('#slideHome').flexslider({
        animation: "slide",
        prevText: "",
        nextText: ""
    });
    
    $('#content-slide').flexslider({
        animation: "slide",
        prevText: "",
        nextText: "",
        controlNav: false
    });
});


$('document').ready(function () {
    // add website
    $("#addweb").click(function (){
        var website = "";
        var html = "";
        website = $("#webinfo").val();
        if(website.trim() == "") {
            alertBox(getLang('invalid_website_format_must_be'));
            return  false;
        }
        if(!isUrlValid(website)){
            alertBox(getLang('invalid_website_format_must_be'));
            return  false;
        }

        var lastWebsite = $('#websiteInfo').find('div.alert.alert-default > input[type="hidden"]:last');
        var nameId = 0;

        if (lastWebsite != null && lastWebsite != undefined && typeof lastWebsite != 'undefined') {
            nameId = $(lastWebsite).prop('name');

            if (nameId == null || nameId == "" || nameId == undefined || typeof nameId == 'undefined') {
                nameId = 0;
            }
            else {
                nameId = nameId.replace(/\D/g,'');
                nameId = parseInt(nameId);
            }
        }

        ++nameId;

        // × cant be displayed sometimes, replace it with 'x'
        html =
            '<div class="alert alert-default" role="alert">' + website.substring(0,90) + '...' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
            '<input type="hidden" name="UsersWebsite[no' + nameId + ']" value="' + website + '">' +
            '</div>';
        $("#websiteInfo").append(html);
        $("#webinfo").val("");
    });
    // action search bar
    // $(".showSearch").click(function (event) {
    //     $("#navBarWTP").addClass('hidden');
    //     $("#searchArea").removeClass('hidden');
    // });
    // $("#closeSearch").click(function (event) {
    //     $("#searchArea").addClass('hidden');
    //     $("#navBarWTP").removeClass('hidden' );
    // });
});


/* GET HOME FILTER PROJECT */
function getHomeFilterProject(ele, sType, iPage){
    $(ele).addClass('loading').attr('onclick', '');
    var DataId = $('#' + sType + ' .DataId').val();
    $.ajax({
        type: 'POST',
        url: '/home/blockFilterProject',
        data: {
            'sType' : sType,
            'page' : iPage,
            'DataId' : DataId,
        },
        success: function (respone) {
            var aResult = JSON.parse(respone);
            $('.home-tab-project #' + aResult.data.sType + ' .button-see-more').html(aResult.data.sHtmlButtonSeeMore);
            if(aResult.data.sHtmlItem.DataId != ''){
                $('.home-tab-project #' + aResult.data.sType + ' .DataId').val(aResult.data.sHtmlItem.DataId);
            }
            if(aResult.data.sHtmlItem.sHtmlItem != ''){
                $('.home-tab-project #' + aResult.data.sType + ' ul.wrapper-item').append(aResult.data.sHtmlItem.sHtmlItem);
            }
        }
    });
}
/* END: GET HOME FILTER PROJECT */


/* GET CURRENT POSITION */
function updateLocation(position){
    if(position){
        $.ajax({
            type: 'POST',
            url: '/locations/updateLocation',
            data: {
                'position' : position,
            },
            success: function (respone) {
                var aResult = JSON.parse(respone);
                if(aResult.status > 0){
                    location.reload();
                }
            }
        });
        return true;
    }
    return false;
}
/* END: GET CURRENT POSITION */

function load_discover_see_more(){
    var current_page = $('#current_page').val();
    current_page = parseInt(current_page) + 1;
    $('#current_page').val(current_page);
    var filterCategory = $('.row-box-filter select[name=filterCategory]').val();
    var sortBy = $('.row-box-filter select[name=sortBy]').val();
    var filterRole = $('.row-box-filter select[name=filterRole]').val();
    var filterLocation = $('.row-box-filter input[name=filterLocation]:checked').val();
    $.ajax({
            type: 'POST',
            url: '/discover/getListDiscoverSeeMoreAjax',
            data: {
                'filterCategory' : filterCategory,
                'sortBy' : sortBy,
                'filterRole' : filterRole,
                'filterLocation' : filterLocation,
                'current_page': current_page
            },
            beforeSend: function(){
                $('#box-loading').show();
            },
            success: function (respone) {
                $('#box-loading').hide();
                $('#list-projects').append(respone);
            }
        });
}

function search_project_by_options(){
    $('#box-loading1').modal('show');
    var filterCategory = $('.row-box-filter select[name=filterCategory]').val();
    var sortBy = $('.row-box-filter select[name=sortBy]').val();
    var filterRole = $('.row-box-filter select[name=filterRole]').val();
    var filterLocation = $('.row-box-filter input[name=filterLocation]:checked').val();
    var linkRequest = '/discover/index?filterCategory='+filterCategory+'&sortBy='+sortBy+'&filterRole='+filterRole+'&filterLocation='+filterLocation;
    if ($('#optionsRadios3').is(':checked')){
        var location_radius = $('#location_radius').val();
        linkRequest = linkRequest+'&location_radius='+location_radius;
    }
    window.location = linkRequest;
}


function load_more_my_folder(box_id,type,tab_id){
    $(box_id+'-load-more').show();
    var current_page = $(box_id + '-load-more input[name=current_page]').val();
    var page = parseInt(current_page) + 1;
    $(box_id + '-load-more input[name=current_page]').val(page);
    $.ajax({
            type: 'POST',
            url: '/Myfolder/getListMyfolderSeeMoreAjax',
            data: {
                'page' : page,
                'type' : type
            },
            success: function (respone) {
                $(box_id+'-load-more').hide();
                $(box_id).append(respone);
                if (respone == ''){
                    $(tab_id + ' .see-more-button').hide();
                }
            }
        });
    
}

function load_more_applicants(project_id){
    $('.applicant-loading').show();
    var current_page = $('.applicant-loading input[name=current_page]').val();
    var page = parseInt(current_page) + 1;
    $('.applicant-loading input[name=current_page]').val(page);
    $.ajax({
            type: 'POST',
            url: '/Applicant/getListApplicantSeeMoreAjax',
            data: {
                'page' : page,
                'project_id': project_id
            },
            success: function (respone) {
                $('.applicant-loading').hide();
                $('.list-findmore').append(respone);
                if (respone == ''){
                    $('#findMore .see-more-button').hide();
                }
            }
        });
    
}

function get_application_tab_content(box_id,project_id,role_id,type,project_role_id){
    $.ajax({
            type: 'POST',
            url: '/Applicant/getListCollaboratorTabAjax',
            data: {
                'type': type,
                'project_id' : project_id,
                'role_id' : role_id,
                'project_role_id': project_role_id
            },
            beforeSend: function (){
                $(box_id + ' .container').html('<div class="col-md-12 text-center applicant-loading"><img src="/img/loading.gif" alt="Loading" title="Loading" /></div>');
            },
            success: function (respone) {
                $(box_id + ' .container').html(respone);
            }
        });
}

function show_list_collaborator_findmore(role_id,project_id){
    $.ajax({
            type: 'POST',
            url: '/Applicant/getCollaboratorByRoleAndKeyword',
            data: {
                'role_id' : role_id,
                'project_id': project_id,
                'keyword' : '',
                'list_page': 'false',
                'page': 1
            },
            beforeSend: function (){
                $('#findMore .container').html('<div class="col-md-12 text-center applicant-loading"><img src="/img/loading.gif" alt="Loading" title="Loading" /></div>');
            },
            success: function (respone) {
                $('#findMore .container').html(respone);
            }
        });
}

function get_list_findmore_tab_content(project_id){
    $.ajax({
            type: 'POST',
            url: '/Applicant/getListFindMoreAjax',
            data: {
                'is_ajax' : 'true',
                'project_id': project_id
            },
            beforeSend: function (){
                $('#findMore .container').html('<div class="col-md-12 text-center applicant-loading"><img src="/img/loading.gif" alt="Loading" title="Loading" /></div>');
            },
            success: function (respone) {
                
                $('#findMore .container').html(respone);
            }
        });
}

function get_list_collaborattor_by_keyword(){
    var keyword = $('.box-search-applicant input[name=keyword_search]').val();
    var role_id = $('.box-search-applicant input[name=role_id]').val();
    var project_id = $('#project_id').val();
    $.ajax({
            type: 'POST',
            url: '/Applicant/getCollaboratorByRoleAndKeyword',
            data: {
                'role_id' : role_id,
                'keyword' : keyword,
                'list_page': 'true',
                'project_id':project_id,
                'page': 1
            },
            beforeSend: function (){
                $('#findMore .applicant-loading').show();
            },
            success: function (respone) {
                $('#findMore .applicant-loading').hide();
                $('.list-applicant-collaborators').html(respone);
            }
        });
}

function load_more_collaborator_by_keyword(){
    var keyword = $('.box-search-applicant input[name=keyword_search]').val();
    var role_id = $('.box-search-applicant input[name=role_id]').val();
    var project_id = $('#project_id').val();
    var page = $('#findMore .applicant-loading input[name=current_page]').val();
    var new_page = parseInt(page) + 1;
    $('#findMore .applicant-loading input[name=current_page]').val(new_page);
    $.ajax({
            type: 'POST',
            url: '/Applicant/getCollaboratorByRoleAndKeyword',
            data: {
                'role_id' : role_id,
                'keyword' : keyword,
                'list_page': 'true',
                'find_more':1,
                'project_id': project_id,
                'page': new_page
            },
            beforeSend: function (){
                $('#findMore .applicant-loading').show();
            },
            success: function (respone) {
                $('#findMore .applicant-loading').hide();
                if (respone.replace(/\s/g,"") == ''){
                    $('#findMore .see-more-button').hide();
                }
                $('.list-applicant-collaborators').append(respone);
            }
        });
}


/*
    SELECT FILE INPUT
*/
function clickFileInput(ele){
    if(!$(ele).hasClass('uploading')){
        var selectorClick = $(ele).attr('data-input');
        $(selectorClick).click();
    }
    return false;
}

function in_array(needle, haystack, argStrict) { // eslint-disable-line camelcase
    var key = ''
    var strict = !!argStrict
    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) { // eslint-disable-line eqeqeq
                return true
            }
        }
    }
    return false
}

function array_merge() {
    var args = Array.prototype.slice.call(arguments)
    var argl = args.length
    var arg
    var retObj = {}
    var k = ''
    var argil = 0
    var j = 0
    var i = 0
    var ct = 0
    var toStr = Object.prototype.toString
    var retArr = true
    for (i = 0; i < argl; i++) {
        if (toStr.call(args[i]) !== '[object Array]') {
            retArr = false
            break
        }
    }

    if (retArr) {
        retArr = []
        for (i = 0; i < argl; i++) {
            retArr = retArr.concat(args[i])
        }
        return retArr
    }

    for (i = 0, ct = 0; i < argl; i++) {
        arg = args[i]
        if (toStr.call(arg) === '[object Array]') {
            for (j = 0, argil = arg.length; j < argil; j++) {
                retObj[ct++] = arg[j]
            }
        } else {
            for (k in arg) {
                if (arg.hasOwnProperty(k)) {
                    if (parseInt(k, 10) + '' === k) {
                        retObj[ct++] = arg[k]
                    } else {
                        retObj[k] = arg[k]
                    }
                }
            }
        }
    }

    return retObj
}

function uploadFileDropbox(ele, project_id){
    var file_data = $(ele).prop('files')[0];

    if(file_data == null){
        alertBox('Unable to process your request.');
        $('.form-upload-file input[type=file]').val('');
        return false;
    }

    var ExtImage   = ['image/bmp', 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'];
    var ExtVideo   = ['video/quicktime', 'video/mpeg', 'application/x-troff-msvideo', 'video/avi', 'video/msvideo', 'video/x-msvideo', 'video/mp4', 'application/mp4', 'video/3gpp', 'video/x-ms-wmv', 'video/x-flv'];
    var ExtText    = ['text/plain'];
    var Extpdf     = ['application/pdf'];

    var message = getLang('file_upload_should_be_in_text_image_media_video_in_range_of_size_of_kb_to_mb');

    var AllowFileTypes = array_merge(ExtImage, ExtVideo, ExtText, Extpdf);
    if(!in_array(file_data.type, AllowFileTypes)){
        alertBox(message);
        $('.form-upload-file input[type=file]').val('');
        return false;
    }

    var FileSizeMb = file_data.size;
    var MinSize    = 1024;
    if(in_array(file_data.type, ExtVideo) || in_array(file_data.type, ExtText) || in_array(file_data.type, Extpdf)){
        var MaxSize = (100 * 1024) * 1024;
    }

    if(in_array(file_data.type, ExtImage)){
        var MaxSize = (3 * 1024) * 1024;
    }

    if(FileSizeMb < MinSize || FileSizeMb > MaxSize){
        alertBox(message);
        $('.form-upload-file input[type=file]').val('');
        return false;
    }
    
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('project_id', project_id);
    $.ajax({
        type: 'POST',
        url: '/mettings/uploadFileDropbox',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'POST',
        beforeSend: function(){
            $('#box-loading').modal('show');
            $('.form-upload-file .button_upload_file').addClass('uploading').find('.default').addClass('hidden').next('.loadding').removeClass('hidden');
        },
        success: function (respone) {
            //console.log(respone);
            $('#box-loading').modal('hide');
            $('.form-upload-file input[type=file]').val('');
            $('.form-upload-file .button_upload_file').removeClass('uploading').find('.default').removeClass('hidden').next('.loadding').addClass('hidden');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                send_notification_for_collaborators(project_id,'9');
                if(aResult.data.html_item && aResult.data.html_item != undefined){
                    if($('.template_table_assets .wrapper_table').hasClass('hidden')){
                        $('.template_table_assets .wrapper_table').removeClass('hidden');
                        $('.template_table_assets .table_body').prepend(aResult.data.html_item);
                        $('.template_table_assets .no-item-message').remove();
                        
                        if(aResult.message != null && aResult.message != ''){                       
                            alertBox(aResult.message);
                        }
                    }else{
                        if(!$('.template_table_assets .table_row').hasClass(aResult.data.response.id)){
                            $('.template_table_assets .table_body').prepend(aResult.data.html_item);

                            if(aResult.message != null && aResult.message != ''){
                                alertBox(aResult.message);
                            }
                        }
                    }
                }else{
                    alertBox(aResult.message);
                }
            }else{
                alertBox(aResult.message);
            }
        }
    });
}

function downloadFileDropbox(ele, project_id, name_file){
    $.ajax({
        type: 'POST',
        url: '/mettings/downloadFileDropbox',
        data: {
            'project_id': project_id,
            'name_file' : name_file,
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                window.location.href = aResult.data.response.link;
            }else{
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }
        }
    });
}

function deleteFileDropbox(ele, project_id, name_file){
    $.ajax({
        type: 'POST',
        url: '/mettings/deleteFileDropbox',
        data: {
            'project_id': project_id,
            'name_file' : name_file,
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                $('.' + aResult.data.response.wtpid).remove();
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }else{
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }
        }
    });
}

function rand (min, max) {
    var argc = arguments.length
    if (argc === 0) {
        min = 0
        max = 2147483647
    } else if (argc === 1) {
        throw new Error('Warning: rand() expects exactly 2 parameters, 1 given')
    }
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function alertBox(Message){
    $(".wrapper_model").show();
    var sHtml = '';
    sHtml += '<div class="modal fade alert-box alert" tabindex="-1" role="dialog">';
        sHtml += '<div class="modal-dialog" role="document">';
            sHtml += '<div class="modal-content">';
                sHtml += '<div class="modal-header">';
                    sHtml += '<button onclick="close_popup();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>';
                    sHtml += '<h4 class="modal-title" id="myModalLabel"></h4>';
                sHtml += '</div>';
                sHtml += '<div class="modal-body">';
                    sHtml += '<div class="row">';
                        sHtml += '<div class="col-xs-12 text-center alert-content"><p><strong>' + Message + '</strong></p></div>';
                    sHtml += '</div>';
                    sHtml += '<div class="row">';
                        sHtml += '<div class="col-xs-12 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close" style="float: none;" >'+getLang('ok')+'</button></div>';
                    sHtml += '</div>';
                sHtml += '</div>';
            sHtml += '</div>';
        sHtml += '</div>';
    sHtml += '</div>';

    $('.wrapper_model').html(sHtml);
    $('.alert-box.alert').modal();
    $('.alert-box.alert').on('shown.bs.modal', function(e) {

    });

    $('.alert-box.alert').on('hidden.bs.modal', function(e) {
        $('body').css({ 'padding-right' : '0px' });
    });
}

function alertBoxprofile(Message){
    $(".wrapper_model").show();
    var sHtml = '';
    sHtml += '<div class="modal fade alert-box alert in" tabindex="-1" role="dialog" style="display: block;"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button onclick="close_popup();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel"></h4></div><div class="modal-body"><div class="row"><div class="col-xs-12 text-center alert-content"><p><strong>'+Message+'</strong></p></div></div><div class="row"><div class="col-xs-12 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close" style="float: none;" onclick="close_popup();">OK</button></div></div></div></div></div></div>';
    $('.wrapper_model').html(sHtml);
}

function confirmBox(ele, Message){
    var sHtml = '';
    sHtml += '<div class="modal fade confirm alert-box" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
        sHtml += '<div class="modal-dialog" role="document">';
            sHtml += '<div class="modal-content">';
                sHtml += '<div class="modal-header">';
                    sHtml += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>';
                    sHtml += '<h4 class="modal-title" id="myModalLabel"></h4>';
                sHtml += '</div>';
                sHtml += '<div class="modal-body">';
                    sHtml += '<div class="row">';
                        sHtml += '<div class="col-xs-12 text-center alert-content"><p><strong>' + Message + '</strong></p></div>';
                    sHtml += '</div>';
                    sHtml += '<div class="row">';
                        sHtml += '<div class="col-xs-6 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close">'+getLang('no')+'</button></div>';
                        sHtml += '<div class="col-xs-6 text-center"><button class="btn btn-launch btn-accept" onclick="resetWrapperModel();' +$(ele).attr('data-action')+'">'+getLang('yes')+'</button></div>';
                    sHtml += '</div>';
                sHtml += '</div>';
            sHtml += '</div>';
        sHtml += '</div>';
    sHtml += '</div>';
    $('.wrapper_model').html(sHtml);
    $('.alert-box.confirm').modal('show');
    $('.alert-box.confirm').on('shown.bs.modal', function(e) {

    });
    $('.alert-box.confirm').on('hidden.bs.modal', function(e) {
        $('body').css({ 'padding-right' : '0px' });
    });
}

function confirmBoxwork(ele, Message,type){
    var sHtml = '';
    sHtml += '<div class="modal fade confirm alert-box" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
        sHtml += '<div class="modal-dialog" role="document">';
            sHtml += '<div class="modal-content">';
                sHtml += '<div class="modal-header">';
                    sHtml += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>';
                    sHtml += '<h4 class="modal-title" id="myModalLabel"></h4>';
                sHtml += '</div>';
                sHtml += '<div class="modal-body">';
                    sHtml += '<div class="row">';
                        sHtml += '<div class="col-xs-12 text-center alert-content"><p><strong>' + Message + '</strong></p></div>';
                    sHtml += '</div>';
                    sHtml += '<div class="row">';
                        sHtml += '<div class="col-xs-6 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close">'+getLang('no')+'</button></div>';
                      
                      if(type == 1)
                       {
                        sHtml += '<div class="col-xs-6 text-center"><button class="btn btn-launch btn-accept" onclick="accept_delete_user_work('+ele+')">'+getLang('yes')+'</button></div>';
                       }
                       else if (type == 2)
                       {
                            sHtml += '<div class="col-xs-6 text-center"><button class="btn btn-launch btn-accept" onclick="accept_join_project('+ele+')">'+getLang('yes')+'</button></div>';
                       }
                       else if (type == 3)
                       {
                           sHtml += '<div class="col-xs-6 text-center"><button class="btn btn-launch btn-accept" onclick="delete_gr_chat('+ele+')">'+getLang('yes')+'</button></div>';   
                       }
                    sHtml += '</div>';
                sHtml += '</div>';
            sHtml += '</div>';
        sHtml += '</div>';
    sHtml += '</div>';
    $('.wrapper_model').html(sHtml);
    $('.alert-box.confirm').modal('show');
    $('.alert-box.confirm').on('shown.bs.modal', function(e) {

    });
    $('.alert-box.confirm').on('hidden.bs.modal', function(e) {
        $('body').css({ 'padding-right' : '0px' });
    });
}

function resetWrapperModel(){
    $('#alert-box').modal('hide');
    $('.modal-backdrop').remove();
}

function addProjectUpdate(ele, project_id, type_id){
    var form_values = $(ele).serializeArray();
    form_values.push({'name': 'project_id', 'value': project_id});
    $.ajax({
        type: 'POST',
        url: '/ProjectsUpdate/addProjectUpdate',
        data: form_values,
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                send_notification_for_collaborators(project_id,'10');
                $('.wrapper-timeline .wrapper-content ul').prepend(aResult.data.html_item);
                $('.form-add .update input').val('');
                $('.form-add .update textarea').val('');
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }else{
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
                $('#alert-box').on('hidden.bs.modal', function (e) {
                    $('.time-line .timeline-add').addClass('open');
                });
            }
        }
    });
}

function updateProjectUpdate(ele, project_update_id, type_id){
    var form_values = $(ele).serializeArray();
    form_values.push({'name': 'project_update_id', 'value': project_update_id});
    form_values.push({'name': 'type_id', 'value': type_id});
    $.ajax({
        type: 'POST',
        url: '/ProjectsUpdate/updateProjectUpdate',
        data: form_values,
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                $('.project_update_' + project_update_id + ' .title-timeline').html(aResult.data.response.title);
                $('.project_update_' + project_update_id + ' .content-timeline').html(aResult.data.response.content);
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }else{
                alertBox(aResult.message);
                $('#alert-box').on('hidden.bs.modal', function (e) {
                    $('.project_update_' + project_update_id).addClass('open');
                });
            }
        }
    });
}

function deleteProjectUpdate(ele, project_update_id, type_id){
    var form_values = $(ele).serializeArray();
    form_values.push({'name': 'project_update_id', 'value': project_update_id});
    form_values.push({'name': 'type_id', 'value': type_id});
    $.ajax({
        type: 'POST',
        url: '/ProjectsUpdate/deleteProjectUpdate',
        data: form_values,
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                $('.project_update_' + project_update_id).remove();
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }else{
                alertBox(aResult.message);
                $('#alert-box').on('hidden.bs.modal', function (e) {
                    $('.project_update_' + project_update_id).addClass('open');
                });
            }
        }
    });
}

function updateProjectMilestone(ele, project_update_id, type_id){
    var form_values = $(ele).serializeArray();
    form_values.push({'name': 'project_update_id', 'value': project_update_id});
    form_values.push({'name': 'type_id', 'value': type_id});
    $.ajax({
        type: 'POST',
        url: '/ProjectsUpdate/updateProjectMilestone',
        data: form_values,
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                if(type_id == 'add'){
                    $('.wrapper-timeline .wrapper-content ul').prepend(aResult.data.html_item);

                    if(aResult.data.milestone_next != null){
                        $('.form-add .milestone input').val(aResult.data.milestone_next.title);
                        $('.form-add .milestone textarea').val(aResult.data.milestone_next.content);
                        $('.form-add .milestone form').attr('onsubmit', 'updateProjectMilestone(this, \''+aResult.data.milestone_next.id+'\', \'add\'); return false;');
                    }else{
                        $('.form-add .item.milestone').remove();
                    }
                }else if(type_id == 'update'){
                    $('.project_update_'+ project_update_id + ' .title-timeline').html(aResult.data.response.title);
                    $('.project_update_'+ project_update_id + ' .content-timeline').html(aResult.data.response.content);
                }
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }else{
                alertBox(aResult.message);
                $('#alert-box').on('hidden.bs.modal', function (e) {
                    if(type_id == 'add'){
                        $('.time-line .timeline-add').addClass('open');
                    }else if(type_id == 'update'){
                        $('.project_update_28').addClass('open');
                    }
                });
            }
        }
    });
}
/*
    END: SELECT FILE INPUT
*/

/*
    GET APPLICANT PROJECT
*/
function getApplicantProject(ele, iPage, UserId){
    if(!$(ele).hasClass('loading')){
        $.ajax({
            type: 'POST',
            url: '/applicant/getProjectApplicantAjax',
            data: {
                'page' : iPage,
                'UserId' : UserId,
            },
            beforeSend: function(){
                $(ele).addClass('loading');
            },
            success: function (respone) {
                $(ele).removeClass('loading');
                var aResult = JSON.parse(respone);
                if(aResult.data.sHtmlItem != null){
                    $('ul.template-project-list').append(aResult.data.sHtmlItem);
                }
                if(aResult.data.TotalProject < aResult.data.PageSize){
                    $(ele).closest('.button-see-more').remove();
                }else{
                    $(ele).attr('onclick', 'getApplicantProject(this, \''+(parseInt(iPage) + 1)+'\', \''+UserId+'\');');
                }
            }
        });
    }
    return false;
}
/*
    END: GET APPLICANT PROJECT
*/

/*
    FOLLOWING AJAX
*/
function addFollowing(ele, ItemId, ConnectionId, sType){
    $.ajax({
        type: 'POST',
        url: '/followings/followingAjax',
        data: {
            'ItemId' : ItemId,
            'ConnectionId' : ConnectionId,
            'sType' : sType
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            //console.log(respone);
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 1){
                var TotalFollower  = aResult.data.TotalFollow.TotalFollower;
                var TotalFollowing = aResult.data.TotalFollow.TotalFollowing;
                if(TotalFollower != null) $('.total_follower_' + ItemId + ' .number').text(TotalFollower);
                if(TotalFollowing != null) $('.total_following_' + ItemId + ' .number').text(TotalFollowing);

                var TotalFollower  = aResult.data.TotalFollowUserCurrent.TotalFollower;
                var TotalFollowing = aResult.data.TotalFollowUserCurrent.TotalFollowing;
                var UserId         = aResult.data.TotalFollowUserCurrent.iUserId;

                if(TotalFollower != null) $('.total_follower_' + UserId + ' .number').text(TotalFollower);
                if(TotalFollowing != null) $('.total_following_' + UserId + ' .number').text(TotalFollowing);

                var NameText = '';
                var sClass = '';
                if(ConnectionId == 1){
                    ConnectionId = 2;
                    NameText = getLang('following');
                }else{
                    ConnectionId = 1;
                    NameText = getLang('follow');
                    sClass = ' unfollow ';
                }
                
                var sHtml = '';

                var sAction = 'addFollowing(this, \''+ItemId+'\', \''+ConnectionId+'\', \''+sType+'\'); return false;';
                sHtml += '<a href="javascript:void(0)" onclick="'+sAction+'" class="'+sClass+'">';
                    sHtml += NameText;
                sHtml += '</a>';
                $('.following_' + ItemId).html(sHtml);
            }
            if(aResult.message != null && aResult.message != ''){
                alertBox(aResult.message);
            }
        }
    });
}
/*
    END: FOLLOWING AJAX
*/


/*
    ACTION SAVE
*/
function actionSave(ele, UserId, ProjectId, RoleId){
    var project_role_id = $("#project_role_id").val();
    $.ajax({
        type: 'POST',
        url: '/collaborators/actionSave',
        data: {
            'UserId' : UserId,
            'ProjectId' : ProjectId,
            'RoleId' : RoleId,
            'project_role_id' : project_role_id,
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 1){
               $(ele).remove();
            }
            if(aResult.message != null && aResult.message != ''){
                alertBox(aResult.message);
            }
        }
    });
}
/*
    END: ACTION SAVE
*/

/*
    ACTION OFFER
*/
function actionOffer(ele, UserId, ProjectId, RoleId){
    var project_role_id = $("#project_role_id").val();
    $.ajax({
        type: 'POST',
        url: '/collaborators/actionOffer',
        data: {
            'UserId' : UserId,
            'ProjectId' : ProjectId,
            'RoleId' : RoleId,
            'project_role_id' : project_role_id,
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            //console.log(respone);
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 1){
               $(ele).remove();
            }
            if(aResult.message != null && aResult.message != ''){
                alertBox(aResult.message);
            }
        }
    });
}
/*
    END: ACTION SAVE
*/


/*
    GET SEE MORE FOLLOWING
*/
function getSeeMoreFollowing(ele, Page, UserId){
    if(!$(ele).hasClass('loading')){
        $.ajax({
            type: 'POST',
            url: '/applicant/getSeeMoreFollowing',
            data: {
                'user_id' : UserId,
                'page' : Page,
            },
            beforeSend: function(){
                $(ele).addClass('loading');
            },
            success: function (respone) {
                $(ele).removeClass('loading');
                var aResult = JSON.parse(respone);
                if(aResult.status == 1){
                    if(aResult.data.sHtmlItem != null && aResult.data.sHtmlItem != ''){
                        $('.template_applicant_follow').append(aResult.data.sHtmlItem);
                    }

                    if(aResult.data.page != null && aResult.data.page != ''){
                        $(ele).attr('onclick', 'getSeeMoreFollowing(this, \''+aResult.data.page+'\',\''+UserId+'\');');
                    }else{
                        $(ele).closest('.button-see-more').remove();
                    }
                }
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }
        });
    }
}
/*
    END: GET SEE MORE FOLLOWING
*/

/*
    GET SEE MORE FOLLOWER
*/
function getSeeMoreFollower(ele, Page, UserId){
    if(!$(ele).hasClass('loading')){
        $.ajax({
            type: 'POST',
            url: '/applicant/getSeeMoreFollower',
            data: {
                'user_id' : UserId,
                'page' : Page,
            },
            beforeSend: function(){
                $(ele).addClass('loading');
            },
            success: function (respone) {
                $(ele).removeClass('loading');
                var aResult = JSON.parse(respone);
                if(aResult.status == 1){
                    if(aResult.data.sHtmlItem != null && aResult.data.sHtmlItem != ''){
                        $('.template_applicant_follow').append(aResult.data.sHtmlItem);
                    }

                    if(aResult.data.page != null && aResult.data.page != ''){
                        $(ele).attr('onclick', 'getSeeMoreFollower(this, \''+aResult.data.page+'\',\''+UserId+'\');');
                    }else{
                        $(ele).closest('.button-see-more').remove();
                    }
                }
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }
        });
    }
}
/*
    END: GET SEE MORE FOLLOWER
*/

function htmlspecialchars (string, quoteStyle, charset, doubleEncode) {
    var optTemp = 0
    var i = 0
    var noquotes = false
    if (typeof quoteStyle === 'undefined' || quoteStyle === null) {
        quoteStyle = 2
    }

    string = string || ''
    string = string.toString()

    if (doubleEncode !== false) {
        // Put this first to avoid double-encoding
        string = string.replace(/&/g, '&amp;')
    }

    string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;')

    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    }
    
    if (quoteStyle === 0) {
        noquotes = true
    }
    if (typeof quoteStyle !== 'number') {
        // Allow for a single string or an array of string flags
        quoteStyle = [].concat(quoteStyle)
        for (i = 0; i < quoteStyle.length; i++) {
            // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
            if (OPTS[quoteStyle[i]] === 0) {
                noquotes = true
            } else if (OPTS[quoteStyle[i]]) {
                optTemp = optTemp | OPTS[quoteStyle[i]]
            }
        }
        quoteStyle = optTemp
    }
    if (quoteStyle & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/'/g, '&#039;')
    }
    if (!noquotes) {
        string = string.replace(/"/g, '&quot;')
    }
    return string
}

/*
    ACTION BLOCK COLLABORATOR
*/
function actionBlockCollaborator(ele, UserId, TypeId){
    $.ajax({
        type: 'POST',
        url: '/collaborators/actionBlockCollaborator',
        data: {
            'UserId' : UserId,
            'TypeId' : (TypeId == null) ? 'block' : 'unblock',
        },
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 1){
                var UserId = aResult.data.UserId;
                $('.user_following_' + UserId).remove();

                var TotalFollower  = aResult.data.TotalFollow.TotalFollower;
                var TotalFollowing = aResult.data.TotalFollow.TotalFollowing;
                if(TotalFollower != null) $('.total_follower_' + UserId + ' .number').text(TotalFollower);
                if(TotalFollowing != null) $('.total_following_' + UserId + ' .number').text(TotalFollowing);

                var TotalFollower  = aResult.data.TotalFollowUserCurrent.TotalFollower;
                var TotalFollowing = aResult.data.TotalFollowUserCurrent.TotalFollowing;
                var UserId         = aResult.data.TotalFollowUserCurrent.iUserId;

                if(TotalFollower != null) $('.total_follower_' + UserId + ' .number').text(TotalFollower);
                if(TotalFollowing != null) $('.total_following_' + UserId + ' .number').text(TotalFollowing);
            }
            if(aResult.message != null && aResult.message != ''){
                alertBox(aResult.message);
            }
        }
    });
}

/*
    END: ACTION BLOCK COLLABORATOR
*/

/*
    GET SEE MORE FOLLOWER
*/
function getSeeMoreBlocked(ele, Page){
    if(!$(ele).hasClass('loading')){
        $.ajax({
            type: 'POST',
            url: '/Users/getSeeMoreBlocked',
            data: {
                'page' : Page,
            },
            beforeSend: function(){
                $(ele).addClass('loading');
            },
            success: function (respone) {
                $(ele).removeClass('loading');
                var aResult = JSON.parse(respone);
                if(aResult.status == 1){
                    if(aResult.data.sHtmlItem != null && aResult.data.sHtmlItem != ''){
                        $('.template_applicant_follow').append(aResult.data.sHtmlItem);
                    }
                    if(aResult.data.page != null && aResult.data.page != ''){
                        $(ele).attr('onclick', 'getSeeMoreBlocked(this, \''+aResult.data.page+'\');');
                    }else{
                        $(ele).closest('.button-see-more').remove();
                    }
                }
                if(aResult.message != null && aResult.message != ''){
                    alertBox(aResult.message);
                }
            }
        });
    }
}
/*
    END: GET SEE MORE FOLLOWER
*/

/*
    GET ACTION SIGNUP NEWS LETTER
*/
function actionSignupNewsletter(ele){
    // alertBox('abc');
    // return false;
    var form_values = $(ele).serializeArray();
    $.ajax({
        type: 'POST',
        url: '/UsersSignupNewsletter/actionSignupNewsletter',
        data: form_values,
        beforeSend: function(){
            $('#box-loading').modal('show');
        },
        success: function (respone) {
            $('#box-loading').modal('hide');
            var aResult = JSON.parse(respone);
            if(aResult.status == 1){
                $(ele).find('input').val('');
            }
            
            if(aResult.message != null && aResult.message != ''){
                alertBox(aResult.message);
            }
        }
    });
}
/*
    END: GET ACTION SIGNUP NEWS LETTER
*/


/*
    GET SET LANGUAGE AJAX
*/
function setLanguageAjax(ele){
    $.ajax({
        type: 'POST',
        url: '/core/setLanguageAjax',
        data: {
            'LangParam': $(ele).attr('data-language'),
        },
        beforeSend: function(){},
        success: function (respone) {
            var aResult = JSON.parse(respone);
            if(aResult.status == 0){
                location.reload();   
            }
        }
    });
}
/*
    END: GET SET LANGUAGE AJAX
*/
function close_p()
{
 $('.check_chat').hide();
 $('.ip-chat').focus();
}
lp = 0;
function send_chat_message(room_id){
    var html = '';
    var chat_content = $('.wrapper_messages_footer input[name=chat_content]').val();

    if(chat_content !== '')
    {
        var check = new Array ("'","~","@","#","$","%","^","&","*",";","/","\\","|");
        var sum = check.length;
        for (var i in check) {if (!Array.prototype[i]) {sum += chat_content.lastIndexOf(check[i])}}
        if (sum) {
            $('.check_chat').show();
        return false;
    }
    }
    if(chat_content !== '')
    {
        var len = chat_content.length;
        if(len > 120)
        {
        $('.check_chat').show();
        return false;
        }
    }
    if(chat_content === '')
    {
        $('.check_chat').show();
        return false;
    }
    
    current_counter++;      
    $.ajax({
        type: 'POST',
        url: '/UsersProjects/sendChatMessageAjax',
        data: {
            'room_id': room_id,
            'chat_content': chat_content
        },
        success: function (respone) {
            var result = JSON.parse(respone);
            if(result['status'] == '1'){
                if(result['text_chat'] == 1)
                {
                location.reload(); 
                }
                else
                {
                var div = document.createElement('div');
                div.className = 'template_list_message';
                div.innerHTML = result['text_chat'];
                document.getElementById('template_list_message').appendChild(div);
                $('.ip-chat').val('');
                $("#list-messages").scrollTop($("#list-messages")[0].scrollHeight);
                }
          }
            else{
                $('.check_chat').show();
                return false;
            }
        }
    });

    return false;
}

function cancel_dropdown(){
    $('.add_to_group .dropdown').on({
        "shown.bs.dropdown": function () {
            this.closable = false;
        },
        "click": function () {
            this.closable = true;
        },
        "hide.bs.dropdown": function () {
            return this.closable;
        }
    });
    $('.add_to_group .dropdown').prev().dropdown('toggle');
}

function select_member_add_to_group(member_id){
    $('.add_to_group .dropdown').on({
        "shown.bs.dropdown": function () {
            this.closable = false;
        },
        "click": function () {
            this.closable = false;
        },
        "hide.bs.dropdown": function () {
            return this.closable;
        }
    });
    var data_selected = $('#member-'+member_id).attr('data-selected');
    if (data_selected == '0'){
        $('#member-'+member_id).attr('data-selected','1');
        $('#member-'+member_id).addClass('selected');
        var data_name = $('#member-'+member_id).attr('data-name');
        $('.list_user_accept').append('<span id="member-selected-'+member_id+'">' + data_name + '</span>');
    }
    else{
        $('#member-'+member_id).attr('data-selected','0');
        $('#member-'+member_id).removeClass('selected');
        $('#member-selected-'+member_id).remove();
    }
}

function add_member_to_group(room_id,project_id){
        $('#btn-creat-group').attr('data-active','1');
        var listIds = '';
        $('.template-list-chat li.selected').each(function () {
            var member_id = $(this).attr('data-id');
            listIds = listIds + member_id + ';';
        });
        if (listIds == '') {
            alertBoxChat(getLang('please_select_one_member'));
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: '/UsersProjects/addMemberToGroupAjax',
                data: {
                    'listIds': listIds,
                    'room_id': room_id,
                    'project_id': project_id
                },
                success: function (respone) {
                    var result = JSON.parse(respone);
                    if (result['status'] == '1') {
                        window.location = '/chat-room/' + project_id + '/' + result['room_id'];
                    } else {
                        alertBoxChat(result['message']);
                        return false;
                    }
                }
            });
        }
 
}

function get_old_message(room_id){
    var current_page = $('#line-see-old a').attr('data-page');

    var new_page = parseInt(current_page) + 1;
    $('#line-see-old a').attr('data-page',new_page);
    $.ajax({
        type: 'POST',
        url: '/UsersProjects/getOldMessagesAjax',
        data: {
            'room_id': room_id,
            'page': new_page
        },
        beforeSend: function(){
            $('#box-old-message-loading').html('<div class="col-md-12 text-center applicant-loading"><img src="/img/loading.gif" alt="Loading" title="Loading" /></div>');
        },
        success: function (respone) {
            $('#box-old-message-loading').html('');
            var result = JSON.parse(respone);
            if (result['status'] == '1') {
                $('#line-see-old').after(result['message']);
            } else {
                $('#line-see-old').hide();
                return false;
            }
        }
    });
}

function get_my_folder_by_type(type,contentId){
    $.ajax({
        type: 'POST',
        url: '/Myfolder/getMyFolderTabAjax',
        data: {
            'type': type
        },
        beforeSend: function(){
            $(contentId + ' .myfolder-tab-content').html('<div class="col-md-12 text-center applicant-loading"><img src="/img/loading.gif" alt="Loading" title="Loading" /></div>');
        },
        success: function (respone) {
            $(contentId + ' .myfolder-tab-content').html(respone);
        }
    });
}

function delete_group_chat(room_id){
    confirmBoxwork(room_id,getLang('are_you_sure_delete_this_group'),3);
}
function delete_gr_chat(room_id)
{
       $.ajax({
            type: 'POST',
            url: '/UsersProjects/deleteRoomAjax',
            data: {
                'room_id': room_id
            },
            success: function () {
                location.reload();
            }
        });
}

function send_request_invite(user_id,project_id){
    $.ajax({
        type: 'POST',
        url: '/Notifications/sendInviteRequestAjax',
        data: {
            'user_id': user_id,
            'project_id': project_id
        },
        beforeSend: function () {
            $('#box-loading').modal('show');
        },
        success: function () {
            location.reload();
        }
    });
}
function view_profile(user_id)
{
    var url = "/applicant-profile-"+user_id;
    window.location.href = "/applicant-profile-"+user_id;
}