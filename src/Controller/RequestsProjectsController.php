<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersRoles Controller
 *
 * @property \App\Model\Table\UsersRolesTable $UsersRoles
 */
class RequestsProjectsController extends AppController
{
  public function index()
  {
      $this->paginate = [
          'contain' => ['Requests', 'Projects']
      ];
      $requestsProjects = $this->paginate($this->RequestsProjects);

      $this->set(compact('requestsProjects'));
      $this->set('_serialize', ['requestsProjects']);
  }


  public function view($id = null)
  {
      $requestsProjects = $this->RequestsProjects->get($id, [
          'contain' => ['Requests', 'Projects']
      ]);

      $this->set('requestsProjects', $requestsProjects);
      $this->set('_serialize', ['requestsProjects']);
  }

  public function add()
  {
      $requestsProjects = $this->RequestsProjects->newEntity();
      if ($this->request->is('post')) {
          $requestsProjects = $this->RequestsProjects->patchEntity($requestsProjects, $this->request->data);
          if ($this->RequestsProjects->save($requestsProjects)) {
              $this->Flash->success(__('The users role has been saved.'));

              return $this->redirect(['action' => 'index']);
          } else {
              $this->Flash->error(__('The users role could not be saved. Please, try again.'));
          }
      }
      $requests = $this->RequestsProjects->Requests->find('list', ['limit' => 200]);
      $projects = $this->RequestsProjects->Projects->find('list', ['limit' => 200]);
      $this->set(compact('requestsProjects', 'requests', 'projects'));
      $this->set('_serialize', ['requestsProjects']);
  }

  public function edit($id = null)
  {
      $requestsProjects = $this->RequestsProjects->get($id, [
          'contain' => []
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $requestsProjects = $this->RequestsProjects->patchEntity($requestsProjects, $this->request->data);
          if ($this->RequestsProjects->save($requestsProjects)) {
              $this->Flash->success(__('The users role has been saved.'));

              return $this->redirect(['action' => 'index']);
          } else {
              $this->Flash->error(__('The users role could not be saved. Please, try again.'));
          }
      }
      $requests = $this->RequestsProjects->Requests->find('list', ['limit' => 200]);
      $projects = $this->RequestsProjects->Projects->find('list', ['limit' => 200]);
      $this->set(compact('requestsProjects', 'requests', 'projects'));
      $this->set('_serialize', ['requestsProjects']);
  }

  public function delete($id = null)
  {
      $this->request->allowMethod(['post', 'delete']);
      $requestsProjects = $this->RequestsProjects->get($id);
      if ($this->RequestsProjects->delete($requestsProjects)) {
          $this->Flash->success(__('The users role has been deleted.'));
      } else {
          $this->Flash->error(__('The users role could not be deleted. Please, try again.'));
      }

      return $this->redirect(['action' => 'index']);
  }
}
