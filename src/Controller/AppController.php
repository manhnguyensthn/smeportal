<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Mailer\Email;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\Network\Http\Client;
use App\Lib\CoreLib;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public $_message = "";
    public $_data = [];
    public $_status = '-1';
    public $_errors = [];
    public $_user_id = '';
    public $components = array('PushNotification');
    public $_lang = 'en';

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Projects',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Projects',
                'action' => 'index'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']
                ]
            ],
            'authError' => false,
            'storage' => ['className' => 'Session', 'key' => 'Auth.User'],
        ]);
        $this->loadComponent('Cookie');
        // Set config cookie
        $this->Cookie->config('path', '/');
        $this->Cookie->config([
            'expires' => '+30 days',
            'httpOnly' => false
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
//        $this->viewBuilder()->layout('default');
    }

    public function beforeFilter(Event $event) {
        $this->_setLocaleLang();


        $this->set('cookieHelper', $this->Cookie);
        // check param
        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {
            $this->viewBuilder()->layout('admin');
        }
        $this->Auth->allow('activate');
        $this->checkDuplicateUserLogin();
    }

    /**
     * set Locale Lang
     * @author Roxannie Nguyen jr
     */
    private function _setLocaleLang() {
        $aRequestSession = $this->request->session();
        $lang = $aRequestSession->read('Config.language');
        $Languages = CoreLib::getDataLanguage();
        if (!array_key_exists($lang, $Languages))
            $lang = 'en';
        if (isset($Languages[$lang]['code']) && !empty($Languages[$lang]['code'])) {
            $this->_lang = $lang;
            I18n::locale($Languages[$lang]['code']);
        } else {
            I18n::locale('en_US');
        }
    }

    /**
     * set Language
     * @param $LangParam
     * @author Roxannie Nguyen jr
     * @return boolean
     */
    public function setLanguage($LangParam) {
        $aLanguages = ['en', 'es', 'ja'];
        if (in_array($LangParam, $aLanguages)) {
            $aRequestSession = $this->request->session();
            $dataLanguage = $aRequestSession->write('Config.language', $LangParam);
            return true;
        }
        return false;
    }

    private function checkDuplicateUserLogin() {

        if (!empty($this->Auth->user())) {

            if ($this->Auth->user('token_session')) {
                $this->Token = TableRegistry::get('Tokens');
				$this->Users = TableRegistry::get('Users');
				$user = $this->Users->find('all', [
                    'conditions' => ['email' => $this->Auth->user('email')]
                ])->first();
                    if(empty($user))
                {
                     return $this->redirect($this->Auth->logout());
                }
				$query = $this->Token->find('all', [
                    'conditions' => ['Tokens.token' => $this->Auth->user('token_session')]
                ]);
				$userid = $user->id;
				$token = $this->Token->find('all', [
                    'conditions' => ['user_id' => $userid]
                ])->first();
			    if(!empty($token->platform) && $token->platform == 2)
				{
                $data = $query->first();
                if (empty($data)) {
                    $this->Flash->error(__('Tài khoản đã đăng nhập ở thiết bị khác.'), ['key' => 'logout_duplicate']);
                    return $this->redirect($this->Auth->logout());
                }
				}
            }
            $this->set('authUser', $this->Auth->user());
        }
    }

    public function stripUnicode($string) {
        $unicode = array(
            'a' => 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ|Đ',
            'e' => 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'ì|í|ị|ỉ|ĩ|Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ',
            'y' => 'ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ'
        );
        foreach ($unicode as $nonUnicode => $uni) {
            $string = preg_replace("/($uni)/", $nonUnicode, $string);
        }
        return $string;
        //exit;
    }

    public function CustomSlug($string, $character = '-') {
        $string = $this->stripUnicode($string);
        $string = Inflector::slug($string, $character);
        return strtolower($string);
    }

    public function responApi($status = 0, $message = '', $data = []) {
        if ($this->request->isJson()) {
            $this->set([
                'message' => $message,
                'data' => $data,
                'status' => $status,
                '_serialize' => ['message', 'data', 'status']
            ]);
        } else {
            echo json_encode([
                'message' => $message,
                'data' => $data,
                'status' => $status
            ]);
        }
    }

    // gen token firebase
    public function create_custom_token($uid, $is_premium_account) {
        $now_seconds = time();
        $payload = array(
            "iss" => FIREBASE_SERVICE_EMAIL,
            "sub" => FIREBASE_SERVICE_EMAIL,
            "aud" => "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
            "iat" => $now_seconds,
            "exp" => $now_seconds + (60 * 60), // Maximum expiration time is one hour
            "uid" => $uid,
            "claims" => array(
                "premium_account" => $is_premium_account
            )
        );
        $rest = JWT::encode($payload, FIREBASE_PRIVATE_KEY, "RS256");
        return $rest;
    }

    function generateRandomString() {
        $str = md5(rand(1, 100) . time());
        return sha1($str);
    }

    function sendMail($template, $from, $to, $viewArrs = null, $subject = '', $layout = 'default', $format = 'html') {
        $mail = new Email($layout);
        $from = [EMAIL_LOGIN => 'Hiệp hội doanh nghiệp nhỏ và vừa'];
        $mail->transport('default')
                ->template($template, $layout)
                ->emailFormat($format)
                ->viewVars($viewArrs)
                ->from($from)
                ->to($to)
                ->subject($subject);
        if ($mail->send()) {
            return true;
        }
        return false;
    }

    // create token login
    public function createToken($user_id = 0, $platform = 0, $tk = null) {
        if ($user_id == 0) {
            return FALSE;
        }
        if (!$tk) {
            $tk = $this->generateRandomString();
        }
        $this->Token = TableRegistry::get('Tokens');
        $this->request->data['user_id'] = $user_id;
        $this->request->data['platform'] = $platform;
        $this->request->data['token'] = $tk;
        $this->request->data['created'] = date("Y-m-d H:i:s");
        $token = $this->Token->newEntity();
        $token = $this->Token->patchEntity($token, $this->request->data);
        if ($this->Token->save($token)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // clear token logout
    public function clearToken($user_id, $platform = 2) {
        $this->Token = TableRegistry::get('Tokens');
        $this->Token->deleteAll(
                [
                    'Tokens.user_id' => $user_id,
                    'Tokens.platform' => $platform
                ]
        );
    }

    /**
     * write log into debug.log file that is stored in LOG_DIR
     * /app/tmp/log/debug.log
     * @param string $str string to be logged
     */
    function log_debug($message) {
        if (DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $last = $backtrace[0];
            Log::write('debug', '[' . date("d-M-Y G:i:s") . '] ' . '[' . date("e") . '] ' . basename($last['file']) . " " . $last['line'] . " " . $message . "\n", 3, LOGS . "debug.log");
        }
    }

    function log_error($message) {
        if (DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $last = $backtrace[0];
            Log::write('error', '[' . date("d-M-Y G:i:s") . '] ' . '[' . date("e") . '] ' . basename($last['file']) . " " . $last['line'] . " " . $message . "\n", 3, LOGS . "error.log");
        }
    }

    private function _validToken() {
        $this->Token = TableRegistry::get('Tokens');
        if (isset($this->request->data['token'])) {
            $query = $this->Token->find('all', [
                'conditions' => ['Tokens.token' => $this->request->data['token']]
            ]);
            $data = $query->first();
            if ($data) {
                $this->_user_id = $data->id;
            }
        }
    }

    function uploadFiles($folder, $file, $itemId = null) {
        // setup dir names absolute and relative
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;

        // create the folder if it does not exist
        if (!is_dir($folder_url)) {
            mkdir($folder_url);
        }

        // if itemId is set create an item folder
        if ($itemId) {
            // set new absolute folder
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            // set new relative folder
            $rel_url = $folder . '/' . $itemId;
            // create directory
            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }
        }

        // list of permitted file types, this is only images but documents can be added
        $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png');

        // loop through and deal with the files
//        foreach ($formdata as $file) {
        // replace spaces with underscores
        $filename = str_replace(' ', '_', $file['name']);
        // assume filetype is false
        $typeOK = false;
        // check filetype is ok
        foreach ($permitted as $type) {
            if ($type == $file['type']) {
                $typeOK = true;
                break;
            }
        }

        // if file type ok upload the file
        if ($typeOK) {
            // switch based on error code
            switch ($file['error']) {
                case 0:
                    // check filename already exists
                    if (!file_exists($folder_url . '/' . $filename)) {
                        // create full filename
                        $full_url = $folder_url . '/' . $filename;
                        $url = $rel_url . '/' . $filename;
                        // upload the file
                        $success = move_uploaded_file($file['tmp_name'], $url);
                    } else {
                        // create unique filename and upload file
                        ini_set('date.timezone', 'Europe/London');
                        $now = date('Y-m-d-His');
                        $full_url = $folder_url . '/' . $now . $filename;
                        $url = $rel_url . '/' . $now . $filename;
                        $success = move_uploaded_file($file['tmp_name'], $url);
                    }
                    // if upload was successful
                    if ($success) {
                        // save the url of the file
                        $result['url'] = $url;
                    } else {
                        $result['errors'] = fprintf(__("Error uploaded %s. Please try again."), $filename);
                    }
                    break;
                case 3:
                    // an error occured
                    $result['errors'] = fprintf(__("Error uploading %s. Please try again."), $filename);
                    break;
                default:
                    // an error occured
                    $result['errors'] = fprintf(__("System error uploading %s. Contact webmaster."), $filename);
                    break;
            }
        } elseif ($file['error'] == 4) {
            // no file was selected for upload
            $result['nofiles'] = __("No file Selected");
        } else {
            // unacceptable file type
            $result['errors'] = fprintf(__("%s cannot be uploaded. Acceptable file types: gif, jpg, png."), $filename);
        }
//        }
        return $result;
    }

    function attachfile($folder, $file, $itemId = null) {
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;

        // create the folder if it does not exist
        if (!is_dir($folder_url)) {
            mkdir($folder_url);
        }

        // if itemId is set create an item folder
        if ($itemId) {
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            // set new relative folder
            $rel_url = $folder . '/' . $itemId;
            // create directory
            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }
        }

        // list of permitted file types, this is only images but documents can be added
        $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.ms-excel', 'application/pdf','text/plain');

        // loop through and deal with the files
//        foreach ($formdata as $file) {
        // replace spaces with underscores
        $filename = str_replace(' ', '_', $file['name']);
        // assume filetype is false
        $typeOK = false;
        // check filetype is ok
        foreach ($permitted as $type) {
            if ($type == $file['type']) {
                $typeOK = true;
                break;
            }
        }
        $sizeOK = false;
        if($file['size'] < 1000000){
            $sizeOK = true;
        }
        // echo $file['size'];

        // if file type ok upload the file
        if ($typeOK && $sizeOK) {
            // switch based on error code
            switch ($file['error']) {
                case 0:
                    // check filename already exists
                    if (!file_exists($folder_url . '/' . $filename)) {
                        // create full filename
                        $full_url = $folder_url . '/' . $filename;
                        $url = $rel_url . '/' . $filename;
                        // upload the file
                        $success = move_uploaded_file($file['tmp_name'], $url);
                    } else {
                        // create unique filename and upload file
                        ini_set('date.timezone', 'Europe/London');
                        $now = date('Y-m-d-His');
                        $full_url = $folder_url . '/' . $now . $filename;
                        $url = $rel_url . '/' . $now . $filename;
                        $success = move_uploaded_file($file['tmp_name'], $url);
                    }
                    // if upload was successful
                    if ($success) {
                        // save the url of the file
                        $result['url'] = $url;
                    } else {
                        $result['errors'] = printf(__("Error uploaded %s. Please try again."), $filename);
                        $result['url']='';
                    }
                    break;
                case 3:
                    // an error occured
                    $result['errors'] = printf(__("Error uploading %s. Please try again."), $filename);
                    $result['url']='';
                    break;
                default:
                    // an error occured
                    $result['errors'] = printf(__("System error uploading %s. Contact webmaster."), $filename);
                    $result['url']='';
                    break;
            }
        } elseif ($file['error'] == 4) {
            // no file was selected for upload
            $result['nofiles'] = ("No file Selected");
            $result['url']='';

        } elseif (!$sizeOK){
            $result['errors'] = "File nặng hơn 1MB";
            $result['url'] = '';
        }

         else {
            // unacceptable file type
            $result['errors'] = "Định dạng không được chấp nhận";
            $result['url']='';
        }
//        }
        return $result;
    }

    // Function: Get youtube id from link
    public function getYoutubeId($url) {
        $videoLink = $url;
        $ytArray = explode("/", $videoLink);
        $ytEndString = end($ytArray);
        $ytEndArray = explode("?v=", $ytEndString);
        $ytEndString = end($ytEndArray);
        $ytEndArray = explode("&", $ytEndString);
        $ytCode = $ytEndArray[0];
        return $ytCode;
    }

    // Function: Check URL is Youtube link
    public function isLinkYoutube($url = NULL) {
        if (preg_match("/(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&>]+)/", $url) || preg_match("/\b(?:youtu)\.be\b/i", $url)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Function: Check video ID exits
    public function checkVideoYoutubeIDExit($videoID = NULL) {
        $headers = get_headers('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v=' . $videoID);
        if (is_array($headers) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/', $headers[0]) : false) {
            // video exists
            return TRUE;
        } else {
            // video does not exist
            return FALSE;
        }
    }

    // Input: dateTime
    // Output: day ago or hour ago or minute
    public function getTimeAgo($datetimeInput = NULL) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $currentDateTime = date('Y-m-d H:i:s');
        $days = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60 * 24);
        if ($days >= 1) {
            return round($days) . __(' days ago');
        } else {
            $hours = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60);
            if ($hours >= 1) {
                return round($hours) . __(' hours ago');
            } else {
                $minutes = (strtotime($currentDateTime) - strtotime($datetimeInput)) / 60;
				if($minutes < 1) {
                return 0 . __(' minute ago');
				}
				else if($minutes < 2) {
                return 1 . __(' minute ago');
				}
				else
				{
					 return round($minutes) . __(' minutes ago');
				}
            }
        }
    }

    // Get string random by length
    public function generateRandomStringByLength($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // Input: dateTime
    // Output: day ago
    public function getEndDateDuration($startDate = NULL, $endDate = NULL) {
        $days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);
        if ($days >= 1) {
            return round($days);
        } else {
            return 0;
        }
    }

    public function getLongLatByLocationRadius($radius = 0) {
        $aRequestSession = $this->request->session();
        $dataLocation = $aRequestSession->read('Config.location');
        if (!empty($dataLocation)) {
            $minLat = 0;
            $maxLat = 0;
            $minLng = 0;
            $maxLng = 0;
            $tocken = TOKEN_GOOGLE_MAP;
            $location = implode(',', $dataLocation);
            $result = file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' . $location . '&radius=' . $radius . '&key=' . $tocken);
            $result = json_decode($result, true);

            if (isset($result['results']) && !empty($result['results'])) {
                foreach ($result['results'] as $row) {
                    $locationResult = (isset($row['geometry']['location']) ? $row['geometry']['location'] : array());
                    if ($locationResult) {
                        $lat = (isset($locationResult['lat']) ? $locationResult['lat'] : '');
                        $lng = (isset($locationResult['lat']) ? $locationResult['lat'] : '');
                        if ($minLat == '')
                            $minLat = $locationResult['lat'];
                        if ($maxLat == '')
                            $maxLat = $locationResult['lat'];
                        if ($minLng == '')
                            $minLng = $locationResult['lng'];
                        if ($maxLng == '')
                            $maxLng = $locationResult['lng'];
                        if ($lat != '') {
                            if ($lat < $minLat)
                                $minLat = $lat;
                            if ($lat > $maxLat)
                                $maxLat = $lat;
                        }

                        if ($lng != '') {
                            if ($lng < $minLng)
                                $minLng = $lng;
                            if ($lng > $maxLng)
                                $maxLng = $lng;
                        }
                    }
                }
            }
            if ($minLat != '' && $maxLat != '' && $minLng != '' && $maxLng != '') {
                $locationLatLng = array('minLat' => $minLat, 'maxLat' => $maxLat, 'minLng' => $minLng, 'maxLng' => $maxLng);
                return $locationLatLng;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    /**
     * get Content notification send mail
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getContentNotifySendMail($actionName = NULL, $type = 0, $optionData = NULL, $userActionId = 0, $role = '') {
        $sHtml = '';
        if ($actionName && $type) {
            $NameFirst = $actionName;
            $optionsArr = json_decode($optionData);

            // GET PROJECT INFO
            $ProjectName = '';
            if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                if (isset($project->title))
                    $ProjectName = $project->title;
            }

            // GET ROLES
            $RoleName = '';
            if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                $RolesTable = TableRegistry::get('Roles');
                $Role = $RolesTable->get($optionsArr->role_id);
                if (isset($Role->role) && !empty($Role->role)) {
                    $RoleName = $Role->role;
                }
            }
            $message = '';
            switch ($type) {
                case '1':
                    $linkAcceptAction = ROOT_URL . 'projectActions/acceptJoinRole?project_role_id=' . $optionsArr->project_role_id . '&user_action_id=' . $userActionId;
                    $linkDenyAction = ROOT_URL . 'projectActions/denyJoinRole?project_role_id=' . $optionsArr->project_role_id . '&user_action_id=' . $userActionId;
                    $message = __('<strong>%s</strong> muốn tham gia doanh nghiệp <strong>%s</strong> trong vai trò <strong>%s</strong><br/><p><a target="_blank" href="%s">Click vào đây</a> để chấp thuận.</p><p><a target="_blank" href="%s">Click vào đây</a> để từ chối.</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $role, $linkAcceptAction, $linkDenyAction);
                    break;
                case '2':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> đã bình luận về doanh nghiệp <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '3':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> applied for your project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '4':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has joined project <strong>%s</strong>  <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $role, $link);
                    break;
                case '5':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> accepted to add you in project <strong>%s</strong>  <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $role,  $link);
                    break;
                case '6':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> replied your comment on project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '7':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> just loved project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '8':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> just followed project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '9':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has uploaded a file on project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '10':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('Project owner <strong>%s</strong> has updated project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                //N has followed you
                case '12':
                    $message = __('<strong>%s</strong> has followed you');
                    $sHtml = sprintf($message, $NameFirst);
                    break;
                //You are offered to be <role> in the project Y
                case '13':
                    $message = __('You are offered to be <strong>%s</strong> in the project <strong>%s</strong>');
                    $sHtml = sprintf($message, $RoleName, $ProjectName);
                    break;
                //N has been offered <role> in  project Y
                case '14':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has been offered <strong>%s</strong> in project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $RoleName, $ProjectName, $link);
                    break;
                //N has been send chat message
                case '15':
                    $roomId = (isset($optionsArr->room_id)) ? $optionsArr->room_id : 0;
                    $link = ROOT_URL . 'chat-room/' . $project->id . '/' . $roomId;
                    $message = __('<strong>%s</strong> đã nhắn tin cho bạn trong doanh nghiệp <strong>%s</strong><br/><p><a target="_blank" href="%s">Click vào đây</a> để xem chi tiết tin nhắn</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                //B has created new project X
                case '16':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has created new project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst,$ProjectName, $link);
                    break;
                //B has invited you join a project
                case '17':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has invited you to join a project<br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $link);
                    break;
				case '18':
				$link = \Cake\Utility\Inflector::slug($project->title, '-');
				$link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
				$message = __('<strong>%s</strong> has update project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
				$sHtml = sprintf($message, $NameFirst,$ProjectName, $link);
				break;
                    $sHtml = __('Send notification content');
                    break;

				case '20':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('Project owner <strong>%s</strong> has added new <strong>%s</strong> of %s <br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $role, $ProjectName, $link);
                    break;


            }
        }
        return $sHtml;
    }

    // Coder: Giang Dien
    // Date: 2016-12-01
    // Function: Get data from API
    public function getDataFromAPI($apiLink = NULL, $options = [], $FullRespone = false) {
        $http = new Client();
        $options['language'] = $this->_lang;
        $response = $http->post($apiLink, http_build_query($options));
        if ($response->isOk()) {
            $dataRespone = json_decode($response->body());
            if ($FullRespone) {
                return $dataRespone;
            } else {
                return (isset($dataRespone->data) ? $dataRespone->data : []);
            }
        } else {
            return [];
        }
    }

    /**
     * set Notification
     * @author Roxannie Nguyen jr
     * @return json
     */
    public function setNotification($Project, $TypeId) {
        if ($Project) {
            $UsersProjectsTable = TableRegistry::get('UsersProjects');
			//$this->sendNotificationForFollowProject($Project->id, 9);
            $UserProjects = $UsersProjectsTable->find()->where(['project_id' => $Project->id, 'status' => 1, 'type' => 2])->contain(['Roles'])->group(['user_id'])->toArray();
            if ($UserProjects) {
                $User = $this->Auth->user();
                foreach ($UserProjects as $key => $UserProject) {
                    $UserNotifySettingTable = TableRegistry::get('UserNotificationSetting');
                    $UserNotifySetting = $UserNotifySettingTable->find('all', ['conditions' => ['user_id' => $UserProject->user_id]])->first();

                    $OptionData = json_encode(['project_id' => $Project->id]);

                    $UserTable = TableRegistry::get('Users');
                    $UserJoinProject = $UserTable->find('all', ['conditions' => ['id' => $UserProject->user_id]])->first();

                    if ($UserNotifySetting && $UserNotifySetting->member_join_project_email_status == 1) {
                        // User setting receil notification via email
                        $template = 'notification';
                        $from = [EMAIL_LOGIN => __('We The Projects')];
                        $subject = __('[SME] Notification on SME');
                        // GET CONTENT MAIL
                        $contentMail = __('Send notification content');
                        if (isset($TypeId) && !empty($TypeId)) {
                            $name = $User['first_name'] . ' ' . $User['last_name'];
                            $contentMail = $this->getContentNotifySendMail($name, $TypeId, $OptionData);
                        }
                        // END: GET CONTENT MAIL
                        if (!$this->sendMail($template, $from, $UserJoinProject['email'], ['first_name' => $UserJoinProject['first_name'], 'last_name' => $UserJoinProject['last_name'], 'notification_content' => $contentMail], $subject)) {
                            $this->Flash->error(__('Send mail to creator fail.'));
                        }
                    }

                    if ($UserNotifySetting && $UserNotifySetting->member_join_project_mobile_status == 1) {
                        $NotificationsTable = TableRegistry::get('Notifications');
                        $NotificationsData = $NotificationsTable->newEntity();
                        $NotificationsData->user_id = $UserProject->user_id;
                        $NotificationsData->action_user_id = $User['id'];
                        $NotificationsData->project_id = $Project->id;
                        $NotificationsData->option_data = $OptionData;
                        $NotificationsData->action_type = $TypeId;
                        $NotificationsData->read_flg = 0;
                        $NotificationsData->created = date('Y-m-d H:i:s');
                        if (!$NotificationsTable->save($NotificationsData)) {
                            $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                        } else {
                            $this->PushNoti($TypeId, ['project_id' => $Project->id] ,$UserProject->user_id,$User['id'] );
                        }
                    }
                }
                // N has uploaded a file
            }
        }
        return false;
    }

    // Coder: Giang Dien
    // Date: 2017-02-09
    // Function: Send notification for user follow
    public function sendNotificationForUserFollow($actionType = 0, $optionData = [], $actionUserId = 0,$role = '') {
        $notificationTable = TableRegistry::get('Notifications');
        $followUserTable = TableRegistry::get('Followings');
		$this->loadModel('UserProjectActions');
		$listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' =>$optionData['project_id'], 'status' => 1, 'action_type' => 2]);
        $followerList = $followUserTable->getFollowUsers(['following_id' => $actionUserId, 'connection' => 2]);
		$userid  = array();
	    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
		$this->loadModel('Projects');
		$currentProject = $this->Projects->find('all', ['conditions' => ['Projects.id' => $optionData['project_id']]])->contain('Users')->first();

		foreach ($followerList as $row) {
            $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row['user_id']]])->first();
            $checkFieldList = $this->getFieldCheckNotificationSettingByActionType($actionType);
            $checkField = $checkFieldList['mobile'];
            $userid[] = $row->id;
			if (!empty($userNotificationSetting) && $userNotificationSetting->$checkField == 1 && $row['user_id'] != $currentProject->user_id) {
                $notificationData = $notificationTable->newEntity();
                $notificationData->user_id = $row['user_id'];
                $notificationData->action_user_id = $actionUserId;
                $notificationData->project_id = intval($optionData['project_id']);
                $notificationData->option_data = json_encode($optionData);
                $notificationData->action_type = $actionType;
                $notificationData->created = date('Y-m-d H:i:s');
                $notificationTable->save($notificationData);
				$this->PushNoti($actionType, ['project_id' => $optionData['project_id']] ,$row['user_id'],$actionUserId,$role);
            }
        }
        $cronjobsTable = TableRegistry::get('Cronjobs');
        $cronjobData = $cronjobsTable->newEntity();
        $cronjobData->cronjob_name = 'send_notification_for_user_follow';
        $cronjobData->option_data = json_encode($optionData);
        $cronjobData->created = date('Y-m-d H:i:s');
        $cronjobsTable->save($cronjobData);
        return TRUE;
    }

    public function getFieldCheckNotificationSettingByActionType($type = 1) {
        $field = ['email' => 'member_join_project_email_status', 'mobile' => 'member_join_project_mobile_status'];
        switch ($type) {
            case 1:
                $field = ['email' => 'member_join_project_email_status', 'mobile' => 'member_join_project_mobile_status'];
                break;
            case 2:
                $field = ['email' => 'replies_comment_email_status', 'mobile' => 'replies_comment_mobile_status'];
                break;
            case 3:
            case 4:
            case 5:
            case 11:
                $field = ['email' => 'member_join_project_email_status', 'mobile' => 'member_join_project_mobile_status'];
                break;
            case 6:
                $field = ['email' => 'replies_comment_email_status', 'mobile' => 'replies_comment_mobile_status'];
                break;
            case 7:
            case 8:
            case 9:
            case 10:
            case 13:
            case 14:
            case 16:
                $field = ['email' => 'project_update_email_status', 'mobile' => 'project_update_mobile_status'];
                break;
            case 12:
                $field = ['email' => 'new_follower_email_status', 'mobile' => 'new_follower_mobile_status'];
                break;
            case 15:
                $field = ['email' => 'private_message_email_status', 'mobile' => 'private_message_mobile_status'];
                break;
        }
        return $field;
    }
	// follow project
	 public function sendNotificationForFollowProject($projectId, $actionType,$comment_id='') {
	   if (!empty($projectId)) {
            $userAction = $this->Auth->user();
            $this->loadModel('UserProjectActions');
		    $this->loadModel('UsersProjects');
			$listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $projectId, 'status' => 1, 'action_type' => 2]);
			$listCollaborators = $this->UsersProjects->getUserProjectsJoin(['project_id' => $projectId, 'status' => 1, 'type' => 2]);
			$userid = array();
			if (count($listUserfollows) > 0) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                foreach ($listUserfollows as $Userfollow) {
                    if ($Userfollow->user_id != $userAction['id'] && !in_array($Userfollow->user_id, $userid)) {
						$userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $Userfollow->user_id]])->first();
					    $optionData = json_encode(['project_id' => $Userfollow->project_id,'comment_id' =>$comment_id]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $Userfollow->user_id;
                            $notificationData->action_user_id = $userAction['id'];
                            $notificationData->project_id = $Userfollow->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType, ['project_id' => $Userfollow->project_id] , $Userfollow->user_id,$userAction['id']);
                        }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $Userfollow->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
                }
                $responeData = ['status' => 1, 'message' => __('Success')];
                echo json_encode($responeData);
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
	}
	//push follow
	    public function PushNotificationForUserFollow($actionType = 0, $optionData = [], $actionUserId = 0) {
        $notificationTable = TableRegistry::get('Notifications');
        $followUserTable = TableRegistry::get('Followings');
		$this->loadModel('UserProjectActions');
		$listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' =>$optionData['project_id'], 'status' => 1, 'action_type' => 2]);
        $followerList = $followUserTable->getFollowUsers(['following_id' => $actionUserId, 'connection' => 2]);
		$userid  = array();
	    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
		$this->loadModel('Projects');
		$currentProject = $this->Projects->find('all', ['conditions' => ['Projects.id' => $optionData['project_id']]])->contain('Users')->first();

		foreach ($followerList as $row) {
            $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row['user_id']]])->first();
            $checkFieldList = $this->getFieldCheckNotificationSettingByActionType($actionType);
            $checkField = $checkFieldList['mobile'];
            $userid[] = $row->id;
			if (!empty($userNotificationSetting) && $userNotificationSetting->$checkField == 1 && $row['user_id'] != $currentProject->user_id) {
				$this->PushNoti($actionType, ['project_id' => $optionData['project_id']] ,$row['user_id'],$actionUserId);
            }
        }
        return TRUE;
    }
	//end push

	// NAM

	 public function sendNotificationForJoinProject($projectId, $actionType, $role_id) {
	   if (!empty($projectId)) {
			$role = '';
			if(isset($role_id) && !empty($role_id))
			{
			  $role_id = $role_id;
			  $this->loadModel('Roles');
			  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
				foreach($roles as $rol)
				{
					$role = $rol->role;
				}
			}
            $userAction = $this->Auth->user();
			$this->sendNotificationForUserFollow($actionType, ['project_id' => $projectId], $userAction['id'],$role);
            $this->loadModel('UserProjectActions');
		    $this->loadModel('UsersProjects');
			$listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $projectId, 'status' => 1, 'action_type' => 2]);
			$listCollaborators = $this->UsersProjects->getUserProjectsJoin(['project_id' => $projectId, 'status' => 1, 'type' => 2]);
			$userid = array();
			foreach($listCollaborators as $Collaborator)
			{
				$userid[] = $Collaborator->user_id;
			}
			if (count($listCollaborators) > 0) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                foreach ($listCollaborators as $collaborator) {
                    if ($collaborator->user_id != $userAction['id']) {
                        $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $collaborator->user_id]])->first();
                        $optionData = json_encode(['project_id' => $collaborator->project_id, 'role_id'=>$role_id]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $collaborator->user_id;
                            $notificationData->action_user_id = $userAction['id'];
                            $notificationData->project_id = $collaborator->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType, ['project_id' => $collaborator->project_id, 'role_id'=>$role_id] , $collaborator->user_id,$userAction['id'],$role);

                        }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $collaborator->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
                }
            }
			if (count($listUserfollows) > 0) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                foreach ($listUserfollows as $Userfollow) {
                    if ($Userfollow->user_id != $userAction['id'] && !in_array($Userfollow->user_id, $userid)) {
						$userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $Userfollow->user_id]])->first();
					    $optionData = json_encode(['project_id' => $Userfollow->project_id, 'role_id'=>$role_id]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $Userfollow->user_id;
                            $notificationData->action_user_id = $userAction['id'];
                            $notificationData->project_id = $Userfollow->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType, ['project_id' => $Userfollow->project_id, 'role_id'=>$role_id] , $Userfollow->user_id,$userAction['id'],$role);
                        }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $Userfollow->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
                }
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
	}
	public function PushNoti($actionType = 0, $optionData = [], $actionUserId = 0, $userid ,  $role = '')
	{
			$actionUserId = $actionUserId;
			$listNotifications = TableRegistry::get('Notifications');
			$listNotifications = $listNotifications->find('all', ['conditions' => ['user_id' => $actionUserId, 'read_flg' => 0]])->toArray();
			$number = count($listNotifications);
			$userTable = TableRegistry::get('Users');
			$userAction = $userTable->find('all', ['conditions' => ['id' => $userid]])->first();
		    $tokenTable = TableRegistry::get('Tokens');
			$userInfo  = $tokenTable->find('all', [
					'conditions' => ['user_id' => $actionUserId, 'platform' => 0],
				])->first();
			$message = $this->getTypeNotificationContentPush($userAction['first_name'] . ' ' . $userAction['last_name'], $actionType, $optionData,$role);
			if ($userInfo['token_device'] != '') {
			   $this->PushNotification->send($userInfo['token_device'], $message,$number);
			}

	}
	// Nam
	public function getTypeNotificationContentPush($actionName = null, $type = 1, $option_data = null, $role = '')
    {
        $Languages = CoreLib::getLanguage();
	$LanguageCurrent = CoreLib::getLanguage(false);
        $result = '';
		if($LanguageCurrent['name'] === '日本語')
				{
        switch ($type) {
		case 1:
                // Request want to join project
                $message = __('ユーザー %s さん は %s として プロジェクト %s に 参加を希望しています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName,$role, $project->title);
                break;
            case 2:
                // alert comment on project
                $message = __('%s は プロジェクト %s に コメントしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s has applied for your project');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $message = __('%s さん  が プロジェクト %s に %s として 参加しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title, $role);
                break;

            case 5:
                // alert accept join project
                $message = __('プロジェクト管理者 %s さん が、 あなた を さん が、 あなた を %s として プロジェクト %s に 参加することを認証しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName,$role,$project->title);
                break;
            case 6:
                // alert reply comment
                $message = __('%s さん が、プロジェクト %s の コメント に 返信しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $message = __('%s さん が、プロジェクト %s に イイネ！しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert follow project
                $message = __('%s さん は、プロジェクト %s を フォローしています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $message = __('%s さん が プロジェクト %s に ファイル を アップロードしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $message = __('プロジェク管理者 %s さん が プロジェクト %s を 更新しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $message = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 12: // N has followed you
                $message = __('%s has followed you');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('あなたは、プロジェクト %s  の %s に なるように提案されています。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$result = sprintf($message, $project->title, $role);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s さん は、プロジェクト %s  の %s に なるように提案されています');
                $projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s さん が、 プロジェクト %s の チャットで 新しいメッセージ を あなた に 送信しました。');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s さん が 新しいプロジェクト %s を 作成しました');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $message = __('%s  さん が プロジェクト %s に 参加するよう に あなた を 招待しました');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 18: // B has update project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('プロジェク管理者 %s さん が プロジェクト %s を 更新しました。');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 19: // B has asset project X
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$message = __('%s has asset project %s');
				$result = sprintf($message, $actionName, $project->title);
			break;
			case 20: // B has add
				$message = __('プロジェクト管理者 %s さん が、プロジェクト %s の 新しい %s を 追加しました。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$result = sprintf($message, $actionName, $project->title, $role);
				break;
				}
		}
			else if($LanguageCurrent['name'] === 'English')
				{
		switch ($type) {
            case 1:
                // Request want to join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s muốn tham gia doanh nghiệp %s với vai trò %s');
                $result = sprintf($message, $actionName, $project->title,$role);
                break;
            case 2:
                // alert comment on project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã bình luận về doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s đã tham gia doanh nghiệp.');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã tham gia %s với vai trò %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 5:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('Chủ doanh nghiệp %s đã đồng ý thêm bạn %s với vai tro %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 6:
                // alert reply comment
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã phản hồi bình luận của bạn trong doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã thích doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã theo dõi doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $projectTable = TableRegistry::get('Projects');
                $message = __('%s has uploaded a file on project %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $projectTable = TableRegistry::get('Projects');
                $message = __('Project owner %s has updated project %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $message = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('%s đã theo dõi bạn');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('You are offered to be %s in the project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($option_data['project_id']) && !empty($option_data['project_id'])) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($option_data['project_id']);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $role, $ProjectName);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s has been offered %s in project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($option_data['project_id']) && !empty($option_data['project_id'])) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($option_data['project_id']);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $actionName, $role, $ProjectName);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã nhắn tin cho bạn trong doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s has created new project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s has invited you join a project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 18: // Update project
                $projectUpdateTable = TableRegistry::get('ProjectUpdates');
                $projectUpdate = $projectUpdateTable->get($option_data['project_id']);
                $message = __('%s has update project %s');
                $result = sprintf($message, $actionName, $projectUpdate->title);
                break;
            case 19: // B has asset project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has asset project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 20: // B has add
				$message = __('Project owner %s has added new %s of %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
            case 21: // new request 
                
                $message = __('Chủ Doanh nghiệp %s đã gửi yêu cầu mới %s từ %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
            case 22: // duyet  
                $message = __('Admin %s đã duyệt yêu cầu %s của %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
            case 23: //tu choi  
                $message = __('Admin %s đã từ chối y/c %s của %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
            case 24: //gui tap ca  
                $message = __('Chủ DN %s đã gửi y/c %s %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
            case 25: //gui vai trò  
                $message = __('Chủ DN %s đã gửi y/c %s đến %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);  
                $result = sprintf($message, $actionName, $role, $project->title);
                break;                      
        }
		}
		else
		{
		switch ($type) {
            case 1:
                // Request want to join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('El usuario %s quiere unirse al proyecto %s como %s');
                $result = sprintf($message, $actionName, $project->title,$role);
                break;
            case 2:
                // alert comment on project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s ha comentado el proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s ha aplicado para su proyecto');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s ha participado en el proyecto %s como %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 5:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('El propietario del proyecto %s te ha aceptado agregar te en el proyecto %s como %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 6:
                // alert reply comment
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s ha respondido a su comentario sobre el proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s le gusto el proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s hay seguido el proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $projectTable = TableRegistry::get('Projects');
                $message = __('%s ha subido un archivo en el proyecto %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $projectTable = TableRegistry::get('Projects');
                $message = __('Propietario de proyecto %s ha actualizado proyecto %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $message = __('Propietario de proyecto %s has denied to add you in project %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('%s te ha seguido');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('Se le ofrece a ser %s en el proyecto %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($option_data['project_id']) && !empty($option_data['project_id'])) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($option_data['project_id']);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $role, $ProjectName);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s se ha ofrecido para ser %s en el proyecto %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($option_data['project_id']) && !empty($option_data['project_id'])) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($option_data['project_id']);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $actionName, $role, $ProjectName);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s te ha enviado un mensaje en el chat del proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s le gusto el proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s te ha invitado a unirte al proyecto %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 18: // Update project
                $projectUpdateTable = TableRegistry::get('ProjectUpdates');
                $projectUpdate = $projectUpdateTable->get($option_data['project_id']);
                $message = __('%s ha actualizado proyecto %s');
                $result = sprintf($message, $actionName, $projectUpdate->title);
                break;
            case 19: // B has asset project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has asset project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 20: // B has add
				$message = __('El propietario del proyecto %s ha agregado un nuevo %s de %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
        }
		}
        return $result;
    }


	//End Nam

    public function isAuthorized($user)
    {
        $this->loadModel('Users');
        $userId = $this->Auth->user('id');
        if(!empty($userId)){
            $user = $this->Users->find('all', [
                    'conditions' => ['id' => $userId],
                    'fields' => ['id', 'email', 'activated']
                ])->first();
            if(!empty($user)){
                $activated = $user->activated;

                if($activated !== 1){
                    $this->Flash->error(__('Your account is not yet activated'));
                    return $this->redirect('/users/activate');
                    // return false;
                }
                else return true;
            }
        }
        return false;
    }

}
