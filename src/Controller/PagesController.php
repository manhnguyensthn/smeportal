<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\I18n;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(array('about', 'display', 'faq', 'privacypolicy', 'Support', 'TermsOfUser', 'WhatIsWeTheProject','guide'));
        $this->viewBuilder()->helpers(['Url']);
    }

    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function about(){
        $this->set('current_lang',I18n::locale());
        $this->set("title", __('AboutUs - We the projects'));
        $AboutUsTable = TableRegistry::get('AboutUs');
        $aboutus = $AboutUsTable->find('all')->first();
        $this->set(compact('aboutus'));
    }

    public function faq(){
        $this->set('current_lang',I18n::locale());
        $this->set("title", __('Faqs - We the projects'));
        $QuestionsTable = TableRegistry::get('Questions');
        $faq = $QuestionsTable->find('all',['conditions'=>['title'=>'faq']])->first();
        $this->set(compact('faq'));

    }

    public function guide(){
        $this->set('current_lang',I18n::locale());
        $this->set("title", __('Hướng dẫn sử dụng'));
        $QuestionsTable = TableRegistry::get('Questions');
        $guide = $QuestionsTable->find('all',['conditions'=>['title'=>'guide']])->first();
        $this->set(compact('guide'));

    }


    public function privacypolicy(){
        $this->set("title", __('Privacy Policy - We the projects'));
    }

    public function Support(){
        $this->set('current_lang',I18n::locale());
        $this->set("title", __('Support and Contact - We the projects'));
    }

    public function TermsOfUser(){
        $this->set("title", __('Terms Of User - We the projects'));
        $TermOfUseTable = TableRegistry::get('TermOfUse');
        $termofuse = $TermOfUseTable->find('all')->first();
        $this->set(compact('termofuse'));
    }

    public function WhatIsWeTheProject(){
        $this->set('current_lang',I18n::locale());
        $this->set("title", __('What is We The Project - We the projects'));
    }
}
