<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Lib\CoreLib;

/**
 * Countries Controller
 *
 * @property \App\Model\Table\CountriesTable $Countries
 */
class CoreController extends AppController
{
	public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow("setLanguageAjax");
    }

    /**
     * set Language Ajax
     * @param {LangParam}
     * @author Roxannie Nguyen jr
     * @return json
     */
    public function setLanguageAjax(){

        $this->_status = 0;
        $RequestData   = $this->request->data;
        $aLanguages    = CoreLib::getDataLanguage();;

        if(!isset($RequestData['LangParam']) || empty($RequestData['LangParam'])){
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        }else{
            $LangParam = $RequestData['LangParam'];
        }

        if($this->_status == 0){
        	if(!array_key_exists($LangParam, $aLanguages)){
        		$this->_status = 1;
                $this->_message = __('Unable to process your request.');
        	}
        }

        if($this->_status == 0){
        	if(!$this->setLanguage($LangParam)){
        		$this->_status = 1;
                $this->_message = __('Unable to process your request.');
        	}
        }

        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }
}