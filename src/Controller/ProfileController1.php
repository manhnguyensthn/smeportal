<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Query;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;

//use App\Lib\phpFlickr;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UsersTable $Projects
 */
class ProfileController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['getWorkPortfolioAjax', 'deleteUserWork']);
        $this->loadModel('Users');
    }

    /**
     * Edit profile method
     *
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit() {
		
        $this->set("title", __('Profile - We the projects'));
        $this->_status = 1;
        $this->normalizedJsonRequestData();

        if (!isset($this->request->data['Users']['avatar'])) {
            $this->request->data['Users']['avatar'] = [];
        }

        $userSaved = false;
        $user_id = null;
        $tokenTable = TableRegistry::get('Tokens');
        $token = '';
        if ($this->request->is('json')) {
            if ($this->request->is('get')) {
                $token = $this->request->query['token'];
            } else {
                $token = $this->request->data['token'];
            }
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $token],
            ]);
            $token = $token->first();
        }

        if (!$this->request->is('json')) {
            $user_id = $this->Auth->user('id');
        } else {
            $user_id = empty($token) ? null : $token->user_id;
        }

        if (!$user_id) {
            if (!$this->request->is('json')) {
                return $this->redirect(['action' => 'login']);
            } else {
                $this->_message = __('Unable to authorize your request.');
                $this->_data = [];
            }
            $this->_status = 0;
        }

        $user = null;
        if ($this->_status) {
            $user = $this->Users->get($user_id, [
                'contain' => ['Projects', 'Roles']
            ]);
            if (empty($user)) {
                if (!$this->request->is('json')) {
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->_message = __('Unable to process your request. User not found.');
                    $this->_data = [];
                }
                $this->_status = 0;
            }
        }

        $usersRoles = TableRegistry::get('UsersRoles');
        $usersWebsites = TableRegistry::get('UsersWebsites');
        $usersWorks = TableRegistry::get('UsersWorks');
        $usersInterests = TableRegistry::get('UsersInterests');

        $role1 = $usersRoles->find('all', ['conditions' => ['user_id' => $user_id, 'position' => 1]])->first();
        $role2 = $usersRoles->find('all', ['conditions' => ['user_id' => $user_id, 'position' => 2]])->first();
        if (empty($role1)) {
            $role1 = $usersRoles->newEntity();
            $role1->role_id = 0;
            $role1->user_id = 0;
            $role1->position = 0;
        }
        if (empty($role2)) {
            $role2 = $usersRoles->newEntity();
            $role2->role_id = 0;
            $role2->user_id = 0;
            $role2->position = 0;
        }

        $websites = $usersWebsites->find('all', ['fields' => ['website_id', 'Websites.name'], 'contain' => 'Websites', 'conditions' => ['user_id' => $user_id]]);
        $interests = $usersInterests->find('all', ['fields' => ['interest_id', 'Interests.name'], 'contain' => 'Interests', 'conditions' => ['user_id' => $user_id]]);

        $currentAvatarPath = $this->getAvatarFilePathByURL($user ? $user->avatar : '');
        $currentAvatarURL = $user ? $user->avatar : '';
        $works = $usersWorks->find('all', ['fields' => ['id', 'work_url', 'work_title', 'work_video_id'], 'conditions' => ['user_id' => $user_id]]);
        $hasWork = false;
        $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
        $avatarURL = join('/', [$webRootURL, 'img', 'avatars', 'default.png']);
        if ($this->request->is(['patch', 'post', 'put']) && $this->_status) {

            $firstName = trim($this->request->data['Users']['first_name']);
            $lastName = trim($this->request->data['Users']['last_name']);
            $biography = trim($this->request->data['Users']['biography']);
            $avatar = $this->request->data['Users']['avatar'];
            $external_avatar_url = trim($this->request->data['Users']['external_avatar_url']);
			$url_avatar = trim($this->request->data['Users']['users_avatar_url']);
			if(!empty($url_avatar))
			{	
			$avatar = '';	
			list($type, $url_avatar) = explode(';',$url_avatar);
			list(, $url_avatar)      = explode(',',$url_avatar);
			$url_avatar = base64_decode($url_avatar);
			$imageName = $_SERVER['DOCUMENT_ROOT'].'/webroot/img/'.time().'.png';
			file_put_contents($imageName, $url_avatar);
			$this->loadModel('Resize');
			$this->Resize->load($imageName);
			// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
			$this->Resize -> resizeImage(1080, 1080, 'crop');
			$name = '/img/avatars/'.time().'.jpg';
			$imageName_new = $_SERVER['DOCUMENT_ROOT'].'/webroot/'.$name;
			// *** 3) Save image
			$this->Resize-> saveImage($imageName_new , 1000);
			unlink($imageName);	
			unlink($_SERVER['DOCUMENT_ROOT'].'/webroot/'.$currentAvatarURL);	
			}
            $about_me = '';
            if ($this->request->is('json')) {
                $about_me = trim($this->request->data['Users']['about_me']);
            }
            $fileType = '';
            if ((!empty($avatar) && !empty($avatar['tmp_name']) && !empty($avatar['name']) || !empty($external_avatar_url)) && $this->_status) {
                $fileType = empty($external_avatar_url) ?
                        $this->getFileTypeByMimeType($avatar['type']) :
                        pathinfo(basename($external_avatar_url), PATHINFO_EXTENSION);

                $allowFileTypes = $this->getAllowedAvatarUploadedFileTypes();
                $avatarOK = true;

                if (!in_array($fileType, $allowFileTypes)) {
                    $avatarOK = false;
                }

                if (!empty($external_avatar_url)) {
                    $tmpDir = ini_get('upload_tmp_dir');
                    $tmpFullPath = tempnam($tmpDir, "tmp");

                    if ($this->saveRemoteImage($external_avatar_url, $tmpFullPath)) {
                        $this->request->data['Users']['avatar'] = [];
                        $avatar = $this->request->data['Users']['avatar'];
                        $avatar['tmp_name'] = $tmpFullPath;
                        $avatar['name'] = basename($external_avatar_url);
                    } else {
                        $avatarOK = false;
                    }
                }

                if ($avatarOK) {
                    list($width, $height) = getimagesize($avatar['tmp_name']);
                    $minimumDimension = $this->getMinimumAvatarDimension();
                    if (empty($width) || empty($height)) {
                        $avatarOK = false;
                    } else {
                        $avatarOK = ($width >= $minimumDimension['width']) && ($height >= $minimumDimension['height']);
                    }
                }

                if (!$avatarOK) {
                    $this->Flash->error(__('Profile image should be: jpeg, png, gif, bmp with minimum @1024x1024.'));
                    return $this->redirect($this->referer());
                    exit;
                }
            }
// END VALIDATE avatar
// VALIDATE website
            if ($this->_status && $this->request->is('json')) {
                $otherWebsites = [];
                $data = $this->request->data;
                if (isset($data['UsersWebsite'])) {
                    $otherWebsites = $data['UsersWebsite'];
                }

                if (!empty($otherWebsites)) {
                    foreach ($otherWebsites as $key => $value) {
                        $trimmedValue = trim($value);
                        if (!empty($trimmedValue)) {
                            if (!$this->isUrlValid($trimmedValue)) {
                                $this->_message = __('Định dạng website sai. Ví dụ: http://www.sme.com, https://www.amazon.co.jp');
                                $this->_data = [];
                                $this->_status = 0;
                                break;
                            }
                        }
                    }
                }
            }
// END VALIDATE website
// VALIDATE interest
            if ($this->_status && $this->request->is('json')) {
                $otherInterests = [];
                $data = $this->request->data;
                if (isset($data['UsersInterest'])) {
                    $otherInterests = $data['UsersInterest'];
                }

                if (!empty($otherInterests)) {
                    foreach ($otherInterests as $key => $value) {
                        $trimmedValue = trim($value);
                        if (!empty($trimmedValue)) {
                            if ($trimmedValue != strip_tags($trimmedValue)) {
                                $this->_message = __('Invalid interest format');
                                $this->_data = [];
                                $this->_status = 0;
                                break;
                            }
                        }
                    }
                }
            }
// END VALIDATE interest
            // Add list portfolios
            if (isset($this->request->data['Portfolio']) && !empty($this->request->data['Portfolio'])) {
                $userWorks = $this->request->data['Portfolio'];
                $this->loadModel('UsersWorks');
                $errorString = '';
                foreach ($userWorks['video'] as $index => $video) {
                    if ($this->isLinkYoutube($userWorks['work_video_url'][$index])) {
                        $respone = $this->UsersWorks->addWork(['user_id' => $user_id, 'work_title' => $userWorks['work_title'][$index], 'work_description' => $userWorks['work_description'][$index], 'work_url' => $userWorks['work_video_url'][$index], 'work_video_id' => $userWorks['work_video_id'][$index]]);
                        if ($respone != 'TRUE') {
                            $errorString .= $userWorks['work_video_url'][$index] . '--- add error<br/>';
                        }
                    } else {
                        $errorString .= $video . '--- add error<br/>';
                    }
                }
                if (!empty($errorString)) {
                    $this->Flash->error($errorString);
                }
            }
            $this->request->data['Users']['first_name'] = $firstName;
            $this->request->data['Users']['last_name'] = $lastName;
            $this->request->data['Users']['name'] = $firstName . ' ' . $lastName;
            $this->request->data['Users']['biography'] = $biography;
            $this->request->data['Users']['country_id'] = $this->request->data['Countries'];
            $this->request->data['Users']['district_id'] = $this->request->data['Districts'];
            if (empty($this->request->data['Countries'])) {
                $this->request->data['Users']['country_id'] = 0;
            }
            if (empty($this->request->data['Districts'])) {
                $this->request->data['Users']['district_id'] = 0;
            }
            if (empty($this->request->data['States'])) {
                $this->request->data['Users']['state_id'] = 0;
            } else if ($this->request->data['States'] == -1) {
                $this->request->data['Users']['state_id'] = null;
            } else {
                $this->request->data['Users']['state_id'] = $this->request->data['States'];
            }

            if ($this->request->is('json')) {
                $this->request->data['Users']['about_me'] = $about_me;
            }

            $user = $this->Users->patchEntity($user, $this->request->data);

            $role1_id = $this->request->data['Roles']['role0'];
            $role2_id = $this->request->data['Roles']['role1'];

            $role1_action = UsersController::UsersRolesDoUnspecified;
            $role2_action = UsersController::UsersRolesDoUnspecified;

// Determine first role action
            if (!empty($role1_id)) {
                if ($role1->isNew()) {
                    $role1->role_id = intval($role1_id);
                    $role1->user_id = $user_id;
                    $role1->position = 1;
                    $role1_action = UsersController::UsersRolesDoInsert;
                } else {
// $role1->role_id = intval($role1_id);
                    $role1_action = UsersController::UsersRolesDoUpdate;
                }
            } else {
                if (!$role1->isNew()) {
                    $role1_action = UsersController::UsersRolesDoDelete;
                }
            }

// Determine second role action
            if (!empty($role2_id)) {
                if ($role2->isNew()) {
                    $role2->role_id = intval($role2_id);
                    $role2->user_id = $user_id;
                    $role2->position = 2;
                    $role2_action = UsersController::UsersRolesDoInsert;
                } else {
// $role2->role_id = intval($role2_id);
                    $role2_action = UsersController::UsersRolesDoUpdate;
                }
            } else {
                if (!$role2->isNew()) {
                    $role2_action = UsersController::UsersRolesDoDelete;
                }
            }

// BEGIN UPDATE FIRST ROLE
            if ($this->_status) {
                switch ($role1_action) {
                    case UsersController::UsersRolesDoInsert:
                        $this->_status = $usersRoles->save($role1) ? 1 : 0;
                        break;
                    case UsersController::UsersRolesDoUpdate:
                        $usersRoles->updateAll(['role_id' => intval($role1_id)], ['user_id' => $user_id, 'position' => 1]);
                        break;
                    case UsersController::UsersRolesDoDelete:
                        $this->_status = $usersRoles->delete($role1) ? 1 : 0;
                        break;
                    default:
                        break;
                }

                if (!$this->_status) {
                    if (!$this->request->is('json')) {
                        $this->Flash->error(__('The profile could not be saved. Please, try again.'));
                    } else {
                        $this->_message = __('The profile could not be saved. Please, try again.');
                        $this->_data = [];
                    }
                    $this->_status = 0;
                }
            }
// END UPDATE FIRST ROLE
// BEGIN UPDATE SECOND ROLE
            if ($this->_status) {
                switch ($role2_action) {
                    case UsersController::UsersRolesDoInsert:
                        $this->_status = $usersRoles->save($role2) ? 1 : 0;
                        break;
                    case UsersController::UsersRolesDoUpdate:
                        $usersRoles->updateAll(['role_id' => intval($role2_id)], ['user_id' => $user_id, 'position' => 2]);
                        break;
                    case UsersController::UsersRolesDoDelete:
                        $this->_status = $usersRoles->delete($role2) ? 1 : 0;
                        break;
                    default:
                        break;
                }

                if (!$this->_status) {
                    if (!$this->request->is('json')) {
                        $this->Flash->error(__('The profile could not be saved. Please, try again.'));
                    } else {
                        $this->_message = __('The profile could not be saved. Please, try again.');
                        $this->_data = [];
                    }
                    $this->_status = 0;
                }
            }
// END UPDATE SECOND ROLE
// UPDATE WEB SITE
            if ($this->_status) {
                $this->_status = $this->updateWebsite($user_id) ? 1 : 0;
                if (!$this->_status) {
                    if (!$this->request->is('json')) {
                        $this->Flash->error(__('The profile could not be saved. Please, try again.'));
                    } else {
                        $this->_message = __('The profile could not be saved. Please, try again.');
                        $this->_data = [];
                    }
                    $this->_status = 0;
                }
            }
// END WEB SITE
// UPDATE INTEREST
            if ($this->_status && $this->request->isJson()) {
                $this->_status = $this->updateInterest($user_id) ? 1 : 0;
                if (!$this->_status) {
                    $this->_message = __('The profile could not be saved. Please, try again.');
                    $this->_data = [];
                }
            }
// END INTEREST
// BEGIN UPDATE USER
            if ($this->_status) {
                $uniqueAvatarPath = '';
                if (!empty($external_avatar_url)) {
                    $uniqueAvatarPath = $this->getUniqueAvatarFilePath($avatar['name'], $fileType, $user_id);
                    $user->avatar = $external_avatar_url;
                } else {
                    if (!empty($avatar) && !empty($avatar['tmp_name']) && !empty($avatar['name'])) {
                        $uniqueAvatarPath = $this->getUniqueAvatarFilePath($avatar['name'], $fileType, $user_id);
                        $user->avatar = $this->getAvatarURLByFilePath($uniqueAvatarPath);
                        if ((!empty($avatar) && !empty($avatar['tmp_name']) && !empty($avatar['name'])) || !empty($external_avatar_url)) {
                            $this->deleteOldAvatar($currentAvatarPath);
                            $this->saveNewAvatar($avatar['tmp_name'], $uniqueAvatarPath);
                        }
                    } else {
                        $user->avatar = $currentAvatarURL;
                    }
                }
				if(isset($name) && !empty($name)){
					$user->avatar = $name;
				}
                $avatarURL = $user->avatar;
			
                if ($this->Users->save($user)) {
                    $this->request->session()->write('Auth.User', $user);
                    if (!$this->request->is('json')) {
                        $this->Flash->success(__('Lưu thành công!'));
                        return $this->redirect($this->referer());
                    } else {
                        $this->_message = __('Lưu thành công!');
                        $this->_data = [];
                    }
                    $userSaved = true;
                    $this->_status = 1;
                } else {
                    if (!$this->request->is('json')) {
                        $this->Flash->error(__('The profile could not be saved. Please, try again.'));
                    } else {
                        $this->_message = __('The profile could not be saved. Please, try again.');
                        $this->_data = [];
                    }
                    $this->_status = 0;
                }
            }
// END UPDATE USER
        }

        $countries = $this->Users->Countries->find('list', ['fields' => ['id', 'country_name'],
            'keyField' => 'id', 'valueField' => 'country_name']);

        $states = $this->Users->States->find('list', ['fields' => ['id', 'name'],
            'conditions' => ['country_id' => $user ? $user->country_id : 0],
            'keyField' => 'id', 'valueField' => 'name'
        ]);
        if ($user && isset($user->country_id) && $user->country_id > 0) {
            $conn = $states->connection();
            $naQuery = new Query($conn);
            $naIdExpr = $naQuery->newExpr()->add('-1');
            $naCountryNameExpr = $naQuery->newExpr()->add('\'N/A\'');
            $naQuery->select(['id' => $naIdExpr, 'name' => $naCountryNameExpr])->from('DUAL');
            $states = $states->unionAll($naQuery);
        }

        $cond_id = 'state_id';
        $cond_val = 0;
        if ($user && $user->state_id == null) {
            $cond_id = 'country_id';
        }

        if ($user) {
            if ($user->state_id == null) {
                $cond_val = ($user->country_id > 0 ? $user->country_id : 0);
            } else {
                $cond_val = $user->state_id;
            }
        }

        $districts = $this->Users->Districts->find('list', ['fields' => ['id', 'name'],
            'conditions' => [$cond_id => $cond_val],
            'keyField' => 'id', 'valueField' => 'name']);

        $districtPostalCode = $this->Users->Districts->find('all', ['fields' => ['postal_code'],
                    'conditions' => ['id' => $user ? $user->district_id : 0]
                ])->first();
        if (!$districtPostalCode) {
            $districtPostalCode = '';
        } else {
            $districtPostalCode = $districtPostalCode->postal_code;
        }

        $rols = $this->Users->Roles->find('list', ['fields' => ['id', 'role', 'role_es', 'role_ja'],
            'keyField' => 'id', 'valueField' => 'role'
        ])->order(['role' => 'ASC']);
		$roles = array();
		$a = array('GRIP 2 (MORE AS NEEDED)','ELECTRICIAN 2 (MORE AS NEEDED)','LEAD MAN','SET DRESSER (MORE AS NEEDED)','GANG BOSS','PA #2 (MORE AS NEEDED)');
		foreach($rols as $rs)
		{
			if(!in_array($rs,$a))
			{
				$roles[] = $rs;
			}
		}	
        $bioRemainCharCounter = 500;

        if (!empty($user)) {
            if (!empty($user->avatar) && strlen($user->avatar) > 0 && strpos($user->avatar, $webRootURL) !== false) {
                $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
                $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', basename($user->avatar)]);
                $avatarFile = new File($fullPath);
                if ($avatarFile->exists()) {
                    $avatarURL = $user->avatar;
                }
            } else if (!empty($user->avatar) && strlen($user->avatar) > 0) {
                $avatarURL = $user->avatar;
            }

            $bioRemainCharCounter = 500 - mb_strlen($user->biography, 'UTF-8');
            if ($bioRemainCharCounter < 0) {
                $bioRemainCharCounter = 0;
            }
        }
        if (!$this->request->is('json')) {
            $this->set(compact(
                            'user', 'districts', 'districtPostalCode', 'states', 'countries', 'fbs', 'googles', 'linkeds', 'projects', 'roles', 'role1', 'role2', 'avatarURL', 'bioRemainCharCounter', 'websites', 'works'));
            $this->set('_serialize', ['user']);
        } else {
            if ($this->_status && !$userSaved) {
// must be post and display as the selected first role, or empty if not select
                $this->_data['Roles']['role0'] = $role1->role_id;
// must be post and display as the selected second role, or empty if not select
                $this->_data['Roles']['role1'] = $role2->role_id;

// must be post as the selected contry id, or empty if not select
                $this->_data['Countries'] = '';
// must be post as the selected district id, or empty if not select
                $this->_data['Districts'] = '';

// must be post as comma separated string, e.g: 1,2
// this is list of deleted websites' ids
                $this->_data['deletedWebsites'] = '';

// must be post as (key => value)
// this is list of added websites
// e.g, [ 'no1' => 'http://this.is.new.url ]
                $this->_data['UsersWebsite'] = [];

// others user information
                $this->_data['Users']['id'] = $user->id;
                $this->_data['Users']['first_name'] = $user->first_name;
                $this->_data['Users']['last_name'] = $user->last_name;
                $this->_data['Users']['name'] = $user->name;
                $this->_data['Users']['biography'] = $user->biography;
                $this->_data['Users']['postal_code'] = $user->postal_code;
                $this->_data['Users']['country_id'] = $user->country_id;
                $this->_data['Users']['district_id'] = $user->district_id;
// must be post and fill with all generated elements, e.g 'tmp_name', 'name', 'type' etc
                $this->_data['Users']['avatar'] = [];

// NOTE: for display only, DONT POST
// List of websites of users, id = $website->id, name = $website->website->name
                $this->_data['websites'] = $websites;
// List of interests of users
                $this->_data['interests'] = $interests;
// List of possible roles, key => value
                $this->_data['roles'] = $roles;
// List of possible countries
                $this->_data['countries'] = $countries;
// List of possible cities (depending on $user->country_id)
                $this->_data['districts'] = $districts;
// User's current avatar
                $this->_data['avatarURL'] = $avatarURL;
// List of possible works
                $this->_data['works'] = $works;
            }
            $this->set([
                'message' => $this->_message,
                'data' => $this->_data,
                'status' => $this->_status,
                '_serialize' => ['message', 'data', 'status']
            ]);
        }
    }

    private function normalizedJsonRequestData() {
        if ($this->request->isJson() && $this->request->isPost()) {
// Data POST-ed from iOS do not match what API wants
// USER INFO BEGIN
            $usersInfo = new \stdClass();
            if (isset($this->request->data['Users'])) {
                $usersInfo = json_decode($this->request->data['Users']);
            }
            $this->request->data['Users'] = [];

            if (property_exists($usersInfo, 'first_name') === TRUE) {
                $this->request->data['Users']['first_name'] = $usersInfo->first_name;
            } else {
                $this->request->data['Users']['first_name'] = "";
            }

            if (property_exists($usersInfo, 'last_name') === TRUE) {
                $this->request->data['Users']['last_name'] = $usersInfo->last_name;
            } else {
                $this->request->data['Users']['last_name'] = "";
            }

            if (property_exists($usersInfo, 'about_me') === TRUE) {
                $this->request->data['Users']['about_me'] = $usersInfo->about_me;
            } else {
                $this->request->data['Users']['about_me'] = "";
            }

            if (property_exists($usersInfo, 'postal_code') === TRUE) {
                $this->request->data['Users']['postal_code'] = $usersInfo->postal_code;
            } else {
                $this->request->data['Users']['postal_code'] = "";
            }

            if (property_exists($usersInfo, 'biography') === TRUE) {
                $this->request->data['Users']['biography'] = $usersInfo->biography;
            } else {
                $this->request->data['Users']['biography'] = "";
            }

            if (isset($this->request->data['avatar'])) {
                $this->request->data['Users']['avatar'] = $this->request->data['avatar'];
                unset($this->request->data['avatar']);
            }
// USER INFO END
// ROLES BEGIN
            $rolesInfo = new \stdClass();
            if (isset($this->request->data['Roles'])) {
                $rolesInfo = json_decode($this->request->data['Roles']);
            }
            $this->request->data['Roles'] = [];

            if (property_exists($rolesInfo, 'role1') === TRUE) {
                $this->request->data['Roles']['role0'] = intval($rolesInfo->role1);
            } else {
                $this->request->data['Roles']['role0'] = "";
            }

            if (property_exists($rolesInfo, 'role2') === TRUE) {
                $this->request->data['Roles']['role1'] = intval($rolesInfo->role2);
            } else {
                $this->request->data['Roles']['role1'] = "";
            }
// ROLES END
// USER WORK BEGIN
            $usersWork = new \stdClass();
            if (isset($this->request->data['UsersWork']) && isset($this->request->data['video'])) {
                $usersWork = json_decode($this->request->data['UsersWork']);
                $this->request->data['UsersWork'] = [];
                $this->request->data['UsersWork']['video'] = $this->request->data['video'];
                unset($this->request->data['video']);
            } else {
                $this->request->data['UsersWork'] = [];
            }

            if (property_exists($usersWork, 'title') === TRUE) {
                $this->request->data['UsersWork']['title'] = $usersWork->title;
            } else {
                $this->request->data['UsersWork']['title'] = "";
            }

            if (property_exists($usersWork, 'description') === TRUE) {
                $this->request->data['UsersWork']['description'] = $usersWork->description;
            } else {
                $this->request->data['UsersWork']['description'] = "";
            }

            if (!isset($this->request->data['UsersWork']['video'])) {
                $this->request->data['UsersWork'] = [];
            }
// USER WORK END
// WEBSITES BEGIN
            if (isset($this->request->data['UsersWebsite'])) {
                $usersWebsite = json_decode($this->request->data['UsersWebsite']);
                unset($this->request->data['UsersWebsite']);
                $this->request->data['UsersWebsite'] = [];
                foreach ($usersWebsite as $key => $value) {
                    $this->request->data['UsersWebsite'][$key] = $value;
                }
            }
// WEBSITES END
// INTERESTS BEGIN
            if (isset($this->request->data['UsersInterest'])) {
                $usersWebsite = json_decode($this->request->data['UsersInterest']);
                unset($this->request->data['UsersInterest']);
                $this->request->data['UsersInterest'] = [];
                foreach ($usersWebsite as $key => $value) {
                    $this->request->data['UsersInterest'][$key] = $value;
                }
            }
// INTERESTS END
        }
    }

    private function getUniqueAvatarFilePath($fileName, $fileType, $user_id) {
        $now = new \DateTime();
        $sanitizedFileName = md5(basename($fileName)) . '.' . $fileType;
        $uniqueFileName = join('_', [$user_id, $now->format('YmdHis'), $sanitizedFileName]);
        $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
        $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', $uniqueFileName]);

        return $fullPath;
    }

    private function getAvatarFilePathByURL($url) {
        $retval = '';
        if (!empty($url) && strlen($url) > 0 &&
                strpos($url, rtrim(Router::url($this->request->webroot, true), '/')) !== false) {
            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
            $fileName = basename($url);
            $retval = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', $fileName]);
        }

        return $retval;
    }

    private function saveWork($user_id) {
        $retval = false;
        $usersWorkData = $this->request->data['UsersWork'];
        $usersVideoData = $usersWorkData['video'];

        $fileType = $this->getFileTypeByMimeType($usersVideoData['type']);
        $uniqueVideoPath = $this->getUniqueVideoFilePath($usersVideoData['name'], $fileType, $user_id);

        $usersWorksTable = TableRegistry::get("UsersWorks");
        $usersWork = $usersWorksTable->newEntity();
        $usersWork->user_id = $user_id;
        $usersWork->work_title = $usersWorkData['title'];
        $usersWork->work_description = empty($usersWorkData['description']) ? '' : $usersWorkData['description'];
        $usersWork->work_url = $this->getVideoURLByFilePath($uniqueVideoPath);
        $usersWork->work_thumbnail = '';

        if ($usersWorksTable->save($usersWork)) {
            $this->saveNewVideo($usersVideoData['tmp_name'], $uniqueVideoPath);
            $retval = true;
        } else {
            $retval = false;
        }

        return $retval;
    }

    private function getAvatarURLByFilePath($path) {
        $retval = '';
        if (!empty($path)) {
            $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
            $fileName = basename($path);
            $retval = join('/', [$webRootURL, 'img', 'avatars', $fileName]);
        }
        return $retval;
    }

    private function getUniqueVideoFilePath($fileName, $fileType, $user_id) {
        $now = new \DateTime();
        $sanitizedFileName = md5(basename($fileName)) . '.' . $fileType;
        $uniqueFileName = join('_', [$user_id, $now->format('YmdHis'), $sanitizedFileName]);
        $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
        $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'media', 'videos', $uniqueFileName]);

        return $fullPath;
    }

    private function getVideoFilePathByURL($url) {
        $retval = '';
        if (!empty($url) && strlen($url) > 0 &&
                strpos($url, rtrim(Router::url($this->request->webroot, true), '/')) !== false) {
            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
            $fileName = basename($url);
            $retval = join(DIRECTORY_SEPARATOR, [$webRoot, 'media', 'videos', $fileName]);
        }

        return $retval;
    }

    private function getVideoURLByFilePath($path) {
        $retval = '';
        if (!empty($path)) {
            $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
            $fileName = basename($path);
            $retval = join('/', [$webRootURL, 'media', 'videos', $fileName]);
        }
        return $retval;
    }

    public function urlExists() {
        $this->autoRender = false;
        if (($this->request->is(['ajax', 'json']) && $this->request->is('post'))) {
            $url = $this->request->data['url'];
            $result = true;

            if (!$url || !is_string($url) || empty(trim($url))) {
                $result = false;
            }

            $ch = true;
            if ($result) {
                $ch = @curl_init($url);
                if ($ch === false) {
                    $result = false;
                }
            }

            if ($result) {
                @curl_setopt_array($ch, UsersController::CurlCommonOptions);
                @curl_setopt($ch, CURLOPT_HEADER, true);
                @curl_setopt($ch, CURLOPT_NOBODY, true);
                @curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
                @curl_exec($ch);
                $errCode = @curl_errno($ch);
                if ($errCode) {
                    @curl_close($ch);
                    $result = false;
                }
            }

// note: php.net documentation shows this returns a string, but really it returns an int
            if ($result) {
                $code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
                @curl_close($ch);
                $result = (200 <= $code) && ($code < 400);
            }

            if ($result) {
                echo json_encode(['status' => 1]);
                exit;
            } else {
                echo json_encode(['status' => 0]);
                exit;
            }
        } else {
            echo json_encode(['status' => 0]);
            exit;
        }
    }

    private function updateWebsite($user_id) {
        $retval = true;
        $data = $this->request->data;

        $deletedWebsites = explode(',', $data['deletedWebsites']);
        $deletedWebsitesIds = [];

        $websitesTables = TableRegistry::get('Websites');
        $usersWebsitesTables = TableRegistry::get('UsersWebsites');
        $websiteEntity = null;
        $usersWebsiteEntity = null;

// Websites to be deleted
        if (!empty($deletedWebsites)) {
            foreach ($deletedWebsites as $deletedWebsite) {
                $deletedWebsite = trim($deletedWebsite);
                if (!empty($deletedWebsite) && preg_match("/^[1-9][0-9]*$/D", $deletedWebsite)) {
                    array_push($deletedWebsitesIds, intval($deletedWebsite));
                }
            }
        }

        if (!empty($deletedWebsitesIds)) {
            $usersWebsitesTables->deleteAll([
                'UsersWebsite.website_id IN' => $deletedWebsitesIds,
                'UsersWebsite.user_id' => $user_id
            ]);

            $websitesTables->deleteAll([
                'Website.id IN' => $deletedWebsitesIds
            ]);
        }

// Websites to be added
        $otherWebsites = [];
        if (isset($data['UsersWebsite'])) {
            $otherWebsites = $data['UsersWebsite'];
        }

        if (!empty($otherWebsites) && $retval) {
            foreach ($otherWebsites as $key => $value) {
                $trimmedValue = trim($value);
                if (!empty($trimmedValue) && !is_numeric($trimmedValue)) {
                    $websiteEntity = $websitesTables->newEntity();
                    $websiteEntity->name = $trimmedValue;
                    if (!$websitesTables->save($websiteEntity)) {
                        $retval = false;
                        break;
                    };

                    $usersWebsiteEntity = $usersWebsitesTables->newEntity();
                    $usersWebsiteEntity->user_id = $user_id;
                    $usersWebsiteEntity->website_id = $websiteEntity->id;

                    if (!$usersWebsitesTables->save($usersWebsiteEntity)) {
                        $retval = false;
                        break;
                    };
                }
            }
        }

        return $retval;
    }

    private function isUrlValid($url) {
        if (empty($url))
            return false;
        return preg_match("/^(https?:\\/\\/)(?:[a-z0-9-]+\\.)*((?:[a-z0-9-]+\\.)[a-z]+)$/i", $url);
    }

    private function getAllowedVideoUploadedFileTypes() {
        return ['mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv'];
    }

    private function getMinimumVideoDimension() {
        return ['width' => 1, 'height' => 1];
    }

    private function getAllowedVideoFileSizeInMB() {
        return ['min' => 1, 'max' => 100];
    }

    private function getAllowedAvatarUploadedFileTypes() {
        return ['jpeg', 'png', 'gif', 'bmp', 'jpg'];
    }

    private function getMinimumAvatarDimension() {
        return ['width' => 1024, 'height' => 1024];
    }

    private function saveUrlWork($user_id) {
        $retval = false;
        $usersWorkData = $this->request->data['UsersWork'];
        $usersWorksTable = TableRegistry::get("UsersWorks");
        $usersWork = $usersWorksTable->newEntity();
        $usersWork->user_id = $user_id;
        $usersWork->work_title = $usersWorkData['title'];
        $usersWork->work_description = empty($usersWorkData['description']) ? '' : $usersWorkData['description'];
        $usersWork->work_url = join('/', [
            UsersController::YoutubeEmbedURLPattern,
            $this->getYoutubeVideoIdByUrl($usersWorkData['external_video_url'])]);
        $usersWork->work_thumbnail = '';

        if ($usersWorksTable->save($usersWork)) {
            $retval = true;
        } else {
            $retval = false;
        }

        return $retval;
    }

    private function isYoutubeURL($url) {
        $retval = false;
        if (isset($url)) {
            $trimmedUrl = trim($url);
            if (!empty($trimmedUrl)) {
                $regex = '/https:\/\/(www\.|m\.){0,1}youtu(\.be|be\.com)\/(watch\?v=|embed\/){0,1}([a-zA-Z0-9_-]{11})/mi';
                $matches = null;
                $retval = preg_match($regex, $trimmedUrl) ? true : false;
            }
        }

        return $retval;
    }

    private function getYoutubeVideoIdByUrl($url) {
        $retval = '';
        $trimmedUrl = trim($url);
        $regex = '/https:\/\/(www\.|m\.){0,1}youtu(\.be|be\.com)\/(watch\?v=|embed\/){0,1}([a-zA-Z0-9_-]{11})/mi';

        $matches = null;
        if (preg_match($regex, $trimmedUrl, $matches)) {
            if ($matches && count($matches) > 4) {
                $retval = $matches[4];
            }
        }

        return $retval;
    }

    private function saveNewVideo($tmpFilePath, $fullPath) {
        if (!empty($tmpFilePath) && !empty($fullPath)) {
            $uploadedFile = new File($tmpFilePath);

            if ($uploadedFile->exists()) {
                move_uploaded_file($tmpFilePath, $fullPath);
            }
        }
    }

    private function saveNewAvatar($tmpFilePath, $fullPath) {
        if (!empty($tmpFilePath) && !empty($fullPath)) {
            $uploadedFile = new File($tmpFilePath);

            if ($uploadedFile->exists()) {
                if (is_uploaded_file($tmpFilePath)) {
                    move_uploaded_file($tmpFilePath, $fullPath);
                } else {
                    @rename($tmpFilePath, $fullPath);
                }
            }
        }
    }

    private function deleteOldAvatar($fullPath) {
        if (!empty($fullPath)) {
            $oldAvatarFile = new File($fullPath);
            if ($oldAvatarFile->exists()) {
                $oldAvatarFile->delete();
            }
        }
    }

    private function getFileTypeByMimeType($mime) {
        $retval = '';
        $mimes = [
// images
            'image/bmp' => 'bmp',
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/pjpeg' => 'jpeg',
            'image/png' => 'png',
            // videos
            'video/quicktime' => 'mov',
            'video/mpeg' => 'mpeg',
            'application/x-troff-msvideo' => 'avi',
            'video/avi' => 'avi',
            'video/msvideo' => 'avi',
            'video/x-msvideo' => 'avi',
            'video/mp4' => 'mp4',
            'application/mp4' => 'mp4',
            'video/3gpp' => '3gp',
            'video/x-ms-wmv' => 'wmv',
            'video/x-flv' => 'flv'
        ];

        foreach ($mimes as $key => $value) {
            if ($key == trim($mime)) {
                $retval = $value;
                break;
            }
        }

        return $retval;
    }

    private function saveRemoteImage($url, $fullPath) {
        $ok = true;
        if (!isset($url) || !isset($fullPath) || empty($url) || empty($fullPath)) {
            $ok = false;
        }

        $ch = true;
        if ($ok) {
            $ch = @curl_init($url);
            if ($ch === false) {
                $ok = false;
            }
        }

        if ($ok) {
            @curl_setopt_array($ch, UsersController::CurlCommonOptions);
            @curl_setopt($ch, CURLOPT_HEADER, false);
            @curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            @curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

            $fp = @fopen($fullPath, 'wb');
            if ($fp !== false) {
                @curl_setopt($ch, CURLOPT_FILE, $fp);
                @curl_exec($ch);
                $errCode = @curl_errno($ch);
                @curl_close($ch);
                @fclose($fp);

                if ($errCode) {
                    $ok = false;
                    if (@file_exists($fullPath)) {
                        @unlink($fullPath);
                    }
                }
            } else {
                $ok = false;
            }
        }

        return $ok;
    }

    // Coder: Giang Dien
    // Date: 2016-11-03
    // Function: get work portfolio info via ajax
    public function getWorkPortfolioAjax() {
        if ($this->request->is('post')) {
            $work_video_url = $this->request->data['userWorkVideoUrl'];
            $work_video_id = $this->request->data['userWorkVideoID'];
            $work_title = $this->request->data['work_title'];
            $YoutubeEmbeded = $this->request->data['YoutubeEmbeded'];
            $work_description = $this->request->data['work_description'];
            $work_string_random = $this->generateRandomStringByLength(5);
            $this->set([
                'YoutubeEmbeded' => $YoutubeEmbeded,
                'work_title' => $work_title,
                'work_description' => $work_description,
                'work_video_url' => $work_video_url,
                'work_video_id' => $work_video_id,
                'work_string' => $work_string_random,
                '_serialize' => ['linkYoutubeImage', 'work_title', 'linkYoutube', 'work_description', 'work_video_url', 'work_video_id', 'work_string']
            ]);
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
        $this->viewBuilder()->layout('ajax');
    }

    // Coder: Giang Dien
    // Date: 2016-11-15
    // Function: Delete User Work Ajax
    public function deleteUserWork() {
        if ($this->request->is('post')) {
            $work_id = $this->request->data['work_id'];
            $this->loadModel('UsersWorks');
            $userWork = $this->UsersWorks->get($work_id);
            if ($this->UsersWorks->delete($userWork)) {
                echo 'TRUE';
            } else {
                echo 'FALSE';
            }
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

}
