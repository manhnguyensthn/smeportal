<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Network\Http\Client;

/**
 * Collaborators Controller
 *
 */
class CollaboratorsController extends AppController
{

	/**
     * action Save Collaborator
     * @author Roxannie Nguyen jr
     * @return array
     */
	public function actionSave(){
		$this->_status = 1;
    	$UserCurrent = $this->Auth->user();
    	if(!$UserCurrent){
    		$this->_status = 0;
            $this->_message = __('Unable to process your request.');
    	}else{
    		$iUserId = $UserCurrent['id'];
    		$TokenTable = TableRegistry::get('Tokens');
            $Token = $TokenTable->find('all', ['conditions' => ['user_id' => $iUserId]])->first()->toArray();
    		if(!isset($Token['token']) || empty($Token['token'])){
    			$this->_status = 0;
	            $this->_message = __('Invalid Token');
    		}else{
    			$Token = $Token['token'];
    		}
    	}

    	$RequestData = $this->request->data;
    	if($this->_status == 1){
	    	if(!isset($RequestData['UserId']) || empty($RequestData['UserId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$UserId = $RequestData['UserId'];
	    	}
    	}

    	if($this->_status == 1){
	    	if(!isset($RequestData['ProjectId']) || empty($RequestData['ProjectId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$ProjectId = $RequestData['ProjectId'];
	    	}
    	}
		if($this->_status == 1){
	    	if(!isset($RequestData['project_role_id']) || empty($RequestData['project_role_id'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$project_role_id = $RequestData['project_role_id'];
	    	}
    	}
    	if($this->_status == 1){
	    	if(!isset($RequestData['RoleId']) || empty($RequestData['RoleId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$RoleId = $RequestData['RoleId'];
	    	}
    	}

    	if($this->_status == 1){
    		$LinkApi = ROOT_URL . 'api/collaborators/saveOfferAction.json';
			$Params   = [
				'token' => $Token,
				'project_id' => $ProjectId,
				'user_id' => $UserId,
				'role_id' => $RoleId,
				'project_role_id' => $project_role_id,
				'collaborator_name' => '',
				'type' => 0,
			];
			$response = $this->getDataFromAPI($LinkApi, $Params, true);
			if ($response) {
	            if($response->status == 1){
	            	$this->_status = $response->status;
	            	$this->_message = $response->message;
	            	$this->_data = $response->data;
	            }else{
	            	$this->_status = 0;
		            $this->_message = $response->message;
	            }
	        }else{
	        	$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	        }
    	}
    	$this->responApi($this->_status, $this->_message, $this->_data);
    	die();
	}


	/**
     * action Offer Collaborator
     * @author Roxannie Nguyen jr
     * @return array
     */
	public function actionOffer(){
		$this->_status = 1;
    	$UserCurrent = $this->Auth->user();
    	if(!$UserCurrent){
    		$this->_status = 0;
            $this->_message = __('Unable to process your request.');
    	}else{
    		$iUserId = $UserCurrent['id'];
    		$TokenTable = TableRegistry::get('Tokens');
            $Token = $TokenTable->find('all', ['conditions' => ['user_id' => $iUserId]])->first()->toArray();
    		if(!isset($Token['token']) || empty($Token['token'])){
    			$this->_status = 0;
	            $this->_message = __('Invalid Token');
    		}else{
    			$Token = $Token['token'];
    		}
    	}

    	$RequestData = $this->request->data;

    	if($this->_status == 1){
	    	if(!isset($RequestData['UserId']) || empty($RequestData['UserId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$UserId = $RequestData['UserId'];
	    	}
    	}

    	if($this->_status == 1){
	    	if(!isset($RequestData['ProjectId']) || empty($RequestData['ProjectId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$ProjectId = $RequestData['ProjectId'];
	    	}
    	}

    	if($this->_status == 1){
	    	if(!isset($RequestData['RoleId']) || empty($RequestData['RoleId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$RoleId = $RequestData['RoleId'];
	    	}
    	}
		
			if($this->_status == 1){
	    	if(!isset($RequestData['project_role_id']) || empty($RequestData['project_role_id'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$project_role_id = $RequestData['project_role_id'];
	    	}
    	}
		
    	if($this->_status == 1){
    		$LinkApi = ROOT_URL . 'api/collaborators/saveOfferAction.json';
			$Params   = [
				'token' => $Token,
				'project_id' => $ProjectId,
				'user_id' => $UserId,
				'role_id' => $RoleId,
				'project_role_id' => $project_role_id,
				'collaborator_name' => '',
				'type' => 1,
				'language' => '',
			];
			$response = $this->getDataFromAPI($LinkApi, $Params, true);
			if ($response) {
	            if($response->status == 1){
	            	$this->_status = $response->status;
	            	$this->_message = $response->message;
	            	$this->_data = $response->data;
	            }else{
	            	$this->_status = 0;
		            $this->_message = $response->message;
	            }
	        }else{
	        	$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	        }
    	}

    	$this->responApi($this->_status, $this->_message, $this->_data);
    	die();
	}

	/**
     * action Block Collaborator
     * @author Roxannie Nguyen jr
     * @return array
     */
	public function actionBlockCollaborator(){
		$this->_status = 1;
    	$UserCurrent = $this->Auth->user();
    	if(!$UserCurrent){
    		$this->_status = 0;
            $this->_message = __('Unable to process your request.');
    	}else{
    		$iUserId = $UserCurrent['id'];
    		$TokenTable = TableRegistry::get('Tokens');
            $Token = $TokenTable->find('all', ['conditions' => ['user_id' => $iUserId]])->first()->toArray();
    		if(!isset($Token['token']) || empty($Token['token'])){
    			$this->_status = 0;
	            $this->_message = __('Invalid Token');
    		}else{
    			$Token = $Token['token'];
    		}
    	}

    	$RequestData = $this->request->data;
    	if($this->_status == 1){
	    	if(!isset($RequestData['UserId']) || empty($RequestData['UserId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$ItemId = $RequestData['UserId'];
	    	}
    	}

    	if($this->_status == 1){
    		$TypeId = $RequestData['TypeId'];
    		$aTypeId = array('block', 'unblock');
    		if(!in_array($TypeId, $aTypeId)){
    			$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
    		}
    	}

    	if($this->_status == 1){
    		$LinkApi = ROOT_URL . 'api/usersFollow/blockUser.json';
			$Params   = [
				'token' => $Token,
				'action' => $TypeId,
				'userid' => $ItemId,
			];
			$response = $this->getDataFromAPI($LinkApi, $Params, true);
			if ($response) {
	            if($response->status == 1){
					$this->_status  = $response->status;
					$this->_message = $response->message;

					$followingsTable               = TableRegistry::get('Followings');
					$TotalFollow                   = $followingsTable->getTotalFollowingFollower($ItemId);
					$response->data['TotalFollow'] = $TotalFollow;
					$response->data['UserId']      = $ItemId;
					
					$TotalFollowUserCurrent                   = $followingsTable->getTotalFollowingFollower($iUserId);
					$TotalFollowUserCurrent['iUserId']        = $iUserId;
					$response->data['TotalFollowUserCurrent'] = $TotalFollowUserCurrent;
					
	            	$this->_data = $response->data;
	            }else{
	            	$this->_status = 0;
		            $this->_message = $response->message;
	            }
	        }else{
	        	$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	        }
    	}

    	$this->responApi($this->_status, $this->_message, $this->_data);
    	die();
	}
	
}