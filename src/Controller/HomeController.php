<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Request;
use App\Lib\ProjectsLib;
use Cake\Network\Http\Client;
use Cake\View\Helper;
use Cake\View\View;

//use App\Lib\phpFlickr;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class HomeController extends AppController {
    public $paginate = array();
    public $helpers = array('Paginator', 'General');
    public $components = array('PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['blockFilterProject', 'getSliders']);
    }

    /**
     * get Featured Project
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function blockFeaturedProject() {
        $this->loadModel('Projects');
        $aCound = array('Projects.status' => 2);
        $sOrder = 'rand()';
        $row = $this->Projects->getFeaturedProject($aCound, $sOrder);
        return $row;
    }

    /**
     * get Jush For You - Topicks - NearYour
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function blockFilterProject() {
        if ($this->request->is('post') && $this->request->isAjax()) {
            $aRequestData = $this->request->data;
            $sType = (isset($aRequestData['sType']) ? $aRequestData['sType'] : 'just-for-you');
            switch ($sType) {
                case 'top-picks': 
                    $sHtmlItem = $this->getTopPicks();
                    break;
                case 'near-you':
                    $sHtmlItem = $this->getNearYous();
                    break;
                default:
                    $sType = 'just-for-you';
                    $sHtmlItem = $this->getJustForYous();
                    break;
            }

            $aRequestPaging = $this->request->param('paging');
            $iPageCount     = (int) (isset($aRequestPaging['Projects']['pageCount']) ? $aRequestPaging['Projects']['pageCount'] : 0);
            $iPage          = (int) (isset($aRequestData['page']) ? $aRequestData['page'] : 1);

            if ($iPage >= $iPageCount) {
                $this->_data['sHtmlButtonSeeMore'] = '';
            } else {
                $sHtml = '<center>';
                    $sHtml .= '<a href="javascript:void(0);" onclick="getHomeFilterProject(this, &#39;' . $sType . '&#39;, ' . ((int) $iPage + 1) . ');" class="btn btn-warning btn-see-more">';
                        $sHtml .= '<span>See more</span>';
                        $sHtml .= '<span class="loading"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Loading...</span>';
                    $sHtml .= '</a>';
                $sHtml .= '</center>';
                $this->_data['sHtmlButtonSeeMore'] = $sHtml;
            }

            if(isset($sHtmlItem['projects'])){
                $view = new View($this->request,$this->response,null);
                $view->set('projects', $sHtmlItem['projects']);
                $view->set('ListUsersActions', isset($sHtmlItem['ListUsersActions']) ? $sHtmlItem['ListUsersActions'] : []);
                $view->set('ListUserProjects', isset($sHtmlItem['ListUserProjects']) ? $sHtmlItem['ListUserProjects'] : []);

                $view->layout = false;
                $sHtmlItem['sHtmlItem'] = $view->render('/Element/Home/item-filter-project');
            }

            $this->_data['sHtmlItem'] = $sHtmlItem;
            $this->_data['sType']     = $sType;
            $this->_status = 1;
            $this->responApi($this->_status, $this->_message, $this->_data);
            die();
        } else {
            $aJustForYous = $this->getJustForYous();
            $aTopPicks = $this->getTopPicks();
            $aNearYous = $this->getNearYous();
            return array($aJustForYous, $aTopPicks, $aNearYous);
        }
    }

    /**
     * get Jush For You
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getJustForYous() {
        $this->loadModel('Projects');
        $this->loadModel('UsersRoles');
        $this->loadModel('SearchedHistory');
        $aRequestData = $this->request->data;

        $aConditions  = array('Projects.status IN' => array(PROJECT_STATUS_SHOW, PROJECT_STATUS_SHOW_FEATURED));
        $aContain     = array('Users', 'Categories', 'States', 'Countries');
        $iPageSize    = 6;
        $aOrder       = ['Projects.created' => 'DESC'];
        $iUserId      = $this->Auth->user('id');
        $iPage        = (int)(isset($aRequestData['page']) ? $aRequestData['page'] : 0);

        if($iUserId){
            $aRoles   = $this->UsersRoles->getUserRoles($iUserId);
            $aRoleIds = array();
            if ($aRoles) {
                foreach ($aRoles as $key => $aRole) {
                    $aRoleIds[] = $aRole->role_id;
                }
            }
            $coundRole = array();
            if($aRoleIds){
                $aContain[] = 'ProjectsRoles';
                $coundRole[]['ProjectsRoles.role_id IN'] = $aRoleIds;
            }

            $aSearchHistorys = $this->SearchedHistory->getUserHistorySearchs($iUserId, 10);
            $aCoundSearch = array();
            if ($aSearchHistorys) {
                foreach ($aSearchHistorys as $key => $aSearchHistory) {
                    if (isset($aSearchHistory->keyword) && !empty($aSearchHistory->keyword)) {
                        $aCoundSearch[]['Projects.title LIKE'] = '%' . strip_tags($aSearchHistory->keyword) . '%';
                    }
                }
            }

            if($coundRole || $aCoundSearch){
                $aConditions['OR'] = array_merge($coundRole, $aCoundSearch);
            }
        }
        $aParamPaginate = array(
            'contain'    => $aContain,
            'conditions' => $aConditions,
            'limit'      => $iPageSize,
            'maxLimit'   => $iPageSize,
            'order'      => $aOrder,
            'group'      => array('Projects.id'),
            'page'       => $iPage,
        );

        $this->paginate = $aParamPaginate;
        $aRows    = $this->paginate($this->Projects);
        $aReturns = array();
        $i = 0;

        $ListProjectIds = array();
        foreach ($aRows as $key => $aRow) {
            $i++;
            if(!$this->request->isAjax() && $i > 3){
                break;
            }
            $aMoreFilterProject = $this->_addMoreFilterProject($aRow);
            if ($aMoreFilterProject){
                $aReturns[$key]   = $aMoreFilterProject;
                $ListProjectIds[] = $aRow->id;
            }
        }

        $this->loadModel('UserProjectActions');
        $this->loadModel('UsersProjects');
        $ListUsersActions = array();
        $ListUserProjects = array();
        if($ListProjectIds){
            $ListUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $ListProjectIds, 'action_type' => 1, 'status' => 1]);
            $ListUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $ListProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
        }

        if (!$this->request->isAjax()) {
            $aRequestParams = $this->request->params;
            if (isset($aRequestParams['paging']['Projects'])) {
                $aPaging = $aRequestParams['paging']['Projects'];
            }
        }

        $aReturn = array(
            'projects'         => $aReturns,
            'paging'           => (isset($aPaging) ? $aPaging : array()),
            'ListUsersActions' => $ListUsersActions,
            'ListUserProjects' => $ListUserProjects,
        );

        return $aReturn;
    }

    /**
     * get Top Picks
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getTopPicks() {
        $this->loadModel('ProjectsRoles');
        $this->loadModel('UsersProjects');
        $this->loadModel('Projects');
        $this->loadModel('UserProjectActions');

        $aRequestData = $this->request->data;
        $iPageSize    = 6;
        $aContain     = ['Users', 'Categories', 'States', 'Countries', 'ProjectsRoles', 'UsersProjects'];
        $aConditions  = array('Projects.status IN' => array(PROJECT_STATUS_SHOW, PROJECT_STATUS_SHOW_FEATURED));
        $aOrder       = ['Projects.viewed_counter' => 'DESC'];
        $aGroup       = ['Projects.id'];
        $iPage        = (int)(isset($aRequestData['page']) ? $aRequestData['page'] : 0);
        $oQuery       = $this->Projects->find();
        
        $this->paginate = array(
            'contain'    => $aContain,
            'conditions' => $aConditions,
            'limit'      => $iPageSize,
            'maxLimit'   => $iPageSize,
            'group'      => $aGroup,
            'page'       => $iPage,
        );

        $oQuery = $this->Projects->find();
        $oQuery->select(array(
            'iCountJoin' => $oQuery->func()->count('UsersProjects.project_id'),
        ))->autoFields(true);
        
        $oQuery->order($aOrder);

        $aRows    = $this->paginate($oQuery);
        $aReturns = array();
        $i        = 0;

        $ListProjectIds = array();
        foreach ($aRows as $key => $aRow) {
            $i++;
            if(!$this->request->isAjax() && $i > 3){
                break;
            }
            $aMoreFilterProject = $this->_addMoreFilterProject($aRow);
            if ($aMoreFilterProject){
                $aReturns[$key]   = $aMoreFilterProject;
                $ListProjectIds[] = $aRow->id;
            }
        }

        $ListUsersActions = array();
        $ListUserProjects = array();
        if($ListProjectIds){
            $ListUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $ListProjectIds, 'action_type' => 1, 'status' => 1]);
            $ListUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $ListProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
        }

        if (!$this->request->isAjax()) {
            $aRequestParams = $this->request->params;
            if (isset($aRequestParams['paging']['Projects'])) {
                $aPaging = $aRequestParams['paging']['Projects'];
            }
        }

        $aReturn = array(
            'projects'         => $aReturns,
            'paging'           => (isset($aPaging) ? $aPaging : array()),
            'ListUsersActions' => $ListUsersActions,
            'ListUserProjects' => $ListUserProjects,
        );
        return $aReturn;
    }

    /**
     * get Near Yous
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getNearYous() {
        $this->loadModel('ProjectsRoles');
        $this->loadModel('UsersProjects');
        $this->loadModel('UserProjectActions');

        $aRequestData       = $this->request->data;
        $aUser              = $this->Auth->user();
        $aRequestSession    = $this->request->session();
        $dataLocation       = $aRequestSession->read('Config.location');
        $dataLocationLatLng = $aRequestSession->read('Config.locationLatLng');

        $iPageSize   = 6;
        $aContain    = ['Users', 'Categories', 'Countries', 'States', 'Districts'];
        $aConditions = array('Projects.status IN' => array(PROJECT_STATUS_SHOW, PROJECT_STATUS_SHOW_FEATURED));
        $aOrder = array();
        $aGroup = ['Projects.id'];
        $iPage = (int) (isset($aRequestData['page']) ? $aRequestData['page'] : 0);
        $aDataId = (array) (isset($this->request->data['DataId']) ? explode(',', $this->request->data['DataId']) : array());
        if (!$aUser) {
            if ($aDataId) {
                $aConditions['Projects.id NOT IN'] = $aDataId;
            }
            $aOrder[] = 'rand()';
        }
        $coundLocationUser = array();
        if ($dataLocation) {
            if ($dataLocationLatLng) {
                $minLat = (isset($dataLocationLatLng['minLat']) ? $dataLocationLatLng['minLat'] : '');
                $maxLat = (isset($dataLocationLatLng['maxLat']) ? $dataLocationLatLng['maxLat'] : '');
                $minLng = (isset($dataLocationLatLng['minLng']) ? $dataLocationLatLng['minLng'] : '');
                $maxLng = (isset($dataLocationLatLng['maxLng']) ? $dataLocationLatLng['maxLng'] : '');
            } else {
                $minLat = '';
                $maxLat = '';
                $minLng = '';
                $maxLng = '';

                $radius = 200000;
                $tocken = TOKEN_GOOGLE_MAP;
                $location = implode(',', $dataLocation);
                //bypass proxy
                // $aContext = array(
                //     'http' => array(
                //         'proxy' => 'tcp://192.168.100.11:8080',
                //         'request_fulluri' => true,
                //     ),
                // );
                // $cxContext = stream_context_create($aContext);
                // //add proxy to third param
                // $result = file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' . $location . '&radius=' . $radius . '&key=' . $tocken,false,$cxContext);

                $result = file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' . $location . '&radius=' . $radius . '&key=' . $tocken);
                $result = json_decode($result, true);

                if (isset($result['results']) && !empty($result['results'])) {
                    foreach ($result['results'] as $key => $row) {
                        $locationResult = (isset($row['geometry']['location']) ? $row['geometry']['location'] : array());
                        if ($locationResult) {
                            $lat = (isset($locationResult['lat']) ? $locationResult['lat'] : '');
                            $lng = (isset($locationResult['lat']) ? $locationResult['lat'] : '');
                            if($minLat == '') $minLat = $locationResult['lat'];
                            if($maxLat == '') $maxLat = $locationResult['lat'];
                            if($minLng == '') $minLng = $locationResult['lng'];
                            if($maxLng == '') $maxLng = $locationResult['lng'];
                            if($lat != ''){
                                if($lat < $minLat) $minLat = $lat;
                                if($lat > $maxLat) $maxLat = $lat;
                            }

                            if($lng != ''){
                                if($lng < $minLng) $minLng = $lng;
                                if($lng > $maxLng) $maxLng = $lng;
                            }
                        }
                    }
                }

                if ($minLat != '' && $maxLat != '' && $minLng != '' && $maxLng != '') {
                    $locationLatLng = array('minLat' => $minLat, 'maxLat' => $maxLat, 'minLng' => $minLng, 'maxLng' => $maxLng);
                    $aRequestSession->write('Config.locationLatLng', $locationLatLng);
                }
            }


            if ($minLat != '' && $maxLat != '' && $minLng != '' && $maxLng != '') {
                $coundCountries = array();
                // $coundCountries['Countries.lat >='] = $minLat;
                // $coundCountries['Countries.lat <='] = $maxLat;
                // $coundCountries['Countries.lng >='] = $minLng;
                // $coundCountries['Countries.lng <='] = $maxLng;

                $coundStates = array();
                $coundStates['States.lat >='] = $minLat;
                $coundStates['States.lat <='] = $maxLat;
                $coundStates['States.lng >='] = $minLng;
                $coundStates['States.lng <='] = $maxLng;

                $coundDistricts = array();
                $coundDistricts['Districts.lat >='] = $minLat;
                $coundDistricts['Districts.lat <='] = $maxLat;
                $coundDistricts['Districts.lng >='] = $minLng;
                $coundDistricts['Districts.lng <='] = $maxLng;
                $coundLocationUser = [$coundCountries, $coundStates, $coundDistricts];
            }
        }

        if ($aUser) {
            $coundUser = array();
            if (isset($aUser['country_id']) && $aUser['country_id']) {
                $coundUser['Projects.country_id'] = $aUser['country_id'];
            }
            if (isset($aUser['state_id']) && $aUser['state_id']) {
                $coundUser['Projects.state_id'] = $aUser['state_id'];
            }
            if (isset($aUser['district_id']) && $aUser['district_id']) {
                $coundUser['Projects.district_id'] = $aUser['district_id'];
            }
            if ($coundUser)
                $coundLocationUser[] = $coundUser;
        }
        if($coundLocationUser) $aConditions[] = array('OR' => $coundLocationUser);
        $this->paginate = array(
            'contain'    => $aContain,
            'conditions' => $aConditions,
            'limit'      => $iPageSize,
            'maxLimit'   => $iPageSize,
            'group'      => $aGroup,
            'order'      => $aOrder,
            'page'       => $iPage,
        );

        $aRows = $this->paginate('Projects');
        $aReturns = array();

        $i = 0;
        $ListProjectIds = array();
        foreach ($aRows as $key => $aRow) {
            $i++;
            if(!$this->request->isAjax() && $i > 3){
                break;
            }
            $aMoreFilterProject = $this->_addMoreFilterProject($aRow);
            if ($aMoreFilterProject) {
                $aDataId[]        = $aRow->id;
                $aReturns[$key]   = $aMoreFilterProject;
                $ListProjectIds[] = $aRow->id;
            }
        }

        $ListUsersActions = array();
        $ListUserProjects = array();
        if($ListProjectIds){
            $ListUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $ListProjectIds, 'action_type' => 1, 'status' => 1]);
            $ListUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $ListProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
        }

        if (!$this->request->isAjax()) {
            $aRequestParams = $this->request->params;
            if (isset($aRequestParams['paging']['Projects'])) {
                $aPaging = $aRequestParams['paging']['Projects'];
            }
        }

        $aReturn = array(
            'projects'         => $aReturns,
            'paging'           => (isset($aPaging) ? $aPaging : array()),
            'DataId'           => trim(implode(',', $aDataId), ','),
            'ListUsersActions' => $ListUsersActions,
            'ListUserProjects' => $ListUserProjects,
        );
        return $aReturn;
    }

    /**
     * add More Filter Project
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _addMoreFilterProject($aRow = null) {
        $aReturn = array();
        if ($aRow) {
            $this->loadModel('ProjectsRoles');
            $this->loadModel('UsersProjects');
            $aReturn = $aRow;
            
            $iQuantity        = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $aRow->id], $aRow);
            $UserProject      = $this->UsersProjects->getUserProjectsByOptions(['project_id' => $aRow->id, 'status' => 1, 'type' => 2]);
            $iTotalRoleJoined = count($UserProject);

            $iPercentRoleJoined = 0;
            if($iTotalRoleJoined < $iQuantity){
                $iPercentRoleJoined = ($iTotalRoleJoined / $iQuantity) * 100;
            }else{
                $iPercentRoleJoined = 100;
            }
            
            $aReturn['iQuantity'] = (int) $iQuantity;
            $aReturn['iTotalRoleJoined'] = (int) $iTotalRoleJoined;
            $aReturn['iPercentRoleJoined'] = (int) $iPercentRoleJoined;
            return $aReturn;
        }
        return false;
    }

    /**
     * GET sliders
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getSliders($iPageSize = 5){
        $oSlidersTable = TableRegistry::get('Sliders');
        $aSliders = $oSlidersTable->find()->limit($iPageSize)->toArray();
        return $aSliders;
    }
}