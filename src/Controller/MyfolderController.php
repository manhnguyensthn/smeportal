<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Network\Http\Client;

//use App\Lib\phpFlickr;

/**
 * Discover Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class MyfolderController extends AppController {

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow([]);
    }

    // Coder: Giang Dien
    // Date: 2016-12-08
    // Function: get my folder
    public function getMyfolder() {
        $linkApi = ROOT_URL . 'api/myFolders/getMyFolder.json';
        // var_dump($linkApi);
        $currentUser = $this->Auth->user();
        $tokenTable = TableRegistry::get('Tokens');
        $token = $tokenTable->find('all', [
            'conditions' => ['user_id' => $currentUser['id']],
        ]);
        $token = $token->first()->toArray();
        if (!empty($token)) {
            $listCreatedProjects = $this->getDataFromAPI($linkApi, ['token' => $token['token'], 'type' => 0, 'limit' => 7, 'offset' => 0, 'page' => 1]);
            $this->set('title', __('We the projects') . ' - ' . __('My folder'));
            $this->set(['current_url' => $this->referer()]);
            $this->set(['meta_description' => __('My folder')]);
            $this->set('listCreatedProjects', $listCreatedProjects->project);
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 16/01/2017
    // Function: get my folder tab content
    public function getMyFolderTabAjax() {
        if ($this->request->is('post')) {
            $type = $this->request->data['type'];
            $linkApi = ROOT_URL . 'api/myFolders/getMyFolder.json';
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $listProjects = $this->getDataFromAPI($linkApi, ['token' => $token['token'], 'type' => $type, 'limit' => 7, 'offset' => 0, 'page' => 1]);
            if (isset($listProjects->project)) {
                $this->set('listProjects', $listProjects->project);
            } else {
                $this->set('listProjects', []);
            }
            $this->set('type', $type);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-12-09
    // Function: get List My folder See More Ajax
    public function getListMyfolderSeeMoreAjax() {
        if ($this->request->is('post')) {
            $page = $this->request->data['page'];
            $type = $this->request->data['type'];
            $linkApi = ROOT_URL . 'api/myFolders/getMyFolder.json';
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $offset = ($page - 1) * 6 + 1;
            $listProjects = $this->getDataFromAPI($linkApi, ['token' => $token['token'], 'type' => $type, 'limit' => 6, 'offset' => $offset, 'page' => $page]);
            $this->set('listProjects', $listProjects->project);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

}

?>