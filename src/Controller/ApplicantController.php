<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Network\Http\Client;
use Cake\View\View;
use App\Lib\CoreLib;
//use App\Lib\phpFlickr;

/**
 * Discover Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class ApplicantController extends AppController {

    public $paginate = array();
    public $helpers = array('Paginator');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['getProfileApplicant','getProjectApplicantAjax', 'getProjectApplicant', 'getFollowingApplicant', 'getFollowerApplicant']);
    }

    // Coder: Giang Dien
    // Date: 2016-12-08
    // Function: get application
    public function getApplication($project_id = 0) {
        $this->loadModel('Projects');
        $project = $this->Projects->find('all', ['conditions' => ['id' => $project_id]])->first();
        if (!empty($project) && !empty($project_id)) {
            $linkApi = ROOT_URL . 'api/ProjectsRoles/getListProjectRoles.json';
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $listApplicants = $this->getDataFromAPI($linkApi, ['token' => $token['token'], 'project_id' => $project_id, 'keyword' => '']);
            $this->set('listApplicants', $listApplicants);
            $linkGetFindmoreApi = ROOT_URL . '/api/ProjectsRoles/getAllListRoles.json';
            $listFindmore = $this->getDataFromAPI($linkGetFindmoreApi, ['token' => $token['token'], 'limit' => 10, 'page' => 1, 'keyword' => '']);
            $this->set('listFindmore', $listFindmore);
            $this->set('project', $project);
            $this->set('title', __('SME') . ' - ' . __('Cá nhân'));
            $this->set(['current_url' => $this->referer()]);
            $this->set(['meta_description' => __('SME')]);
        } else {
            $this->Flash->error(__('No match data'));
            $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-12-12
    // Function: get List Applicant SeeMore Ajax
    public function getListApplicantSeeMoreAjax() {
        if ($this->request->is('post')) {
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $page = $this->request->data['page'];
            $linkGetFindmoreApi = ROOT_URL . '/api/ProjectsRoles/getAllListRoles.json';
            $listFindmore = $this->getDataFromAPI($linkGetFindmoreApi, ['token' => $token['token'], 'limit' => 10, 'page' => $page, 'keyword' => '']);
            $this->set('project_id',$this->request->data['project_id']);
            $this->set('listFindmore', $listFindmore);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang_Dien
    // Date: 2016-12-12
    // Function: Get list collaborator in applicants
    public function ApplicantDetail($project_id = 0, $project_role_id = 0) {
        $this->loadModel('Projects');
        $project = $this->Projects->find('all', ['conditions' => ['id' => $project_id]])->first();
        if (!empty($project) && !empty($project_id)) {
            $this->set('project', $project);
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $linkGetUserProjectsApi = ROOT_URL . '/api/UsersProjects/getUsersProjectsByOptions.json';
            $listUsers = $this->getDataFromAPI($linkGetUserProjectsApi, ['token' => $token['token'], 'project_id' => $project_id, 'project_role_id' => $project_role_id, 'option' => 0, 'keyword' => '']);
            $this->set('listUsers', $listUsers);
            $this->loadModel('ProjectsRoles');
            $projectRole = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.id' => $project_role_id], ['Roles']);
            if (isset($projectRole[0]['role'])) {
                $this->set('currentRole', $projectRole[0]['role']);
            } else {
                $this->Flash->error(__('No match data'));
                $this->redirect('/');
            }
            $this->set('project_role_id', $project_role_id);
            $this->set('title', __('We the projects') . ' - ' . __('Applicants detail'));
            $this->set(['current_url' => $this->referer()]);
            $this->set(['meta_description' => __('Applicants detail')]);
        } else {
            $this->Flash->error(__('No match data'));
            $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-12-12
    // Function: get List Collaborator Tab Ajax
    public function getListCollaboratorTabAjax() {
        if ($this->request->is('post')) {
            $project_id = $this->request->data['project_id'];
            $this->loadModel('Projects');
            $project = $this->Projects->find('all', ['conditions' => ['id' => $project_id]])->first();
            $this->set('project', $project);
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $linkGetUserProjectsApi = ROOT_URL . '/api/UsersProjects/getUsersProjectsByOptions.json';
            $project_role_id = $this->request->data['project_role_id'];
            $role_id = $this->request->data['role_id'];
            $type = $this->request->data['type'];
            $listUsers = $this->getDataFromAPI($linkGetUserProjectsApi, ['token' => $token['token'], 'project_id' => $project_id, 'project_role_id' => $project_role_id, 'option' => $type, 'keyword' => '']);
            $this->set('listUsers', $listUsers);
			$this->set('project_role_id', $project_role_id);
            $this->loadModel('Roles');
            $currentRole = $this->Roles->get($role_id);
            $this->set('currentRole', $currentRole);
            $this->set('type', $type);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang_Dien
    // Date: 2016-12-12
    // Function: get list collaborator by role
    public function getCollaboratorByRoleAndKeyword() {
        if ($this->request->is('post')) {
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $role_id = $this->request->data['role_id'];
            $keyword = $this->request->data['keyword'];
			if(isset($this->request->data['find_more']))
			{
				$find_more = 0;
			}
			else
			{
				$find_more = 1;
			}
            $page = $this->request->data['page'];
            $list_page = $this->request->data['list_page'];
            $linkGetCollaboratorFindmoreApi = ROOT_URL . '/api/UsersRoles/findUserByKeyword.json';
            $listUsers = $this->getDataFromAPI($linkGetCollaboratorFindmoreApi, ['token' => $token['token'], 'role_id' => $role_id, 'limit' => 10, 'page' => $page, 'keyword' => $keyword]);
            $this->set('listUsers', $listUsers);
            $this->set('current_role_id', $role_id);
            $this->set('list_page', $list_page);
			$this->set('find_more',$find_more);
            $this->set('project_id',$this->request->data['project_id']);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-12-12
    // Function: get list find more
    public function getListFindMoreAjax() {

        if ($this->request->is('post')) {
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $linkGetFindmoreApi = ROOT_URL . '/api/ProjectsRoles/getAllListRoles.json';
			$Languages = CoreLib::getLanguage();
			$LanguageCurrent = CoreLib::getLanguage(false);	
			if($LanguageCurrent['name'] === 'English')
			{
				$language = 'en';
			}
			else if($LanguageCurrent['name'] === '日本語')
			{
				$language = 'ja';
			}
			else
			{
				$language = 'es';
			}
	
            $listFindmore = $this->getDataFromAPI($linkGetFindmoreApi, ['token' => $token['token'], 'limit' => 10, 'page' => 1, 'keyword' => '','language' =>$language]);
            $this->set('project_id',$this->request->data['project_id']);
            $this->set('listFindmore', $listFindmore);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

	public function get_findmore($project_id = 0)
	{
		$this->loadModel('Projects');
		if($project_id > 0 )
		{
		$project = $this->Projects->find('all', ['conditions' => ['id' => $project_id]])->first();
        }
		else
		{
		$project = $this->Projects->find()->first();
		}

		$project_id =  $project->id;
        if (!empty($project)) {
            $linkApi = ROOT_URL . 'api/ProjectsRoles/getListProjectRoles.json';
            $currentUser = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $currentUser['id']],
            ]);
            $token = $token->first()->toArray();
            $listApplicants = $this->getDataFromAPI($linkApi, ['token' => $token['token'], 'project_id' => $project_id, 'keyword' => '']);
            $this->set('listApplicants', $listApplicants);
            $linkGetFindmoreApi = ROOT_URL . '/api/ProjectsRoles/getAllListRoles.json';
            $listFindmore = $this->getDataFromAPI($linkGetFindmoreApi, ['token' => $token['token'], 'limit' => 10, 'page' => 1, 'keyword' => '']);
            $this->set('listFindmore', $listFindmore);
            $this->set('project', $project);
			$this->set('project_id', $project_id);
            $this->set('title', __('We the projects') . ' - ' . __('Applicants'));
            $this->set(['current_url' => $this->referer()]);
            $this->set(['meta_description' => __('Applicants')]);
        } else {
            $this->Flash->error(__('No match data'));
            $this->redirect('/');
        }
	}
	
    // Coder: Giang Dien
    // Date: 2016-12-12
    // Function: get profile applicant
    public function getProfileApplicant($user_id = 0, $project_id = 0, $role_id = 0, $project_role_id = 0) {
        $user = $this->_getUserApplicant($user_id);
		$authUser = $this->Auth->user();
		if(!empty($authUser))
		{
			$checkUser = 1;
		}
		else
		{
			$checkUser = 0;
		}
		$this->loadModel('UsersProjects');	
		$usercreatepro = $this->UsersProjects->getUserProjectsByOptions(['project_id' => $project_id],"", "");
		foreach ($usercreatepro as $us)
		{
			$userid = $us->user_id;
		}
		$userproject = $this->UsersProjects->
		getMessage_content(['user_id' => $user_id, 'project_id' => $project_id, 'role_id' => $role_id]);
		if(!empty($user_id) && !empty($project_id) && !empty($role_id) && $userid == $authUser['id'])
		{
		$message = '';
		if(!empty($userproject))
		{
			foreach($userproject as $us)
			{
				$message =  $us->message_content;
			}
			$reply = "/usersprojects/userChatDetail/".$project_id.'/'.$user_id.'?role='.$role_id;
		}
		}
        if (!$user) {
            if (!$this->request->isAjax()) {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            } else {
                $this->_redirectUserIsNotLoginAjax();
            }
        } else {
            // Check blocking user
            $blockUserTable = TableRegistry::get('Blocks');
            $blockUserList = $blockUserTable->find('all', ['conditions' => ['user_id' => $user_id, 'blocked_id' => $authUser['id']]])->toArray();
            $blockerUser = $blockUserTable->find('all', ['conditions' => ['user_id' => $authUser['id'], 'blocked_id' => $user_id ]])->toArray();
            if (!empty($blockUserList) || !empty($blockerUser)) {
                $this->Flash->error(__('No match data'));
                $this->redirect('/');
            } else {
                $this->_getFollowingFollower($user);
                $this->_getWorkApplicant($user);
                $this->_validateCollaborator($user_id, $project_id, $role_id,$project_role_id);
            }
        }

        if ($this->request->isAjax()) {
            $view = new View($this->request, $this->response, null);
            $view->set('user', $user);
            $view->layout = false;
            echo $view->render('/Element/Applicant/content_profile');
            die();
        }
        $this->set('title', __('Hà Nội SME') . ' - ' . __('Cá nhân'));
        $this->set('TotalProject', $this->_countCompletedCreated($user->id));
        $this->set('user', $user);
		$this->set('checkUser', $checkUser);
		if(isset($message))
		{
		$this->set('message', $message);
		$this->set('reply', $reply);
		}
		$this->set('project_role_id', $project_role_id);
        $this->set('tab_active', 'profile');
    }

    // Coder: Giang Dien
    // Date: 2017-02-10
    // Function: get findmore profile
    public function getFindMoreProfile($project_id = 0, $user_id = 0) {
        $user = $this->_getUserApplicant($user_id);
        // Check blocking user
        $authUser = $this->Auth->user();
        $blockUserTable = TableRegistry::get('Blocks');
		$Projects = TableRegistry::get('Projects');
        $userpro = $Projects->find('all', ['conditions' => ['user_id' => $authUser['id']]])->toArray();
		if(!empty($userpro))
		{
			$invite = 1;
		}
		else
		{
			$invite = 0;
		}
        $blockUserList = $blockUserTable->find('all', ['conditions' => ['user_id' => $user_id, 'blocked_id' => $authUser['id']]])->toArray();
        $blockUser = $blockUserTable->find('all', ['conditions' => ['user_id' => $authUser['id'], 'blocked_id' => $user_id]])->toArray(); 
		if (!empty($blockUserList) || !empty($blockUser)) {
            $this->Flash->error(__('No match data'));
            $this->redirect('/');
        } else {
            $this->_getFollowingFollower($user);
            $this->_getWorkApplicant($user);
        }
        if ($this->request->isAjax()) {
            $view = new View($this->request, $this->response, null);
            $view->set('user', $user);
            $view->layout = false;
            echo $view->render('/Element/Applicant/content_profile');
            die();
        }
        $this->set('project_id',$project_id);
        $this->set('is_userProfile',TRUE);
        $this->set('TotalProject', $this->_countCompletedCreated($user->id));
        $this->set('user', $user);
		$this->set('invite', $invite);
        $this->set('tab_active', 'profile');
    }

    /**
     * Redirect Ajax
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _validateCollaborator($user_id = 0, $project_id = 0, $role_id = 0, $project_role_id ='') {
        $bSave = false;
        $bOffer = false;
        $status = 1;
        $authUser = $this->Auth->user();

        if (($project_id || $role_id) && $authUser['id'] != $user_id) {
            if (!$project_id || !$role_id) {
                $status = 0;
            }

            if ($status == 1) {
                $ProjectsTable = TableRegistry::get('Projects');
                $UsersProjectsTable = TableRegistry::get('UsersProjects');
                $project = $ProjectsTable->find()
                        ->contain(['Users', 'Categories'])
                        ->where(['Projects.id' => $project_id])
                        ->group(['Projects.id'])
                        ->first()
                        ->toArray();

                // Check UserId Owner
                if (!isset($project['user']['id']) || $project['user']['id'] != $authUser['id']) {
                    $status = 0;
                }
            }

            // Check User Collaborator UserId
            if ($status == 1) {
                $this->loadModel('UsersProjects');
				if(empty($project_role_id))
				{
					   $UserProjects = $UsersProjectsTable->find()
                        ->where(['user_id' => $user_id, 'project_id' => $project_id, 'role_id' => $role_id])
                        ->first();
				}
				else
				{
                $UserProjects = $UsersProjectsTable->find()
                        ->where(['user_id' => $user_id, 'project_id' => $project_id, 'role_id' => $role_id,'project_role_id' =>$project_role_id])
                        ->first();
				}
                if ($UserProjects) {
                    if (isset($UserProjects->saved) && $UserProjects->saved != 1) {
                        $bSave = true;
                    }
                    if (isset($UserProjects->offer) && $UserProjects->offer != 1) {
                        $bOffer = true;
                    }
                }
            }
        }

        $this->set('bSave', $bSave);
        $this->set('bOffer', $bOffer);
        $this->set('project_id', $project_id);
        $this->set('role_id', $role_id);
    }

    /**
     * Redirect Ajax
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _redirectUserIsNotLoginAjax($Link = '/') {
        $this->Flash->error(__('Tài khoản đã đăng nhập ở thiết bị khác.'));
        echo '<script type="text/javascript">window.location = "' . $Link . '";</script>';
        die();
    }

    /**
     * Controller Project Applicant
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getProjectApplicant($user_id = 0, $project_id = 0, $role_id = 0) {
        $user = $this->_getUserApplicant($user_id);
        $authUser = $this->Auth->user();
		if(!empty($authUser))
		{
			$checkUser = 1;
		}
		else
		{
			$checkUser = 0;
		}
		$blockUserTable = TableRegistry::get('Blocks');
        $blockUserList = $blockUserTable->find('all', ['conditions' => ['user_id' => $user_id, 'blocked_id' => $authUser['id']]])->toArray();
        $blockUser = $blockUserTable->find('all', ['conditions' => ['user_id' => $authUser['id'], 'blocked_id' => $user_id]])->toArray(); 
		if (!empty($blockUserList) || !empty($blockUser)) {
            $this->Flash->error(__('No match data'));
            $this->redirect('/');
        }
        if (!$user) {
            if (!$this->request->isAjax()) {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            } else {
                $this->_redirectUserIsNotLoginAjax();
            }
        } else {
            $this->_getFollowingFollower($user);
            $this->_validateCollaborator($user_id, $project_id, $role_id);
        }


        $projects = array();
        $ListUsersActions = array();
        $ListUserProjects = array();
        $this->getProjectApplicantAjax($user->id, $projects, $ListUsersActions, $ListUserProjects);

        $TotalProject = $this->_countCompletedCreated($user->id);

        if ($this->request->isAjax()) {
            $view = new View($this->request, $this->response, null);
            $view->set('user', $user);
            $view->set('projects', $projects);
            $view->set('ListUsersActions', $ListUsersActions);
            $view->set('ListUserProjects', $ListUserProjects);
            $view->set('TotalProject', $TotalProject);
            $view->set('PageSize', 3);
            $view->layout = false;
            echo $view->render('/Element/Applicant/content_project');
            die();
        }
		$this->set('checkUser', $checkUser);
        $this->set('TotalProject', $TotalProject);
        $this->set('projects', $projects);
        $this->set('ListUsersActions', $ListUsersActions);
        $this->set('ListUserProjects', $ListUserProjects);
        $this->set('user', $user);
        $this->set('tab_active', 'project');
        $this->set('title', __('Hiệp hội doanh nghiệp nhỏ và vừa'));
    }

    /**
     * Get Project Aplicant
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getProjectApplicantAjax($UserId = 0, &$projects = array(), &$ListUsersActions = array(), &$ListUserProjects = array()) {
        $aRequestData = $this->request->data;
        $PageSize = 3;
	    $Page = (int) (isset($aRequestData['page']) ? $aRequestData['page'] : 1);
        $bAjax = false;
        if (!$UserId) {
            $bAjax = true;
            $UserId = $aRequestData['UserId'];
			
        }

        $this->loadModel('Projects');
        $projects = $this->Projects->find()
                ->contain(['Users', 'Categories', 'Countries', 'States', 'Districts'])
                ->where(['user_id' => $UserId])
                ->limit($PageSize)
                ->page($Page)
                ->toArray();

        $ListProjectIds = array();
        foreach ($projects as $key => $project) {
            $aMoreFilterProject = $this->_addMoreFilterProject($project);
            if ($aMoreFilterProject) {
                $projects[$key] = $aMoreFilterProject;
                $ListProjectIds[] = $project->id;
            }
        }

        $ListUsersActions = array();
        $ListUserProjects = array();
        if ($ListProjectIds) {
            $this->loadModel('UserProjectActions');
            $this->loadModel('UsersProjects');
            $ListUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $ListProjectIds, 'action_type' => 1, 'status' => 1]);
            $ListUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $ListProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
        }

        if ($bAjax) {
            $view = new View($this->request, $this->response, null);
            $view->set('projects', $projects);
            $view->set('ListUsersActions', isset($ListUsersActions) ? $ListUsersActions : []);
            $view->set('ListUserProjects', isset($ListUserProjects) ? $ListUserProjects : []);
            $view->layout = false;
            $this->_data['sHtmlItem'] = $view->render('/Element/Home/item-filter-project');
            $this->_data['page'] = $Page;
            $this->_data['PageSize'] = $PageSize;
            $this->_data['TotalProject'] = count($projects);
            $this->_status = 1;
            $this->responApi($this->_status, $this->_message, $this->_data);
            die();
        } else {
            $this->set('PageSize', $PageSize);
        }
    }

    /**
     * Controller 
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getFollowingApplicant($user_id = 0, $project_id = 0, $role_id = 0) {
        $user = $this->_getUserApplicant($user_id);
        $authUser = $this->Auth->user();
        if (!$user || !$authUser) {
            if (!$this->request->isAjax()) {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            } else {
                $this->_redirectUserIsNotLoginAjax();
            }
        } else {
            $this->_getFollowingFollower($user);
            $this->_validateCollaborator($user_id, $project_id, $role_id);
        }

        $PageSize = 10;
        $Followings = array();
        $LinkApi = ROOT_URL . 'api/usersFollow/showFollowing.json';
        $Params = [
            'user_id' => $user_id,
            'page' => 1,
            'limit' => $PageSize,
        ];
        $response = $this->getDataFromAPI($LinkApi, $Params, true);
        if (isset($response->data->user)) {
            $Followings = $response->data->user;
        }

        if ($this->request->isAjax()) {
            $view = new View($this->request, $this->response, null);
            $view->set('user', $user);
            $view->set('authUser', $authUser);
            $view->set('Followings', $Followings);
            $view->set('PageSize', $PageSize);
            $view->layout = false;
            echo $view->render('/Element/Applicant/content_following');
            die();
        }

        $this->set('TotalProject', $this->_countCompletedCreated($user->id));
        $this->set('user', $user);
        $this->set('Followings', $Followings);
        $this->set('PageSize', $PageSize);
        $this->set('tab_active', 'following');
    }

    /**
     * get See More Following 
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getSeeMoreFollowing() {
        $this->_status = 1;
        $this->_message = '';

        if (!$this->request->isAjax()) {
            $this->_status = 0;
            $this->_message = __('Không thể xử lý yêu cầu !');
        }

        $RequestData = $this->request->data;
        $PageSize = 10;

        if ($this->_status == 1) {
            if (!isset($RequestData['user_id']) || empty($RequestData['user_id'])) {
                $this->_status = 0;
                $this->_message = __('Không thể xử lý yêu cầu !');
            } else {
                $UserId = $RequestData['user_id'];
            }
        }

        if ($this->_status == 1) {
            if (!isset($RequestData['page']) || empty($RequestData['page'])) {
                $this->_status = 0;
                $this->_message = __('Không thể xử lý yêu cầu !');
            } else {
                $Page = $RequestData['page'];
            }
        }

        if ($this->_status == 1) {
            $Followings = array();
            $LinkApi = ROOT_URL . 'api/usersFollow/showFollowing.json';
            $Params = [
                'user_id' => $UserId,
                'page' => $Page,
                'limit' => $PageSize,
            ];
            $response = $this->getDataFromAPI($LinkApi, $Params, true);
            if (isset($response->data->user)) {
                $Followings = $response->data->user;
            }

            $view = new View($this->request, $this->response, null);
            $view->set('Follows', $Followings);
            $view->set('type', 'following');
            $view->set('authUser', $this->Auth->user());
            $view->layout = false;

            $this->_data['sHtmlItem'] = $view->render('/Element/Applicant/item_follow');
            if (count($Followings) >= $PageSize) {
                $this->_data['page'] = $Page + 1;
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * Controller 
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getFollowerApplicant($user_id = 0, $project_id = 0, $role_id = 0) {
        $user = $this->_getUserApplicant($user_id);
        $authUser = $this->Auth->user();
		$block = TableRegistry::get('blocks');
		$user_block = $block->find('all', ['conditions' => ['user_id' =>$user_id]]);	
        if (!$user || !$authUser || empty($user_block )) {
            if (!$this->request->isAjax()) {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            } else {
                $this->_redirectUserIsNotLoginAjax();
            }
        } else {
            $this->_getFollowingFollower($user);
            $this->_validateCollaborator($user_id, $project_id, $role_id);
        }

        $PageSize = 10;
        $Followers = array();
        $LinkApi = ROOT_URL . 'api/usersFollow/showFollower.json';
        $Params = [
            'user_id' => $user_id,
            'type' => 2,
            'page' => 1,
            'limit' => $PageSize,
        ];
        $response = $this->getDataFromAPI($LinkApi, $Params, true);
        if (isset($response->data->user)) {
            $Followers = $response->data->user;
        }

        if ($this->request->isAjax()) {
            $view = new View($this->request, $this->response, null);
            $view->set('user', $user);
            $view->set('authUser', $authUser);
            $view->set('Followers', $Followers);
            $view->set('PageSize', $PageSize);
            $view->layout = false;
            echo $view->render('/Element/Applicant/content_follower');
            die();
        }

        $this->set('TotalProject', $this->_countCompletedCreated($user->id));
        $this->set('user', $user);
        $this->set('PageSize', $PageSize);
        $this->set('Followers', $Followers);
        $this->set('tab_active', 'follower');
    }

    /**
     * get See More Follower
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getSeeMoreFollower() {
        $this->_status = 1;
        $this->_message = '';

        if (!$this->request->isAjax()) {
            $this->_status = 0;
            $this->_message = __('Không thể xử lý yêu cầu !');
        }

        $RequestData = $this->request->data;
        $PageSize = 10;

        if ($this->_status == 1) {
            if (!isset($RequestData['user_id']) || empty($RequestData['user_id'])) {
                $this->_status = 0;
                $this->_message = __('Không thể xử lý yêu cầu !');
            } else {
                $UserId = $RequestData['user_id'];
            }
        }

        if ($this->_status == 1) {
            if (!isset($RequestData['page']) || empty($RequestData['page'])) {
                $this->_status = 0;
                $this->_message = __('Không thể xử lý yêu cầu !');
            } else {
                $Page = $RequestData['page'];
            }
        }

        if ($this->_status == 1) {
            $Followers = array();
            $LinkApi = ROOT_URL . 'api/usersFollow/showFollower.json';
            $Params = [
                'user_id' => $UserId,
                'type' => 2,
                'page' => $Page,
                'limit' => $PageSize,
            ];
            $response = $this->getDataFromAPI($LinkApi, $Params, true);
            if (isset($response->data->user)) {
                $Followers = $response->data->user;
            }

            $view = new View($this->request, $this->response, null);
            $view->set('Follows', $Followers);
            $view->set('type', 'follower');
            $view->set('authUser', $this->Auth->user());
            $view->layout = false;
            $this->_data['sHtmlItem'] = $view->render('/Element/applicant/item_follow');
            if (count($Followers) >= $PageSize) {
                $this->_data['page'] = $Page + 1;
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * add More Filter Project
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _addMoreFilterProject($aRow = null) {
        $aReturn = array();
        if ($aRow) {
            $this->loadModel('ProjectsRoles');
            $this->loadModel('UsersProjects');
            $aReturn = $aRow;

            $iQuantity = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $aRow->id], $aRow);
            $UserProject = $this->UsersProjects->getUserProjectsByOptions(['project_id' => $aRow->id, 'status' => 1, 'type' => 2]);
            $iTotalRoleJoined = count($UserProject);

            $iPercentRoleJoined = 0;
            if ($iTotalRoleJoined < $iQuantity) {
                $iPercentRoleJoined = ($iTotalRoleJoined / $iQuantity) * 100;
            } else {
                $iPercentRoleJoined = (!empty($iTotalRoleJoined)) ? 100 : 0;
            }

            $aReturn['iQuantity'] = (int) $iQuantity;
            $aReturn['iTotalRoleJoined'] = (int) $iTotalRoleJoined;
            $aReturn['iPercentRoleJoined'] = (int) $iPercentRoleJoined;
            return $aReturn;
        }
        return false;
    }

    /**
     * add User Applicant
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _getUserApplicant($UserId) {
        $this->loadModel('Users');
        $user = $this->Users->find('all', [
                    'contain' => ['Roles'],
                    'conditions' => ['id' => $UserId],
                    'fields' => ['id', 'name', 'first_name', 'last_name', 'district_id', 'state_id', 'country_id', 'biography', 'avatar', 'created'],
                ])->first();

        if ($user) {
            $aStates = $this->Users->States->find('all', array(
                        'fields' => array('id', 'name'),
                        'conditions' => array('id' => $user->state_id),
                    ))->first();

            $aCountries = $this->Users->Countries->find('all', array(
                        'fields' => array('id', 'country_name'),
                        'conditions' => array('id' => $user->country_id),
                    ))->first();

            $this->loadModel('UsersWebsites');
            $websites = $this->UsersWebsites->find('all', [
                        'fields' => ['website_id', 'Websites.name'],
                        'contain' => 'Websites',
                        'conditions' => ['user_id' => $UserId]
                    ])->limit(100)->toArray();

            $user['States'] = $aStates;
            $user['Countries'] = $aCountries;
            $user['UsersWebsites'] = $websites;
        }
        return $user;
    }

    /**
     * Cound Completed Project AND Created Project
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _countCompletedCreated($UserId) {
        $Return = array(
            'TotalCompletedProjects' => 0,
            'TotalCreatedProjects' => 0,
            'TotalProject' => 0,
        );

        if ($UserId) {
            $this->loadModel('Projects');
            $Projects = $this->Projects->find()->contain(['Users'])->where(['user_id' => $UserId])->toArray();
            if (isset($Projects) && !empty($Projects)) {
                foreach ($Projects as $key => $Project) {
                    $Return['TotalCreatedProjects'] += 1;
                    if ($Project['is_completed'] == 1){
                        $Return['TotalCompletedProjects'] += 1;
                    }
                }
            }
        }
        $Return['TotalProject'] = $Return['TotalCompletedProjects'] + $Return['TotalCreatedProjects'];
        return $Return;
    }

    /**
     * get Folowing AND Folower 
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _getFollowingFollower(&$user) {
        $UserId = (isset($user->id) ? $user->id : 0);
        $followingsTable = TableRegistry::get('Followings');
        $TotalFollowingFollower = $followingsTable->getTotalFollowingFollower($UserId);
        $user['TotalFollowing'] = $TotalFollowingFollower['TotalFollowing'];
        $user['TotalFollower'] = $TotalFollowingFollower['TotalFollower'];
        $user['followers'] = $TotalFollowingFollower['followers'];
        $user['followings'] = $TotalFollowingFollower['followings'];
        $user['TotalBlock'] = TableRegistry::get('Blocks')->TotalBlockUser($UserId);
        return $this;
    }

    /**
     * get Work Applicant
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _getWorkApplicant(&$user) {
        $usersWorks = TableRegistry::get('UsersWorks');
        $works = $usersWorks->find('all', [
                    'fields' => ['id', 'work_url', 'work_video_id', 'work_thumbnail', 'work_title'],
                    'conditions' => ['user_id' => $user->id]
                ])->toArray();
        $user['userWorks'] = $works;
        return $this;
    }

}

?>