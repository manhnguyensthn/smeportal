<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Query;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\View\View;

/**
 * UsersRoles Controller
 *
 * @property \App\Model\Table\UsersRolesTable $UsersRoles
 */
class UsersSignupNewsletterController extends AppController
{
	public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['actionSignupNewsletter']);
    }


	/**
     * get action Signup Newsletter
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function actionSignupNewsletter(){
        $this->_status = 1;
        $this->_message = '';
        $RequestData = $this->request->data;
        if(!isset($RequestData['email']) || empty($RequestData['email'])){
            $this->_status  = 0;
            $this->_message = __('Invalid email format. Must be, ex: john.smith@gmail.com, nakatajim@fuji.co.jp.');
        }else{
        	$Email = trim($RequestData['email']);
        }

        if($this->_status == 1){
        	if(!preg_match("/^[0-9a-zA-Z]([\-+.\w]*[0-9a-zA-Z]?)*@([0-9a-zA-Z\-.\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,}$/", $Email)){
        		$this->_status  = 0;
	            $this->_message = __('Invalid email format. Must be, ex: john.smith@gmail.com, nakatajim@fuji.co.jp');
        	}
        }

        $UsersSignupNewsletterTable = TableRegistry::get('UsersSignupNewsletter');
        if($this->_status == 1){
        	if($UsersSignupNewsletterTable->checkExitEmail($Email)){
        		$this->_status  = 0;
	            $this->_message = __('This email has been registered.');
        	}
        }

        if($this->_status == 1){
        	$UsersSignupNewsletter = $UsersSignupNewsletterTable->newEntity();
        	$UsersSignupNewsletter->email = $Email;
            $UsersSignupNewsletter->created = date('Y-m-d H:i:s');
			if ($UsersSignupNewsletterTable->save($UsersSignupNewsletter)) {
	            $this->_message = __('Welcome to We The Project! <br> Thank you for signing up for newsletter.');
			}else{
				$this->_message = __('Signup failed, please try again.');
			}
        }
        
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }
}