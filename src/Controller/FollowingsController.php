<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Network\Http\Client;

class FollowingsController extends AppController {
	public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function followingAjax(){
    	$this->_status = 1;
    	$UserCurrent = $this->Auth->user();
    	if(!$UserCurrent){
    		$this->_status = 0;
            $this->_message = __('Unable to process your request.');
    	}else{
    		$iUserId = $UserCurrent['id'];
    		$TokenTable = TableRegistry::get('Tokens');
            $Token = $TokenTable->find('all', ['conditions' => ['user_id' => $iUserId]])->first()->toArray();

    		if(!isset($Token['token']) || empty($Token['token'])){
    			$this->_status = 0;
	            $this->_message = __('Invalid Token');
    		}else{
    			$Token = $Token['token'];
    		}
    	}

    	if($this->_status == 1){
    		$RequestData = $this->request->data;
	    	if(!isset($RequestData['ItemId']) || empty($RequestData['ItemId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$ItemId = $RequestData['ItemId'];
	    	}
    	}

    	if($this->_status == 1){
    		if($iUserId == $ItemId){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}
    	}
    	
    	if($this->_status == 1){
    		if(!isset($RequestData['ConnectionId']) || empty($RequestData['ConnectionId'])){
	    		$this->_status = 0;
	            $this->_message = __('Unable to process your request.');
	    	}else{
	    		$ConnectionId = $RequestData['ConnectionId'];
	    	}
    	}


    	if($this->_status == 1){
    		$blocksTable = TableRegistry::get('Blocks');
	        $isBlock = $blocksTable->find()
	            ->where(['user_id' => $iUserId, 'blocked_id' => $ItemId])
	            ->orWhere(['user_id' => $ItemId, 'blocked_id' => $iUserId])
	            ->count();
	        if($isBlock){
				$this->_status  = 0;
				$this->_message = __('You have already blocked this user before!');
	        }
    	}

    	if($this->_status == 1){
			$LinkApi = ROOT_URL . 'api/usersFollow/followUserAction.json';
			$type = (isset($RequestData['sType']) && $RequestData['sType'] == 'follower') ? 0 : 1;
			$Params   = [
				'token'             => $Token,
				'user_id'           => $ItemId,
				'type'              => $type,
				'connection'        => $ConnectionId,
				'collaborator_name' => '',
			];

			$response = $this->getDataFromAPI($LinkApi, $Params, true);
			if ($response) {
	            if($response->status == 1){
	            	$this->_status = $response->status;
	            	$this->_message = $response->message;

					$followingsTable = TableRegistry::get('Followings');
					$TotalFollow = $followingsTable->getTotalFollowingFollower($ItemId);
					$response->data->TotalFollow = $TotalFollow;

					$TotalFollowUserCurrent = $followingsTable->getTotalFollowingFollower($iUserId);
					$TotalFollowUserCurrent['iUserId'] = $iUserId;
					$response->data->TotalFollowUserCurrent = $TotalFollowUserCurrent;

	            	$this->_data = $response->data;
	            }else{
	            	$this->_status = 0;
		            $this->_message = $response->message;
	            }
	        }else{
	        	// $this->_status = 0;
	         	// $this->_message = __('Unable to process your request.');
	        }
    	}
    	$this->responApi($this->_status, $this->_message, $this->_data);
    	die();
    }
}
?>