<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends ApiController
{

    public $components = array('RequestHandler');

    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function show()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $conditions = [
                    'user_id' => $token->user_id,
                    'project_id' => $data['project_id']
                ];
                $notifications = $this->Notifications->find('all', ['conditions' => $conditions, 'limit' => 10, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']]);
                if (!empty($notifications)) {
                    $read = 0;
                    $this->_data['notification'] = [];
                    $this->_status = 1;
                    $this->_message = '';

                    $this->loadModel('Users');
                    foreach ($notifications as $notification) {
                        if ($notification->read_flg == 0) {
                            $read++;
                        }
                        $user = $this->Users->get($notification->action_user_id, ['fields' => ['id', 'name']])->toArray();
                        if (!empty($user)) {
                            $this->_data['notification'] = [
                                'id' => $notification->id,
                                'message' => $this->getTypeNotificationContent($user->name, $notification->action_type, $notification->option_data),
                                'read' => $notification->read_flg,
                                'created' => $notification->created,
                                'msg_not_read' => $read
                            ];
                        }
                    }
                }
            } else {
                $this->_status = 0;
                $this->_message = __('Invalid Token');
                $this->_data = [];
            }
        } else {
            $this->_status = 0;
            $this->_message = __('You dont permission to access.');
            $this->_data = [];
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }

    public function getListNotification()
    {
        $this->_status = 1;
        if ($this->request->is('post')) {
            $RequestData = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $RequestData['token']],
            ])->first();
            $limit = 6;
            if (isset($RequestData['limit']) && !empty($RequestData['limit'])) {
                $limit = ((int)$RequestData['limit'] > 0 ? (int)$RequestData['limit'] : 6);
            }

            $page = 1;
            if (isset($RequestData['page']) && !empty($RequestData['page'])) {
                $page = (int)$RequestData['page'];
            }

            if ($token) {
                $NotificationsTable = TableRegistry::get('Notifications');
                $ListNotifications = $NotificationsTable->find('all', ['conditions' => ['user_id' => $token->user_id], 'limit' => $limit, 'page' => $page, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']])->toArray();
            } else {
                $this->_status = 0;
                $this->_message = __('Invalid Token');
            }

            if ($this->_status == 1) {
                $UsersTable = TableRegistry::get('Users');
                $Notifications = array();
                if (isset($ListNotifications) && $ListNotifications) {
                    foreach ($ListNotifications as $key => $Notification) {
						$data_role = json_decode($Notification->option_data);
						$role = '';	
						if(isset($data_role->role_id))
						{
						  $role_id = $data_role->role_id;
						  $this->loadModel('Roles');
						  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
							foreach($roles as $rol)
							{
								$role = $rol->role;
							}
						}
                        $user = $UsersTable->get($Notification->action_user_id, ['fields' => ['id', 'name']])->toArray();
                        $Notifications[$key]['id'] = $Notification->id;
                        $Notifications[$key]['project_id'] = $Notification->project_id;
                        $Notifications[$key]['option_data'] = json_decode($Notification->option_data);
                        $Notifications[$key]['action_type'] = $Notification->action_type;
                        $Notifications[$key]['action_user_id'] = $Notification->action_user_id;
                        $Notifications[$key]['created'] = $Notification->created;
                        $Notifications[$key]['time_text'] = $this->getTimeAgo($Notification->created);
                        $Notifications[$key]['read_flg'] = $Notification->read_flg;
                        $Notifications[$key]['message'] = $this->getTypeNotificationContent($user['name'], $Notification->action_type, $Notification->option_data,$role);
                    }
                }
                $this->_data['Notifications'] = $Notifications;
            }
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }

    // Coder: Luong Pham
    // Date: 09-03-2017
    // Function: get project notification
    public function getSpecListNotification()
    {
        $this->_status = 1;
        if ($this->request->is('post')) {
            $RequestData = $this->request->data;
            $project_id = $RequestData['project_id'];
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $RequestData['token']],
            ])->first();

            if ($token) {
                $projectTable = TableRegistry::get('Projects');
                $NotificationsTable = TableRegistry::get('Notifications');

                $project = $projectTable->find()
                    ->where(['id' => $project_id])
                    ->first();

                $ListNotifications = $NotificationsTable->find()
                    ->where(['user_id' => $token->user_id,'project_id' => $project['id']])
                    ->order(['read_flg' => 'ASC', 'created' => 'DESC'])
                    ->all();
            } else {
                $this->_status = 0;
                $this->_message = __('Invalid Token');
            }

            if ($this->_status == 1) {
                $UsersTable = TableRegistry::get('Users');
                $Notifications = array();
	
                if (isset($ListNotifications) && $ListNotifications) {
                    foreach ($ListNotifications as $key => $Notification) {
						$data_role = json_decode($Notification->option_data);
						$role = '';	
						if(isset($data_role->role_id))
						{
						  $role_id = $data_role->role_id;
						  $this->loadModel('Roles');
						  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
							foreach($roles as $rol)
							{
								$role = $rol->role;
							}
						}
                        $user = $UsersTable->get($Notification->action_user_id, ['fields' => ['id', 'name']])->toArray();
                        $Notifications[$key]['id'] = $Notification->id;
                        $Notifications[$key]['project_id'] = $Notification->project_id;
                        $Notifications[$key]['option_data'] = json_decode($Notification->option_data);
                        $Notifications[$key]['action_type'] = $Notification->action_type;
                        $Notifications[$key]['action_user_id'] = $Notification->action_user_id;
                        $Notifications[$key]['created'] = $Notification->created;
                        $Notifications[$key]['time_text'] = $this->getTimeAgo($Notification->created);
                        $Notifications[$key]['read_flg'] = $Notification->read_flg;
                        $Notifications[$key]['message'] = $this->getTypeNotificationContent($user['name'], $Notification->action_type, $Notification->option_data,$role);
                    }
                }
                $this->_data['Notifications'] = $Notifications;
            }
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }

    public function getTypeNotificationContent($actionName = null, $type = 1, $option_data = null, $role = '')
    {
		$aRequestData = $this->request->data;
        $result = '';
        $optionsArr = json_decode($option_data);
		if($aRequestData['language'] == 'ja')
		{
        switch ($type) {
		case 1:
                // Request want to join project
                $message = __('ユーザー %s さん は %s として プロジェクト %s に 参加を希望しています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName,$role, $project->title);
                break;
            case 2:
                // alert comment on project
                $message = __('%s は プロジェクト %s に コメントしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s has applied for your project');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $message = __('%s さん  が プロジェクト %s に %s として 参加しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
		
            case 5:
                // alert accept join project
                $message = __('プロジェクト管理者 %s さん が、 あなた を さん が、 あなた を %s として プロジェクト %s に 参加することを認証しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName,$role,$project->title);
                break;
            case 6:
                // alert reply comment
                $message = __('%s さん が、プロジェクト %s の コメント に 返信しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $message = __('%s さん が、プロジェクト %s に イイネ！しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert follow project
                $message = __('%s さん は、プロジェクト %s を フォローしています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $message = __('%s さん が プロジェクト %s に ファイル を アップロードしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $message = __('プロジェク管理者 %s さん が プロジェクト %s を 更新しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $message = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 12: // N has followed you
                $message = __('%s has followed you');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('あなたは、プロジェクト %s  の %s に なるように提案されています。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $project->title, $role);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s さん は、プロジェクト %s  の %s に なるように提案されています');
                $projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s さん が、 プロジェクト %s の チャットで 新しいメッセージ を あなた に 送信しました。');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s さん が 新しいプロジェクト %s を 作成しました');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $message = __('%s  さん が プロジェクト %s に 参加するよう に あなた を 招待しました');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 18: // B has update project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('プロジェク管理者 %s さん が プロジェクト %s を 更新しました。');
                $result = sprintf($message, $actionName, $project->title);
                break;	
			case 19: // B has asset project X
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$message = __('%s has asset project %s');
				$result = sprintf($message, $actionName, $project->title);
			break;	
			case 20: // B has add  
				$message = __('プロジェクト管理者 %s さん が、プロジェクト %s の 新しい %s を 追加しました。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $actionName, $project->title, $role);
				break; }
		}
		else
		{
		switch ($type) {
            case 1:
                // Request want to join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s muốn tham gia doanh nghiệp %s với vai trò %s');
                $result = sprintf($message, $actionName, $project->title,$role);
                break;
            case 2:
                // alert comment on project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s đã bình luận về doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s has applied for your project');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has joined on project %s as %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 5:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('Chủ doanh nghiệp %s đã đồng ý thêm bạn %s với vai tro %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 6:
                // alert reply comment
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has replied your comment on project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s đã thích doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s đã theo dõi doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $projectTable = TableRegistry::get('Projects');
                $message = __('%s has uploaded a file on project %s');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $projectTable = TableRegistry::get('Projects');
                $message = __('Project owner %s has updated project %s');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $message = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('%s has followed you');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('You are offered to be %s in the project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                    $RolesTable = TableRegistry::get('Roles');
                    $Role = $RolesTable->get($optionsArr->role_id);
                    if (isset($Role->role) && !empty($Role->role)) {
                        $RoleName = $Role->role;
                    }
                }
                if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($optionsArr->project_id);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $RoleName, $ProjectName);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s has been offered %s in project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                    $RolesTable = TableRegistry::get('Roles');
                    $Role = $RolesTable->get($optionsArr->role_id);
                    if (isset($Role->role) && !empty($Role->role)) {
                        $RoleName = $Role->role;
                    }
                }
                if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($optionsArr->project_id);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $actionName, $RoleName, $ProjectName);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s đã nhắn tin cho bạn trong doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has created new project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has invited you join a project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 18: // Update project
                $projectUpdateTable = TableRegistry::get('ProjectUpdates');
                $projectUpdate = $projectUpdateTable->get($optionsArr->project_id);
                $message = __('%s has update project %s');
                $result = sprintf($message, $actionName, $projectUpdate->title);
                break;
            case 19: // B has asset project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has asset project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 20: // B has add  
				$message = __('Project owner %s has added new %s of %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;	

            case 21: // new request 
                $message = __('Chủ Doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(21,%s);" >%s</a> đã gửi yêu cầu mới %s từ %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);

                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->get($optionsArr->request_id);

                $result = sprintf($message,$user_id, $actionName, $request->title, $project->title);
                break;
            case 22: // duyet  
                $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(22,%s);" >%s</a> đã duyệt yêu cầu %s của %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                break;
            case 23: //tu choi  
                $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(23,%s);" >%s</a> đã từ chối y/c %s của %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                break;
            case 24: //gui tat ca  
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);

                $userTable = TableRegistry::get('Users');
                $user = $userTable->get($optionsArr->user_id);

                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->get($optionsArr->request_id);

                $content = substr($request->title, 0,50) . '...';
                if($user->admin == 0){
                    $message = __('Chủ DN <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> đã gửi y/c %s %s'); 
                    $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                } else {
                    $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> thông báo: %s');
                    $result = sprintf($message,$user_id, $actionName, $content);
                }

                break;
            case 25: //gui vai trò  
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);

                $userTable = TableRegistry::get('Users');
                $user = $userTable->get($optionsArr->user_id);

                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->get($optionsArr->request_id);

                $content = substr($request->title, 0,50) . '...';
                if($user->admin == 0){
                    $message = __('Chủ DN <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> đã gửi y/c %s %s'); 
                    $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                } else {
                    $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> thông báo: %s');
                    $result = sprintf($message,$user_id, $actionName, $content);
                }

                break;
        }
		}
        return $result;
    }

    private function _getNumberCollaboratorByRole($list = [], $project_role_id = 0) {
        $counter = 0;
        foreach ($list as $row) {
            if ($row->project_role_id == $project_role_id) {
                $counter++;
            }
        }
        return $counter;
    }

    // Coder: Luong Pham
    // Date: 08-03-2017
    // Function: update read_flg notification
    public function updateNotification()
    {
        $this->autoRender = False;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokeTable = TableRegistry::get('Tokens');
            $token = $tokeTable->find('all', [
                'condition' => ['token' => $data['token']]
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['notification_id']) && !empty($data['notification_id']) && isset($data['project_id']) && !empty($data['project_id'])) {
                    $notificationTable = TableRegistry::get('Notifications');
                    $notificationTable->updateAll(['read_flg' => 1], ['id' => $data['notification_id']]);
                    $this->responseApi(1, 'Update successfully', $notificationTable);
                } else {
                    $this->responseApi(0, 'Update false');
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    // Coder: Luong Pham
    // Date: 09-03-2017
    // Function: update read_flg notification
    public function acceptUserWantToJoinProject()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $notification_id = $data['notification_id'];
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'condition' => ['token' => $data['token']]
            ])->first();

            if (!empty($token)) {
                $notificationTable = TableRegistry::get('Notifications');
                $notification = $notificationTable->find('all', ['conditions' => ['id' => $notification_id]])->first();
                $notification = $notificationTable->patchEntity($notification, ['read_flg' => 1]);
                if ($notificationTable->save($notification)) {
                    $userTable = TableRegistry::get('Users');
                    $projectRoleTable = TableRegistry::get('ProjectsRoles');
                    $userRequest = $userTable->find('all', ['conditions' => ['id' => $notification->action_user_id]])->first();;
                    $optionData = json_decode($notification->option_data);
//                    print_r($notification);
//                    exit();
                    if (!empty($optionData->project_role_id)) {
                        $roleInfo = $projectRoleTable->find('all', ['conditions' => ['id' => $optionData->project_role_id]])->first();
                    } else {
                        $roleInfo = $projectRoleTable->find('all', ['conditions' => ['project_id' => $optionData->project_id, 'role_id' => $optionData->role_id]])->first();
                    }			
					 if (!empty($roleInfo)) {
                        $userProjectTable = TableRegistry::get('UsersProjects');
                        $collaborators = $userProjectTable->find('all', ['conditions' => ['project_id' => $optionData->project_id, 'UsersProjects.type' => 2, 'user_id !=' => $userRequest['id'], 'UsersProjects.status' => 1]])->contain(['Users'])->toArray();
                        $roleCounter = $this->_getNumberCollaboratorByRole($collaborators, $roleInfo->id);
                        if ($roleCounter < $roleInfo->quantity) {
							$role = '';	
							if(isset($optionData->role_id) && !empty($optionData->role_id))
							{
							  $role_id = $optionData->role_id;
							  $this->loadModel('Roles');
							  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
								foreach($roles as $rol)
								{
									$role = $rol->role;
								}
							}							
							$this->sendNotificationForJoinProject($optionData->project_id, 4,$userRequest['id'],$optionData->role_id);
                            $this->sendNotificationForUserFollow(4, ['project_id' => $optionData->project_id, 'user_action_id' => $userRequest['id'], 'action_type' => 4], $userRequest['id'],$role);
                            $projectRoleTable->updateAll(['is_read' => 0], ['project_id' => $optionData->project_id, 'role_id' => $optionData->role_id]);
                            foreach ($collaborators as $row) {
                                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                                $userNotification = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row->user_id]])->first();
                                if (isset($userNotification->project_update_email_status) && $userNotification->project_update_email_status == 1) {
                                    $template = 'notification';
                                    $from = [EMAIL_LOGIN => __('We The Projects')];
                                    $subject = __('[SME] Notification on SME');

                                    // GET CONTENT MAIL
                                    $name = $userRequest->first_name . ' ' . $userRequest->last_name;
                                    $contentMail = $this->getContentNotifySendMail($name, 4, $notification->option_data);
                                    // END: GET CONTENT MAIL

                                    if (!$this->sendMail($template, $from, $row['user']['email'], ['first_name' => $row['user']->first_name, 'last_name' => $row['user']->last_name, 'notification_content' => $contentMail], $subject)) {
                                        $this->responseApi(0,'Send mail to creator fail.');
                                    }
                                }
                                if (isset($userNotification->project_update_mobile_status) && $userNotification->project_update_mobile_status == 1) {
                                    $notificationData = $notificationTable->newEntity();
                                    $newNotification = $notificationTable->patchEntity($notificationData, ['user_id' => $row->user_id, 'action_user_id' => $userRequest['id'], 'option_data' => $notification->option_data, 'project_id' => $optionData->project_id, 'action_type' => 4, 'read_flg' => 0, 'created' => date('Y-m-d H:i:s')]);
                                    if (!$notificationTable->save($newNotification)) {
                                        $this->responseApi(0,'Add notification fail.');
                                    }
                                }
                            }
                            // End send notification for collaborator
                            $actionUser = $userTable->get($notification->action_user_id);
                            $notificationData = $notificationTable->newEntity();
                            $newNotification = $notificationTable->patchEntity($notificationData, ['user_id' => $actionUser->id, 'action_user_id' => $userRequest['id'], 'option_data' => $notification->option_data, 'action_type' => 5, 'project_id' => $optionData->project_id, 'read_flg' => 0, 'created' => date('Y-m-d H:i:s')]);
                            if (!$notificationTable->save($newNotification)) {
                                $this->responseApi(0,'Add notification fail.');
                            }
							else
							{
							$userRe = $userTable->get($notification->user_id);	
							$userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $notification->action_user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');
							$actionType = 5;
                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userRe['first_name'] . ' ' . $userRe['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $notification->option_data);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);  
							}
                            // Change status user project
                            $option_data = json_decode($notification->option_data);
                            if (isset($option_data->role_id) && isset($option_data->project_id)) {
                                $userProject = $userProjectTable->find('all', ['conditions' => ['user_id' => $notification->action_user_id, 'project_id' => $option_data->project_id, 'role_id' => $option_data->role_id]])->first();
                                $userProject->status = 1;
                                if (!$userProjectTable->save($userProject)) {
                                    $this->responseApi(0,'Change status fail.');
                                }
                            }
                            $this->responseApi(1,'Bạn vừa đồng ý yêu cầu tham gia doanh nghiệp.');
                            die;
                        } else {
                            $this->responseApi(0, 'This role is filled!');
                            die;
                        }
                    } else {
                        $this->responseApi(0, 'Not match role on database');
                        die;
                    }
                } else {
                    $this->responseApi(0, 'Could not accepted a collaborator to join your project.');
                    die;
                }
            } else {
                $this->responseApi(1032);
            }
        } else {
            $this->responseApi(1031);
        }
    }

    // Coder: Luong Pham
    // Date: 10-03-2017
    // Function: send notification for follow user
//    public function sendNotificationForUserFollow()


    // Coder: Luong Pham
    // Date: 10-03-2017
    // Function: send notification invite request
    public function sendInviteRequest()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $project_id = $data['project_id'];
            $user_id = $data['user_id'];
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', ['conditions' => ['token' =>$data['token']]])->first();
            if (!empty($token)) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                $userTable = TableRegistry::get('Users');
                $userRequest = $userTable->find('all', ['condition' => ['id' => $token->user_id]])->first();
                $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $user_id]])->first();
                $optionData = ['project_id' => $project_id, 'user_invite' => $token->user_id];
                $actionType = 17;
                if (!empty($userNotificationSetting) && $userNotificationSetting->private_message_mobile_status == 1) {
                    // add notification on web			 
				    $notificationTable = TableRegistry::get('Notifications');
                    $notificationData = $notificationTable->newEntity();
                    $notificationData->user_id = $user_id;
                    $notificationData->action_user_id = $token->user_id;
                    $notificationData->project_id = $project_id;
                    $notificationData->option_data = json_encode($optionData);
                    $notificationData->action_type = $actionType;
                    $notificationData->created = date('Y-m-d H:i:s');
                    $notificationTable->save($notificationData);
                }
                if (!empty($userNotificationSetting) && $userNotificationSetting->private_message_email_status == 1) {
                    // send notification via mobile
//                    $this->loadModel('Users');
//                    $userInvition = $this->Users->get($user_id);
                    $userInvition = $userTable->get($user_id);
                    $template = 'notification';
                    $from = [EMAIL_LOGIN => __('We The Projects')];
                    $subject = __('[SME] Notification on SME');

                    // GET CONTENT MAIL
                    $contentMail = __('Send  notification content');
                    $name = $userRequest['first_name'] . ' ' . $userRequest['last_name'];
                    $contentMail = $this->getContentNotifySendMail($name, $actionType, json_encode($optionData));
                    // END: GET CONTENT MAIL

                    $this->sendMail($template, $from, $userInvition['email'], ['first_name' => $userInvition['first_name'], 'last_name' => $userInvition['last_name'], 'notification_content' => $contentMail], $subject);
                }
                $this->responseApi(1,__('Gửi lời mơ!'));
                die;
            } else {
                $this->responseApi(1031);
            }
        }
    }
}