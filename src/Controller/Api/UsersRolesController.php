<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * UsersRoles Controller
 *
 * @property \App\Model\Table\UsersRolesTable $UsersRoles
 */
class UsersRolesController extends ApiController
{

    public $components = array('RequestHandler', 'PushNotification');

    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function findUserByKeyword()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('UsersRoles');
                $params = [];
                if (isset($data['role_id']) && !empty($data['role_id'])) {
                    $params['role_id'] = $data['role_id'];
                }
                if (isset($data['keyword']) && !empty($data['keyword'])) {
                    $params['Users.name LIKE'] = $data['keyword'] . '%';
                }

                if (isset($data['limit']) && !empty($data['limit'])) {
                    $limit = $data['limit'];
                } else {
                    $limit = 20;
                }
                if (isset($data['page']) && !empty($data['page'])) {
                    $page = $data['page'];
                } else {
                    $page = 1;
                }
                $offset = ($page - 1) * $limit;
                //get list user roles
                $listUserRoles = $this->UsersRoles->findUserRolesByKeyword($params, $limit, $offset);
                if (!empty($listUserRoles)) {
                    foreach ($listUserRoles as $key => $users) {
                        $this->_data[] = $users['Users'];
                    }
                }
                $this->_status = 1;
                $this->responseApi($this->_status, '', $this->_data);
            } else {
                $this->responseApi(1031);
            }
        }
    }
	
	//Nam 
	 public function findApiUserByKeyword()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('UsersRoles');
                $params = [];
                if (isset($data['role_id']) && !empty($data['role_id'])) {
                    $params['role_id'] = $data['role_id'];
                }
                if (isset($data['keyword']) && !empty($data['keyword'])) {
                    $params['Users.name LIKE'] = $data['keyword'] . '%';
                }

                if (isset($data['limit']) && !empty($data['limit'])) {
                    $limit = $data['limit'];
                } else {
                    $limit = 20;
                }
                if (isset($data['page']) && !empty($data['page'])) {
                    $page = $data['page'];
                } else {
                    $page = 1;
                }
                $offset = ($page - 1) * $limit;
                //get list user roles
                $listUserRoles = $this->UsersRoles->findUserRolesByKeyword($params, $limit, $offset);
				$blocksTable = TableRegistry::get('Blocks');				
				if (!empty($listUserRoles)) {
                    foreach ($listUserRoles as $key => $users) {
							$block = $blocksTable->find()
						->where(['blocked_id' => $token->user_id, 'user_id' => $users['Users']['id']])
						->first();
						$blockuser = $blocksTable->find()
						->where(['user_id' => $token->user_id, 'blocked_id' => $users['Users']['id']])
						->first();
						 if(empty($blockuser) && empty($block))
							{
								$users['Users']->can_view = 1;
								$this->_data[] = $users['Users'];
							}
							else
							{
								$users['Users']->can_view = 0;
								$this->_data[] = $users['Users'];
							}
                    }
                }	
                $this->_status = 1;
                $this->responseApi($this->_status, '', $this->_data);
            } else {
                $this->responseApi(1031);
            }
        }
    }
	//End Nam

    public function joinProjectRole()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['message'])) {
                $this->responseApi(0, __('Message is required.'), []);
                exit;
            }
            $usersProjects = TableRegistry::get('UsersProjects');
            $chatMessages = TableRegistry::get('chatMessages');

            $tokenTable = TableRegistry::get('Tokens');
            $blocksTable = TableRegistry::get('Blocks');

            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
				$this->loadModel('Projects');
                $project = $this->Projects->find('all', ['conditions' => ['id' =>$data['project_id']]])->first();      
                $block = $blocksTable->find()
                    ->where(['blocked_id' => $token->user_id, 'user_id' => $project->user_id])
                    ->first();
				$blockuser = $blocksTable->find()
                    ->where(['user_id' => $token->user_id, 'blocked_id' => $project->user_id])
                    ->first();	
                if (!empty($block) || !empty($blockuser)) {
                    $this->responseApi(0, __('You have been blocked'));
                } else {
                    if (isset($data['project_role_id'])) {
						$role_id = $data['project_role_id'];
                        $this->loadModel('ProjectsRoles');
                        $projectRole = $this->ProjectsRoles->find('all', ['conditions' => ['project_id' =>$data['project_id'],'id'=>$data['project_role_id']]])->first();      
						if (!empty($projectRole)) {
                            // Check request role exits
                            $userProjectRoles = $usersProjects->find('all', ['conditions' => ['user_id' => $token->user_id, 'project_role_id' => $data['project_role_id']]])->toArray();
                            if (empty($userProjectRoles)) {
                                // Add request join role
                                $this->loadModel('Users');
                                $user = $this->Users->get($token->user_id, ['fields' => ['id', 'name', 'first_name', 'last_name', 'avatar', 'biography']])->toArray();
                                $userProjectsData = $usersProjects->newEntity();
                                $userProjectsData->user_id = $token->user_id;
                                $userProjectsData->project_id = $projectRole->project_id;
                                $userProjectsData->project_role_id = $data['project_role_id'];
                                $userProjectsData->role_id = $projectRole->role_id;
                                $userProjectsData->name = !empty($user['name']) ? $user['name'] : $user['first_name'] . ' ' . $user['last_name'];
                                $userProjectsData->user_picture = $user['avatar'];
                                $userProjectsData->biography = $user['biography'];
                                $userProjectsData->message_content = isset($data['message']) ? $data['message'] : '';
                                $userProjectsData->created = date('Y-m-d H:i:s');
                                $userProjectsData->status = 0;
                                if ($usersProjects->save($userProjectsData)) {
                                    // Send notification for creator
                                    $projectsTable = TableRegistry::get('Projects');
                                    $projectCreator = $projectsTable->find('all', ['conditions' => ['Projects.id' => $projectRole->project_id]])->contain(['Users'])->first();
                                    if (!empty($projectCreator)) {
                                        $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                                        $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $projectCreator->user_id]])->first();
                                        if (!empty($userNotificationSetting)) {
                                            if ($userNotificationSetting->member_join_project_email_status == 1) {
                                                // User setting receil notification via email
                                                $template = 'notification';
                                                $from = [EMAIL_LOGIN => __('We The Projects')];
                                                $subject = __('[SME] Notification on SME');
                                                $name = $user['first_name'] . ' ' . $user['last_name'];
                                                $optionData = ['project_id' => $projectRole->project_id, 'project_role_id' => $data['project_role_id']];
                                                $contentMail = $this->getContentNotifySendMail($name, 1, json_encode($optionData));
                                                if (!$this->sendMail($template, $from, $projectCreator['user']['email'], ['first_name' => $projectCreator['user']['first_name'], 'last_name' => $projectCreator['user']['last_name'], 'notification_content' => $contentMail], $subject)) {
                                                    $this->responseApi(0, __('Email could not be send at this time.'));
                                                }
                                             }
                                            if ($userNotificationSetting->member_join_project_mobile_status == 1) {
                                                $notificationsTable = TableRegistry::get('Notifications');
                                                $notificationData = $notificationsTable->newEntity();
                                                $notificationData->user_id = $projectCreator->user_id;
                                                $notificationData->project_id = $projectCreator->id;
                                                $notificationData->action_user_id = $user['id'];
                                                $notificationData->option_data = json_encode(['project_id' => $projectCreator->id, 'role_id' => $projectRole->role_id, 'project_role_id' => $data['project_role_id']]);
                                                $notificationData->action_type = 1;  // Type of notification join project
                                                $notificationData->read_flg = 0;
                                                $notificationData->created = date('Y-m-d H:i:s');
                                                if (!$notificationsTable->save($notificationData)) {
                                                    $this->responseApi(0, __('Can not save notification to creator. Please try again laster.'), []);
                                                } else {
                                                    $collaboratorName = !empty($user['name']) ? $user['name'] : $user['first_name'] . ' ' . $user['last_name'];
//                                        $message = $collaboratorName . ' wants to join the ' . $projectCreator->title . ' project. Please, Confirm?';
                                                    //push notification for project owner
//                                        if (!empty($projectCreator['user']['device_token'])) {
//                                            $this->PushNotification->send($projectCreator['user']['device_token'], $message);
//                                        }
                                                }
                                            }

                                            $this->responseApi(1, __('You just joined a role, please wait for confirming.'), []);
                                        } else {
                                            $this->responseApi(0, __('Can not send notification to creator. Please try again laster.'), []);
                                        }
                                    }
                                } else {
                                    // Add request fail
                                    $this->responseApi(1030);
                                }
                            } else {
                                // Request join exits
                                $this->responseApi(0, __('Bạn vừa đăng ký một vị trí, vui lòng đợi xác nhận.'), []);
                            }
                        } else {
                            $this->responseApi(1032);
                        }
                    } else {
                        $this->responseApi(1032);
                    }
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    public function acceptUserWantToJoinProject() {
        $this->autoRender = False;
        if ($this->request->is('post')){
            $data = $this->request;
            $tokenTable = TableRegistry::get('Tokens');

            $token = $tokenTable->find('all', [
                'condition' => ['token' => $data['user_id']]
            ])->first();
        }
    }

}
