<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class RolesController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
    }

    public function getRoles() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                $this->loadModel('Projects');
                $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $data['project_id']]])->contain([ 'Users'])->first();
                $this->loadModel('UsersProjects');
                $userProjects = $this->UsersProjects->getUserProjectsByOptions(['project_id'=>$data['project_id'],'type'=>2]);
                if (!empty($project)) {
                    $this->loadModel('ProjectsRoles');
                    $projectRoles = $this->ProjectsRoles->getProjectRoles(['project_id' => $project['id']]);
                    $this->_status = 1;
                    if(!empty($projectRoles)){
                        foreach ($projectRoles as $roles){
                            $memberJoinedCounter = $this->getRoleOpenInList($userProjects,$project['id'],$roles['id']);
                            $data = [
                                'id'  => $roles['id'],
                                'title' => $roles['role']['role'],
                                'description'   => $roles['description'],
                                'charactor_name' => ($roles['role']['role'] === 'Actor') ? $roles['charactor_name'] : '',
                                'active'=> ($memberJoinedCounter < $roles['quantity']) ? 1: 0,
								'filledRole'=> ($memberJoinedCounter < $roles['quantity']) ? 0: 1
                            ];
                            if ($this->_check_member_join_role($userProjects,$roles['id'],$token->user_id)){
                                $data['active'] = 0;
                            }
                            $this->_data[] = $data;
                        }
                    }
                    
                    $this->responseApi($this->_status, '', $this->_data);
                }else{
                    $this->responseApi(1034);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }
    
    // Coder: Giang Dien
    // Date: 07-02-2017
    // Function: check member join role
    private function _check_member_join_role($listUsers = [],$project_role_id = 0,$userId = 0){
        foreach ($listUsers as $row){
            if ($row['user_id'] == $userId && $row['project_role_id'] == $project_role_id){
                return TRUE;
            }
        }
        return FALSE;
    }
}
