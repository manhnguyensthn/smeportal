<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class DiscoverController extends ApiController {

    public $components = array('RequestHandler', 'PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
    }

    // Coder: Giang Dien
    // Date: 2016-11-22
    // Function: get Options Search
    public function getOptionSearch() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (isset($data['group']) && in_array($data['group'], ['0', '1', '2', '3'])) {
                $dataRespone = [];
                $listEverything = [['object_id' => 1, 'object_name' => __('Recommended For You'), 'group' => 1, 'type' => 1], ['object_id' => 2, 'object_name' => __('Staff Pick'), 'group' => 1, 'type' => 1], ['object_id' => 3, 'object_name' => __('Favorites'), 'group' => 1, 'type' => 1], ['object_id' => 4, 'object_name' => __('Following'), 'group' => 1, 'type' => 1], ['object_id' => 5, 'object_name' => __('Everything'), 'group' => 1, 'type' => 1]];
                $listSortBy = [['object_id' => 1, 'object_name' => __('Popularity (viewed)'), 'group' => 2, 'type' => 3], ['object_id' => 2, 'object_name' => __('Most Applied'), 'group' => 2, 'type' => 3], ['object_id' => 3, 'object_name' => __('Newest'), 'group' => 2, 'type' => 3], ['object_id' => 4, 'object_name' => __('Ending Soonest'), 'group' => 2, 'type' => 3], ['object_id' => 5, 'object_name' => __('Primary Role'), 'group' => 2, 'type' => 3], ['object_id' => 6, 'object_name' => __('Secondary Role'), 'group' => 2, 'type' => 3]];
                if ($data['group'] == 0) {
                    // Get all group
                    $dataRespone = $listEverything;
                    $this->loadModel('Categories');
                    $listCategories = $this->Categories->getListCategoriesByOptions();
                    foreach ($listCategories as $cat) {
                        $dataCat = [
                            'object_id' => $cat['id'],
                            'object_name' => $cat['name'],
                            'group' => 1,
                            'type' => 2
                        ];
                        $dataRespone[] = $dataCat;
                    }
                    foreach ($listSortBy as $sortItem) {
                        $dataRespone[] = $sortItem;
                    }
                    $this->loadModel('Roles');
                    $listRoles = $this->Roles->getRolesByOptions([]);
                    foreach ($listRoles as $role) {
                        $dataRole = [
                            'object_id' => $role['id'],
                            'object_name' => $role['role'],
                            'group' => 3,
                            'type' => 4
                        ];
                        $dataRespone[] = $dataRole;
                    }
                } else if ($data['group'] == 1) {
                    // Get list everything
                    $dataRespone = $listEverything;
                    $this->loadModel('Categories');
                    $listCategories = $this->Categories->getListCategoriesByOptions();
                    foreach ($listCategories as $cat) {
                        $dataCat = [
                            'object_id' => $cat['id'],
                            'object_name' => $cat['name'],
                            'group' => 1,
                            'type' => 2
                        ];
                        $dataRespone[] = $dataCat;
                    }
                } else if ($data['group'] == 2) {
                    // Get list sort by
                    foreach ($listSortBy as $sortItem) {
                        $dataRespone[] = $sortItem;
                    }
                } else {
                    // Get list roles
                    $this->loadModel('Roles');
                    $listRoles = $this->Roles->getRolesByOptions([]);
                    foreach ($listRoles as $role) {
                        $dataRole = [
                            'object_id' => $role['id'],
                            'object_name' => $role['role'],
                            'group' => 3,
                            'type' => 4
                        ];
                        $dataRespone[] = $dataRole;
                    }
                }
                $this->responseApi(1, 'Success', $dataRespone);
            } else {
                $this->responseApi(1032);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    // Coder: Giang Dien
    // Date: 2016-23-11
    // Function: get List Projects By Option Search
    public function getListProjectsByOptionSearch() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token['user_id'];
                $currentPage = (isset($data['page']) && is_numeric($data['page']) && $data['page'] > 0) ? $data['page'] : 1;
                $limit = (isset($data['limit']) && is_numeric($data['limit']) && $data['limit'] > 0) ? $data['limit'] : 20;
                $offset = ($currentPage - 1) * $limit;
                $params = ['limit' => $limit, 'offset' => $offset];
                $filterCategory = $this->_convertArrayDefaultToObject(explode('|', $data['filterCategory']));
                $sortBy = $this->_convertArrayDefaultToObject(explode('|', $data['sortBy']));
                $filterRole = $this->_convertArrayDefaultToObject(explode('|', $data['filterRole']));
                $filterLocation = $this->_convertArrayDefaultToObject(explode('|', $data['filterLocation']));
                $dataRespones = $this->_getProjectsByOption($filterCategory, $params, $userId, $sortBy['object_id'], $filterRole['object_id'], $filterLocation);
				$listProjectIds = $this->_getListProjectIds($dataRespones);
				$this->loadModel('UserProjectActions');
				$this->loadModel('UsersProjects');
				if (count($listProjectIds) > 0) {
					$listUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $listProjectIds, 'action_type' => 1, 'status' => 1]);
					$listUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $listProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
				} else 
				{
					$listUsersActions = [];
					$listUserProjects = [];
				}		
				$this->loadModel('Projects');
				$this->loadModel('UsersProjects');
				$Userprojects = $this->Projects->getAllProjectUsers($userId);
				$UserprojectJoins = $this->UsersProjects-> getAllProejectJoinUser($userId);
				$projectIdUserJoin  = array();
				$projectIdUser = array();	
				foreach($Userprojects as $Userproject)
				{
					$projectIdUser[] = $Userproject->id;
				}
				foreach($UserprojectJoins as $UserprojectJoin)
				{
					$projectIdUserJoin[] = $UserprojectJoin->project_id;
				}	
				$datapro = array();
				foreach($dataRespones as $data)
				{
					$projectIdUserFollow = array();
					$projectIdUserFollowJoin = array();
					$UserprojectFollows = $this->Projects->getAllProjectUsers($data['user_id']);	
					$UserprojectFollowJoins = $this->UsersProjects->getAllProejectJoinUser($data['user_id']);		
					$isApplicant = 0;
					foreach($UserprojectFollows as $Userproject)
					{
						$projectIdUserFollow[] = $Userproject->id;
					}
					foreach($UserprojectFollowJoins  as $UserprojectFollowJoin)
					{
						$projectIdUserFollowJoin[] = $UserprojectFollowJoin->project_id;
					}
					for($i= 0;$i<count($projectIdUser); $i++)
					{
						if(in_array($projectIdUser[$i],$projectIdUserFollowJoin))
						{
							$isApplicant = 1;
						}
					}
					for($i= 0;$i<count($projectIdUserJoin); $i++)
					{
						if(in_array($projectIdUserJoin[$i],$projectIdUserFollow))
						{
							$isApplicant = 1;
						}
					}
					if($isApplicant==1)
					{
					 $role['roleUser'] = 'isApplicant';	
					}
					else
					{
						$role['roleUser'] = 'isStranger';
					}				
					$blocksTable = TableRegistry::get('Blocks');
					$blockUserList = $blocksTable->find('all', ['conditions' => ['user_id' => $data['user_id'], 'blocked_id' => $token->user_id]])->toArray();
					$blockUser = $blocksTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'blocked_id' => $data['user_id']]])->toArray(); 
					if(!empty($blockUserList) || !empty($blockUser))
					{
						$role['canView'] = (int)0;
					}
					else
					{
						$role['canView'] = (int)1;
					}
					$role["user_id"] = $data['user_id'];
					$role["project_id"] = $data['project_id'];
					$role["title"] =  $data["title"];
					$role[ "description"] =  $data[ "description"];
					$role["image_url"] =	 $data["image_url"];
					$role["video_id"] =  $data["video_id"];
					$role["pitch"] =  $data["pitch"] ;
					$role["start_date"] =  $data["start_date"];
					$role["end_date"] =  $data["end_date"]; 
					$role["owner"] =  $data["owner"] ;
					$role["address"] =   $data["address"];
					$role["category"] =  $data["category"];
					$role["category_id"] =  $data["category_id"] ; 
					$role["is_liked"] =  $data["is_liked"] ;
					$role["total_role"] =  $data["total_role"];
					$role[ "total_role_joined"] =  $data[ "total_role_joined"];
					$row = array();
					$role_id = array();
					foreach($data['list_roles'] as $ls)
					{					
					$joined = $this->_get_role_open_in_list($listUserProjects, $data['project_id'], $ls['id']);				
					$rol['count'] = $ls['quantity'] - $joined;	
					$rol['id'] = $ls['id'];
					$role_id[] = $ls['id'];
					$rol['role'] = $ls['role'];
					$rol['role_es'] = $ls['role_es'];
					$rol['role_ja'] = $ls['role_ja'];
					$rol['quantity'] = $ls['quantity'];	
					$row[] = $rol;
					
					}			
					$role[ "list_roles"]= $row;
					$datapro[] = $role;
				}
				$this->responseApi(1, __('Success'), $datapro);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    // Coder: Giang Dien
    // Date: 2016-23-11
    // Function: get List Projects By Option Search For Web
    public function getListProjectsByOptionSearchForWeb() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (isset($data['token'])) {
                $tokenTable = TableRegistry::get('Tokens');
                $token = $tokenTable->find('all', [
                    'conditions' => ['token' => $data['token']],
                ]);
                $token = $token->first();
                $userId = $token->user_id;
            } else {
                $userId = (isset($data['user_id']) && !empty($data['user_id'])) ? $data['user_id'] : 0;
            }
            $currentPage = (isset($data['page']) && is_numeric($data['page']) && $data['page'] > 0) ? $data['page'] : 1;
            $limit = (isset($data['limit']) && is_numeric($data['limit']) && $data['limit'] > 0) ? $data['limit'] : 20;
            $offset = ($currentPage - 1) * $limit;
            $params = ['limit' => $limit, 'offset' => $offset];
            $filterCategory = $this->_convertArrayDefaultToObject(explode('|', $data['filterCategory']));
            $sortBy = $this->_convertArrayDefaultToObject(explode('|', $data['sortBy']));
            $filterRole = $this->_convertArrayDefaultToObject(explode('|', $data['filterRole']));
            $filterLocation =  $data['filterLocation'];
            $dataRespones = $this->_getProjectsByOption($filterCategory, $params, $userId, $sortBy['object_id'], $filterRole['object_id'], $filterLocation);
            $this->responseApi(1, 'Success', $dataRespones);
        } else {
            $this->responseApi(1032);
        }
    }

    private function _convertArrayDefaultToObject($params = array()) {
        $result = ['object_id' => $params[0], 'object_name' => $params[1], 'group' => $params[2], 'type' => $params[3]];
        if (isset($params[4])) {
            $result['minLng'] = $params[4];
        }
        if (isset($params[5])) {
            $result['minLat'] = $params[5];
        }
        if (isset($params[6])) {
            $result['maxLng'] = $params[6];
        }
        if (isset($params[7])) {
            $result['maxLat'] = $params[7];
        }
        return $result;
    }

    // Coder: Giang Dien
    // Date: 2016-23-11
    // Function: get list projects by option
    private function _getProjectsByOption($options = array(), $params = array(), $userId = NULL, $sortParam = 1, $filterRole = 0, $filterLocation = []) {
        $listProjects = [];
        // Get list project ids current user liked
        $userProjectActionTable = TableRegistry::get('UserProjectActions');
		$listProjectLikedIds = [];
        if (!empty($userId)) {
            $listProjectLikes = $userProjectActionTable->find('all', ['conditions' => ['user_id' => $userId, 'action_type' => 1, 'status' => 1]]);
			foreach($listProjectLikes as $ls)
			{
				$listProjectLikedIds[] = $ls->project_id;
			}
			}
			else {
            $listProjectLikedIds = [];
			}
			
        switch ($options['type']) {
            case 1:
                if ($options['object_id'] == 1) {
                    // recommended for you
                    $this->loadModel('SearchedHistory');
                    $this->loadModel('UsersProjects');
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('Projects');
					$this->loadModel('UsersRoles');
                    if (!empty($userId)) {
                        $listSearchHistory = $this->SearchedHistory->getListHistorySearchByOptions('all', ['user_id' => $userId]);
                    } else {
                        $listSearchHistory = $this->SearchedHistory->getListHistorySearchByOptions('all', []);
                    }
                    if (count($listSearchHistory) > 0) {
                        // Get list projects by keyword seached
                        $conditions = [];             
						$param = [];
						foreach ($listSearchHistory as $row) {
							$param['OR'][] = ['role LIKE' => '%' . $row['keyword'] . '%'];
						  }
						$roleTable = TableRegistry::get('Roles');
						$role = $roleTable->find('all', [
							'conditions' =>$param,
						]);
						$roles =  $role->toArray();
						$role_search = [];
						foreach($roles as $rs)
						{	
							$role_search[] = $rs['id'];
						}
						//$token = $token->first();
						if (!empty($userId)) {
                            $userRoles = $this->UsersRoles->getListUsersRolesByOptions('all', ['user_id' => $userId]);
                        } else {
                            $userRoles = $this->UsersRoles->getListUsersRolesByOptions('all', []);
                        }
                        $listRoles = [];
                        foreach ($userRoles as $role) {
							if(!in_array( $role['role_id'],$role_search)) {
								$listRoles[] = $role['role_id'];	
							}
                        }
                        if (count($listRoles) > 0) {
                            $conditions['OR'][]  = ['ProjectsRoles.role_id IN' =>  $listRoles];
                        }
						if (count($role_search) > 0) {
                            $conditions['OR'][]  = ['ProjectsRoles.role_id IN' =>  $role_search];
                        }
                        $sort = [];
                        if ($sortParam == 1) {
                            // Sort by viewed
                            $sort = ['viewed_counter' => 'DESC'];
                        }
                        if ($sortParam == 3) {
                            // Sort by created
                            $sort = ['Projects.created' => 'DESC'];
                        }
                        if ($sortParam == 4) {
                            // Sort by enddate
                            $sort = ['Projects.end_date' => 'ASC'];
                        }
                        if ($sortParam == 5 && !empty($userId)) {
                            // Filter project by primary role
                            $this->loadModel('UsersRoles');
                            $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                            if (!empty($role1)) {
                                $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                                if (count($listProjectIsAllow) > 0) {
                                    $conditions['Projects.id IN'] = $listProjectIsAllow;
                                }
                            }
                        }
                        if ($sortParam == 6 && !empty($userId)) {
                            // Filter project by secondary role
                            $this->loadModel('UsersRoles');
                            $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                            if (!empty($role2)) {
                                $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                                if (count($listProjectIsAllow) > 0) {
                                    $conditions['Projects.id IN'] = $listProjectIsAllow;
                                }
                            }
                        }
                        if (!empty($filterRole)) {
                            // Has filter role
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                            if (count($listProjectIsAllow) > 0) {
                                if (isset($conditions['Projects.id IN'])) {
                                    $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                                } else {
                                    $conditions['Projects.id IN'] = $listProjectIsAllow;
                                }
                            }
                        }
                        if (!empty($filterLocation)) {
                            // Has filter location
                            if (isset($filterLocation['minLng']) && isset($filterLocation['minLat']) && isset($filterLocation['maxLng']) && isset($filterLocation['maxLat'])) {
                                $contains = ['Countries', 'Districts', 'States'];
                                $conditions['OR'][] = [
                                    'Countries.lng >=' => $filterLocation['minLng'],
                                    'Countries.lng <=' => $filterLocation['maxLng'],
                                    'Countries.lat >=' => $filterLocation['minLat'],
                                    'Countries.lat <=' => $filterLocation['maxLat']
                                ];
                                $conditions['OR'][] = [
                                    'Districts.lng >=' => $filterLocation['minLng'],
                                    'Districts.lng <=' => $filterLocation['maxLng'],
                                    'Districts.lat >=' => $filterLocation['minLat'],
                                    'Districts.lat <=' => $filterLocation['maxLat']
                                ];
                                $conditions['OR'][] = [
                                    'States.lng >=' => $filterLocation['minLng'],
                                    'States.lng <=' => $filterLocation['maxLng'],
                                    'States.lat >=' => $filterLocation['minLat'],
                                    'States.lat <=' => $filterLocation['maxLat']
                                ];
                            } else {
                                $contains = [];
                            }
                        } else {
                            $contains = [];
                        }

                        $this->_getCoundKeyword($conditions);
                        if ($sortParam != 2) {
                            // Sort by other
                            $contains[] = 'ProjectsRoles';
                            $contains[] = 'Users';
                            $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, $params['limit'], $params['offset'], $sort,'project_id');
                        } else {
                            // Sort by joined
                            $sort = ['joined_counter' => 'DESC'];
                            $contains[] = 'UsersProjects';
                            $contains[] = 'Users';
                            $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', $conditions, $contains, $params['limit'], $params['offset'], $sort);
                        }
                        $dataRespone = [];
                        foreach ($listProjects as $project) {
                            $data = [
								'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                                'project_id' => empty($project['id']) ? '' : $project['id'],
                                'title' => empty($project['title']) ? '' : $project['title'],
                                'description' => empty($project['description']) ? '' : $project['description'],
                                'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                                'video_id' => empty($project['video_id']) ? '' : $project['video_id'],
                                'pitch' => empty($project['pitch']) ? '' : $project['pitch'],
                                'start_date' => empty($project['start_date']) ? '' : $project['start_date'],
                                'end_date' => empty($project['end_date']) ? '' : $project['end_date'],
                                'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                            ];
                            if (!empty($project['country_id'])) {
                                $this->loadModel('Countries');
                                $country = $this->Countries->get($project['country_id']);
                                $data['address'] = $country['country_name'];
                                if (!empty($project['state_id'])) {
                                    $this->loadModel('States');
                                    $state = $this->States->get($project['state_id']);
                                    $data['address'] = $state['name'] . ', ' . $country['country_name'];
                                }
                            } else {
                                $data['address'] = '';
                            }
                            if (!empty($project['category_id'])) {
                                $this->loadModel('Categories');
                                $category = $this->Categories->get($project['category_id']);
                                $data['category'] = $category['name'];
								$data['category_id'] = $category['id'];
                            } else {
                                $data['category'] = 'Feature';
                            }
                            $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                            $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                            $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                            $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                            $dataRespone[] = $data;
                        }
                         return $dataRespone;
                    } else {
                        // Get list projects by roles
                        $conditions = [];
                        $this->loadModel('UsersRoles');
                        $this->loadModel('ProjectsRoles');
                        if (!empty($userId)) {
                            $userRoles = $this->UsersRoles->getListUsersRolesByOptions('all', ['user_id' => $userId]);
                        } else {
                            $userRoles = $this->UsersRoles->getListUsersRolesByOptions('all', []);
                        }
                        $listRoles = [];
                        foreach ($userRoles as $role) {
                            $listRoles[] = $role['role_id'];
                        }
                        if (count($listRoles) > 0) {
                            $conditions['ProjectsRoles.role_id IN'] = $listRoles;
                        }
                        $sort = [];
                        if ($sortParam == 1) {
                            $sort = ['viewed_counter' => 'DESC'];
                        }
                        if ($sortParam == 3) {
                            // Sort by created
                            $sort = ['Projects.created' => 'DESC'];
                        }
                        if ($sortParam == 4) {
                            // Sort by enddate
                            $sort = ['Projects.end_date' => 'ASC'];
                        }
                        if ($sortParam == 5 && !empty($userId)) {
                            // Filter project by primary role
                            $this->loadModel('UsersRoles');
                            $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                            if (!empty($role1)) {
                                $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                        if ($sortParam == 6 && !empty($userId)) {
                            // Filter project by secondary role
                            $this->loadModel('UsersRoles');
                            $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                            if (!empty($role2)) {
                                $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                        if (!empty($filterRole)) {
                            // Has filter role
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                            if (count($listProjectIsAllow) > 0) {
                                if (isset($conditions['Projects.id IN'])) {
                                    $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                                } else {
                                    $conditions['Projects.id IN'] = $listProjectIsAllow;
                                }
                            }
                        }
                        if (!empty($filterLocation)) {
                            // Has filter location
                            if (isset($filterLocation['minLng']) && isset($filterLocation['minLat']) && isset($filterLocation['maxLng']) && isset($filterLocation['maxLat'])) {
                                $contains = ['Countries', 'Districts', 'States'];
                                $conditions['OR'][] = [
                                    'Countries.lng >=' => $filterLocation['minLng'],
                                    'Countries.lng <=' => $filterLocation['maxLng'],
                                    'Countries.lat >=' => $filterLocation['minLat'],
                                    'Countries.lat <=' => $filterLocation['maxLat']
                                ];
                                $conditions['OR'][] = [
                                    'Districts.lng >=' => $filterLocation['minLng'],
                                    'Districts.lng <=' => $filterLocation['maxLng'],
                                    'Districts.lat >=' => $filterLocation['minLat'],
                                    'Districts.lat <=' => $filterLocation['maxLat']
                                ];
                                $conditions['OR'][] = [
                                    'States.lng >=' => $filterLocation['minLng'],
                                    'States.lng <=' => $filterLocation['maxLng'],
                                    'States.lat >=' => $filterLocation['minLat'],
                                    'States.lat <=' => $filterLocation['maxLat']
                                ];
                            } else {
                                $contains = [];
                            }
                        } else {
                            $contains = [];
                        }

                        $this->_getCoundKeyword($conditions);
                        if ($sortParam != 2) {
                            // Sort by other
                            $contains[] = 'ProjectsRoles';
                            $contains[] = 'Users';
                            $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, $params['limit'], $params['offset'], $sort, 'ProjectsRoles.project_id');
                        } else {
                            // Sort by joined
                            $contains[] = 'ProjectsRoles';
                            $listAllProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, 0, 0, $sort, 'ProjectsRoles.project_id');
                            $listProjectIds = $this->_getListProjectIdsFromList($listAllProjects);
                            $sort = ['joined_counter' => 'DESC'];
                            $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', ['Projects.id IN' => $listProjectIds], ['UsersProjects', 'Users'], $params['limit'], $params['offset'], $sort);
                        }
                        $dataRespone = [];
                        foreach ($listProjects as $project) {
                            $data = [
								'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                                'project_id' => empty($project['id']) ? 0 : $project['id'],
                                'title' => empty($project['title']) ? '' : $project['title'],
                                'description' => empty($project['description']) ? '' : $project['description'],
                                'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                                'video_id' => empty($project['video_id']) ? '' : $project['video_id'],
                                'pitch' => empty($project['pitch']) ? '' : $project['pitch'],
                                'start_date' => empty($project['start_date']) ? '' : $project['start_date'],
                                'end_date' => empty($project['end_date']) ? '' : $project['end_date'],
                                'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                            ];
                            if (!empty($project['country_id'])) {
                                $this->loadModel('Countries');
                                $country = $this->Countries->get($project['country_id']);
                                $data['address'] = $country['country_name'];
                                if (!empty($project['state_id'])) {
                                    $this->loadModel('States');
                                    $state = $this->States->get($project['state_id']);
                                    $data['address'] = $state['name'] . ', ' . $country['country_name'];
                                }
                            } else {
                                $data['address'] = '';
                            }
                            if (!empty($project['category_id'])) {
                                $this->loadModel('Categories');
                                $category = $this->Categories->get($project['category_id']);
                                $data['category'] = $category['name'];
                                $data['category_id'] = $category['id'];
                            } else {
                                $data['category'] = 'Feature';
                                $data['category_id'] = 0;
                            }
                            $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                            $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                            $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                            $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                            $dataRespone[] = $data;
                        }
                        return $dataRespone;
                    }
                } else if ($options['object_id'] == 2) {
                    // Staff picks
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('Projects');
                    $this->loadModel('UsersProjects');
                    $conditions = ['Projects.status' => 2];
                    $sort = [];
                    if ($sortParam == 1) {
                        $sort = ['viewed_counter' => 'DESC'];
                    }
                    if ($sortParam == 3) {
                        // Sort by created
                        $sort = ['Projects.created' => 'DESC'];
                    }
                    if ($sortParam == 4) {
                        // Sort by enddate
                        $sort = ['Projects.end_date' => 'ASC'];
                    }
                    if ($sortParam == 5 && !empty($userId)) {
                        // Filter project by primary role
                        $this->loadModel('UsersRoles');
                        $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                        if (!empty($role1)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                            $conditions['Projects.id IN'] = $listProjectIsAllow;
                        }
                    }
                    if ($sortParam == 6 && !empty($userId)) {
                        // Filter project by secondary role
                        $this->loadModel('UsersRoles');
                        $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                        if (!empty($role2)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                            $conditions['Projects.id IN'] = $listProjectIsAllow;
                        }
                    }
                    if (!empty($filterRole)) {
                        // Has filter role
                        $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                        if (count($listProjectIsAllow) > 0) {
                            if (isset($conditions['Projects.id IN'])) {
                                $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                            } else {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterLocation)) {
                        // Has filter location
                        if (isset($filterLocation['minLng']) && isset($filterLocation['minLat']) && isset($filterLocation['maxLng']) && isset($filterLocation['maxLat'])) {
                            $contains = ['Countries', 'Districts', 'States'];
                            $conditions['OR'][] = [
                                'Countries.lng >=' => $filterLocation['minLng'],
                                'Countries.lng <=' => $filterLocation['maxLng'],
                                'Countries.lat >=' => $filterLocation['minLat'],
                                'Countries.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'Districts.lng >=' => $filterLocation['minLng'],
                                'Districts.lng <=' => $filterLocation['maxLng'],
                                'Districts.lat >=' => $filterLocation['minLat'],
                                'Districts.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'States.lng >=' => $filterLocation['minLng'],
                                'States.lng <=' => $filterLocation['maxLng'],
                                'States.lat >=' => $filterLocation['minLat'],
                                'States.lat <=' => $filterLocation['maxLat']
                            ];
                        } else {
                            $contains = [];
                        }
                    } else {
                        $contains = [];
                    }

                    $this->_getCoundKeyword($conditions);
                    if ($sortParam != 2) {
                        // Sort by others
                        $contains[] = 'Users';
                        $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, $params['limit'], $params['offset'], $sort);
                    } else {
                        // Sort by joined
                        $sort = ['joined_counter' => 'DESC'];
                        $contains[] = 'UsersProjects';
                        $contains[] = 'Users';
                        $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', $conditions, $contains, $params['limit'], $params['offset'], $sort);
                    }
                    $dataRespone = [];
                    foreach ($listProjects as $project) {
                        $data = [
							'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                            'project_id' => empty($project['id']) ? 0 : $project['id'],
                            'title' => empty($project['title']) ? '' : $project['title'],
                            'description' => $project['description'],
                            'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                            'video_id' => $project['video_id'],
                            'pitch' => $project['pitch'],
                            'start_date' => $project['start_date'],
                            'end_date' => $project['end_date'],
                            'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                        ];
                        if (!empty($project['country_id'])) {
                            $this->loadModel('Countries');
                            $country = $this->Countries->get($project['country_id']);
                            $data['address'] = $country['country_name'];
                            if (!empty($project['state_id'])) {
                                $this->loadModel('States');
                                $state = $this->States->get($project['state_id']);
                                $data['address'] = $state['name'] . ', ' . $country['country_name'];
                            }
                        } else {
                            $data['address'] = '';
                        }
                        if (!empty($project['category_id'])) {
                            $this->loadModel('Categories');
                            $category = $this->Categories->get($project['category_id']);
                            $data['category'] = $category['name'];
                            $data['category_id'] = $category['id'];
                        } else {
                            $data['category'] = 'Feature';
                            $data['category_id'] = 0;
                        }
                        $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                        $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                        $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                        $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                        $dataRespone[] = $data;
                    }
                    return $dataRespone;
                } else if ($options['object_id'] == 3) {
                    // Favorites
                    $this->loadModel('UserProjectActions');
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('UsersProjects');
                    $this->loadModel('Projects');
                    $sort = [];
                    if (!empty($userId)) {
                        $conditions = ['UserProjectActions.action_type' => 1, 'UserProjectActions.status' => 1, 'UserProjectActions.user_id' => $userId];
                    } else {
                        $conditions = ['UserProjectActions.action_type' => 1, 'UserProjectActions.status' => 1];
                    }
                    if ($sortParam == 1) {
                        $sort = ['Projects.viewed_counter' => 'DESC'];
                    }
                    if ($sortParam == 3) {
                        // Sort by created
                        $sort = ['Projects.created' => 'DESC'];
                    }
                    if ($sortParam == 4) {
                        // Sort by enddate
                        $sort = ['Projects.end_date' => 'ASC'];
                    }
                    if ($sortParam == 5 && !empty($userId)) {
                        // Filter project by primary role
                        $this->loadModel('UsersRoles');
                        $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                        if (!empty($role1)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                            if (count($listProjectIsAllow) > 0) {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if ($sortParam == 6 && !empty($userId)) {
                        // Filter project by secondary role
                        $this->loadModel('UsersRoles');
                        $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                        if (!empty($role2)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                            if (count($listProjectIsAllow) > 0) {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterRole)) {
                        // Has filter role
                        $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                        if (count($listProjectIsAllow) > 0) {
                            if (isset($conditions['Projects.id IN'])) {
                                $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                            } else {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterLocation)) {
                        // Has filter location
                        if (isset($filterLocation['minLng']) && isset($filterLocation['minLat']) && isset($filterLocation['maxLng']) && isset($filterLocation['maxLat'])) {
                            $contains = ['UserProjectActions', 'Users', 'Countries', 'Districts', 'States'];
                            $conditions['OR'][] = [
                                'Countries.lng >=' => $filterLocation['minLng'],
                                'Countries.lng <=' => $filterLocation['maxLng'],
                                'Countries.lat >=' => $filterLocation['minLat'],
                                'Countries.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'Districts.lng >=' => $filterLocation['minLng'],
                                'Districts.lng <=' => $filterLocation['maxLng'],
                                'Districts.lat >=' => $filterLocation['minLat'],
                                'Districts.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'States.lng >=' => $filterLocation['minLng'],
                                'States.lng <=' => $filterLocation['maxLng'],
                                'States.lat >=' => $filterLocation['minLat'],
                                'States.lat <=' => $filterLocation['maxLat']
                            ];
                        } else {
                            $contains = ['UserProjectActions', 'Users'];
                        }
                    } else {
                        $contains = ['UserProjectActions', 'Users'];
                    }

                    $this->_getCoundKeyword($conditions);
                    if ($sortParam != 2) {
                        // Sort by other
                        $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, $params['limit'], $params['offset'], $sort, 'UserProjectActions.project_id');
                    } else {
                        // Sort by joined
                        $listAllProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, 0, 0, $sort, 'UserProjectActions.project_id');
                        $listProjectIds = $this->_getListProjectIdsFromList($listAllProjects);
                        if (count($listProjectIds) > 0) {
                            $sort = ['joined_counter' => 'DESC'];
                            $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', ['Projects.id IN' => $listProjectIds], ['UsersProjects', 'Users'], $params['limit'], $params['offset'], $sort);
                        } else {
                            $listProjects = [];
                        }
                    }
                    $dataRespone = [];
                    foreach ($listProjects as $project) {
                        $data = [
							'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                            'project_id' => $project['id'],
                            'title' => $project['title'],
                            'description' => $project['description'],
                            'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                            'video_id' => $project['video_id'],
                            'pitch' => $project['pitch'],
                            'start_date' => $project['start_date'],
                            'end_date' => $project['end_date'],
                            'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                        ];
                        if (!empty($project['country_id'])) {
                            $this->loadModel('Countries');
                            $country = $this->Countries->get($project['country_id']);
                            $data['address'] = $country['country_name'];
                            if (!empty($project['state_id'])) {
                                $this->loadModel('States');
                                $state = $this->States->get($project['state_id']);
                                $data['address'] = $state['name'] . ', ' . $country['country_name'];
                            }
                        } else {
                            $data['address'] = '';
                        }
                        if (!empty($project['category_id'])) {
                            $this->loadModel('Categories');
                            $category = $this->Categories->get($project['category_id']);
                            $data['category'] = $category['name'];
                            $data['category_id'] = $category['id'];
                        } else {
                            $data['category'] = 'Feature';
                            $data['category_id'] = 0;
                        }
                        $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                        $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                        $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                        $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                        $dataRespone[] = $data;
                    }
                    return $dataRespone;
                } else if ($options['object_id'] == 4) {
                    // Get list following
                    $this->loadModel('UserProjectActions');
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('UsersProjects');
                    $this->loadModel('Projects');
                    $sort = [];
                    if (!empty($userId)) {
                        $conditions = ['action_type' => 2, 'UserProjectActions.status' => 1, 'UserProjectActions.user_id' => $userId];
                    } else {
                        $conditions = ['action_type' => 2, 'UserProjectActions.status' => 1];
                    }
                    if ($sortParam == 1) {
                        $sort = ['Projects.viewed_counter' => 'DESC'];
                    }
                    if ($sortParam == 3) {
                        // Sort by created
                        $sort = ['Projects.created' => 'DESC'];
                    }
                    if ($sortParam == 4) {
                        // Sort by enddate
                        $sort = ['Projects.end_date' => 'ASC'];
                    }
                    if ($sortParam == 5 && !empty($userId)) {
                        // Filter project by primary role
                        $this->loadModel('UsersRoles');
                        $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                        if (!empty($role1)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                            if (count($listProjectIsAllow) > 0) {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if ($sortParam == 6 && !empty($userId)) {
                        // Filter project by secondary role
                        $this->loadModel('UsersRoles');
                        $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                        if (!empty($role2)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                            if (count($listProjectIsAllow) > 0) {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterRole)) {
                        // Has filter role
                        $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                        if (count($listProjectIsAllow) > 0) {
                            if (isset($conditions['Projects.id IN'])) {
                                $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                            } else {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterLocation)) {
                        // Has filter location
                        if (isset($filterLocation['minLng']) && isset($filterLocation['minLat']) && isset($filterLocation['maxLng']) && isset($filterLocation['maxLat'])) {
                            $contains = ['Countries', 'Districts', 'States'];
                            $conditions['OR'][] = [
                                'Countries.lng >=' => $filterLocation['minLng'],
                                'Countries.lng <=' => $filterLocation['maxLng'],
                                'Countries.lat >=' => $filterLocation['minLat'],
                                'Countries.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'Districts.lng >=' => $filterLocation['minLng'],
                                'Districts.lng <=' => $filterLocation['maxLng'],
                                'Districts.lat >=' => $filterLocation['minLat'],
                                'Districts.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'States.lng >=' => $filterLocation['minLng'],
                                'States.lng <=' => $filterLocation['maxLng'],
                                'States.lat >=' => $filterLocation['minLat'],
                                'States.lat <=' => $filterLocation['maxLat']
                            ];
                        } else {
                            $contains = [];
                        }
                    } else {
                        $contains = [];
                    }
                    $contains[] = 'UserProjectActions';
                    $contains[] = 'Users';

                    $this->_getCoundKeyword($conditions);
                    if ($sortParam != 2) {
                        // Sort by other
                        $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, $params['limit'], $params['offset'], $sort, 'UserProjectActions.project_id');
                    } else {
                        // Sort by joined
                        $listAllProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, 0, 0, $sort, 'UserProjectActions.project_id');
                        $listProjectIds = $this->_getListProjectIdsFromList($listAllProjects);
                        if (count($listProjectIds) > 0) {
                            $sort = ['joined_counter' => 'DESC'];
                            $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', ['Projects.id IN' => $listProjectIds], ['UsersProjects', 'Users'], $params['limit'], $params['offset'], $sort);
                        } else {
                            $listProjects = [];
                        }
                    }
                    $dataRespone = [];
                    foreach ($listProjects as $project) {
                        $data = [
							'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                            'project_id' => $project['id'],
                            'title' => $project['title'],
                            'description' => $project['description'],
                            'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                            'video_id' => $project['video_id'],
                            'pitch' => $project['pitch'],
                            'start_date' => $project['start_date'],
                            'end_date' => $project['end_date'],
                            'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                        ];
                        if (!empty($project['country_id'])) {
                            $this->loadModel('Countries');
                            $country = $this->Countries->get($project['country_id']);
                            $data['address'] = $country['country_name'];
                            if (!empty($project['state_id'])) {
                                $this->loadModel('States');
                                $state = $this->States->get($project['state_id']);
                                $data['address'] = $state['name'] . ', ' . $country['country_name'];
                            }
                        } else {
                            $data['address'] = '';
                        }
                        if (!empty($project['category_id'])) {
                            $this->loadModel('Categories');
                            $category = $this->Categories->get($project['category_id']);
                            if (!empty($category)) {
                                $data['category'] = $category['name'];
                                $data['category_id'] = $category['id'];
                            } else {
                                $data['category'] = 'Feature';
                                $data['category_id'] = 0;
                            }
                        } else {
                            $data['category'] = 'Feature';
                        }
                        $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                        $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                        $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                        $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                        $dataRespone[] = $data;
                    }
                    return $dataRespone;
                } else {
                    // Everything
                    $this->loadModel('Projects');
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('UsersProjects');
                    $sort = [];
                    $conditions = [];
                    if ($sortParam == 1) {
                        $sort = ['Projects.viewed_counter' => 'DESC'];
                    }
                    if ($sortParam == 3) {
                        // Sort by created
                        $sort = ['Projects.created' => 'DESC'];
                    }
                    if ($sortParam == 4) {
                        // Sort by enddate
                        $sort = ['Projects.end_date' => 'ASC'];
                    }
                    if ($sortParam == 5 && !empty($userId)) {
                        // Filter project by primary role
                        $this->loadModel('UsersRoles');
                        $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                        if (!empty($role1)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                            if (count($listProjectIsAllow) > 0) {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if ($sortParam == 6 && !empty($userId)) {
                        // Filter project by secondary role
                        $this->loadModel('UsersRoles');
                        $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                        if (!empty($role2)) {
                            $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                            if (count($listProjectIsAllow) > 0) {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterRole)) {
                        // Has filter role
                        $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                        if (count($listProjectIsAllow) > 0) {
                            if (isset($conditions['Projects.id IN'])) {
                                $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                            } else {
                                $conditions['Projects.id IN'] = $listProjectIsAllow;
                            }
                        }
                    }
                    if (!empty($filterLocation)) {
                        // Has filter location
                        if (isset($filterLocation['minLng']) && isset($filterLocation['minLat']) && isset($filterLocation['maxLng']) && isset($filterLocation['maxLat'])) {
                            $contains = ['Countries', 'Districts', 'States'];
                            $conditions['OR'][] = [
                                'Countries.lng >=' => $filterLocation['minLng'],
                                'Countries.lng <=' => $filterLocation['maxLng'],
                                'Countries.lat >=' => $filterLocation['minLat'],
                                'Countries.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'Districts.lng >=' => $filterLocation['minLng'],
                                'Districts.lng <=' => $filterLocation['maxLng'],
                                'Districts.lat >=' => $filterLocation['minLat'],
                                'Districts.lat <=' => $filterLocation['maxLat']
                            ];
                            $conditions['OR'][] = [
                                'States.lng >=' => $filterLocation['minLng'],
                                'States.lng <=' => $filterLocation['maxLng'],
                                'States.lat >=' => $filterLocation['minLat'],
                                'States.lat <=' => $filterLocation['maxLat']
                            ];
                        } else {
                            $contains = [];
                        }
                    } else {
                        $contains = [];
                    }

                    $this->_getCoundKeyword($conditions);
                    if ($sortParam != 2) {
                        // Sort by other
                        $contains[] = 'Users';
                        $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, $contains, $params['limit'], $params['offset'], $sort);
                    } else {
                        // Sort by joined
                        $sort = ['joined_counter' => 'DESC'];
                        $contains[] = 'UsersProjects';
                        $contains[] = 'Users';
                        $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', [], $contains, $params['limit'], $params['offset'], $sort);
                    }
                    $dataRespone = [];
                    foreach ($listProjects as $project) {
                        $data = [
							'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                            'project_id' => $project['id'],
                            'title' => $project['title'],
                            'description' => $project['description'],
                            'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                            'video_id' => $project['video_id'],
                            'pitch' => $project['pitch'],
                            'start_date' => $project['start_date'],
                            'end_date' => $project['end_date'],
                            'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                        ];
                        if (!empty($project['country_id'])) {
                            $this->loadModel('Countries');
                            $country = $this->Countries->get($project['country_id']);
                            $data['address'] = $country['country_name'];
                            if (!empty($project['state_id'])) {
                                $this->loadModel('States');
                                $state = $this->States->get($project['state_id']);
                                $data['address'] = $state['name'] . ', ' . $country['country_name'];
                            }
                        } else {
                            $data['address'] = '';
                        }
                        if (!empty($project['category_id'])) {
                            $this->loadModel('Categories');
                            $category = $this->Categories->get($project['category_id']);
                            $data['category'] = $category['name'];
                            $data['category_id'] = $category['id'];
                        } else {
                            $data['category'] = 'Feature';
                            $data['category_id'] = 0;
                        }
                        $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                        $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                        $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                        $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                        $dataRespone[] = $data;
                    }
                    return $dataRespone;
                }
                break;
            case 2:
                // Get list projects by category
                $this->loadModel('Projects');
                $this->loadModel('ProjectsRoles');
                $this->loadModel('UsersProjects');
                $sort = [];
                $conditions = ['category_id' => $options['object_id']];
                if ($sortParam == 1) {
                    $sort = ['Projects.viewed_counter' => 'DESC'];
                }
                if ($sortParam == 3) {
                    // Sort by created
                    $sort = ['Projects.created' => 'DESC'];
                }
                if ($sortParam == 4) {
                    // Sort by enddate
                    $sort = ['Projects.end_date' => 'ASC'];
                }
                if ($sortParam == 5 && !empty($userId)) {
                    // Filter project by primary role
                    $this->loadModel('UsersRoles');
                    $role1 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 1]])->first();
                    if (!empty($role1)) {
                        $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role1['role_id']]);
                        if (count($listProjectIsAllow) > 0) {
                            $conditions['Projects.id IN'] = $listProjectIsAllow;
                        }
                    }
                }
                if ($sortParam == 6 && !empty($userId)) {
                    // Filter project by secondary role
                    $this->loadModel('UsersRoles');
                    $role2 = $this->UsersRoles->find('all', ['conditions' => ['user_id' => $userId, 'position' => 2]])->first();
                    if (!empty($role2)) {
                        $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $role2['role_id']]);
                        if (count($listProjectIsAllow) > 0) {
                            $conditions['Projects.id IN'] = $listProjectIsAllow;
                        }
                    }
                }
                if (!empty($filterRole)) {
                    // Has filter role
                    $listProjectIsAllow = $this->ProjectsRoles->getListProjectIdsByOptions(['role_id' => $filterRole]);
                    if (count($listProjectIsAllow) > 0) {
                        if (isset($conditions['Projects.id IN'])) {
                            $conditions['Projects.id IN'] = array_merge($conditions['Projects.id IN'], $listProjectIsAllow);
                        } else {
                            $conditions['Projects.id IN'] = $listProjectIsAllow;
                        }
                    }
                }

                $this->_getCoundKeyword($conditions);
                if ($sortParam != 2) {
                    // Sort by other
                    $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, ['Users'], $params['limit'], $params['offset'], $sort);
                } else {
                    // Sort by joined
                    $sort = ['joined_counter' => 'DESC'];
                    $listProjects = $this->Projects->getListProjectsAndCountJoinedForAPI('all', $conditions, ['UsersProjects', 'Users'], $params['limit'], $params['offset'], $sort);
                }
                $dataRespone = [];
                foreach ($listProjects as $project) {
                    $data = [
						'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                        'project_id' => $project['id'],
                        'title' => $project['title'],
                        'description' => $project['description'],
                        'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                        'video_id' => $project['video_id'],
                        'pitch' => $project['pitch'],
                        'start_date' => $project['start_date'],
                        'end_date' => $project['end_date'],
                        'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                    ];
                    if (!empty($project['country_id'])) {
                        $this->loadModel('Countries');
                        $country = $this->Countries->get($project['country_id']);
                        $data['address'] = $country['country_name'];
                        if (!empty($project['state_id'])) {
                            $this->loadModel('States');
                            $state = $this->States->get($project['state_id']);
                            $data['address'] = $state['name'] . ', ' . $country['country_name'];
                        }
                    } else {
                        $data['address'] = '';
                    }
                    if (!empty($project['category_id'])) {
                        $this->loadModel('Categories');
                        $category = $this->Categories->get($project['category_id']);
                        $data['category'] = $category['name'];
                        $data['category_id'] = $category['id'];
                    } else {
                        $data['category'] = 'Feature';
                        $data['category_id'] = 0;
                    }
                    $data['is_liked'] = in_array($project['id'], $listProjectLikedIds) ? 1 : 0;
                    $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                    $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                    $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                    $dataRespone[] = $data;
                }
                return $dataRespone;
                break;
        }
        return [];
    }

    private function _getCoundKeyword(&$conditions) {
        $RequestData = $this->request->data;
        if (isset($RequestData['keyword']) && !empty($RequestData['keyword'])) {
            $keyword = trim(strip_tags($RequestData['keyword']));

            $UserId = (isset($RequestData['user_id']) && !empty($RequestData['user_id'])) ? (int) $RequestData['user_id'] : 0;
            if ($UserId) {
                $SearchedHistoryTable = TableRegistry::get('SearchedHistory');
                $TotalCount = $SearchedHistoryTable->find()->where(['user_id' => $UserId, 'keyword' => $keyword])->count();
                if (!$TotalCount) {
                    $SearchedHistory = $SearchedHistoryTable->newEntity();
                    $SearchedHistory->user_id = $UserId;
                    $SearchedHistory->keyword = $keyword;
                    $SearchedHistory->created = date('Y-m-d H:i:s');
                    $SearchedHistoryTable->save($SearchedHistory);
                } else {
                    $SearchedHistory = $SearchedHistoryTable->find()->where(['user_id' => $UserId, 'keyword' => $keyword])->first();
                    $SearchedHistory->created = date('Y-m-d H:i:s');
                    $SearchedHistoryTable->save($SearchedHistory);
                }
            }

            $conditions['OR'] = array(
                'Projects.title LIKE' => '%' . $keyword . '%',
                'Projects.description LIKE' => '%' . $keyword . '%',
            );
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-28
    // Function: get list project ids from list projects
    private function _getListProjectIdsFromList($listProject = []) {
        $result = [];
        foreach ($listProject as $row) {
            $result[] = $row['id'];
        }
        return $result;
    }
	 private function _getListProjectIds($list = []) {
        $listIds = [];
        foreach ($list as $row) {
            $listIds[] = $row['project_id'];
        }
        return $listIds;
    }
	private function _get_role_open_in_list($listUserJoined = [], $project_id = 0, $project_role_id = 0) {
        $counter = 0;
        foreach ($listUserJoined as $row) {
            if ($row['project_id'] == $project_id && $row['status'] == 1 && $row['project_role_id'] == $project_role_id) {
                $counter++;
            }
        }
        return $counter;
    }

}
