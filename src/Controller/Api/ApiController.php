<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Mailer\Email;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use App\Lib\CoreLib;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class ApiController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
//    public $components = array('RequestHandler','Flash');
    public $_message   = "";
    public $_data      = [];
    public $_status    = 0;
    public $_errors    = [];
    public $_user_id   = '';
    public $_api_error = array();
    public $_bContinue = true;
    public $components = array('PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }
    public $allow = array(
        'Users/login',
        'Users/register',
        'Users/loginSns',
        'Users/changePass',
        'Users/getPaymentInfo',
        'Users/showFollowing',
        'Users/showFollower',
        'Users/showBlocklist',
        'Users/changeEmail',
        'Users/updatePaymentInfo',
        'NotificationSetting/setting',
        'NotificationSetting/update',
        'ProjectsRoles/getListProjectRoles',

        // add by Nguyen Duc
        'Profile/edit',
    );
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        $this->set('_serialize', true);
    }

    public function beforeFilter(Event $event)
    {
        $this->_setLocaleLang();

        require_once(ROOT . DS . 'src' . DS  . 'Controller' . DS  . 'Api' . DS . 'ErrorDefine.php');
        $this->api_error = Configure::read('API_ERROR');

        if(isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'api') {
            $this->viewBuilder()->layout('api');
        }
        $path = $this->request->controller . '/' . $this->request->action;
//        if (!in_array($path, $this->allow)) {
//            $this->_validToken();
//        }
    }

    /**
     * set Locale Lang
     * @author Roxannie Nguyen jr
     */
    private function _setLocaleLang(){
        $Languages = CoreLib::getDataLanguage();
        $lang      = isset($this->request->data['language']) ? $this->request->data['language'] : "en";
        if(!array_key_exists($lang, $Languages)) $lang = 'en';
        if(isset($Languages[$lang]['code']) && !empty($Languages[$lang]['code'])){
            I18n::locale($Languages[$lang]['code']);
        }else{
            I18n::locale('en_US');
        }
    }

    public function generateRandomString() {
        $str = md5(rand(1, 100) . time());
        return sha1($str);
    }
    public function sendMail($template, $from, $to, $viewArrs = null, $subject = '', $layout = 'default', $format = 'html') {
        $mail = new Email($layout);
        $from = [EMAIL_LOGIN => __('We The Project')];
        $mail->transport('default')
            ->template($template, $layout)
            ->emailFormat($format)
            ->viewVars($viewArrs)
            ->from($from)
            ->to($to)
            ->subject($subject);
        if ($mail->send()) {
            return true;
        }
        return false;
    }
    // create token login
   public function createToken($user_id = 0, $platform = 0, $tk = null,$devicetoken  = null) {
        if ($user_id == 0) {
            return FALSE;
        }
        if (!$tk) {
            $tk = $this->generateRandomString();
        }
		$this->Token = TableRegistry::get('Tokens');
        $this->request->data['user_id'] = $user_id;
        $this->request->data['platform'] = $platform;
        $this->request->data['token'] = $tk;
		$this->request->data['token_device'] = $devicetoken;
        $this->request->data['created'] = date("Y-m-d H:i:s");
        $token = $this->Token->newEntity();
        $token = $this->Token->patchEntity($token, $this->request->data);
        if ($this->Token->save($token)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // clear token logout
    public function clearToken($user_id, $platform = 2) {
        $this->Token = TableRegistry::get('Tokens');
        $this->Token->deleteAll(
            [
                'Tokens.user_id' => $user_id,
                'Tokens.platform' => $platform
            ]
        );
    }

    function log_debug($message) {
        if (DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $last = $backtrace[0];
            Log::write('debug', '[' . date("d-M-Y G:i:s") . '] ' . '[' . date("e") . '] ' . basename($last['file']) . " " . $last['line'] . " " . $message . "\n", 3, LOGS . "debug.log");
        }
    }

    function log_error($message) {
        if (DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $last = $backtrace[0];
            Log::write('error', '[' . date("d-M-Y G:i:s") . '] ' . '[' . date("e") . '] ' . basename($last['file']) . " " . $last['line'] . " " . $message . "\n", 3, LOGS . "error.log");
        }
    }

    private function _validToken() {
        $this->Token = TableRegistry::get('Tokens');
        if (isset($this->request->data['token'])) {
            $query = $this->Token->find('all', [
                'conditions' => ['Tokens.token' => $this->request->data['token']]
            ]);
            $data = $query->first();
            if ($data) {
                $this->_user_id = $data->id;
            } else {
                echo json_encode(array(
                        'status' => 0,
                        'message' => __("Token expired"),
                        'data' => []
                    )
                );
                die();
            }
        } else {
            echo json_encode(array(
                    'status' => 0,
                    'message' =>__('Token not empty'),
                    'data' => []
                )
            );
            die();
        }
    }
    function attachfile($folder, $file, $itemId = null) {
        // setup dir names absolute and relative
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;

        // create the folder if it does not exist
        if (!is_dir($folder_url)) {
            mkdir($folder_url);
        }

        // if itemId is set create an item folder
        if ($itemId) {
            // set new absolute folder
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            // set new relative folder
            $rel_url = $folder . '/' . $itemId;
            // create directory
            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }
        }

        // list of permitted file types, this is only images but documents can be added
        $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.ms-excel', 'application/pdf','text/plain');

        // loop through and deal with the files
//        foreach ($formdata as $file) {
        // replace spaces with underscores
        $filename = str_replace(' ', '_', $file['name']);
        // assume filetype is false
        $typeOK = false;
        // check filetype is ok
        foreach ($permitted as $type) {
            if ($type == $file['type']) {
                $typeOK = true;
                break;
            }
        }

        // if file type ok upload the file
        if ($typeOK) {
            // switch based on error code
            switch ($file['error']) {
                case 0:
                    // check filename already exists
                    if (!file_exists($folder_url . '/' . $filename)) {
                        // create full filename
                        $full_url = $folder_url . '/' . $filename;
                        $url = $rel_url . '/' . $filename;
                        // upload the file
                        $success = move_uploaded_file($file['tmp_name'], $url);
                    } else {
                        // create unique filename and upload file
                        ini_set('date.timezone', 'Europe/London');
                        $now = date('Y-m-d-His');
                        $full_url = $folder_url . '/' . $now . $filename;
                        $url = $rel_url . '/' . $now . $filename;
                        $success = move_uploaded_file($file['tmp_name'], $url);
                    }
                    // if upload was successful
                    if ($success) {
                        // save the url of the file
                        $result['url'] = $url;
                    } else {
                        $result['errors'] = fprintf(__("Error uploaded %s. Please try again."), $filename);
                    }
                    break;
                case 3:
                    // an error occured
                    $result['errors'] = fprintf(__("Error uploading %s. Please try again."), $filename);
                    break;
                default:
                    // an error occured
                    $result['errors'] = fprintf(__("System error uploading %s. Contact webmaster."), $filename);
                    break;
            }
        } elseif ($file['error'] == 4) {
            // no file was selected for upload
            $result['nofiles'] = ("No file Selected");
            $result['url']='';
        } else {
            // unacceptable file type
            $result['errors'] = fprintf(__("Định dạng không được chấp nhận"));
        }
//        }
        return $result;
    }

    function responseApi($status = 1, $message = '', $data = array(), $header_type = 'json') {
        header('Content-Type: application/json');

        if ($status == 1) {
            echo json_encode(array(
                    'status' => $status,
                    'message' => $message,
                    'data' => $data
                )
            );
        } else {
            if (!$message) {
                $message = isset($this->api_error[$status]) ? $this->api_error[$status] : '';
            }
            echo json_encode(array(
                    'status' => $status,
                    'message' => $message,
                    'data' => $data
                )
            );
        }
        die;
    }

    public function customSlug($string, $character = '-') {
        $string = $this->stripUnicode($string);
        $string = Inflector::slug($string, $character);
        return strtolower($string);
    }

    public function stripUnicode($string) {
        $unicode = array(
            'a' => 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ|Đ',
            'e' => 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'ì|í|ị|ỉ|ĩ|Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ',
            'y' => 'ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ'
        );
        foreach ($unicode as $nonUnicode => $uni) {
            $string = preg_replace("/($uni)/", $nonUnicode, $string);
        }
        return $string;
        //exit;
    }

    public function getRoleOpenInList($listUserJoined = [],$project_id = 0, $project_role_id = 0){
        $counter = 0;
        foreach ($listUserJoined as $row){
            if ($row['project_id'] == $project_id && $row['status'] == 1 && $row['project_role_id'] == $project_role_id){
                $counter++;
            }
        }
        return $counter;
    }


    /**
     * get Content notification send mail
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getContentNotifySendMail($actionName = NULL, $type = 0, $optionData = NULL, $userActionId = 0) {
        $sHtml = '';
        if ($actionName && $type) {
            $NameFirst = $actionName;
            $optionsArr = json_decode($optionData);

            // GET PROJECT INFO
            $ProjectName = '';
            if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                if (isset($project->title))
                    $ProjectName = $project->title;
            }

            // GET ROLES
            $RoleName = '';
            if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                $RolesTable = TableRegistry::get('Roles');
                $Role = $RolesTable->get($optionsArr->role_id);
                if(isset($Role->role) && !empty($Role->role)){
                    $RoleName = $Role->role;
                }
            }

            $message = '';
            switch ($type) {
                case '1':
                    $linkAcceptAction = ROOT_URL . 'projectActions/acceptJoinRole?project_role_id=' . $optionsArr->project_role_id . '&user_action_id=' . $userActionId;
                    $linkDenyAction   = ROOT_URL . 'projectActions/denyJoinRole?project_role_id=' . $optionsArr->project_role_id . '&user_action_id=' . $userActionId;
                    $message          = __('<strong>%s</strong> wants to join project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to accept.</p><p><a target="_blank" href="%s">Click here</a> to deny.</p>');
                    $sHtml            = sprintf($message, $NameFirst, $ProjectName, $linkAcceptAction, $linkDenyAction);
                    break;
                case '2':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> đã bình luận về doanh nghiệp <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '3':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> applied for your project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '4':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has joined project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '5':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> accepted to add you in project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '6':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> replied your comment on project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '7':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> just loved project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '8':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> just followed project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '9':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has uploaded a file on project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '10':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('Project owner <strong>%s</strong> has updated project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                //N has followed you
                case '12':
                    $message = __('<strong>%s</strong> has followed you');
                    $sHtml   = sprintf($message, $NameFirst);
                    break;
                //You are offered to be <role> in the project Y
                case '13':
                    $message = __('You are offered to be <strong>%s</strong> in the project <strong>%s</strong>');
                    $sHtml   = sprintf($message, $RoleName, $ProjectName);
                    break;
                //N has been offered <role> in  project Y
                case '14':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has been offered <strong>%s</strong> in project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $RoleName, $ProjectName, $link);
                    break;
                 //N has been send chat message
                case '15':
                    $roomId = (isset($optionsArr->room_id)) ? $optionsArr->room_id : 0;
                    $link = ROOT_URL . 'chat-room/' . $project->id . '/' . $roomId;
                    $message = __('<strong>%s</strong> đã nhắn tin cho bạn trong doanh nghiệp <strong>%s</strong><br/><p><a target="_blank" href="%s">Click vào đây</a> để xem chi tiết tin nhắn</p>');
                    $sHtml = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                //B has created new project X
                case '16':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has created new project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst,$ProjectName, $link);
                    break;
                //B has invited you join a project
                case '17':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has invited you to join a project<br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $link);
                    break;
				case '18':
				$link = \Cake\Utility\Inflector::slug($project->title, '-');
				$link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
				$message = __('<strong>%s</strong> has update project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
				$sHtml = sprintf($message, $NameFirst,$ProjectName, $link);
				break;
                    $sHtml = __('Send notification content');
                    break;

				case '20':
                    $link = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link = ROOT_URL . 'projects/' . $link . '-' . $project->id;
                    $message = __('Project owner <strong>%s</strong> has added new <strong>%s</strong> of %s <br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml = sprintf($message, $NameFirst, $role, $ProjectName, $link);
                    break;
                    $sHtml = __('Send notification content');
                    break;
            }
        }
        return $sHtml;
    }

    /**
     * check Token
     * @param int $iUserId - user id
     * @return json
     */
    public function checkToken(){
        $sToken  = '';
        $iUserId = 0;
        if($this->request->is('post')){

            $oTokenTable  = TableRegistry::get('Tokens');
            $aRequestData = $this->request->data;
            $sToken       = (isset($aRequestData['token']) ? $aRequestData['token'] : '');

            $aToken = $oTokenTable->find('all', array(
                'conditions' => array( 'token' => $sToken ),
            ));

            $aToken  = $aToken->first();
            $iUserId = empty($aToken) ? null : $aToken->user_id;
        }

        if (!$iUserId) {
            $this->_message   = __('Unable to authorize your request.');
            $this->_data      = array();
            $this->_bContinue = false;
            $this->_status    = 1;
        }

        return $iUserId;
    }


       public function getTimeAgo($datetimeInput = NULL) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $currentDateTime = date('Y-m-d H:i:s');
        $days = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60 * 24);
        if ($days >= 1) {
            return round($days) . __(' days ago');
        } else {
            $hours = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60);
            if ($hours >= 1) {
                return round($hours) . __(' hours ago');
            } else {
                $minutes = (strtotime($currentDateTime) - strtotime($datetimeInput)) / 60;
				if($minutes < 1) {
                return 0 . __(' minute ago');
				}
				else if($minutes < 2) {
                return 1 . __(' minute ago');
				}
				else
				{
					 return round($minutes) . __(' minutes ago');
				}
            }
        }
    }

    public function sendNotificationForUserFollow($actionType = 0, $optionData = [], $actionUserId = 0,$role = '')
    {
		$notificationTable = TableRegistry::get('Notifications');
        $followUserTable = TableRegistry::get('Followings');
		$this->loadModel('UserProjectActions');
		$listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' =>$optionData['project_id'],
     'status' => 1, 'action_type' => 2]);
        $followerList = $followUserTable->getFollowUsers(['following_id' => $actionUserId, 'connection' => 2]);
		$userid  = array();
	    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
		$this->loadModel('Projects');
		$currentProject = $this->Projects->find('all', ['conditions' => ['Projects.id' => $optionData['project_id']]])->contain('Users')->first();

		foreach ($followerList as $row) {
            $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row['user_id']]])->first();
            $checkFieldList = $this->getFieldCheckNotificationSettingByActionType($actionType);
            $checkField = $checkFieldList['mobile'];
            $userid[] = $row->id;
			if (!empty($userNotificationSetting) && $userNotificationSetting->$checkField == 1 && $row['user_id'] != $currentProject->user_id) {
                $notificationData = $notificationTable->newEntity();
                $notificationData->user_id = $row['user_id'];
                $notificationData->action_user_id = $actionUserId;
                $notificationData->project_id = intval($optionData['project_id']);
                $notificationData->option_data = json_encode($optionData);
                $notificationData->action_type = $actionType;
                $notificationData->created = date('Y-m-d H:i:s');
                $notificationTable->save($notificationData);
				$this->PushNoti($actionType,['project_id' => $optionData['project_id']],$row['user_id'],$actionUserId);
            }
        }
        $cronjobsTable = TableRegistry::get('Cronjobs');
        $cronjobData = $cronjobsTable->newEntity();
        $cronjobData->cronjob_name = 'send_notification_for_user_follow';
        $cronjobData->option_data = json_encode($optionData);
        $cronjobData->created = date('Y-m-d H:i:s');
        $cronjobsTable->save($cronjobData);
        return TRUE;


    }

    public function getFieldCheckNotificationSettingByActionType($type = 1) {
        $field = ['email' => 'member_join_project_email_status', 'mobile' => 'member_join_project_mobile_status'];
        switch ($type) {
            case 1:
                $field = ['email' => 'member_join_project_email_status', 'mobile' => 'member_join_project_mobile_status'];
                break;
            case 2:
                $field = ['email' => 'replies_comment_email_status', 'mobile' => 'replies_comment_mobile_status'];
                break;
            case 3:
            case 4:
            case 5:
            case 11:
                $field = ['email' => 'member_join_project_email_status', 'mobile' => 'member_join_project_mobile_status'];
                break;
            case 6:
                $field = ['email' => 'replies_comment_email_status', 'mobile' => 'replies_comment_mobile_status'];
                break;
            case 7:
            case 8:
            case 9:
            case 10:
            case 13:
            case 14:
            case 16:
                $field = ['email' => 'project_update_email_status', 'mobile' => 'project_update_mobile_status'];
                break;
            case 12:
                $field = ['email' => 'new_follower_email_status', 'mobile' => 'new_follower_mobile_status'];
                break;
            case 15:
                $field = ['email' => 'private_message_email_status', 'mobile' => 'private_message_mobile_status'];
                break;
        }
        return $field;
    }

    public function sendNotificationForFollowProject($projectId, $actionType)
    {
        if (!empty($projectId)) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'condition' => ['token' => $data['token']]
            ])->first();
//            $userAction = $this->Auth->user();
            if (!empty($token)) {
                $userAction = $token->user_id;
                $this->loadModel('UserProjectActions');
                $listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $projectId, 'status' => 1, 'action_type' => 2]);
                if (count($listUserfollows) > 0) {
                    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                    foreach ($listUserfollows as $Userfollow) {
                        if ($Userfollow->user_id != $userAction['id']) {
                            $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $Userfollow->user_id]])->first();
                            $optionData = json_encode(['project_id' => $Userfollow->project_id]);
                            $isSendNotificationByMail = FALSE;
                            $isSendNotificationByMobile = FALSE;
                            // check notification setting
                            if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                                $isSendNotificationByMail = TRUE;
                            }
                            if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                                $isSendNotificationByMobile = TRUE;
                            }
                            if ($isSendNotificationByMobile == TRUE) {
                                // add notification on web
                                $notificationTable = TableRegistry::get('Notifications');
                                $notificationData = $notificationTable->newEntity();
                                $notificationData->user_id = $Userfollow->user_id;
                                $notificationData->action_user_id = $userAction['id'];
                                $notificationData->project_id = $Userfollow->project_id;
                                $notificationData->option_data = $optionData;
                                $notificationData->action_type = $actionType;
                                $notificationData->created = date('Y-m-d H:i:s');
                                $notificationTable->save($notificationData);
								$this->PushNoti($actionType,['project_id' => $Userfollow->project_id],$Userfollow->user_id,$userAction['id']);

                            }
                            if ($isSendNotificationByMail == TRUE) {
                                // send notification via mobile
                                $userTable = TableRegistry::get('Users');
                                $userInfo = $userTable->find('all', ['conditions' => ['id' => $Userfollow->user_id]])->first();
                                $template = 'notification';
                                $from = [EMAIL_LOGIN => __('We The Projects')];
                                $subject = __('[SME] Notification on SME');

                                // GET CONTENT MAIL
                                $contentMail = __('Send notification content');
                                if (isset($actionType) && !empty($actionType)) {
                                    $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                    $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                                }
                                // END: GET CONTENT MAIL

                                $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                            }
                        }
                    }
                    $responeData = ['status' => 1, 'message' => __('Success')];
                    echo json_encode($responeData);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(__(0, 'No match data'));
        }
    }

//nam code
// 29-03-2017
 public function sendNotificationForCreator($projectId, $actionType,$userid) {
				$projectTable = TableRegistry::get('Projects');
                $currentProject = $projectTable->get($projectId);
				//Send notification Creator
				if (!empty($currentProject)) {
						$actionType = $actionType;
						$userTable = TableRegistry::get('Users');
						$userAction = $userTable->find('all', ['conditions' => ['id' => $userid]])->first();
						$userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
							if ($currentProject->user_id != $userAction['id']) {
								$userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $currentProject->user_id]])->first();
								$optionData = json_encode(['project_id' =>$projectId]);
								$isSendNotificationByMail = FALSE;
								$isSendNotificationByMobile = FALSE;
								// check notification setting
								if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
									$isSendNotificationByMail = TRUE;
								}
								if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
									$isSendNotificationByMobile = TRUE;
								}
								if ($isSendNotificationByMobile == TRUE) {
									// add notification on web
									$notificationTable = TableRegistry::get('Notifications');
									$notificationData = $notificationTable->newEntity();
									$notificationData->user_id = $currentProject->user_id;
									$notificationData->action_user_id = $userAction['id'];
									$notificationData->project_id = $projectId;
									$notificationData->option_data = $optionData;
									$notificationData->action_type = $actionType;
									$notificationData->created = date('Y-m-d H:i:s');
									$notificationTable->save($notificationData);
									$this->PushNoti($actionType,['project_id' => $projectId],$currentProject->user_id,$userAction['id']);

								}
								if ($isSendNotificationByMail == TRUE) {
									// send notification via mobile
									$userTable = TableRegistry::get('Users');
									$userInfo = $userTable->find('all', ['conditions' => ['id' => $currentProject->user_id]])->first();
									$template = 'notification';
									$from = [EMAIL_LOGIN => __('We The Projects')];
									$subject = __('[SME] Notification on SME');

									// GET CONTENT MAIL
									$contentMail = __('Send notification content');
									if (isset($actionType) && !empty($actionType)) {
										$name = $userAction['first_name'] . ' ' . $userAction['last_name'];
										$contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
									}
									// END: GET CONTENT MAIL

									$this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
								}
							}

				}
 }
//end code

// nam code
// 29-03-2017
 public function sendNotificationForJoinProject($projectId, $actionType,$userid,$role = '') {
	   if (!empty($projectId)) {
			$role_name = '';
			if(isset($role) && !empty($role))
			{
			  $role_id = $role;
			  $this->loadModel('Roles');
			  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
				foreach($roles as $rol)
				{
					$role_name = $rol->role;
				}
			}
            $userTable = TableRegistry::get('Users');
            $userAction = $userTable->find('all', ['conditions' => ['id' => $userid]])->first();
            $this->loadModel('UserProjectActions');
		    $this->loadModel('UsersProjects');
			$listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $projectId, 'status' => 1, 'action_type' => 2]);
			$listCollaborators = $this->UsersProjects->getUserProjectsJoin(['project_id' => $projectId, 'status' => 1, 'type' => 2]);
			$userid = array();
			foreach($listCollaborators as $Collaborator)
			{
				$userid[] = $Collaborator->user_id;
			}
			if (count($listCollaborators) > 0) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                foreach ($listCollaborators as $collaborator) {
                    if ($collaborator->user_id != $userid) {
                        $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $collaborator->user_id]])->first();
                        $optionData = json_encode(['project_id' => $collaborator->project_id ,'role_id' => $role]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $collaborator->user_id;
                            $notificationData->action_user_id = $userAction['id'];
                            $notificationData->project_id = $collaborator->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType,['project_id' => $collaborator->project_id],$collaborator->user_id, $userAction['id'], $role_name);
                   }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $collaborator->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
                }
            }
			if (count($listUserfollows) > 0) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                foreach ($listUserfollows as $Userfollow) {
                    if ($Userfollow->user_id != $userAction['id'] && !in_array($Userfollow->user_id, $userid)) {
						$userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $Userfollow->user_id]])->first();
					    $optionData = json_encode(['project_id' => $Userfollow->project_id]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $Userfollow->user_id;
                            $notificationData->action_user_id = $userAction['id'];
                            $notificationData->project_id = $Userfollow->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType,['project_id' => $Userfollow->project_id],$Userfollow->user_id,$userAction['id'], $role_name);
                    }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $Userfollow->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
                }
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
	}

//end Nam
    public function writeLogSystem($fileName = '', $className = '', $functionName = '', $requestArr = [] ,$logArr = [], $errorCode = '')
            {
                if (empty($logArr) || empty($requestArr)) {
                    return false;
                } else {
                    $fileLog = LOGS . $fileName;
                    $log_date = date("Y/m/d H:i:s");
                    $requestData = http_build_query($requestArr);
                    $responsData = "[" . $errorCode . "]";
                    foreach ($logArr as $item) {
                        $item = (array) $item;
                        $responsData = $responsData . "[" . $item['code'] . '=' . $item['column'] . "]";
                    }
            $error_contents = '['.$className.']'.'['.$functionName.']'.'['.$log_date.']'.$requestData;
            $error_contents .= '#'.$responsData.PHP_EOL;
            @file_put_contents($fileLog, $error_contents, FILE_APPEND);
            return true;
        }
    }


	public function PushNoti($actionType = 0, $optionData = [], $actionUserId = 0,$userid = 0, $role = '')
	{
		    $actionUserId = $actionUserId;
			$listNotifications = TableRegistry::get('Notifications');
			$listNotifications = $listNotifications->find('all', ['conditions' => ['user_id' => $actionUserId, 'read_flg' => 0]])->toArray();
			$number = count($listNotifications);
			$userTable = TableRegistry::get('Users');
			$userAction = $userTable->find('all', ['conditions' => ['id' => $userid]])->first();
		    $tokenTable = TableRegistry::get('Tokens');
			$userInfo  = $tokenTable->find('all', [
					'conditions' => ['user_id' => $actionUserId, 'platform' => 0],
				])->first();
			$message = $this->getTypeNotificationContentPush($userAction['first_name'] . ' ' . $userAction['last_name'], $actionType, $optionData,$role);
			if ($userInfo['token_device'] != '') {
			   $this->PushNotification->send($userInfo['token_device'], $message,$number);
			}
	}

	public function getTypeNotificationContentPush($actionName = null, $type = 1, $option_data = null, $role = '')
    {
		$aRequestData = $this->request->data;
        $result = '';
		if(isset($aRequestData['language']) && !empty($aRequestData['language']))
		{
			$aRequestData['language'] = $aRequestData['language'];
		}
		else
			{
				$aRequestData['language'] = '';
			}
		if($aRequestData['language'] == 'ja')
		{
        switch ($type) {
		case 1:
                // Request want to join project
                $message = __('ユーザー %s さん は %s として プロジェクト %s に 参加を希望しています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName,$role, $project->title);
                break;
            case 2:
                // alert comment on project
                $message = __('%s は プロジェクト %s に コメントしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s has applied for your project');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $message = __('%s さん  が プロジェクト %s に %s として 参加しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title, $role);
                break;

            case 5:
                // alert accept join project
                $message = __('プロジェクト管理者 %s さん が、 あなた を さん が、 あなた を %s として プロジェクト %s に 参加することを認証しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName,$role,$project->title);
                break;
            case 6:
                // alert reply comment
                $message = __('%s さん が、プロジェクト %s の コメント に 返信しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $message = __('%s さん が、プロジェクト %s に イイネ！しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert follow project
                $message = __('%s さん は、プロジェクト %s を フォローしています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $message = __('%s さん が プロジェクト %s に ファイル を アップロードしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $message = __('プロジェク管理者 %s さん が プロジェクト %s を 更新しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $message = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 12: // N has followed you
                $message = __('%s has followed you');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('あなたは、プロジェクト %s  の %s に なるように提案されています。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$result = sprintf($message, $project->title, $role);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s さん は、プロジェクト %s  の %s に なるように提案されています');
                $projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s さん が、 プロジェクト %s の チャットで 新しいメッセージ を あなた に 送信しました。');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s さん が 新しいプロジェクト %s を 作成しました');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $message = __('%s  さん が プロジェクト %s に 参加するよう に あなた を 招待しました');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 18: // B has update project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('プロジェク管理者 %s さん が プロジェクト %s を 更新しました。');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 19: // B has asset project X
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$message = __('%s has asset project %s');
				$result = sprintf($message, $actionName, $project->title);
			break;
			case 20: // B has add
				$message = __('プロジェクト管理者 %s さん が、プロジェクト %s の 新しい %s を 追加しました。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($option_data['project_id']);
				$result = sprintf($message, $actionName, $project->title, $role);
				break; }
		}
		else
		{
		switch ($type) {
            case 1:
                // Request want to join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s muốn tham gia doanh nghiệp %s với vai trò %s');
                $result = sprintf($message, $actionName, $project->title,$role);
                break;
            case 2:
                // alert comment on project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã bình luận về doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s has applied for your project');
                $result = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s has joined on project %s as %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 5:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('Chủ doanh nghiệp %s đã đồng ý thêm bạn %s với vai tro %s');
                $result = sprintf($message, $actionName, $project->title, $role);
                break;
            case 6:
                // alert reply comment
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s has replied your comment on project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã thích doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã theo dõi doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $projectTable = TableRegistry::get('Projects');
                $message = __('%s has uploaded a file on project %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $projectTable = TableRegistry::get('Projects');
                $message = __('Project owner %s has updated project %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $message = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('%s has followed you');
                $result = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('You are offered to be %s in the project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($option_data['project_id']) && !empty($option_data['project_id'])) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($option_data['project_id']);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $role, $ProjectName);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('%s has been offered %s in project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($option_data['project_id']) && !empty($option_data['project_id'])) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($option_data['project_id']);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $actionName, $role, $ProjectName);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s đã nhắn tin cho bạn trong doanh nghiệp %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s has created new project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $message = __('%s has invited you join a project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
            case 18: // Update project
                $projectUpdateTable = TableRegistry::get('ProjectUpdates');
                $projectUpdate = $projectUpdateTable->get($option_data['project_id']);
                $message = __('%s has update project %s');
                $result = sprintf($message, $actionName, $projectUpdate->title);
                break;
            case 19: // B has asset project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('%s has asset project %s');
                $result = sprintf($message, $actionName, $project->title);
                break;
			case 20: // B has add
				$message = __('Project owner %s has added new %s of %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($option_data['project_id']);
                $result = sprintf($message, $actionName, $role, $project->title);
                break;
        }
		}
        return $result;
    }


	//End Nam
}
