<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class ProjectsController extends ApiController
{

    public $components = array('RequestHandler', 'PushNotification');

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Email');
        $this->loadComponent('Xls');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    //fee, staff
    public function getListProjectsByAdminSME()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $userTable = TableRegistry::get('Users');
                $adminType = $userTable->find('all', ['conditions' => ['id' => $userId], 'fields' => 'admin'])->first();
                // var_dump($adminType['admin']);
                if ($adminType['admin'] == 5 || $adminType['admin'] == 4 || $adminType['admin'] == 3 || $adminType['admin'] == 2 || $adminType['admin'] == 1) {
                    $projectsTable = TableRegistry::get('Projects');
                    $projects = $projectsTable->find('all')->toArray();
                    if ($this->request->is('post')) {
                        $group = $this->request->data('group');
                        $fee_status = $this->request->data('fee_status');
                        $location = $this->request->data('location');
                        $category = $this->request->data('category');
                        switch ($fee_status) {
                            case 1:
                                $fee_status = 0;
                                break;
                            case 2:
                                $fee_status = 1;
                                break;
                            default:
                                $fee_status = 4;
                                break;
                        }
                        $search = [];
                        if (!empty($group && isset($group))) {
                            $search['group_id'] = $group;
                        }
                        if (isset($fee_status) && $fee_status != 4) {
                            $search['fee_status'] = $fee_status;
                        }
                        if (!empty($category) && isset($category)) {
                            $search['category_id'] = $category;
                        }
                        if (!empty($location) && isset($location)) {
                            $search['district_id'] = $location;
                        }
                        $projects = $this->Projects->find('all', ['conditions' => $search]);
                    }
                    foreach ($projects as $items) {
                        $data['id'] = $items['id'];
                        $data['title'] = $items['title'];
                        if ($items['fee_status'] == 0)
                            $data['fee_status'] = "Chưa đóng hội phí";
                        else
                            $data['fee_status'] = "Đã đóng hội phí";
                        $GroupTable = TableRegistry::get('Groups');
                        $group = $GroupTable->find('list', ['conditions' => ['id' => $items['group_id']], 'field' => 'title']);
                        $data['BCH'] = $group;
                        $dataRespone[] = $data;
                    }
                }
                $this->responseApi(1, __('Success'), $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function toBeFee()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $userTable = TableRegistry::get('Users');
                $adminType = $userTable->find('all', ['conditions' => ['id' => $userId], 'fields' => 'admin'])->first();
                // var_dump($adminType['admin']);
                if ($adminType['admin'] == 5 || $adminType['admin'] == 4 || $adminType['admin'] == 3 || $adminType['admin'] == 2 || $adminType['admin'] == 1) {
                    $this->loadModel('Projects');
                    $projects = $this->Projects->get($data['project_id']);
                    $dataProject['id'] = $data['project_id'];
                    if ($projects['fee_status'] == 1) {
                        $dataProject['fee_status'] = 0;
                    } else {
                        $dataProject['fee_status'] = 1;
                    }

                    $projects = $this->Projects->patchEntity($projects, $dataProject);
                }
                if ($this->Projects->save($projects)) {
                    $dataRespone['status'] = "Đã cập nhật hội phí cho doanh nghiệp" . $projects['title'];
                    $this->responseApi(1, __('Success'), $dataRespone);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function toBeStaff()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $userTable = TableRegistry::get('Users');
                $adminType = $userTable->find('all', ['conditions' => ['id' => $userId], 'fields' => 'admin'])->first();
                // var_dump($adminType['admin']);
                if ($adminType['admin'] == 5) {
                    $this->loadModel('Projects');
                    $projects = $this->Projects->get($data['project_id']);
                    $dataProject['id'] = $data['project_id'];
                    if ($projects['group_id'] == 3) {
                        $dataProject['group_id'] = 0;
                    } else {
                        $dataProject['group_id'] = 3;
                    }

                    $projects = $this->Projects->patchEntity($projects, $dataProject);
                }
                if ($this->Projects->save($projects)) {
                    $dataRespone['status'] = "Đã cập nhật vai trò ban chấp hành cho doanh nghiệp" . $projects['title'];
                    $this->responseApi(1, __('Success'), $dataRespone);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    //
    public function getlistproject()
    {
        $this->autoRender = false;

        $this->loadModel('Projects');
        $project = $this->Projects->find('all', ['fields' => ['id', 'title']]);
        if (!empty($project)) {
            foreach ($project as $value) {
                $data['id'] = $value->id;
                $data['title'] = $value->title;
                $dataRespone[] = $data;
            }
            $this->responseApi(1, __('Danh sach doanh nghiep'), $dataRespone);
        } else {
            $this->responseApi(0, ('Không tìm thấy doanh nghiệp nào'), []);

        }
    }

    public function getlistOwnerProject()
    {
        $this->autoRender = false;
        $userId = $this->request->data['userID'];
        $this->loadModel('Projects');
        $project = $this->Projects->find('all', ['conditions'=>['user_id'=>$userId],'fields' => ['id', 'title']]);

        if (!empty($project)) {
            foreach ($project as $value) {
                $data['id'] = $value->id;
                $data['title'] = $value->title;
                $dataRespone[] = $data;
            }
            $this->responseApi(1, __('Danh sach doanh nghiep của tôi'), $dataRespone);
        } else {
            $this->responseApi(0, ('Không tìm thấy doanh nghiệp nào'), []);

        }
    }

    public function getListCategory()
    {
        $this->autoRender = false;

        $this->loadModel('Categories');
        $cate = $this->Categories->find('all', ['fields' => ['id', 'name']]);
        if (!empty($cate)) {
            foreach ($cate as $value) {
                $data['id'] = $value->id;
                $data['title'] = $value->name;
                $dataRespone[] = $data;
            }
            $this->responseApi(1, __('Danh sách lĩnh vực'), $dataRespone);
        } else {
            $this->responseApi(0, ('Không tìm thấy lĩnh vực nào'), []);

        }
    }

    public function getListBCH()
    {
        $this->autoRender = false;

        $this->loadModel('Groups');
        $bch = $this->Groups->find('all', ['fields' => ['id', 'name']]);
        if (!empty($bch)) {
            foreach ($bch as $value) {
                $data['id'] = $value->id;
                $data['title'] = $value->name;
                $dataRespone[] = $data;
            }
            $this->responseApi(1, __('Danh sách BCH'), $dataRespone);
        } else {
            $this->responseApi(0, ('Không tìm thấy'), []);

        }
    }

    public function getListRole()
    {
        $this->autoRender = false;

        $this->loadModel('Users_type');
        $type = $this->Users_type->find('all', ['fields' => ['id', 'name']]);
        if (!empty($type)) {
            foreach ($type as $value) {
                $data['id'] = $value->id;
                $data['title'] = $value->name;
                $dataRespone[] = $data;
            }
            $this->responseApi(1, __('Danh sách BCH'), $dataRespone);
        } else {
            $this->responseApi(0, ('Không tìm thấy'), []);

        }
    }

    public function getListLocation()
    {
        $this->autoRender = false;

        $this->loadModel('Districts');
        $locations = $this->Districts->find('all', [
            'conditions' => ['country_id' => 235],
            'order' => ['name' => 'ASC'],
            'fields' => ['id', 'name']
        ]);
        // echo json_encode($locations);
        if (!empty($locations)) {
            foreach ($locations as $value) {
                $data['id'] = $value->id;
                $data['name'] = $value->name;
                $dataRespone[] = $data;
            }
            $this->responseApi(1, __('Danh sách vùng'), $dataRespone);
        } else {
            $this->responseApi(0, ('Không tìm thấy vùng nào'), []);

        }
    }

    public function getListMyProject()
    {
        $this->autoRender = false;
        // $id = $this->request->data('user_id');
        $this->loadModel('Projects');

        $data1 = $this->request->data;
        $tokenTable = TableRegistry::get('Tokens');
        $token = $tokenTable->find('all', [
            'conditions' => ['token' => $data1['token']],
        ]);
        $token = $token->first();

        if (!empty($token)) {
            $projects = $this->Projects->find('all', [
                'conditions' => ['user_id' => $token->user_id],
                // 'order' => ['name' => 'ASC'],
                'fields' => ['id', 'title']
            ]);
            // echo json_encode($locations);
            $dataRespone = [];
            // $dataRespone['user_id'] = $projects;
            if (!empty($projects)) {
                foreach ($projects as $value) {
                    $data['id'] = $value->id;
                    $data['title'] = $value->title;
                    $dataRespone[] = $data;
                }
                $this->responseApi(1, __('Danh sách doanh nghiệp của bạn'), $dataRespone);
            } else {
                $this->responseApi(0, ('Không tìm thấy doanh nghiệp nào'), []);

            }
        } else {
            $this->responseApi(1031);
        }
    }

    public function action()
    {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $type = $data['action_type'];

            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {

                $this->loadModel('UserProjectActions');
                $this->loadModel('Projects');
                $this->loadModel('Users');
                $this->loadModel('Token');
                $project = $this->Projects->get($data['project_id'], ['fields' => ['title', 'user_id'], 'contain' => ['Users']]);
                $blocksTable = TableRegistry::get('Blocks');
                $block = $blocksTable->find()
                    ->where(['blocked_id' => $token->user_id, 'user_id' => $project['user_id']])
                    ->first();
                $blockuser = $blocksTable->find()
                    ->where(['user_id' => $token->user_id, 'blocked_id' => $project['user_id']])
                    ->first();
                $projectAction = $this->UserProjectActions->getUserProjectAction(['user_id' => $token->user_id, 'project_id' => $data['project_id'], 'action_type' => $type]);
                if ((!empty($block) || !empty($blockuser)) && $type == 2) {
                    $this->responseApi(0, __("you can't follow this project"));
                    die();
                } else if ((!empty($block) || !empty($blockuser)) && $type == 1) {
                    $this->responseApi(0, __("you can't like this project"));
                    die();
                }

                //set name for push
                $collaboratorName = !empty($project['user']['name']) ? $project['user']['name'] : $project['user']['first_name'] . ' ' . $project['user']['last_name'];
                if (empty($projectAction)) {
                    $dataAction = ['action_type' => $type, 'project_id' => $data['project_id'], 'user_id' => $token->user_id, 'status' => 1, 'created' => date('Y-m-d H:i:s')];
                    $respone = $this->UserProjectActions->addProjectAction($dataAction);
                    if ($respone == 'TRUE') {
                        if ($type == 1) {
                            $this->sendNotificationForJoinProject($data['project_id'], 7, $token->user_id);
                            $this->sendNotificationForCreator($data['project_id'], 7, $token->user_id);
                            $this->responseApi(1, __('You have been liked this project.'));
                            //set message for push
                            $message = $collaboratorName . __(' liked your project.');
                        } elseif ($type == 2) {
                            $this->sendNotificationForJoinProject($data['project_id'], 8, $token->user_id);
                            $this->sendNotificationForCreator($data['project_id'], 8, $token->user_id);
                            $this->responseApi(1, __('You have been followed this project.'));
                            //set message for push
                            $message = $collaboratorName . __(' followed your project.');
                        }
                        //send notification for owner project
                        if (!empty($project['user']['device_token'])) {
                            $this->PushNotification->send($project['user']['device_token'], $message);
                        }
                    } else {
                        $this->responseApi(1030);
                    }
                } else {
                    $status = (isset($projectAction['status']) && $projectAction['status'] == 0) ? 1 : 0;
                    $respone = $this->UserProjectActions->updateProjectAction($projectAction['id'], ['status' => $status]);
                    if ($respone == 'TRUE') {
                        if ($status == 0 && $type == 1) {
                            $this->responseApi(1, __('You have been un-liked this project.'));
                        } elseif ($status == 1 && $type == 1) {
                            $this->sendNotificationForJoinProject($data['project_id'], 7, $token->user_id);
                            $this->sendNotificationForCreator($data['project_id'], 7, $token->user_id);
                            $this->responseApi(1, __('You have been liked this project.'));
                            $message = $collaboratorName . __(' liked your project.');
                        } elseif ($status == 0 && $type == 2) {
                            $this->responseApi(1, __('You have been un-followed this project.'));
                        } elseif ($status == 1 && $type == 2) {
                            $this->sendNotificationForJoinProject($data['project_id'], 8, $token->user_id);
                            $this->sendNotificationForCreator($data['project_id'], 8, $token->user_id);
                            $this->responseApi(1, __('You have been followed this project.'));
                            $message = $collaboratorName . __(' followed your project.');
                        }
                        //send notification for owner project
                        if (!empty($project['user']['device_token'])) {
                            $this->PushNotification->send($project['user']['device_token'], $message);
                        }
                    } else {
                        $this->responseApi(1030);
                    }
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function showTeam()
    {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            $blocksTable = TableRegistry::get('Blocks');
            if (!empty($token)) {
                // Get tab member in the project
                $this->loadModel('UsersProjects');
                $this->loadModel('Projects');
                $usersProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id' => $data['project_id'], 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);

                $user_creator = $this->Projects->find()
                    ->where(['user_id' => $token->user_id, 'id' => $data['project_id']])
                    ->first();
                $usersjoinProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id' => $data['project_id'], 'user_id' => $token->user_id, 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);
                $dataRespone = [];
                foreach ($usersProjects as $row) {
                    $block = $blocksTable->find()
                        ->where(['blocked_id' => $token->user_id, 'user_id' => $row['user_id']])
                        ->first();
                    $blockuser = $blocksTable->find()
                        ->where(['user_id' => $token->user_id, 'blocked_id' => $row['user_id']])
                        ->first();
                    if (empty($blockuser) && empty($block) && (!empty($usersjoinProjects) || !empty($user_creator)) && $token->user_id != $row['user_id']) {
                        $data = [
                            'user_id' => $row['user_id'],
                            'role' => $row['role']['role'],
                            'name' => $row['name'],
                            'user_picture' => !empty($row['user_picture']) ? $row['user_picture'] : !empty($row['user']['avatar']) ? $row['user']['avatar'] : ROOT_URL . 'img/avatars_default.jpg',
                            'can_chat' => 1,
                        ];
                    } else {
                        $data = [
                            'user_id' => $row['user_id'],
                            'role' => $row['role']['role'],
                            'name' => $row['name'],
                            'can_chat' => 0,
                            'user_picture' => !empty($row['user_picture']) ? $row['user_picture'] : !empty($row['user']['avatar']) ? $row['user']['avatar'] : ROOT_URL . 'img/avatars_default.jpg',
                        ];
                    }
                    $dataRespone[] = $data;
                }
                $this->responseApi(1, 'Success', $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        }
    }

    public function getListProjectUpdate()
    {
        // Get list updates of the project
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                $this->loadModel('ProjectUpdates');
                $projectUpdates = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['ProjectUpdates.project_id' => $data['project_id'], 'ProjectUpdates.status' => 1], ['Projects']);
                if (count($projectUpdates) > 0) {
                    foreach ($projectUpdates as $updates) {
                        $this->_data['projectUpdate'][] = [
                            'id' => $updates->id,
                            'project_id' => $updates->project_id,
                            'title' => $updates->title,
                            'content' => $updates->content,
                            'status' => $updates->status,
                            'type' => $updates->type,
                            'created' => date('Y-m-d\TH:i:s', strtotime($updates->created))
                        ];
                        $this->_data['project'] = ['start_date' => ($updates->Project['start_date'] == '0000-00-00') ? '' : date('Y-m-d\TH:i:s', strtotime($updates->Project['start_date'])), 'end_date' => ($updates->Project['end_date'] == '0000-00-00') ? '' : $this->getTimeAgo($updates->Project['end_date'])];
                    }
                } else {
                    $this->_data['projectUpdate'] = [];
                    $this->_data['project'] = ['start_date' => '', 'end_date' => ''];
                }
                $this->responseApi(1, '', $this->_data);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function getProjectComment()
    {
        // Get list comments of the project
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            $this->_data['comment'] = [];
            if (!empty($token)) {
                $blockTable = TableRegistry::get('Blocks');
                $id = $data['project_id'];
                $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain(['Users'])->first();
                $blockUserList = $blockTable->find('all', ['conditions' => ['user_id' => $project['user_id'], 'blocked_id' => $token->user_id]])->toArray();
                $blockUser = $blockTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'blocked_id' => $project['user_id']]])->toArray();
                $this->loadModel('UserProjectActions');
                $this->loadModel('UsersProjects');
                $this->loadModel('Projects');
                $Userfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $data['project_id'], 'status' => 1, 'user_id' => $token->user_id, 'action_type' => 2]);
                $user_creator = $this->Projects->find()
                    ->where(['user_id' => $token->user_id, 'id' => $data['project_id']])
                    ->first();
                $usersjoinProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id' => $data['project_id'], 'user_id' => $token->user_id, 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);
                if (empty($Userfollows) && empty($user_creator) && empty($usersjoinProjects)) {
                    $this->responseApi(0, __('You can not comment this project'));
                }
                if (!empty($blockUserList) || !empty($blockUser)) {
                    $this->responseApi(0, __('You have been blocked by creator'));
                } else {
                    $this->loadModel('ProjectComments');
                    $listComments = $this->ProjectComments->getListCommentByProjectId($data['project_id']);
                    $dataRespone = [];
                    foreach ($listComments as $row) {
                        $blockUserC = $blockTable->find('all', ['conditions' => ['user_id' => $row->user['id'], 'blocked_id' => $token->user_id]])->toArray();
                        $blockUserC1 = $blockTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'blocked_id' => $row->user['id']]])->toArray();
                        if (!empty($blockUserC) || !empty($blockUserC1)) {
                            $canView = 0;
                        } else {
                            $canView = 1;
                        }
                        $dataRespone[] = [
                            'id' => empty($row->id) ? 0 : $row->id,
                            'project_id' => empty($row->project_id) ? 0 : $row->project_id,
                            'comment' => empty($row->comment) ? '' : $row->comment,
                            'created' => empty($row->created) ? date('Y-m-d H:i:s') : $this->getTimeAgo($row->created),
                            'parent_comment_id' => empty($row->parent_comment_id) ? 0 : $row->parent_comment_id,
                            'user' => [
                                'name' => $row->user['first_name'] . ' ' . $row->user['last_name'],
                                'avatar' => empty($row->user['avatar']) ? '' : $row->user['avatar'],
                                'user_id' => $row->user['id'],
                                'canView' => $canView
                            ]
                        ];
                    }
                    $this->_data['comment'] = $dataRespone;

                    $this->responseApi(1, '', $this->_data);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function addProjectUpdate()
    {
        // Add updates project
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('Projects');
                $this->loadModel('ProjectUpdates');
                $this->request->data['created'] = date('Y-m-d H:i:s');

                $project = $this->Projects->get($data['project_id'], ['fields' => ['title']]);
                $respone = $this->ProjectUpdates->addUpdate($data);
                if ($respone == 'TRUE') {
                    $this->loadModel('Users');
                    $this->loadModel('UsersProjects');
                    $this->loadModel('UsersProjectActions');
                    $userPro = $this->UsersProjects->find('all', ['fields' => ['project_id', 'user_id'], 'conditions' => ['project_id' => $data['project_id']]])->toArray();
                    $userFollow = $this->UserProjectActions->find('all', ['fields' => ['project_id', 'user_id'], 'conditions' => ['project_id' => $data['project_id'], 'action_type' => 2]])->toArray();
                    if (!empty($userPro)) {
                        foreach ($userPro as $key => $val) {
                            $user = $this->Users->get($val['user_id'], ['fields' => ['device_token']])->toArray();
                            if (!empty($user)) {
                                $this->PushNotification->send($user['device_token'], __($project->title . ' has been updated.'));
                            }
                        }
                    }
                    if (!empty($userFollow)) {
                        foreach ($userFollow as $item => $value) {
                            $users = $this->Users->get($value['user_id'], ['fields' => ['device_token']])->toArray();
                            if (!empty($users)) {
                                $this->PushNotification->send($users['device_token'], __($project->title . ' has been updated.'));
                            }
                        }
                    }
                    $this->responseApi(1, __('Project update has been created.'));
                } else {
                    $message = '';
                    foreach ($respone as $row) {
                        foreach ($row as $item) {
                            $message .= $item . ' ';
                        }
                    }
                    $this->responseApi(0, $message);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }
    //Nam
    // check user project
    function checkUserProject()
    {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                $this->loadModel('UserProjectActions');
                $this->loadModel('UsersProjects');
                $this->loadModel('Projects');
                $Userfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $data['project_id'], 'status' => 1, 'user_id' => $token->user_id, 'action_type' => 2]);
                $user_creator = $this->Projects->find()
                    ->where(['id' => $data['project_id']])
                    ->first();
                $usersjoinProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id' => $data['project_id'], 'user_id' => $token->user_id, 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);
                $dataRespone = [];
                if ($user_creator['user_id'] == $token->user_id) {
                    $userProject = 'isOwner';

                } else if (!empty($usersjoinProjects)) {
                    $userProject = 'isJoined';
                } else if (!empty($Userfollows)) {
                    $userProject = 'isFollower';
                } else {
                    $userProject = 'isStranger';
                }
                $Userprojects = $this->Projects->getAllProjectUsers($token->user_id);
                $UserprojectJoins = $this->UsersProjects->getAllProejectJoinUser($token->user_id);
                $projectIdUserJoin = array();
                $projectIdUser = array();
                $projectIdUserFollow = array();
                $projectIdUserFollowJoin = array();
                foreach ($Userprojects as $Userproject) {
                    $projectIdUser[] = $Userproject->id;
                }
                foreach ($UserprojectJoins as $UserprojectJoin) {
                    $projectIdUserJoin[] = $UserprojectJoin->project_id;
                }
                $UserprojectFollows = $this->Projects->getAllProjectUsers($user_creator['user_id']);
                $UserprojectFollowJoins = $this->UsersProjects->getAllProejectJoinUser($user_creator['user_id']);
                $isApplicant = 0;
                foreach ($UserprojectFollows as $Userproject) {
                    $projectIdUserFollow[] = $Userproject->id;
                }
                foreach ($UserprojectFollowJoins as $UserprojectFollowJoin) {
                    $projectIdUserFollowJoin[] = $UserprojectFollowJoin->project_id;
                }
                for ($i = 0; $i < count($projectIdUser); $i++) {
                    if (in_array($projectIdUser[$i], $projectIdUserFollowJoin)) {
                        $isApplicant = 1;
                    }
                }
                for ($i = 0; $i < count($projectIdUserJoin); $i++) {
                    if (in_array($projectIdUserJoin[$i], $projectIdUserFollow)) {
                        $isApplicant = 1;
                    }
                }
                if ($isApplicant == 1) {
                    $roleUser = 'isApplicant';
                } else {
                    $roleUser = 'isStranger';
                }
                $blocksTable = TableRegistry::get('Blocks');
                $blockUserList = $blocksTable->find('all', ['conditions' => ['user_id' => $user_creator['user_id'], 'blocked_id' => $token->user_id]])->toArray();
                $blockUser = $blocksTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'blocked_id' => $user_creator['user_id']]])->toArray();
                if (!empty($blockUserList) || !empty($blockUser)) {
                    $canView = (int)0;
                } else {
                    $canView = (int)1;
                }
                $dataRespone[] = [
                    'roleUserLogin' => $userProject,
                    'user_id' => $user_creator['user_id'],
                    'roleUser' => $roleUser,
                    'canView' => $canView,
                ];
                $this->responseApi(1, '', $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }

    }

    //End Nam
    public function addProjectComment()
    {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $blocksTable = TableRegistry::get('Blocks');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $id = $data['project_id'];
                $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain(['Users'])->first();
                $block = $blocksTable->find()
                    ->where(['blocked_id' => $token->user_id])
                    ->first();
                $blockuser = $blocksTable->find()
                    ->where(['user_id' => $token->user_id])
                    ->first();
                $data = $this->request->data;
                if (isset($data['project_id']) && !empty($data['project_id']) && isset($data['comment']) && !empty($data['comment'])) {
                    if ((!empty($block) || !empty($blockuser)) && $token->user_id == $block['blocked_id'] && $block['user_id'] == $project['user_id'] && $blockuser['user_id'] == $project['user_id']) {
                        $this->responseApi(0, __('You have been blocked by creator'));
                    } else {
                        $data['created'] = date('Y-m-d H:i:s');
                        $this->loadModel('ProjectComments');
                        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                            $ipRequest = $_SERVER['HTTP_CLIENT_IP'];
                        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                            $ipRequest = $_SERVER['HTTP_X_FORWARDED_FOR'];
                        } else {
                            $ipRequest = $_SERVER['REMOTE_ADDR'];
                        }
                        $data['ip_comment'] = $ipRequest;
                        $data['user_id'] = $token->user_id;
                        $this->sendNotificationForJoinProject($id, 2, $token->user_id);
                        $this->sendNotificationForCreator($id, 2, $token->user_id);
                        $respone = $this->ProjectComments->addComment($data);
                        if ($respone == 'TRUE') {
                            $this->responseApi(1, __('Project comment has been created.'));
                        } else {
                            $message = '';
                            foreach ($respone as $row) {
                                foreach ($row as $item) {
                                    $message .= $item . ' <br/>';
                                }
                            }
                            $this->responseApi(0, $message);
                        }
                    }
                } else {
                    $this->responseApi(1032);
                }

            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function deleteComment()
    {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('ProjectComments');

                $comments = $this->ProjectComments->find('all', [
                    'conditions' => ['id' => $data['comment_id']],
                ]);
                $comment = $comments->first();

                if ($comment) {
                    $result = $this->ProjectComments->delete($comment);
                    $this->responseApi(1, __('Delete comment success.'));
                } else {
                    $this->responseApi(1033);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

//    public function getProjectInfo() {
//        $this->autoRender = false;
//        if ($this->request->is('post')) {
//            $data = $this->request->data;
//            $tokenTable = TableRegistry::get('Tokens');
//            $token = $tokenTable->find('all', [
//                'conditions' => ['token' => $data['token']],
//            ]);
//            $token = $token->first();
//
//            if (!empty($token)) {
//                $id = $data['project_id'];
//                $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain([ 'Users'])->first();
//                if (!empty($project)) {
//                    $address = '';
//                    if (!empty($project['country_id'])) {
//                        $this->loadModel('Countries');
//                        $country = $this->Countries->get($project['country_id']);
//                        $address .= $country['country_name'];
//                    } else {
//                        $country = [];
//                    }
//                    if (!empty($project['state_id'])) {
//                        $this->loadModel('States');
//                        $state = $this->States->get($project['state_id']);
//                        $address .= (!empty($address)) ? ' ' . $state['name'] : $state['name'];
//                    } else {
//                        $state = [];
//                    }
//                    if (!empty($project['category_id'])) {
//                        $this->loadModel('Categories');
//                        $category = $this->Categories->get($project['category_id']);
//                    } else {
//                        $category = [];
//                    }
//
//                    $this->loadModel('UserProjectActions');
//                    //check is like
//                    $likes = $this->UserProjectActions->getCountUserProjectActions(['user_id' => $token->user_id, 'project_id' => $project['id'], 'status' => 1, 'action_type' => 1]);
//                    //check is followed
//                    $follows = $this->UserProjectActions->getCountUserProjectActions(['user_id' => $token->user_id, 'project_id' => $project['id'], 'status' => 1, 'action_type' => 2]);
//
//                    $this->loadModel('ProjectsRoles');
//                    $quantity = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
//                    $startDate = new \DateTime(date('Y-m-d H:i:s'));
//                    $startDate->modify('-7 day');
//                    $this->loadModel('ProjectComments');
//                    $listComments = $this->ProjectComments->getListCommentByOptions('all', ['project_id' => $project['id'], 'created >=' => $startDate->format('Y-m-d H:i:s')], []);
//                    $this->loadModel('ProjectUpdates');
//                    $listUpdates = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['project_id' => $project['id'], 'status' => 1, 'created >=' => $startDate->format('Y-m-d H:i:s')], []);
//                    $this->loadModel('UsersProjects');
//                    $this->_data = [
//                        'title' => $project['title'],
//                        'description' => $project['description'],
//                        'start_date' => date('Y-m-d', strtotime($project['start_date'])),
//                        'image_url' => $project['image_url'],
//                        'project_url' => ROOT_URL . 'projects/' . $this->customSlug($project->title) . '-' . $project->id,
//                        'video_id' => $project['video_id'],
//                        'address' => $address,
//                        'category' => isset($category['name']) ? $category['name'] : '',
//                        'pitch' => $project['pitch'],
//                        'owner' => !empty($project['user']['name']) ? $project['user']['name'] : $project['user']['first_name'] . ' ' . $project['user']['last_name'],
//                        'total_role' => $quantity,
//                        'total_role_joined' => count($this->UsersProjects->getUserProjectsByOptions(['project_id' => $project['id'], 'UsersProjects.status' => 1, 'UsersProjects.type' => 2])),
//                        'total_comment' => count($listComments),
//                        'total_update' => count($listUpdates),
//                        'is_like' => ($likes > 0) ? 1 : 0,
//                        'is_follow' => ($follows > 0) ? 1 : 0,
//                    ];
//
//                    $this->responseApi(1, '', $this->_data);
//                } else {
//                    $this->responseApi(1034);
//                }
//            } else {
//                $this->responseApi(1031);
//            }
//        }
//    }

    public function getProjectInfo()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            $this->loadModel('Roles');
            if (!empty($token)) {
                $id = $data['project_id'];
                $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain(['Users'])->first();
                if (!empty($project)) {
                    $address = '';
                    if (!empty($project['country_id'])) {
                        $this->loadModel('Countries');
                        $country = $this->Countries->get($project['country_id']);
                        $address .= $country['country_name'];
                    } else {
                        $country = [];
                    }
                    if (!empty($project['state_id'])) {
                        $this->loadModel('States');
                        $state = $this->States->get($project['state_id']);
                        $address .= (!empty($address)) ? ' ' . $state['name'] : $state['name'];
                    } else {
                        $state = [];
                    }
                    if (!empty($project['category_id'])) {
                        $this->loadModel('Categories');
                        $category = $this->Categories->get($project['category_id']);
                    } else {
                        $category = [];
                    }

                    $this->loadModel('UserProjectActions');
                    //check is like
                    $likes = $this->UserProjectActions->getCountUserProjectActions(['user_id' => $token->user_id, 'project_id' => $project['id'], 'status' => 1, 'action_type' => 1]);
                    //check is followed
                    $follows = $this->UserProjectActions->getCountUserProjectActions(['user_id' => $token->user_id, 'project_id' => $project['id'], 'status' => 1, 'action_type' => 2]);

                    $this->loadModel('ProjectsRoles');
                    $quantity = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                    $startDate = new \DateTime(date('Y-m-d H:i:s'));
                    $startDate->modify('-7 day');
                    $this->loadModel('ProjectComments');
                    $listComments = $this->ProjectComments->getListCommentByOptions('all', ['project_id' => $project['id'], 'created >=' => $startDate->format('Y-m-d H:i:s')], []);
                    $this->loadModel('ProjectUpdates');
                    $listUpdates = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['project_id' => $project['id'], 'status' => 1, 'created >=' => $startDate->format('Y-m-d H:i:s')], []);
                    $this->loadModel('UsersProjects');
                    if ($data['language'] == 'es') {
                        $name = isset($category['name_es']) ? $category['name_es'] : '';
                    } else if ($data['language'] == 'ja') {
                        $name = isset($category['name_ja']) ? $category['name_ja'] : '';
                    } else {
                        $name = isset($category['name']) ? $category['name'] : '';
                    }
                    $UsersProjectRole = $this->UsersProjects->find('all', ['conditions' => ['project_id' => $data['project_id'], 'project_role_id' => 0]])->contain(['Projects']);
                    $UsersProjectRole = $UsersProjectRole->first();
                    $RoleProjects = $this->Roles->find('all', ['conditions' => ['id' => $UsersProjectRole['role_id']]])->first();
                    if ($data['language'] == 'en') {
                        $role = $RoleProjects['role'];
                    } else if ($data['language'] == 'es') {
                        $role = $RoleProjects['role_es'];
                    } else {
                        $role = $RoleProjects['role_ja'];
                    }
                    $name_owner = !empty($project['user']['name']) ? $project['user']['name'] : $project['user']['first_name'] . ' ' . $project['user']['last_name'];
                    $this->_data = [
                        'title' => $project['title'],
                        'description' => $project['description'],
                        'start_date' => date('Y-m-d', strtotime($project['start_date'])),
                        'image_url' => $project['image_url'],
                        'project_url' => ROOT_URL . 'projects/' . $this->customSlug($project->title) . '-' . $project->id,
                        'video_id' => $project['video_id'],
                        'address' => $address,
                        'category' => $name,
                        'pitch' => $project['pitch'],
                        'owner' => $name_owner . ' - ' . $role,
                        'total_role' => $quantity,
                        'total_role_joined' => count($this->UsersProjects->getUserProjectsByOptions(['project_id' => $project['id'], 'UsersProjects.status' => 1, 'UsersProjects.type' => 2])),
                        'total_comment' => count($listComments),
                        'total_update' => count($listUpdates),
                        'is_like' => ($likes > 0) ? 1 : 0,
                        'is_follow' => ($follows > 0) ? 1 : 0,
                    ];

                    $this->responseApi(1, '', $this->_data);
                } else {
                    $this->responseApi(1034);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 24/02/2017
    // Function: Get current and created project
    public function getListProjectByUser()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                // Type = 1 ==> created projects
                // Type = 2 ==> current projects
                if (isset($data['type']) && !empty($data['type'])) {
                    if (isset($data['user_id']) && !empty($data['user_id'])) {
                        $userId = $data['user_id'];
                    } else {
                        $userId = $token->user_id;
                    }
                    // Get list project ids current user liked
                    $userProjectActionTable = TableRegistry::get('UserProjectActions');
                    $listProjectIds = $userProjectActionTable->find('list', ['conditions' => ['user_id' => $userId, 'action_type' => 1, 'status' => 1]])->select(['project_id'])->toArray();
                    $this->loadModel('Projects');
                    $conditions = [];
                    if ($data['type'] == 1) {
                        // created projects
                        $conditions['user_id'] = $userId;
                    } else {
                        // current projects
                        $userProjectTable = TableRegistry::get('UsersProjects');
                        $listProjectJoinIds = $userProjectTable->find('list', ['conditions' => ['user_id' => $userId, 'UsersProjects.type' => 2, 'UsersProjects.status' => 1], 'valueField' => 'project_id'])->toArray();
                        if (!empty($listProjectJoinIds)) {
                            $conditions['Projects.id IN'] = $listProjectJoinIds;
                        }
                    }
                    $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, ['Users']);
                    $dataRespone = [];
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('UsersProjects');
                    $this->loadModel('Projects');
                    $Userprojects = $this->Projects->getAllProjectUsers($userId);
                    $UserprojectJoins = $this->UsersProjects->getAllProejectJoinUser($userId);
                    $projectIdUserJoin = array();
                    $projectIdUser = array();
                    foreach ($Userprojects as $Userproject) {
                        $projectIdUser[] = $Userproject->id;
                    }
                    foreach ($UserprojectJoins as $UserprojectJoin) {
                        $projectIdUserJoin[] = $UserprojectJoin->project_id;
                    }
                    foreach ($listProjects as $project) {
                        $projectIdUserFollow = array();
                        $projectIdUserFollowJoin = array();
                        $UserprojectFollows = $this->Projects->getAllProjectUsers($project['user_id']);
                        $UserprojectFollowJoins = $this->UsersProjects->getAllProejectJoinUser($project['user_id']);
                        $isApplicant = 0;
                        foreach ($UserprojectFollows as $Userproject) {
                            $projectIdUserFollow[] = $Userproject->id;
                        }
                        foreach ($UserprojectFollowJoins as $UserprojectFollowJoin) {
                            $projectIdUserFollowJoin[] = $UserprojectFollowJoin->project_id;
                        }
                        for ($i = 0; $i < count($projectIdUser); $i++) {
                            if (in_array($projectIdUser[$i], $projectIdUserFollowJoin)) {
                                $isApplicant = 1;
                            }
                        }
                        for ($i = 0; $i < count($projectIdUserJoin); $i++) {
                            if (in_array($projectIdUserJoin[$i], $projectIdUserFollow)) {
                                $isApplicant = 1;
                            }
                        }
                        if ($isApplicant == 1) {
                            $roleUser = 'isApplicant';
                        } else {
                            $roleUser = 'isStranger';
                        }
                        $blocksTable = TableRegistry::get('Blocks');
                        $blockUserList = $blocksTable->find('all', ['conditions' => ['user_id' => $project['user_id'], 'blocked_id' => $token->user_id]])->toArray();
                        $blockUser = $blocksTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'blocked_id' => $project['user_id']]])->toArray();
                        if (!empty($blockUserList) || !empty($blockUser)) {
                            $canView = (int)0;
                        } else {
                            $canView = (int)1;
                        }
                        $data = [
                            'roleUser' => $roleUser,
                            'canView' => $canView,
                            'user_id' => empty($project['user_id']) ? '' : $project['user_id'],
                            'project_id' => empty($project['id']) ? '' : $project['id'],
                            'title' => empty($project['title']) ? '' : $project['title'],
                            'description' => empty($project['description']) ? '' : $project['description'],
                            'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/project_default.jpg',
                            'video_id' => empty($project['video_id']) ? '' : $project['video_id'],
                            'pitch' => empty($project['pitch']) ? '' : $project['pitch'],
                            'start_date' => empty($project['start_date']) ? '' : $project['start_date'],
                            'end_date' => empty($project['end_date']) ? '' : $project['end_date'],
                            'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                        ];
                        if (!empty($project['country_id'])) {
                            $this->loadModel('Countries');
                            $country = $this->Countries->get($project['country_id']);
                            $data['address'] = $country['country_name'];
                            if (!empty($project['state_id'])) {
                                $this->loadModel('States');
                                $state = $this->States->get($project['state_id']);
                                $data['address'] = $state['name'] . ', ' . $country['country_name'];
                            }
                        } else {
                            $data['address'] = '';
                        }
                        if (!empty($project['category_id'])) {
                            $this->loadModel('Categories');
                            $category = $this->Categories->get($project['category_id']);
                            $data['category'] = $category['name'];
                        } else {
                            $data['category'] = 'Feature';
                        }
                        $data['is_liked'] = in_array($project['id'], $listProjectIds) ? 1 : 0;
                        $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                        $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                        $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                        $dataRespone[] = $data;
                    }
                    $this->responseApi(1, __('Success'), $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function exportExcel()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $userTable = TableRegistry::get('Users');
                $adminType = $userTable->find('all', ['conditions' => ['id' => $userId], 'fields' => 'admin'])->first();
                if ($adminType['admin'] != 0) {
                    $this->Xls->exportProject('Projects');
                }
            }
        }
    }


}
