<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * UsersProjects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class UsersProjectsController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
    }

    // Coder: Giang Dien
    // Date: 2016-11-22
    // Function: get list users of project by options
    public function getUsersProjectsByOptions() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
	
            if (!empty($token)) {
                if (isset($data['option']) && in_array($data['option'], ['0', '1', '2'])) {
                    if (isset($data['project_id']) && !empty($data['project_role_id'])) {
                        if (isset($data['project_id']) && isset($data['project_role_id'])){
                            $ProjectRoleTable = TableRegistry::get('ProjectsRoles');
                            $ProjectRoleTable->updateAll(['is_read'=>1], ['id'=>$data['project_role_id']]);
                        }
                        $project_id = $this->request->data['project_id'];
                        $this->loadModel('UsersProjects');
                        $param = ['project_id' => $project_id, 'UsersProjects.type' =>2,'UsersProjects.status'=>1];
                        if (isset($data['keyword']) && !empty($data['keyword'])){
                            $param['Users.name LIKE'] = $data['keyword'].'%';
                        }
                        if (isset($data['project_role_id'])){
                            $param['project_role_id'] = $data['project_role_id'];
                        }
                        if ($data['option'] == 1) {
                            $param['saved'] = 1;
                        }
                        if ($data['option'] == 2) {
                            $param['offer'] = 1;
                        }
						$blocksTable = TableRegistry::get('Blocks');	
                        $listUsers = $this->UsersProjects->getUserProjectsByAllOptions($param,0,0,['Users']);
                     
					  $responeData = [];
                        foreach ($listUsers as $user) {
							 $block = $blocksTable->find()
							->where(['blocked_id' => $token->user_id, 'user_id' => $user['user_id']])
							->first();
							$blockuser = $blocksTable->find()
							->where(['user_id' => $token->user_id, 'blocked_id' => $user['user_id']])
							->first();
							 if(empty($blockuser) && empty($blockuser))
							{
                            $dataArr = [
                                'icon' => !empty($user['user']['avatar']) ? $user['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                'name' => $user['user']['first_name'].' '.$user['user']['last_name'],
                                'user_id'=> $user['user_id'],
                                'project_id'=> $user['project_id'],
								'can_view'=> 1,
                                'role_id'=> $user['role_id']
                            ];
							}
							else
							{
							$dataArr = [
                                'icon' => !empty($user['user']['avatar']) ? $user['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                'name' => $user['user']['first_name'].' '.$user['user']['last_name'],
                                'user_id'=> $user['user_id'],
                                'project_id'=> $user['project_id'],
								'can_view'=> 0,
                                'role_id'=> $user['role_id']
                            ];
							}
                            if ($data['option'] == 0) {
                                if ($user['offer'] == 1) {
                                    $dataArr['type'] = 2;
                                } else if ($user['saved'] == 1) {
                                    $dataArr['type'] = 1;
                                } else {
                                    $dataArr['type'] = 0;
                                }
                            } else {
                                $dataArr['type'] = ($data['option'] == 1) ? 1 : 2;
                            }
                            $responeData[] = $dataArr;
                        }
                        $this->responseApi(1, 'Success', $responeData);
                    } else {
                        $this->responseApi(1032);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

}

?>