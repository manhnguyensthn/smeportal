<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * ProjectsRoles Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class ProjectsRolesController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
    }

    // Coder: Giang Dien
    // Date: 2016-11-18
    // Function: API get list project roles for mobile
    public function getListProjectRoles() {
        $this->autoRender = false;
        $this->set('title', __('SME') . ' - ' . __('Cá nhân'));
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                        'conditions' => ['token' => $data['token']],
                    ])->first();
            if (!empty($token)) {
                if (isset($data['project_id']) && !empty($data['project_id'])) {
                    $project_id = $data['project_id'];
                    $this->loadModel('ProjectsRoles');
                    $param = ['project_id' => $project_id];
                    if (isset($data['keyword']) && !empty($data['keyword'])) {
                        if($data['language'] == 'ja')
						{
							$param['role_ja LIKE'] = '%'.$data['keyword'] . '%';
						}
						else if($data['language'] == 'es')
						{
							$param['role_es LIKE'] = '%'.$data['keyword'] . '%';
						}
						else
						{
							$param['role LIKE'] = '%'.$data['keyword'] . '%';
						}
                    }
				
                    $listRoles = $this->ProjectsRoles->getProjectRoles($param, ['Roles']);
                    $responeData = [];
                    foreach ($listRoles as $role) {
                        $data = [
                            'icon' => !empty($role['role']['icon']) ? ROOT_URL . $role['role']['icon'] : ROOT_URL . 'img/role_icon.png',
                            'title' => $role['role']['role'],
                            'charactor_name' => $role['charactor_name'],
                            'checkNewsMember' => $role['is_read'],
                            'role_id' => $role['role_id'],
                            'project_role_id' => $role['id'],
                            'project_id' => $project_id
                        ];
                        $this->loadModel('UsersProjects');
                        $listUsers = $this->UsersProjects->getUserProjectsByOptions(['project_id' => $project_id, 'UsersProjects.status' => 1, 'UsersProjects.type' => 2]);
                        $data['totalNumber'] = $this->_get_count_users_by_status($listUsers, $role['id']);
                        $data['numberOffer'] = $this->_get_count_users_by_offer_status($listUsers, 1, $role['id']);
                        $data['numberSave'] = $this->_get_count_users_by_saved_status($listUsers, 1, $role['id']);
                        $responeData[] = $data;
                    }
                    $this->responseApi(1, 'Success', $responeData);
                } else {
                    $this->responseApi(1033);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-18
    // Function: get number of users by status from list
    private function _get_count_users_by_status($list = array(), $project_role_id = 0) {
        $counter = 0;
        foreach ($list as $row) {
            if ($row['project_role_id'] == $project_role_id) {
                $counter ++;
            }
        }
        return $counter;
    }

    // Coder: Giang Dien
    // Date: 2016-11-18
    // Function: get number of users by offer status from list
    private function _get_count_users_by_offer_status($list = array(), $offer_status = 1, $project_role_id = 0) {
        $counter = 0;
        foreach ($list as $row) {
            if ($row['offer'] == $offer_status && $row['project_role_id'] == $project_role_id && $row['status'] == 1) {
                $counter ++;
            }
        }
        return $counter;
    }

    // Coder: Giang Dien
    // Date: 2016-11-18
    // Function: get number of users by saved status from list
    private function _get_count_users_by_saved_status($list = array(), $saved_status = 1, $project_role_id = 0) {
        $counter = 0;
        foreach ($list as $row) {
            if ($row['saved'] == $saved_status && $row['project_role_id'] == $project_role_id && $row['status'] == 1) {
                $counter ++;
            }
        }
        return $counter;
    }

    // Coder: Giang Dien
    // Date: 2016-11-18
    // Function: API get all list project roles for mobile
    public function getAllListRoles() {
        $this->autoRender = false;
        $this->set('title', __('SME') . ' - ' . __('Cá nhân'));
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);

            $token = $token->first();
            if (!empty($token)) {
                $this->loadModel('Roles');
                $params = [];
                if (isset($data['keyword']) && !empty($data['keyword'])) {
				if($data['language'] == 'ja')
				{
                    $params['role_ja LIKE'] = '%'.$data['keyword'] . '%';
					$order = 'role_ja';
                }
				else if($data['language'] == 'es')
				{
					$params['role_es LIKE'] = '%'.$data['keyword'] . '%';
					$order = 'role_es';
				}
				else
				{
					$params['role LIKE'] = '%'.$data['keyword'] . '%';
					$order = 'role';
				}	
				}
				if($data['language'] == 'ja')
				{
					$order = 'role_ja';
                }
				else if($data['language'] == 'es')
				{
					$order = 'role_es';
				}
				else
				{
					$order = 'role';
				}	
                if (isset($data['limit']) && !empty($data['limit'])) {
                    $limit = $data['limit'];
                } else {
                    $limit = 20;
                }
                if (isset($data['page']) && !empty($data['page'])) {
                    $page = $data['page'];
                } else {
                    $page = 1;
                }
                $offset = ($page - 1) * $limit;
                $listRoles = $this->Roles->getRolesByOptionsSort($params, $limit, $offset,$order);
                $responeData = [];
                foreach ($listRoles as $role) {
                    $data = [
                        'role_id' => $role['id'],
                        'icon' => !empty($role['icon']) ? ROOT_URL . $role['icon'] : ROOT_URL . 'img/role_icon.png',
                        'title' => $role['role']
                    ];
                    $responeData[] = $data;
                }
                $this->responseApi(1, 'success', $responeData);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    // Coder: Giang Dien
    // Date: 16/02/2017
    // Function: get list user joined in list project
    public function getListUserJoinedInListProject() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['projectIds']) && !empty($data['projectIds'])){
                    $listIds = json_decode($data['projectIds']);
                    $this->loadModel('UsersProjects');
                    $listUsers = $this->UsersProjects->getOneUserProjectFolder(['project_id IN'=>$listIds,'status'=> 1,'type'=>2],['project_id','role_id','project_role_id','user_id']);
                    $dataResponse = [];
                    foreach ($listUsers as $row){
                        $dataRow = [
                            'project_id'=> !empty($row['project_id']) ? $row['project_id'] : 0,
                            'role_id'=> !empty($row['role_id']) ? $row['role_id'] : 0,
                            'project_role_id'=> !empty($row['project_role_id']) ? $row['project_role_id'] : 0,
                            'user_id'=> !empty($row['user_id']) ? $row['user_id'] : 0
                        ];
                        $dataResponse[] = $dataRow;
                    }
                    $this->responseApi(1,'success',$dataResponse);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

}
