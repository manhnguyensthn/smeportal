<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class RequestsController extends ApiController
{

    public function getListRequestByAdminSME()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $userTable = TableRegistry::get('Users');
                $adminType = $userTable->find('all', ['conditions' => ['id' => $userId], 'fields' => 'admin'])->first();
                if ($adminType['admin'] == 5) {
                    $requestTable = TableRegistry::get('Requests');
                    $requests = $requestTable->find('all')->toArray();
                    if ($this->request->is('post')) {
                        $group = $this->request->data('group');
                        $status = $this->request->data('status');
                        $type = $this->request->data('type');
                        switch ($status) {
                            case 0:
                                $status = 0;
                                break;
                            case 1:
                                $status = 1;
                                break;
                            case 2:
                                $status = 2;
                                break;
                            default:
                                $status = 4;
                                break;
                        }
                        switch ($type) {
                            case 1:
                                $type = 1;
                                break;
                            case 2:
                                $type = 2;
                                break;
                            case 3:
                                $type = 3;
                                break;
                            case 4:
                                $type = 4;
                                break;
                            default:
                                $type = 5;
                                break;
                        }

                        $location = $this->request->data('location');
                        $category = $this->request->data('category');
                        $role = $this->request->data('role');
                        // var_dump($this->request->data);
                        // echo $status;
                        $search = [];
                        if (!empty($group && isset($group))) {
                            $search['receiver_id2'] = $group;
                        }
                        if (isset($status) && $status != 4) {
                            $search['status'] =  $status;
                        }
                        if (!empty($type) && $type != 5) {
                            $search['type'] = $type;
                        }
                        if (!empty($location) && isset($location)) {
                            $search['receiver_id4'] = $location;
                        }
                        if (!empty($category) && isset($category)) {
                            $search['receiver_id1'] = $category;
                        }
                        if (!empty($role) && isset($role)) {
                            $search['receiver_id3'] = $role;
                        }
                        $requests = $this->Requests->find('all', [
                            'conditions' => $search
                        ]);
                    }

                    foreach ($requests as $items) {
                        $data['id'] = $items['id'];
                        $data['title'] = $items['title'];
                        $data['content'] = $items['content'];
                        $data['attach1'] = $items['attach1'];
                        $data['attach2'] = $items['attach2'];
                        $data['repattach1'] = $items['repattach1'];
                        $data['repattach2'] = $items['repattach2'];
                        $data['project_id'] = $items['project_id'];
                        $data['repcontent1'] = $items['repcontent1'];
                        $data['repcontent2'] = $items['repcontent2'];
                        $data['type'] = $items['type'];
                        $data['status'] = $items['status'];

                        $CategoriesTable = TableRegistry::get('Categories');
                        $data['category'] = $CategoriesTable->find('list', ['conditions' => ['id' => $items['receiver_id1']], 'field' => 'name']);
                        $GroupsTable = TableRegistry::get('Groups');
                        $data['group'] = $GroupsTable->find('list', ['conditions' => ['id' => $items['receiver_id2']], 'field' => 'name']);
                        $RolesTable = TableRegistry::get('Roles');
                        $data['role'] = $RolesTable->find('list', ['conditions' => ['id' => $items['receiver_id3']], 'field' => 'name']);
                        $LocationsTable = TableRegistry::get('Districts');
                        $data['location'] = $LocationsTable->find('list', ['conditions' => ['id' => $items['receiver_id4']], 'field' => 'name']);

                        $ProjectsTable = TableRegistry::get('Projects');
                        $project = $ProjectsTable->find('list', ['conditions' => ['id' => $items['project_id']], 'field' => 'title']);
                        $data['projectSend'] = $project;
                        $data['projectReceive'] = $ProjectsTable->find('list', ['conditions' => ['id' => $items['receiver_id5']], 'field' => 'title']);
                        $UsersTable = TableRegistry::get('Users');
                        $user = $UsersTable->find('list', ['conditions' => ['id' => $items['user_id']], 'field' => 'last_name']);
                        $data['userSend'] = $user;
                        $dataRespone[] = $data;
                    }
                } else {
                    if ($adminType['admin'] == 1) {
                        $requestType = 1;
                    } elseif ($adminType['admin'] == 2) {
                        $requestType = 2;
                    } elseif ($adminType['admin'] == 3) {
                        $requestType = 3;
                    } elseif ($adminType['admin'] == 4) {
                        $requestType = 4;
                    }
                    // var_dump($requestType);

                    $requestTable = TableRegistry::get('Requests');
                    $requests = $requestTable->find('all', ['conditions' => ['type' => $requestType]])->toArray();


                    if ($this->request->is('post')) {
                        // var_dump($this->request->data);
                        $group = $this->request->data('group');
                        $status = $this->request->data('status');
                        $type = $requestType;
                        // $status = intval($status);
                        switch ($status) {
                            case 0:
                                $status = 0;
                                break;
                            case 1:
                                $status = 1;
                                break;
                            case 2:
                                $status = 2;
                                break;
                            default:
                                $status = 4;
                                break;
                        }

                        $location = $this->request->data('location');
                        $category = $this->request->data('category');
                        $role = $this->request->data('role');
                        // var_dump($this->request->data);
                        // echo $status;
                        $search = [];
                        if (!empty($group && isset($group))) {
                            $search['receiver_id2'] = $group;
                        }
                        if (isset($status) && $status != 4) {
                            $search['status'] = $status;
                        }
                        if (!empty($type) && $type != 5) {
                            $search['type'] = $type;
                        }
                        if (!empty($location) && isset($location)) {
                            $search['receiver_id4'] = $location;
                        }
                        if (!empty($category) && isset($category)) {
                            $search['receiver_id1'] = $category;
                        }
                        if (!empty($role) && isset($role)) {
                            $search['receiver_id3'] = $role;
                        }
                        $requests = $this->Requests->find('all', [
                            'conditions' => $search
                        ]);
                    }
                    if ($requests == null) {
                        $dataRespone[] = $requests;
                    } else {
                        foreach ($requests as $items) {
                            $data['id'] = $items['id'];
                            $data['title'] = $items['title'];
                            $data['content'] = $items['content'];
                            $data['project_id'] = $items['project_id'];
                            $data['repcontent1'] = $items['repcontent1'];
                            $data['repcontent2'] = $items['repcontent2'];
                            $data['status'] = $items['status'];
                            $data['type'] = $items['type'];

                            $CategoriesTable = TableRegistry::get('Categories');
                            $data['category'] = $CategoriesTable->find('list', ['conditions' => ['id' => $items['receiver_id1']], 'field' => 'name']);
                            $GroupsTable = TableRegistry::get('Groups');
                            $data['group'] = $GroupsTable->find('list', ['conditions' => ['id' => $items['receiver_id2']], 'field' => 'name']);
                            $RolesTable = TableRegistry::get('Roles');
                            $data['role'] = $RolesTable->find('list', ['conditions' => ['id' => $items['receiver_id3']], 'field' => 'name']);
                            $LocationsTable = TableRegistry::get('Districts');
                            $data['location'] = $LocationsTable->find('list', ['conditions' => ['id' => $items['receiver_id4']], 'field' => 'name']);

                            $ProjectsTable = TableRegistry::get('Projects');
                            $project = $ProjectsTable->find('list', ['conditions' => ['id' => $items['project_id']], 'field' => 'title']);
                            $data['projectSend'] = $project;
                            $data['projectReceive'] = $ProjectsTable->find('list', ['conditions' => ['id' => $items['receiver_id5']], 'field' => 'title']);
                            $UsersTable = TableRegistry::get('Users');
                            $user = $UsersTable->find('list', ['conditions' => ['id' => $items['user_id']], 'field' => 'last_name']);
                            $data['userSend'] = $user;
                            $dataRespone[] = $data;
                        }
                    }
                }
                $this->responseApi(1, __('Success'), $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function getDetailRequest()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $token = $tokenTable->find('all', [
                    'conditions' => ['token' => $data['token']],
                ]);
                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->find('all', ['conditions' => ['id' => $data['id']]]);
                foreach ($request as $items) {
                    $data['id'] = $items['id'];
                    $data['title'] = $items['title'];
                    $data['content'] = $items['content'];
                    $data['attach1'] = $items['attach1'];
                    $data['attach2'] = $items['attach2'];
                    $data['repattach1'] = $items['repattach1'];
                    $data['repattach2'] = $items['repattach2'];
                    $data['project_id'] = $items['project_id'];
                    $data['repcontent1'] = $items['repcontent1'];
                    $data['repcontent2'] = $items['repcontent2'];
                    $data['type'] = $items['type'];
                    $data['status'] = $items['status'];
                    $dataRespone[] = $data;
                }
                $this->responseApi(1, __('Success'), $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function getListOnwerRequests()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $requestTable = TableRegistry::get('Requests');
                $requests = $requestTable->find('all', ['conditions' => ['user_id' => $userId]])->toArray();

                if ($this->request->is('post')) {
                    // var_dump($this->request->data);
                    $group = $this->request->data('group');
                    $status = $this->request->data('status');
                    // $status = intval($status);
                    switch ($status) {
                        case 0:
                            $status = 0;
                            break;
                        case 1:
                            $status = 1;
                            break;
                        case 2:
                            $status = 2;
                            break;
                        default:
                            $status = 4;
                            break;
                    }

                    $location = $this->request->data('location');
                    $category = $this->request->data('category');
                    $role = $this->request->data('role');
                    // var_dump($this->request->data);
                    // echo $status;
                    $search = [];
                    if (!empty($group && isset($group))) {
                        $search['receiver_id2'] = $group;
                    }
                    if (isset($status) && $status != 4) {
                        $search['status'] = $status;
                    }
                    if (!empty($type) && $type != 5) {
                        $search['type'] = $type;
                    }
                    if (!empty($location) && isset($location)) {
                        $search['receiver_id4'] = $location;
                    }
                    if (!empty($category) && isset($category)) {
                        $search['receiver_id1'] = $category;
                    }
                    if (!empty($role) && isset($role)) {
                        $search['receiver_id3'] = $role;
                    }
                    $search['user_id'] = $userId;
                    $requests = $requestTable->find('all', [
                        'conditions' => $search
                    ]);
                }

                if ($requests == null) {
                    $dataRespone[] = $requests;
                } else {
                    foreach ($requests as $items) {
                        $data['id'] = $items['id'];
                        $data['title'] = $items['title'];
                        $data['content'] = $items['content'];
                        $data['attach1'] = $items['attach1'];
                        $data['attach2'] = $items['attach2'];
                        $data['repattach1'] = $items['repattach1'];
                        $data['repattach2'] = $items['repattach2'];
                        $data['project_id'] = $items['project_id'];
                        $data['repcontent1'] = $items['repcontent1'];
                        $data['repcontent2'] = $items['repcontent2'];
                        $data['type'] = $items['type'];
                        $data['status'] = $items['status'];

                        $CategoriesTable = TableRegistry::get('Categories');
                        $data['category'] = $CategoriesTable->find('list', ['conditions' => ['id' => $items['receiver_id1']], 'field' => 'name']);
                        $GroupsTable = TableRegistry::get('Groups');
                        $data['group'] = $GroupsTable->find('list', ['conditions' => ['id' => $items['receiver_id2']], 'field' => 'name']);
                        $RolesTable = TableRegistry::get('Roles');
                        $data['role'] = $RolesTable->find('list', ['conditions' => ['id' => $items['receiver_id3']], 'field' => 'name']);
                        $LocationsTable = TableRegistry::get('Districts');
                        $data['location'] = $LocationsTable->find('list', ['conditions' => ['id' => $items['receiver_id4']], 'field' => 'name']);
                        $ProjectsTable = TableRegistry::get('Projects');
                        $project = $ProjectsTable->find('list', ['conditions' => ['id' => $items['project_id']], 'field' => 'title']);
                        $data['projectSend'] = $project;
                        $data['projectReceive'] = $ProjectsTable->find('list', ['conditions' => ['id' => $items['receiver_id5']], 'field' => 'title']);
                        $UsersTable = TableRegistry::get('Users');
                        $user = $UsersTable->find('list', ['conditions' => ['id' => $items['user_id']], 'field' => 'last_name']);
                        $data['userSend'] = $user;
                        $dataRespone[] = $data;
                    }
                    $this->responseApi(1, __('Success'), $dataRespone);
                }

            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function getListRequestByUser()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $dataRespone[] = '';
            $search = [];
            $token = $token->first();
            if (!empty($token)) {
                $userId = $token->user_id;
                $requestTable = TableRegistry::get('Requests');
                $requests = $requestTable->getListRequestUser($userId,$search);
                $dataRespone[] = '';
                foreach ($requests as $items) {
                    $data['id'] = $items['id'];
                    $data['title'] = $items['title'];
                    $data['content'] = $items['content'];
                    $data['attach1'] = $items['attach1'];
                    $data['attach2'] = $items['attach2'];
                    $data['repattach1'] = $items['repattach1'];
                    $data['repattach2'] = $items['repattach2'];
                    $ProjectsTable = TableRegistry::get('Projects');
                    $project = $ProjectsTable->find('list', ['conditions' => ['id' => $items['project_id']], 'field' => 'title']);
                    $data['projectSend'] = $project;
                    $UsersTable = TableRegistry::get('Users');
                    $user = $UsersTable->find('list', ['conditions' => ['id' => $items['user_id']], 'field' => 'last_name']);
                    $data['userSend'] = $user;
                    $data['repcontent1'] = $items['repcontent1'];
                    $data['repcontent2'] = $items['repcontent2'];
                    $data['status'] = $items['status'];
                    $data['type'] = $items['type'];
                    $dataRespone[] = $data;
                }
                $this->responseApi(1, __('Success'), $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }


    public function create($id = null)
    {
        $data = $this->request->data;
        $tokenTable = TableRegistry::get('Tokens');
        $token = $tokenTable->find('all', [
            'conditions' => ['token' => $data['token']],
        ]);
        $token = $token->first();
        if (!empty($token)) {
            $this->loadModel('Requests');
            $this->loadModel('Requests_Projects');
            $this->loadModel('Users');
            $this->set('title', __('Thêm yêu cầu'));
            $userid = $token->user_id;
            $user1 = $this->Users->find('all', [
                'conditions' => ['id' => $userid],
                'fields' => ['id', 'email', 'activated', 'admin']
            ])->first();
            if ($id) {
                $request = $this->Requests->find('all', ['conditions' => ['id' => $id]])->first();

                if (($request['user_id'] != $token->user_id) && ($user1->admin == 0)) {
                    $this->responseApi(0, __('Bạn không thể sửa yêu cầu'));
                }
            } else {
                $request = $this->Requests->newEntity();
            }
            if ($this->request->is('post')) {

            $img_url = '';
            $f_url = '';

            //attach files
            if (isset($this->request->data['attach1'])) {
                $fileOK = $this->attachfile('img/attach', $this->request->data['attach1']);
                if (!empty($fileOK['url'])) {
                    $file_url1 = ROOT_URL . $fileOK['url'];
                }
                // var_dump($fileOK);
                if (!empty($fileOK['errors'])) {
                    // $this->Flash->error("Định dạng file không hợp lệ!");
                    $this->Flash->error($fileOK['errors']);
                    // die();
                    return $this->redirect(['action' => 'create']);
                }
            }
            if (!empty($this->request->data['attach2'])) {

                $fileOK1 = $this->attachfile('img/attach', $this->request->data['attach2']);
                if (!empty($fileOK1['url'])) {
                    $file_url2 = ROOT_URL . $fileOK1['url'];
                }
                if (!empty($fileOK1['errors'])) {
                    // $this->Flash->error("Định dạng file không hợp lệ!");
                    $this->Flash->error($fileOK1['errors']);
                    // die();
                    return $this->redirect(['action' => 'create']);
                }
            }


            //set file url to save
            if (isset($file_url1)) {
                $this->request->data['attach1'] = $file_url1;
            } else {
                $this->request->data['attach1'] = $f_url;
            }
            if (isset($file_url2)) {
                $this->request->data['attach2'] = $file_url2;
            } else {
                $this->request->data['attach2'] = $f_url;
            }
            $this->request->data['user_id'] = $userid;


            //receiver
            $receiver1 = $this->request->data('receiver_id1');
            $receiver2 = $this->request->data('receiver_id2');
            $receiver3 = $this->request->data('receiver_id3');
            $receiver4 = $this->request->data('receiver_id4');
            $receiver5 = $this->request->data('receiver_id5');

            if ((!empty($receiver1)) || (!empty($receiver2)) || (!empty($receiver3)) || (!empty($receiver4)) || (!empty($receiver5))) {
                switch ($this->request->data('type')) {

                    //type 1 = QLHV
                    case 0:
                        $type = 1;
                        break;
                    //type 2 = Phap Ly
                    case 1:
                        $type = 2;
                        break;
                    //type3 = XTTM
                    case 2:
                        $type = 3;
                        break;
                    //type4 = Von vay
                    case 3:
                        $type = 4;
                        break;

                    default:
                        # code...
                        break;
                }
                // set type to save
                $this->request->data['type'] = $type;
                $request = $this->Requests->patchEntity($request, $this->request->data);

                //save new record to dtb
                $request->created = date('Y-m-d H:i:s');
                if ($this->Requests->save($request)) {
                    $ProjectsTable = TableRegistry::get('Projects');
                    $UsersTable = TableRegistry::get('Users');
                    //save all pairs of sender receiver projects/user to table request_project
                    if (!empty($receiver1)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['category_id' => $receiver1]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                                }
                            }
                        }
                    }
                    if (!empty($receiver2)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['group_id' => $receiver2]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));

                                }
                            }
                        }
                    }

                    if (!empty($receiver4)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['district_id' => $receiver4]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                                }
                            }
                        }
                    }
                    if (!empty($receiver5)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['id' => $receiver5]]);
                        if (!empty($projects)) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $receiver5;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                } 
                        }
                    }
                    if (!empty($receiver3)) {
                        $roles = $this->Users->find('all', ['conditions' => ['users_type_id' => $receiver3]]);
                        if (!empty($roles)) {
                            foreach ($roles as $role) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['user_id'] = $role->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                }
                            }
                        }
                    }
                    //find corresponding admin
                    $admins = TableRegistry::get('Users');
                    $admins1 = $this->Users->find('all', [
                        'conditions' => ['admin' => $type],
                        'fields' => ['id', 'email', 'admin']
                    ])->first();
                    $isSendNotificationByMobile = FALSE;
                    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                    $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $admins1->id]])->first();
                    if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                        $isSendNotificationByMobile = TRUE;
                    }
//                    die();
                    //send notification
          
                    $project_id = $this->request->data('project_id');
                    $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                    $actionType = 21;
                    $notificationsTable = TableRegistry::get('Notifications');
                    $notificationData = $notificationsTable->newEntity();
                    $notificationData->user_id = $admins1->id;
                    $notificationData->action_user_id = $user1['id'];
                    $notificationData->project_id = $this->request->data('project_id');
                    $notificationData->option_data = $optionData;
                    $notificationData->action_type = $actionType;
                    $notificationData->read_flg = 0;
                    // echo $this->request->data('project_id');
                    $notificationData->created = date('Y-m-d H:i:s');
                    // var_dump($notificationData);
                    if ($isSendNotificationByMobile == TRUE) {
                    if (!$notificationsTable->save($notificationData)) {
                        $this->responseApi(0, __('Khong push dc noti'));
                    }
                    }
                    $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $admins1->id, $user1['id']);
                     $this->responseApi(1, __('Thong cong'));
                    

                } else {
                    $this->responseApi(0, __('Khong the luu'));
                    die();
                }
            } else {
                 $this->responseApi(0, __('Phai co nguoi nhan'));
                 die();
            }
        } else {
                $this->responseApi(0, __('Không phải post'));
                die();
            }

        } else {
            $this->responseApi(1031, __('Vui lòng đăng nhập trước khi tạo yêu cầu!'));
        }
    }

    public function respond()
    {
        $accept = 0;
        $id = $this->request->data('confirm_id');
        $data = $this->request->data;
        $tokenTable = TableRegistry::get('Tokens');
        $token = $tokenTable->find('all', [
            'conditions' => ['token' => $data['token']],
        ]);
        $token = $token->first();
        if (!empty($token)) {

            $accept = $this->request->data('accept');
            $this->loadModel('Requests');
            $this->loadModel('Requests_Projects');
            $this->loadModel('Projects');
            $this->loadModel('Users');
            $this->set('title', __('Thêm yêu cầu'));
            // $user = $this->Auth->user();
            $userid = $token->user_id;
            $user1 = $this->Users->find('all', [
                'conditions' => ['id' => $userid],
                'fields' => ['id', 'name', 'email', 'activated', 'admin']
            ])->first();
            if ($user1->admin != 0) {

                $request = $this->Requests->get($id, [
                    'contain' => ['Projects']
                ]);
                switch ($request->type) {
                    case 1:
                        $type = "Quản lý hội viên";
                        break;
                    case 2:
                        $type = "Pháp lý";
                        break;
                    case 3:
                        $type = "Xúc tiến";
                        break;
                    case 4:
                        $type = "Vốn vay";
                        break;

                    default:
                        # code...
                        break;
                }
                $title = $request['project']->title;
                $project_id = $request->project_id;
            } else {
                $this->responseApi(0, __('Bạn không được phép duyệt yêu cầu'));
            }

            if ($this->request->is(['patch', 'post', 'put'])) {
                //admin duyet
                if ($user1->admin == 1 || $user1->admin == 2 || $user1->admin == 3 || $user1->admin == 4) {

                    ini_set('max_execution_time', 300);

                    $f_url = '';
                    $data = $this->request->data;
                    if ($accept == 0) {
                        $data['status'] = 1;
                        if (!empty($this->request->data['attach1'])) {
                            $fileOK = $this->attachfile('img/attach', $this->request->data['repattach1']);
                            if (!empty($fileOK['url'])) {

                                $file_url1 = ROOT_URL . $fileOK['url'];
                            }
                        }
                        if (!empty($this->request->data['attach1'])) {
                            $fileOK1 = $this->attachfile('img/attach', $this->request->data['repattach2']);
                            if (!empty($fileOK['url'])) {

                                $file_url2 = ROOT_URL . $fileOK1['url'];
                            }
                        }

                        if (isset($file_url1)) {
                            $data['repattach1'] = $file_url1;
                        } else {
                            $data['repattach1'] = $f_url;
                        }
                        if (isset($file_url2)) {
                            $data['repattach2'] = $file_url2;
                        } else {
                            $data['repattach2'] = $f_url;
                        }
                        $data['admin_id'] = $user1->id;
                        $request = $this->Requests->patchEntity($request, $data);
                        if ($this->Requests->save($request)) {
                            $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                            $admins1 = $this->Users->find('all', [
                                'conditions' => ['admin' => 5],
                                'fields' => ['id', 'email', 'admin']
                            ])->first();

                            $actionType = 21;
                            $notificationsTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationsTable->newEntity();
                            $notificationData->user_id = $admins1->id;
                            $notificationData->action_user_id = $token->user_id;
                            $notificationData->project_id = $project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->read_flg = 0;
                            $notificationData->created = date('Y-m-d H:i:s');
                            if (!$notificationsTable->save($notificationData)) {
                                $this->responseApi(0, __('Không tạo được noti'));
                            }
                            $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $admins1->id, $token->user_id);
                            $this->responseApi(1, __('Duyệt thành công'));
                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->responseApi(0, __('Không lưu được yêu cầu'));

                        }
                    } else {
                        $data['status'] = 3;
                        if (!empty($this->request->data['attach1'])) {
                            $fileOK = $this->attachfile('img/attach', $this->request->data['repattach1']);
                            $file_url1 = ROOT_URL . $fileOK['url'];
                        }
                        if (!empty($this->request->data['attach1'])) {
                            $fileOK1 = $this->attachfile('img/attach', $this->request->data['repattach2']);
                            $file_url2 = ROOT_URL . $fileOK1['url'];
                        }

                        if (isset($file_url1)) {
                            $data['repattach1'] = $file_url1;
                        } else {
                            $data['repattach1'] = $f_url;
                        }
                        if (isset($file_url2)) {
                            $data['repattach2'] = $file_url2;
                        } else {
                            $data['repattach2'] = $f_url;
                        }
                        $data['user_id'] = $userid;
                        $data['admin_id'] = $user1->id;
                        $request = $this->Requests->patchEntity($request, $data);
                        if ($this->Requests->save($request)) {
                            $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                            $admins1 = $this->Users->find('all', [
                                'conditions' => ['admin' => 5],
                                'fields' => ['id', 'email', 'admin']
                            ])->first();

                            $actionType = 23;
                            $notificationsTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationsTable->newEntity();
                            $notificationData->user_id = $request->user_id;
                            $notificationData->action_user_id = $token->user_id;
                            $notificationData->project_id = $project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;  // Type of notification join project
                            $notificationData->read_flg = 0;
                            $notificationData->created = date('Y-m-d H:i:s');
                            if (!$notificationsTable->save($notificationData)) {
                                $this->responseApi(0, __('Không lưu được thông báo'));
                            }
                            $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $request->user_id, $token->user_id);
                            $this->responseApi(1, __('Từ chối yêu cầu'));
                        } else {
                            $this->responseApi(0, __('Không lưu được yêu cầu'));
                        }
                    }
                }
                if ($user1->admin == 5 && $request->status == 1) {
                    $data = $this->request->data;
                    if ($accept == 0) {
                        $data['status'] = 2;

                        $request = $this->Requests->patchEntity($request, $data);
                        if ($this->Requests->save($request)) {
                            $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                            $actionType = 24;

                            $test = [];
                            $receivers = $this->Requests_Projects->find('all', ['conditions' => ['request_id' => $request->id], 'fields' => ['request_id', 'project_id', 'user_id'], 'group' => ['project_id', 'user_id']]);
                            if (!empty($receivers)) {

                                foreach ($receivers as $receiver) {
                                    if (!empty($receiver->project_id) || !empty($receiver->user_id)) {
                                        if (!empty($receiver->project_id)) {
                                            $receivep = $receiver->project_id;
                                            $pnotis = $this->Projects->find('all', ['conditions' => ['id' => $receivep], 'fields' => ['user_id']]);
                                            if (!empty($pnotis)) {
                                                $pnotis = $pnotis->first()->user_id;
                                                if (!in_array($pnotis, $test)) {
                                                    array_push($test, $pnotis);
                                                    $sender = $pnotis;
                                                }
                                                if (!empty($sender)) {

                                                    $notificationsTable = TableRegistry::get('Notifications');
                                                    $notificationData = $notificationsTable->newEntity();

                                                    $notificationData->project_id = $project_id;
                                                    $notificationData->option_data = $optionData;
                                                    $notificationData->action_type = $actionType;
                                                    $notificationData->read_flg = 0;
                                                    $notificationData->created = date('Y-m-d H:i:s');
                                                    $notificationData->user_id = $pnotis;
                                                    $notificationData->action_user_id = $request->user_id;
                                                    if (!$notificationsTable->save($notificationData)) {
                                                        $this->responseApi(0, ('Không lưu được thông báo'));
                                                    }
                                                    $this->PushNoti($actionType, ['project_id' => $receivep, 'request_id' => $request->id], $receivep, $pnotis);
                                                }
                                            }
                                        } else {
                                            $pnotis = $receiver->user_id;
                                            $sender = $pnotis;
                                            $actionType = 25;
                                            if (!empty($sender)) {
                                                $notificationsTable = TableRegistry::get('Notifications');
                                                $notificationData = $notificationsTable->newEntity();
                                                $notificationData->project_id = $project_id;
                                                $notificationData->option_data = $optionData;
                                                $notificationData->action_type = $actionType;
                                                $notificationData->read_flg = 0;
                                                $notificationData->created = date('Y-m-d H:i:s');
                                                $notificationData->user_id = $pnotis;
                                                $notificationData->action_user_id = $request->user_id;
                                                if (!$notificationsTable->save($notificationData)) {
                                                    $this->responseApi( __('Không lưu được thông báo'));
                                                }
                                                $this->PushNoti($actionType, ['user_id' => $receiver->user_id, 'request_id' => $request->id], $receiver->user_id, $pnotis);
                                            }
                                        }
                                    }
                                }
                            }
                            $this->responseApi(1, __('Yêu cầu được duyệt thành công'));
                        } else {
                            $this->responseApi(0, __('Không lưu được yêu cầu'));
                        }


                        //else not accept
                    } else {
                        $data['status'] = 0;
                        $request = $this->Requests->patchEntity($request, $data);
                        if ($this->Requests->save($request)) {
                            $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                            $actionType = 23;
                            $notificationsTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationsTable->newEntity();
                            $notificationData->user_id = $request->admin_id;
                            $notificationData->action_user_id = $token->user_id;
                            $notificationData->project_id = $project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->read_flg = 0;
                            $notificationData->created = date('Y-m-d H:i:s');
                            if (!$notificationsTable->save($notificationData)) {
                                $this->responseApi(0, __('Không lưu được thông báo'));
                            }
                            $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $request->admin_id, $token->user_id);
                            $this->responseApi(1, __('Chuyển lại yêu cầu cho admin'));
                        }
                        $this->responseApi(0, __('Không lưu được yêu cầu'));
                    }
                } else {
                    $this->responseApi(0, __('Bạn không phải là mod'));
                }
            }

        } else {
            $this->responseApi(1031, __('Vui lòng đăng nhập trước khi tạo yêu cầu!'));
        }
    }


}


?>
