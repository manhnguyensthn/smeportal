<?php
use Cake\Core\Configure;

Configure::write('API_ERROR', array(
		1000	=>	'',
		1030	=>	__('Oops, have a problem when saving data. Please try again.'),
		1031	=>	__('Please login before execute action'),
		1032	=>	__('No match data'),
		1033	=>	__('The comment does not exist'),
		1034	=>	__('Project not found'),
        1035    =>  __('You can\'t not see this')
	)
);
