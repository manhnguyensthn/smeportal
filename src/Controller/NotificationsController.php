<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use App\Lib\CoreLib;
/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends AppController {

    public function initialize() {
        parent::initialize();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['updateNotification']);
    }

    // Coder: Giang Dien
    // Date: 2016-11-16
    // Function: Get notification of current user login
    public function getNotificationByCurrentUser() {
        $user = $this->Auth->user();
        $listNotifications = $this->Notifications->find('all', ['conditions' => ['user_id' => $user['id'], 'read_flg' => 0]])->toArray();
        $result = '';
        if (count($listNotifications) > 0) {
            $counter = count($listNotifications);
            $result = '<span class="box-number-notification"><span>' . $counter . '</span></span>';
        } else {
            $result = '<span class="box-number-notification"></span>';
        }
        return $result;
    }

    // Coder: Giang Dien
    // Date: 2016-11-16
    // Function: Get list notifications of current login
    public function getListNotificationsAjax(){
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $listNotifications = $this->Notifications->find('all', ['conditions' => ['user_id' => $user['id']], 'limit' => 6, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']])->toArray();

		  if (count($listNotifications) > 0) {
                $result = '<div class="box-content">
                        <ul class="list-notifications">';
                $userTable = TableRegistry::get('Users');
				$CommentTable = TableRegistry::get('project_comments');
                foreach ($listNotifications as $notification) {
				$data = json_decode($notification->option_data);
				$role = '';	
				if(isset($data->role_id) && !empty($data->role_id))
				{
				  $role_id = $data->role_id;
				  $this->loadModel('Roles');
				  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
					foreach($roles as $rol)
					{
						$role = $rol->role;
					}
				}
				$userAction = $userTable->get($notification->action_user_id);
				$class = ($notification->read_flg == 0) ? ' not-read' : '';
				$timeAgo = $this->getTimeAgo($notification->created);
				$javascriptAction = ($notification->read_flg == 0) ? 'onclick="process_notifications(' . $notification->id . ');"' : '';
				if ($notification->read_flg == 1) {
					$javascriptAction = '';
				}
				
				if(isset($data->comment_id) && !empty($data->comment_id) && $notification->action_type == 6)
				{
				$Comment = $CommentTable->find()
                    ->where(['id' => $data->comment_id])->first();
				if($Comment['user_id'] == $user['id'])
				{		
		
				 $message = $this->getTypeNotificationContent($userAction->first_name . ' ' . $userAction->last_name, $notification->action_type, $notification->option_data,$role,$notification->action_user_id) ;          
				}
				else
				{
					$Users = $userTable->find()
                    ->where(['id' => $Comment['user_id']])->first();
					$username =  $Users['first_name'].' '.$Users['last_name'];
					$projectsTable = TableRegistry::get('Projects');
					$projects = $projectsTable->get($data->project_id);
					$message = __('<a href="javascript:void(0)">'.$userAction->first_name.' '.$userAction->last_name.'</a> has <a href="javascript:void(0)" onlcick = "process_notifications('.$notification->action_user_id.')">replied</a> comment of '.$username.' on project '.$projects->title.'');          
				}
				$result .= '<li class="notification-item' . $class . '" ' . $javascriptAction . ' >
							<p class="notification-title">' .$message. '</p>
							<p class="notification-time">' . $timeAgo . '</p>
						</li>';		
				}
				else
				{	
					$result .= '<li class="notification-item' . $class . '" ' . $javascriptAction . ' >
                                <p class="notification-title">' . $this->getTypeNotificationContent($userAction->first_name . ' ' . $userAction->last_name, $notification->action_type, $notification->option_data,$role,$notification->action_user_id) . '</p>
                                <p class="notification-time">' . $timeAgo . '</p>
                            </li>';
                }
				}
                $result .= '<li class="see-all"><a href="/notifications/seeall">' . __('See All') . '</a></li></ul></div>';
                echo $result;
                die;
            } else {
                echo '';
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-16
    // Function: get type of notification
    public function getTypeNotificationContent($actionName = NULL, $type = 1, $option_data = NULL, $role = '',$user_id ='') {
      $Languages = CoreLib::getLanguage();
	  $LanguageCurrent = CoreLib::getLanguage(false);	
	  $result = '';
      $optionsArr = json_decode($option_data);
			if($LanguageCurrent['name'] === '日本語')
				{
        switch ($type) {
			case 1:
                // Request want to join project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >ユーザー %s</a> さん は %s として プロジェクト %s に 参加を希望しています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName,$role, $project->title);
                break;
            case 2:
                // alert comment on project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> は プロジェクト %s に コメントしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s has applied for your project');
                $result = sprintf($message, $user_id,$actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が プロジェクト %s に %s として 参加しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,  $user_id, $actionName, $project->title, $role);
                break;
		
            case 5:
                // alert accept join project
                $message = __('プロジェクト管理者 <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が、 あなた を さん が、 あなた を %s として プロジェクト %s に 参加することを認証しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName,$role,$project->title);
                break;
            case 6:
                // alert reply comment
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が、プロジェクト %s の コメント に 返信しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が、プロジェクト %s に イイネ！しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 8:
                // alert follow project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん は、プロジェクト %s を フォローしています。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が プロジェクト %s に ファイル を アップロードしました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $message = __('プロジェク管理者 <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が プロジェクト %s を 更新しました。');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> has denied to add you in project %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 12: // N has followed you
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん は あなた を フォローしています。');
                $result = sprintf($message,$user_id, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('あなたは、プロジェクト %s の %s に なるように提案されています。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $project->title, $role);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん は、プロジェクト %s の %s に なるように提案されています');
                $projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message,$user_id, $actionName, $project->title, $role);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が、 プロジェクト %s の チャットで 新しいメッセージ を あなた に 送信しました。');
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が 新しいプロジェクト %s を 作成しました');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
				$projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a>  さん が プロジェクト %s に 参加するよう に あなた を 招待しました');
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
			case 18: // B has update project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('プロジェク管理者 <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a>  さん が プロジェクト %s を 更新しました。');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;	
			case 19: // B has asset project X
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> has asset project %s');
				$result = sprintf($message,$user_id, $actionName, $project->title);
			break;	
			case 20: // B has add  
				$message = __('プロジェクト管理者 <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> さん が、プロジェクト %s の 新しい %s を 追加しました。');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message,$user_id, $actionName, $project->title, $role);
				break;
				}
				}
			else if($LanguageCurrent['name'] === 'English')
				{
					 switch ($type) {
			case 1:
                // Request want to join project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> muốn tham gia doanh nghiệp %s với vai trò %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title,$role);
                break;
            case 2:
                // alert comment on project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã bình luận về doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã đăng ký vào doanh nghiệp của bạn');
                $result = sprintf($message, $user_id,$actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã tham gia doanh nghiệp %s as %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title, $role);
                break;
			case 5:
                // alert accept join project
                $message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã đồng ý thêm bạn vào %s với vai trò %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id,$actionName, $project->title, $role);
                break;
			
            case 6:
                // alert reply comment
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã phản hồi bình luận của bạn trong doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã thích doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 8:
                // alert follow project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã theo dõi doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã tải một tài liệu lên doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã cập nhật doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã <a href="javascript:void(0)">từ chối</a> thêm bạn vào doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã theo dõi bạn');
                $result = sprintf($message,$user_id, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('You are offered to be %s in the project %s');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $role, $project->title);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã được chỉ định %s trong doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $user_id,$actionName, $role, $project->title);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã nhắn tin cho bạn trong doanh nghiệp %s');
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã tạo doanh nghiệp mới %s');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã mời bạn tham gia doanh nghiệp');
                $result = sprintf($message,$user_id, $actionName);
                break;
			case 18: // B has update project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã cập nhật doanh nghiệp %s');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;	
			case 19: // B has asset project X
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> has asset project %s');
				$result = sprintf($message, $user_id,$actionName, $project->title);
				break;	
			case 20: // B has add  
				$message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> đã thêm mới %s trong %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                break;
            case 21: // new request 
                $message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(21,%s);" >%s</a> đã gửi yêu cầu mới từ doanh nghiệp %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);

                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->get($optionsArr->request_id);

                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 22: // duyet  
                $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(22,%s);" >%s</a> đã duyệt yêu cầu %s của %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                break;
            case 23: //tu choi  
                $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(23,%s);" >%s</a> đã từ chối yêu cầu %s của %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                break;
            case 24: //gui tat ca  
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);

                $userTable = TableRegistry::get('Users');
                $user = $userTable->get($optionsArr->user_id);

                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->get($optionsArr->request_id);

                $content = substr($request->title, 0,50) . '...';
                if($user->admin == 0){
                    $message = __('Chủ doanh nghiệp <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> đã gửi yêu cầu từ doanh nghiệp %s'); 
                    $result = sprintf($message,$user_id, $actionName , $project->title);
                } else {
                    $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> thông báo: %s');
                    $result = sprintf($message,$user_id, $actionName, $content);
                }

                break;
            case 25: //gui vai trò  
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);

                $userTable = TableRegistry::get('Users');
                $user = $userTable->get($optionsArr->user_id);

                $requestTable = TableRegistry::get('Requests');
                $request = $requestTable->get($optionsArr->request_id);

                $content = substr($request->title, 0,50) . '...';
                if($user->admin == 0){
                    $message = __('Chủ DN <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> đã gửi yêu cầu từ doanh nghiệp %s'); 
                    $result = sprintf($message,$user_id, $actionName , $project->title);
                } else {
                    $message = __('Admin <a href="javascript:void(0)" onclick="process_notifications(24,%s);" >%s</a> thông báo: %s');
                    $result = sprintf($message,$user_id, $actionName, $content);
                }

                break;         	
        }
		}
		else
		{
					 switch ($type) {
			case 1:
                // Request want to join project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >El usuario %s</a> quiere unirse al proyecto %s como %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title,$role);
                break;
            case 2:
                // alert comment on project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha comentado el proyecto %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha aplicado para su proyecto');
                $result = sprintf($message, $user_id,$actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha participado en el proyecto %s como %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title, $role);
                break;
			case 5:
                // alert accept join project
                $message = __('El propietario del proyecto <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> te ha aceptado agregar te en el proyecto %s como %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id,$actionName, $project->title, $role);
                break;
			
            case 6:
                // alert reply comment
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha respondido a su comentario sobre el proyecto %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> le gusto el proyecto %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 8:
                // alert follow project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> hay seguido el proyecto %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha subido un archivo en el proyecto %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $message = __('Propietario de proyecto <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha actualizado proyecto %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $message = __('Propietario de proyecto <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> has denied to add you in project %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> te ha seguido');
                $result = sprintf($message,$user_id, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('Se le ofrece a ser %s en el proyecto %s');
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $role, $project->title);
                break;
            case 14: //N has been offered <role> in  project Y
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> se ha ofrecido para ser %s en el proyecto %s');
                $projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$result = sprintf($message, $user_id,$actionName, $role, $project->title);
                break;
            case 15: // N đã nhắn tin cho bạn trong doanh nghiệp X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> te ha enviado un mensaje en el chat del proyecto %s');
                $result = sprintf($message, $user_id,$actionName, $project->title);
                break;
            case 16: // B has created new project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> le gusto el proyecto %s');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
            case 17: // B has invited you join a project
                $message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> te ha invitado a unirte al proyecto %s');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;
			case 18: // B has update project X
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $message = __('Propietario de proyecto <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha actualizado proyecto %s');
                $result = sprintf($message,$user_id, $actionName, $project->title);
                break;	
			case 19: // B has asset project X
				$projectTable = TableRegistry::get('Projects');
				$project = $projectTable->get($optionsArr->project_id);
				$message = __('<a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> has asset project %s');
				$result = sprintf($message, $user_id,$actionName, $project->title);
				break;	
			case 20: // B has add  
				$message = __('El propietario del proyecto <a href="javascript:void(0)" onclick="process_notifications(1,%s);" >%s</a> ha agregado un nuevo %s de %s');
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                $result = sprintf($message,$user_id, $actionName, $role, $project->title);
                break;	
        }
		}
        return $result;
    }

    // Coder: Giang Dien
    // Date: 2016-11-16
    // Function: proccess accept join role action
    public function processAcceptJoinRoleAjax() {
        if ($_REQUEST) {
            $user = $this->Auth->user();
            $notification_id = $_REQUEST['notification_id'];
			$this->loadModel('Notifications');
            $notification = $this->Notifications->find('all', ['conditions' => ['id' => $notification_id]])->first();
          	$action_user_id = $notification['action_user_id'];
		    $notification = $this->Notifications->patchEntity($notification, ['read_flg' => 1]);
            $userTable = TableRegistry::get('Users');
            $userRequest = $userTable->get($notification->action_user_id);
            if ($this->Notifications->save($notification)) {
                // Send notification for collaborator
                $optionData = json_decode($notification->option_data);
				$role = '';	
				if(isset($optionData->role_id) && !empty($optionData->role_id))
				{
				  $role_id = $optionData->role_id;
				  $this->loadModel('Roles');
				  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
					foreach($roles as $rol)
					{
						$role = $rol->role;
					}
				}
                $ProjectRoleTable = TableRegistry::get('ProjectsRoles');
                if (isset($optionData->project_role_id)) {
                    $roleInfo = $ProjectRoleTable->find('all', ['conditions' => ['id' => $optionData->project_role_id]])->first();
                } else {
                    $roleInfo = $ProjectRoleTable->find('all', ['conditions' => ['project_id' => $optionData->project_id, 'role_id' => $optionData->role_id]])->first();
                }
                if (!empty($roleInfo)) {
                    $userProjectTable = TableRegistry::get('UsersProjects');
					$this->loadModel('UserProjectActions'); 
                    $collaborators = $userProjectTable->find('all', ['conditions' => ['project_id' => $optionData->project_id, 'UsersProjects.type' => 2, 'user_id !=' => $user['id'], 'UsersProjects.status' => 1]])->contain(['Users'])->distinct('user_id')->toArray();
                    $listUserfollows = $this->UserProjectActions->getListUserFollowProjectAction(['project_id' => $optionData->project_id, 'status' => 1, 'action_type' => 2]);
					$roleCounter = $this->_getNumberCollaboratorByRole($collaborators, $roleInfo->id);
                    // Change status user project
                        $option_data = json_decode($notification->option_data);
                        if (isset($option_data->role_id) && isset($option_data->project_id)) {
                            $userProject = $userProjectTable->find('all', ['conditions' => ['user_id' => $notification->action_user_id, 'project_id' => $option_data->project_id, 'role_id' => $option_data->role_id,'project_role_id'=>$optionData->project_role_id ]])->first();
							$userProject->status = 1;
                            if (!$userProjectTable->save($userProject)) {
                                $this->Flash->error(__('Change status fail.'));
                            }
                        }
				   if ($roleCounter < $roleInfo->quantity) {
                       $this->sendNotificationForUserFollow(4, ['project_id' => $optionData->project_id, 'user_action_id' => $user['id'],'role_id' => $optionData->role_id, 'action_type' => 4], $action_user_id);       
					   $ProjectRoleTable->updateAll(['is_read' => 0], ['project_id' => $optionData->project_id, 'role_id' => $optionData->role_id]);
                        $userid = array();
						foreach($collaborators as $Collaborator)
						{
							$userid[] = $Collaborator->user_id;
						}  				
                        // End send notification for collaborator
                        $actionUser = $userTable->get($notification->action_user_id);
                        $notificationData = $this->Notifications->newEntity();
                        $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' =>  $action_user_id, 'action_user_id' =>$notification->user_id, 'option_data' => $notification->option_data, 'action_type' => 5, 'project_id' => $optionData->project_id, 'read_flg' => 0, 'created' => date('Y-m-d H:i:s')]);
                        if (!$this->Notifications->save($newNotification)) {
                            $this->Flash->error(__('Add notification fail.'));
                        }
						else
						{
							$this->PushNoti(5, ['project_id' => $optionData->project_id] ,$action_user_id,$notification->user_id,$role);	       
							$userRe = $userTable->get($notification->user_id);
							$userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $action_user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');
							$actionType = 5;
                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userRe['first_name'] . ' ' . $userRe['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $notification->option_data);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                       
						}
                       
					 foreach ($collaborators as $row) {
							if($row->user_id != $action_user_id)
							{
                            $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                            $userNotification = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row->user_id]])->first();
                            if (isset($userNotification->project_update_email_status) && $userNotification->project_update_email_status == 1) {
                                $template = 'notification';
                                $from = [EMAIL_LOGIN => __('We The Projects')];
                                $subject = __('[SME] Notification on SME');

                                // GET CONTENT MAIL
                                $name = $userRequest->first_name . ' ' . $userRequest->last_name;
                                $contentMail = $this->getContentNotifySendMail($name, 4, $notification->option_data);
                                // END: GET CONTENT MAIL

                                if (!$this->sendMail($template, $from, $row['user']['email'], ['first_name' => $row['user']->first_name, 'last_name' => $row['user']->last_name, 'notification_content' => $contentMail], $subject)) {
                                    $this->Flash->error(__('Send mail to creator fail.'));	
                                }
                            }
                            if (isset($userNotification->project_update_mobile_status) && $userNotification->project_update_mobile_status == 1) {
                                $notificationData = $this->Notifications->newEntity();
                                $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' => $row->user_id, 'action_user_id' => $action_user_id, 'option_data' => $notification->option_data, 'project_id' => $optionData->project_id, 'action_type' => 4, 'read_flg' => 0, 'created' => date('Y-m-d H:i:s')]);
								$this->PushNoti(4, ['project_id' =>$optionData->project_id] ,$row->user_id,$action_user_id,$role);	  
							   if (!$this->Notifications->save($newNotification)) {
                                    $this->Flash->error(__('Add notification fail.'));
                                }
                            }
							}
                        }
						if (count($listUserfollows) > 0) {
						$userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
						foreach ($listUserfollows as $Userfollow) {
						$actionType = 4;
						if ($Userfollow->user_id != $action_user_id && !in_array($Userfollow->user_id, $userid)) {	
						$userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $Userfollow->user_id]])->first();
					    $optionData = json_encode(['project_id' => $Userfollow->project_id, 'role_id' =>$option_data->role_id]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $Userfollow->user_id;
                            $notificationData->action_user_id = $action_user_id;
                            $notificationData->project_id = $Userfollow->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti(5, ['project_id' =>$Userfollow->project_id] ,$Userfollow->user_id,$action_user_id,$role);	       
							
                        }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $Userfollow->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userRequest['first_name'] . ' ' . $userRequest['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
						}
					}				
						
                        echo __('Bạn vừa đồng ý yêu cầu tham gia doanh nghiệp.');
                        die;
                    } else {
                        echo __('Vai trò đã đầy !');
                        die;
                    }
                } else {
                    echo __('Not match role on database');
                    die;
                }
            } else {
                echo __('Không thể đồng ý yêu cầu tham gia doanh nghiệp này.');
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    private function _getNumberCollaboratorByRole($list = [], $project_role_id = 0) {
        $counter = 0;
        foreach ($list as $row) {
            if ($row->project_role_id == $project_role_id) {
                $counter++;
            }
        }
        return $counter;
    }

    // Coder: Giang Dien
    // Date: 2016-12-09
    // Function: process Denie Join Role Ajax
    public function processDenieJoinRoleAjax() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $notification_id = $this->request->data['notification_id'];
            $notification = $this->Notifications->find('all', ['conditions' => ['id' => $notification_id]])->first();
            $notification = $this->Notifications->patchEntity($notification, ['read_flg' => 1]);
            if ($this->Notifications->save($notification)) {
                $optionData = json_decode($notification->option_data);
                $ProjectRoleTable = TableRegistry::get('ProjectsRoles');
                $ProjectRoleTable->updateAll(['is_read' => 0], ['project_id' => $optionData->project_id, 'role_id' => $optionData->role_id]);
                $userTable = TableRegistry::get('Users');
                $actionUser = $userTable->get($notification->action_user_id);
                $notificationData = $this->Notifications->newEntity();
                $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' => $actionUser->id, 'action_user_id' => $user['id'], 'option_data' => $notification->option_data, 'project_id' => $optionData->project_id, 'action_type' => 11, 'read_flg' => 0, 'created' => date('Y-m-d H:i:s')]);
               	$this->PushNoti(11, $optionData->project_id ,$actionUser->id,  $user['id']);	        
				if (!$this->Notifications->save($newNotification)) {	
                    $this->Flash->error(__('Add notification fail.'));
                }
                echo __('You have dennied a collaborator to join your project.');
                die;
            } else {
                echo __('Could not dennied a collaborator to join your project.');
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-16
    // Function: process read notification ajax
    public function processReadNotificationAjax() {
        if ($this->request->is('post')) {
            $notification_id = $this->request->data['notification_id'];
            $notification = $this->Notifications->find('all', ['conditions' => ['id' => $notification_id]])->first();
            $notification = $this->Notifications->patchEntity($notification, ['read_flg' => 1]);
            if ($this->Notifications->save($notification)) {
                echo 'success';
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-17
    // Function: see all notification for current user login
    public function seeAll() {
        $user = $this->Auth->user();
        // var_dump($user['id']);
        $conditions = ['user_id' => $user['id']];
        if (isset($_GET['projectid'])) {
            $conditions['project_id'] = $_GET['projectid'];
        }
        $listNotifications = $this->Notifications->find('all', ['conditions' => $conditions, 'limit' => 12, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']])->toArray();
		$listNos = array();
        // echo json_encode($listNotifications);
		foreach( $listNotifications as $no)
		{
			$data = json_decode($no->option_data);
            // var_dump($data);
			$no->role_title = '';
			if(isset($data->role_id) && !empty($data->role_id))
			{
			  $role_id = $data->role_id;
			  $this->loadModel('Roles');
			  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
				foreach($roles as $rol)
				{
					$role = $rol->role;
				}
			   $no->role_title = $role;	
			}
            // echo 'a';
			$listNos[] = $no;
		}
		
		$this->set([
            'listNotifications' => $listNos,
            '_serialize' => ['listNotifications']
        ]);
    }

    // Coder: Giang Dien
    // Date: 2016-11-17
    // Function: Load list notification by page
    public function loadNotificationByPage() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $page = $this->request->data['page'];
            $offset = ($page - 1) * 12;
            $listNotifications = $this->Notifications->find('all', ['conditions' => ['user_id' => $user['id']], 'limit' => 12, 'offset' => $offset, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']])->toArray();
            $listNos = array();
			foreach( $listNotifications as $no)
			{
				$data = json_decode($no->option_data);
				$no->role_title = '';
				if(isset($data->role_id) && !empty($data->role_id))
				{
				  $role_id = $data->role_id;
				  $this->loadModel('Roles');
				  $roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
					foreach($roles as $rol)
					{
						$role = $rol->role;
					}
				$no->role_title = $role;	
				}
				$listNos[] = $no;
			}
			$this->set([
                'listNotifications' => $listNos,
                'currentPage' => $page,
                '_serialize' => ['listNotifications', 'currentPage']
            ]);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    public function updateNotification() {
        $listNotifications = $this->Notifications->find('all', ['conditions' => ['project_id' => 0]])->toArray();
        foreach ($listNotifications as $row) {
            $optionData = json_decode($row->option_data);
            $data = $this->Notifications->patchEntity($row, ['project_id' => $optionData->project_id]);
            if ($this->Notifications->save($data)) {
                echo $row->id . ' --- success<br/>';
            } else {
                echo $row->id . ' --- error<br/>';
            }
        }
        die;
    }

    // Code: Giang Dien
    // Date: 2016-12-16
    // Function: process notification
    public function processNotificationAjax() {
        if ($this->request->is('post')) {
            $notification_id = $this->request->data['notification_id'];
            $notification = $this->Notifications->find('all', ['conditions' => ['id' => $notification_id]])->first();
			  $optionData = json_decode($notification->option_data);
            if ($notification->action_type != 1) {
                $notification = $this->Notifications->patchEntity($notification, ['read_flg' => 1]);
                if ($this->Notifications->save($notification)) {
                    // update status success
                    // 0: NULL; 1: want to join; 2: commented; 3: applied; 4: joined; 5: accepted join; 6: replied comment;7: like project; 8: follow project; 9: owner upload to Asset; 10: Project Update Milestone
                    $result = [];
                    switch ($notification->action_type) {
                        case 0:
                            // NULL
                            $result = ['status' => 'true', 'action' => 'reload'];
                            break;
                        
                        case 2:
                        // Commented
                        case 3:
                        // applied
                        case 4:
                        // joined
                        case 5:
                        // accepted join
                        case 6:
                        // repplied
                        case 7:
                        // Like project
                        case 8:
                            // Follow project
						case 9:	
						// asset a project
						$this->loadModel('Projects');
						$project = $this->Projects->find('all', ['conditions' => ['id' => $notification->project_id]])->first();
						if (!empty($project)) {
							$link = \Cake\Utility\Inflector::slug($project->title, '-');
							$link = '/projects/' . $link . '-' . $notification->project_id;
							$result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
						} else {
							$result = ['status' => 'false', 'message' => __('No match data')];
						}
						break;
						case 10:	
						// asset a project
						$this->loadModel('Projects');
						$project = $this->Projects->find('all', ['conditions' => ['id' => $notification->project_id]])->first();
						if (!empty($project)) {
							$link = \Cake\Utility\Inflector::slug($project->title, '-');
							$link = '/projects/' . $link . '-' . $notification->project_id;
							$result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
						} else {
							$result = ['status' => 'false', 'message' => __('No match data')];
						}
						break;	
						case 12:	
						//follow you
							$link = '/applicant-profile-'. $optionData->user_id;
							$result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
						break;						
						case 13:	
						// offer a project
						$this->loadModel('Projects');
						$project = $this->Projects->find('all', ['conditions' => ['id' => $notification->project_id]])->first();
						if (!empty($project)) {
							$link = \Cake\Utility\Inflector::slug($project->title, '-');
							$link = '/projects/' . $link . '-' . $notification->project_id;
							$result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
						} else {
							$result = ['status' => 'false', 'message' => __('No match data')];
						}
						break;		
						case 14:	
						// has offer a project
						$this->loadModel('Projects');
						$project = $this->Projects->find('all', ['conditions' => ['id' => $notification->project_id]])->first();
						if (!empty($project)) {
							$link = \Cake\Utility\Inflector::slug($project->title, '-');
							$link = '/projects/' . $link . '-' . $notification->project_id;
							$result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
						} else {
							$result = ['status' => 'false', 'message' => __('No match data')];
						}
						break;	
                        case 16:
                            // Follow user
                        case 17:
                            // Invite join a project
                            $this->loadModel('Projects');
                            $project = $this->Projects->find('all', ['conditions' => ['id' => $notification->project_id]])->first();
                            if (!empty($project)) {
                                $link = \Cake\Utility\Inflector::slug($project->title, '-');
                                $link = '/projects/' . $link . '-' . $notification->project_id;
                                $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                            } else {
                                $result = ['status' => 'false', 'message' => __('No match data')];
                            }
                            break;
                        case 15:
                            // Chat message
                            $optionDataArr = json_decode($notification->option_data);
                            $roomId = (isset($optionDataArr->room_id)) ? $optionDataArr->room_id : 0;
                            $link = '/chat-room/' . $notification->project_id . '/' . $roomId;
                            $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                            break;
						case 20:	
						// has added new
						$this->loadModel('Projects');
						$project = $this->Projects->find('all', ['conditions' => ['id' => $notification->project_id]])->first();
						if (!empty($project)) {
							$link = \Cake\Utility\Inflector::slug($project->title, '-');
							$link = '/projects/' . $link . '-' . $notification->project_id;
							$result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
						} else {
							$result = ['status' => 'false', 'message' => __('No match data')];
						}
						break;
                        case 21:
                            $optionDataArr = json_decode($notification->option_data);
                            $link = '/requests/respond/'.$optionDataArr->request_id;
                            $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                        break;

                        case 22:
                            $optionDataArr = json_decode($notification->option_data);
                            $link = '/requests/respond/'.$optionDataArr->request_id;
                            $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                        break;

                        case 23:
                            $optionDataArr = json_decode($notification->option_data);
                            $link = '/request/view/'.$optionDataArr->request_id;
                            $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                        break;
                        case 24:
                            $optionDataArr = json_decode($notification->option_data);
                            $link = '/request/view/'.$optionDataArr->request_id;
                            $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                        break;
                        case 25:
                            $optionDataArr = json_decode($notification->option_data);
                            $link = '/request/view/'.$optionDataArr->request_id;
                            $result = ['status' => 'true', 'action' => 'redirect', 'link' => $link];
                        break;

                        default:
                            $result = ['status' => 'true', 'action' => 'reload'];
                    }
                    echo json_encode($result);
                    die;
                } else {
                    // Update status fail
                    $result = ['status' => 'false', 'message' => __('Could not saved data')];
                    echo json_encode($result);
                    die;
                }
            } else {
                // Request want to join
                $result = ['status' => 'true', 'action' => 'confirm', 'notification_id' => $notification_id];
                echo json_encode($result);
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2017-02-10
    // Function: send Invite Request Ajax
    public function sendInviteRequestAjax() {
        if ($this->request->is('POST')) {
            $project_id = $this->request->data['project_id'];
            $user_id = $this->request->data['user_id'];
            $userRequest = $this->Auth->user();
            $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
            $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $user_id]])->first();
            $optionData = ['project_id' => $project_id, 'user_invite' => $userRequest['id']];
            $actionType = 17;
            if (!empty($userNotificationSetting) && $userNotificationSetting->private_message_mobile_status == 1) {
                // add notification on web
                $notificationTable = TableRegistry::get('Notifications');
                $notificationData = $notificationTable->newEntity();
                $notificationData->user_id = $user_id;
                $notificationData->action_user_id = $userRequest['id'];
                $notificationData->project_id = $project_id;
                $notificationData->option_data = json_encode($optionData);
                $notificationData->action_type = $actionType;
                $notificationData->created = date('Y-m-d H:i:s');
                $notificationTable->save($notificationData);
				$this->PushNoti($actionType, ['project_id' => $project_id] , $user_id, $userRequest['id']);	        
            }
            if (!empty($userNotificationSetting) && $userNotificationSetting->private_message_email_status == 1) {
                // send notification via mobile
                $this->loadModel('Users');
                $userInvition = $this->Users->get($user_id);
                $template = 'notification';
                $from = [EMAIL_LOGIN => __('We The Projects')];
                $subject = __('[SME] Notification on SME');

                // GET CONTENT MAIL
                $contentMail = __('Send  notification content');
                $name = $userRequest['first_name'] . ' ' . $userRequest['last_name'];
                $contentMail = $this->getContentNotifySendMail($name, $actionType, json_encode($optionData));
                // END: GET CONTENT MAIL

                $this->sendMail($template, $from, $userInvition['email'], ['first_name' => $userInvition['first_name'], 'last_name' => $userInvition['last_name'], 'notification_content' => $contentMail], $subject);
            }
            $this->Flash->success(__('Send invitation successfully!'));
            die;
            // return true;
        } else {
            $this->Flash->error(__('No match data'));
            $this->redirect('/');
        }
    }

}
