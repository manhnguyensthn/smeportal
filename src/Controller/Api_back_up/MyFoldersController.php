<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UsersProjectsTable $UsersProjects
 */
class MyFoldersController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadModel('Projects');
        $this->loadModel('UsersProjects');
        $this->loadModel('Users');
        $this->loadModel('UserProjectActions');
        $this->loadModel('Notifications');
    }

    public function getMyFolder() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (isset($data['limit']) && !empty($data['limit'])) {
                $limit = $data['limit'];
            } else {
                $limit = 3;
            }
            if (isset($data['page']) && !empty($data['page'])) {
                $page = $data['page'];
            } else {
                $page = 1;
            }
            if (isset($data['offset'])) {
                $offset = $data['offset'];
            } else {
                $offset = ($page - 1) * $limit;
            }
            $conditions = [];
            $fields = [];
            $this->_data['project'] = [];

            if (!empty($token)) {
                switch ($data['type']) {
                    case 0: //created
                        $conditions['user_id'] = $token->user_id;
                        $fields = ['id', 'user_id', 'title', 'image_url'];
                        //get all project user create
                        $projects = $this->Projects->getAllProjectFolder($conditions, $fields, $limit, $offset);
                        if (!empty($projects)) {
                            foreach ($projects as $key => $project) {
                                //get notification not read
                                $project->count_notify_chat = $this->_getChatNotification($token->user_id,$project->id);
                                $project->count_notify = $this->Notifications->getNotificationByProject(['project_id' => $project->id, 'user_id' => $token->user_id, 'read_flg' => 0]);
                                $this->_data['project'][$key] = $project->toArray();

                                //get all user join this project
                                $userPros = $this->UsersProjects->getOneUserProjectFolder(['project_id' => $project->id, 'UsersProjects.status' => 1,'UsersProjects.type'=>2], ['user_id', 'Users.first_name', 'Users.last_name','Users.avatar'],['Users']);
                                if (!empty($userPros)) {
                                    foreach ($userPros as $k => $ups) {
                                        $this->_data['project'][$key]['users'][$k]['user_id'] = empty($ups->user_id) ? 0 : $ups->user_id;
                                        $this->_data['project'][$key]['users'][$k]['name'] = $ups['user']['first_name'].' '.$ups['user']['last_name'];
                                        $this->_data['project'][$key]['users'][$k]['name'] = empty($this->_data['project'][$key]['users'][$k]['name']) ? '' : $this->_data['project'][$key]['users'][$k]['name'];
                                        $this->_data['project'][$key]['users'][$k]['user_picture'] = empty($ups['user']['avatar']) ? '' : $ups['user']['avatar'];
                                    }
                                } else {
                                    $this->_data['project'][$key]['users'] = [];
                                }
                            }
                        }
                        break;
                    case 1: // joined
                        $userProjects = $this->UsersProjects->getAllUserProjectFolder(['UsersProjects.user_id' => $token->user_id,'UsersProjects.status'=>1,'UsersProjects.type'=>2], ['user_id', 'project_id', 'name', 'user_picture'], $limit, $offset, ['Projects'],['UsersProjects.created' => 'DESC'],TRUE);
                        if (!empty($userProjects)) {
                            foreach ($userProjects as $key => $up) {
                                //get project user that joined
                                $projects = $this->Projects->getOneProjectFolder(['id' => $up->project_id], ['id', 'user_id', 'title', 'image_url']);
                                if (!empty($projects)) {
                                    //get notification not read
                                    $projects->count_notify_chat = $this->_getChatNotification($token->user_id,$projects->id);
                                    $projects->count_notify = $this->Notifications->getNotificationByProject(['project_id' => $projects->id, 'user_id' => $token->user_id, 'read_flg' => 0]);
                                    $this->_data['project'][$key] = $projects->toArray();

                                    $this->_data['project'][$key]['users'][] = [
                                        'user_id' => empty($up->user_id) ? 0 : $up->user_id,
                                        'name' => empty($up->name) ? '' : $up->name,
                                        'user_picture' => empty($up->user_picture) ? '' : $up->user_picture
                                    ];

                                    //get all user join in this project
                                    $allUP = $this->UsersProjects->getOneUserProjectFolder(['project_id' => $projects->id,'UsersProjects.status' => 1,'UsersProjects.type'=>2], ['user_id', 'Users.first_name', 'Users.last_name','Users.avatar'],['Users']);
                                    $this->log(count($allUP).'\n');
                                    if (!empty($allUP)) {
                                        $this->_data['project'][$key]['users'] = [];
                                        foreach ($allUP as $aups) {
                                            $this->_data['project'][$key]['users'][] = [
                                                'user_id' => empty($aups->user_id) ? '' : $aups->user_id,
                                                'name' => $aups['user']['first_name'].' '.$aups['user']['last_name'],
                                                'user_picture' => empty($aups['user']['avatar']) ? '' : $aups['user']['avatar']
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 2: //followed
                        $userProjectActions = $this->UserProjectActions->getAllUserProjectActionFolder(['user_id' => $token->user_id, 'action_type' => 2,'status'=>1], ['id', 'user_id', 'project_id', 'action_type'], $limit, $offset);
                        if (!empty($userProjectActions)) {
                            foreach ($userProjectActions as $key => $upa) {
                                //get project user that followed
                                $projects = $this->Projects->getOneProjectFolder(['id' => $upa->project_id], ['id', 'user_id', 'title', 'image_url']);
                                if (!empty($projects)) {
                                    //get notification not read
                                    $projects->count_notify_chat = $this->_getChatNotification($token->user_id,$projects->id);
                                    $projects->count_notify = $this->Notifications->getNotificationByProject(['project_id' => $projects->id, 'user_id' => $token->user_id, 'read_flg' => 0]);
                                    $this->_data['project'][$key] = $projects->toArray();
                                    $this->_data['project'][$key]['users'][] = [
                                        'user_id' => empty($upa->user_id) ? 0 : $upa->user_id,
                                        'name' => empty($upa->name) ? '' : $upa->name,
                                        'user_picture' => empty($upa->user_picture) ? '' : $upa->user_picture
                                    ];

                                    //get all user join in this project
                                    $allUPA = $this->UsersProjects->getOneUserProjectFolder(['project_id' => $projects->id, 'UsersProjects.status' => 1,'UsersProjects.type'=>2], ['user_id', 'Users.first_name', 'Users.last_name','Users.avatar'],['Users']);
                                    if (!empty($allUPA)) {
                                        $this->_data['project'][$key]['users'] = [];
                                        foreach ($allUPA as $upas) {
                                            $this->_data['project'][$key]['users'][] = [
                                                'user_id' => empty($upas->user_id) ? '' : $upas->user_id,
                                                'name' => $upas['user']['first_name'].' '.$upas['user']['last_name'],
                                                'user_picture' => empty($upas['user']['avatar']) ? '' : $upas['user']['avatar']
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    default :
                        break;
                }
                $this->_status = 1;
                if (!empty($this->_data)) {
                    $this->responseApi($this->_status, '', $this->_data);
                } else {

                    $this->responseApi($this->_status, '', $this->_data);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }
    
    // Coder: Giang Dien
    // Date: 17-01-2017
    // Function: get chat notification
    private function _getChatNotification($userId = 0, $projectId = 0){
        $this->loadModel('ChatNotifications');
        $listNotifications = $this->ChatNotifications->getNotificationByOptions(['ChatRooms.project_id'=>$projectId,'receive_id'=>$userId,'read_flg'=>0],[],0,0,['ChatRooms']);
        return count($listNotifications);
    }

}
