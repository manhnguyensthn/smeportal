<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
    }

    public function login() {
        // login process
        if ($this->request->is('post')) {
            $user = $this->Users->loginApi($this->request->data['email'], $this->request->data['password']);
            if ($user) {
                $token = $this->generateRandomString();
                $platform = isset($this->request->data['platform']) ? $this->request->data['platform'] : 0;
                $devicetoken = isset($this->request->data['devicetoken']) ? $this->request->data['devicetoken'] : NULL;

                $user = $this->Users->patchEntity($user, ['device_token' => $devicetoken]);
                if ($this->Users->save($user)) {
                    $this->clearToken($user->id, $platform);
                    $this->createToken($user->id, $platform, $token);
                    $this->_status = 1;
                    $this->_data = [];
                    $this->_data['token'] = $token;
                    $this->_message = __('Login success');
                }
            } else {
                $this->_message = __('Invalid username or password.');
                $this->_data = [];
                $this->_status = 0;
            }
            $this->responseApi($this->_status, $this->_message, $this->_data);
        }
    }

    /**
     * Register method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function register() {
        $this->_data = $this->Users->newEntity();
        // register process
        if ($this->request->is('post')) {
            if (isset($this->request->data['newsletters'])) {
                $this->request->data['newsletters'] = NEWSLETTER_ON;
            }
            if (empty($this->request->data['name ']) && isset($this->request->data['first_name']) && isset($this->request->data['last_name'])) {
                $this->request->data['name'] = $this->request->data['first_name'] . ' ' . $this->request->data['last_name'];
            }
            $this->request->data['device_token'] = isset($this->request->data['device_token']) ? $this->request->data['device_token'] : '';

            $this->_data = $this->Users->patchEntity($this->_data, $this->request->data);
            if ($this->Users->save($this->_data)) {
                $this->loadModel('UserNotificationSetting');
                $respone = $this->UserNotificationSetting->addUserNotificationSetting(['user_id' => $this->_data->id]);
                if ($respone == 'TRUE') {
                    $template = 'register';
                    $from = [EMAIL_LOGIN => __('We The Projects')];
                    $subject = __('[SME] Welcome');
                    if ($this->sendMail($template, $from, $this->request->data['email'], [], $subject)) {
                        $this->_message = __('Register success.');
                        $this->_status = 1;
                    }
                } else {
                    $this->_message = __('Register fail. Please try again.');
                    $this->_status = 0;
                }
            } else {
                $this->_message = __('Register fail. Please try again');
                foreach ($this->_data->errors() as $key => $value) {
                    $this->_message = implode(array_values($value));
                    break;
                }
                $this->_data = [];
                $this->_status = 0;
            }
        }
        $this->responseApi($this->_status, $this->_message, $this->_data);
    }

//    params : sns_id, email, type, platform
    public function loginSns() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $devicetoken = isset($this->request->data['devicetoken']) ? $this->request->data['devicetoken'] : NULL;

            $userExist = $this->Users->getUserbyEmail($this->request->data['email']);
            if (!empty($userExist)) {
                $check = false;

                if (($this->request->data['type'] == 1) && ($userExist->fb_id == $this->request->data['sns_id'])) { // facebook
                    $check = true;
                } elseif (($this->request->data['type'] == 2) && ($userExist->google_id == $this->request->data['sns_id'])) {  // google
                    $check = true;
                } elseif (($this->request->data['type'] == 3 ) && ($userExist->linked_id == $this->request->data['sns_id'])) { // linkedIn
                    $check = true;
                }

                if ($check) {
                    $user = $this->Users->patchEntity($userExist, ['device_token' => $devicetoken]);
                    if ($this->Users->save($user)) {
                        $platform = isset($this->request->data['platform']) ? $this->request->data['platform'] : 2;

                        //clear token in other device
                        $this->clearToken($userExist->id, $platform);

                        $tokenTable = TableRegistry::get('Tokens');
                        $token = $tokenTable->newEntity();
                        $tk = $this->generateRandomString();

                        $token->user_id = $userExist->id;
                        $token->token = $tk;
                        $token->created = date('Y-m-d H:i:s');
                        $token->platform = $platform;

                        if ($tokenTable->save($token)) {
                            $this->createToken($userExist->id, $token->platform, $tk);

                            $this->_status = 1;
                            $this->_data['token'] = $tk;
                            $this->responseApi($this->_status, '', $this->_data);
                            exit;
                        }
                    }
                } else {
                    $this->responseApi(0, __('This email has been registered.'), []);
                    exit;
                }
            } else {
                if ((trim($this->request->data['last_name']) == "") || (trim($this->request->data['first_name']) == "")) {
                    $this->_message = __('Your Facebook/Google+/LinkedIn account is missing either first or last name which is not allowed at SME. Please use a different method to sign-up with SME.');
                    $this->responseApi(0, $this->_message, []);
                    exit;
                }

                $userTable = TableRegistry::get('Users');
                $users = $userTable->newEntity();

                $users->email = $this->request->data['email'];
                $users->first_name = $this->request->data['first_name'];
                $users->last_name = $this->request->data['last_name'];
                $users->name = $this->request->data['first_name'] . ' ' . $this->request->data['last_name'];
                $users->password = $this->generateRandomPassword();
                $users->device_token = $devicetoken;

                if ($this->request->data['type'] == 1) { // facebook
                    $users->fb_id = $this->request->data['sns_id'];
                } elseif ($this->request->data['type'] == 3) { // linkedIn
                    $users->linked_id = $this->request->data['sns_id'];
                } elseif ($this->request->data['type'] == 2) {   // google
                    $users->google_id = $this->request->data['sns_id'];
                }

//                $user = $this->Users->newEntity();
//                $users = $this->Users->patchEntity($user, $this->request->data);

                if ($userTable->save($users)) {
                    $this->loadModel('UserNotificationSetting');
                    $respone = $this->UserNotificationSetting->addUserNotificationSetting(['user_id' => $users->id]);
                    if ($respone == 'TRUE') {
                        $template = 'register';
                        $from = [EMAIL_LOGIN => __('We The Projects')];
                        $subject = __('[SME] Welcome');
                        $platform = isset($this->request->data['platform']) ? $this->request->data['platform'] : PLATFORM_IOS;
                        if ($this->sendMail($template, $from, $this->request->data['email'], [], $subject)) {
                            $token = $this->generateRandomString();
                            $this->createToken($users->id, $platform, $token);

                            $this->_data['token'] = $token;
                            $this->responseApi(1, '', $this->_data);
                            exit;
                        } else {
                            $this->responseApi(0, __('Email can not be send.'), []);
                            exit;
                        }
                    }
                } else {
                    $this->responseApi(0, __('Could not save user information.'), []);
                    exit;
                }
            }
        }
    }

    public function forgotPass() {
        $this->set('title', __('Forgot Password - We the projects'));
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($this->request->isJson()) {
                foreach ($data as $dk => $vk) {
                    if ($vk == '' || $vk == null) {
                        $this->_status = 0;
                        $this->_message = __($dk . " can't be blank");
                        $this->_data = [];
                        break;
                    }
                }
            }
            $users = $this->Users->find('all', [
                'conditions' => ['email' => $data['email'], 'status' => 1],
                'fields' => ['id', 'email', 'status']
            ]);
            $users = $users->first();
            if (empty($users)) {
                if ($this->request->isJson()) {
                    $this->_status = 1;
                    $this->_message = __('Invalid email');
                    $this->_data = [];
                } else {
                    $this->Flash->error(__('Invalid email.'));
                }
            } else {
                $users->toArray();
                $token = $this->generateRandomString();
                $time = time();
                $url = '/users/update_pass?tk=' . $token . '&ti=' . $time;

                $template = 'forgot';
                $viewVars = ['link' => ROOT_URL . $url];
                $from = [EMAIL_LOGIN => __('We The Projects')];
                $to = $data['email'];
                $subject = __('[SME] Reset Password');
                if ($this->sendMail($template, $from, $to, $viewVars, $subject)) {
                    $user = $this->Users->get($users['id']);
                    $user = $this->Users->patchEntity($user, ['token' => $token, 'time_limit' => md5($time)]);
                    if ($this->Users->save($user)) {
                        if ($this->request->isJson()) {
                            $this->_status = 1;
                            $this->_message = __('Email password reset has been sent.');
                            $this->_data = [];
                        } else {
                            $this->Flash->success(__('Email password reset has been sent.'));
                        }
                    }
                } else {
                    $this->Flash->error(__('Email can not be send. Please try again!'));
                }
            }
            $this->set([
                'message' => $this->_message,
                'data' => $this->_data,
                'status' => $this->_status,
                '_serialize' => ['message', 'data', 'status']
            ]);
        }
    }

    public function updatePass() {
        $this->set('title', __('Reset Password - We the projects'));
        $token = $time = '';
        $token = $this->request->query('tk');
        $time = $this->request->query('ti');
        $users = $this->Users->find('all', [
                    'conditions' => ['token' => $token, 'time_limit' => md5($time), 'status' => 1],
                ])->first();
        if (!empty($users)) {
            if (time() - $time > 1800) {
                if ($this->request->isJson()) {
                    $this->_status = 0;
                    $this->_message = __("Your password reset link has expried. Please re-activate the link.");
                    $this->_data = [];
                } else {
                    return $this->redirect(['action' => 'expriedPass']);
                }
            } else {
                if ($this->request->is(['patch', 'post', 'put'])) {
                    $this->request->data['token'] = "";
                    $this->request->data['time_limit'] = "";
                    $users = $this->Users->patchEntity($users, $this->request->data);

                    if ($this->Users->save($users)) {
                        if ($this->request->isJson()) {
                            $this->_status = 1;
                            $this->_message = __('Your password has been reset.');
                            $this->_data = [];
                        } else {
                            $this->Flash->success(__('Your password has been reset.'));
                            return $this->redirect(['action' => 'login']);
                        }
                    } else {
                        if ($this->request->isJson()) {
                            $this->_status = 0;
                            foreach ($users->errors() as $key => $value) {
                                $this->_message = implode(array_values($value));
                                break;
                            }
//                            $this->_message = __('Your password has been reset.');
                            $this->_data = [];
                        } else {
                            $this->Flash->error(__('Can not reset password'));
                            $this->_data = $users;
                        }
                    }
                }
            }
        } else {
            if ($this->request->isJson()) {
                $this->_status = 0;
                $this->_message = __("Your password reset link has expried. Please re-activate the link.");
                $this->_data = [];
            } else {
                return $this->redirect(['action' => 'expriedPass']);
                return $this->redirect(['action' => 'login']);
            }
        }
//        pr($users);
        $this->set('users', $users);
        $this->set([
            'message' => $this->_message,
            'data' => $this->_data,
            'status' => $this->_status,
            '_serialize' => ['message', 'data', 'status']
        ]);
    }

    public function expriedPass() {
        $this->viewBuilder()->layout('ajax');
        $this->set('title', __('Expried Password - We the projects'));
    }

    // action detail user
    public function detail() {
        $id = $this->Auth->user('id');
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            pr($this->request->data);
            die;
            if (isset($this->request->data['update'])) {
                $token = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($token)) {
                    $this->Flash->success(__('The token has been saved.'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The token could not be saved. Please, try again.'));
                }
            }
        }
        $this->set('title', __('Account details - We the projects'));
        $this->set([
            'message' => $this->_message,
            'data' => $user,
            'status' => $this->_status,
            '_serialize' => ['message', 'data', 'status']
        ]);
    }

    //function notification
    public function notifications() {
        $this->set('title', __('Notifications - We the projects'));
    }

    //check password
    public function checkP() {
        $this->autoRender = FALSE;
        if ($this->request->is('post')) {
            $id = $this->Auth->user('id');
            $user = $this->Users->get($id);
            if (password_verify($this->request->data['p'], $user->password)) {
                echo json_encode(['ret' => 'OK']);
                exit();
            } else {
                echo json_encode(['ret' => 'NG']);
                exit();
            }
        }
    }

    public function getPaymentInfo() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $user = $this->Users->get($token->user_id, [
                    'fields' => ['email', 'card_number']
                ]);
                if (!empty($user)) {
                    $this->_data['email'] = $user->email;
                    $this->_data['card_number'] = $user->card_number;
                    $this->responseApi(1, '', $this->_data);
                } else {
                    $this->responseApi(0, __('User does not exist.'), []);
                }
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    public function changePass() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $users = $this->Users->get($token->user_id, [
                    'fields' => ['email', 'password']
                ]);
                if (!empty($users)) {
                    if (password_verify($data['old_password'], $users->password)) {
                        $usersTable = TableRegistry::get('Users');
                        $user = $usersTable->get($token->user_id);

                        $user->password = $data['password'];
                        if ($usersTable->save($user)) {
                            $this->responseApi(1, __('Your password has been changed.'), []);
                        } else {
                            $this->responseApi(0, __('Could not save your password. Please try again!'), []);
                        }
                    } else {
                        $this->responseApi(0, __('Old Password is invalid. Please try again!'), []);
                    }
                } else {
                    $this->responseApi(0, __('User does not exist.'), []);
                }
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    public function changeEmail() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                //check email
                if ($this->Users->checkExitEmail($data['email']))
                    $this->responseApi(0, __('This email has been registered.'));

                $user = $this->Users->get($token->user_id, [
                    'fields' => ['email', 'first_name', 'last_name']
                ]);

                if ($data['email'] != $user->email) {
                    $usersTable = TableRegistry::get('Users');
                    $users = $usersTable->get($token->user_id);
                    $users->secondary_email = $data['email'];

                    if ($usersTable->save($users)) {
                        $template = 'changeemail';
                        $from = [EMAIL_LOGIN => __('We The Projects')];
                        $subject = __('[SME] Confirm your email change on SME');
                        if ($this->sendMail($template, $from, $user->email, ['first_name' => $user->first_name, 'last_name' => $user->last_name, 'email' => $user->email], $subject)) {
                            $this->responseApi(1, __('Please check email to confirm change of your email address on SME.'), []);
                        } else {
                            $this->responseApi(0, __('Not send mail at this time.'), []);
                        }
                    } else {
                        $this->responseApi(0, __('Your email not change.'), []);
                    }
                } else {
                    $this->responseApi(0, __('Your email not change.'), []);
                }
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    public function confirmChangeEmail() {
        if ($this->request->is('get') && isset($this->request->query['token'])) {
            $email = base64_decode($this->request->query('token'));
            $user = $this->Users->find('all', ['conditions' => ['email' => $email]])->first();
            if (!empty($user)) {
                $dataUser = $this->Users->patchEntity($user, ['email' => $user['secondary_email']]);
                if ($this->Users->save($dataUser)) {
                    $this->responseApi(1, __('Your email has been changed.'), []);
                } else {
                    $this->responseApi(0, __('Change email address fail.'), []);
                }
                return $this->redirect('/');
            } else {
                $this->responseApi(0, __('Email not found.'), []);
            }
        } else {
            $this->responseApi(0, __('Invalid Token.'), []);
        }
    }

    public function updatePaymentInfo() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;

            if (!is_numeric($data['cvv_number'])) {
                $this->responseApi(0, __('Invalid CVV number format. This field must on numeric.'), []);
                die();
            }

            if (!is_numeric($data['card_number'])) {
                $this->responseApi(0, __('Invalid Card Number format. This field must on numeric.'), []);
                die();
            }

            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $user = $this->Users->get($token->user_id, [
                    'fields' => ['email', 'first_name', 'last_name']
                ]);

                if (!empty($user)) {
                    $usersTable = TableRegistry::get('Users');
                    $users = $usersTable->get($token->user_id);
                    $users->card_number = (!empty($data['card_number'])) ? $data['card_number'] : '';
                    $users->card_owner_name = (!empty($data['card_owner_name'])) ? $data['card_owner_name'] : '';
                    $users->expiry_month = (!empty($data['expiry_month'])) ? $data['expiry_month'] : '';
                    $users->expiry_year = (!empty($data['expiry_year'])) ? $data['expiry_year'] : '';
                    $users->cvv_number = (!empty($data['cvv_number'])) ? $data['cvv_number'] : '';

                    if ($usersTable->save($users)) {
                        $this->responseApi(1, __('Your payment information has been updated.'), []);
                    } else {
                        $this->responseApi(0, __('Could not update your payment information.'), []);
                    }
                } else {
                    $this->responseApi(0, __('Users does not exist.'), []);
                }
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    private function generateRandomPassword($length = 16) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
