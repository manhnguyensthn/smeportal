<?php

namespace App\Controller\Api;

/**
 * @copyright	[PHPFOX_COPYRIGHT]
 * @author  	Roxannie Nguyen jr
 * @version 	MettingsController.php 1.0.0 2016-01-03 10:49:00 Roxannie Nguyen jr $
 */
use App\Controller\Api\ApiController;
use App\Controller\MettingsController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

class AssetController extends ApiController {

    public $components = array('RequestHandler', 'PushNotification');

    public function initialize() {
        parent::initialize();
    }

    /**
     * API ASSET
     * DATA: {'token', 'project_id'}
     * @author Roxannie Nguyen jr
     */
    public function asset() {

        $bJson = (!$this->request->is('json') ? false : true);
        $bPost = (!$this->request->is('post') ? false : true);
        $bOwner = false;
        $aRequestData = $this->request->data;

        if (!$bJson || !$bPost) {
            $this->_message = __('Unable to process your request. User not found.');
            $this->_status = 1;
        }

        if ($this->_status == 0) {
            $UserId = $this->checkToken();
        }

        if ($this->_status == 0) {
            if (!isset($aRequestData['project_id']) || empty($aRequestData['project_id'])) {
                $this->_message = __('Unable to process your request');
                $this->_status = 1;
            } else {
                $this->loadModel('Projects');
                $ProjectId = $aRequestData['project_id'];
                $Project = $this->Projects->getProject($ProjectId);
                if (!$Project) {
                    $this->_message = __('Unable to process your request');
                    $this->_status = 1;
                } else {
                    $UserIdOwner = (isset($Project->user->id) ? $Project->user->id : 0);
                }
            }
        }

        $MetingsControler = new MettingsController();
        if ($this->_status == 0) {
            $Path = $MetingsControler->_getRootDropbox($UserIdOwner, $ProjectId);
            if (!$Path) {
                $this->_message = __('Unable to process your request');
                $this->_status = 1;
            }
        }

        if ($this->_status == 0) {
            if ($UserId != $UserIdOwner) {
                $this->loadModel('UsersProjects');
                $UserProject = $this->UsersProjects->getUserProjectsByOptions(['user_id' => $UserId, 'project_id' => $Project->id, 'status' => 1, 'type' => 2]);
                if (!$UserProject) {
                    $this->_message = __('Unable to process your request');
                    $this->_status = 1;
                }
            }
        }


        if ($this->_status == 0) {
            $ParamListFolder = array('path' => $Path);
            $ListFolders = $MetingsControler->_callCurl($MetingsControler->_ListFolder, $ParamListFolder);

            if ($UserId == $UserIdOwner) {
                $bOwner = true;
                if (empty($ListFolders) || isset($ListFolders['error'])) {
                    $CreateFolder = $MetingsControler->_callCurl($MetingsControler->_CreateFolder, $ParamListFolder);
                    $ListFolders = $MetingsControler->_callCurl($MetingsControler->_ListFolder, $ParamListFolder);
                }
            }

            if (isset($ListFolders['entries']) && !empty($ListFolders['entries'])) {
                foreach ($ListFolders['entries'] as $key => $ListFolder) {
                    $MetingsControler->_validateDataDropbox($ListFolders['entries'][$key]);
                }
                krsort($ListFolders['entries']);
            }

            $this->_data['bOwner'] = $bOwner;
            $this->_data['ListFolders'] = (isset($ListFolders) && !empty($ListFolders) ? $ListFolders : array());
            $this->_data['Project'] = (isset($Project) && !empty($Project) ? $Project : array());
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
    }

    /**
     * API DOWNLOAD FILE DROPBOX
     * DATA: {'token', 'project_id', 'name_file'}
     * @author Roxannie Nguyen jr
     */
    public function downloadFileDropbox() {
        $bJson = (!$this->request->is('json') ? false : true);
        $bPost = (!$this->request->is('post') ? false : true);
        $bOwner = false;
        $aRequestData = $this->request->data;

        if (!$bJson || !$bPost) {
            $this->_message = __('Unable to process your request. User not found.');
            $this->_status = 1;
        }

        if ($this->_status == 0) {
            $UserId = $this->checkToken();
        }

        if ($this->_status == 0) {
            if (!isset($aRequestData['project_id']) || empty($aRequestData['project_id'])) {
                $this->_message = __('Unable to process your request');
                $this->_status = 1;
            } else {
                $this->loadModel('Projects');
                $ProjectId = $aRequestData['project_id'];
                $Project = $this->Projects->getProject($ProjectId);
                if (!$Project) {
                    $this->_message = __('Unable to process your request');
                    $this->_status = 1;
                } else {
                    $UserIdOwner = (isset($Project->user->id) ? $Project->user->id : 0);
                }
            }
        }

        $MetingsControler = new MettingsController();
        if ($this->_status == 0) {
            $Path = $MetingsControler->_getRootDropbox($UserIdOwner, $ProjectId);
            if (!$Path) {
                $this->_message = __('Unable to process your request');
                $this->_status = 1;
            }
        }

        if ($this->_status == 0) {
            if ($UserId != $UserIdOwner) {
                $this->loadModel('UsersProjects');
                $UserProject = $this->UsersProjects->getUserProjectsByOptions(['user_id' => $UserId, 'project_id' => $Project->id, 'status' => 1, 'type' => 2]);
                if (!$UserProject) {
                    $this->_message = __('Unable to process your request');
                    $this->_status = 1;
                }
            }
        }

        if ($this->_status == 0) {
            if (!isset($aRequestData['name_file']) || empty($aRequestData['name_file'])) {
                $this->_message = __('Unable to process your request');
                $this->_status = 1;
            } else {
                $NameFile = $aRequestData['name_file'];
            }
        }

        if ($this->_status == 0) {
            $Path = $MetingsControler->_getRootDropbox($UserIdOwner, $ProjectId);
            $Path .= '/' . $NameFile;
            $data = array('path' => $Path);
            $FileInfo = $MetingsControler->_callCurl($MetingsControler->_TemporaryLink, $data);
            if (is_array($FileInfo) && !empty($FileInfo)) {
                $this->_data['FileInfo'] = $FileInfo;
            } else {
                $this->_status = 1;
                $this->_message = __('Download failed, please try again.');
            }
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
    }

}
