<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
    }

    public function show() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $conditions = [
                    'user_id' => $token->user_id,
                    'project_id' => $data['project_id']
                ];
                $notifications = $this->Notifications->find('all', ['conditions' => $conditions, 'limit' => 10, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']]);
                if(!empty($notifications)){
                    $read = 0;
                    $this->_data['notification'] = [];
                    $this->_status = 1;
                    $this->_message = '';
                    
                    $this->loadModel('Users');
                    foreach ($notifications as $notification){
                        if($notification->read_flg == 0){
                            $read ++;
                        }
                        $user = $this->Users->get($notification->action_user_id, ['fields' => ['id', 'name']])->toArray();
                        if(!empty($user)){
                            $this->_data['notification'] = [
                                'id'        => $notification->id,
                                'message'   => $this->getTypeNotificationContent($user->name, $notification->action_type, $notification->option_data),
                                'read'      => $notification->read_flg,
                                'created'   => $notification->created,
                                'msg_not_read' => $read
                            ];
                        }
                    }
                }
            } else {
                $this->_status = 0;
                $this->_message = __('Invalid Token');
                $this->_data = [];
            }
        } else {
            $this->_status = 0;
            $this->_message = __('You dont permission to access.');
            $this->_data = [];
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }

    public function getListNotification(){
        $this->_status = 1;
        if ($this->request->is('post')) {
            $RequestData = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $RequestData['token']],
            ])->first();

            $limit = 6;
            if(isset($RequestData['limit']) && !empty($RequestData['limit'])){
                $limit = ((int)$RequestData['limit'] > 0 ? (int)$RequestData['limit'] : 6);
            }

            $page = 1;
            if(isset($RequestData['page']) && !empty($RequestData['page'])){
                $page = (int)$RequestData['page'];
            }

            if($token){
                $NotificationsTable = TableRegistry::get('Notifications');
                $ListNotifications = $NotificationsTable->find('all', ['conditions' => ['user_id' => $token->user_id], 'limit' => $limit, 'page' => $page, 'order' => ['read_flg' => 'ASC', 'created' => 'DESC']])->toArray();
            }else{
                $this->_status  = 0;
                $this->_message = __('Invalid Token');
            }

            if($this->_status == 1){
                $UsersTable = TableRegistry::get('Users');
                $Notifications = array();
                if(isset($ListNotifications) && $ListNotifications){
                    foreach ($ListNotifications as $key => $Notification) {
                        $user = $UsersTable->get($Notification->action_user_id, ['fields' => ['id', 'name']])->toArray();
                        $Notifications[$key]['id']          = $Notification->id;
                        $Notifications[$key]['action_type'] = $Notification->action_type;
                        $Notifications[$key]['created']     = $Notification->created;
                        $Notifications[$key]['time_text']   = $this->getTimeAgo($Notification->created);
                        $Notifications[$key]['read_flg']    = $Notification->read_flg;
                        $Notifications[$key]['message']     = $this->getTypeNotificationContent($user['name'], $Notification->action_type, $Notification->option_data);
                    }
                }
                $this->_data['Notifications'] = $Notifications;
            }
        }

        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }
    
    public function getTypeNotificationContent($actionName = null, $type = 1, $option_data = null) {
        $result = '';
        $optionsArr = json_decode($option_data);
        switch ($type) {
            case 1:
                // Request want to join project
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('%s wants to join project %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 2:
                // alert comment on project
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('%s đã bình luận về doanh nghiệp %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 3:
                // apply project
                $message = __('%s has applied for your project');
                $result  = sprintf($message, $actionName);
                break;
            case 4:
                // alert to collaborator has someone joined project
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('%s has joined on project %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 5:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('Chủ doanh nghiệp %s đã đồng ý thêm bạn %s với vai tro %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 6:
                // alert reply comment
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('%s has replied your comment on project %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 7:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('%s đã thích doanh nghiệp %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 8:
                // alert like project
                $projectTable = TableRegistry::get('Projects');
                $project      = $projectTable->get($optionsArr->project_id);
                $message      = __('%s đã theo dõi doanh nghiệp %s');
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 9:
                // owner upload to Asset
                $projectTable = TableRegistry::get('Projects');
                $message      = __('%s has uploaded a file on project %s');
                $project      = $projectTable->get($optionsArr->project_id);
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 10:
                // Project Update Milestone
                $projectTable = TableRegistry::get('Projects');
                $message      = __('Project owner %s has updated project %s');
                $project      = $projectTable->get($optionsArr->project_id);
                $result       = sprintf($message, $actionName, $project->title);
                break;
            case 11:
                // alert accept join project
                $projectTable = TableRegistry::get('Projects');
                $message      = __('Chủ doanh nghiệp %s từ chối bạn tham gia doanh nghiệp %s');
                $project      = $projectTable->get($optionsArr->project_id);
                $result       = sprintf($message, $actionName, $project->title);
                break;

            case 12: // N has followed you
                $message = __('%s has followed you');
                $result  = sprintf($message, $actionName);
                break;
            case 13: //You are offered to be <role> in the project Y
                $message = __('You are offered to be %s in the project %s');
                $RoleName = '';
                $ProjectName = '';
                if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                    $RolesTable = TableRegistry::get('Roles');
                    $Role = $RolesTable->get($optionsArr->role_id);
                    if (isset($Role->role) && !empty($Role->role)) {
                        $RoleName = $Role->role;
                    }
                }
                if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($optionsArr->project_id);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $RoleName, $ProjectName);
                break;
            case 14: //N has been offered <role> in  project Y
                $message     = __('%s has been offered %s in project %s');
                $RoleName    = '';
                $ProjectName = '';
                if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                    $RolesTable = TableRegistry::get('Roles');
                    $Role = $RolesTable->get($optionsArr->role_id);
                    if (isset($Role->role) && !empty($Role->role)) {
                        $RoleName = $Role->role;
                    }
                }
                if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($optionsArr->project_id);
                    if (isset($project->title) && !empty($project->title)) {
                        $ProjectName = $project->title;
                    }
                }
                $result = sprintf($message, $actionName, $RoleName, $ProjectName);
                break;
        }
        return $result;
    }
}