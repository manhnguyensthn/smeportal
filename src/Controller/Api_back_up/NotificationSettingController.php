<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * NotificationSetting Controller
 *
 * @property \App\Model\Table\UserNotificationSettingTable $UserNotificationSetting
 */
class NotificationSettingController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
        
        //load model
        $this->loadModel('UserNotificationSetting');
    }

    public function setting() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $notifications = $this->UserNotificationSetting->getNotificationByUser($token->user_id);
                if (!empty($notifications)) {
                    $this->_status = 1;
                    $this->_message = '';
                    $this->_data = $notifications;
                }
            } else {
                $this->_status = 0;
                $this->_message = __('Invalid Token');
                $this->_data = [];
            }
        } else {
            $this->_status = 0;
            $this->_message = __('You dont permission to access.');
            $this->_data = [];
        }
        
        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }

    public function update() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $dataSave = [
                    'replies_comment_email_status' => $data['replies_comment_email_status'],
                    'replies_comment_mobile_status' => $data['replies_comment_mobile_status'],
                    'private_message_email_status' => $data['private_message_email_status'],
                    'private_message_mobile_stutus' => $data['private_message_mobile_stutus'],
                    'group_message_email_status' => $data['group_message_email_status'],
                    'group_message_mobile_status' => $data['group_message_mobile_status'],
                    'new_follower_email_status' => $data['new_follower_email_status'],
                    'new_follower_mobile_status' => $data['new_follower_mobile_status'],
                    'project_update_email_status' => $data['project_update_email_status'],
                    'project_update_mobile_status' => $data['project_update_mobile_status'],
                    'member_join_project_email_status' => $data['member_join_project_email_status'],
                    'member_join_project_mobile_status' => $data['member_join_project_mobile_status'],
                ];

                $notification = $this->UserNotificationSetting->getNotificationByUser($token->user_id);
                if (!empty($notification)) {
                    $dataNotifications = $this->UserNotificationSetting->patchEntity($notification, $dataSave);
                    if ($this->UserNotificationSetting->save($dataNotifications)) {
                        $this->_status = 1;
                        $this->_message = __('Your notification setting has been updated.');
                        $this->_data = [];
                    } else {
                        $this->_status = 0;
                        $this->_message = __('Could not update notification setting.');
                        $this->_data = [];
                    }
                }
            } else {
                $this->_status = 0;
                $this->_message = __('Invalid Token');
                $this->_data = [];
            }
        } else {
            $this->_status = 0;
            $this->_message = __('You dont permission to access.');
            $this->_data = [];
        }
        $this->responseApi($this->_status, $this->_message, $this->_data);
        die();
    }
}
