<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Mailer\Email;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use App\Lib\CoreLib;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class ApiController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
//    public $components = array('RequestHandler','Flash');
    public $_message   = "";
    public $_data      = [];
    public $_status    = 0;
    public $_errors    = [];
    public $_user_id   = '';
    public $_api_error = array();
    public $_bContinue = true;
    public $components = array('PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }
    public $allow = array(
        'Users/login',
        'Users/register',
        'Users/loginSns',
        'Users/changePass',
        'Users/getPaymentInfo',
        'Users/showFollowing',
        'Users/showFollower',
        'Users/showBlocklist',
        'Users/changeEmail',
        'Users/updatePaymentInfo',
        'NotificationSetting/setting',
        'NotificationSetting/update',
        'ProjectsRoles/getListProjectRoles',

        // add by Nguyen Duc
        'Profile/edit',
    );
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        $this->set('_serialize', true);
    }

    public function beforeFilter(Event $event)
    {
        $this->_setLocaleLang();

        require_once(ROOT . DS . 'src' . DS  . 'Controller' . DS  . 'Api' . DS . 'ErrorDefine.php');
        $this->api_error = Configure::read('API_ERROR');

        if(isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'api') {
            $this->viewBuilder()->layout('api');
        }
        $path = $this->request->controller . '/' . $this->request->action;
//        if (!in_array($path, $this->allow)) {
//            $this->_validToken();
//        }
    }

    /**
     * set Locale Lang
     * @author Roxannie Nguyen jr
     */
    private function _setLocaleLang(){
        $Languages = CoreLib::getDataLanguage();
        $lang      = isset($this->request->data['language']) ? $this->request->data['language'] : "en";
        if(!array_key_exists($lang, $Languages)) $lang = 'en';
        if(isset($Languages[$lang]['code']) && !empty($Languages[$lang]['code'])){
            I18n::locale($Languages[$lang]['code']);
        }else{
            I18n::locale('en_US');
        }
    }

    public function generateRandomString() {
        $str = md5(rand(1, 100) . time());
        return sha1($str);
    }
    public function sendMail($template, $from, $to, $viewArrs = null, $subject = '', $layout = 'default', $format = 'html') {
        $mail = new Email($layout);
        $mail->transport('gmail')
            ->template($template, $layout)
            ->emailFormat($format)
            ->viewVars($viewArrs)
            ->from($from)
            ->to($to)
            ->subject($subject);
        if ($mail->send()) {
            return true;
        }
        return false;
    }
    // create token login
    public function createToken($user_id = 0, $platform = 0, $tk = null) {
        if ($user_id == 0) {
            return FALSE;
        }
        if (!$tk) {
            $tk = $this->generateRandomString();
        }
        $this->Token = TableRegistry::get('Tokens');
        $this->request->data['user_id'] = $user_id;
        $this->request->data['platform'] = $platform;
        $this->request->data['token'] = $tk;
        $this->request->data['created'] = date("Y-m-d H:i:s");
        $token = $this->Token->newEntity();
        $token = $this->Token->patchEntity($token, $this->request->data);
        if ($this->Token->save($token)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // clear token logout
    public function clearToken($user_id, $platform = 2) {
        $this->Token = TableRegistry::get('Tokens');
        $this->Token->deleteAll(
            [
                'Tokens.user_id' => $user_id,
                'Tokens.platform' => $platform
            ]
        );
    }

    function log_debug($message) {
        if (DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $last = $backtrace[0];
            Log::write('debug', '[' . date("d-M-Y G:i:s") . '] ' . '[' . date("e") . '] ' . basename($last['file']) . " " . $last['line'] . " " . $message . "\n", 3, LOGS . "debug.log");
        }
    }

    function log_error($message) {
        if (DEBUG_MODE) {
            $backtrace = debug_backtrace();
            $last = $backtrace[0];
            Log::write('error', '[' . date("d-M-Y G:i:s") . '] ' . '[' . date("e") . '] ' . basename($last['file']) . " " . $last['line'] . " " . $message . "\n", 3, LOGS . "error.log");
        }
    }

    private function _validToken() {
        $this->Token = TableRegistry::get('Tokens');
        if (isset($this->request->data['token'])) {
            $query = $this->Token->find('all', [
                'conditions' => ['Tokens.token' => $this->request->data['token']]
            ]);
            $data = $query->first();
            if ($data) {
                $this->_user_id = $data->id;
            } else {
                echo json_encode(array(
                        'status' => 0,
                        'message' => __("Token expired"),
                        'data' => []
                    )
                );
                die();
            }
        } else {
            echo json_encode(array(
                    'status' => 0,
                    'message' =>__('Token not empty'),
                    'data' => []
                )
            );
            die();
        }
    }

    function responseApi($status = 1, $message = '', $data = array(), $header_type = 'json') {
        header('Content-Type: application/json');
        
        if ($status == 1) {
            echo json_encode(array(
                    'status' => $status,
                    'message' => $message,
                    'data' => $data
                )
            );
        } else {
            if (!$message) {
                $message = isset($this->api_error[$status]) ? $this->api_error[$status] : '';
            }
            echo json_encode(array(
                    'status' => $status,
                    'message' => $message,
                    'data' => $data
                )
            );
        }
        die;
    }
    
    public function customSlug($string, $character = '-') {
        $string = $this->stripUnicode($string);
        $string = Inflector::slug($string, $character);
        return strtolower($string);
    }
    
    public function stripUnicode($string) {
        $unicode = array(
            'a' => 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ|Đ',
            'e' => 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'ì|í|ị|ỉ|ĩ|Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ',
            'y' => 'ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ'
        );
        foreach ($unicode as $nonUnicode => $uni) {
            $string = preg_replace("/($uni)/", $nonUnicode, $string);
        }
        return $string;
        //exit;
    }
    
    public function getRoleOpenInList($listUserJoined = [],$project_id = 0, $project_role_id = 0){
        $counter = 0;
        foreach ($listUserJoined as $row){
            if ($row['project_id'] == $project_id && $row['status'] == 1 && $row['project_role_id'] == $project_role_id){
                $counter++;
            }
        }
        return $counter;
    }
    
    
    /**
     * get Content notification send mail
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getContentNotifySendMail($actionName = NULL, $type = 0, $optionData = NULL, $userActionId = 0) {
        $sHtml = '';
        if ($actionName && $type) {
            $NameFirst = $actionName;
            $optionsArr = json_decode($optionData);

            // GET PROJECT INFO
            $ProjectName = '';
            if (isset($optionsArr->project_id) && !empty($optionsArr->project_id)) {
                $projectTable = TableRegistry::get('Projects');
                $project = $projectTable->get($optionsArr->project_id);
                if (isset($project->title))
                    $ProjectName = $project->title;
            }

            // GET ROLES
            $RoleName = '';
            if (isset($optionsArr->role_id) && !empty($optionsArr->role_id)) {
                $RolesTable = TableRegistry::get('Roles');
                $Role = $RolesTable->get($optionsArr->role_id);
                if(isset($Role->role) && !empty($Role->role)){
                    $RoleName = $Role->role;
                }
            }

            $message = '';
            switch ($type) {
                case '1':
                    $linkAcceptAction = ROOT_URL . 'projectActions/acceptJoinRole?project_role_id=' . $optionsArr->project_role_id . '&user_action_id=' . $userActionId;
                    $linkDenyAction   = ROOT_URL . 'projectActions/denyJoinRole?project_role_id=' . $optionsArr->project_role_id . '&user_action_id=' . $userActionId;
                    $message          = __('<strong>%s</strong> wants to join project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to accept.</p><p><a target="_blank" href="%s">Click here</a> to deny.</p>');
                    $sHtml            = sprintf($message, $NameFirst, $ProjectName, $linkAcceptAction, $linkDenyAction);
                    break;
                case '2':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> đã bình luận về doanh nghiệp <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '3': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> applied for your project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '4':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has joined project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '5': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> accepted to add you in project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '6': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> replied your comment on project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '7': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> just loved project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '8': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> just followed project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '9': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has uploaded a file on project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                case '10': 
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('Project owner <strong>%s</strong> has updated project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                //N has followed you
                case '12':
                    $message = __('<strong>%s</strong> has followed you');
                    $sHtml   = sprintf($message, $NameFirst);
                    break;
                //You are offered to be <role> in the project Y
                case '13':
                    $message = __('You are offered to be <strong>%s</strong> in the project <strong>%s</strong>');
                    $sHtml   = sprintf($message, $RoleName, $ProjectName);
                    break;
                //N has been offered <role> in  project Y
                case '14':
                    $link    = \Cake\Utility\Inflector::slug($project->title, '-');
                    $link    = ROOT_URL.'projects/' . $link . '-' . $project->id;
                    $message = __('<strong>%s</strong> has been offered <strong>%s</strong> in project <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view project detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $RoleName, $ProjectName, $link);
                    break;
                //N has been send chat message
                case '15':
                    $roomId = (isset($optionsArr->room_id)) ? $optionsArr->room_id : 0;
                    $link    = ROOT_URL.'chat-room/'. $project->id.'/'.$roomId;
                    $message = __('<strong>%s</strong> đã nhắn tin cho bạn trong doanh nghiệp <strong>%s</strong><br/><p><a target="_blank" href="%s">Click here</a> to view message detail</p>');
                    $sHtml   = sprintf($message, $NameFirst, $ProjectName, $link);
                    break;
                default:
                    $sHtml = __('Send notification content');
                    break;
            }
        }
        return $sHtml;
    }

    /**
     * check Token
     * @param int $iUserId - user id
     * @return json
     */
    public function checkToken(){
        $sToken  = '';
        $iUserId = 0;
        if($this->request->is('post')){

            $oTokenTable  = TableRegistry::get('Tokens');
            $aRequestData = $this->request->data;
            $sToken       = (isset($aRequestData['token']) ? $aRequestData['token'] : '');

            $aToken = $oTokenTable->find('all', array(
                'conditions' => array( 'token' => $sToken ),
            ));

            $aToken  = $aToken->first();
            $iUserId = empty($aToken) ? null : $aToken->user_id;
        }

        if (!$iUserId) {
            $this->_message   = __('Unable to authorize your request.');
            $this->_data      = array();
            $this->_bContinue = false;
            $this->_status    = 1;
        }
        
        return $iUserId;
    }


    public function getTimeAgo($datetimeInput = NULL) {
        $currentDateTime = date('Y-m-d H:i:s');
        $days = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60 * 24);
        if ($days >= 1) {
            return round($days) . __(' days ago');
        } else {
            $hours = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60);
            if ($hours >= 1) {
                return round($hours) . __(' hours ago');
            } else {
                $minutes = (strtotime($currentDateTime) - strtotime($datetimeInput)) / 60;
				if($minutes<1)
				{
					 return round($minutes) . __(' minute ago');
				}
				else
				{
					 return round($minutes) . __(' minutes ago');
				}
               
            }
        }
    }
}