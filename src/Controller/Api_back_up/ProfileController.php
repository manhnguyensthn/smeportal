<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

class ProfileController extends ApiController {

    const ProfileRolesDoUnspecified = -1;
    const ProfileRolesDoInsert = 0;
    const ProfileRolesDoUpdate = 1;
    const ProfileRolesDoDelete = 2;

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadModel('Users');
    }

    /**
     * get Profile
     * @param 
     * @return json
     */
    public function getProfile() {
        // DATA DFAULT
        $this->autoRender = false;
        $bUserSaved = false;
        $bJson = (!$this->request->is('json') ? false : true);
        $aUser = null;
        $aRequestData = $this->request->data;
        $aRequestQuery = $this->request->query;
        $iUserId = $this->checkToken();

        if ($this->_bContinue) {

            $aUser = $this->Users->get($iUserId, array(
                'contain' => array(
                    'Projects',
                    'Roles',
                ),
            ));

            if (!$aUser) {
                $this->_message = __('Unable to process your request. User not found.');
                $this->_data = array();
                $this->_bContinue = false;
            }

            $aDestricts = TableRegistry::get('Districts')->find('all', ['conditions' => ['id' => $aUser->district_id]])->first();
            $aUser['Districts'] = $aDestricts;
        }

        if ($this->_bContinue) {
            $oUsersRoles = TableRegistry::get('UsersRoles');
            $oUsersWebsites = TableRegistry::get('UsersWebsites');
            $oUsersWorks = TableRegistry::get('UsersWorks');
            $oUsersInterests = TableRegistry::get('UsersInterests');
            $aRole1 = $oUsersRoles->find('all', ['conditions' => ['user_id' => $iUserId, 'position' => 1]])->first();
            $aRole2 = $oUsersRoles->find('all', ['conditions' => ['user_id' => $iUserId, 'position' => 2]])->first();
            if (!$aRole1) {
                $aRole1 = $oUsersRoles->newEntity();
                $aRole1->role_id = 0;
                $aRole1->user_id = 0;
                $aRole1->position = 0;
            }

            if (!$aRole2) {
                $aRole2 = $oUsersRoles->newEntity();
                $aRole2->role_id = 0;
                $aRole2->user_id = 0;
                $aRole2->position = 0;
            }

            $aWebsites = $oUsersWebsites->find('all', ['fields' => ['website_id', 'Websites.name'], 'contain' => 'Websites', 'conditions' => ['user_id' => $iUserId]])->limit(100);
            $aInterests = $oUsersInterests->find('all', ['fields' => ['interest_id', 'Interests.name'], 'contain' => 'Interests', 'conditions' => ['user_id' => $iUserId]]);
            $aWorks = $oUsersWorks->find('all', ['fields' => ['id', 'work_url', 'work_video_id', 'work_thumbnail', 'work_title'], 'conditions' => ['user_id' => $iUserId]]);

            $bHasWork = false;

            $sCurrentAvatarPath = $this->_getAvatarFilePathByURL($aUser ? $aUser->avatar : '');
            $sCurrentAvatarURL = $aUser ? $aUser->avatar : '';
            // pr($aUser);
            $aCountries = $this->Users->Countries->find('list', array(
                        'fields' => array('id', 'country_name'),
                        'keyField' => 'id',
                        'valueField' => 'country_name',
                    ))->toArray();

            $aStates = $this->Users->States->find('list', array(
                        'fields' => array('id', 'name'),
                        'conditions' => array('country_id' => (isset($aUser) && !empty($aUser)) ? $aUser->country_id : 0),
                        'keyField' => 'id',
                        'valueField' => 'name',
                    ))->toArray();

            $aDistricts = $this->Users->Districts->find('list', array(
                        'fields' => array('id', 'name'),
                        'conditions' => array('state_id' => ($aUser ? $aUser->state_id : 1)),
                        'keyField' => 'id',
                        'valueField' => 'name',
                    ))->toArray();

            $aRoles = $this->Users->Roles->find('list', array(
                'fields' => array('id', 'role'),
                'keyField' => 'id',
                'valueField' => 'role',
            ));

            $sWebRootURL = rtrim(Router::url($this->request->webroot, true), '/');
            $sAvatarURL = join(DIRECTORY_SEPARATOR, [$sWebRootURL, 'img', 'avatar_default.jpg']);
            $iBioRemainCharCounter = 500;
            if (!empty($aUser->avatar) && strlen($aUser->avatar) > 0 && strpos($aUser->avatar, $sWebRootURL) !== false) {
                $sWebRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
                $sFullPath = join(DIRECTORY_SEPARATOR, [$sWebRoot, 'img', 'avatars', basename($aUser->avatar)]);
                $oAvatarFile = new File($sFullPath);

                if ($oAvatarFile->exists()) {
                    $sAvatarURL = $aUser->avatar;
                }
            }

            $iBioRemainCharCounter = 500 - mb_strlen($aUser->biography, 'UTF-8');
            if ($iBioRemainCharCounter < 0) {
                $iBioRemainCharCounter = 0;
            }

            $this->_data['Roles']['role0'] = $aRole1->role_id; // must be post and display as the selected first role, or empty if not select
            $this->_data['Roles']['role1'] = $aRole2->role_id; // must be post and display as the selected second role, or empty if not select
            // others user information
            $this->_data['Users']['id'] = (int) $aUser->id;
            $this->_data['Users']['first_name'] = ($aUser->first_name ? $aUser->first_name : '');
            $this->_data['Users']['last_name'] = ($aUser->last_name ? $aUser->last_name : '');
            $this->_data['Users']['name'] = ($aUser->name ? $aUser->name : '');
            $this->_data['Users']['biography'] = ($aUser->biography ? $aUser->biography : '');
            $this->_data['Users']['postal_code'] = (int) (isset($aUser->Districts->postal_code) ? $aUser->Districts->postal_code : 0);
            $this->_data['Users']['country_id'] = (int) $aUser->country_id;
            $this->_data['Users']['state_id'] = (int) $aUser->state_id;
            $this->_data['Users']['district_id'] = (int) $aUser->district_id;
            $this->_data['Users']['avatar'] = ['' => '']; // must be post and fill with all generated elements, e.g 'tmp_name', 'name', 'type' etc
            // pr($aUser); 
            // NOTE: for display only, DONT POST
            $this->_data['websites'] = ($aWebsites ? $aWebsites : ['' => '']); // List of websites of users, id = $website->id, name = $website->website->name
            $this->_data['interests'] = ($aInterests ? $aInterests : ['' => '']); // List of interests of users
            $this->_data['roles'] = ($aRoles ? $aRoles : ['' => '']); // List of possible roles, key => value
            $this->_data['countries'] = ($aCountries ? $aCountries : ['' => '']); // List of possible countries
            $this->_data['states'] = ($aStates ? $aStates : ['' => '']); // List of possible aStates
            $this->_data['districts'] = ($aDistricts ? $aDistricts : ['' => '']); // List of possible cities (depending on $user->country_id)
            $this->_data['avatarURL'] = $sAvatarURL; // User's current avatar

            $this->_data['works'] = ($aWorks ? $aWorks : ['' => '']); // List of possible works
            $this->_bContinue = true;
        }

        $this->_status = (!$this->_bContinue ? 0 : 1);
        $this->responseApi($this->_status, $this->_message, $this->_data);
    }

    // Coder: Giang Dien
    // Date: 2017-02-10
    // Function: update user profile

    public function updateUserProfile() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            if (!isset($data['token'])) {
                $this->responseApi(1032);
            } else {
                $tokenTable = TableRegistry::get('Tokens');
                $token = $tokenTable->find('all', [
                    'conditions' => ['token' => $data['token']],
                ]);
                $token = $token->first();
                if (!empty($token)) {
                    $userTable = TableRegistry::get('Users');
                    $userData = $userTable->get($token->user_id);
                    $userDataUpdate = [];
                    // Update base info
                    if (isset($data['district_id'])) {
                        $userDataUpdate['district_id'] = $data['district_id'];
                    }
                    if (isset($data['state_id'])) {
                        $userDataUpdate['state_id'] = $data['state_id'];
                    }
                    if (isset($data['country_id'])) {
                        $userDataUpdate['country_id'] = $data['country_id'];
                    }
                    if (isset($data['first_name'])) {
                        $userDataUpdate['first_name'] = $data['first_name'];
                    }
                    if (isset($data['last_name'])) {
                        $userDataUpdate['last_name'] = $data['last_name'];
                    }
                    if (isset($data['first_name']) && isset($data['last_name'])) {
                        $userDataUpdate['name'] = $data['first_name'] . ' ' . $data['last_name'];
                    }
                    if (isset($data['biography'])) {
                        $userDataUpdate['biography'] = $data['biography'];
                    }
                    // Update role
                    $userRoleTable = TableRegistry::get('UsersRoles');
                    if (isset($data['role1']) && !empty($data['role1'])) {
                        $dataUserRole1 = $userRoleTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'position' => 1]])->first();
                        if (!empty($dataUserRole1)) {
                            // Update role
                            $query = $userRoleTable->query();
                            $query->update()
                                    ->set(['role_id' => $data['role1']])
                                    ->where(['user_id' => $token->user_id, 'position' => 1])
                                    ->execute();
                        } else {
                            // Insert role
                            $dataUserRole = $userRoleTable->newEntity();
                            $dataUserRole->user_id = $token->user_id;
                            $dataUserRole->role_id = $data['role1'];
                            $dataUserRole->position = 1;
                            $userRoleTable->save($dataUserRole);
                        }
                    }
                    if (isset($data['role2']) && !empty($data['role2'])) {
                        $dataUserRole2 = $userRoleTable->find('all', ['conditions' => ['user_id' => $token->user_id, 'position' => 2]])->first();
                        if (!empty($dataUserRole2)) {
                            // Udpate role
                            $query = $userRoleTable->query();
                            $query->update()
                                    ->set(['role_id' => $data['role2']])
                                    ->where(['user_id' => $token->user_id, 'position' => 2])
                                    ->execute();
                        } else {
                            // Insert role
                            $dataUserRole = $userRoleTable->newEntity();
                            $dataUserRole->user_id = $token->user_id;
                            $dataUserRole->role_id = $data['role2'];
                            $dataUserRole->position = 2;
                            $userRoleTable->save($dataUserRole);
                        }
                    }
                    // Update avatar
                    if (isset($data['avatar']) && !empty($data['avatar'])) {
                        $avatarArr = $data['avatar'];
                        if (!empty($avatarArr) && isset($avatarArr['type']) && isset($avatarArr['name']) && isset($avatarArr['tmp_name'])) {
                            $avatarFileType = $this->_getFileTypeByMimeType($avatarArr['type']);
                            $listAllowFileTypes = $this->_getAllowedAvatarUploadedFileTypes();
                            if (!in_array($avatarFileType, $listAllowFileTypes)) {
                                // File type not allow
                                $this->responseApi(0, __('Profile image should be: jpeg, png, gif, bmp with minimum @1024x576.'), []);
                            } else {
                                list($iWidth, $iHeight) = getimagesize($avatarArr['tmp_name']);
                                if (empty($iWidth) || empty($iWidth)) {
                                    // File size not allow
                                    $this->responseApi(0, __('Profile image should be: jpeg, png, gif, bmp with minimum @1024x576.'), []);
                                } else {
                                    $uniqueAvatarPath = $this->_getUniqueAvatarFilePath($avatarArr['name'], $avatarFileType, $token->user_id);
                                    $userDataUpdate['avatar'] = $this->_getAvatarURLByFilePath($uniqueAvatarPath);
                                    $this->_saveNewAvatar($avatarArr['tmp_name'], $uniqueAvatarPath);
                                }
                            }
                        }
                    }
                    // update website list
                    if (isset($data['website']) && !empty($data['website'])) {
                        $websiteArr = json_decode($data['website']);
                        if (isset($websiteArr->addWebsiteList) && isset($websiteArr->deleteWebsiteList)) {
                            // Add list website
                            $websiteTable = TableRegistry::get('Websites');
                            $userWebsiteTable = TableRegistry::get('UsersWebsites');
                            foreach ($websiteArr->addWebsiteList as $item) {
                                $websiteData = $websiteTable->newEntity();
                                $websiteData->name = $item;
                                $websiteData->created = date('Y-m-d H:i:s');
                                $websiteTable->save($websiteData);
                                $userWebsiteData = $userWebsiteTable->newEntity();
                                $userWebsiteData->user_id = $token->user_id;
                                $userWebsiteData->website_id = $websiteData->id;
                                $userWebsiteTable->save($userWebsiteData);
                            }
                            // Delete list website
                            if (!empty($websiteArr->deleteWebsiteList)) {
                                $websiteTable->deleteAll(['id IN' => $websiteArr->deleteWebsiteList]);
                                $userWebsiteTable->deleteAll(['website_id IN' => $websiteArr->deleteWebsiteList]);
                            }
                        }
                    }
                    // update newwork
                    if (isset($data['newwork']) && !empty($data['newwork'])) {
                        $newworkArr = json_decode($data['newwork']);
                        $userWorkTable = TableRegistry::get('UsersWorks');
                        foreach ($newworkArr as $item) {
                            if (!empty($item) && isset($item->work_url)) {
                                if (preg_match("/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/", $item->work_url, $videoIdArr)) {
                                    $userWorkData = $userWorkTable->newEntity();
                                    $userWorkData->work_video_id = (isset($videoIdArr[4])) ? $videoIdArr[4] : '';
                                    $userWorkData->user_id = $token->user_id;
                                    $userWorkData->work_title = $item->work_title;
                                    $userWorkData->work_description = $item->work_description;
                                    $userWorkData->work_url = $item->work_url;
                                    $userWorkTable->save($userWorkData);
                                }
                            }
                        }
                    }
                    $userData = $userTable->patchEntity($userData, $userDataUpdate);
                    if ($userTable->save($userData)) {
                        $this->responseApi(1, __('Hồ sơ lưu thành công.'), []);
                    } else {
                        $this->responseApi(0, __('Hồ sơ lưu không thành công, vui lòng thử lại'), []);
                    }
                } else {
                    $this->responseApi(1031);
                }
            }
        }
    }

    /**
     * update Profile
     * @param 
     * @return json
     */
    public function updateProfile() {
        $this->normalizedJsonRequestData();
        $iUserId = $this->checkToken();
        $bHasWork = false;
        $aUser = array();
        $aRequestUser = null;
        $aRequestData = $this->request->data;

        if ($this->_bContinue) {
            $aUser = $this->Users->get($iUserId, array(
                'contain' => array(
                    'Projects',
                    'Roles',
                ),
            ));

            if (!$aUser) {
                $this->_message = __('Unable to process your request. User not found.');
                $this->_bContinue = false;
            }
        }

        if ($this->_bContinue && $this->request->is(['patch', 'post', 'put'])) {
            $sCurrentAvatarPath = $this->_getAvatarFilePathByURL((isset($aUser->avatar) ? $aUser->avatar : ''));
            $sCurrentAvatarURL = $aUser ? $aUser->avatar : '';

            if (isset($aRequestData['Users'])) {
                $aRequestUser = $aRequestData['Users'];
            }

            if ($this->_bContinue && $aRequestUser) {
                $sFirstName = isset($aRequestUser['first_name']) && !empty($aRequestUser['first_name']) ? trim($aRequestUser['first_name']) : '';
                $sLastName = isset($aRequestUser['last_name']) && !empty($aRequestUser['last_name']) ? trim($aRequestUser['last_name']) : '';
                $sBiography = isset($aRequestUser['biography']) && !empty($aRequestUser['biography']) ? trim($aRequestUser['biography']) : '';
                $sAboutMe = isset($aRequestUser['about_me']) && !empty($aRequestUser['about_me']) ? trim($aRequestUser['about_me']) : 0;
                $sCountries = isset($aRequestData['Countries']) && !empty($aRequestData['Countries']) ? trim($aRequestData['Countries']) : 0;
                $sDistricts = isset($aRequestData['Districts']) && !empty($aRequestData['Districts']) ? trim($aRequestData['Districts']) : 0;
                $sStates = isset($aRequestData['States']) && !empty($aRequestData['States']) ? trim($aRequestData['States']) : 0;

                $this->request->data['Users']['first_name'] = $sFirstName;
                $this->request->data['Users']['last_name'] = $sLastName;
                $this->request->data['Users']['name'] = $sFirstName . ' ' . $sLastName;
                $this->request->data['Users']['biography'] = $sBiography;
                $this->request->data['Users']['about_me'] = $sAboutMe;
                $this->request->data['Users']['country_id'] = $sCountries;
                $this->request->data['Users']['district_id'] = $sDistricts;
                $this->request->data['Users']['state_id'] = $sStates;
                // VALIDATE avatar
                $aAvatar = (isset($aRequestData['Users']['avatar']) ? $aRequestData['Users']['avatar'] : null);
                $sFileType = '';
                if ($this->_bContinue && !empty($aAvatar) && !empty($aAvatar['tmp_name']) && !empty($aAvatar['name']) && $this->_status) {
                    $sFileType = $this->_getFileTypeByMimeType($aAvatar['type']);
                    $aAllowFileTypes = $this->_getAllowedAvatarUploadedFileTypes();
                    $bAvatarSuccess = true;

                    if (!in_array($sFileType, $aAllowFileTypes))
                        $bAvatarSuccess = false;

                    if ($bAvatarSuccess) {
                        list($iWidth, $iHeight) = getimagesize($aAvatar['tmp_name']);
                        $aMinimumDimension = $this->_getMinimumAvatarDimension();

                        if (empty($iWidth) || empty($iWidth)) {
                            $bAvatarSuccess = false;
                        } else {
                            $bAvatarSuccess = ($iWidth >= $aMinimumDimension['width']) && ($iHeight >= $aMinimumDimension['height']);
                        }
                    }

                    if (!$bAvatarSuccess) {
                        $this->_message = __('Profile image should be: jpeg, png, gif, bmp with minimum @1024x576.');
                        $this->_bContinue = false;
                    }
                }
                // END VALIDATE avatar
                // VALIDATE website
                if ($this->_bContinue) {
                    $aOtherWebsites = array();
                    if (isset($aRequestData['UsersWebsite']))
                        $aOtherWebsites = $aRequestData['UsersWebsite'];
                    if (!empty($aOtherWebsites)) {
                        foreach ($aOtherWebsites as $key => $value) {
                            $sTrimmedValue = trim($value);
                            if ($sTrimmedValue) {
                                if (!$this->_isUrlValid($sTrimmedValue)) {
                                    $this->_message = __('Định dạng website sai. Ví dụ: http://www.sme.com, https://www.amazon.co.jp');
                                    $this->_bContinue = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                // END VALIDATE website
                // Validate UsersWork
                $aUsersWorks = null;
                if (isset($aRequestData['UsersWork']))
                    $aUsersWorks = $aRequestData['UsersWork'];
                if ($this->_bContinue && $aUsersWorks) {
                    $count = 0;
                    foreach ($aUsersWorks as $key => $aUsersWork) {
                        if (isset($aUsersWork['work_url']) && !empty($aUsersWork['work_url']) &&
                                isset($aUsersWork['title']) && !empty($aUsersWork['title'])
                        ) {
                            $count++;
                            if (!$this->_bContinue)
                                break;

                            if (!preg_match("/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/", $aUsersWork['work_url'], $aCodeVideo)) {
                                $this->_message = __('Not a youtube URL');
                                $this->_bContinue = false;
                            } else {
                                $this->request->data['UsersWork'][$key]['work_video_id'] = (isset($aCodeVideo[4]) ? $aCodeVideo[4] : null);
                            }

                            $sTitle = trim($aUsersWork['title']);
                            $sDescription = trim($aUsersWork['description']);

                            if (!$sTitle || mb_strlen($sTitle) > 25) {
                                $this->_message = __('Title is required. Max length is 25 characters.');
                                $this->_bContinue = false;
                            }

                            $aUsersWorks[$key]['title'] = $sTitle;
                            $aUsersWorks[$key]['description'] = $sDescription;
                        }
                    }
                    // Check link video work

                    if ($count > 0 && $this->_bContinue) {
                        $bHasWork = true;
                    }
                }
                // END: Validate UsersWork
                // BEGIN UPDATE FIRST ROLE
                $oUsersRoles = TableRegistry::get('UsersRoles');
                $aRole1 = $oUsersRoles->find('all', ['conditions' => ['user_id' => $iUserId, 'position' => 1]])->first();
                $aRole2 = $oUsersRoles->find('all', ['conditions' => ['user_id' => $iUserId, 'position' => 2]])->first();

                if (!$aRole1) {
                    $aRole1 = $oUsersRoles->newEntity();
                    $aRole1->role_id = 0;
                    $aRole1->user_id = 0;
                    $aRole1->position = 0;
                }

                if (!$aRole2) {
                    $aRole2 = $oUsersRoles->newEntity();
                    $aRole2->role_id = 0;
                    $aRole2->user_id = 0;
                    $aRole2->position = 0;
                }

                $aUser = $this->Users->patchEntity($aUser, $this->request->data);
                $iRole1Id = (isset($aRequestData['Roles']['role0']) ? $aRequestData['Roles']['role0'] : 0);
                $iRole2Id = (isset($aRequestData['Roles']['role1']) ? $aRequestData['Roles']['role1'] : 0);

                if ($this->_bContinue) {
                    if ($iRole1Id) {
                        if ($aRole1->isNew()) {
                            $aRole1->role_id = intval($iRole1Id);
                            $aRole1->user_id = $iUserId;
                            $aRole1->position = 1;
                            if (!$oUsersRoles->save($aRole1)) {
                                $this->_bContinue = false;
                            }
                        } else {
                            $aUpdate = array('role_id' => intval($iRole1Id));
                            $aCound = array('user_id' => $iUserId, 'position' => 1);
                            $oUsersRoles->updateAll($aUpdate, $aCound);
                        }
                    } else {
                        if (!$aRole1->isNew()) {
                            if (!$oUsersRoles->delete($aRole1)) {
                                $this->_bContinue = false;
                            };
                        }
                    }
                }

                if ($this->_bContinue) {
                    if ($iRole2Id) {
                        if ($aRole2->isNew()) {
                            $aRole2->role_id = intval($iRole2Id);
                            $aRole2->user_id = $iUserId;
                            $aRole2->position = 2;
                            if (!$oUsersRoles->save($aRole2)) {
                                $this->_bContinue = false;
                            }
                        } else {
                            $aUpdate = array('role_id' => intval($iRole2Id));
                            $aCound = array('user_id' => $iUserId, 'position' => 2);
                            $oUsersRoles->updateAll($aUpdate, $aCound);
                        }
                    } else {
                        if (!$aRole2->isNew()) {
                            if (!$oUsersRoles->delete($aRole2)) {
                                $this->_bContinue = false;
                            };
                        }
                    }
                }
                // END UPDATE FIRST ROLE
                // UPDATE WEB SITE
                if ($this->_bContinue) {
                    if (!$this->_updateWebsite($iUserId)) {
                        $this->_bContinue = false;
                    }
                }
                // END WEB SITE
                // UPDATE INTEREST
                if ($this->_bContinue) {
                    if (!$this->_updateInterest($iUserId)) {
                        $this->_bContinue = false;
                    }
                }
                // END INTEREST
                // BEGIN UPDATE AND DELETE WORK
                if ($this->_bContinue && $bHasWork) {
                    if (!$this->_saveWork($iUserId)) {
                        $this->_bContinue = false;
                    }
                }


                if (isset($this->request->data['deletedUsersWorks']) && !empty($this->request->data['deletedUsersWorks'])) {
                    $this->_deleteWork($iUserId);
                }
                // END UPDATE AND DELETE WORK
                // BEGIN UPDATE USER
                if ($this->_bContinue) {
                    $sUniqueAvatarPath = '';
                    if (!empty($aAvatar) && !empty($aAvatar['tmp_name']) && !empty($aAvatar['name'])) {
                        $sFileType = $this->_getFileTypeByMimeType($aAvatar['type']);
                        $sUniqueAvatarPath = $this->_getUniqueAvatarFilePath($aAvatar['name'], $sFileType, $iUserId);
                        $aUser->avatar = $this->_getAvatarURLByFilePath($sUniqueAvatarPath);
                    } else {
                        $aUser->avatar = $sCurrentAvatarURL;
                    }

                    if (!$this->Users->save($aUser)) {
                        $this->_bContinue = false;
                    } else {
                        if (!empty($aAvatar) && !empty($aAvatar['tmp_name']) && !empty($aAvatar['name'])) {
                            $this->_saveNewAvatar($aAvatar['tmp_name'], $sUniqueAvatarPath);
                        }
                        $this->_message = __('The profile has been saved.');
                    }
                }
                // END UPDATE USER
            } else {
                $this->_message = __('No data input');
                $this->_bContinue = false;
            }
        } else {
            $this->_message = __('Unable to authorize your request.');
        }

        $this->_status = (!$this->_bContinue ? 0 : 1);
        $this->_message = (!$this->_bContinue && !$this->_message ? __('The profile could not be saved. Please, try again.') : $this->_message);
        $this->responseApi($this->_status, $this->_message);
        die();
    }

    /**
     * Check link website
     * @param string $url - Link url
     * @return boolean
     */
    private function _isUrlValid($url) {
        if (!$url)
            return false;
        return preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $url);
    }

    /**
     * get Minimum Avatar Dimension
     * @return array
     */
    private function _getMinimumAvatarDimension() {
        return ['width' => 1024, 'height' => 576];
    }

    /**
     * get Allowed Avatar Uploaded File Types
     * @return array
     */
    private function _getAllowedAvatarUploadedFileTypes() {
        return ['jpeg', 'png', 'gif', 'bmp', 'jpg'];
    }

    /**
     * GET file TYPE
     * @param string $mime
     * @return string
     */
    private function _getFileTypeByMimeType($mime) {
        $retval = '';
        $mimes = [
            // images
            'image/bmp' => 'bmp',
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/pjpeg' => 'jpeg',
            'image/png' => 'png',
            // videos
            'video/quicktime' => 'mov',
            'video/mpeg' => 'mpeg',
            'application/x-troff-msvideo' => 'avi',
            'video/avi' => 'avi',
            'video/msvideo' => 'avi',
            'video/x-msvideo' => 'avi',
            'video/mp4' => 'mp4',
            'application/mp4' => 'mp4',
            'video/3gpp' => '3gp',
            'video/x-ms-wmv' => 'wmv',
            'video/x-flv' => 'flv'
        ];

        foreach ($mimes as $key => $value) {
            if ($key == trim($mime)) {
                $retval = $value;
                break;
            }
        }
        return $retval;
    }

    private function _getAvatarFilePathByURL($url) {
        $retval = '';
        if (!empty($url) && strlen($url) > 0 &&
                strpos($url, rtrim(Router::url($this->request->webroot, true), '/')) !== false) {
            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
            $fileName = basename($url);
            $retval = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', $fileName]);
        }

        return $retval;
    }

    /**
     * update Website
     * @param int $user_id - user id
     * @return json
     */
    private function _updateWebsite($user_id) {
        $retval = true;
        $data = $this->request->data;

        $websitesTables = TableRegistry::get('Websites');
        $usersWebsitesTables = TableRegistry::get('UsersWebsites');


        $deletedWebsites = (isset($data['deletedWebsites']) ? explode(',', $data['deletedWebsites']) : null);
        $deletedWebsitesIds = [];


        $websiteEntity = null;
        $usersWebsiteEntity = null;

        // Websites to be deleted
        if (!empty($deletedWebsites)) {
            foreach ($deletedWebsites as $deletedWebsite) {
                $deletedWebsite = trim($deletedWebsite);
                if (!empty($deletedWebsite) && preg_match("/^[1-9][0-9]*$/D", $deletedWebsite)) {
                    array_push($deletedWebsitesIds, intval($deletedWebsite));
                }
            }
        }

        if (!empty($deletedWebsitesIds)) {
            $usersWebsitesTables->deleteAll([
                'UsersWebsite.website_id IN' => $deletedWebsitesIds,
                'UsersWebsite.user_id' => $user_id
            ]);

            $websitesTables->deleteAll([
                'Website.id IN' => $deletedWebsitesIds
            ]);
        }

        // Websites to be added
        $otherWebsites = [];
        if (isset($data['UsersWebsite'])) {
            $otherWebsites = $data['UsersWebsite'];
        }

        if (!empty($otherWebsites) && $retval) {
            foreach ($otherWebsites as $key => $value) {
                $trimmedValue = trim($value);
                if (!empty($trimmedValue) && !is_numeric($trimmedValue)) {
                    $websiteEntity = $websitesTables->newEntity();
                    $websiteEntity->name = $trimmedValue;
                    if (!$websitesTables->save($websiteEntity)) {
                        $retval = false;
                        break;
                    };

                    $usersWebsiteEntity = $usersWebsitesTables->newEntity();
                    $usersWebsiteEntity->user_id = $user_id;
                    $usersWebsiteEntity->website_id = $websiteEntity->id;

                    if (!$usersWebsitesTables->save($usersWebsiteEntity)) {
                        $retval = false;
                        break;
                    };
                }
            }
        }

        return $retval;
    }

    /**
     * update Interest
     * @param int $user_id - user id
     * @return json
     */
    private function _updateInterest($user_id) {
        $retval = true;
        $data = $this->request->data;

        $deletedInterests = (isset($data['deletedInterests']) ? explode(',', $data['deletedInterests']) : '');
        $deletedInterestsIds = [];

        $interestsTables = TableRegistry::get('Interests');
        $usersInterestsTables = TableRegistry::get('UsersInterests');
        $interestEntity = null;
        $usersInterestEntity = null;

        // Interests to be deleted
        if (!empty($deletedInterests)) {
            foreach ($deletedInterests as $deletedInterest) {
                $trimmedDeletedInterest = trim($deletedInterest);
                if (!empty($trimmedDeletedInterest) && preg_match("/^[1-9][0-9]*$/D", $trimmedDeletedInterest)) {
                    array_push($deletedInterestsIds, intval($trimmedDeletedInterest));
                }
            }
        }

        if (!empty($deletedInterestsIds)) {
            $usersInterestsTables->deleteAll([
                'interest_id IN' => $deletedInterestsIds,
                'user_id' => $user_id
            ]);

            $interestsTables->deleteAll([
                'id IN' => $deletedInterestsIds
            ]);
        }

        // Interests to be added
        $otherInterests = [];
        if (isset($data['UsersInterest'])) {
            $otherInterests = $data['UsersInterest'];
        }

        if (!empty($otherInterests) && $retval) {
            foreach ($otherInterests as $key => $value) {
                $trimmedValue = trim($value);
                if (!empty($trimmedValue) && !is_numeric($trimmedValue)) {
                    $interestEntity = $interestsTables->newEntity();
                    $interestEntity->name = $trimmedValue;
                    if (!$interestsTables->save($interestEntity)) {
                        $retval = false;
                        break;
                    };

                    $usersInterestEntity = $usersInterestsTables->newEntity();
                    $usersInterestEntity->user_id = $user_id;
                    $usersInterestEntity->interest_id = $interestEntity->id;

                    if (!$usersInterestsTables->save($usersInterestEntity)) {
                        $retval = false;
                        break;
                    };
                }
            }
        }

        return $retval;
    }

    /**
     * delete Work
     * @param int $user_id - user id
     * @return json
     */
    private function _deleteWork($iUserId) {
        $aDeletedUsersWorks = (isset($this->request->data['deletedUsersWorks']) ? explode(',', $this->request->data['deletedUsersWorks']) : '');
        if ($iUserId && $aDeletedUsersWorks) {
            $oUsersWorksTable = TableRegistry::get("UsersWorks");
            $aDeletedWorks = array();
            foreach ($aDeletedUsersWorks as $iDeletedUsersWork) {
                if (!empty($iDeletedUsersWork) && preg_match("/^[1-9][0-9]*$/D", $iDeletedUsersWork)) {
                    array_push($aDeletedWorks, intval($iDeletedUsersWork));
                }
            }

            if ($aDeletedWorks) {
                $oUsersWorksTable->deleteAll([
                    'id IN' => $aDeletedWorks,
                    'user_id' => $iUserId
                ]);

                return true;
            }
        }
        return false;
    }

    /**
     * save Work
     * @param int $user_id - user id
     * @return json
     */
    private function _saveWork($user_id) {
        $oUsersWorksTable = TableRegistry::get("UsersWorks");
        $retval = false;
        $aUsersWorks = isset($this->request->data['UsersWork']) && !empty($this->request->data['UsersWork']) ? $this->request->data['UsersWork'] : array();
        if ($aUsersWorks) {
            foreach ($aUsersWorks as $key => $aUsersWork) {
                if (isset($aUsersWork['title']) && $aUsersWork['title'] && $aUsersWork['work_url']) {
                    $usersWork = $oUsersWorksTable->newEntity();
                    $usersWork->user_id = $user_id;
                    $usersWork->work_title = $aUsersWork['title'];
                    $usersWork->work_description = empty($aUsersWork['description']) ? '' : $aUsersWork['description'];
                    $usersWork->work_url = $aUsersWork['work_url'];
                    $usersWork->work_video_id = (isset($aUsersWork['work_video_id']) ? $aUsersWork['work_video_id'] : null);
                    $usersWork->work_thumbnail = '';
                    if ($oUsersWorksTable->save($usersWork)) {
                        $retval = true;
                    }
                }
            }
        }
        return $retval;
    }

    /**
     * get Unique Avatar File Path
     * @param string $fileName
     * @param string $fileType
     * @param int $user_id
     * @return string
     */
    private function _getUniqueAvatarFilePath($fileName, $fileType, $user_id) {
        $now = new \DateTime();
        $sanitizedFileName = md5(basename($fileName)) . '.' . $fileType;
        $uniqueFileName = join('_', [$user_id, $now->format('YmdHis'), $sanitizedFileName]);
        $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
        $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', $uniqueFileName]);

        return $fullPath;
    }

    /**
     * get Avatar URL File Path
     * @param string $path
     * @return string
     */
    private function _getAvatarURLByFilePath($path) {
        $retval = '';
        if (!empty($path)) {
            $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
            $fileName = basename($path);
            $retval = join('/', [$webRootURL, 'img', 'avatars', $fileName]);
        }
        return $retval;
    }

    /**
     * save New Avata
     * @param string $tmpFilePath
     * @param string $fullPath
     */
    private function _saveNewAvatar($tmpFilePath, $fullPath) {
        if (!empty($tmpFilePath) && !empty($fullPath)) {
            $uploadedFile = new File($tmpFilePath);

            if ($uploadedFile->exists()) {
                move_uploaded_file($tmpFilePath, $fullPath);
            }
        }
    }

    /**
     * Get getDistricts - POST
     * @param state_id int
     * @return json
     */
    public function getDistricts() {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $aRequestData = $this->request->data;
            if (isset($aRequestData['token']) && !empty($aRequestData['token']) && isset($aRequestData['state_id'])) {
                $state_id = (isset($aRequestData['state_id']) ? $aRequestData['state_id'] : 0);
                $aDistricts = $this->Users->Districts->find('all', array(
                            'fields' => array('id', 'name', 'state_id', 'postal_code'),
                            'conditions' => array('state_id' => $state_id),
                        ))->toArray();

                $this->_data = $aDistricts;
                $this->_status = 1;
            } else {
                $this->responseApi(1032);
            }
        } else {
            $this->_message = __('No data input');
        }
        $this->responseApi($this->_status, $this->_message, $this->_data);
    }

    /**
     * Get getStates - POST
     * @param country_id int
     * @return json
     */
    public function getStates() {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $aRequestData = $this->request->data;
            if (isset($aRequestData['token']) && !empty($aRequestData['token']) && isset($aRequestData['country_id'])) {
                $country_id = (isset($aRequestData['country_id']) ? $aRequestData['country_id'] : 0);
                $aStates = $this->Users->States->find('all', array(
                            'fields' => array('id', 'name'),
                            'conditions' => array('country_id' => $country_id),
                        ))->toArray();

                $this->_data = $aStates;
                $this->_status = 1;
            } else {
                $this->responseApi(1032);
            }
        } else {
            $this->_message = __('No data input');
        }
        $this->responseApi($this->_status, $this->_message, $this->_data);
    }

    private function normalizedJsonRequestData() {
        if ($this->request->isJson() && $this->request->isPost()) {
            // $this->log(var_export($this->request->data, true));
            // USER INFO BEGIN
            if (isset($this->request->data['Users']) && !is_array($this->request->data['Users'])) {
                $usersInfo = json_decode($this->request->data['Users'], true);
                $this->request->data['Users'] = $usersInfo;
            }

            if (isset($this->request->data['avatar']) && isset($this->request->data['Users']) && !empty($this->request->data['Users'])) {
                $this->request->data['Users']['avatar'] = $this->request->data['avatar'];
                unset($this->request->data['avatar']);
            }
            // USER INFO END
            // ROLES BEGIN
            if (isset($this->request->data['Roles']) && !is_array($this->request->data['Roles'])) {
                $rolesInfo = json_decode($this->request->data['Roles'], true);
                $this->request->data['Roles'] = $rolesInfo;
            }
            // ROLES END
            // USER WORK BEGIN
            if (isset($this->request->data['UsersWork']) && !is_array($this->request->data['UsersWork'])) {
                $usersWork = json_decode($this->request->data['UsersWork'], true);
                $this->request->data['UsersWork'] = $usersWork;
            }
            // USER WORK END
            // WEBSITES BEGIN
            if (isset($this->request->data['UsersWebsite']) && !is_array($this->request->data['UsersWebsite'])) {
                $UsersWebsite = json_decode($this->request->data['UsersWebsite'], true);
                $this->request->data['UsersWebsite'] = $UsersWebsite;
            }
            // WEBSITES END
        }
    }

}
