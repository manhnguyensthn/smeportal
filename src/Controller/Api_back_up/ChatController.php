<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

class ChatController extends ApiController {

    public $components = array('RequestHandler', 'PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    // Coder: Giang Dien
    // Date: 04-01-2017
    // Function: get list rooms for chat form
    public function getListRoom() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            if (!isset($data['token']) && !isset($data['user_id'])) {
                $this->responseApi(1032);
            } else {
                if (isset($data['token'])) {
                    $tokenTable = TableRegistry::get('Tokens');
                    $token = $tokenTable->find('all', [
                        'conditions' => ['token' => $data['token']],
                    ]);
                    $token = $token->first();
                    if (!empty($token)) {
                        $user_id = $token['user_id'];
                    } else {
                        $this->responseApi(1032);
                    }
                } else {
                    $user_id = $data['user_id'];
                }
            }
            if (!empty($user_id)) {
                if (isset($data['project_id']) && !empty($data['project_id'])) {
                    // Get tab member in the project
                    $this->loadModel('ChatRoomMembers');
                    $listRoomMembers = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.project_id' => $data['project_id'], 'ChatRooms.type' => 2, 'ChatRooms.status' => 1], [], 0, 0, ['ChatRooms', 'Users']);
                    $listRooms = $this->_getRoomsByList($listRoomMembers, $token['user_id']);
                    $dataRespone['group'] = [];
                    foreach ($listRooms as $row) {
                        $dataRoom = ['room_id' => $row['id'], 'room_type' => $row['type']];
                        $roomMembers = $this->_getMembersByRoomId($listRoomMembers, $row['id']);
                        // Chat group
                        $dataRoom['room_name'] = '';
                        $dataRoom['room_status'] = '';
                        $data['users'] = [];
                        foreach ($roomMembers as $item) {
                            $dataRoom['room_name'] .= $item['user']['first_name'] . ' ' . $item['user']['last_name'] . ',';
                            $dataRoom['users'][] = [
                                'member_id' => $item['member_id'],
                                'avatar' => !empty($item['user']['avatar']) ? $item['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                'member_name' => $item['user']['first_name'] . ' ' . $item['user']['last_name']
                            ];
                        }
                        $dataRespone['group'][] = $dataRoom;
                    }
                    $this->responseApi(1, 'Success', $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 04-01-2017
    // Function: Get list rooms from list room members
    private function _getRoomsByList($listRoomMembers = [], $memberId = 0) {
        $listRoomIds = [];
        $listRooms = [];
        foreach ($listRoomMembers as $row) {
            if (!in_array($row->room_id, $listRoomIds) && $row->member_id == $memberId) {
                $listRooms[] = $row['chat_room'];
                $listRoomIds[] = $row['room_id'];
            }
        }
        return $listRooms;
    }

    // Coder: Giang Dien
    // Date: 04-01-2017
    // Function: Get list members by room id
    private function _getMembersByRoomId($listRoomMembers = [], $room_id = 0) {
        $listMembers = [];
        foreach ($listRoomMembers as $row) {
            if ($row['room_id'] == $room_id) {
                $listMembers[] = $row;
            }
        }
        return $listMembers;
    }

    public function updateRoomStatus() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['room_status'])) {
                    // Get tab member in the project
                    $dataRespone = ['room_id' => 1, 'room_status' => $data['room_status']];
                    $this->responseApi(1, 'Status updated successfully!', $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    public function getContactChatList() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['project_id']) && !empty($data['project_id'])) {
                    $this->loadModel('UsersProjects');
                    $this->loadModel('ChatRoomMembers');
                    $conditions = ['project_id' => $data['project_id'], 'UsersProjects.type' => 2, 'UsersProjects.status' => 1];
                    if (isset($data['keyword']) && !empty($data['keyword'])) {
                        $conditions['OR'] = ['Users.first_name LIKE' => '%' . $data['keyword'] . '%', 'Users.last_name LIKE' => '%' . $data['keyword'] . '%'];
                    }
                    $limit = 10;
                    if (isset($data['limit']) && is_numeric($data['limit'])) {
                        $limit = $data['limit'];
                    }
                    $offset = 0;
                    if (isset($data['offset']) && is_numeric($data['offset'])) {
                        $offset = $data['offset'];
                    }
                    $listMemberRespone = [];
                    if (isset($data['room_id']) && !empty($data['room_id'])) {
                        $listMembersInRoom = $this->ChatRoomMembers->getMembersByOptions(['room_id' => $data['room_id']], [], $limit, $offset, ['ChatRooms']);
                        $listMemberIdInRoom = $this->_getUserIdByList($listMembersInRoom);
                        if (count($listMemberIdInRoom) > 0) {
                            $conditions['UsersProjects.user_id NOT IN'] = $listMemberIdInRoom;
                            $listMemberRespone = $listMemberIdInRoom;
                        }
                    }
                    $listMembers = $this->UsersProjects->getUserProjectsByAllOptions($conditions, $limit, $offset, ['Users'], ['UsersProjects.user_id']);
                    $dataRespone['users'] = [];
                    $this->loadModel('Token');
                    foreach ($listMembers as $row) {
                        if ($row['user_id'] != $token->user_id) {
                            $memberData = [
                                'id' => $row['user_id'],
                                'name' => $row['user']['first_name'] . ' ' . $row['user']['last_name'],
                                'avatar' => !empty($row['user']['avatar']) ? $row['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                'member_status' => !empty($row['chat_status']) ? $row['chat_status'] : '',
                                'number_not_read' => 0
                            ];
                            $UserToken = $this->Token->find('all', ['conditions' => ['user_id' => $row['user_id'], 'platform' => 2]])->first();
                            if (!empty($UserToken)) {
                                $memberData['chat_status'] = 1;
                            } else {
                                $memberData['chat_status'] = 0;
                            }
                            $singleRooms = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.creator_id' => $row['user_id'], 'ChatRooms.type' => 1, 'ChatRoomMembers.member_id' => $token->user_id], [], $limit, $offset, ['ChatRooms']);
                            $this->loadModel('ChatNotifications');
                            if (count($singleRooms) > 0) {
                                $memberData['number_not_read'] = $this->ChatNotifications->find('list', ['conditions' => ['room_id' => $singleRooms[0]->room_id, 'receive_id' => $row['user_id'], 'read_flg' => 0]])->count();
                            } else {
                                $singleRooms = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.creator_id' => $token->user_id, 'ChatRooms.type' => 1, 'ChatRoomMembers.member_id' => $row['user_id']], [], $limit, $offset, ['ChatRooms']);
                                if (count($singleRooms) > 0) {
                                    $memberData['number_not_read'] = $this->ChatNotifications->find('list', ['conditions' => ['room_id' => $singleRooms[0]->room_id, 'receive_id' => $token->user_id, 'sender_id' => $row['user_id'], 'read_flg' => 0]])->count();
                                } else {
                                    $memberData['number_not_read'] = 0;
                                }
                            }
                            $dataRespone['users'][] = $memberData;
                            $listMemberRespone[] = $row['user_id'];
                        }
                    }
                    $this->loadModel('Projects');
                    $projectInfo = $this->Projects->getListProjectsByOptions('all', ['Projects.id' => $data['project_id']], ['Users']);
                    if (!empty($projectInfo)) {
                        // Has project info
                        if ($projectInfo[0]->user_id != $token->user_id && !in_array($projectInfo[0]->user_id, $listMemberRespone)) {
                            // Add project creator to list contact
                            $memberData = [
                                'id' => $projectInfo[0]['user_id'],
                                'name' => $projectInfo[0]['user']['first_name'] . ' ' . $projectInfo[0]['user']['last_name'],
                                'avatar' => !empty($projectInfo[0]['user']['avatar']) ? $projectInfo[0]['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                'member_status' => !empty($projectInfo[0]['user']['chat_status']) ? $projectInfo[0]['user']['chat_status'] : '',
                                'number_not_read' => 0
                            ];
                            $UserToken = $this->Token->find('all', ['conditions' => ['user_id' => $projectInfo[0]['user_id'], 'platform' => 2]])->first();
                            if (!empty($UserToken)) {
                                $memberData['chat_status'] = 1;
                            } else {
                                $memberData['chat_status'] = 0;
                            }
                            $singleRooms = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.creator_id' => $projectInfo[0]['user_id'], 'ChatRooms.type' => 1, 'ChatRoomMembers.member_id' => $token->user_id], [], $limit, $offset, ['ChatRooms']);
                            $this->loadModel('ChatNotifications');
                            if (count($singleRooms) > 0) {
                                $memberData['number_not_read'] = $this->ChatNotifications->find('list', ['conditions' => ['room_id' => $singleRooms[0]->room_id, 'receive_id' => $projectInfo[0]['user_id'], 'read_flg' => 0]])->count();
                            } else {
                                $singleRooms = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.creator_id' => $token->user_id, 'ChatRooms.type' => 1, 'ChatRoomMembers.member_id' => $projectInfo[0]['user_id']], [], $limit, $offset, ['ChatRooms']);
                                if (count($singleRooms) > 0) {
                                    $memberData['number_not_read'] = $this->ChatNotifications->find('list', ['conditions' => ['room_id' => $singleRooms[0]->room_id, 'receive_id' => $token->user_id, 'sender_id' => $projectInfo[0]['user_id'], 'read_flg' => 0]])->count();
                                } else {
                                    $memberData['number_not_read'] = 0;
                                }
                            }
                            $dataRespone['users'][] = $memberData;
                        }
                    }
                    $this->responseApi(1, 'Success', $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 06-01-2017
    // Function: get user id by list members
    private function _getUserIdByList($listMembers = []) {
        $listUserIds = [];
        foreach ($listMembers as $item) {
            $listUserIds[] = $item['member_id'];
        }
        return $listUserIds;
    }

    // Coder: Giang Dien
    // Date: 09-01-2017
    // Function: create member to room
    public function createMemberRoom() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['member_id']) && !empty($data['member_id'])) {
                    if (isset($data['project_id']) && !empty($data['project_id'])) {
                        if (isset($data['room_id']) && !empty($data['room_id']) && is_numeric($data['room_id'])) {
                            $chatRoomTable = TableRegistry::get('ChatRooms');
                            $currentRoom = $chatRoomTable->getRoomsByOptions(['id' => $data['room_id']]);
                            if (!empty($currentRoom)) {
                                $chatMemberTable = TableRegistry::get('ChatRoomMembers');
                                if ($currentRoom[0]->type == 1) {
                                    // Single room
                                    $chatRoom = $chatRoomTable->newEntity();
                                    $chatRoom->creator_id = $token['user_id'];
                                    $chatRoom->name = 'New room';
                                    $chatRoom->project_id = $data['project_id'];
                                    $chatRoom->type = 2;
                                    $chatRoom->created = date('Y-m-d H:i:s');
                                    if ($chatRoomTable->save($chatRoom)) {
                                        $room_id = $chatRoom->id;
                                        // Add old member to new room
                                        $oldMembersInRoom = $chatMemberTable->getMembersByOptions(['room_id' => $data['room_id']]);
                                        foreach ($oldMembersInRoom as $row) {
                                            $chatMemberData = $chatMemberTable->newEntity();
                                            $chatMemberData->room_id = $room_id;
                                            $chatMemberData->member_id = $row['member_id'];
                                            $chatMemberData->created = date('Y-m-d H:i:s');
                                            $chatMemberTable->save($chatMemberData);
                                        }
                                    } else {
                                        $this->responseApi(1030);
                                    }
                                } else {
                                    // group room
                                    $room_id = $data['room_id'];
                                }
                                // Add member to room
                                $chatMemberData = $chatMemberTable->newEntity();
                                $chatMemberData->room_id = $room_id;
                                $chatMemberData->member_id = $data['member_id'];
                                $chatMemberData->created = date('Y-m-d H:i:s');
                                if ($chatMemberTable->save($chatMemberData)) {
                                    $dataRespone = ['room_id' => $room_id];
                                    $this->responseApi(1, __('A group has been created.'), $dataRespone);
                                } else {
                                    $this->responseApi(1030);
                                }
                            } else {
                                $this->responseApi(1032);
                            }
                        } else {
                            $this->responseApi(1032);
                        }
                    } else {
                        $this->responseApi(1032);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 09-01-2017
    // Function: remove a member from a room
    public function removeMember() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['member_id']) && isset($data['room_id']) && !empty($data['member_id']) && !empty($data['room_id'])) {
                    $this->loadModel('ChatRooms');
                    $roomInfo = $this->ChatRooms->getRoomsByOptions(['id' => $data['room_id']]);
                    if (!empty($roomInfo)) {
                        if (isset($roomInfo[0]->creator_id) && $roomInfo[0]->creator_id == $token['user_id']) {
                            // check role user action
                            $chatMemberTable = TableRegistry::get('ChatRoomMembers');
                            if ($chatMemberTable->deleteAll(['room_id' => $data['room_id'], 'member_id' => $data['member_id']])) {
                                $dataRespone = ['member_id' => $data['member_id']];
                                $userTable = TableRegistry::get('Users');
                                $userAction = $userTable->get($token['user_id']);
                                $memberInfo = $userTable->get($data['member_id']);
                                if (!empty($memberInfo)) {
                                    $alert = $userAction->first_name . ' ' . $userAction->last_name . __(' has been removed ') . $memberInfo->first_name . ' ' . $memberInfo->last_name;
                                    $this->responseApi(1, $alert, $dataRespone);
                                } else {
                                    $this->responseApi(1030);
                                }
                            } else {
                                $this->responseApi(1030);
                            }
                        } else {
                            $this->responseApi(1030);
                        }
                    } else {
                        $this->responseApi(1032);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 04-01-2017
    // Function: get conversations
    public function getConversations() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['room_id']) && !empty($data['room_id'])) {
                    $this->loadModel('ChatRoomMessages');
                    $listMessages = $this->ChatRoomMessages->getMessagesByOptions(['room_id' => $data['room_id']], [], 0, 0, ['ChatMessages', 'Users'], ['ChatRoomMessages.id' => 'ASC']);
                    $dataRespone['message'] = [];
                    $this->loadModel('ChatRoomMembers');
                    foreach ($listMessages as $row) {
                        $dataMessage = [
                            'id' => $row['id'],
                            'message' => $row['chat_message']['message'],
                            'read_flg' => ($row['chat_message']['read_flg'] == TRUE) ? 1 : 0,
                            'created' => $row['chat_message']['created'],
                            'read_date' => $row['chat_message']['last_update'],
                            'user' => [
                                'id' => $row['sender_id'],
                                'name' => $row['user']['first_name'] . ' ' . $row['user']['last_name'],
                                'avatar' => !empty($row['user']['avatar']) ? $row['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg'
                            ]
                        ];
                        $dataRespone['message'][] = $dataMessage;
                    }
                    $this->responseApi(1, 'Success', $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: get old conversations
    public function getOldConversations() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['room_id']) && !empty($data['room_id'])) {
                    $this->loadModel('ChatRoomMessagesHistory');
                    $conditions = ['room_id' => $data['room_id']];
                    $limit = 20;
                    $offset = 0;
                    if (isset($data['page']) && $data['page'] > 1) {
                        $offset = ($data['page'] - 1) * $limit;
                    }
                    $listMessages = $this->ChatRoomMessagesHistory->getMessagesByOptions($conditions, [], $limit, $offset, ['ChatMessagesHistory', 'Users'], ['ChatRoomMessagesHistory.id' => 'ASC']);
                    $dataRespone['message'] = [];
                    $this->loadModel('ChatRoomMembers');
                    foreach ($listMessages as $row) {
                        $dataMessage = [
                            'id' => $row['id'],
                            'message' => $row['chat_messages_history']['message'],
                            'read_flg' => ($row['chat_messages_history']['read_flg'] == TRUE) ? 1 : 0,
                            'created' => $row['chat_messages_history']['created'],
                            'read_date' => $row['chat_messages_history']['last_update'],
                            'user' => [
                                'id' => $row['sender_id'],
                                'name' => $row['user']['first_name'] . ' ' . $row['user']['last_name'],
                                'avatar' => !empty($row['user']['avatar']) ? $row['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg'
                            ]
                        ];
                        $dataRespone['message'][] = $dataMessage;
                    }
                    $this->responseApi(1, 'Success', $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 04-01-2017
    // Function: check Conversation Status
    public function checkConversationStatus() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if ((isset($data['room_id']) && !empty($data['room_id'])) || (isset($data['current_counter']) && !empty($data['current_counter']))) {
                    $this->loadModel('ChatRoomMessages');
                    $listMessages = $this->ChatRoomMessages->getMessagesByOptions(['room_id' => $data['room_id']], [], 0, 0, []);
                    if (count($listMessages) != $data['current_counter']) {
                        $dataRespone['action'] = 'reload';
                    } else {
                        $dataRespone['action'] = 'not_change';
                    }
                    $this->responseApi(1, 'Success', $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 09-01-2017
    // Function: Send message to room
    public function sendMessage() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['room_id']) && !empty($data['room_id']) && isset($data['message']) && !empty($data['message'])) {
                    // Add message
                    $chatMessageTable = TableRegistry::get('ChatMessages');
                    $chatRoomMessageTable = TableRegistry::get('ChatRoomMessages');
                    $messageData = $chatMessageTable->newEntity();
                    $messageData->message = $data['message'];
                    $messageData->type = 1; // Type of message (default is text)
                    $messageData->created = date('Y-m-d H:i:s');
                    $messageData->last_update = date('Y-m-d H:i:s');
                    if ($chatMessageTable->save($messageData)) {
                        $messageId = $messageData->id;
                    } else {
                        $this->responseApi(1030);
                    }
                    $chatRoomMessageData = $chatRoomMessageTable->newEntity();
                    $chatRoomMessageData->sender_id = $token['user_id'];
                    $chatRoomMessageData->room_id = $data['room_id'];
                    $chatRoomMessageData->message_id = $messageId;
                    $chatRoomMessageTable->save($chatRoomMessageData);
                    $this->loadModel('ChatRoomMembers');
                    $listMembers = $this->ChatRoomMembers->getMembersByOptions(['room_id' => $data['room_id']]);
                    $chatNotificationTable = TableRegistry::get('ChatNotifications');
                    $this->loadModel('ChatRooms');
                    $roomInfo = $this->ChatRooms->get($data['room_id']);
                    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                    $optionData = ['project_id' => $roomInfo->project_id, 'room_id' => $data['room_id']];
                    $optionDataJson = json_encode($optionData);
                    // Add cronjob
                    $cronjobsTable = TableRegistry::get('Cronjobs');
                    $cronjobData = $cronjobsTable->newEntity();
                    $cronjobData->cronjob_name = 'send_chat_notification';
                    $cronjobData->option_data = json_encode(['room_id' => $roomInfo->id, 'project_id' => $roomInfo->project_id, 'sender_id' => $token->user_id]);
                    $cronjobData->created = date('Y-m-d H:i:s');
                    $cronjobsTable->save($cronjobData);
                    $this->loadModel('Notifications');
                    // Add notification for other member
                    foreach ($listMembers as $row) {
                        if ($row->member_id != $token->user_id) {
                            $chatNotificationData = $chatNotificationTable->newEntity();
                            $chatNotificationData->room_id = $data['room_id'];
                            $chatNotificationData->sender_id = $token['user_id'];
                            $chatNotificationData->receive_id = $row->member_id;
                            $chatNotificationData->read_flg = 0;
                            $chatNotificationData->created = date('Y-m-d H:i:s');
                            $chatNotificationTable->save($chatNotificationData);
                            $userNotification = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row->member_id]])->first();
                            $activeNotification = FALSE;
                            if ($roomInfo->type == 1) {
                                // Chat single
                                $activeNotification = ($userNotification->private_message_mobile_status == 1) ? TRUE : FALSE;
                            } else {
                                // Chat group
                                $activeNotification = ($userNotification->group_message_mobile_status == 1) ? TRUE : FALSE;
                            }
                            if ($activeNotification == TRUE) {
                                $notificationData = $this->Notifications->newEntity();
                                $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' => $row->member_id, 'action_user_id' => $token->user_id, 'option_data' => $optionDataJson, 'project_id' => $roomInfo->project_id, 'action_type' => 15, 'read_flg' => 0, 'created' => date('Y-m-d H:i:s')]);
                                $this->Notifications->save($newNotification);
                            }
                        }
                    }
                    $this->responseApi(1, __('Your message has been sent.'), []);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 10-01-2017
    // Function: get room current info
    public function getRoomInfo() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['room_id']) && !empty($data['room_id']) && isset($data['project_id']) && !empty($data['project_id'])) {
                    $chatNotificationTable = TableRegistry::get('ChatNotifications');
                    $chatNotificationTable->updateAll(['read_flg' => 1], ['room_id' => $data['room_id'], 'receive_id' => $token->user_id]);
                    $this->loadModel('ChatRoomMembers');
                    $listMembers = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.project_id' => $data['project_id'], 'ChatRooms.id' => $data['room_id']], [], 0, 0, ['ChatRooms', 'Users']);
                    if ($this->_checkMemberExits($listMembers, $token->user_id)) {
                        $roomInfo = isset($listMembers[0]->chat_room) ? $listMembers[0]->chat_room : [];
                        if (!empty($roomInfo)) {
                            if ($roomInfo['status'] == 1) {
                                // Room active
                                if ($roomInfo['type'] == 2) {
                                    // Chat group
                                    $dataRoom['room_name'] = '';
                                    $dataRoom['room_status'] = '';
                                    $dataRoom['chat_status'] = 0;
                                    $dataRoom['room_type'] = 2;
                                    $dataRoom['room_avatar'] = ROOT_URL . 'img/avatar_default.jpg';
                                    $data['users'] = [];
                                    foreach ($listMembers as $item) {
                                        $dataRoom['room_name'] .= $item['user']['first_name'] . ' ' . $item['user']['last_name'] . ',';
                                        $dataRoom['users'][] = [
                                            'member_id' => $item['member_id'],
                                            'avatar' => !empty($item['user']['avatar']) ? $item['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                            'member_name' => $item['user']['first_name'] . ' ' . $item['user']['last_name']
                                        ];
                                    }
                                } else {
                                    // Chat single
                                    $data['users'] = [];
                                    $dataRoom['room_name'] = '';
                                    $dataRoom['room_type'] = 1;
                                    $dataRoom['room_avatar'] = ROOT_URL . 'img/avatar_default.jpg';
                                    foreach ($listMembers as $item) {
                                        if ($item['member_id'] != $token->user_id) {
                                            $dataRoom['room_name'] = $item['user']['first_name'] . ' ' . $item['user']['last_name'];
                                            $dataRoom['room_status'] = !empty($item['user']['chat_status']) ? $item['user']['chat_status'] : '';
                                            $dataRoom['room_avatar'] = !empty($item['user']['avatar']) ? $item['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg';
                                            $UserToken = $tokenTable->find('all', ['user_id' => $item['member_id']])->first();
                                            if (!empty($UserToken)) {
                                                $dataRoom['chat_status'] = 1;
                                            } else {
                                                $dataRoom['chat_status'] = 0;
                                            }
                                        }
                                        $dataRoom['users'][] = [
                                            'member_id' => $item['member_id'],
                                            'avatar' => !empty($item['user']['avatar']) ? $item['user']['avatar'] : ROOT_URL . 'img/avatar_default.jpg',
                                            'member_name' => $item['user']['first_name'] . ' ' . $item['user']['last_name']
                                        ];
                                    }
                                }
                                $this->responseApi(1, 'Success', $dataRoom);
                            } else {
                                // Room inactive
                                $this->responseApi(1, 'Success', []);
                            }
                        } else {
                            $this->responseApi(1, 'Success', []);
                        }
                    } else {
                        $this->responseApi(1, 'Success', []);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 11-01-2017
    // Function: check member exits in list members
    private function _checkMemberExits($listMembers = [], $memberId = 0) {
        foreach ($listMembers as $row) {
            if ($row->member_id == $memberId) {
                return TRUE;
            }
        }
        return FALSE;
    }

    // Coder: Giang Dien
    // Date: 11-01-2017
    // Function: create single room
    public function createSingleRoom() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['member_id']) && !empty($data['member_id']) && ($data['member_id'] != $token->user_id) && isset($data['project_id']) && !empty($data['project_id'])) {
                    // Add room
                    $chatRoomTable = TableRegistry::get('ChatRooms');
                    $chatRoomData = $chatRoomTable->newEntity();
                    $chatRoomData->creator_id = $token->user_id;
                    $chatRoomData->name = 'New Room';
                    $chatRoomData->project_id = $data['project_id'];
                    $chatRoomData->created = date('Y-m-d H:i:s');
                    $chatRoomData->type = 1; // single chat room
                    if ($chatRoomTable->save($chatRoomData)) {
                        // create room success
                        $chatRoomMemberTable = TableRegistry::get('ChatRoomMembers');
                        $chatRoomMemberData = $chatRoomMemberTable->newEntity();
                        $chatRoomMemberData->room_id = $chatRoomData->id;
                        $chatRoomMemberData->member_id = $token->user_id;
                        $chatRoomMemberData->created = date('Y-m-d H:i:s');
                        $chatRoomMemberTable->save($chatRoomMemberData);
                        $chatRoomMemberData = $chatRoomMemberTable->newEntity();
                        $chatRoomMemberData->room_id = $chatRoomData->id;
                        $chatRoomMemberData->member_id = $data['member_id'];
                        $chatRoomMemberData->created = date('Y-m-d H:i:s');
                        $chatRoomMemberTable->save($chatRoomMemberData);
                        $dataRespone = [
                            'room_id' => $chatRoomData->id,
                            'room_name' => 'New room',
                            'created' => date('Y-m-d H:i:s')
                        ];
                        $this->responseApi(1, 'Success', $dataRespone);
                    } else {
                        $this->responseApi(1030);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 11-01-2017
    // Function: check member exit in single room
    public function checkSingleRoomExit() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['member_id']) && !empty($data['member_id']) && isset($data['project_id']) && !empty($data['project_id'])) {
                    $this->loadModel('ChatRoomMembers');
                    $listMembers = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.project_id' => $data['project_id'], 'ChatRooms.type' => 1, 'ChatRooms.creator_id' => $token->user_id, 'ChatRoomMembers.member_id' => $data['member_id']], [], 0, 0, ['ChatRooms']);
                    if (!empty($listMembers)) {
                        $this->responseApi(1, 'Success', ['room_id' => $listMembers[0]->room_id]);
                    } else {
                        $listMembers = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.project_id' => $data['project_id'], 'ChatRooms.type' => 1, 'ChatRooms.creator_id' => $data['member_id'], 'ChatRoomMembers.member_id' => $token->user_id], [], 0, 0, ['ChatRooms']);
                        if (!empty($listMembers)) {
                            $this->responseApi(1, 'Success', ['room_id' => $listMembers[0]->room_id]);
                        } else {
                            $this->responseApi(1, 'Success', ['room_id' => 0]);
                        }
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 07-02-2017
    // Function: Delete room
    public function deleteRoom() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                if (isset($data['room_id']) && !empty($data['room_id'])) {
                    $chatRoomTable = TableRegistry::get('ChatRooms');
                    $dataChatRoom = $chatRoomTable->get($data['room_id']);
                    $chatRoomTable->patchEntity($dataChatRoom, ['status' => 0]);
                    if ($chatRoomTable->save($dataChatRoom)) {
                        $this->responseApi(1, 'Success', []);
                    } else {
                        $this->responseApi(1030);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

}

?>