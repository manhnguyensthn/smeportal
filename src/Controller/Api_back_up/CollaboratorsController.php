<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class CollaboratorsController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
    }

    public function getProfile() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $userId = ($data['user_id'] == 0) ? $token->user_id : $data['user_id'];
                //load users model
                $this->loadModel('Users');
                $collaborators = $this->Users->find('all', [
                    'conditions' => ['id' => $userId],
                    'fields' => ['id', 'name', 'first_name', 'last_name', 'biography', 'avatar'],
                    'contain' => ['Roles']
                ]);
                if (!empty($collaborators)) {
                    $collaborators = $collaborators->first();
                    //get colaborator portfolio
                    $usersWorks = TableRegistry::get('UsersWorks');
                    $works = $usersWorks->find('all', [
                                'fields' => ['id', 'work_url', 'work_video_id', 'work_thumbnail', 'work_title'],
                                'conditions' => ['user_id' => $collaborators->id]
                            ])->toArray();

                    //check if user is follow this collaborator
                    $followingsTable = TableRegistry::get('Followings');
                    $query = $followingsTable->find()
                            ->where(['user_id' => $token->user_id, 'following_id' => $collaborators->id, 'connection' => 2]);
                    $isFollowing = $query->count();

                    //get count following user of collaborator
//                    $followings = $followingsTable->find()
//                                    ->where(['user_id' => $collaborators->id, 'following_id !=' => 0, 'connection' => 2]);
                    $followings = $followingsTable->getFollowerFollowing(['user_id' => $collaborators->id, 'following_id !=' => 0, 'connection' => 2]);
                    if (!empty($followings)) {
                        foreach ($followings as $key => $following) {
                            if (!$this->Users->exists(['id' => $following->following_id])) {
                                unset($followings[$key]);
                            }
                        }
                    }
                    //get count follower user of collaborator
//                    $followers = $followingsTable->find()
//                                    ->where(['following_id' => $collaborators->id, 'user_id !=' => 0, 'connection' => 2]);
                    $followers = $followingsTable->getFollowerFollowing(['following_id' => $collaborators->id, 'user_id !=' => 0, 'connection' => 2]);
                    if (!empty($followers)) {
                        foreach ($followers as $key => $follower) {
                            if (!$this->Users->exists(['id' => $follower->user_id])) {
                                unset($followers[$key]);
                            }
                        }
                    }
                    //get all project that collborator created
                    $projectsTable = TableRegistry::get('Projects');
                    $projects = $projectsTable->find()
                                    ->where(['user_id' => $collaborators->id])->count();
                    $this->_status = 1;
                    $this->_data = [
                        'user' => [
                            'id' => $collaborators->id,
                            'name' => $collaborators->name,
                            'first_name' => $collaborators->first_name,
                            'last_name' => $collaborators->last_name,
                            'biography' => (string) $collaborators->biography,
                            'message' => '',
                            'avatar' => (string) $collaborators->avatar,
                            'is_save' => 0, // is user login
                            'is_offer' => 0, // is user login
                            'roles' => $collaborators->roles
                        ],
                        'is_follow' => ($isFollowing > 0) ? 2 : 1,
                        'following' => count($followings),
                        'follower' => count($followers),
                        'projects' => $projects
                    ];
                    $this->_data['userWorks'] = [];
                    if (!empty($works)) {
                        foreach ($works as $work) {
                            $this->_data['userWorks'][] = [
                                'id' => $work['id'],
                                'work_url' => $work['work_url'],
                                'work_video_id' => $work['work_video_id'],
                                'work_thumbnail' => (string) $work['work_thumbnail'],
                                'work_title' => $work['work_title']
                            ];
                        }
                    }

                    if (isset($data['project_id']) && isset($data['role_id'])) {
                        $userProjectTable = TableRegistry::get('UsersProjects');
                        $listUserRoles = $userProjectTable->find('all',['conditions'=>['user_id'=>$collaborators->id,'project_id'=>$data['project_id'],'role_id'=>$data['role_id'],'type'=>2]])->toArray();
                        $this->_data['user']['message'] = (isset($listUserRoles[0]->message_content)) ? $listUserRoles[0]->message_content : '';
                        $listSaved = $this->_getListUserRoleByType($listUserRoles,1);
                        $listOffered = $this->_getListUserRoleByType($listUserRoles,2);
                        $this->_data['user']['is_save'] = (count($listSaved) > 0) ? 1 : 0;
                        $this->_data['user']['is_offer'] = (count($listOffered) > 0) ? 1 : 0;

                    }

                    $this->responseApi($this->_status, '', $this->_data);
                }
            } else {
                $this->responseApi(0, __('Invalid Token'), []);
            }
        }
    }
    
    // Coder: Giang Dien
    // Date: 23/01/2017
    // Function: 
    private function _getListUserRoleByType($list = [],$type = 1){
        $listUsers = [];
        foreach ($list as $row){
            if ($type == 1){
                // saved
                if ($row->saved == 1){
                    $listUsers[] = $row;
                }
            }
            else{
                // offer
                if ($row->offer == 1){
                    $listUsers[] = $row;
                }
            }
        }
        return $listUsers;
    }

    public function saveOfferAction() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            $this->loadModel('Users');
            $collaborators = $this->Users->get($data['user_id'], [
                'fields' => ['id', 'name', 'first_name', 'last_name', 'about_me'],
            ]);

            if (!empty($token) && !empty($collaborators)) {
                $UsersProjectsTable = TableRegistry::get('UsersProjects');
                $userProjects = $UsersProjectsTable->find('all', ['conditions' => ['project_id' => $data['project_id'], 'user_id' => $collaborators->id,'role_id'=> $data['role_id']]]);
                $userProjects = $userProjects->first();
                if (!empty($userProjects)) {
                    switch ($data['type']) {
                        case 0:
                            //update user project with save type
                            $saved = ($userProjects->saved == 0 ) ? 1 : 0;
                            $this->log($userProjects);
                            $userProjects = $UsersProjectsTable->patchEntity($userProjects, ['saved' => $saved]);
                            if ($UsersProjectsTable->save($userProjects)) {
                                if ($userProjects->saved == 1) {
                                    $this->_status = 1;
                                    $this->_message = __('Lưu thành công!');
                                } else {
                                    $this->_status = 1;
                                    $this->_message = __('You just un-saved successfully!');
                                }
                            } else {
                                $this->_status = 0;
                                $this->_message = __('Oops. Have some problem with your requested. Please try again!');
                            }
                            $this->responseApi($this->_status, $this->_message, []);
                            break;
                        case 1:
                            //update user project with offer type
                            $offer = ($userProjects->offer == 0 ) ? 1 : 0;
                            $userProjects = $UsersProjectsTable->patchEntity($userProjects, ['offer' => $offer]);
                            if ($UsersProjectsTable->save($userProjects)) {
                                if ($userProjects->status == 1 && $userProjects->offer == 1) {

                                    // SEND notification
                                    $ItemId = (isset($data['project_id']) ? $data['project_id'] : 0);
                                    $UsersProjectsTable = TableRegistry::get('UsersProjects');
                                    $UserProjects = $UsersProjectsTable->find()
                                        ->where(['project_id' => $ItemId, 'status' => 1, 'type' => 2])
                                        ->contain(['Roles'])->group(['user_id'])
                                        ->toArray();
                                        
                                    if($UserProjects){
                                        $UserId = $data['user_id'];
                                        $RoleId = (isset($data['role_id']) ? $data['role_id'] : 0);
                                        if($token->user_id && $UserId){
                                            foreach ($UserProjects as $key => $UserProject) {
                                                $TypeId = ($UserProject->user_id == $UserId) ? 13 : 14;
                                                $OptionData = array(
                                                    'role_id'    => $RoleId,
                                                    'project_id' => $ItemId,
                                                );
                                                $UserNotifySettingTable = TableRegistry::get('UserNotificationSetting');
                                                $UserNotifySetting      = $UserNotifySettingTable->find('all', ['conditions' => ['user_id' => $UserProject->user_id]])->first();

                                                $UserTable       = TableRegistry::get('Users');
                                                $UserJoinProject = $UserTable->find('all', ['conditions' => ['id' => $UserProject->user_id]])->first();

                                                $OptionData = json_encode($OptionData);

                                                // Send Mail notification
                                                if ($UserJoinProject && isset($UserNotifySetting->member_join_project_email_status) && $UserNotifySetting->member_join_project_email_status == 1) {
                                                    $template = 'notification';
                                                    $from     = [EMAIL_LOGIN => __('We The Projects')];
                                                    $subject  = __('[SME] Notification on SME');

                                                    // GET CONTENT MAIL
                                                    $contentMail = __('Send notification content');
                                                    if (isset($TypeId) && !empty($TypeId)) {
                                                        $User = $this->Users->get($data['user_id']);
                                                        $name = $collaborators->first_name . ' ' . $collaborators->last_name;
                                                        $contentMail = $this->getContentNotifySendMail($name, $TypeId, $OptionData);
                                                    }

                                                    if (!$this->sendMail( $template, $from, $UserJoinProject['email'], array(
                                                        'first_name' => $UserJoinProject['first_name'],
                                                        'last_name' => $UserJoinProject['last_name'],
                                                        'notification_content' => $contentMail
                                                    ), $subject)) {
                                                        $this->Flash->error(__('Send mail to creator fail.'));
                                                    }
                                                }
                                                
                                                // Send notification mobile
                                                if(isset($UserNotifySetting->member_join_project_mobile_status) && $UserNotifySetting->member_join_project_mobile_status == 1){
                                                    $NotificationsTable                = TableRegistry::get('Notifications');
                                                    $NotificationsData                 = $NotificationsTable->newEntity();
                                                    $NotificationsData->user_id        = $UserId;
                                                    $NotificationsData->action_user_id = $token->user_id;
                                                    $NotificationsData->project_id     = $ItemId;
                                                    $NotificationsData->option_data    = $OptionData;
                                                    $NotificationsData->action_type    = $TypeId;
                                                    $NotificationsData->read_flg       = 0;
                                                    $NotificationsData->created        = date('Y-m-d H:i:s');
                                                    if (!$NotificationsTable->save($NotificationsData)) {
                                                        $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                                                    }else{
                                                        //push notification for parent user comment
                                                        // if ($UserJoinProject->device_token != '') {
                                                        //     $ownerName = !empty($UserJoinProject->name) ? $UserJoinProject->name : $UserJoinProject->first_name . ' ' . $UserJoinProject->last_name;
                                                        //     $message = $ownerName . __('just reply your comment.');
                                                        //     $this->PushNotification->send($UserJoinProject->device_token, $message);
                                                        // }
                                                    }
                                                }
                                            }
                                        }
                                    }                                    
                                    // END: SEND notification

                                    $this->_status = 1;
                                    $this->_message = __('You just offered successfully!');
                                } else {
                                    $this->_status = 1;
                                    $this->_message = __('You just un-offered successfully!');
                                }
                            } else {
                                $this->_status = 0;
                                $this->_message = __('Oops. Have some problem with your requested. Please try again!');
                            }
                            $this->responseApi($this->_status, $this->_message, []);
                            break;
                        default :
                            break;
                    }
                } else {
                    $this->_status = 0;
                    if ($data['type'] == 0) {
                        $this->_message = __('Sorry, You can not save this user.');
                    }
                    if ($data['type'] == 1) {
                        $this->_message = __('Sorry, You can not offer this user.');
                    }
                    $this->responseApi($this->_status, $this->_message, []);
                }
            } else {
                $this->responseApi(0, __('Invalid Token'), []);
            }
        }
    }
}
