<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * UsersFollow Controller
 *
 */
class UsersFollowController extends ApiController {

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Followings');
    }

    public function showFollowing() {
        $this->autoRender = false;
        $follow = array();
        $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
        $avatarDefault = join(DIRECTORY_SEPARATOR, [$webRootURL, 'img', 'avatar_default.jpg']);

        if ($this->request->is('post')) {
            $data = $this->request->data;

            if (isset($data['user_id'])) { //get following for collaborator 
                $userId = $data['user_id'];
            } elseif (isset($data['token'])) { //get following this user (login user)
                $tokenTable = TableRegistry::get('Tokens');
                $token = $tokenTable->find('all', [
                    'conditions' => ['token' => $data['token']],
                ]);
                $token = $token->first();

                if (!empty($token)) {
                    $userId = $token->user_id;
                } else {
                    $userId = 0;
                }
            } else {
                $userId = 0;
            }
            if (isset($data['limit']) && !empty($data['limit'])) {
                $limit = $data['limit'];
            } else {
                $limit = 20;
            }
            if (isset($data['page']) && !empty($data['page'])) {
                $page = $data['page'];
            } else {
                $page = 1;
            }
            $offset = ($page - 1) * $limit;

            if (isset($userId) && $userId != 0) {
                $conditions = ['user_id' => $userId, 'following_id !=' => 0, 'connection' => 2];
                $fields = ['following_id', 'connection'];

                $data = $this->Followings->getFollowerFollowing($conditions, $fields, $limit, $offset);

                foreach ($data as $user) {
                    if (is_numeric($user->following_id)) {
                        if (!$this->Users->exists(['id' => $user->following_id])) {
                            break;
                        } else {
                            $user_following = $this->Users->get($user->following_id, [
                                'contain' => ['Roles']
                            ]);
                        }
                    } else {
                        $user_following = '';
                    }

                    if (!empty($user_following)) {
                        $avatarURL = $avatarDefault;
                        $result['userid'] = empty($user_following->id) ? 0 : $user_following->id;
                        $result['name'] = $user_following->first_name . ' ' . $user_following->last_name;
                        $result['name'] = empty($result['name']) ? '' : $result['name'];
                        if (!empty($user_following->avatar) && strlen($user_following->avatar) > 0) {
                            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
                            $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', basename($user_following->avatar)]);

                            $avatarFile = new File($fullPath);
                            if ($avatarFile->exists()) {
                                $avatarURL = $user_following->avatar;
                            }
                        }
                        $result['avatar'] = empty($avatarURL) ? '' : $avatarURL;

                        $state = $this->Users->States->find('list', ['fields' => ['name'],
                                    'conditions' => ['id' => $user_following ? $user_following->state_id : 0],
                                    'keyField' => 'id', 'valueField' => 'name'
                                ])->first();

                        $country = $this->Users->Countries->find('list', ['fields' => ['country_name'],
                                    'conditions' => ['id' => $user_following ? $user_following->country_id : 0],
                                    'keyField' => 'id', 'valueField' => 'country_name'])->first();

                        $result['address'] = (!empty($state) ? $state : '') . (!empty($state) && !empty($country) ? ', ' : '') . (!empty($country) ? $country : '');
                        $roles = array();
                        foreach ($user_following->roles as $role) {
                            $roles[] = empty($role->role) ? '' : $role->role;
                        }
                        $result['roles'] = implode(", ", $roles);
//                        $result['connection'] = $this->connection($token->user_id, $user_following->id, $data);
                        $result['connection'] = $user['connection'];

                        $follow[] = $result;
                    }
                }
                $this->_status = 1;
                $this->_data['user'] = $follow;
                $this->responseApi($this->_status, '', $this->_data);
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    public function showFollower() {
        $this->autoRender = false;
        $follow = array();
        $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
        $avatarDefault = join(DIRECTORY_SEPARATOR, [$webRootURL, 'img', 'avatar_default.jpg']);

        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (isset($data['user_id'])) { //get follower for collaborator 
                $userId = $data['user_id'];
            } elseif (isset($data['token'])) { //get follower this user (login user)
                $tokenTable = TableRegistry::get('Tokens');
                $token = $tokenTable->find('all', [
                    'conditions' => ['token' => $data['token']],
                ]);
                $token = $token->first();
                if (!empty($token)) {
                    $userId = $token->user_id;
                } else {
                    $userId = 0;
                }
            } else {
                $userId = 0;
            }
            if (isset($data['limit']) && !empty($data['limit'])) {
                $limit = $data['limit'];
            } else {
                $limit = 20;
            }
            if (isset($data['page']) && !empty($data['page'])) {
                $page = $data['page'];
            } else {
                $page = 1;
            }
            $offset = ($page - 1) * $limit;

            if (isset($userId) && $userId != 0) {
                $data = $this->Followings->getFollowerFollowing(['following_id' => $userId, 'connection' => 2], ['user_id', 'following_id','connection'], $limit, $offset);
                foreach ($data as $user) {
                    if (is_numeric($user->user_id)) {
                        $user_follower = $this->Users->get($user->user_id, [
                            'contain' => ['Roles']
                        ]);
                    } else
                        $user_follower = '';

                    if (!empty($user_follower)) {
                        $avatarURL = $avatarDefault;
                        $result['userid'] = empty($user_follower->id) ? 0 : $user_follower->id;
                        $result['name'] = $user_follower->first_name . ' ' . $user_follower->last_name;
                        $result['name'] = empty($result['name']) ? '' : $result['name'];
                        if (!empty($user_follower->avatar) && strlen($user_follower->avatar) > 0) {
                            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
                            $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', basename($user_follower->avatar)]);
                            $avatarFile = new File($fullPath);
                            if ($avatarFile->exists()) {
                                $avatarURL = $user_follower->avatar;
                            }
                        }
                        $result['avatar'] = empty($avatarURL) ? '' : $avatarURL;
                        $state = $this->Users->States->find('list', ['fields' => ['name'],
                                    'conditions' => ['id' => $user_follower ? $user_follower->state_id : 0],
                                    'keyField' => 'id', 'valueField' => 'name'
                                ])->first();

                        $country = $this->Users->Countries->find('list', ['fields' => ['country_name'],
                                    'conditions' => ['id' => $user_follower ? $user_follower->country_id : 0],
                                    'keyField' => 'id', 'valueField' => 'country_name'])->first();

                        $result['address'] = (!empty($state) ? $state : '') . (!empty($state) && !empty($country) ? ', ' : '') . (!empty($country) ? $country : '');
                        $roles = array();
                        foreach ($user_follower->roles as $role) {
                            $roles[] = empty($role->role) ? '' : $role->role;
                        }
                        $result['roles'] = implode(", ", $roles);
//                        $result['connection'] = $this->connection($token->user_id, $user_follower->id, $users_followings);
                        $result['connection'] = $user['connection'];
                        $follow[] = $result;
                    }
                }
                $this->_status = 1;
                $this->_data['user'] = $follow;
                $this->responseApi($this->_status, '', $this->_data);
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    public function followUserAction() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            
            if (!empty($token)) {
                $this->loadModel('Followings');
                switch ($data['type']) {
                    case 1:
                        /* check following */
                        $followings = $this->Followings->find('all', [
                            'conditions' => [
                                'user_id' => $token->user_id,
                                'following_id' => $data['user_id']
                            ]
                        ]);
                        $followings = $followings->first();
                        if (!empty($followings)) {
                            $connection = ($followings->connection == 2) ? 1: 2;
                            $followings = $this->Followings->patchEntity($followings, ['connection' => $connection]);
                            if ($this->Followings->save($followings)) {
                                $this->_status = 1;
                                $this->_data = $followings;
                                if ($data['connection'] == 1) {
                                    $this->_message = __('You just followed successfully!');
                                } else {
                                    $this->_message = __('You just un-followed successfully!');
                                }
                            } else {
                                $this->_message = __('Oops. Have some problem with your request. Please try agian later.');
                            }
                            $this->responseApi($this->_status, $this->_message, $this->_data);
                        } else {
                            //create new following
                            $followings = $this->Followings->newEntity();
                            $followData['user_id'] = $token->user_id;
                            $followData['following_id'] = $data['user_id'];
                            $followData['connection'] = 2;
                            $followings = $this->Followings->patchEntity($followings, $followData);
                            if ($this->Followings->save($followings)) {

                            	// SEND notification
								$UserId     = $data['user_id'];
								$OptionData = array( 'user_id' => $UserId );
								$ItemId     = $data['user_id'];
								$TypeId     = 12;

						        if($token->user_id && $UserId){
						            $UserNotifySettingTable = TableRegistry::get('UserNotificationSetting');
						            $UserNotifySetting      = $UserNotifySettingTable->find('all', ['conditions' => ['user_id' => $UserId]])->first();

						            $UserTable       = TableRegistry::get('Users');
						            $UserJoinProject = $UserTable->find('all', ['conditions' => ['id' => $UserId]])->first();

						            $OptionData = json_encode($OptionData);
                                    // Send Mail notification
                                    if ($UserJoinProject && isset($UserNotifySetting->new_follower_email_status) && $UserNotifySetting->new_follower_email_status == 1) {
                                        $template = 'notification';
                                        $from     = [EMAIL_LOGIN => __('We The Projects')];
                                        $subject  = __('[SME] Notification on SME');

                                        // GET CONTENT MAIL
                                        $contentMail = __('Send notification content');
                                        if (isset($TypeId) && !empty($TypeId)) {
                                            $User = $this->Users->get($token->user_id);
                                            $name = $User->first_name . ' ' . $User->last_name;
                                            $contentMail = $this->getContentNotifySendMail($name, $TypeId, $OptionData);
                                        }
                                        if (!$this->sendMail( $template, $from, $UserJoinProject['email'], array(
                                            'first_name' => $UserJoinProject['first_name'],
                                            'last_name' => $UserJoinProject['last_name'],
                                            'notification_content' => $contentMail
                                        ), $subject)) {
                                            $this->Flash->error(__('Send mail to creator fail.'));
                                        }
                                    }

						            // Send notification mobile
						            if(isset($UserNotifySetting->new_follower_mobile_status) && $UserNotifySetting->new_follower_mobile_status == 1){
						                $NotificationsTable                = TableRegistry::get('Notifications');
						                $NotificationsData                 = $NotificationsTable->newEntity();
						                $NotificationsData->user_id        = $UserId;
						                $NotificationsData->action_user_id = $token->user_id;
						                $NotificationsData->project_id     = $ItemId;
						                $NotificationsData->option_data    = $OptionData;
						                $NotificationsData->action_type    = $TypeId;
						                $NotificationsData->read_flg       = 0;
						                $NotificationsData->created        = date('Y-m-d H:i:s');
						                if (!$NotificationsTable->save($NotificationsData)) {
						                    $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
						                }else{
						                    //push notification for parent user comment
						                    // if ($UserJoinProject->device_token != '') {
						                    //     $ownerName = !empty($UserJoinProject->name) ? $UserJoinProject->name : $UserJoinProject->first_name . ' ' . $UserJoinProject->last_name;
						                    //     $message = $ownerName . __('just reply your comment.');
						                    //     $this->PushNotification->send($UserJoinProject->device_token, $message);
						                    // }
						                }
						            }
						        }
                                // END: SEND notification

                                $this->_status = 1;
                                $this->_data = $followings;
                                if ($data['connection'] == 1) {
                                    $this->_message = __('You just followed successfully!');
                                } else {
                                    $this->_message = __('You just un-followed successfully!');
                                }
                            } else {
                                $this->_message = __('Oops. Have some problem with your request. Please try agian later.');
                            }
                            $this->responseApi($this->_status, $this->_message, $this->_data);
                        }
                        break;

                    case 0:
                        /* check follower */
                        $followers = $this->Followings->find('all', [
                            'conditions' => [
                                'user_id' => $data['user_id'],
                                'following_id' => $token->user_id
                            ]
                        ]);
                        $followers = $followers->first();
                        if (!empty($followers)) {
                            $connection = ($followers->connection == 2) ? 1 : 2;
                            $followers = $this->Followings->patchEntity($followers, ['connection' => $connection]);
                            if ($this->Followings->save($followers)) {
                                $this->_status = 1;
                                $this->_data = $followers;
                                if ($data['connection'] == 1) {
                                    $this->_message = __('You just followed successfully!');
                                } else {
                                    $this->_message = __('You just un-followed successfully!');
                                }
                            } else {
                                $this->_message = __('Oops. Have some problem with your request. Please try agian later.');
                            }
                            $this->responseApi($this->_status, $this->_message, $this->_data);
                        } else {
                            //create new follower
                            $followers = $this->Followings->newEntity();
                            $followData['user_id'] = $data['user_id'];
                            $followData['following_id'] = $token->user_id;
                            $followData['connection'] = 2;
                            $followers = $this->Followings->patchEntity($followers, $followData);
                            if ($this->Followings->save($followers)) {
                                $this->_status = 1;
                                $this->_data = $followers;
                                if ($data['connection'] == 1) {
                                    $this->_message = __('You just followed successfully!');
                                } else {
                                    $this->_message = __('You just un-followed successfully!');
                                }
                            } else {
                                $this->_message = __('Oops. Have some problem with your request. Please try agian later.');
                            }
                            $this->responseApi($this->_status, $this->_message, $this->_data);
                        }
                        break;

                    default:
                        break;
                }
            } else {
                $this->responseApi(0, 'Invalid Token', []);
            }
        }
    }

    public function showBlocklist() {
        $this->autoRender = false;
        $block = array();
        $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
        $avatarDefault = join(DIRECTORY_SEPARATOR, [$webRootURL, 'img', 'avatar_default.jpg']);

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $blocksTable = TableRegistry::get('Blocks');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $limit = 20;
                if (isset($data['limit']) && !empty($data['limit'])) {
                    $limit = $data['limit'];
                }

                $page = 1;
                if (isset($data['page']) && !empty($data['page'])) {
                    $page = $data['page'];
                }
                $offset = ($page - 1) * $limit;
                $users = $blocksTable->find('list', ['fields' => ['blocked_id'],
                    'conditions' => ['user_id' => $token->user_id],
                    'valueField' => 'blocked_id',
                    'limit'      => $limit,
                    'offset'     => $offset,
                ]);
                $data = $users->toArray();

                foreach ($data as $user) {
                    if (is_numeric($user)) {
                        $user_blocked = $this->Users->get($user, [
                            'contain' => ['Roles']
                        ]);
                    } else
                        $user_blocked = '';

                    if (!empty($user_blocked)) {
                        $avatarURL = $avatarDefault;
                        $result['userid'] = $user_blocked->id;
                        $result['name'] = $user_blocked->first_name . ' ' . $user_blocked->last_name;
                        if (!empty($user_blocked->avatar) && strlen($user_blocked->avatar) > 0) {
                            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
                            $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', basename($user_blocked->avatar)]);
                            $avatarFile = new File($fullPath);
                            if ($avatarFile->exists()) {
                                $avatarURL = $user_blocked->avatar;
                            }
                        }
                        $result['avatar'] = $avatarURL;

                        $state = $this->Users->States->find('list', ['fields' => ['name'],
                                    'conditions' => ['id' => $user_blocked ? $user_blocked->state_id : 0],
                                    'keyField' => 'id', 'valueField' => 'name'
                                ])->first();

                        $country = $this->Users->Countries->find('list', ['fields' => ['country_name'],
                                    'conditions' => ['id' => $user_blocked ? $user_blocked->country_id : 0],
                                    'keyField' => 'id', 'valueField' => 'country_name'])->first();

                        $result['address'] = (!empty($state) ? $state : '') . (!empty($state) && !empty($country) ? ', ' : '')
                                . (!empty($country) ? $country : '');

                        $roles = array();
                        foreach ($user_blocked->roles as $role) {
                            $roles[] = $role->role;
                        }
                        $result['roles'] = implode(", ", $roles);

                        $block[] = $result;
                    }
                }
                $this->_status = 1;
                $this->_data['user'] = $block;
                $this->responseApi($this->_status, '', $this->_data);
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }

    public function blockUser() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $blocksTable = TableRegistry::get('Blocks');
            $followingsTable = TableRegistry::get('Followings');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                /* check blocking */
                $query = $blocksTable->find()
                        ->where(['user_id' => $token->user_id, 'blocked_id' => $data['userid']])
                        ->orWhere(['user_id' => $data['userid'], 'blocked_id' => $token->user_id]);
                $isBlock = $query->count();

                switch ($data['action']) {
                    case 'block':
                        /* check blocking */
                        if ($isBlock) {
                            $this->responseApi(0, __('You have already blocked this user before!'), []);
                            die;
                        }

                        $query = $followingsTable->query();
                        $query->delete()
                                ->where(['user_id' => $token->user_id, 'following_id' => $data['userid']])
                                ->orWhere(['user_id' => $data['userid'], 'following_id' => $token->user_id])
                                ->execute();

                        $block = $blocksTable->newEntity();
                        $block->user_id = $token->user_id;
                        $block->blocked_id = $data['userid'];

                        if ($blocksTable->save($block)) {
                            $this->responseApi(1, __('Block user successfully.'), []);
                            die;
                        }

                        break;

                    case 'unblock':
                        if (!$isBlock)
                            return;

                        $blocksTable->deleteAll(array(
                            'Blocks.user_id' => $token->user_id,
                            'Blocks.blocked_id' => $data['userid'],
                                ), false);

                        $this->responseApi(1, __('Unblock user successfully.'), []);

                        break;

                    default:
                        break;
                }
            } else {
                $this->responseApi(0, __('Invalid Token.'), []);
            }
        }
    }
}
