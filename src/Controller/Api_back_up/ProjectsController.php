<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UserProjectsTable $UserProjects
 */
class ProjectsController extends ApiController {

    public $components = array('RequestHandler', 'PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function action() {
        $this->autoRender = false;

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $type = $data['action_type'];

            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('UserProjectActions');
                $this->loadModel('Projects');
                $this->loadModel('Users');

                $project = $this->Projects->get($data['project_id'], ['fields' => ['title'], 'contain' => ['Users']]);
                $projectAction = $this->UserProjectActions->getUserProjectAction(['user_id' => $token->user_id, 'project_id' => $data['project_id'], 'action_type' => $type]);

                //set name for push
                $collaboratorName = !empty($project['user']['name']) ? $project['user']['name'] : $project['user']['first_name'] . ' ' . $project['user']['last_name'];

                if (empty($projectAction)) {
                    $dataAction = ['action_type' => $type, 'project_id' => $data['project_id'], 'user_id' => $token->user_id, 'status' => 1, 'created' => date('Y-m-d H:i:s')];
                    $respone = $this->UserProjectActions->addProjectAction($dataAction);
                    if ($respone == 'TRUE') {
                        if ($type == 1) {
                            $this->responseApi(1, __('You have been liked this project.'));
                            //set message for push
                            $message = $collaboratorName . __(' liked your project.');
                        } elseif ($type == 2) {
                            $this->responseApi(1, __('You have been followed this project.'));
                            //set message for push
                            $message = $collaboratorName . __(' followed your project.');
                        }
                        //send notification for owner project
                        if (!empty($project['user']['device_token'])) {
                            $this->PushNotification->send($project['user']['device_token'], $message);
                        }
                    } else {
                        $this->responseApi(1030);
                    }
                } else {
                    $status = (isset($projectAction['status']) && $projectAction['status'] == 0) ? 1 : 0;
                    $respone = $this->UserProjectActions->updateProjectAction($projectAction['id'], ['status' => $status]);
                    if ($respone == 'TRUE') {
                        if ($status == 0 && $type == 1) {
                            $this->responseApi(1, __('You have been un-liked this project.'));
                        } elseif ($status == 1 && $type == 1) {
                            $this->responseApi(1, __('You have been liked this project.'));
                            $message = $collaboratorName . __(' liked your project.');
                        } elseif ($status == 0 && $type == 2) {
                            $this->responseApi(1, __('You have been un-followed this project.'));
                        } elseif ($status == 1 && $type == 2) {
                            $this->responseApi(1, __('You have been followed this project.'));
                            $message = $collaboratorName . __(' followed your project.');
                        }
                        //send notification for owner project
                        if (!empty($project['user']['device_token'])) {
                            $this->PushNotification->send($project['user']['device_token'], $message);
                        }
                    } else {
                        $this->responseApi(1030);
                    }
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function showTeam() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                // Get tab member in the project
                $this->loadModel('UsersProjects');
                $usersProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id' => $data['project_id'], 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);
                $dataRespone = [];
                foreach ($usersProjects as $row) {
                    $data = [
                        'user_id' => $row['user_id'],
                        'role' => $row['role']['role'],
                        'name' => $row['name'],
                        'user_picture' => !empty($row['user_picture']) ? $row['user_picture'] : !empty($row['user']['avatar']) ? $row['user']['avatar'] : ROOT_URL . 'img/avatars/default.png',
                    ];
                    $dataRespone[] = $data;
                }
                $this->responseApi(1, 'Success', $dataRespone);
            } else {
                $this->responseApi(1031);
            }
        }
    }

    public function getListProjectUpdate() {
        // Get list updates of the project
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                $this->loadModel('ProjectUpdates');
                $projectUpdates = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['ProjectUpdates.project_id' => $data['project_id'], 'ProjectUpdates.status' => 1], ['Projects']);
                if (count($projectUpdates) > 0) {
                    foreach ($projectUpdates as $updates) {
                        $this->_data['projectUpdate'][] = [
                            'id' => $updates->id,
                            'project_id' => $updates->project_id,
                            'title' => $updates->title,
                            'content' => $updates->content,
                            'status' => $updates->status,
                            'type' => $updates->type,
                            'created' => date('Y-m-d\TH:i:s', strtotime($updates->created))
                        ];
                        $this->_data['project'] = ['start_date' => ($updates->Project['start_date'] == '0000-00-00') ? '' : date('Y-m-d\TH:i:s', strtotime($updates->Project['start_date'])), 'end_date' => ($updates->Project['end_date'] == '0000-00-00') ? '' : date('Y-m-d\TH:i:s', strtotime($updates->Project['end_date']))];
                    }
                } else {
                    $this->_data['projectUpdate'] = [];
                    $this->_data['project'] = ['start_date' => '', 'end_date' => ''];
                }
                $this->responseApi(1, '', $this->_data);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function getProjectComment() {
        // Get list comments of the project
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            $this->_data['comment'] = [];
            if (!empty($token)) {
                $this->loadModel('ProjectComments');
                $listComments = $this->ProjectComments->getListCommentByProjectId($data['project_id']);
                $dataRespone = [];
                foreach ($listComments as $row) {
                    $dataRespone[] = [
                        'id' => empty($row->id) ? 0 : $row->id,
                        'project_id' => empty($row->project_id) ? 0 : $row->project_id,
                        'comment' => empty($row->comment) ? '' : $row->comment,
                        'created' => empty($row->created) ? date('Y-m-d H:i:s') : $row->created,
                        'parent_comment_id' => empty($row->parent_comment_id) ? 0 : $row->parent_comment_id,
                        'user' => [
                            'name' => $row->user['first_name'] . ' ' . $row->user['last_name'],
                            'avatar' => empty($row->user['avatar']) ? '' : $row->user['avatar']
                        ]
                    ];
                }
                $this->_data['comment'] = $dataRespone;

                $this->responseApi(1, '', $this->_data);
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function addProjectUpdate() {
        // Add updates project
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('Projects');
                $this->loadModel('ProjectUpdates');
                $this->request->data['created'] = date('Y-m-d H:i:s');

                $project = $this->Projects->get($data['project_id'], ['fields' => ['title']]);
                $respone = $this->ProjectUpdates->addUpdate($data);
                if ($respone == 'TRUE') {
                    $this->loadModel('Users');
                    $this->loadModel('UsersProjects');
                    $userPro = $this->UsersProjects->find('all', ['fields' => ['project_id', 'user_id'], 'conditions' => ['project_id' => $data['project_id']]])->toArray();
                    if (!empty($userPro)) {
                        foreach ($userPro as $key => $val) {
                            $user = $this->Users->get($val['user_id'], ['fields' => ['device_token']])->toArray();
                            if (!empty($user)) {
                                $this->PushNotification->send($user['device_token'], __($project->title . ' has been updated.'));
                            }
                        }
                    }
                    $this->responseApi(1, __('Project update has been created.'));
                } else {
                    $message = '';
                    foreach ($respone as $row) {
                        foreach ($row as $item) {
                            $message .= $item . ' ';
                        }
                    }
                    $this->responseApi(0, $message);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function addProjectComment() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $data = $this->request->data;
                if (isset($data['project_id']) && !empty($data['project_id']) && isset($data['comment']) && !empty($data['comment'])) {
                    $data['created'] = date('Y-m-d H:i:s');
                    $this->loadModel('ProjectComments');
                    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                        $ipRequest = $_SERVER['HTTP_CLIENT_IP'];
                    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                        $ipRequest = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    } else {
                        $ipRequest = $_SERVER['REMOTE_ADDR'];
                    }
                    $data['ip_comment'] = $ipRequest;

                    $data['user_id'] = $token->user_id;
                    $respone = $this->ProjectComments->addComment($data);
                    if ($respone == 'TRUE') {
                        $this->responseApi(1, __('Project comment has been created.'));
                    } else {
                        $message = '';
                        foreach ($respone as $row) {
                            foreach ($row as $item) {
                                $message .= $item . ' <br/>';
                            }
                        }
                        $this->responseApi(0, $message);
                    }
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function deleteComment() {
        $this->autoRender = false;
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $this->loadModel('ProjectComments');

                $comments = $this->ProjectComments->find('all', [
                    'conditions' => ['id' => $data['comment_id']],
                ]);
                $comment = $comments->first();

                if ($comment) {
                    $result = $this->ProjectComments->delete($comment);
                    $this->responseApi(1, __('Delete comment success.'));
                } else {
                    $this->responseApi(1033);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

    public function getProjectInfo() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();

            if (!empty($token)) {
                $id = $data['project_id'];
                $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain([ 'Users'])->first();
                if (!empty($project)) {
                    $address = '';
                    if (!empty($project['country_id'])) {
                        $this->loadModel('Countries');
                        $country = $this->Countries->get($project['country_id']);
                        $address .= $country['country_name'];
                    } else {
                        $country = [];
                    }
                    if (!empty($project['state_id'])) {
                        $this->loadModel('States');
                        $state = $this->States->get($project['state_id']);
                        $address .= (!empty($address)) ? ' ' . $state['name'] : $state['name'];
                    } else {
                        $state = [];
                    }
                    if (!empty($project['category_id'])) {
                        $this->loadModel('Categories');
                        $category = $this->Categories->get($project['category_id']);
                    } else {
                        $category = [];
                    }

                    $this->loadModel('UserProjectActions');
                    //check is like
                    $likes = $this->UserProjectActions->getCountUserProjectActions(['user_id' => $token->user_id, 'project_id' => $project['id'], 'status' => 1, 'action_type' => 1]);
                    //check is followed
                    $follows = $this->UserProjectActions->getCountUserProjectActions(['user_id' => $token->user_id, 'project_id' => $project['id'], 'status' => 1, 'action_type' => 2]);

                    $this->loadModel('ProjectsRoles');
                    $quantity = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                    $startDate = new \DateTime(date('Y-m-d H:i:s'));
                    $startDate->modify('-7 day');
                    $this->loadModel('ProjectComments');
                    $listComments = $this->ProjectComments->getListCommentByOptions('all', ['project_id' => $project['id'], 'created >=' => $startDate->format('Y-m-d H:i:s')], []);
                    $this->loadModel('ProjectUpdates');
                    $listUpdates = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['project_id' => $project['id'], 'status' => 1, 'created >=' => $startDate->format('Y-m-d H:i:s')], []);
                    $this->loadModel('UsersProjects');
                    $this->_data = [
                        'title' => $project['title'],
                        'description' => $project['description'],
                        'start_date' => date('Y-m-d', strtotime($project['start_date'])),
                        'image_url' => $project['image_url'],
                        'project_url' => ROOT_URL . 'projects/' . $this->customSlug($project->title) . '-' . $project->id,
                        'video_id' => $project['video_id'],
                        'address' => $address,
                        'category' => isset($category['name']) ? $category['name'] : '',
                        'pitch' => $project['pitch'],
                        'owner' => !empty($project['user']['name']) ? $project['user']['name'] : $project['user']['first_name'] . ' ' . $project['user']['last_name'],
                        'total_role' => $quantity,
                        'total_role_joined' => count($this->UsersProjects->getUserProjectsByOptions(['project_id' => $project['id'], 'UsersProjects.status' => 1, 'UsersProjects.type' => 2])),
                        'total_comment' => count($listComments),
                        'total_update' => count($listUpdates),
                        'is_like' => ($likes > 0) ? 1 : 0,
                        'is_follow' => ($follows > 0) ? 1 : 0,
                    ];

                    $this->responseApi(1, '', $this->_data);
                } else {
                    $this->responseApi(1034);
                }
            } else {
                $this->responseApi(1031);
            }
        }
    }

    // Coder: Giang Dien
    // Date: 24/02/2017
    // Function: Get current and created project
    public function getListProjectByUser() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $data['token']],
            ]);
            $token = $token->first();
            if (!empty($token)) {
                // Type = 1 ==> created projects
                // Type = 2 ==> current projects
                if (isset($data['type']) && !empty($data['type'])) {
                    if (isset($data['user_id']) && !empty($data['user_id'])) {
                        $userId = $data['user_id'];
                    } else {
                        $userId = $token->user_id;
                    }
                    // Get list project ids current user liked
                    $userProjectActionTable = TableRegistry::get('UserProjectActions');
                    $listProjectIds = $userProjectActionTable->find('list', ['conditions' => ['user_id' => $userId, 'action_type' => 1,'status'=>1]])->select(['project_id'])->toArray();
                    $this->loadModel('Projects');
                    $conditions = [];
                    if ($data['type'] == 1){
                        // created projects
                        $conditions['user_id'] = $userId;
                    }
                    else{
                        // current projects
                        $userProjectTable = TableRegistry::get('UsersProjects');
                        $listProjectJoinIds = $userProjectTable->find('list',['conditions'=>['user_id'=>$userId,'UsersProjects.type'=>2,'UsersProjects.status'=>1]])->select(['project_id'])->toArray();
                        if (!empty($listProjectJoinIds)){
                            $conditions['Projects.id IN']= $listProjectJoinIds;
                        }
                    }
                    $listProjects = $this->Projects->getListProjectsByOptions('all', $conditions, ['Users']);
                    $dataRespone = [];
                    $this->loadModel('ProjectsRoles');
                    $this->loadModel('UsersProjects');
                    foreach ($listProjects as $project) {
                        $data = [
                            'project_id' => empty($project['id']) ? '' : $project['id'],
                            'title' => empty($project['title']) ? '' : $project['title'],
                            'description' => empty($project['description']) ? '' : $project['description'],
                            'image_url' => !empty($project['image_url']) ? $project['image_url'] : ROOT_URL . 'img/logo.png',
                            'video_id' => empty($project['video_id']) ? '' : $project['video_id'],
                            'pitch' => empty($project['pitch']) ? '' : $project['pitch'],
                            'start_date' => empty($project['start_date']) ? '' : $project['start_date'],
                            'end_date' => empty($project['end_date']) ? '' : $project['end_date'],
                            'owner' => $project['user']['first_name'] . ' ' . $project['user']['last_name']
                        ];
                        if (!empty($project['country_id'])) {
                            $this->loadModel('Countries');
                            $country = $this->Countries->get($project['country_id']);
                            $data['address'] = $country['country_name'];
                            if (!empty($project['state_id'])) {
                                $this->loadModel('States');
                                $state = $this->States->get($project['state_id']);
                                $data['address'] = $state['name'] . ', ' . $country['country_name'];
                            }
                        } else {
                            $data['address'] = '';
                        }
                        if (!empty($project['category_id'])) {
                            $this->loadModel('Categories');
                            $category = $this->Categories->get($project['category_id']);
                            $data['category'] = $category['name'];
                        } else {
                            $data['category'] = 'Feature';
                        }
                        $data['is_liked'] = in_array($project['id'], $listProjectIds) ? 1 : 0;
                        $data['total_role'] = $this->ProjectsRoles->getSumQuantityByProject(['project_id' => $project['id']]);
                        $data['total_role_joined'] = $this->UsersProjects->getCountUserProjectsByOptions(['project_id' => $project['id'], 'status' => 1, 'type' => 2]);
                        $data['list_roles'] = $this->ProjectsRoles->getProjectRolesByField(['ProjectsRoles.project_id' => $project['id'], 'ProjectsRoles.role_id !=' => 1], ['Roles'], ['Roles.id', 'Roles.role', 'Roles.role_es', 'Roles.role_ja', 'ProjectsRoles.id', 'ProjectsRoles.quantity']);
                        $dataRespone[] = $data;
                    }
                    $this->responseApi(1, __('Success'), $dataRespone);
                } else {
                    $this->responseApi(1032);
                }
            } else {
                $this->responseApi(1031);
            }
        } else {
            $this->responseApi(1032);
        }
    }

}
