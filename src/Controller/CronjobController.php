<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;

class CronjobController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['backupMessages', 'removeOldMessage', 'backupChatNotification', 'runCronjobQueue']);
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: backup message table to history
    public function backupMessages() {
        $this->loadModel('ChatMessages');
        $weekDay = new \DateTime();
        $weekDay->modify('- 7day');
        $listChatMessage = $this->ChatMessages->getMessagesByOptions(['created <' => $weekDay->format('Y-m-d')]);
        $chatMessageTable = TableRegistry::get('ChatMessages');
        $chatMessageHistoryTable = TableRegistry::get('ChatMessagesHistory');
        $listMessageIds = [];
        foreach ($listChatMessage as $row) {
            $chatMessageData = $chatMessageHistoryTable->newEntity();
            $chatMessageData->id = $row['id'];
            $chatMessageData->message = $row['message'];
            $chatMessageData->type = $row['type'];
            $chatMessageData->created = $row['created'];
            $chatMessageData->last_update = $row['last_update'];
            if ($chatMessageHistoryTable->save($chatMessageData)) {
                $chatMessageTable->delete($row);
                echo 'Insert message ' . $row['id'] . ' success<br/>';
                $listMessageIds[] = $row['id'];
            } else {
                echo 'Insert message ' . $row['id'] . ' error<br/>';
            }
        }
        $this->_backupRoomMessage($listMessageIds);
        die;
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: backup room message table
    private function _backupRoomMessage($listIds = []) {
        $this->loadModel('ChatRoomMessages');
        if (!empty($listIds)) {
            $listChatMessage = $this->ChatRoomMessages->getMessagesByOptions(['message_id IN' => $listIds]);
            $chatRoomMessageTable = TableRegistry::get('ChatRoomMessages');
            $chatRoomMessageHistoryTable = TableRegistry::get('ChatRoomMessagesHistory');
            foreach ($listChatMessage as $row) {
                $chatRoomMessageData = $chatRoomMessageHistoryTable->newEntity();
                $chatRoomMessageData->id = $row['id'];
                $chatRoomMessageData->sender_id = $row['sender_id'];
                $chatRoomMessageData->room_id = $row['room_id'];
                $chatRoomMessageData->message_id = $row['message_id'];
                $chatRoomMessageData->bk_created = date('Y-m-d H:i:s');
                if ($chatRoomMessageHistoryTable->save($chatRoomMessageData)) {
                    $chatRoomMessageTable->delete($row);
                    echo 'Insert room message ' . $row['id'] . ' success<br/>';
                } else {
                    echo 'Insert room message ' . $row['id'] . ' error<br/>';
                }
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: remove old message last 1 year
    public function removeOldMessage() {
        $chatMessageHistoryTable = TableRegistry::get('ChatMessagesHistory');
        $yearDay = new \DateTime();
        $yearDay->modify('- 365day');
        $listChatMessage = $chatMessageHistoryTable->getMessagesByOptions(['created <' => $yearDay->format('Y-m-d')]);
        $chatRoomMessageHistoryTable = TableRegistry::get('ChatRoomMessagesHistory');
        foreach ($listChatMessage as $row) {
            $chatMessageHistoryTable->delete($row);
            $chatRoomMessageHistoryTable->deleteAll(['message_id' => $row['id']]);
            echo 'Delete message ' . $row['id'] . ' success<br/>';
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: backup chat notification
    public function backupChatNotification() {
        $chatNotificationTable = TableRegistry::get('ChatNotifications');
        $listNotifications = $chatNotificationTable->getNotificationByOptions(['read_flg' => 1]);
        $chatNotificationHistoryTable = TableRegistry::get('ChatNotificationsHistory');
        foreach ($listNotifications as $row) {
            $chatNotificationData = $chatNotificationHistoryTable->newEntity();
            $chatNotificationData->id = $row['id'];
            $chatNotificationData->sender_id = $row['sender_id'];
            $chatNotificationData->receive_id = $row['receive_id'];
            $chatNotificationData->room_id = $row['room_id'];
            $chatNotificationData->read_flg = $row['read_flg'];
            $chatNotificationData->created = $row['created'];
            $chatNotificationData->bk_created = date('Y-m-d H:i:s');
            if ($chatNotificationHistoryTable->save($chatNotificationData)) {
                $chatNotificationTable->delete($row);
                echo 'Insert chat notification ' . $row['id'] . ' success<br/>';
            } else {
                echo 'Insert chat notification ' . $row['id'] . ' error<br/>';
            }
        }
    }

    // Coder: Giang Dien
    // Date: 18-01-2017
    // Function: running cronjobs
    public function runCronjobQueue() {
        $cronjobsTable = TableRegistry::get('Cronjobs');
        $cronjobsList = $cronjobsTable->getCronjobsByOptions();
        foreach ($cronjobsList as $row) {
            switch ($row->cronjob_name) {
                case 'send_chat_notification':
                    $optionDataArr = json_decode($row->option_data);
                    $this->_sendNotificationToMemberChat($optionDataArr);
                    $cronjobsTable->delete($row);
                    break;
                case 'send_notification_for_user_follow':
                    $optionDataArr = json_decode($row->option_data);
                    $this->_sendEmailNotificationForUserFollow($optionDataArr);
                    $cronjobsTable->delete($row);
                    break;
            }
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 18-01-2017
    // Function: send mail to list member chat
    private function _sendNotificationToMemberChat($optionData = []) {
        $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
        $this->loadModel('ChatRoomMembers');
        $listMembers = $this->ChatRoomMembers->getMembersByOptions(['ChatRooms.project_id' => $optionData->project_id, 'ChatRooms.id' => $optionData->room_id], [], 0, 0, ['ChatRooms', 'Users']);
        $this->loadModel('ChatRooms');
        $roomInfo = $this->ChatRooms->get($optionData->room_id);
        foreach ($listMembers as $row) {
            if ($row->member_id != $roomInfo->creator_id) {
                $userNotification = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row->member_id]])->first();
                $activeNotification = FALSE;
                if ($roomInfo->type == 1) {
                    // Chat single
                    $activeNotification = ($userNotification->private_message_email_status == 1) ? TRUE : FALSE;
                } else {
                    // Chat group
                    $activeNotification = ($userNotification->group_message_email_status == 1) ? TRUE : FALSE;
                }
                if ($activeNotification == TRUE) {
                    $template = 'notification';
                    $from = [EMAIL_LOGIN => __('We The Projects')];
                    $subject = __('[SME] Notification on SME');

                    // GET CONTENT MAIL
                    $name = $row['user']->first_name . ' ' . $row['user']->last_name;
                    $contentMail = $this->getContentNotifySendMail($name, 15, json_encode($optionData));
                    // END: GET CONTENT MAIL

                    if (!$this->sendMail($template, $from, $row['user']['email'], ['first_name' => $row['user']->first_name, 'last_name' => $row['user']->last_name, 'notification_content' => $contentMail], $subject)) {
                        echo 'Send mail to ' . $name . ' fail<br/>';
                    } else {
                        echo 'Send mail to ' . $name . ' success<br/>';
                    }
                }
            }
        }
        return TRUE;
    }

    // Coder: Giang Dien
    // Date: 2017-02-09
    // Function: send notification for user follow
    private function _sendEmailNotificationForUserFollow($optionData = []) {
        if (isset($optionData->user_action_id) && isset($optionData->action_type)) {
            $followUserTable = TableRegistry::get('Followings');
            $followerList = $followUserTable->getFollowUsers(['following_id' => $optionData->user_action_id, 'connection' => 2]);
            $userTable = TableRegistry::get('Users');
            $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
            foreach ($followerList as $row) {
                $userInfo = $userTable->get($row['user_id']);
                $userNotification = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row->member_id]])->first();
                if ($userNotification->project_update_email_status == 1) {
                    $template = 'notification';
                    $from = [EMAIL_LOGIN => __('We The Projects')];
                    $subject = __('[SME] Notification on SME');

                    // GET CONTENT MAIL
                    $name = $userInfo->first_name . ' ' . $userInfo->last_name;
                    $contentMail = $this->getContentNotifySendMail($name, $optionData->action_type, json_encode($optionData));
                    // END: GET CONTENT MAIL

                    if (!$this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject)) {
                        echo 'Send mail to ' . $name . ' fail<br/>';
                    } else {
                        echo 'Send mail to ' . $name . ' success<br/>';
                    }
                }
            }
        } else {
            return FALSE;
        }
    }

}
