<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Request;
use Cake\Network\Http\Client;
use Cake\View\View;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class ProjectsUpdateController extends AppController {

    public $components = array('PushNotification');

    public function initialize() {
        parent::initialize();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow([]);
    }

    /**
     * add Update
     * @author Roxannie Nguyen jr
     * @return json
     */
    public function addProjectUpdate() {
        $this->_status = 0;
        $RequestData = $this->request->data;
        $aUser = $this->Auth->user();
        if (!$aUser) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $iUserId = $aUser['id'];
        }

        if (!isset($RequestData['project_id']) || empty($RequestData['project_id'])) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $ProjectId = $RequestData['project_id'];
            $this->loadModel('Projects');
            $Project = $this->Projects->getProject($ProjectId);
            if (!$Project) {
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            } else {
                $UserIdOwner = $Project->user->id;
            }
        }

        if ($this->_status == 0) {
            if ($UserIdOwner != $iUserId) {
                $this->_status = 1;
                $this->_message = __('You are not owner.');
            }
        }

        $this->_validateFormUpdate($RequestData);

        if ($this->_status == 0) {
            $ProjectUpdatesTable = TableRegistry::get('ProjectUpdates');
            $ProjectUpdates = $ProjectUpdatesTable->newEntity();
            $ProjectUpdates->project_id = $ProjectId;
            $ProjectUpdates->title = $RequestData['title'];
            $ProjectUpdates->content = $RequestData['description'];
            $ProjectUpdates->type = UPDATE_TYPE;
            $ProjectUpdates->status = 1;
            $ProjectUpdates->created = date('Y-m-d H:i:s');

            if ($ProjectUpdatesTable->save($ProjectUpdates)) {
                $view = new View($this->request, $this->response, null);
                $view->set('ProjectUpdate', $ProjectUpdates);
                $view->set('bOwner', true);
                $view->layout = false;
                $this->set('Project', $Project);
                $this->_data['html_item'] = $view->render('/Element/Mettings/item_update');
                $this->_data['response'] = $ProjectUpdates;
                $this->_message = __('Add new successfully!');
               // $this->setNotification($Project, 10);
            } else {
                $this->_status = 1;
                $this->_message = __('Save Error');
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * Update Project Update
     * @author Roxannie Nguyen jr
     * @return json
     */
    public function updateProjectUpdate() {
        $this->_status = 0;
        $RequestData = $this->request->data;
        $aUser = $this->Auth->user();
        if (!$aUser) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $iUserId = $aUser['id'];
        }

        if (!isset($RequestData['project_update_id']) || empty($RequestData['project_update_id'])) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $ProjectUpdateId = $RequestData['project_update_id'];

            $this->loadModel('ProjectUpdates');
            $ProjectDataMilestone = $this->ProjectUpdates->find('all')->where(['id' => $ProjectUpdateId, 'type' => UPDATE_TYPE])->first();
            if (!$ProjectDataMilestone) {
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            } else {
                $this->loadModel('Projects');
                $Project = $this->Projects->getProject($ProjectDataMilestone->project_id);
                if (!$Project) {
                    $this->_status = 1;
                    $this->_message = __('Unable to process your request.');
                } else {
                    $UserIdOwner = $Project->user->id;
                    $ProjectId = $Project->id;
                }
            }
        }

        if ($this->_status == 0) {
            if ($UserIdOwner != $iUserId) {
                $this->_status = 1;
                $this->_message = __('You are not owner.');
            }
        }

        $this->_validateFormUpdate($RequestData);
        if ($this->_status == 0) {
            $ProjectUpdates = $this->ProjectUpdates->get($ProjectDataMilestone->id);
            $ProjectUpdates->title = $RequestData['title'];
            $ProjectUpdates->content = $RequestData['description'];
            $ProjectUpdates->status = 1;
            if ($this->ProjectUpdates->save($ProjectUpdates)) {
                $this->_data['response'] = $ProjectUpdates;
                $this->_message = __('Update successfully!');
                $this->sendNotificationForJoinProject($Project->id, 10,'');
            } else {
                $this->_status = 1;
                $this->_message = __('Update Error');
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * Delete Update
     * @author Roxannie Nguyen jr
     * @return json
     */
    public function deleteProjectUpdate() {
        $this->_status = 0;
        $RequestData = $this->request->data;
        $aUser = $this->Auth->user();
        if (!$aUser) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $iUserId = $aUser['id'];
        }

        if (!isset($RequestData['project_update_id']) || empty($RequestData['project_update_id'])) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $ProjectUpdateId = $RequestData['project_update_id'];

            $this->loadModel('ProjectUpdates');
            $ProjectDataMilestone = $this->ProjectUpdates->find('all')->where(['id' => $ProjectUpdateId, 'type' => UPDATE_TYPE])->first();
            if (!$ProjectDataMilestone) {
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            } else {
                $this->loadModel('Projects');
                $Project = $this->Projects->getProject($ProjectDataMilestone->project_id);
                if (!$Project) {
                    $this->_status = 1;
                    $this->_message = __('Unable to process your request.');
                } else {
                    $UserIdOwner = $Project->user->id;
                    $ProjectId = $Project->id;
                }
            }
        }

        if ($this->_status == 0) {
            if ($UserIdOwner != $iUserId) {
                $this->_status = 1;
                $this->_message = __('You are not owner.');
            }
        }

        if ($this->_status == 0) {
            $ProjectUpdates = $this->ProjectUpdates->get($ProjectDataMilestone->id);
            if ($this->ProjectUpdates->delete($ProjectUpdates)) {
                $this->_data['response'] = $ProjectUpdates;
                $this->_message = __('Delete successfully!');
            } else {
                $this->_status = 1;
                $this->_message = __('Delete Error');
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * Update Milestone
     * @author Roxannie Nguyen jr
     * @return json
     */
    public function updateProjectMilestone() {
        $this->_status = 0;
        $RequestData = $this->request->data;

        $AllowTypeId = array('add', 'update');
        if (!isset($RequestData['type_id']) || empty($RequestData['type_id'])) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            if (!in_array($RequestData['type_id'], $AllowTypeId)) {
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            } else {
                $TypeId = $RequestData['type_id'];
            }
        }

        $aUser = $this->Auth->user();
        if (!$aUser) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $iUserId = $aUser['id'];
        }

        if (!isset($RequestData['project_update_id']) || empty($RequestData['project_update_id'])) {
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        } else {
            $ProjectUpdateId = $RequestData['project_update_id'];

            $this->loadModel('ProjectUpdates');
            $ProjectDataMilestone = $this->ProjectUpdates->find('all')->where(['id' => $ProjectUpdateId, 'type' => MILESTONE_TYPE])->first();
            if (!$ProjectDataMilestone) {
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            } else {
                $this->loadModel('Projects');
                $Project = $this->Projects->getProject($ProjectDataMilestone->project_id);
                if (!$Project) {
                    $this->_status = 1;
                    $this->_message = __('Unable to process your request.');
                } else {
                    $UserIdOwner = $Project->user->id;
                    $ProjectId = $Project->id;
                }
            }
        }

        if ($this->_status == 0) {
            if ($UserIdOwner != $iUserId) {
                $this->_status = 1;
                $this->_message = __('You are not owner.');
            }
        }

        $this->_validateFormUpdate($RequestData);
        if ($this->_status == 0) {
            $ProjectUpdates = $this->ProjectUpdates->get($ProjectDataMilestone->id);
            $ProjectUpdates->title = $RequestData['title'];
            $ProjectUpdates->content = $RequestData['description'];
            $ProjectUpdates->status = 1;
            if ($TypeId == 'add') {
                $ProjectUpdates->created = date('Y-m-d H:i:s');
            }

            if ($this->ProjectUpdates->save($ProjectUpdates)) {
                // check and update project complete status
                $listMileStones = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['project_id'=>$ProjectUpdates->project_id,'type' => 2]);
                $listMileStoneNotActive = $this->_getListProjectUpdateByTypeAndStatus($listMileStones, 2, 0);
                if (count($listMileStones) > 0 && count($listMileStoneNotActive) == 0) {
                    $projectTable = TableRegistry::get('Projects');
                    $project = $projectTable->get($ProjectUpdates->project_id);
                    $projectTable->patchEntity($project, ['is_completed' => 1]);
                    $projectTable->save($project);
                }
                if ($TypeId == 'add') {
                    $view = new View($this->request, $this->response, null);
                    $view->set('ProjectUpdate', $ProjectUpdates);
                    $view->set('bOwner', true);
                    $view->layout = false;
                    $this->set('Project', $Project);
                    $this->_data['html_item'] = $view->render('/Element/Mettings/item_update');
                }

                $this->_data['response'] = $ProjectUpdates;
                $ProjectDataMilestone = $this->ProjectUpdates->find('all')->where(['project_id' => $ProjectId, 'status' => 0, 'type' => MILESTONE_TYPE])->first();
                if ($ProjectDataMilestone) {
                    $this->_data['milestone_next'] = $ProjectDataMilestone;
                }
                $this->_message = __('Update successfully!');
                $this->sendNotificationForJoinProject($Project->id, 10,'');
            } else {
                $this->_status = 1;
                $this->_message = __('Update Error');
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }
    
    private function _getListProjectUpdateByTypeAndStatus($list = [],$type = 1,$status = 1){
        $listArr = [];
        foreach ($list as $row){
            if ($row['type'] == $type && $row['status'] == $status){
                $listArr[] = $row;
            }
        }
        return $listArr;
    }

    /**
     * validate form
     * @author Roxannie Nguyen jr
     * @return json
     */
    private function _validateFormUpdate(&$RequestData) {
        $this->loadModel('Projects');
        if ($this->_status == 0) {
            if (!isset($RequestData['title']) || empty($RequestData['title'])) {
                $this->_status = 1;
                $this->_message = __('Title is required. Max length of 60 chars. HTML or script tags is not allowed.');
            } else {
                if (!$this->Projects->customCheckHtml($RequestData['title']) || mb_strlen($RequestData['title']) > 60) {
                    $this->_status = 1;
                    $this->_message = __('Title is required. Max length of 60 chars. HTML or script tags is not allowed.');
                } else {
                    $RequestData['title'] = htmlspecialchars($RequestData['title']);
                }
            }
        }

        if ($this->_status == 0) {
            if (!isset($RequestData['description']) || empty($RequestData['description'])) {
                $this->_status = 1;
                $this->_message = __('Max length of 120 chars. HTML or script tags is not allowed.');
            } else {
                if (!$this->Projects->customCheckHtml($RequestData['description']) || mb_strlen($RequestData['description']) > 120) {
                    $this->_status = 1;
                    $this->_message = __('Max length of 120 chars. HTML or script tags is not allowed.');
                } else {
                    $RequestData['description'] = htmlspecialchars($RequestData['description']);
                }
            }
        }
    }

    /**
     * CHECK INPUT HTML
     * @author Roxannie Nguyen jr
     * @return json
     */
    private function _is_html($string) {
        return preg_match("/<[^<]+/", $string, $m) != 1;
    }

}
