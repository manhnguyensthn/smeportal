<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ValidationCodes Controller
 *
 * @property \App\Model\Table\ValidationCodesTable $ValidationCodes */
class ValidationCodesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $validationCodes = $this->paginate($this->ValidationCodes);

        $this->set(compact('validationCodes'));
        $this->set('_serialize', ['validationCodes']);
    }

    /**
     * View method
     *
     * @param string|null $id Validation Code id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $validationCode = $this->ValidationCodes->get($id, [
            'contain' => []
        ]);

        $this->set('validationCode', $validationCode);
        $this->set('_serialize', ['validationCode']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $validationCode = $this->ValidationCodes->newEntity();
        if ($this->request->is('post')) {
            $validationCode = $this->ValidationCodes->patchEntity($validationCode, $this->request->data);
            if ($this->ValidationCodes->save($validationCode)) {
                $this->Flash->success(__('The validation code has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The validation code could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('validationCode'));
        $this->set('_serialize', ['validationCode']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Validation Code id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $validationCode = $this->ValidationCodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $validationCode = $this->ValidationCodes->patchEntity($validationCode, $this->request->data);
            if ($this->ValidationCodes->save($validationCode)) {
                $this->Flash->success(__('The validation code has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The validation code could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('validationCode'));
        $this->set('_serialize', ['validationCode']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Validation Code id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $validationCode = $this->ValidationCodes->get($id);
        if ($this->ValidationCodes->delete($validationCode)) {
            $this->Flash->success(__('Mã kích hoạt đã xóa.'));
        } else {
            $this->Flash->error(__('The validation code could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
