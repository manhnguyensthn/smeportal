<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersWorks Controller
 *
 * @property \App\Model\Table\UsersWorksTable $UsersWorks
 */
class UsersWorksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $usersWorks = $this->paginate($this->UsersWorks);

        $this->set(compact('usersWorks'));
        $this->set('_serialize', ['usersWorks']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Work id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersWork = $this->UsersWorks->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('usersWork', $usersWork);
        $this->set('_serialize', ['usersWork']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersWork = $this->UsersWorks->newEntity();
        if ($this->request->is('post')) {
            $usersWork = $this->UsersWorks->patchEntity($usersWork, $this->request->data);
            if ($this->UsersWorks->save($usersWork)) {
                $this->Flash->success(__('The users work has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users work could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersWorks->Users->find('list', ['limit' => 200]);
        $this->set(compact('usersWork', 'users'));
        $this->set('_serialize', ['usersWork']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Work id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersWork = $this->UsersWorks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersWork = $this->UsersWorks->patchEntity($usersWork, $this->request->data);
            if ($this->UsersWorks->save($usersWork)) {
                $this->Flash->success(__('The users work has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users work could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersWorks->Users->find('list', ['limit' => 200]);
        $this->set(compact('usersWork', 'users'));
        $this->set('_serialize', ['usersWork']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Work id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersWork = $this->UsersWorks->get($id);
        if ($this->UsersWorks->delete($usersWork)) {
            $this->Flash->success(__('The users work has been deleted.'));
        } else {
            $this->Flash->error(__('The users work could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
