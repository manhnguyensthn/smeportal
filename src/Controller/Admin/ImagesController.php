<?php

namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\Admin\AdminController;

class ImagesController extends AdminController {

    public $paginate = [
        'limit' => 10,
        'order' => [
            'modified' => 'DESC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Images');
    }

    public function index() {
        $images = $this->Images->find('all');
        $this->set([
            'images' => $this->paginate($images),
            '_serialize' => ['images']
        ]);
    }

    public function delete($id) {
        $images= $this->Images->get($id);
        if ($this->Images->delete($images)) {
            $this->Flash->success(__('Mã ảnh : {0} đã bị xóa.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

      public function add() {
        $images = $this->Images->newEntity();
        if ($this->request->is('post')) {
            $error = false;
            $image_path = '';
            if ($this->request->data['url']['error'] == 0) {
                if ($this->request->data['url']['size'] > 1000000) {
                    $this->Flash->error(__('Vui lòng upload ảnh không quá 1M.'));
                    $error = true;
                } else {
                    $fileOK = $this->uploadFiles('img/uploads/images', $this->request->data['url']);
                    $image_path = $fileOK['url'];
                }
            }
            $this->request->data['url'] = '/'.$image_path;
            $images = $this->Images->patchEntity($images, $this->request->data);
            if ($this->Images->save($images)) {
                    $this->Flash->success(__('Thêm ảnh thành công !'));
                    return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Thêm bị lỗi. Vui lòng thử lại.'));
        }
        $this->set('images', $images);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('Ảnh này không tồn tại.'));
            return $this->redirect(['action' => 'index']);
        }
        $images = $this->Images->getImages($id);
        if (empty($images)) {
            $this->Flash->error(__('Ảnh này không tồn tại.'));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['post', 'put'])) {
            $error = false;
            $image_path = '';
            if ($this->request->data['url']['error'] == 0) {
                if ($this->request->data['url']['size'] > 1000000) {
                    $this->Flash->error(__('Vui lòng upload ảnh không quá 1M.'));
                    $error = true;
                } else {
                    $fileOK = $this->uploadFiles('/img/uploads/images', $this->request->data['url']);
                    $image_path = $fileOK['url'];
                }
                $this->request->data['url'] = $image_path;
            }
            $images = $this->Images->patchEntity($images, $this->request->data);
            if ($this->Images->save($images)) {
                $this->Flash->success('role have been updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Không thể update ảnh. Vui lòng thử lại'));
                return $this->redirect($this->referer());
            }
        }
        $this->set([
            'images' => $images,
            '_serialize' => ['images']
        ]);
    }

}
