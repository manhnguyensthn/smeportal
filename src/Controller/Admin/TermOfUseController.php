<?php
/**
 * Created by PhpStorm.
 * User: vietis
 * Date: 7/10/2017
 * Time: 2:48 PM
 *
 */

namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\Admin\AdminController;
class TermOfUseController extends AdminController{
    public function initialize() {
        parent::initialize();
        $this->loadModel('TermOfUse');
    }

    public function index(){
        $termofuse = $this->TermOfUse->find('all');
        $this->set(compact('termofuse'));
    }
    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('About us does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $termofuse = $this->TermOfUse->get($id);
        if (empty($termofuse)) {
            $this->Flash->error(__('About us does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['post', 'put'])) {
            $termofuse = $this->TermOfUse->patchEntity($termofuse, $this->request->data);
            if ($this->TermOfUse->save($termofuse)) {
                $this->Flash->success('About us have been updated.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Could not update your categories. Please try again!'));
                return $this->redirect($this->referer());
            }
        }
        $this->set([
            'termofuse' => $termofuse,
            '_serialize' => ['termofuse']
        ]);
    }
}






?>
