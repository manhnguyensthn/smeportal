<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

class UsersTypeController extends AdminController {

    public $paginate = [
        'limit' => 20,
        'order' => [
            'created' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('UsersType');
    }

    public function index() {
        $userstype = $this->UsersType->find('all');

        $this->set([
            'userstype' => $this->paginate($userstype),
            '_serialize' => ['userstype']
        ]);
    }
}
