<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

class GroupsController extends AdminController {

    public $paginate = [
        'limit' => 20,
        'order' => [
            'created' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Groups');
    }

    public function index() {
        $Groups = $this->Groups->getGroups();

        $this->set([
            'Groups' => $this->paginate($Groups),
            '_serialize' => ['Groups']
        ]);
    }

    public function add() {
        $Groups = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['created'] = date('Y-m-d H:i:s');
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $Groups = $this->Groups->patchEntity($Groups, $this->request->data);
            if ($this->Groups->save($Groups)) {
                $this->Flash->success(__('Groups have been created.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Could not create category. Please try agian!'));
        }
        $this->set('Groups', $Groups);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $Groups = $this->Groups->getGroups($id);
        if (empty($Groups)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $Groups = $this->Groups->patchEntity($Groups, $this->request->data);
            if ($this->Groups->save($Groups)) {
                $this->Flash->success('Groups have been updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Could not update your Groups. Please try again!'));
                return $this->redirect($this->referer());
            }
        }

        $this->set([
            'Groups' => $Groups,
            '_serialize' => ['Groups']
        ]);
    }

    public function delete($id) {
        $this->request->allowMethod(['post', 'delete']);

        $Groups = $this->Groups->get($id);
        if ($this->Groups->delete($Groups)) {
            $this->Flash->success(__('Groups id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }
}
