<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;
use Cake\ORM\TableRegistry;
class UsersController extends AdminController {

    public $paginate = [
        'limit' => 10,

    ];

    public function initialize() {
        parent::initialize();

        $this->loadModel('Administrators');
        $this->loadModel('Users');
        // $this->loadModel('Users');
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);

        $this->Auth->config('authenticate', [
            'Form' => ['userModel' => 'Administrators']
        ]);
    }

    public function index() {
        $roles = array();
        $conditions = array();
        if(isset($this->request->query['username']) && !empty($this->request->query['username'])) {
            $username = $this->request->query['username'];
            $conditions['name like'] = '%'.$username.'%';
        }
        if(isset($this->request->query['email']) && !empty($this->request->query['email'])) {
            $email = $this->request->query['email'];
            $conditions['email like'] = '%'.$email.'%';
        }
        $users = $this->Users->find('all', [
            'fields' => ['id', 'name', 'avatar', 'country_id', 'district_id', 'state_id', 'phone_work', 'email', 'created', 'modified','admin'],
            'contain' => ['Roles']
        ])->where($conditions);
        if (!empty($users)) {
            foreach ($users as $user) {
                $state = $this->Users->States->find('list', ['fields' => ['name'],
                            'conditions' => ['id' => $user ? $user->state_id : 0],
                            'keyField' => 'id', 'valueField' => 'name'
                        ])->first();

                $country = $this->Users->Countries->find('list', ['fields' => ['country_name'],
                            'conditions' => ['id' => $user ? $user->country_id : 0],
                            'keyField' => 'id', 'valueField' => 'country_name'])->first();

                $users->address = (!empty($state) ? $state : '') . (!empty($state) && !empty($country) ? ', ' : '')
                        . (!empty($country) ? $country : '');

                foreach ($user->roles as $role) {
                    $roles[] = $role->role;
                }
                $users->role = implode(", ", $roles);
            }
        }
        $this->set([
            'users' => $this->paginate($users),
            '_serialize' => ['users']
        ]);
    }

    public function login() {
        $this->viewBuilder()->layout('admin_login');

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Username or Password is not correct. Please try again.'), ['key' => 'auth']);
        }
    }

    public function add() {
        $this->viewBuilder()->layout('admin_login');
        $admins = $this->Administrators->newEntity();
        if ($this->request->is('post')) {
            $admins = $this->Administrators->patchEntity($admins, $this->request->data);
            if ($this->Administrators->save($admins)) {
                $this->Flash->success('The admin has been saved.');
                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The admin could not be saved. Please, try again.'));
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function delete($id) {
        //$this->request->allowMethod(['post', 'delete']);		
		$ProjectsTable = TableRegistry::get('Projects');
		$UserProjectsTable = TableRegistry::get('UsersProjects');
		$Projects= $ProjectsTable->find('all', ['conditions' => ['user_id' => $id]])->toArray();
		$ProjectsCommentsUTable = TableRegistry::get('ProjectComments');
		$ProjectsCommentsU = $ProjectsCommentsUTable->find('all', ['conditions' =>['user_id' => $id]])->toArray();
		$UsersWebsitesTable = TableRegistry::get('UsersWebsites');
		$UsersWebsites = $UsersWebsitesTable->find('all', ['conditions' =>['user_id' => $id]])->toArray();
		$UsersWorksTable = TableRegistry::get('UsersWorks');
		$UsersWorks = $UsersWorksTable->find('all', ['conditions' =>['user_id' => $id]])->toArray();
		$NotificationsTable = TableRegistry::get('Notifications');
		$Notifications = $NotificationsTable->find('all', ['conditions' => ['user_id' => $id]])->toArray();
		$NotificationsActions = $NotificationsTable->find('all', ['conditions' => ['action_user_id' => $id]])->toArray();
		$ChatNotificationsTable = TableRegistry::get('ChatNotifications');
		$ChatNotifications = $ChatNotificationsTable->find('all', ['conditions' => ['sender_id' => $id]])->toArray();
		$ChatNotificationsActions = $ChatNotificationsTable->find('all', ['conditions' => ['receive_id' => $id]])->toArray();
		$ChatRoomMessagesTable = TableRegistry::get('ChatNotifications');
		$UserProjects = $UserProjectsTable->find('all', ['conditions' => ['user_id' => $id]])->toArray();
		$ChatNotifications = $ChatNotificationsTable->find('all', ['conditions' => ['sender_id' => $id]])->toArray();
		$UsersRolesTable = TableRegistry::get('UsersRoles');
		$UserRoles = $UsersRolesTable->find('all', ['conditions' => ['user_id' => $id]])->toArray();
		$UsersProjectsActionsTable = TableRegistry::get('UserProjectActions');
		$UsersProjectsActions = $UsersProjectsActionsTable->find('all', ['conditions' => ['user_id' => $id]])->toArray();
		foreach($UsersProjectsActions as $UPA)
		{
			$UsersProjectsActionsTable->delete($UPA);
		}
		foreach($UserRoles as $UR)
		{
			$UsersRolesTable->delete($UR);
		}
		foreach($UserProjects as $UP)
		{
			$UserProjectsTable->delete($UP);
		}
		foreach($ChatNotifications as $CN)
		{
			$ChatNotificationsTable->delete($CN);
		}
		foreach($ChatNotificationsActions as $CNA)
		{
			$ChatNotificationsTable->delete($CNA);
		}
		foreach($Notifications as $N)
		{
			$NotificationsTable->delete($N);
		}
		foreach($NotificationsActions as $NA)
		{
			$NotificationsTable->delete($NA);
		}
		foreach($ProjectsCommentsU as $PCU)
		{
			$ProjectsCommentsUTable->delete($PCU);
		}
		foreach($UsersWebsites as $UW)
		{
			$UsersWebsitesTable->delete($UW);
		}
		foreach($UsersWorks as $UW)
		{
			$UsersWorksTable->delete($UW);
		}
		foreach($Projects as $P)
		{
			$ProjectsRolesTable = TableRegistry::get('ProjectsRoles');
			$ProjectsRoles= $ProjectsRolesTable->find('all', ['conditions' => ['project_id' => $P->id]])->toArray();
			$ProjectsUpdatesTable = TableRegistry::get('ProjectUpdates');
			$ProjectsUpdates= $ProjectsUpdatesTable->find('all', ['conditions' => ['project_id' => $P->id]])->toArray();
			$UsersProjectTable = TableRegistry::get('UsersProjects');
			$UsersProject= $UsersProjectTable->find('all', ['conditions' => ['project_id' => $P->id]])->toArray();
			$ProjectsCommentsTable = TableRegistry::get('ProjectComments');
			$ProjectsComments = $ProjectsCommentsTable->find('all', ['conditions' =>['project_id' => $P->id]])->toArray();	
			foreach($ProjectsRoles as $PR)
			{
				$ProjectsRolesTable->delete($PR);
			}
			foreach($ProjectsUpdates as $PU)
			{
				$ProjectsUpdatesTable->delete($PU);
			}
			foreach($UsersProject as $UP)
			{
				$UsersProjectTable->delete($UP);
			}
			foreach($ProjectsComments as $PC)
			{
				$ProjectsCommentsTable->delete($PC);
			}
		
	
		}
		foreach($Projects as $P)
		{
			$ProjectsTable->delete($P);
		}
		$SearchedHistoryTable = TableRegistry::get('SearchedHistory');
		$SearchedHistory = $SearchedHistoryTable->find('all', ['conditions' =>['user_id' => $id]])->toArray();
		foreach($SearchedHistory as $SH)
		{
			$SearchedHistoryTable->delete($SH);
		}	
		$TokensTable = TableRegistry::get('Tokens');
		$Tokens = $TokensTable->find('all', ['conditions' =>['user_id' => $id]])->toArray();
		foreach($Tokens as $T)
		{
			$TokensTable->delete($T);
		}	
		
        $projects = $this->Users->get($id);
        if ($this->Users->delete($projects)) {
            $this->Flash->success(__('user id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function  toBeAdmin(){
        if($this->request->is('post')){
            $data['id'] = $this->request->data['user_id'];
            $user = $this->Users->get($data['id']);
            $data['admin'] = $this->request->data['group'];
            $user = $this->Users->patchEntity($user, $data);
            if($this->Users->save($user)){
                $this->Flash->success(__("Đã ủy quyền !"));
                $this->redirect($this->referer());
            }else{
                $this->redirect($this->referer());
                $this->Flash->error(__("Không thể ủy quyền !"));
            }
        }
    }
}
