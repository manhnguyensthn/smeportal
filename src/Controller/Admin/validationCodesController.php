<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

/**
 * ValidationCodes Controller
 *
 * @property \App\Model\Table\ValidationCodesTable $ValidationCodes */
class ValidationCodesController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public $paginate = [
        'limit' => 30,
        'order' => [
            'status' => 'DESC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('ValidationCodes');
    }
    public function index()
    {
        $codes = $this->ValidationCodes->find('all');
        $this->set([
            'codes' => $this->paginate($codes),
            '_serialize' => ['codes']
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id Validation Code id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $validationCode = $this->ValidationCodes->get($id, [
            'contain' => []
        ]);

        $this->set('validationCode', $validationCode);
        $this->set('_serialize', ['validationCode']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $validationCode = $this->ValidationCodes->newEntity();
        if ($this->request->is('post')) {
            $validationCode = $this->ValidationCodes->patchEntity($validationCode, $this->request->data);
            if ($this->ValidationCodes->save($validationCode)) {
                $this->Flash->success(__('The validation code has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The validation code could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('validationCode'));
        $this->set('_serialize', ['validationCode']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Validation Code id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $validationCode = $this->ValidationCodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $validationCode = $this->ValidationCodes->patchEntity($validationCode, $this->request->data);
            if ($this->ValidationCodes->save($validationCode)) {
                $this->Flash->success(__('The validation code has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The validation code could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('validationCode'));
        $this->set('_serialize', ['validationCode']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Validation Code id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $validationCode = $this->ValidationCodes->get($id);
        if ($this->ValidationCodes->delete($validationCode)) {
            $this->Flash->success(__('The validation code has been deleted.'));
        } else {
            $this->Flash->error(__('The validation code could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function toBeFeature($id = null) {
        $this->autoRender = false;
        $codes = $this->ValidationCodes->get($id);
        if (!empty($codes)) {
            if($codes->status == 0){
                $data = ['status' => 1];
                $message = sprintf(__('The %s code is ready to use.'), strtoupper($codes->code));
            }elseif($codes->status == 1){
                $data = ['status' => 0];
                $message = sprintf(__('The %s can not be used now.'), strtoupper($codes->code));
            }
            $codes = $this->ValidationCodes->patchEntity($codes, $data);
            if ($this->ValidationCodes->save($codes)) {
                $this->Flash->success($message);
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->success(__('Could not execute your request at this time. Please try again later.'));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $this->Flash->error(__('Could not execute your request at this time. Please try again later.'));
            return $this->redirect(['action' => 'index']);
        }
    }
}
