<?php
/**
 * Created by PhpStorm.
 * User: vietis
 * Date: 7/10/2017
 * Time: 2:48 PM
 *
 */

namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\Admin\AdminController;
class AboutUsController extends AdminController{
    public function initialize() {
        parent::initialize();
        $this->loadModel('AboutUs');
    }

    public function index(){
        $aboutus = $this->AboutUs->find('all');
        $this->set(compact('aboutus'));
    }
    public function  view(){
        $aboutus = $this->AboutUs->find('all');
        $this->set(compact('aboutus'));
        $this->render('../Pages/about');
    }
    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('About us does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $aboutus = $this->AboutUs->get($id);
        if (empty($aboutus)) {
            $this->Flash->error(__('About us does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['post', 'put'])) {
            $aboutus = $this->AboutUs->patchEntity($aboutus, $this->request->data);
            if ($this->AboutUs->save($aboutus)) {
                $this->Flash->success('About us have been updated.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Could not update your categories. Please try again!'));
                return $this->redirect($this->referer());
            }
        }
        $this->set([
            'aboutus' => $aboutus,
            '_serialize' => ['aboutus']
        ]);
    }
}






?>
