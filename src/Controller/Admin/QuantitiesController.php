<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

class QuantitiesController extends AdminController {

    public $paginate = [
        'limit' => 20,
        'order' => [
            'created' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Quantities');
    }

    public function index() {
        $Quantities = $this->Quantities->getQuantities();

        $this->set([
            'Quantities' => $this->paginate($Quantities),
            '_serialize' => ['Quantities']
        ]);
    }

    public function add() {
        $Quantities = $this->Quantities->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['created'] = date('Y-m-d H:i:s');
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $Quantities = $this->Quantities->patchEntity($Quantities, $this->request->data);
            if ($this->Quantities->save($Quantities)) {
                $this->Flash->success(__('Quantities have been created.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Could not create category. Please try agian!'));
        }
        $this->set('Quantities', $Quantities);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $Quantities = $this->Quantities->getQuantities($id);
        if (empty($Quantities)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $Quantities = $this->Quantities->patchEntity($Quantities, $this->request->data);
            if ($this->Quantities->save($Quantities)) {
                $this->Flash->success('Quantities have been updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Could not update your Quantities. Please try again!'));
                return $this->redirect($this->referer());
            }
        }

        $this->set([
            'Quantities' => $Quantities,
            '_serialize' => ['Quantities']
        ]);
    }

    public function delete($id) {
        $this->request->allowMethod(['post', 'delete']);

        $Quantities = $this->Quantities->get($id);
        if ($this->Quantities->delete($Quantities)) {
            $this->Flash->success(__('Quantities id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }
}
