<?php
/**
 * Created by PhpStorm.
 * User: vietis
 * Date: 7/10/2017
 * Time: 2:48 PM
 *
 */

namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\Admin\AdminController;
class QuestionsController extends AdminController{
    public function initialize() {
        parent::initialize();
        $this->loadModel('Questions');
    }

    public function index(){
        $questions = $this->Questions->find('all');
        $this->set(compact('questions'));
    }
    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('About us does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $questions = $this->Questions->get($id);
        if (empty($questions)) {
            $this->Flash->error(__('About us does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['post', 'put'])) {
            $questions = $this->Questions->patchEntity($questions, $this->request->data);
            if ($this->Questions->save($questions)) {
                $this->Flash->success('About us have been updated.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Could not update your categories. Please try again!'));
                return $this->redirect($this->referer());
            }
        }
        $this->set([
            'questions' => $questions,
            '_serialize' => ['questions']
        ]);
    }
}






?>
