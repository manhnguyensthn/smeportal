<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

class ScalesController extends AdminController {

    public $paginate = [
        'limit' => 20,
        'order' => [
            'created' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Scales');
    }

    public function index() {
        $Scales = $this->Scales->getScales();

        $this->set([
            'Scales' => $this->paginate($Scales),
            '_serialize' => ['Scales']
        ]);
    }

    public function add() {
        $Scales = $this->Scales->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['created'] = date('Y-m-d H:i:s');
            $this->request->data['modified'] = date('Y-m-d H:i:s');
            $Scales = $this->Scales->patchEntity($Scales, $this->request->data);
            if ($this->Scales->save($Scales)) {
                $this->Flash->success(__('Scales have been created.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Could not create category. Please try agian!'));
        }
        $this->set('Scales', $Scales);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $Scales = $this->Scales->getScales($id);
        if (empty($Scales)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $Scales = $this->Scales->patchEntity($Scales, $this->request->data);
            if ($this->Scales->save($Scales)) {
                $this->Flash->success('Scales have been updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Could not update your Scales. Please try again!'));
                return $this->redirect($this->referer());
            }
        }

        $this->set([
            'Scales' => $Scales,
            '_serialize' => ['Scales']
        ]);
    }

    public function delete($id) {
        $this->request->allowMethod(['post', 'delete']);

        $Scales = $this->Scales->get($id);
        if ($this->Scales->delete($Scales)) {
            $this->Flash->success(__('Scales id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }
}
