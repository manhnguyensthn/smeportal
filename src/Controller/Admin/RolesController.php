<?php

namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\Admin\AdminController;

class RolesController extends AdminController {

    public $paginate = [
        'limit' => 10,
        'order' => [
            'modified' => 'DESC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Roles');
    }

    public function index() {
        $roles = $this->Roles->find('all');
        $this->set([
            'roles' => $this->paginate($roles),
            '_serialize' => ['roles']
        ]);
    }

    public function delete($id) {
       		
                $roles= $this->Roles->get($id);
		$UsersRolesTable = TableRegistry::get('UsersRoles');
		$UsersProjectsTable = TableRegistry::get('UsersProjects');
		$ProjectRolesTable = TableRegistry::get('ProjectRoles');
		$UsersRoles = $UsersRolesTable->find('all', ['conditions' => ['role_id' => $id]])->toArray();
		$UsersProjects = $UsersProjectsTable->find('all', ['conditions' => ['role_id' => $id]])->toArray();
		$ProjectRoles = $ProjectRolesTable->find('all', ['conditions' => ['role_id' => $id]])->toArray();
		foreach($UsersRoles as $UR)
		{
		 $UsersRolesTable->delete($UR);
		}
		foreach($ProjectRoles as $PR)
		{
			$ProjectsRolesTable->delete($PR);
		}
		foreach($UsersProjects as $UP)
		{
			$UsersProjectsTable->delete($UP);
		}
        if ($this->Roles->delete($roles)) {
            $this->Flash->success(__('Role id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

      public function add() {
        $roles = $this->Roles->newEntity();
        if ($this->request->is('post')) {
            $error = false;
            $image_path = '';
            if ($this->request->data['icon']['error'] == 0) {
                if ($this->request->data['icon']['size'] > 8388608) {
                    $this->Flash->error(__('Please select file upload < 8M.'));
                    $error = true;
                } else {
                    $fileOK = $this->uploadFiles('img/role_icons', $this->request->data['icon']);
                    $image_path = $fileOK['url'];
                }
            }
            $this->request->data['quatity'] = (int)1;
            $this->request->data['lang_id'] = (int)1;
            $this->request->data['role_type'] = (int)1;
            $this->request->data['icon'] = '/'.$image_path;
            $roles = $this->Roles->patchEntity($roles, $this->request->data);
            if ($this->Roles->save($roles)) {
                    $this->Flash->success(__('Add Role successfully!'));
                    return $this->redirect(['action' => 'index']);
            }
            
            $this->Flash->error(__('Add failed, please try again.'));
        }
        $this->set('roles', $roles);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('The role does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $roles = $this->Roles->getRoles($id);
        if (empty($roles)) {
            $this->Flash->error(__('The role does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['post', 'put'])) {
            $error = false;
            $image_path = '';
            if ($this->request->data['icon']['error'] == 0) {
                if ($this->request->data['icon']['size'] > 8388608) {
                    $this->Flash->error(__('Please select file upload < 8M.'));
                    $error = true;
                } else {
                    $fileOK = $this->uploadFiles('/img/role_icons', $this->request->data['icon']);
                    $image_path = $fileOK['url'];
                }
                $this->request->data['icon'] = $image_path;
            }
            $roles = $this->Roles->patchEntity($roles, $this->request->data);
            if ($this->Roles->save($roles)) {
                $this->Flash->success('role have been updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Could not update your categories. Please try again!'));
                return $this->redirect($this->referer());
            }
        }
        $this->set([
            'roles' => $roles,
            '_serialize' => ['roles']
        ]);
    }

}
