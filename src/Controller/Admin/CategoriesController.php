<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

class CategoriesController extends AdminController {

    public $paginate = [
        'limit' => 20,
        'order' => [
            'created' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Categories');
    }

    public function index() {
        $conditions = array();
        if(isset($this->request->query['title']) && !empty($this->request->query['title'])) {
            $title = $this->request->query['title'];
            $conditions['name like'] = '%'.$title.'%';
        }
        $categories = $this->Categories->find('all')->where($conditions);
        $this->set([
            'categories' => $this->paginate($categories),
            '_serialize' => ['categories']
        ]);
    }

    public function add() {
        $categories = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['created'] = date('Y-m-d H:i:s');
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $categories = $this->Categories->patchEntity($categories, $this->request->data);
            if ($this->Categories->save($categories)) {
                $this->Flash->success(__('Categories have been created.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Could not create category. Please try agian!'));
        }
        $this->set('categories', $categories);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }
        $categories = $this->Categories->getCategories($id);
        if (empty($categories)) {
            $this->Flash->error(__('The category does not exist.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['modified'] = date('Y-m-d H:i:s');

            $categories = $this->Categories->patchEntity($categories, $this->request->data);
            if ($this->Categories->save($categories)) {
                $this->Flash->success('Categories have been updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Could not update your categories. Please try again!'));
                return $this->redirect($this->referer());
            }
        }

        $this->set([
            'categories' => $categories,
            '_serialize' => ['categories']
        ]);
    }

    public function delete($id) {
        $this->request->allowMethod(['post', 'delete']);

        $categories = $this->Categories->get($id);
        if ($this->Categories->delete($categories)) {
            $this->Flash->success(__('Categories id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }
}
