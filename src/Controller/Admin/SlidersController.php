<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;

class SlidersController extends AdminController {

    public $paginate = [
        'limit' => 5,
        'order' => [
            'created' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Sliders');
    }

    public function index() {
        $sliders = $this->Sliders->getSliders();

        $this->set([
            'sliders' => $this->paginate($sliders),
            '_serialize' => ['sliders']
        ]);
    }

    public function add() {
        $sliders = $this->Sliders->newEntity();
        if ($this->request->is('post')) {
            $error = false;
            $image_path = '';

            if ($this->request->data['image_path']['error'] == 0) {
                if ($this->request->data['image_path']['size'] > 8388608) {
                    $this->Flash->error(__('Please select file upload < 8M.'));
                    $error = true;
                } else {
                    $fileOK = $this->uploadFiles('img/uploads/sliders', $this->request->data['image_path']);
                    $image_path = $fileOK['url'];
                }
            }
            if (!$error) {
                $this->request->data['created'] = date('Y-m-d H:i:s');
                $this->request->data['updated'] = date('Y-m-d H:i:s');
                $this->request->data['image_path'] = $image_path;

                $sliders = $this->Sliders->patchEntity($sliders, $this->request->data);
                if ($this->Sliders->save($sliders)) {
                    $this->Flash->success(__('Add Slider successfully!'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('Add failed, please try again.'));
        }
        $this->set('sliders', $sliders);
    }

    public function edit($id = null) {
        if (empty($id)) {
            $this->Flash->error(__('Load sliders failed, please try again.'));
            return $this->redirect(['action' => 'index']);
        }
        $sliders = $this->Sliders->getSliders($id);
        if (empty($sliders)) {
            $this->Flash->error(__('Load sliders failed, please try again.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['post', 'put'])) {
            $error = false;
            $image_path = '';
            if ($this->request->data['image_path']['error'] == 0) {
                if ($this->request->data['image_path']['size'] > 8388608) {
                    $this->Flash->error(__('Please select file upload < 8M.'));
                    $error = true;
                } else {
                    $fileOK = $this->uploadFiles('img/uploads/sliders', $this->request->data['image_path']);
                    $image_path = $fileOK['url'];
                }
            }
            $this->request->data['updated'] = date('Y-m-d H:i:s');
            $this->request->data['image_path'] = $image_path;
            
            $sliders = $this->Sliders->patchEntity($sliders, $this->request->data);
            if ($this->Sliders->save($sliders)) {
                $this->Flash->success(__('Update Slider successfully!'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Update failed, please try again.'));
                return $this->redirect($this->referer());
            }
        }

        $this->set([
            'sliders' => $sliders,
            '_serialize' => ['sliders']
        ]);
    }

    public function delete($id) {
        $this->request->allowMethod(['post', 'delete']);

        $sliders = $this->Sliders->get($id);
        if ($this->Sliders->delete($sliders)) {
            $this->Flash->success(__('sliders id: {0} Deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

}
