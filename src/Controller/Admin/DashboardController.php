<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;
use Cake\ORM\TableRegistry;

class DashboardController extends AdminController {

    public function initialize() {
        parent::initialize();
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
    }

    public function index() {
    	$validationtable = TableRegistry::get('Validation_codes');
    	$total = $validationtable->find('all');
    	$total = $total->count();
    	// echo $total;
    	$this->set(compact('total'));
        
    }
    public function generate($id = null){
        $validationtable = TableRegistry::get('Validation_codes');
        for($i = 0; $i<500; $i++){
        $validation = $validationtable->newEntity();

        $key = md5(uniqid());
        $validation->code = $key;

        if ($validationtable->save($validation)) {
            // The $article entity contains the id now
        }
        }
        $this->setAction('index');
    }
}
