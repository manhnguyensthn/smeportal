<?php

namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;
use App\Controller\Admin\AdminController;

class ProjectsController extends AdminController {

    public $paginate = [
        'limit' => 10,
        'order' => [
            // 'modified' => 'ASC',
            'id' => 'ASC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        //load paginate component
        $this->loadComponent('Paginator');
        //load Model
        $this->loadModel('Projects');
    }

    public function index() {
        $GroupTable = TableRegistry::get('Groups');
        $conditions = array();
        if(isset($this->request->query['title']) && !empty($this->request->query['title'])) {
            $title = $this->request->query['title'];
            $conditions['title like'] = '%'.$title.'%';
        }
        $groups = $GroupTable->find('list', ['limit' => 200,'field'=>'name'])->order(['name' => 'DESC']);
        $projects = $this->Projects->find('all',['contain'=>['Groups']])->where($conditions);
        $this->set(compact('groups'));
        $this->set([
            'projects' => $this->paginate($projects),
            '_serialize' => ['projects']
        ]);      
    }

    public function delete($id) { 		
        $projects = $this->Projects->get($id);
		$UsersProjectsTable = TableRegistry::get('UsersProjects');
		$ProjectsUpdatesTable = TableRegistry::get('ProjectUpdates');
		$ProjectsRolesTable = TableRegistry::get('ProjectRoles');
		$ProjectsCommentsTable = TableRegistry::get('ProjectComments');
		$NotificationsTable = TableRegistry::get('Notifications');
		$ChatRoomsTable = TableRegistry::get('ChatRooms');
		$ProjectsComments = $ProjectsCommentsTable->find('all', ['conditions' => ['project_id' => $id]])->toArray();
		$ProjectRoles = $ProjectsRolesTable->find('all', ['conditions' => ['project_id' => $id]])->toArray();
		$UsersProjects = $UsersProjectsTable->find('all', ['conditions' => ['project_id' => $id]])->toArray();
		$ProjectsUpdates = $ProjectsUpdatesTable->find('all', ['conditions' => ['project_id' => $id]])->toArray();
		$Notifications = $NotificationsTable->find('all', ['conditions' => ['project_id' => $id]])->toArray();
		$ChatRooms = $ChatRoomsTable->find('all', ['conditions' => ['project_id' => $id]])->toArray();
		foreach($ProjectsComments as $PC)
		{
			$ProjectsCommentsTable->delete($PC);
		}
		foreach($ChatRooms as $CR)
		{
			$ChatRoomsTable->delete($CR);
		}
		foreach($Notifications as $N)
		{
			$NotificationsTable->delete($N);
		}
		foreach($ProjectRoles as $PR)
		{
			$ProjectsRolesTable->delete($PR);
		}
		foreach($ProjectsUpdates as $PU)
		{
			$ProjectsUpdatesTable->delete($PU);
		}
		foreach($UsersProjects as $UP)
		{
			$UsersProjectsTable->delete($UP);
		}
        if ($this->Projects->delete($projects)) {
            $this->Flash->success(__('projects id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function toBeFeature($id = null) {
        $this->autoRender = false;
        $projects = $this->Projects->get($id);
        if (!empty($projects)) {
            if($projects->status == 1){
                $data = ['status' => 2];
                $message = sprintf(__('The %s project become to feature project.'), strtoupper($projects->title));
            }elseif($projects->status == 2){
                $data = ['status' => 1];
                $message = sprintf(__('You have removed the %s feature project.'), strtoupper($projects->title));
            }
            $projects = $this->Projects->patchEntity($projects, $data);
            if ($this->Projects->save($projects)) {
                $this->Flash->success($message);
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->success(__('Could not execute your request at this time. Please try again later.'));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $this->Flash->error(__('Could not execute your request at this time. Please try again later.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    // 2017-6-26
    public function toBeFee($id = null){
        $this->autoRender = false;
        $projects = $this->Projects->get($id);
        if (!empty($projects)) {
            if($projects->fee_status == 0){
                $data['fee_status'] = 1;
                $message = sprintf(__('Đã cập nhật hội phí cho doanh nghiệp %s.'), strtoupper($projects->title));
                // $message = sprintf(__('The %s project become to feature project.'), strtoupper($projects->title));
            }elseif($projects->fee_status == 1){
                $data['fee_status'] = 0;
                $message = sprintf(__('Đã hủy hội phí của doanh nghiệp %s.'), strtoupper($projects->title));
                // $message = sprintf(__('You have removed the %s feature project.'), strtoupper($projects->title));
            }
            $projects = $this->Projects->patchEntity($projects, $data);
            if ($this->Projects->save($projects)) {
                $this->Flash->success($message);
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->success(__('Could not execute your request at this time. Please try again later.'));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $this->Flash->error(__('Could not execute your request at this time. Please try again later.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function toBeStaff($id = null){
        $this->autoRender = false;
        $projects = $this->Projects->get($id);
        if (!empty($projects)) {
            if($projects->group_id == 2){
                $data['group_id'] = 0;
                $message = sprintf(__('Doanh nghiệp %s đã được thăng cấp phó chủ tịch.'), strtoupper($projects->title));
            }elseif($projects->group_id == 0){
                $data['group_id'] = 2;
                $message = sprintf(__('Đã hủy quyền phó chủ tịch của doanh nghiệp %s.'), strtoupper($projects->title));
            }
            $projects = $this->Projects->patchEntity($projects, $data);
            if ($this->Projects->save($projects)) {
                $this->Flash->success($message);
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->success(__('Could not execute your request at this time. Please try again later.'));
                return $this->redirect(['action' => 'index']);
            }
        } else {
            $this->Flash->error(__('Could not execute your request at this time. Please try again later.'));
            return $this->redirect(['action' => 'index']);
        }
    }

}
