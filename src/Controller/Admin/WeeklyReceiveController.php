<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminController;
use Cake\ORM\TableRegistry;

class WeeklyReceiveController extends AdminController {

    public function initialize() {
        parent::initialize();
        
        $this->loadComponent('Email');
        //load Model
        $this->loadModel('WeeklyReceive');
    }

    /**
     * get List Template mail
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function index(){
        $RequestQuery = $this->request->query;
        if(isset($RequestQuery['delete']) && !empty($RequestQuery['delete'])){
            $iDelete = $RequestQuery['delete'];
            $WeeklyReceive = $this->WeeklyReceive->get($iDelete);
            if($this->WeeklyReceive->delete($WeeklyReceive)){
                $this->Flash->success(__('Your data has been deleted.'));
            }else{
                $this->Flash->error(__('Oops.Could not be delete your data.'));
            }
            return $this->redirect($this->referer());
        }

        $WeeklyReceive = $this->WeeklyReceive->find()->toArray();
        $this->set([
            'WeeklyReceives' => $WeeklyReceive,
        ]);
    }

    /**
     * Controller Edit mail
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function edit($id = null) {
        $bEdit = false;
        $weeklyReceive = (object)[];
        $id = (int)$id;
        if($id){
            $WeeklyReceiveTable = TableRegistry::get('WeeklyReceive');
            $weeklyReceive = $WeeklyReceiveTable->getWeeklyReceive($id);
            if(!$weeklyReceive){
                $this->Flash->error(__('Your pages request does not exist.'));
                return $this->redirect(['controller' => 'weekly-receive', 'action' => 'edit']);
            }
            $bEdit = true;
        }

        if ($this->request->is(['post', 'put'])) {
            $aVals = $this->request->data;
            if(!isset($aVals['title']) || empty($aVals['title'])){
                $this->Flash->error(__('Title is required'));
            }

            if(!isset($aVals['content']) || empty($aVals['content'])){
                $this->Flash->error(__('Content is required'));
            }

            if($bEdit){
                $aVals['updated'] = date('Y-m-d H:i:s');
                $weeklyReceive = $this->WeeklyReceive->patchEntity($weeklyReceive, $aVals);
                if ($this->WeeklyReceive->save($weeklyReceive)) {
                    $this->Flash->success(__('Your data has been saved.'));
                } else {
                    $this->Flash->error(__('Oops.Could not be save your data.'));
                }
                return $this->redirect($this->referer());
            }else{
                $WeeklyReceive = $this->WeeklyReceive->newEntity();
                $WeeklyReceive->title   = $aVals['title'];
                $WeeklyReceive->content = $aVals['content'];
                $WeeklyReceive->created = date('Y-m-d H:i:s');
                if ($this->WeeklyReceive->save($WeeklyReceive)) {
                    $this->Flash->success(__('Your data has been saved.'));
                    return $this->redirect(['controller' => 'weekly-receive', 'action' => 'edit', $WeeklyReceive->id]);
                }else{
                    $this->Flash->error(__('Oops.Could not be insert your data.'));
                    return $this->redirect($this->referer());
                }
            }
            
        }

        $this->set([
            'weeklyReceive' => $weeklyReceive,
            '_serialize'    => ['weeklyReceive'],
            'bEdit'         => $bEdit,
        ]);
    }
}
