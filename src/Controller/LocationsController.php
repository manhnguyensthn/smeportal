<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Districts Controller
 *
 * @property \App\Model\Table\DistrictsTable $Districts
 */
class LocationsController extends AppController
{
	public $paginate = array();
    public $helpers = array('Paginator');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

	public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['updateLocation', 'updateLatLng']);
    }

	/**
     * update Location
     * @author Roxannie Nguyen jr
     * @return array
     */
	public function updateLocation(){
		if ($this->request->is('post') && $this->request->isAjax()) {
			$aRequestData    = $this->request->data;
			$aRequestSession = $this->request->session();
			if(isset($aRequestData['position']['coords']['latitude']) && isset($aRequestData['position']['coords']['longitude'])){
				$this->_data = array(
					'latitude'  => $aRequestData['position']['coords']['latitude'],
					'longitude' => $aRequestData['position']['coords']['longitude'],
				);
				$aRequestSession->write('Config.location', $this->_data);
				$this->_status  = 1;
				$this->_message = __("Update Location to Session successfull");
			}
		}
		$this->responApi($this->_status, $this->_message, $this->_data);
		die();
	}
	
	/**
     * update lat lng countries, districts, states
     * @author Roxannie Nguyen jr
     * @return array
     */

	public function updateLatLng(){
		$this->loadModel('Countries');
		$this->loadModel('States');
		$this->loadModel('Districts');
		$iPageSize = 1;
        $aParamPaginate = array(
			'limit'    => $iPageSize,
			'maxLimit' => $iPageSize,
			'order'    => ['Countries.id' => 'ASC'],
        );

        $aToken = array(
        	'AIzaSyCVNvGYwFKsfbshoDuBNn2tBYSkBPZdCc4',
        );
        $sLinkDefault  = 'https://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false&key=%s';

		$this->paginate = $aParamPaginate;
		$aCountries = $this->paginate($this->Countries);
		// pr($aCountries); die();
		if($aCountries){
			foreach ($aCountries as $key => $aCountry) {
				if(isset($aCountry->id) && !empty($aCountry->id)){
					// Update Countries
					$address = str_replace(' ','+', $aCountry->country_name);

					if(isset($aCountry->lat) && $aCountry->lat == 0 && $address){
						$token = $aToken[array_rand($aToken)];
						$sLink  = sprintf($sLinkDefault, $address, $token);
						$result = file_get_contents($sLink);
						$output = json_decode($result, true);
						if(isset($output['status']) && $output['status'] == 'OVER_QUERY_LIMIT'){
							echo 'OVER_QUERY_LIMIT';
							die();
						}
						echo 'Country: ' . $sLink . '<br>';
						pr($output);
						echo '<br>';
						if(isset($output['results'][0]['geometry']['location']['lat']) && isset($output['results'][0]['geometry']['location']['lng'])){
							$latitude = $output['results'][0]['geometry']['location']['lat'];
							$longitude = $output['results'][0]['geometry']['location']['lng'];

							$row = $this->Countries->get($aCountry->id);
							$row->lat = $latitude;
							$row->lng = $longitude;
							$this->Countries->save($row);
						}
					}
					// END: Update Countries


					$aStates = $this->States->find('all', array(
		                'conditions' => array(
		                	'country_id' => $aCountry->id,
		                ),
		            ))->toArray();

		            if(isset($aStates) && !empty($aStates)){
		            	foreach ($aStates as $keyState => $aState) {
		            		// $addressState = str_replace(' ','+', $aState->name) . ',+' . $address;
		            		$addressState = str_replace(' ','+', $aState->name);
		            		if(isset($aState->lat) && $aState->lat == 0){
		            			// Update States
								$token = $aToken[array_rand($aToken)];
								$sLinkState  = sprintf($sLinkDefault, $addressState, $token);
								$result = file_get_contents($sLinkState);
								$output = json_decode($result, true);
								if(isset($output['status']) && $output['status'] == 'OVER_QUERY_LIMIT'){
									echo 'OVER_QUERY_LIMIT';
									die();
								}
								echo 'States: ' . $sLinkState . '<br>';
								pr($output);
								echo '<br>';
								if(isset($output['results'][0]['geometry']['location']['lat']) && isset($output['results'][0]['geometry']['location']['lng'])){
									$latitude = $output['results'][0]['geometry']['location']['lat'];
									$longitude = $output['results'][0]['geometry']['location']['lng'];

									$row = $this->States->get($aState->id);
									$row->lat = $latitude;
									$row->lng = $longitude;
									$this->States->save($row);
								}
							}

							if(isset($aState->id) && !empty($aState->id)){
		            			// END: Update States
		            			$aDistricts = $this->Districts->find('all', array(
					                'conditions' => array(
										'state_id' => $aState->id,
										'lat'      => 0,
										'lng'      => 0,
					                ),
					            ))->toArray();

					            if(isset($aDistricts) && !empty($aDistricts)){
					            	foreach ($aDistricts as $keyDistrict => $aDistrict) {
					            		// Update Districts
					            		// $addressDistricts = str_replace(' ','+', $aDistrict->name) . ',+' . $addressState . ',+' . $address;
					            		$addressDistricts = str_replace(' ','+', $aDistrict->name);
										$token = $aToken[array_rand($aToken)];
										$sLinkDistricts  = sprintf($sLinkDefault, $addressDistricts, $token);
										$result = file_get_contents($sLinkDistricts);
										$output = json_decode($result, true);
										if(isset($output['status']) && $output['status'] == 'OVER_QUERY_LIMIT'){
											echo 'OVER_QUERY_LIMIT';
											die();
										}
										echo 'Districts: ' . $sLinkDistricts . '<br>';
										pr($output);
										echo '<br>';
										if(isset($output['results'][0]['geometry']['location']['lat']) && isset($output['results'][0]['geometry']['location']['lng'])){
											$latitude = $output['results'][0]['geometry']['location']['lat'];
											$longitude = $output['results'][0]['geometry']['location']['lng'];

											$row = $this->Districts->get($aDistrict->id);
											$row->lat = $latitude;
											$row->lng = $longitude;
											$this->Districts->save($row);
										}
					            		// END: Update Districts
					            	}
					            }
					        }

		            	}
		            }
				}
			}
		}

		$requestQuery  = $this->request->query;
		$requestParams = $this->request->params;
		$iPage         = (isset($requestQuery['page']) && $requestQuery['page'] ? (int)$requestQuery['page'] : 1);
		$iPageCount    = (isset($requestParams['paging']['Countries']['pageCount']) ? (int)$requestParams['paging']['Countries']['pageCount'] : 0);
		if($iPage < $iPageCount){
			echo 'Update lat lng countries, districts, states successfull: page=' . $iPage;
			echo '<script type="text/javascript">';
			echo 'window.location = "/locations/updateLatLng?page='.($iPage + 1).'";';
			echo '</script>';
		}else{
			echo 'Update lat lng countries, districts, states successfull';
		}
        die();
	}
}