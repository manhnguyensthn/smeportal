<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Network\Request;
use App\Lib\ProjectsLib;

//use App\Lib\phpFlickr;

/**
 * Discover Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class DiscoverController extends AppController {

    public $paginate = array();
    public $helpers = array('Paginator');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'getListDiscoverSeeMoreAjax', 'search']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    // Coder: Giang Dien
    // Date: 2016-12-01
    // Function: Discover
    public function index() {
        $aRequestSession = $this->request->session();
        $myDataLocationLatLng = $aRequestSession->read('Config.locationLatLng');
        if (isset($_GET['location_radius'])) {
            $location_radius = $_GET['location_radius'];
            $dataLocation = $this->getLongLatByLocationRadius($location_radius);
        } else {
            $dataLocation = '';
            $location_radius = '';
        }
        ini_set('max_execution_time', 300);
        $apiGetListOptionFilters = ROOT_URL . 'api/Discover/getOptionSearch.json';
        $listOptions = $this->getDataFromAPI($apiGetListOptionFilters, ['group' => 0]);
        $searchParams = [];
        $user = $this->Auth->user();

        $aRequestQuery = $this->request->query;
        if (isset($aRequestQuery['keyword']) && !empty($aRequestQuery['keyword'])) {
            $searchParams['keyword'] = $aRequestQuery['keyword'];
        }

        $searchParams['user_id'] = !empty($user) ? $user['id'] : 0;
        $searchParams['filterCategory'] = isset($_GET['filterCategory']) ? $_GET['filterCategory'] : '5|everything|1|1';
        $searchParams['sortBy'] = isset($_GET['sortBy']) ? $_GET['sortBy'] : '1|Popularity|2|3';
        $searchParams['filterRole'] = isset($_GET['filterRole']) ? $_GET['filterRole'] : '0|Role Available|3|4';
        $searchParams['filterLocation'] = isset($_GET['filterLocation']) ? $_GET['filterLocation'] : '1|everywhere|4|5';
        $searchParams['location_radius'] = isset($_GET['location_radius']) ? $_GET['location_radius'] : '';
		$searchParams['limit'] = 6;
        $searchParams['page'] = 1;
		$filterLocation =  $this->_getCurrentObjectFromString($searchParams['filterLocation']);
		$currentFilterLocation =$this->_getCurrentObjectFromString($searchParams['filterLocation']);
		if($filterLocation['object_id']  == 2) {
			$location_radius = 50000;
			$filterLocation = $this->getLongLatByLocationRadius($location_radius);
		}
		else if ($filterLocation['object_id']  == 3 && isset($searchParams['location_radius']) &&  $searchParams['location_radius'] !=='') 
		{
			$location_radius =  $searchParams['location_radius']*1000;
			$filterLocation = $this->getLongLatByLocationRadius($location_radius);
		}
		if(empty($filterLocation))
		{
			$filterLocation =  $this->_getCurrentObjectFromString($searchParams['filterLocation']);
		}
		
		$searchParams['filterLocation'] = $filterLocation;
	    $apiGetListProjectsByOption = ROOT_URL . 'api/Discover/getListProjectsByOptionSearchForWeb.json';
        $listProjects = $this->getDataFromAPI($apiGetListProjectsByOption, $searchParams);
		if((!isset($user['id']) && ($searchParams['filterCategory']=='1|Recommended to You|1|1')) || (!isset($user['id']) && $searchParams['filterCategory'] =='4|Following|1|1' ))
		{
			$listProjects = [];
		}
		$listProjectIds = $this->_getListProjectIdsFromList($listProjects);
        $this->loadModel('UserProjectActions');
        $this->loadModel('UsersProjects');
        if (count($listProjectIds) > 0) {
            $listUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $listProjectIds, 'action_type' => 1, 'status' => 1]);
            $listUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $listProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
        } else 
		{
            $listUsersActions = [];
            $listUserProjects = [];
        }
		//role 
		$rols = $this->_getListOptionByGroup($listOptions, 3);
		$roles = array();
		$a = array('GRIP 2 (MORE AS NEEDED)','ELECTRICIAN 2 (MORE AS NEEDED)','LEAD MAN','SET DRESSER (MORE AS NEEDED)','GANG BOSS','PA #2 (MORE AS NEEDED)');
		foreach($rols as $rs)
		{
			if(!in_array($rs->object_name,$a))
			{
				$roles[] = $rs;
			}
		}
        $this->set('listProjects', $listProjects);
        $this->set('listEverything', $this->_getListOptionByGroup($listOptions, 1));
        $this->set('listSortBy', $this->_getListOptionByGroup($listOptions, 2));
        $this->set('listRoles', $roles);
        $this->set('myLocation', $myDataLocationLatLng);
        $this->set('currentFilterCategory', $this->_getCurrentObjectFromString($searchParams['filterCategory']));
        $this->set('currentSortBy', $this->_getCurrentObjectFromString($searchParams['sortBy']));
        $this->set('currentFilterRole', $this->_getCurrentObjectFromString($searchParams['filterRole']));
        $this->set('currentFilterLocation', $currentFilterLocation);
        $this->set('listUserActions', $listUsersActions);
        $this->set('location_radius', $searchParams['location_radius']);
        $this->set('dataLocation', $dataLocation);
        $this->set('listUserProjects', $listUserProjects);
        $this->set('title', __('Tìm kiếm - Hà Nội SME'));
    }

    public function test() {
   
        $location_radius = 100;
        $dataLocation = $this->getLongLatByLocationRadius($location_radius);
        pr($dataLocation);
        die;
    }

    // Coder: Giang Dien
    // Date: 2016-12-01
    // Function: Get list option by group
    private function _getListOptionByGroup($list = [], $group = 0) {
        $result = [];
        foreach ($list as $row) {
            if ($row->group == $group) {
                $result[] = $row;
            }
        }
        return $result;
    }

    // Coder: Giang Dien
    // Date: 2016-12-02
    // Function: get list discover seemore ajax
    public function getListDiscoverSeeMoreAjax() {
        if ($this->request->is('post')) {
            $aRequestSession = $this->request->session();
            $dataLocationLatLng = $aRequestSession->read('Config.locationLatLng');
            ini_set('max_execution_time', 300);
            $searchParams = [];
            $user = $this->Auth->user();
            $searchParams['user_id'] = !empty($user) ? $user['id'] : 0;
            $searchParams['filterCategory'] = $this->request->data['filterCategory'];
            $searchParams['sortBy'] = $this->request->data['sortBy'];
            $searchParams['filterRole'] = $this->request->data['filterRole'];
            $searchParams['filterLocation'] = $this->request->data['filterLocation'];
            $searchParams['limit'] = 6;
            $searchParams['page'] = $this->request->data['current_page'];
            $apiGetListProjectsByOption = ROOT_URL . 'api/Discover/getListProjectsByOptionSearchForWeb.json';
            $listProjects = $this->getDataFromAPI($apiGetListProjectsByOption, $searchParams);
            $listProjectIds = $this->_getListProjectIdsFromList($listProjects);
            $this->loadModel('UserProjectActions');
            $this->loadModel('UsersProjects');
            if (count($listProjectIds) > 0) {
                $listUsersActions = $this->UserProjectActions->getListUserProjectAction(['project_id IN' => $listProjectIds, 'action_type' => 1, 'status' => 1]);
                $listUserProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id IN' => $listProjectIds, 'UsersProjects.type' => 2], 0, 0, []);
            } else {
                $listUsersActions = [];
                $listUserProjects = [];
            }
            $this->set('listProjects', $listProjects);
            $this->set('listUserActions', $listUsersActions);
            $this->set('listUserProjects', $listUserProjects);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 2016-12-02
    // Function: Get current object from string
    private function _getCurrentObjectFromString($str = NULL) {
        $strArr = explode('|', $str);
        return ['object_id' => $strArr[0], 'type' => $strArr[3]];
    }

    // Coder: Giang Dien
    // Date: 2016-12-02
    // Function: Get list project ids from list
    private function _getListProjectIdsFromList($list = []) {
        $listIds = [];
        foreach ($list as $row) {
            $listIds[] = $row->project_id;
        }
        return $listIds;
    }

    public function search() {
        $aParameters = array();
        $aRequestQuery = $this->request->query;
        if (isset($aRequestQuery['keyword']) && !empty($aRequestQuery['keyword'])) {
            $aParameters['keyword'] = $aRequestQuery['keyword'];
        }

        if (isset($aRequestQuery['filterCategory']) && !empty($aRequestQuery['filterCategory'])) {
            $aParameters['filterCategory'] = $aRequestQuery['filterCategory'];
        }

        if (isset($aRequestQuery['sortBy']) && !empty($aRequestQuery['sortBy'])) {
            $aParameters['sortBy'] = $aRequestQuery['sortBy'];
        }

        if (isset($aRequestQuery['filterRole']) && !empty($aRequestQuery['filterRole'])) {
            $aParameters['filterRole'] = $aRequestQuery['filterRole'];
        }

        if (isset($aRequestQuery['filterLocation']) && !empty($aRequestQuery['filterLocation'])) {
            $aParameters['filterLocation'] = $aRequestQuery['filterLocation'];
        }

        $sParameters = '';
        if ($aParameters) {
            foreach ($aParameters as $keyFilter => $sParameter) {
                $sParameters .= ($sParameters) ? '&' : '?';
                $sParameters .= $keyFilter . '=' . $sParameter;
            }
        }
        return $this->redirect('/discover/index/' . $sParameters);
        die();
    }

}
