<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\View\View;

/**
 * UsersProjects Controller
 *
 * @property \App\Model\Table\UsersProjectsTable $UsersProjects
 */
class UsersProjectsController extends AppController {

    public $components = array('PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow([]);
    }

    // Coder: Giang Dien
    // Date: 2016-11-12
    // Function: Collaborator request join project

    public function joinProject() {
        if ($this->request->is('post')) {
            $usersProjects = TableRegistry::get('UsersProjects');
            $role_id = $this->request->data['role_id'];
			$this->loadModel('Roles');
			$roles = $this->Roles->getRolesByOptions(['id' => $role_id],$limit = '',$offset = '');
			foreach($roles as $rol)
			{
				$role = $rol->role;
			}
            $project_id = $this->request->data['project_id'];
            $project_role_id = $this->request->data['project_role_id'];
            $message = $this->request->data['message'];
            $user = $this->Auth->user();
            $userProjectsList = $usersProjects->getUserProjectsByOptions(['project_id' => $project_id, 'project_role_id' => $project_role_id, 'user_id' => $user['id']]);
		   if (count($userProjectsList) < 0) {
                $this->Flash->error(__('Bạn vừa đăng ký một vị trí, vui lòng đợi xác nhận.'));
            } else {
                $userProjectsData = $usersProjects->newEntity();
                $userProjectsData->user_id = $user['id'];
                $userProjectsData->project_id = $project_id;
                $userProjectsData->role_id = $role_id;
                $userProjectsData->project_role_id = $project_role_id;
                $userProjectsData->name = $user['first_name'] . ' ' . $user['last_name'];
                $userProjectsData->user_picture = $user['avatar'];
                $userProjectsData->biography = $user['biography'];
                $userProjectsData->message_content = $message;
                $userProjectsData->created = date('Y-m-d H:i:s');
                $userProjectsData->status = 0;
                if ($usersProjects->save($userProjectsData)) {
                    $projectsTable = TableRegistry::get('Projects');
                    $projectCreator = $projectsTable->find('all', ['conditions' => ['Projects.id' => $project_id]])->contain([ 'Users'])->first();
					if (!empty($projectCreator)) {
                        $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                        $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $projectCreator->user_id]])->first();
                        $actionType = 1;
                        $optionData = json_encode(['project_id' => $project_id, 'role_id' => $role_id, 'project_role_id' => $project_role_id]);
                        if (!empty($userNotificationSetting)) {
                            if ($userNotificationSetting->member_join_project_mobile_status == 1) {
                                $notificationsTable = TableRegistry::get('Notifications');
                                $notificationData = $notificationsTable->newEntity();
                                $notificationData->user_id = $projectCreator->user_id;
                                $notificationData->action_user_id = $user['id'];
                                $notificationData->project_id = $project_id;
                                $notificationData->option_data = $optionData;
                                $notificationData->action_type = $actionType;  // Type of notification join project
                                $notificationData->read_flg = 0;
                                $notificationData->created = date('Y-m-d H:i:s');
                                if (!$notificationsTable->save($notificationData)) {
                                    $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                                }
								$this->PushNoti($actionType, ['project_id' => $project_id] , $projectCreator->user_id,$user['id'],$role);	 
                            }

                            if ($userNotificationSetting->member_join_project_email_status == 1) {
                                // User setting receil notification via email
                                $template = 'notification';
                                $from = [EMAIL_LOGIN => 'We The Projects'];
                                $subject = __('[SME] Notification on SME');

                                // GET CONTENT MAIL
                                $contentMail = __('Send notification content');
                                if (isset($actionType) && !empty($actionType)) {
                                    $name = $user['first_name'] . ' ' . $user['last_name'];
                                    $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData, $user['id'],$role);
                                }
                                // END: GET CONTENT MAIL

                                if (!$this->sendMail($template, $from, $projectCreator['user']['email'], ['first_name' => $projectCreator['user']['first_name'], 'last_name' => $projectCreator['user']['last_name'], 'notification_content' => $contentMail], $subject)) {
                                    $this->Flash->error(__('Send mail to creator fail.'));
                                }
                            }

                            $this->Flash->success(__('Bạn vừa đăng ký một vị trí, vui lòng đợi xác nhận.'));
                        } else {
                            $this->Flash->error(__('Can not send notification to creator. Please try again laster.'));
                        }
                    }
                } else {
                    $this->Flash->error(__('Can not join this role. Please try again laster.'));
                }
            }
        } else {
            $this->Flash->error(__('No match data'));
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 10-01-2017
    // Function: get form chat detail
    public function userChatDetail($projectId = NULL, $userId = NULL) {
	    $this->loadModel('Projects');
        $this->loadModel('UsersProjects');
        $user = $this->Auth->user();
        $tokenTable = TableRegistry::get('Tokens');
        $token = $tokenTable->find('all', [
            'conditions' => ['user_id' => $user['id']],
        ]);
        $token = $token->first()->toArray();
		$this->loadModel('ChatRoomMembers');
      
        $usersProjects = $this->UsersProjects->find('all', ['conditions' => ['project_id' => $projectId, 'status' => 1]])->toArray();
        if ($this->_checkUserExits($userId, $usersProjects)) {
            // Check blocking user
            $blockUserTable = TableRegistry::get('Blocks');
            $blockUserList = $blockUserTable->find('all', ['conditions' => ['user_id' => $userId, 'blocked_id' => $user['id']]])->toArray();
            $blockerUser = $blockUserTable->find('all', ['conditions' => ['user_id' => $user['id'], 'blocked_id' => $userId ]])->toArray();
            if (!empty($blockUserList) || !empty($blockerUser)) {
                $this->Flash->error(__('Không thể chat với người dùng bị block, vui lòng hủy block trước !'));
                $this->redirect($this->referer());
            } else {
                ini_set('max_execution_time', 300);
                $apiCheckSingleRoom = ROOT_URL . '/api/Chat/checkSingleRoomExit.json';
                $respone = $this->getDataFromAPI($apiCheckSingleRoom, ['token' => $token['token'], 'project_id' => $projectId, 'member_id' => $userId]);
                if (!empty($respone->room_id)) { 
					return $this->redirect('/chat-room/' . $projectId . '/' . $respone->room_id);
				}else {
                    if ($userId != $user['id']) {
                        $apiCreateSingleRoom = ROOT_URL . '/api/Chat/createSingleRoom.json';
                        $respone = $this->getDataFromAPI($apiCreateSingleRoom, ['token' => $token['token'], 'project_id' => $projectId, 'member_id' => $userId]);
					   	if(isset($_REQUEST['role']))
						{
						$userproject = $this->UsersProjects->
						getMessage_content(['user_id' => $userId, 'project_id' => $projectId, 'role_id' => $_REQUEST['role']]);
						$message = '';
						if(!empty($userproject))
						{
							foreach($userproject as $us)
							{
								$message =  $us->message_content;
							}						
						}
					     $token_chat = $tokenTable->find('all', [
						'conditions' => ['user_id' => $userId],
						]);
						$token_chat = $token_chat->first()->toArray();
						$apiSendMessage = ROOT_URL . '/api/Chat/sendMessage.json';
						$this->getDataFromAPI($apiSendMessage, ['token' => $token_chat['token'], 'room_id' => $respone->room_id, 'message' =>$message]);
					    }
						return $this->redirect('/chat-room/' . $projectId . '/' . $respone->room_id);
                    } else {
                        $this->Flash->error(__('No match data'));
                        return $this->redirect('/');
                    }
                }
            }
        } else {
            $this->Flash->error(__('No match data !'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 10-01-2017
    // Function: check user exits from collaborators list
    private function _checkUserExits($userId = 0, $listCollaborators = []) {
        foreach ($listCollaborators as $row) {
            if ($row->user_id == $userId) {
                return TRUE;
            }
        }
        return FALSE;
    }

    // Coder: Giang Dien
    // Date: 10-01-2017
    // Function: chat room detail
    public function chatRoomDetail($projectId = 0, $roomId = 0) {
        $this->loadModel('Projects');
        $this->loadModel('ChatRoomMembers');
        $user = $this->Auth->user();
        $tokenTable = TableRegistry::get('Tokens');
        $token = $tokenTable->find('all', [
            'conditions' => ['user_id' => $user['id']],
        ]);
        
        $token = $token->first()->toArray();
        ini_set('max_execution_time', 300);
        if (!empty($roomId)) {
            $apiGetRoomInfo = ROOT_URL . '/api/Chat/getRoomInfo.json';
            $roomInfo = $this->getDataFromAPI($apiGetRoomInfo, ['token' => $token['token'], 'project_id' => $projectId, 'room_id' => $roomId]);
            if (empty($roomInfo) || !empty($blockerUser) || !empty($blockUserList)) {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            }
            else
            {
            $blockUserTable = TableRegistry::get('Blocks');
            $blockUserList = $blockUserTable->find('all', ['conditions' => ['user_id' => $user['id'], 'blocked_id' => $roomInfo->users[1]->member_id]])->toArray();
            $blockerUser = $blockUserTable->find('all', ['conditions' => ['user_id' => $roomInfo->users[1]->member_id, 'blocked_id' => $user['id'] ]])->toArray();
            if(!empty($blockUserList) || !empty($blockerUser))
            {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            }
            }
            $this->set('roomInfo', $roomInfo);
            $apiGetConversations = ROOT_URL . '/api/Chat/getConversations.json';
            $listConversations = $this->getDataFromAPI($apiGetConversations, ['token' => $token['token'], 'room_id' => $roomId]);
            $this->set('listConversations', $listConversations->message);
            $this->set('current_counter', $listConversations->current_counter);
            $this->set('title', $roomInfo->room_name . __(' - Hà Nội SME'));
        } else {
            $this->set('roomInfo', []);
            $this->set('listConversations', []);
            $this->set('listConversations', []);
            $this->set('title', __('Chat room - Hà Nội SME'));
        }
        $apiGetListRoom = ROOT_URL . '/api/Chat/getListRoom.json';
        $listRooms = $this->getDataFromAPI($apiGetListRoom, ['token' => $token['token'], 'project_id' => $projectId]);
        if (isset($listRooms->group)) {
            $this->set('listRooms', $listRooms->group);
        } else {
            $this->set('listRooms', []);
        }
        $this->set('current_user', $user);
        $apiGetListContacts = ROOT_URL . '/api/Chat/getContactChatList.json';
        $listContacts = $this->getDataFromAPI($apiGetListContacts, ['token' => $token['token'], 'project_id' => $projectId, 'limit' => 0]);
        $this->set('contacts', $listContacts->users);
        $this->set(['current_url' => $this->referer()]);
        $this->set(['meta_description' => 'Nancy Wheeler' . __(' - Hà Nội SME')]);
        $this->set('roomId', $roomId);
        $Project = $this->Projects->getProject($projectId);
        $this->set('Project', $Project);
    }

    // Coder: Giang Dien
    // Date: 11-01-2017
    // Function: check room status ajax
    public function checkRoomStatusAjax() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $user['id']],
            ]);
            $token = $token->first()->toArray();
            ini_set('max_execution_time', 300);
            $roomId = $this->request->data['room_id'];
            $current_counter = $this->request->data['current_counter'];
            if (!empty($roomId)) {
                $apiGetRoomStatus = ROOT_URL . '/api/Chat/checkConversationStatus.json';
                $roomStatus = $this->getDataFromAPI($apiGetRoomStatus, ['token' => $token['token'], 'room_id' => $roomId, 'current_counter' => $current_counter]);
                echo $roomStatus->text_chat;
                die;
            } else {
                echo 'not_change';
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 11-01-2017
    // Function: send chat message ajax
    public function sendChatMessageAjax() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $user['id']],
            ]);
            $token = $token->first()->toArray();
            ini_set('max_execution_time', 300);
            $roomId = $this->request->data['room_id'];
            $message = $this->request->data['chat_content'];
            if (!empty($roomId)) {
                if ($this->_validateMessage($message)) {
                    $apiSendMessage = ROOT_URL . '/api/Chat/sendMessage.json';
                    $respone = $this->getDataFromAPI($apiSendMessage, ['token' => $token['token'], 'room_id' => $roomId, 'message' => $message]);         
				 if (!empty($respone)) {
                        $responeData = ['status' => 1, 'message' => __('Your message has been sent.'),'text_chat' =>$respone->text_chat];
                    } else {
                        $responeData = ['status' => 0, 'message' => __('You could not send this message.')];
                    }
                    echo json_encode($responeData);
                    die;
                } else {
                    $responeData = ['status' => 0, 'message' => __('Message content is required to send. Max length of 120 chars. HTML or script tags is not allowed.')];
                    echo json_encode($responeData);
                    die;
                }
            } else {
                $responeData = ['status' => 0, 'message' => __('No match data')];
                echo json_encode($responeData);
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 11-01-2017
    // Function: get list contact ajax
    public function getListContactAjax() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $user['id']],
            ]);
            $token = $token->first()->toArray();
            ini_set('max_execution_time', 300);
            $roomId = $this->request->data['room_id'];
            $projectId = $this->request->data['project_id'];
            $apiGetListContacts = ROOT_URL . '/api/Chat/getContactChatList.json';
            $listContacts = $this->getDataFromAPI($apiGetListContacts, ['token' => $token['token'], 'project_id' => $projectId, 'room_id' => $roomId, 'limit' => 0, 'offset' => 0]);
            $this->set('contacts', $listContacts->users);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: add Member To Group Ajax
    public function addMemberToGroupAjax() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $user['id']],
            ]);
            $token = $token->first()->toArray();
            ini_set('max_execution_time', 300);
            $listIds = $this->request->data['listIds'];
            $roomId = $this->request->data['room_id' ];
            $projectId = $this->request->data['project_id'];
            $apiAddMember = ROOT_URL . '/api/Chat/createMemberRoom.json';
            $listIdsArr = explode(';', $listIds);
            foreach ($listIdsArr as $item) {
                if (!empty($item)) {
                    $respone = $this->getDataFromAPI($apiAddMember, ['token' => $token['token'], 'project_id' => $projectId, 'room_id' => $roomId, 'member_id' => $item]);
                    $roomId = $respone->room_id;
                }
            }
            $responeData = ['status' => 1, 'room_id' => $respone->room_id, 'message' => __('A group has been created.')];
            echo json_encode($responeData);
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 12-01-2017
    // Function: get Old Messages Ajax
    public function getOldMessagesAjax() {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $user['id']],
            ]);
            $token = $token->first()->toArray();
            ini_set('max_execution_time', 300);
            $roomId = $this->request->data['room_id'];
            $page = $this->request->data['page'];
            $apiGetMessage = ROOT_URL . '/api/Chat/getOldConversations.json';
            $listMessages = $this->getDataFromAPI($apiGetMessage, ['token' => $token['token'], 'room_id' => $roomId, 'page' => $page, 'limit' => 20]);
            $view = new View($this->request, $this->response, null);
            $view->set('listConversations', $listMessages->message);
            $view->set('current_user', $user);
            $view->layout = false;
            $viewData = $view->render('/UsersProjects/chat_message_ajax');
            if (count($listMessages->message) > 0) {
                $responeData = ['status' => 1, 'message' => $viewData];
            } else {
                $responeData = ['status' => 0, 'message' => __('No match data')];
            }
            echo json_encode($responeData);
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    private function _validateMessage($message = NULL) {
        $message = trim($message);
        if (preg_match("/<[^<]+/", $message, $m)) {
            return FALSE;
        } else {
            if (empty($message)) {
                return FALSE;
            } else {
                if (strlen($message) > 1000) {
                    return FALSE;
                } else {
                    return TRUE;
                }
            }
        }
    }

    // Coder: Giang Dien
    // Date: 07-02-2017
    // Function: delete room ajax
    public function deleteRoomAjax() {
        if ($this->request->is('POST')) {
            $roomId = $this->request->data['room_id'];
            $user = $this->Auth->user();
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['user_id' => $user['id']],
            ]);
            $token = $token->first()->toArray();
            ini_set('max_execution_time', 300);
            $apiDeleteRoom = ROOT_URL . '/api/Chat/deleteRoom.json';
            $respone = $this->getDataFromAPI($apiDeleteRoom, ['token' => $token['token'], 'room_id' => $roomId]);
            echo json_encode($respone);
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

}
