<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SearchedHistory Controller
 *
 * @property \App\Model\Table\SearchedHistoryTable $SearchedHistory
 */
class SearchedHistoryController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $searchedHistory = $this->paginate($this->SearchedHistory);

        $this->set(compact('searchedHistory'));
        $this->set('_serialize', ['searchedHistory']);
    }

    /**
     * View method
     *
     * @param string|null $id Searched History id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $searchedHistory = $this->SearchedHistory->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('searchedHistory', $searchedHistory);
        $this->set('_serialize', ['searchedHistory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $searchedHistory = $this->SearchedHistory->newEntity();
        if ($this->request->is('post')) {
            $searchedHistory = $this->SearchedHistory->patchEntity($searchedHistory, $this->request->data);
            if ($this->SearchedHistory->save($searchedHistory)) {
                $this->Flash->success(__('The searched history has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The searched history could not be saved. Please, try again.'));
            }
        }
        $users = $this->SearchedHistory->Users->find('list', ['limit' => 200]);
        $this->set(compact('searchedHistory', 'users'));
        $this->set('_serialize', ['searchedHistory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Searched History id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $searchedHistory = $this->SearchedHistory->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $searchedHistory = $this->SearchedHistory->patchEntity($searchedHistory, $this->request->data);
            if ($this->SearchedHistory->save($searchedHistory)) {
                $this->Flash->success(__('The searched history has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The searched history could not be saved. Please, try again.'));
            }
        }
        $users = $this->SearchedHistory->Users->find('list', ['limit' => 200]);
        $this->set(compact('searchedHistory', 'users'));
        $this->set('_serialize', ['searchedHistory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Searched History id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $searchedHistory = $this->SearchedHistory->get($id);
        if ($this->SearchedHistory->delete($searchedHistory)) {
            $this->Flash->success(__('The searched history has been deleted.'));
        } else {
            $this->Flash->error(__('The searched history could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
