<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Mailer\Email;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Cake\Network\Http\Client;
use App\Lib\CoreLib;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class DeleteAllController extends Controller {

 
 
	public function index()
	{
		
		 $data = array('blocks', 'categories', 'chat_messages', 'chat_messages_history', 'chat_notifications', 'chat_notifications_history', 'chat_rooms', 'chat_room_members', 'chat_room_messages', 'chat_room_messages_history', 'countries', 'cronjobs', 'districts', 'files', 'followings', 'friends', 'users_interests', 'interests', 'languages', 'notifications',  'project_comments', 'project_milestones', 'project_roles', 'project_updates','roles', 'searched_history', 'sliders', 'states', 'stories', 'sub_category', 'tokens', 'weekly_receives', 'user_project_actions', 'user_notification_setting', 'users_works', 'users_websites', 'users_signup_newsletter', 'users_roles', 'users_projects' , 'websites' ,'projects','users');
			foreach($data as $us)
			{
					$users = TableRegistry::get($us);
					 $query = $users->deleteAll();
			}
	die();
       
	}

}
