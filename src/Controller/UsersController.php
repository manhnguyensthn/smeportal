<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Query;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\View\View;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    const UsersRolesDoUnspecified = -1;
    const UsersRolesDoInsert = 0;
    const UsersRolesDoUpdate = 1;
    const UsersRolesDoDelete = 2;
    const YoutubeEmbedURLPattern = 'https://www.youtube.com/embed';
    const CurlCommonOptions = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_PROXYTYPE => CURLPROXY_HTTP,
        // CURLOPT_PROXYUSERPWD => "an.tran:ta@12345",
        // CURLOPT_PROXY => "192.168.100.11:8080",
        CURLOPT_SSL_VERIFYPEER => false
    ];

    public $components = array('RequestHandler');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Email');
		    $this->loadModel('UserNotificationSetting');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
// Allow users to register and logout.
// You should not add the "login" action to allow list. Doing so would
// cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'logout', 'register', 'ajaxSNS', 'login_google',
            'forgotPass', 'updatePass', 'expriedPass', 'loginSns', 'profile', 'getUserInfor', 'editUserInfor', 'getDistricts', 'confirmChangeEmail', 'actionSignupNewsletter', 'sendNewsletter','Checkmail']);
    }

    public function login() {
        $this->viewBuilder()->layout('login');
        $this->set('title', __('Login - We the projects'));
// check logged
        if (!$this->request->isJson() && !empty($this->Auth->user())) {
            return $this->redirect('/');
        }
        $TermOfUseTable = TableRegistry::get('TermOfUse');
        $termofuse = $TermOfUseTable->find('all')->first();
        $rememberMe = ($this->Cookie->check('rememberMe')) ? $this->Cookie->read('rememberMe') : 0;
// login process
        if ($this->request->is('post')) {
            $this->_data = $this->Auth->identify();
            if ($this->_data) {
                if (isset($this->request->data['remember'])) {
                    $this->Cookie->write('User', ['email' => $this->request->data['email'], 'password' => $this->request->data['password']]);
                    $this->Cookie->write('rememberMe', 1);
                    $rememberMe = 1;
                } else {
                    $this->Cookie->delete('User');
                    $this->Cookie->delete('rememberMe');
                }
                $token = $this->generateRandomString();
                $this->_data['token_session'] = $token;
                $this->Auth->setUser($this->_data);
                $this->_data['token'] = $token;
                $platform = isset($this->request->data['platform']) ? $this->request->data['platform'] : 0;
                $devicetoken = isset($this->request->data['devicetoken']) ? $this->request->data['devicetoken'] : NULL;

				$this->clearToken($this->Auth->user('id'), $platform);
                $this->createToken($this->Auth->user('id'), $platform, $token);
				$check_noti_setting = $this->UserNotificationSetting->getNotificationByUser($this->Auth->user('id'));
				if(empty($check_noti_setting ))
				{
					$data_no = [];
					$data_no['user_id'] = $this->Auth->user('id');
					$data_no['replies_comment_email_status'] = 1;
					$data_no['replies_comment_mobile_status']= 1;
					$data_no['private_message_email_status'] = 1;
					$data_no['private_message_mobile_status'] = 1;
					$data_no['group_message_email_status'] = 1;
					$data_no['group_message_mobile_status'] = 1;
					$data_no['new_follower_email_status'] = 1;
					$data_no['new_follower_mobile_status'] = 1;
					$data_no['project_update_email_status'] = 1;
					$data_no['project_update_mobile_status'] = 1;
					$data_no['member_join_project_email_status'] = 1;
					$data_no['member_join_project_mobile_status'] = 1;
					$data_no['group_notification'] = 1;
					$this->UserNotificationSetting->addUserNotificationSetting($data_no);
				}
                $userID = $this->Auth->user('id');
                $user = $this->Users->find('all', [
                        'conditions' => ['id' => $userID],
                        'fields' => ['id', 'email', 'activated']
                    ])->first();
                if($user->activated !== 1){
                    return $this->redirect('/users/activate');
                }
                else{
                  return $this->redirect($this->Auth->redirectUrl());
                }
            } else {
                $this->Flash->error(__('Invalid username or password.'));
            }
            $this->set([
                'message' => $this->_message,
                'data' => $this->_data,
                'status' => $this->_status,
                '_serialize' => ['message', 'data', 'status']
            ]);
        }
          $this->set([
                'termofuse' => $termofuse
            ]);
        if ($this->Cookie->check('User')) {
            $this->set([
                'cookieData' => $this->Cookie->read('User')
            ]);
        }
        $this->set(['rememberMe' => $rememberMe]);
    }

    public function logout() {
//        if ($this->request->isJson()) {
//            if(isset($this->request->data['platform'])) {
//                $this->clearToken($this->Auth->user('id'), $this->request->data['platform']);
//            }
//        } else {
        $this->clearToken($this->Auth->user('id'), PLATFORM_WEB);
//        }
        return $this->redirect($this->Auth->logout());
    }

    private function generateRandomPassword($length = 14) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return 'V0' . $randomString;
    }

    public function ajaxSNS() {
        $this->autoRender = FALSE;
        $this->Token = TableRegistry::get('Token');
        if (!empty($this->Auth->user())) {
            exit();
        }

        if ($this->request->is('post')) {
            // var_dump($this->request->data);
            if (empty($this->request->data['email'])) {
                echo json_encode(array('ret' => 'NG', 'msg' => __('You need an email to register or login to SME system.')));
                exit;
            }
            if ($this->Users->checkExitEmail($this->request->data['email'])) {
                $user = $this->Users->getUserbyEmail($this->request->data['email']);
                $check = false;
                if (($this->request->data['sns'] == 1) && !empty($user->fb_id)) { // facebook
                    $check = true;
                } elseif (($this->request->data['sns'] == 2 ) && !empty($user->google_id)) { // google
                    $check = true;
                } elseif (($this->request->data['sns'] == 3) && !empty($user->linked_id)) {  // linkedIn
                    $check = true;
                }
                if ($check) {
                    $token = $this->generateRandomString();
                    $user['token_session'] = $token;
					$user['mobile'] = 1;
                    $this->Auth->setUser($user);
                    $this->clearToken($user->id, PLATFORM_WEB);
                    $this->createToken($user->id, PLATFORM_WEB, $token);
					$check_noti_setting = $this->UserNotificationSetting->getNotificationByUser($user->id);
					if(empty($check_noti_setting ))
					{
						$data_no = [];
						$data_no['user_id'] = $user->id;
						$data_no['replies_comment_email_status'] = 1;
						$data_no['replies_comment_mobile_status']= 1;
						$data_no['private_message_email_status'] = 1;
						$data_no['private_message_mobile_status'] = 1;
						$data_no['group_message_email_status'] = 1;
						$data_no['group_message_mobile_status'] = 1;
						$data_no['new_follower_email_status'] = 1;
						$data_no['new_follower_mobile_status'] = 1;
						$data_no['project_update_email_status'] = 1;
						$data_no['project_update_mobile_status'] = 1;
						$data_no['member_join_project_email_status'] = 1;
						$data_no['member_join_project_mobile_status'] = 1;
						$data_no['group_notification'] = 1;
						$this->UserNotificationSetting->addUserNotificationSetting($data_no);
					}
                    echo json_encode(array('ret' => 'OK'));
                    exit;
                } else {
                    echo json_encode(array('ret' => 'NG', 'msg' => __('This email has been registered.')));
                    exit;
                }
            } else {
                if ($this->request->data['sns'] == 1) { // facebook
                    $this->request->data['fb_id'] = $this->request->data['sns_id'];
                    $this->request->data['avatar'] = 'https://graph.facebook.com/' . $this->request->data['sns_id'] . '/picture?type=normal';
                } elseif ($this->request->data['sns'] == 2) { // google
                    $this->request->data['google_id'] = $this->request->data['sns_id'];
                } elseif ($this->request->data['sns'] == 3) {   // linkedIn
                    $this->request->data['linked_id'] = $this->request->data['sns_id'];
                }
                if ((trim($this->request->data['last_name']) == "") || (trim($this->request->data['first_name']) == "")) {
                    echo json_encode(array('ret' => 'NG', 'msg' => __('Your Facebook/Google+/LinkedIn account is missing either first or last name which is not allowed at SME. Please use a different method to sign-up with SME.')));
                    exit;
                }
                if (empty($this->request->data['name ']) && isset($this->request->data['first_name']) && isset($this->request->data['last_name'])) {
                    $this->request->data['name'] = $this->request->data['first_name'] . ' ' . $this->request->data['last_name'];
                }
                $this->request->data['password'] = $this->generateRandomPassword();
                $users = $this->Users->newEntity();
                $users = $this->Users->patchEntity($users, $this->request->data);
                if ($this->Users->save($users)) {
                    $template = 'register';
                    $from = [EMAIL_LOGIN => 'We The Projects'];
                    $subject = __('[SME] Welcome');
                    if ($this->sendMail($template, $from, $this->request->data['email'], [], $subject)) {
                        $token = $this->generateRandomString();
                        $users['token_session'] = $token;
						$user['mobile'] = 1;
                        $this->Auth->setUser($users);
                        $this->createToken($this->Auth->user('id'), PLATFORM_WEB, $token);
						$check_noti_setting = $this->UserNotificationSetting->getNotificationByUser($this->Auth->user('id'));
						if(empty($check_noti_setting ))
						{
							$data_no = [];
							$data_no['user_id'] = $this->Auth->user('id');
							$data_no['replies_comment_email_status'] = 1;
							$data_no['replies_comment_mobile_status']= 1;
							$data_no['private_message_email_status'] = 1;
							$data_no['private_message_mobile_status'] = 1;
							$data_no['group_message_email_status'] = 1;
							$data_no['group_message_mobile_status'] = 1;
							$data_no['new_follower_email_status'] = 1;
							$data_no['new_follower_mobile_status'] = 1;
							$data_no['project_update_email_status'] = 1;
							$data_no['project_update_mobile_status'] = 1;
							$data_no['member_join_project_email_status'] = 1;
							$data_no['member_join_project_mobile_status'] = 1;
							$data_no['group_notification'] = 1;
							$this->UserNotificationSetting->addUserNotificationSetting($data_no);
						}
                        echo json_encode(array('ret' => 'OK'));
                        exit;
                    }
                } else {
                    foreach ($users->errors() as $key => $err) {
                        $msg = implode(array_values($err));
                        break;
                    }
                    echo json_encode(array('ret' => 'NG', 'msg' => $msg));
                    exit;
                }
            }
        }
    }

    /**
     * Register method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function register() {
        $this->viewBuilder()->layout('login');

        $this->_data = $this->Users->newEntity();
// check logged
        if (!empty($this->Auth->user())) {
            return $this->redirect('/');
        }
        $TermOfUseTable = TableRegistry::get('TermOfUse');
        $termofuse = $TermOfUseTable->find('all')->first();
         $this->set([
                'termofuse' => $termofuse
            ]);
// register process
        if ($this->request->is('post')) {
            $this->request->data['email'] = trim($this->request->data['email']);
            if (!$this->request->isJson()) {
                if (isset($this->request->data['newsletters'])) {
                    $this->request->data['newsletters'] = NEWSLETTER_ON;
                }
            }
            if (empty($this->request->data['name ']) && isset($this->request->data['first_name']) && isset($this->request->data['last_name'])) {
                $this->request->data['name'] = $this->request->data['first_name'] . ' ' . $this->request->data['last_name'];
            }
            $this->_data = $this->Users->patchEntity($this->_data, $this->request->data);
            if ($this->Users->save($this->_data)) {
                $this->loadModel('UserNotificationSetting');
				$data_no = [];
				$data_no['user_id'] = $this->_data->id;
				$data_no['replies_comment_email_status'] = 1;
				$data_no['replies_comment_mobile_status']= 1;
				$data_no['private_message_email_status'] = 1;
				$data_no['private_message_mobile_status'] = 1;
				$data_no['group_message_email_status'] = 1;
				$data_no['group_message_mobile_status'] = 1;
				$data_no['new_follower_email_status'] = 1;
				$data_no['new_follower_mobile_status'] = 1;
				$data_no['project_update_email_status'] = 1;
				$data_no['project_update_mobile_status'] = 1;
				$data_no['member_join_project_email_status'] = 1;
				$data_no['member_join_project_mobile_status'] = 1;
				$data_no['group_notification'] = 1;
                $respone = 	$this->UserNotificationSetting->addUserNotificationSetting($data_no);;
                if ($respone == 'TRUE') {
                    $template = 'register';
                    $from = [EMAIL_LOGIN => 'Hiệp hội doanh nghiệp nhỏ và vừa'];
                    $subject = __('[SME] Welcome');

                    $viewArrs = [
                        'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                    ];

                    if ($this->sendMail($template, $from, $this->request->data['email'], $viewArrs, $subject)) {
                        if ($this->request->isJson()) {
                            $this->_message = __('Register success.');
                        } else {
                            $this->Flash->success(__('Welcome to SME!'));
                            return $this->redirect('/');
                        }
                        $this->_status = 1;
                    }
                } else {
                    $this->Flash->error(__("Update Notification setting fail."));
                    return $this->redirect('/');
                }
            } else {
                if ($this->request->isJson()) {
                    $this->_message = __('Register fail. Please try again');
                    foreach ($this->_data->errors() as $key => $value) {
                        $this->_message = implode(array_values($value));
                        break;
                    }
                    $this->_data = [];
                } else {
                    $this->Flash->error(__("Register fail. Please, try again."));
                }
                $this->_status = 0;
            }
        }
        $this->set('title', __('Register - We the projects'));
        $this->set([
            'message' => $this->_message,
            'data' => $this->_data,
            'status' => $this->_status,
            '_serialize' => ['message', 'data', 'status']
        ]);
    }

    public function getDistricts() {
        if (($this->request->is(['ajax', 'json']) && $this->request->is('post'))) {

            $state_id = $this->request->data['state_id'];
            $country_id = $this->request->data['country_id'];

            if ($state_id != -1) {
                $districts = $this->Users->Districts->find('list', ['fields' => ['id', 'name'],
                            'conditions' => ['state_id' => $state_id]])->order(['name' => 'ASC'])->execute();
            } else {
// cities not belong to any state
                $districts = $this->Users->Districts->find('list', ['fields' => ['id', 'name'],
                            'conditions' => ['country_id' => $country_id]])->execute();
            }

            $data = $districts->fetchAll('assoc');

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function getStates() {
        if (($this->request->is(['ajax', 'json']) && $this->request->is('post'))) {
            $country_id = $this->request->data['country_id'];
            $states = $this->Users->States->find('list', ['fields' => ['id', 'name'], 'conditions' => ['country_id' => $country_id]])->order(['name' => 'ASC']);
//            if (!empty($country_id) && ($country_id + 0) > 0) {
//                $conn = $states->connection();
//                $naQuery = new Query($conn);
//                $naIdExpr = $naQuery->newExpr()->add('-1');
//                $naCountryNameExpr = $naQuery->newExpr()->add('\'N/A\'');
//                $naQuery->select(['id' => $naIdExpr, 'name' => $naCountryNameExpr])->from('DUAL');
//                $states = $states->unionAll($naQuery);
//            }

            $states = $states->execute();

            $data = $states->fetchAll('assoc');

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function getPostalCode() {
        $this->autoRender = false;
        if (($this->request->is(['ajax', 'json']) && $this->request->is('post'))) {
            $district_id = $this->request->data['district_id'];
            $postal_code = $this->Users->Districts->find('all', ['fields' => ['postal_code'], 'conditions' => ['id' => $district_id]]
                    )->first();

            if ($postal_code) {
                echo json_encode(['status' => 1, 'postal_code' => $postal_code->postal_code]);
                exit;
            } else {
                echo json_encode(['status' => 0, 'postal_code' => '']);
                exit;
            }
        } else {
            echo json_encode(['status' => 0, 'postal_code' => '']);
            exit;
        }
    }

    public function forgotPass() {
        $this->set('title', __('Forgot Password - We the projects'));
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($this->request->isJson()) {
                foreach ($data as $dk => $vk) {
                    if ($vk == '' || $vk == null) {
                        $this->_status = 0;
                        $this->_message = __($dk . " can't be blank");
                        $this->_data = [];
                        break;
                    }
                }
            }
            $users = $this->Users->find('all', [
                'conditions' => ['email' => $data['email'], 'status' => 1],
                'fields' => ['id', 'email', 'status']
            ]);
            $users = $users->first();
            if (empty($users)) {
                if ($this->request->isJson()) {
                    $this->_status = 1;
                    $this->_message = __('Invalid email');
                    $this->_data = [];
                } else {
                    $this->Flash->error(__('Invalid email.'));
                }
            } else {
                $users->toArray();
                $token = $this->generateRandomString();
                $time = time();
                $url = 'users/update_pass?tk=' . $token . '&ti=' . $time;

                $template = 'forgot';
                $viewVars = ['link' => ROOT_URL . $url];
                $from = [EMAIL_LOGIN => 'We The Projects'];
                $to = $data['email'];
                $subject = __('[SME] Reset Password');
                if ($this->sendMail($template, $from, $to, $viewVars, $subject)) {
                    $user = $this->Users->get($users['id']);
                    $user = $this->Users->patchEntity($user, ['token' => $token, 'time_limit' => md5($time),'active_resetpass'=>1]);
                    if ($this->Users->save($user)) {
                        if ($this->request->isJson()) {
                            $this->_status = 1;
                            if ($user->active_resetpass == 0){
                                $this->_message = __('Email password reset has been sent.');
                            }
                            else{
                                $this->_message = __('Email password reset has been re-sent.');
                            }
                            $this->_data = [];
                        } else {
                            if ($user->active_resetpass == 0){
                                $this->Flash->success(__('Email password reset has been sent.'));
                            }
                            else{
                                $this->Flash->success(__('Email password reset has been re-sent.'));
                            }

                        }
                    }
                } else {
                    $this->Flash->error(__('Email can not be send. Please try again!'));
                }
            }
            $this->set([
                'message' => $this->_message,
                'data' => $this->_data,
                'status' => $this->_status,
                '_serialize' => ['message', 'data', 'status']
            ]);
        }
    }
    public function activate() {
        $this->loadModel('ValidationCodes');
        $this->set('title', __('Kích hoạt tài khoản'));
        if ($this->request->is('post')) {
            // echo json_encode($this->request->Data());
            $code = $this->request->Data('code');
            $userId = $this->Auth->user('id');
            if ($this->request->isJson()) {
                foreach ($data as $dk => $vk) {
                    if ($vk == '' || $vk == null) {
                        $this->_status = 0;
                        $this->_message = __($dk . " can't be blank");
                        $this->_data = [];
                        break;
                    }
                }
            }
            $user = $this->Users->find('all', [
                'conditions' => ['id' => $userId,'activated'=>0],
                'fields' => ['id', 'email', 'activated']
            ])->first();
            if(!empty($user)){
                $codes = $this->ValidationCodes->find('all',[
                    'conditions'=>['code'=> $code]
                    ])->first();
                // echo $codes;
                // echo json_encode($codes);
                if(!empty($codes)){
                    // $codes = $codes->first();
                    if($code == $codes->code){
                        $user = $this->Users->get($userId);
                        $user = $this->Users->patchEntity($user, ['activated'=>1]);
                        if($this->Users->save($user)){
                            $this->Flash->success(__('Tài khoản đã kích hoạt.'));
                            if ($this->ValidationCodes->delete($codes)) {
                                $this->Flash->success(__('The validation code has been deleted.'));
                                return $this->redirect(['controller'=>'projects','action'=>'index']);
                                // return redirectUrl();
                            }
                            else {
                                $this->Flash->error(__('The validation code could not be deleted. Please, try again.'));
                                // return false;
                            }
                        }
                        else{
                            $this->Flash->error(__('Không thể kích hoạt tài khoản'));
                            // return false;
                        }
                    }
                    else{
                        $this->Flash->error(__('Mã không hợp lệ.'));
                        // return false;
                    }
                }
                else{
                    $this->Flash->error(__('Mã không hợp lệ.'));
                    // return false;
                }
            }
            else{
                $this->Flash->error(__('Tài khoản này đã được kích hoạt '));
                // return false;
            }

        }
    }
    public function updatePass() {
        $this->set('title', __('Reset Password - We the projects'));
        $token = $time = '';
        $token = $this->request->query('tk');
        $time = $this->request->query('ti');
        $users = $this->Users->find('all', [
                    'conditions' => ['token' => $token, 'time_limit' => md5($time), 'status' => 1],
                ])->first();
        if (!empty($users)) {
            if (time() - $time > 1800) {
                if ($this->request->isJson()) {
                    $this->_status = 0;
                    $this->_message = __("Your password reset link has expried. Please re-activate the link.");
                    $this->_data = [];
                } else {
                    return $this->redirect(['action' => 'expriedPass']);
                }
            } else {
                if ($this->request->is(['patch', 'post', 'put'])) {
                    $this->request->data['token'] = "";
                    $this->request->data['time_limit'] = "";
                    $users = $this->Users->patchEntity($users, $this->request->data);

                    if ($this->Users->save($users)) {
                        if ($this->request->isJson()) {
                            $this->_status = 1;
                            $this->active_resetpass = 0;
                            $this->_message = __('Your password has been reset.');
                            $this->_data = [];
                        } else {
                            $this->Flash->success(__('Your password has been reset.'));
                            return $this->redirect(['action' => 'login']);
                        }
                    } else {
                        if ($this->request->isJson()) {
                            $this->_status = 0;
                            foreach ($users->errors() as $key => $value) {
                                $this->_message = implode(array_values($value));
                                break;
                            }
//                            $this->_message = __('Your password has been reset.');
                            $this->_data = [];
                        } else {
                            $this->Flash->error(__('Can not reset password'));
                            $this->_data = $users;
                        }
                    }
                }
            }
        } else {
            if ($this->request->isJson()) {
                $this->_status = 0;
                $this->_message = __("Your password reset link has expried. Please re-activate the link.");
                $this->_data = [];
            } else {
                return $this->redirect(['action' => 'expriedPass']);
                return $this->redirect(['action' => 'login']);
            }
        }
//        pr($users);
        $this->set('users', $users);
        $this->set([
            'message' => $this->_message,
            'data' => $this->_data,
            'status' => $this->_status,
            '_serialize' => ['message', 'data', 'status']
        ]);
    }

    public function expriedPass() {
        $this->viewBuilder()->layout('ajax');
        $this->set('title', __('Expried Password - We the projects'));
    }

    private function updateInterest($user_id) {
        $retval = true;
        $data = $this->request->data;

        $deletedInterests = explode(',', $data['deletedInterests']);
        $deletedInterestsIds = [];

        $interestsTables = TableRegistry::get('Interests');
        $usersInterestsTables = TableRegistry::get('UsersInterests');
        $interestEntity = null;
        $usersInterestEntity = null;

// Interests to be deleted
        if (!empty($deletedInterests)) {
            foreach ($deletedInterests as $deletedInterest) {
                $trimmedDeletedInterest = trim($deletedInterest);
                if (!empty($trimmedDeletedInterest) && preg_match("/^[1-9][0-9]*$/D", $trimmedDeletedInterest)) {
                    array_push($deletedInterestsIds, intval($trimmedDeletedInterest));
                }
            }
        }

        if (!empty($deletedInterestsIds)) {
            $usersInterestsTables->deleteAll([
                'interest_id IN' => $deletedInterestsIds,
                'user_id' => $user_id
            ]);

            $interestsTables->deleteAll([
                'id IN' => $deletedInterestsIds
            ]);
        }

// Interests to be added
        $otherInterests = [];
        if (isset($data['UsersInterest'])) {
            $otherInterests = $data['UsersInterest'];
        }

        if (!empty($otherInterests) && $retval) {
            foreach ($otherInterests as $key => $value) {
                $trimmedValue = trim($value);
                if (!empty($trimmedValue) && !is_numeric($trimmedValue)) {
                    $interestEntity = $interestsTables->newEntity();
                    $interestEntity->name = $trimmedValue;
                    if (!$interestsTables->save($interestEntity)) {
                        $retval = false;
                        break;
                    };

                    $usersInterestEntity = $usersInterestsTables->newEntity();
                    $usersInterestEntity->user_id = $user_id;
                    $usersInterestEntity->interest_id = $interestEntity->id;

                    if (!$usersInterestsTables->save($usersInterestEntity)) {
                        $retval = false;
                        break;
                    };
                }
            }
        }

        return $retval;
    }

// action detail user
//    Coder: Giang_Dien
//    Date: 2016/10/18
    public function detail() {
        $id = $this->Auth->user('id');
        $user = $this->Users->get($id);
        $user['secondary_email'] = $user['email'];
        if ($this->request->is(['patch', 'post', 'put'])) {
//            var_dump($this->request->data);
            if (isset($this->request->data['updateEmail'])) {
                $dataRequest = $this->request->data['Users'];
                if ($dataRequest['secondary_email'] != $user['email']) {
                    $secondEmail = $this->Users->find('list', ['conditions' => ['email' => $dataRequest['secondary_email']]])->toArray();
                    if (count($secondEmail) > 0) {
                        $this->Flash->error(__('This email has been registered'));
                    } else {
                        $respone = $this->Users->updateUser($user['id'], $dataRequest);
                        if ($respone == 'TRUE') {
                            $template = 'changeemail';
                            $from = [EMAIL_LOGIN => 'We The Projects'];
                            $subject = __('[SME] Confirm your email change on SME');
                            if ($this->sendMail($template, $from, $user['email'], ['first_name' => $user['first_name'], 'last_name' => $user['last_name'], 'email' => $user['email']], $subject)) {
                                $this->Flash->success(__('Please check email to confirm change of your email address on SME'));
                            } else {
                                $this->Flash->error(__('Not send mail at this time.'));
                            }
                        } else {
                            foreach ($respone as $key => $item) {
                                $user->errors($key, $item, TRUE);
                            }
                            $this->Flash->error(__('Update email fail. Please check email format'));
                        }
                    }
                } else {
                    $this->Flash->error(__('There is nothing changed.'));
                }
            }
            if (isset($this->request->data['updatePass'])) {
// Process update password
                $user['new_password'] = $this->request->data['Users']['password'];
                if (password_verify($this->request->data['Users']['cf_password'], $user->password)) {
                    $respone = $this->Users->updateUser($user['id'], $this->request->data['Users']);
                    if ($respone == 'TRUE') {
                        $this->Flash->success(__('Update password success'));
                    } else {
                        foreach ($respone as $key => $item) {
                            $user->errors($key, $item, TRUE);
                        }
                        $this->Flash->error(__('Update password fail'));
                    }
                    $user->new_password = $this->request->data['Users']['password'];
                } else {
                    $user->error_password = TRUE;
                    if (empty($user['fb_id']) && empty($user['google_id']) && empty($user['linked_id'])) {
                        $user->errors('cf_password', ['cf_password' => ['valid-password' => __('Password does not exist.')]], TRUE);
                    }
                    $this->Flash->error(__('Password does not exist.'));
                }
            }
//             if (isset($this->request->data['saveAll'])) {
// // Process update all
//                 $dataRequest = $this->request->data['Users'];
//                 $Mgs = __("Invalid information. Please identify the correct information of VISA, including: <br/>- 16 digits for visa number.<br/>- Correct name on card, like: JOHN SMITH <br/>- 3 digits for CVV.<br/>- Expired month/year should match with credit card and should be a future date.");
//                 if (!empty($dataRequest['cvv_number']) && strlen($dataRequest['cvv_number']) == 3) {
//                     if (is_numeric($dataRequest['cvv_number'])) {
//                         $respone = $this->Users->updateUser($user['id'], $dataRequest);
//                         if ($respone == "TRUE") {
//                             $this->Flash->success(__('Update payment infomation success'));
//                         } else {
//                             foreach ($respone as $key => $item) {
//                                 $user->errors($key, $item, TRUE);
//                             }
//                             $this->Flash->error($Mgs);
//                         }
//                     } else {
//                         $user->errors('cvv_number', ['cvv_number' => ['valid-cvv-number' => __('Invalid CVV number format. This field not empty. Must be 3 chars length.')]], TRUE);
//                         $this->Flash->error($Mgs);
//                     }
//                 } else {
//                     $user->errors('cvv_number', ['cvv_number' => ['valid-cvv-number' => __('Invalid CVV number format. This field not empty. Must be 3 chars length.')]], TRUE);
//                     $this->Flash->error($Mgs);
//                 }
//             }
        }
        $this->set('title', __('Account details - We the projects'));
        $this->set([
            'message' => $this->_message,
            'data' => $user,
            'status' => $this->_status,
            '_serialize' => ['message', 'data', 'status']
        ]);
    }

//    Confirm change email
//    Coder: Giang_Dien
//    Date: 2016/10/19
    public function confirmChangeEmail() {
        if ($this->request->is('get') && isset($this->request->query['token'])) {
            $email = base64_decode($this->request->query('token'));
            $user = $this->Users->find('all', ['conditions' => ['email' => $email]])->first();
            if (!empty($user)) {
                $dataUser = $this->Users->patchEntity($user, ['email' => $user['secondary_email']]);
                if ($this->Users->save($dataUser)) {
                    $this->Flash->success(__('Change email address success'));
                } else {
                    $this->Flash->error(__('Change email address fail'));
                }
                return $this->redirect('/');
            } else {
                $this->Flash->error(__('email not found'));
                return $this->redirect('/');
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
        die;
    }

//function notification
//    Coder: Giang_Dien
//    Date: 2016/10/20
    public function notifications() {
        $user = $this->Auth->user();
        $this->loadModel('UserNotificationSetting');
        $notification = $this->UserNotificationSetting->getNotificationByUser($user['id']);
        $noties = [];
        if (!empty($notification)) {
            $noties['replies_comment_email_status'] = $notification->replies_comment_email_status;
            $noties['replies_comment_mobile_status'] = $notification->replies_comment_mobile_status;
            $noties['private_message_email_status'] = $notification->private_message_email_status;
            $noties['private_message_mobile_status'] = $notification->private_message_mobile_status;
            $noties['group_message_email_status'] = $notification->group_message_email_status;
            $noties['group_message_mobile_status'] = $notification->group_message_mobile_status;
            $noties['new_follower_email_status'] = $notification->new_follower_email_status;
            $noties['new_follower_mobile_status'] = $notification->new_follower_mobile_status;
            $noties['project_update_email_status'] = $notification->project_update_email_status;
            $noties['project_update_mobile_status'] = $notification->project_update_mobile_status;
            $noties['member_join_project_email_status'] = $notification->member_join_project_email_status;
            $noties['member_join_project_mobile_status'] = $notification->member_join_project_mobile_status;

            if ($this->request->is(['post', 'put'])) {
                $dataRequest = $this->request->data();
                if ($dataRequest == $noties) {
                    $this->Flash->error(__('There is nothing changed.'));
                } else {
                    $respone = $this->UserNotificationSetting->updateUserNotificationSetting($notification['id'], $dataRequest);
                    if ($respone == TRUE) {
                        $this->Flash->success(__('Lưu thành công!'));
                        $notification = $this->UserNotificationSetting->getNotificationByUser($user['id']);
                    } else {
                        $this->Flash->error(__('Update notification setting error'));
                    }
                }
            }
            $this->set('title', __('Notifications - We the projects'));
            $this->set([
                'message' => $this->_message,
                'user' => $user,
                'data' => $notification,
                'status' => $this->_status,
                '_serialize' => ['message', 'data', 'user', 'status']
            ]);
        } else {
            $this->loadModel('UserNotificationSetting');
            $respone = $this->UserNotificationSetting->addUserNotificationSetting(['user_id' => $user['id']]);
            return $this->redirect($this->referer());
        }
    }

//check password
    public function checkP() {
        $this->autoRender = FALSE;
        if ($this->request->is('post')) {
            $id = $this->Auth->user('id');
            $user = $this->Users->get($id);
            if (password_verify($this->request->data['p'], $user->password)) {
                echo json_encode(['ret' => 'OK']);
                exit();
            } else {
                echo json_encode(['ret' => 'NG']);
                exit();
            }
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-17
    // Function: get user information by user id
    public function getUserInfoById($userId = 0) {
        $user = $this->Users->find('all', ['conditions' => ['id' => $userId]])->first();
        return $user;
    }

    /**
     * sendNewsletter
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function sendNewsletter(){
        $this->_status = 0;
        if ($this->request->is('post') && $this->request->isAjax()) {
            $RequestData = $this->request->data;
            if(!isset($RequestData['title']) || empty($RequestData['title'])){
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            }else{
                $sTitle = $RequestData['title'];
            }

            if($this->_status == 0){
                if(!isset($RequestData['content']) || empty($RequestData['content'])){
                    $this->_status = 1;
                    $this->_message = __('Unable to process your request.');
                }else{
                    $sContent = $RequestData['content'];
                }
            }

            if($this->_status == 0){
                $aType = array('user', 'newsletter');
                if(!isset($RequestData['TypeId']) || empty($RequestData['TypeId'])){
                    $this->_status = 1;
                    $this->_message = __('Unable to process your request.');
                }else{
                    if(!in_array($RequestData['TypeId'], $aType)){
                        $this->_status = 1;
                        $this->_message = __('Unable to process your request.');
                    }else{
                        $TypeId = $RequestData['TypeId'];
                    }
                }
            }

            if($this->_status == 0){
                $bContinue = true;
                $ListMails = array();
                if(isset($RequestData['page']) && !empty($RequestData['page'])){
                    $page = $RequestData['page'];
                }else{
                    $page = 1;
                }
                $PageSize  = 1;
                $template  = 'newsletter';
                $from      = [EMAIL_LOGIN => __('We The Projects')];
                $subject   = $sTitle;
                switch ($TypeId) {
                    case 'user':
                        $Users = TableRegistry::get('Users')->find('all', ['conditions' => ['receive_newletter' => 1], 'page' => $page, 'limit'=>$PageSize])->toArray();
                        if($Users){
                            foreach ($Users as $key => $InfoMail) {
                                $Email = $InfoMail['email'];
                                $ListMails[$key]['email']      = $Email;
                                $ListMails[$key]['first_name'] = $InfoMail->first_name;
                                $ListMails[$key]['last_name']  = $InfoMail->last_name;
                                if ($this->sendMail($template, $from, $Email, ['user' => $InfoMail, 'content' => $sContent], $subject)) {
                                    $ListMails[$key]['status'] = __('Send successfully');
                                    $ListMails[$key]['status'] = 1;
                                }else{
                                    $ListMails[$key]['status'] = __('Send Fail');
                                    $ListMails[$key]['status'] = 0;
                                }
                                $ListMails[$key]['sTypeId'] = 'User';
                            }
                            $this->_data['TypeId'] = 'user';
                        }else{
                            $this->_data['TypeId'] = 'newsletter';
                            $page = 0;
                        }
                    break;
                    case 'newsletter':
                        $UsersSignupNewsletterTable = TableRegistry::get('users_signup_newsletter');
                        $SignupNewsLetters = $UsersSignupNewsletterTable->find('all', ['page' => $page, 'limit'=>$PageSize])->toArray();
                        if($SignupNewsLetters){
                            foreach ($SignupNewsLetters as $key => $InfoMail) {
                                $Email      = $InfoMail['email'];
                                $UsersTable = TableRegistry::get('Users')->find('all', ['conditions' => ['receive_newletter' => 1, 'email' => $Email]])->group(['email'])->first();
                                if(!$UsersTable){
                                    $InfoMail->first_name = $Email;
                                    $InfoMail->last_name = '';
                                    $ListMails[$key]['email']      = $Email;
                                    $ListMails[$key]['first_name'] = $InfoMail->first_name;
                                    $ListMails[$key]['last_name']  = $InfoMail->last_name;
                                    if ($this->sendMail($template, $from, $Email, ['user' => $InfoMail, 'content' => $sContent], $subject)) {
                                        $ListMails[$key]['status'] = 1;
                                    }else{
                                        $ListMails[$key]['status'] = 0;
                                    }
                                    $ListMails[$key]['sTypeId'] = 'News Letter';
                                }
                            }
                            $this->_data['TypeId'] = 'newsletter';
                        }else{
                            $bContinue = false;
                        }
                    break;
                }
                $this->_data['bContinue'] = $bContinue;
                $HtmlListMails = '';
                if($ListMails){
                    foreach ($ListMails as $key => $ListMail) {
                        $sColor = 'red';
                        $sText = 'Error';
                        if($ListMail['status'] == 1){
                            $sColor = 'green';
                            $sText = 'Success';
                        }

                        $HtmlListMails .= '<tr style="color: '.$sColor.';">';
                            $HtmlListMails .= '<td>'. $ListMail['email'].'</td>';
                            $HtmlListMails .= '<td>'. $ListMail['first_name'] . ' ' . $ListMail['last_name'] . '</td>';
                            $HtmlListMails .= '<td>'. $ListMail['sTypeId'] . '</td>';
                            $HtmlListMails .= '<td class="text-center"><strong>'.$sText.'</strong></td>';
                        $HtmlListMails .= '</tr>';
                    }
                }
                $this->_data['HtmlListMails'] = $HtmlListMails;
                $this->_data['page']      = $page + 1;
            }
        }

        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * Controller Blocked
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function blocked(){
        $authUser = $this->Auth->user();
        if(isset($authUser['id']) && !empty($authUser['id'])){
            $UserId = $authUser['id'];
        }else{
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }

        $PageSize   = 10;
        $Blockeds   = array();
        $LinkApi    = ROOT_URL . 'api/usersFollow/showBlocklist.json';
        $Token      = '';
        $TokenTable = TableRegistry::get('Tokens');
        $aToken     = $TokenTable->find('all', ['conditions' => ['user_id' => $UserId]])->first()->toArray();
        if(isset($aToken['token']) && !empty($aToken['token'])){
            $Token = $aToken['token'];
        }
        $Params = [
            'token' => $Token,
            'page'  => 1,
            'limit' => $PageSize,
        ];
        $response = $this->getDataFromAPI($LinkApi, $Params, true);
        if(isset($response->data->user)){
            $Blockeds = $response->data->user;
        }

        $this->set('title', __('Blocked Users - We the projects'));
        $this->set('PageSize', $PageSize);
        $this->set('Blockeds', $Blockeds);
    }

    /**
     * get See More Blocked
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getSeeMoreBlocked(){
        $this->_status = 1;
        $this->_message = '';

        if(!$this->request->isAjax()){
            $this->_status = 0;
            $this->_message = __('Unable to process your request.');
        }

        $authUser = $this->Auth->user();
        if(isset($authUser['id']) && !empty($authUser['id'])){
            $UserId = $authUser['id'];
        }else{
            $this->_status = 0;
            $this->_message = __('Unable to process your request.');
        }

        $RequestData = $this->request->data;
        $PageSize    = 10;

        if($this->_status == 1){
            if(!isset($RequestData['page']) || empty($RequestData['page'])){
                $this->_status = 0;
                $this->_message = __('Unable to process your request.');
            }else{
                $Page = $RequestData['page'];
            }
        }

        if($this->_status == 1){
            $Followings = array();
            $LinkApi    = ROOT_URL . 'api/usersFollow/showBlocklist.json';
            $Token      = '';
            $TokenTable = TableRegistry::get('Tokens');
            $aToken     = $TokenTable->find('all', ['conditions' => ['user_id' => $UserId]])->first()->toArray();
            if(isset($aToken['token']) && !empty($aToken['token'])){
                $Token = $aToken['token'];
            }
            $Params   = [
                'token' => $Token,
                'page'  => $Page,
                'limit' => $PageSize,
            ];
            $response = $this->getDataFromAPI($LinkApi, $Params, true);
            if(isset($response->data->user)){
                $Blockeds = $response->data->user;
            }

            $view = new View($this->request, $this->response, null);
            $view->set('Blockeds', $Blockeds);
            $view->set('type', 'following');
            $view->set('authUser', $this->Auth->user());

            $view->layout = false;
            $this->_data['sHtmlItem'] = $view->render('/Element/users/item_blocked');
            if(count($Blockeds) >= $PageSize){
                $this->_data['page'] = $Page + 1;
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }
    public function Checkmail() {
         if (isset($this->request->data['email']) && !empty($this->request->data['email'])) {
             $email = $this->Users->getUserbyEmail($this->request->data['email']);
             if(empty($email)) {
                 echo 0;
             } else {
                 echo 1;
             }
             die();
            }
    }
}
