<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Request;
use App\Lib\ProjectsLib;
use Cake\Network\Http\Client;
use Cake\View\View;

//use App\Lib\phpFlickr;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class MettingsController extends AppController {
    public $paginate     = array();
    public $helpers      = array('Paginator');
    public $components   = array('PushNotification');
    
    public $_UrlApi     = 'https://api.dropboxapi.com/';
    public $_UrlContent = 'https://content.dropboxapi.com/';
    public $_Token      = TOKEN_DROPBOX;
    public $_UserAgent  = 'User-Agent: api-explorer-client';
    public $_NameRoot   = '/WTP_DATA/';

    /*
        URL API
    */
    public $_GetCurrentAccount = '2/users/get_current_account';
    public $_CreateFolder      = '2/files/create_folder';
    public $_ListFolder        = '2/files/list_folder';
    public $_Upload            = '2/files/upload';
    public $_Download          = '2/files/download';
    public $_DeleteFile        = '2/files/delete';
    public $_TemporaryLink     = '2/files/get_temporary_link';
    /*
        END: URL API
    */

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow([]);
    }

    /**
     * call Curl Upload
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function _callCurlUpload($sUrl, $params = array(), $header = array(), $TypeId = '', $Method = 'POST'){
        $ch   = curl_init();
        $sUri = $this->_UrlContent . $sUrl;

        curl_setopt($ch, CURLOPT_URL, $sUri);

        $headers = array('Authorization: Bearer ' . $this->_Token);
        if($header){
            $headers = array_merge($headers, $header);
        }else{
            $headers[] = 'Content-Type: application/json';
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->_UserAgent); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $queryString = ($params ? json_encode($params) : 'null');
        if($queryString) curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);

        if($TypeId == 'upload_file'){
            $RequestData = $this->request->data;
            $File        = (isset($RequestData['file']) ? $RequestData['file'] : 0);
            if($File){
                $body = file_get_contents($File['tmp_name']);
                $FileSize = filesize($File['tmp_name']);
                $fp = fopen('php://temp/maxmemory:256000', 'w');
                if (!$fp) die('could not open temp memory data');
                fwrite($fp, $body);
                fseek($fp, 0);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                curl_setopt($ch, CURLOPT_UPLOAD, 1);
                curl_setopt($ch, CURLOPT_INFILE, $fp);
                curl_setopt($ch, CURLOPT_INFILESIZE, $FileSize);
            }
        }

        $response = curl_exec($ch);
        curl_close($ch);
        $return = json_decode($response, true);
        if(!$return){
            $return = $response;
        }
        return $return;
    }

    /**
     * call Curl
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function _callCurl($sUrl, $params = array(), $header = array(), $Method = 'POST'){
        $ch   = curl_init();
        $sUri = $this->_UrlApi . $sUrl;
        curl_setopt($ch, CURLOPT_URL, $sUri);

        $headers = array('Authorization: Bearer ' . $this->_Token);
        if($header) $headers = array_merge($headers, $header);
        else $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_USERAGENT, $this->_UserAgent); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $Method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $queryString = ($params ? json_encode($params) : 'null');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);

        $response = curl_exec($ch);
        curl_close($ch);
        $return = json_decode($response, true);
        if(!$return){
            $return = $response;
        }
        return $return;
    }

    /**
     * add More Filter Project
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function update($ProjectId = 0){
        $this->loadModel('Projects');
        $Project = $this->Projects->getProject($ProjectId);
        $this->set('Project', $Project);

        if(!$Project){
            return $this->redirect('/');
        }

        $aUser       = $this->Auth->user();
        $UserId      = $aUser['id'];
        $UserIdOwner = (isset($Project->user->id) ? $Project->user->id : 0);
        $bOwner      = false;

        if(!$aUser){
            return $this->redirect('/');
        }

        if($aUser['id'] == $UserIdOwner){
            $bOwner = true;
        }else{
            $this->loadModel('UsersProjects');
            $UserProject = $this->UsersProjects->getUserProjectsByOptions(['user_id' => $UserId, 'project_id' => $Project->id, 'status' => 1, 'type' => 2]);
            if(!$UserProject){
                return $this->redirect('/');
            }
        }

        $this->loadModel('ProjectUpdates');
        $ProjectUpdates = $this->ProjectUpdates->find('all')
            ->where(['project_id' => $ProjectId, 'status' => 1])
            ->order(['ProjectUpdates.created' => 'DESC'])
            ->toArray();

        $ProjectDataMilestone = $this->ProjectUpdates->find('all')->where(['project_id' => $ProjectId, 'status' => 0, 'type' => MILESTONE_TYPE])->first();

        $this->set('title', __('We the projects').' - '.__('Update'));
        $this->set('bOwner', $bOwner);
        $this->set('Project', $Project);
        $this->set('ProjectUpdates', $ProjectUpdates);
        $this->set('ProjectDataMilestone', $ProjectDataMilestone);
    }

    /**
     * controller Asset
     * @author Roxannie Nguyen jr
     */
    public function asset($ProjectId = 0){
        $this->loadModel('Projects');
        $Project = $this->Projects->getProject($ProjectId);

        if(!$Project){
            return $this->redirect('/');
        }

        $aUser       = $this->Auth->user();
        $UserId      = (isset($aUser['id']) ? $aUser['id'] : 0);
        
        $UserIdOwner = (isset($Project->user->id) ? $Project->user->id : 0);
        $Path        = $this->_getRootDropbox($UserIdOwner, $ProjectId);
        $ListFolders = array();
        $bOwner      = false;

        if($UserId != $UserIdOwner){
            $this->loadModel('UsersProjects');
            $UserProject = $this->UsersProjects->getUserProjectsByOptions(['user_id' => $UserId, 'project_id' => $Project->id, 'status' => 1, 'type' => 2]);
            if(!$UserProject){
                return $this->redirect('/');
            }
        }

        if($Path){
            $ParamListFolder = array('path' => $Path);
            $ListFolders     = $this->_callCurl($this->_ListFolder, $ParamListFolder);
        }
        
        if($UserId == $UserIdOwner && $Path){
            $bOwner = true;
            if(empty($ListFolders) || isset($ListFolders['error'])){
                $ParamCreateFolder = array('path' => $Path);
                $CreateFolder      = $this->_callCurl($this->_CreateFolder, $ParamCreateFolder);
                $ListFolders       = $this->_callCurl($this->_ListFolder, $ParamListFolder);
            }
        }

        if(isset($ListFolders['entries']) && !empty($ListFolders['entries'])){
            foreach ($ListFolders['entries'] as $key => $ListFolder) {
                $this->_validateDataDropbox($ListFolders['entries'][$key]);
            }
            krsort($ListFolders['entries']);
        }

        $this->set('title', __('We the projects').' - '.__('Assets'));
        $this->set('bOwner', $bOwner);
        $this->set('ListFolders', $ListFolders);
        $this->set('Project', $Project);
    }

    /**
     * controller schedule
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function schedule($ProjectId = 0){
        $this->loadModel('Projects');
        $Project = $this->Projects->getProject($ProjectId);
        $this->set('Project', $Project);

        if(!$Project){
            return $this->redirect('/');
        }

        $aUser       = $this->Auth->user();
        $UserId      = $aUser['id'];
        $UserIdOwner = (isset($Project->user->id) ? $Project->user->id : 0);
        $bOwner      = false;

        if(!$aUser){
            return $this->redirect('/');
        }

        if($aUser['id'] == $UserIdOwner){
            $bOwner = true;
        }else{
            $this->loadModel('UsersProjects');
            $UserProject = $this->UsersProjects->getUserProjectsByOptions(['user_id' => $UserId, 'project_id' => $Project->id, 'status' => 1, 'type' => 2]);
            if(!$UserProject){
                return $this->redirect('/');
            }
        }

        $this->set('title', __('We the projects').' - '.__('Schedule'));
        $this->set('bOwner', $bOwner);
        $this->set('Project', $Project);
    }

    /**
     * validata dropbopx
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function _validateDataDropbox(&$ListFolder){
        $ExtImage = $this->_getFileTypeImage();
        $ExtText  = $this->_getFileTypeText();
        $Extpdf   = $this->_getFileTypePdf();
        $ExtVideo = $this->_getFileTypeVideo();

        if($ListFolder){
            if(isset($ListFolder['server_modified']) && !empty($ListFolder['server_modified'])){
                $this->_formatDate($ListFolder['server_modified']);
            }

            if(isset($ListFolder['client_modified']) && !empty($ListFolder['client_modified'])){
                $this->_formatDate($ListFolder['client_modified']);
            }

            if(isset($ListFolder['size']) && !empty($ListFolder['size'])){
                $iSize = (int)$ListFolder['size'];
                if($iSize < 1024){
                    $ListFolder['size'] = $iSize . ' B';
                }else{
                    $iSize = number_format(($iSize/1024), '0');
                    if($iSize < 1024){
                        $ListFolder['size'] = $iSize . ' KB';
                    }else{
                        $ListFolder['size'] = number_format(($iSize/1024), '0') . ' MB';
                    }
                }
            }

            if(isset($ListFolder['id']) && !empty($ListFolder['id'])){
                $ListFolder['wtpid'] = 'wtp'.str_replace('id:', '', $ListFolder['id']);
            }

            if((isset($ListFolder['.tag']) && $ListFolder['.tag'] == 'file') || !isset($ListFolder['.tag'])){
                $name = explode('.', $ListFolder['name']);
                $extName = strtolower(array_pop($name));
                if(in_array($extName, $ExtImage)){
                    $ListFolder['type_file'] = 'photo';
                }

                if(!isset($ListFolder['type_file']) || empty($ListFolder['type_file'])){
                    if(in_array($extName, $ExtText)){
                        $ListFolder['type_file'] = 'text';
                    }
                }

                if(!isset($ListFolder['type_file']) || empty($ListFolder['type_file'])){
                    if(in_array($extName, $Extpdf)){
                        $ListFolder['type_file'] = 'pdf';
                    }
                }

                if(!isset($ListFolder['type_file']) || empty($ListFolder['type_file'])){
                    if(in_array($extName, $ExtVideo)){
                        $ListFolder['type_file'] = 'video';
                    }
                }
            }
        }
    }

    /**
     * Format Date
     * @author Roxannie Nguyen jr
     * @return array
     */
    private function _formatDate(&$SourceDate){
        $SourceDate = date("d/m/Y", strtotime($SourceDate));
    }

    /**
     * get File type video
     * @return array
     */
    private function _getFileTypeVideo(){
        return array('mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv');
    }

    /**
     * get File type Text
     * @return array
     */
    private function _getFileTypeText(){
        return array('txt');
    }

    /**
     * get File type Pdf
     * @return array
     */
    private function _getFileTypePdf(){
        return array('pdf');
    }

    /**
     * get File type Pdf
     * @return array
     */
    private function _getFileTypeImage(){
        return array('jpg', 'jpeg', 'png', 'gif', 'bmp');
    }
    /**
     * get Allowed Dropbox Uploaded
     * @return array
     */
    private function _getAllowedFileTypesUploadDropbox() {
        $ExtImage = (array)$this->_getFileTypeImage();
        $ExtText  = (array)$this->_getFileTypeText();
        $Extpdf   = (array)$this->_getFileTypePdf();
        $ExtVideo = (array)$this->_getFileTypeVideo();
        return array_merge($ExtImage, $ExtText, $Extpdf, $ExtVideo);
    }

    /**
     * GET file TYPE
     * @param string $mime
     * @return string
     */
    private function _getFileTypeByMimeType($mime) {
        $retval = '';
        $mimes = [
            // images
            'image/bmp'   => 'bmp',
            'image/gif'   => 'gif',
            'image/jpeg'  => 'jpeg',
            'image/pjpeg' => 'jpeg',
            'image/png'   => 'png',
            
            // videos
            'video/quicktime'             => 'mov',
            'video/mpeg'                  => 'mpeg',
            'application/x-troff-msvideo' => 'avi',
            'video/avi'                   => 'avi',
            'video/msvideo'               => 'avi',
            'video/x-msvideo'             => 'avi',
            'video/mp4'                   => 'mp4',
            'application/mp4'             => 'mp4',
            'video/3gpp'                  => '3gp',
            'video/x-ms-wmv'              => 'wmv',
            'video/x-flv'                 => 'flv',

            // TEXT
            'text/plain'      => 'txt',
            'application/pdf' => 'pdf',
        ];

        foreach ($mimes as $key => $value) {
            if ($key == trim($mime)) {
                $retval = $value;
                break;
            }
        }
        return $retval;
    }

    /**
     * Upload File Dropbox
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function uploadFileDropbox(){
        if (!$this->request->isAjax()) {
            $this->Flash->error(__('Unable to process your request.'));
            return $this->redirect('/');
        }

        $this->_status = 0;
        $aUser = $this->Auth->user();

        if(!$aUser){
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        }

        if($this->_status == 0){
            $RequestData = $this->request->data;

            if(!isset($RequestData['project_id']) || empty($RequestData['project_id'])){
                $this->_status = 1;
                $this->_message = __('Invalid Project Id.');
            }else{
                $ProjectId = $RequestData['project_id'];
                $this->loadModel('Projects');
                $Project   = $this->Projects->getProject($ProjectId);
                if(!$Project){
                    $this->_status = 1;
                    $this->_message = __('Invalid Project Id.');
                }
            }
        }
        
        if($this->_status == 0){
            if($Project->user->id != $aUser['id']){
                $this->_status = 1;
                $this->_message = __('You are not owner.');
            }
        }

        if($this->_status == 0){
            $UserId = $aUser['id'];
            
            if(!isset($RequestData['file']) || empty($RequestData['file'])){
                $this->_status = 1;
                $this->_message = __('No file was uploaded.');
            }
        }

        if($this->_status == 0){
            $File = $RequestData['file'];
            if($File['error'] == UPLOAD_ERR_INI_SIZE){
                $this->_status = 1;
                $this->_message = __('The uploaded file exceeds the upload_max_filesize directive in php.ini.');
            }
        }

        if($this->_status == 0){
            if($File['error'] == UPLOAD_ERR_FORM_SIZE){
                $this->_status = 1;
                $this->_message = __('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form');
            }
        }

        if($this->_status == 0){
            if($File['error'] == UPLOAD_ERR_PARTIAL){
                $this->_status = 1;
                $this->_message = __('The uploaded file was only partially uploaded.');
            }
        }

        if($this->_status == 0){
            if($File['error'] == UPLOAD_ERR_NO_FILE){
                $this->_status = 1;
                $this->_message = __('No file was uploaded.');
            }
        }

        if($this->_status == 0){
            if($File['error'] == UPLOAD_ERR_NO_TMP_DIR){
                $this->_status = 1;
                $this->_message = __('Missing a temporary folder');
            }
        }

        if($this->_status == 0){
            if($File['error'] == UPLOAD_ERR_CANT_WRITE){
                $this->_status = 1;
                $this->_message = __('Failed to write file to disk');
            }
        }

        if($this->_status == 0){
            if($File['error'] == UPLOAD_ERR_EXTENSION){
                $this->_status = 1;
                $this->_message = __('A PHP extension stopped the file upload');
            }
        }

        if($this->_status == 0){
            $Path = $this->_getRootDropbox($UserId, $ProjectId);
            $sFileType       = $this->_getFileTypeByMimeType($File['type']);
            $aAllowFileTypes = $this->_getAllowedFileTypesUploadDropbox();

            $message = __('File upload should be in (in text, image, media, video), in range of size of 1KB to 100MB.');
            if(!in_array($sFileType, $aAllowFileTypes)){
                $this->_status = 1;
                $this->_message = $message;
            }else{

                $ExtVideo   = $this->_getFileTypeVideo();
                $ExtText    = $this->_getFileTypeText();
                $Extpdf     = $this->_getFileTypePdf();
                $ExtImage   = $this->_getFileTypeImage();
                $FileSizeMb = $File['size'];
                $MinSize    = 1024;
                if(in_array($sFileType, $ExtVideo) || in_array($sFileType, $ExtText) || in_array($sFileType, $Extpdf)){
                    $MaxSize = (100 * 1024) * 1024;
                }

                if(in_array($sFileType, $ExtImage)){
                    $MaxSize = (3 * 1024) * 1024;
                }

                if($FileSizeMb < $MinSize || $FileSizeMb > $MaxSize){
                    $this->_status = 1;
                    $this->_message = $message;
                }

                if($this->_status == 0 && in_array($sFileType, $ExtImage)){
                    list($width, $height) = getimagesize($File['tmp_name']);
                    $iMinDimension = 1500;
                    $iMaxDimension = 2500;
                    if($width > $height){
                        if($width > $iMaxDimension || $height > $iMinDimension){
                            $this->_status = 1;
                            $this->_message = $message;
                        }
                    }else{
                        if($height > $iMaxDimension || $width > $iMinDimension){
                            $this->_status = 1;
                            $this->_message = $message;
                        }
                    }
                }
            }
        }

        if($this->_status == 0){
            $Path .= '/' . $File['name'];
            $ParamUpload = array(
                'path'       => $Path,
                'autorename' => true,
                'mode'       => array(
                    '.tag' => 'overwrite'
                )
            );

            $header = array(
                'Content-Type: application/octet-stream',
                'Dropbox-API-Arg: ' . json_encode($ParamUpload),
            );


            $data      = array( 'path' => $Path );
            $CheckFile = $this->_callCurl($this->_TemporaryLink, $data);

            if(isset($CheckFile['error'])){
                $Response = $this->_callCurlUpload($this->_Upload, null, $header, 'upload_file', 'POST');
                if(is_array($Response) && !empty($Response)){
                    $this->_validateDataDropbox($Response);
                    $Response['.tag'] = 'file';
                    $view = new View($this->request, $this->response, null);
                    $view->set('ListFolder', $Response);
                    $view->set('Project', $Project);
                    $view->set('project_id', $ProjectId);
                    $view->set('user_id', $UserId);
                    $view->set('bOwner', true);
                    $view->layout  = false;
                    $this->_data['html_item']  = $view->render('/Element/Mettings/item_asset');
                    $this->_data['response']   = $Response;
                    $this->_message = __('Upload successfully!');
                    //$this->setNotification($Project, 9);
                }else{
                    $this->_status = 1;
                    $this->_message = __('Upload failed, please try again.');
                }
            }else{
                $this->_status = 1;
                $this->_message = __('Filename already exists.');
            }
        }

        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * Get Root dropbox
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function _getRootDropbox($UserId, $ProjectId){
        if($UserId && $ProjectId){
            return $this->_NameRoot . $UserId . '/' . $ProjectId;
        }
        return false;
    }

    /**
     * Delete File Dropbox
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function deleteFileDropbox(){
        if (!$this->request->isAjax()) {
            $this->Flash->error(__('Unable to process your request.'));
            return $this->redirect('/');
        }

        $this->_status = 0;
        $aUser = $this->Auth->user();

        if(!$aUser){
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        }

        $RequestData = $this->request->data;
        if($this->_status == 0){
            if(!isset($RequestData['project_id']) || empty($RequestData['project_id'])){
                $this->_status = 1;
                $this->_message = __('Invalid Project Id.');
            }else{
                $ProjectId = $RequestData['project_id'];
                $this->loadModel('Projects');
                $Project   = $this->Projects->getProject($ProjectId);
                if(!$Project){
                    $this->_status = 1;
                    $this->_message = __('Invalid Project Id.');
                }
            }
        }
        
        if($this->_status == 0){
            if($Project->user->id != $aUser['id']){
                $this->_status = 1;
                $this->_message = __('You are not owner.');
            }else{
                $iUserId = $Project->user->id;
            }
        }

        if($this->_status == 0){
            if(!isset($RequestData['name_file']) || empty($RequestData['name_file'])){
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            }else{
                $NameFile = base64_decode($RequestData['name_file']);
            }
        }

        if($this->_status == 0){
            $Path = $this->_getRootDropbox($iUserId, $ProjectId);
            $ParamListFolder = array('path' => $Path . '/' . $NameFile);
            $Response = $this->_callCurl($this->_DeleteFile, $ParamListFolder);

            if(is_array($Response) && !empty($Response)){
                $this->_validateDataDropbox($Response);
                $Response['.tag'] = 'file';
                $this->_data['response']   = $Response;
                $this->_message = __('Delete successfully!');
            }else{
                $this->_status = 1;
                $this->_message = __('Delete failed, please try again.');
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }

    /**
     * download File Dropbox
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function downloadFileDropbox(){
        if (!$this->request->isAjax()) {
            $this->Flash->error(__('Unable to process your request.'));
            return $this->redirect('/');
        }

        $this->_status = 0;
        $aUser = $this->Auth->user();

        if(!$aUser){
            $this->_status = 1;
            $this->_message = __('Unable to process your request.');
        }

        $RequestData = $this->request->data;
        if($this->_status == 0){
            if(!isset($RequestData['project_id']) || empty($RequestData['project_id'])){
                $this->_status = 1;
                $this->_message = __('Invalid Project Id.');
            }else{
                $ProjectId = $RequestData['project_id'];
                $this->loadModel('Projects');
                $Project   = $this->Projects->getProject($ProjectId);
                if(!$Project){
                    $this->_status = 1;
                    $this->_message = __('Invalid Project Id.');
                }
            }
        }
        
        if($this->_status == 0){
            $UserId      = $aUser['id'];
            $UserIdOwner = (isset($Project->user->id) ? $Project->user->id : 0);
            if($UserIdOwner != $UserId){
                $this->loadModel('UsersProjects');
                $UserProject = $this->UsersProjects->getUserProjectsByOptions(['user_id' => $UserId, 'project_id' => $Project->id, 'status' => 1, 'type' => 2]);
                if(!$UserProject){
                    $this->_status = 1;
                    $this->_message = __('You can not download file.');
                }
            }
        }

        if($this->_status == 0){
            if(!isset($RequestData['name_file']) || empty($RequestData['name_file'])){
                $this->_status = 1;
                $this->_message = __('Unable to process your request.');
            }else{
                $NameFile = base64_decode($RequestData['name_file']);
            }
        }

        if($this->_status == 0){
            $Path = $this->_getRootDropbox($UserIdOwner, $ProjectId);
            $Path .= '/' . $NameFile;
            $data = array( 'path' => $Path );
            $Response = $this->_callCurl($this->_TemporaryLink, $data);
            if(is_array($Response) && !empty($Response)){
                $this->_data['response']   = $Response;
            }else{
                $this->_status = 1;
                $this->_message = __('Download failed, please try again.');
            }
        }
        $this->responApi($this->_status, $this->_message, $this->_data);
        die();
    }
}
