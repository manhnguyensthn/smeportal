<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Request;
use App\Lib\ProjectsLib;
use Cake\Network\Http\Client;
use Cake\View\Helper;
/**
 * Requests Controller
 *
 * @property \App\Model\Table\RequestsTable $Requests
 *
 * @method \App\Model\Entity\Request[] paginate($object = null, array $settings = [])
 */
class RequestsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
//        $this->loadHelper('XlsHelper');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public $paginate = [
        'limit' => 6,
        'order' => [
            'created' => 'DESC',
            'id' => 'DESC'
        ]
    ];

    public function index()
    {
        $this->loadModel('Requests');
        $userId = $this->Auth->user('id');
        $this->loadModel('Users');
        $adminType = $this->Users->getAdminType($userId);
        $search[] = '';
        if ($adminType == 1 || $adminType == 2 || $adminType == 3 || $adminType == 4 || $adminType == 5) {
            $requests = $this->Requests->getListRequestByAdminSME($adminType, $search);
            // echo json_encode($requests);
            if ($this->request->is('get')) {
                $group = $this->request->query('group');
                $status = $this->request->query('status');
                switch ($status) {
                    case 1:
                        $status = 0;
                        break;
                    case 2:
                        $status = 1;
                        break;
                    case 3:
                        $status = 2;
                        break;
                    default:
                        $status = 4;
                        break;
                }
                $type = $this->request->query('type1');
                switch ($type) {
                    case 1:
                        $type = 2;
                        break;
                    case 2:
                        $type = 4;
                        break;
                    case 3:
                        $type = 3;
                        break;
                    case 4:
                        $type = 1;
                        break;
                    default:
                        $type = 5;
                        break;
                }
                $location = $this->request->query('location');
                $category = $this->request->query('category');
                $role = $this->request->query('role');

                //search by projects
                $projectReceive = $this->request->query('projectReceive');
                $requestsByProject = [];
                if (!empty($projectReceive)) {
                    $ProjectsTable = TableRegistry::get('Projects');
                    $projects = $ProjectsTable->find('all', ['conditions' => ['title' => $projectReceive], 'fields' => 'id'])->first();
                    if(!empty($projects)){
                        $searchproject = $projects->id;
                        $RequestsProjectsTable = TableRegistry::get('RequestsProjects');
                        $requests_projects = $RequestsProjectsTable->find('all', ['conditions' => ['project_id' => $searchproject], 'fields' => 'request_id']);
                        $searchkey = [];
                        $i = 1;
                        foreach ($requests_projects as $key => $value) {
                            $request_id = $value->request_id;

                            if(!in_array($request_id, $searchkey)){
                                array_push($searchkey,$request_id);
                                $i = $i +1;
                            }
                        }
                        $search['Requests.id in'] = $searchkey;
                    }else{
                        $search['Requests.id in'] = 0;
                    }

                }

                $projectSend = $this->request->query('projectSend');
                if(!empty($projectSend)){
                    $ProjectsTable = TableRegistry::get('Projects');
                    $projects = $ProjectsTable->find('all', ['conditions' => ['title' => $projectSend], 'fields' => 'id'])->first();
                    if(!empty($projects)){
                        $searchproject = $projects->id;
                        $search['Requests.project_id'] = $searchproject;
                    }else{
                        $search['Requests.project_id'] = 0;
                    }
                }
                //end search by projects

                if (!empty($group && isset($group))) {
                    $search['receiver_id2'] = $group;
                }
                if (isset($status) && $status != 4) {
                    $search['Requests.status'] = $status;
                }
                if (!empty($type) && $type != 5) {
                    $search['Requests.type'] = $type;
                }
                if (!empty($location) && isset($location)) {
                    $search['receiver_id4'] = $location;
                }
                if (!empty($category) && isset($category)) {
                    $search['receiver_id1'] = $category;
                }
                if (!empty($role) && isset($role)) {
                    $search['receiver_id3'] = $role;
                }
                $requests = $this->Requests->getListRequestByAdminSME($adminType, $search);
            }
            /*Draw chart*/
            //QL
            $manage = $this->Requests->getCountRequests(['type' => 1, 'status' => 0]);
            $manageProcessing = $this->Requests->getCountRequests(['type' => 1, 'status' => 1]);
            $manageProcessed = $this->Requests->getCountRequests(['type' => 1, 'status' => 2]);
            //Phapche
            $juridical = $this->Requests->getCountRequests(['type' => 2, 'status' => 0]);
            $juridicalProcessing = $this->Requests->getCountRequests(['type' => 2, 'status' => 1]);
            $juridicalProcessed = $this->Requests->getCountRequests(['type' => 2, 'status' => 2]);
            //XTTM
            $commerce = $this->Requests->getCountRequests(['type' => 3, 'status' => 0]);
            $commerceProcessing = $this->Requests->getCountRequests(['type' => 3, 'status' => 1]);
            $commerceProcessed = $this->Requests->getCountRequests(['type' => 3, 'status' => 2]);
            //VV
            $loans = $this->Requests->getCountRequests(['type' => 4, 'status' => 0]);
            $loansProcessing = $this->Requests->getCountRequests(['type' => 4, 'status' => 1]);
            $loansProcessed = $this->Requests->getCountRequests(['type' => 4, 'status' => 2]);
            //filter
            $GroupTable = TableRegistry::get('Groups');
            $groups = $GroupTable->find('list', ['limit' => 200, 'field' => 'name']);
            $CategoriesTable = TableRegistry::get('Categories');
            $categories = $CategoriesTable->find('list', ['limit' => 200, 'field' => 'name']);
            $UsersType = TableRegistry::get('UsersType');
            $usersType = $UsersType->find('list', ['limit' => 200, 'field' => 'name']);
            $LocationsTable = TableRegistry::get('districts');
            $locations = $LocationsTable->find('list', [
                'conditions' => ['country_id' => 235],
                'order' => ['name' => 'ASC']
            ]);
            //end filter
            $attribute[] = array('adminType', 'manage', 'manageProcessing', 'manageProcessed', 'juridical', 'juridicalProcessing', 'juridicalProcessed', 'commerce'
            , 'commerceProcessing', 'commerceProcessed', 'loans', 'loansProcessing', 'loansProcessed', 'groups', 'usersType', 'locations', 'categories', 'requests');

            /*End Draw chart*/

            $this->set(compact($attribute));
            $this->set([
                'requests' => $this->paginate($requests),
                '_serialize' => ['requests']
            ]);
            // $this->set('_serialize', ['requests']);
        } else { //normal user
            $search = [];
            if ($this->request->is('get')) {
                $group = $this->request->query('group');
                $type = $this->request->query('type1');
                switch ($type) {
                    case 1:
                        $type = 2;
                        break;
                    case 2:
                        $type = 4;
                        break;
                    case 3:
                        $type = 3;
                        break;
                    case 4:
                        $type = 1;
                        break;
                    default:
                        $type = 5;
                        break;
                }

                $location = $this->request->query('location');
                $category = $this->request->query('category');
                $role = $this->request->query('role');
                //search by projects
                $projectReceive = $this->request->query('projectReceive');
                $requestsByProject = [];
                if (!empty($projectReceive)) {
                    $ProjectsTable = TableRegistry::get('Projects');
                    $_projects = $ProjectsTable->find('all', ['conditions' => ['title' => $projectReceive], 'fields' => 'id'])->first();
                    if(!empty($_projects)) {
                    $searchproject = $_projects->id;
                    $RequestsProjectsTable = TableRegistry::get('RequestsProjects');
                    $requests_projects = $RequestsProjectsTable->find('all', ['conditions' => ['project_id' => $searchproject], 'fields' => 'request_id']);
                    $searchkey = [];
                    $i = 1;
                    foreach ($requests_projects as $key => $value) {
                        $request_id = $value->request_id;

                        if(!in_array($request_id, $searchkey)){
                            array_push($searchkey,$request_id);
                            $i = $i +1;
                        }

                    }
                    $search['Requests.id in'] = $searchkey;
                } else    { 
                    $search['Requests.id in'] = 0;
                }      
                }

                $projectSend = $this->request->query('projectSend');
                if(!empty($projectSend)){
                    $ProjectsTable = TableRegistry::get('Projects');
                    $projects = $ProjectsTable->find('all', ['conditions' => ['title' => $projectSend], 'fields' => 'id'])->first();
                    if(!empty($projects)){
                        $searchproject = $projects->id;
                        $search['Requests.project_id'] = $searchproject;
                    }else{
                        $search['Requests.project_id'] = 0;
                    }
                }

                //end search by projects
                if (!empty($group && isset($group))) {
                    $search['receiver_id2'] = $group;
                }
                if (isset($status) && $status != 4) {
                    $search['Requests.status'] = $status;
                }
                if (!empty($type) && $type != 5) {
                    $search['Requests.type'] = $type;
                }
                if (!empty($location) && isset($location)) {
                    $search['receiver_id4'] = $location;
                }
                if (!empty($category) && isset($category)) {
                    $search['receiver_id1'] = $category;
                }
                if (!empty($role) && isset($role)) {
                    $search['receiver_id3'] = $role;
                }
            }
            $requests = $this->Requests->getListRequestUser($userId, $search);
            $project = [];
            if ($requests->first() != null) {
                foreach ($requests as $items) {
                    $project_id[] = $items->project_id;
                }

                $ProjectsTable = TableRegistry::get('Projects');
                foreach ($project_id as $item) {
                    $project = $ProjectsTable->getProjectByID($item);
                    $projects[] = $project;
                }
            }
            $CategoriesTable = TableRegistry::get('Categories');
            $categories = $CategoriesTable->find('list', ['limit' => 200, 'field' => 'name']);
            $GroupTable = TableRegistry::get('Groups');
            $groups = $GroupTable->find('list', ['limit' => 200, 'field' => 'name']);

            $UsersType = TableRegistry::get('UsersType');
            $usersType = $UsersType->find('list', ['limit' => 200, 'field' => 'name']);

            $LocationsTable = TableRegistry::get('districts');
            $locations = $LocationsTable->find('list', [
                'conditions' => ['country_id' => 235],
                'order' => ['name' => 'ASC']
            ]);
            //end filter
            $this->set([
                'requests' => $this->paginate($requests),
                '_serialize' => ['requests']
            ]);

            $this->set(compact('requests', 'project', 'groups', 'usersType', 'locations', 'categories'));

            $this->set('title', __('Danh sách yêu cầu/kiến nghị'));
            $this->render('list_request');
        }

        $this->set('title', __('Danh sách yêu cầu/kiến nghị'));
    }


    /**
     * View method
     *
     * @param string|null $id Request id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $request = $this->Requests->get($id);
        $project_id = $request->project_id;

        // var_dump($request);

        $this->loadModel('Projects');
        $projectSend = $this->Projects->getProjectByID($project_id);
        $projectReceiver = $this->Projects->getProjectByRequest($id);
        // echo json_encode($projectReceiver);
        // var_dump($id);
        $this->set(compact('projectSend', 'projectReceiver'));
        $this->set('request', $request);
        $this->set('_serialize', ['request']);
        $this->set('title', __('Xem yêu cầu/kiến nghị'));
    }

    /*
 * Add method
 *
 * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
 */

    public function create($id = null)
    {
        $this->loadModel('Requests');
        $this->loadModel('Projects');
        $this->loadModel('Requests_Projects');
        $this->loadModel('Users');
        $this->set('title', __('Thêm yêu cầu'));
        //find current logged user
        $user = $this->Auth->user();
        $userid = $user['id'];
        $user1 = $this->Users->find('all', [
            'conditions' => ['id' => $userid],
            'fields' => ['id', 'email', 'activated', 'admin']
        ])->first();
        if ($id) {
            $request = $this->Requests->find('all', ['conditions' => ['id' => $id]])->first();
            // var_dump($user1);
            if (($request['user_id'] != $user['id']) && ($user1->admin == 0)) {
                // echo $user1->admin;
                $this->Flash->error(__('Bạn không thể sửa yêu cầu'));
                // return $this->redirect('/');
            }
        } else {
            $request = $this->Requests->newEntity();
        }

        //action when form submit
        if ($this->request->is('post')) {
            if (isset($_POST['title'])) {
                ini_set('max_execution_time', 300);
            }
            $img_url = '';
            $f_url = '';

            //attach files
            if (isset($this->request->data['attach1'])) {
                $fileOK = $this->attachfile('img/attach', $this->request->data['attach1']);
                if (!empty($fileOK['url'])) {
                    $file_url1 = ROOT_URL . $fileOK['url'];
                }
                // var_dump($fileOK);
                if (!empty($fileOK['errors'])) {
                    // $this->Flash->error("Định dạng file không hợp lệ!");
                    $this->Flash->error($fileOK['errors']);
                    // die();
                    return $this->redirect(['action' => 'create']);
                }
            }
            if (!empty($this->request->data['attach2'])) {

                $fileOK1 = $this->attachfile('img/attach', $this->request->data['attach2']);
                if (!empty($fileOK1['url'])) {
                    $file_url2 = ROOT_URL . $fileOK1['url'];
                }
                if (!empty($fileOK1['errors'])) {
                    // $this->Flash->error("Định dạng file không hợp lệ!");
                    $this->Flash->error($fileOK1['errors']);
                    // die();
                    return $this->redirect(['action' => 'create']);
                }
            }


            //set file url to save
            if (isset($file_url1)) {
                $this->request->data['attach1'] = $file_url1;
            } else {
                $this->request->data['attach1'] = $f_url;
            }
            if (isset($file_url2)) {
                $this->request->data['attach2'] = $file_url2;
            } else {
                $this->request->data['attach2'] = $f_url;
            }
            $this->request->data['user_id'] = $userid;


            //receiver
            $receiver1 = $this->request->data('receiver_id1');
            $receiver2 = $this->request->data('receiver_id2');
            $receiver3 = $this->request->data('receiver_id3');
            $receiver4 = $this->request->data('receiver_id4');
            $receiver5 = $this->request->data('receiver_id5');

            if ((!empty($receiver1)) || (!empty($receiver2)) || (!empty($receiver3)) || (!empty($receiver4)) || (!empty($receiver5))) {
                switch ($this->request->data('type')) {

                    //type 1 = QLHV
                    case 0:
                        $type = 1;
                        break;
                    //type 2 = Phap Ly
                    case 1:
                        $type = 2;
                        break;
                    //type3 = XTTM
                    case 2:
                        $type = 3;
                        break;
                    //type4 = Von vay
                    case 3:
                        $type = 4;
                        break;

                    default:
                        # code...
                        break;
                }
                // set type to save
                $this->request->data['type'] = $type;
                //check if receiver is specific / find it
                if (!empty($receiver5)) {
                    $prjid = $this->Projects->find('all', ['conditions' => ['title' => $receiver5], 'fields' => 'id'])->first();
                    if (empty($prjid)) {
                        $this->Flash->error(__("Không tìm thấy doanh nghiệp"));
                        return $this->redirect(['action' => 'create']);
                    }
                    $this->request->data['receiver_id5'] = $prjid->id;
                }
                $request = $this->Requests->patchEntity($request, $this->request->data);

                //save new record to dtb
                $request->created = date('Y-m-d H:i:s');
                if ($this->Requests->save($request)) {
                    $ProjectsTable = TableRegistry::get('Projects');
                    $UsersTable = TableRegistry::get('Users');

                    //save all pairs of sender receiver projects/user to table request_project
                    if (!empty($receiver1)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['category_id' => $receiver1]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                                }
                            }
                        }
                    }
                    if (!empty($receiver2)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['group_id' => $receiver2]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));

                                }
                            }
                        }
                    }

                    if (!empty($receiver4)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['district_id' => $receiver4]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                                }
                            }
                        }
                    }
                    if (!empty($receiver5)) {
                        $projects = $ProjectsTable->find('all', ['conditions' => ['title' => $receiver5]]);
                        if (!empty($projects)) {
                            foreach ($projects as $project) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['project_id'] = $project->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                                }
                            }
                        }
                    }
                    if (!empty($receiver3)) {
                        $roles = $this->Users->find('all', ['conditions' => ['users_type_id' => $receiver3]]);
                        if (!empty($roles)) {
                            foreach ($roles as $role) {
                                $receiver = $this->Requests_Projects->newEntity();
                                $receiverdata['request_id'] = $request->id;
                                $receiverdata['user_id'] = $role->id;
                                $receiv = $this->Requests_Projects->patchEntity($receiver, $receiverdata);
                                if ($this->Requests_Projects->save($receiv)) {
                                    // $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                                }
                            }
                        }
                    }
				//find corresponding admin
                    $admins = TableRegistry::get('Users');
                    $admins1 = $this->Users->find('all', [
                        'conditions' => ['admin' => $type],
                        'fields' => ['id', 'email', 'admin']
                    ])->first();
                    $isSendNotificationByMobile = FALSE;
                    $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                    $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $admins1->id]])->first();
                    if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                    }
                    //send notification
          
                    $project_id = $this->request->data('project_id');
                    $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                    $actionType = 21;
                    $notificationsTable = TableRegistry::get('Notifications');
                    $notificationData = $notificationsTable->newEntity();
                    $notificationData->user_id = $admins1->id;
                    $notificationData->action_user_id = $user['id'];
                    $notificationData->project_id = $this->request->data('project_id');
                    $notificationData->option_data = $optionData;
                    $notificationData->action_type = $actionType;
                    $notificationData->read_flg = 0;
                    // echo $this->request->data('project_id');
                    $notificationData->created = date('Y-m-d H:i:s');
                    // var_dump($notificationData);
                    if ($isSendNotificationByMobile == TRUE) {
                    if (!$notificationsTable->save($notificationData)) {
                        $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                    }
                    }
                    $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $admins1->id, $user['id']);
                    $this->Flash->success(__('Tạo yêu cầu mới thành công!'));
                    return $this->redirect(['action' => 'list_send']);

                } else {
                    $this->Flash->error(__('Không thể lưu.'));
                }
            } else {
                $this->Flash->error('Phải có người nhận');
            }
        }
//        $projects = $this->Requests->Projects->find('list', array('conditions' => array('user_id' => $userid)));
        $projects = $this->Requests->Projects->find('all',['conditions'=>['user_id'=>$userid]]);
        $users = $this->Requests->Users->find('list', ['limit' => 200]);
        $ScalesTable = TableRegistry::get('groups');
        $scales = $ScalesTable->find('list', ['limit' => 200, 'field' => 'name']);
        $CategoriesTable = TableRegistry::get('categories');
        $categories = $CategoriesTable->find('list', ['limit' => 200, 'field' => 'name']);
        $QuantitiesTable = TableRegistry::get('Users_type');
        $quantities = $QuantitiesTable->find('list', ['limit' => 200, 'field' => 'name']);
        $LocationsTable = TableRegistry::get('districts');
        $locations = $LocationsTable->find('list', [
            'conditions' => ['country_id' => 235],
            'order' => ['name' => 'ASC']
        ]);

        $admin = $user1->admin;
        $this->set(compact('projects', 'request', 'projects', 'users', 'admin', 'scales', 'quantities', 'locations', 'categories'));
        $this->set('_serialize', ['request']);
    }

    public function respond($id = null, $accept = 0)
    {
        // accept 0 = pass, accept 1 = fail
        $accept = $this->request->data('accept');
        $this->loadModel('Requests');
        $this->loadModel('Requests_Projects');
        $this->loadModel('Projects');
        $this->set('title', __('Thêm yêu cầu'));

        //find current user
        $user = $this->Auth->user();
        $userid = $user['id'];
        $user1 = $this->Users->find('all', [
            'conditions' => ['id' => $userid],
            'fields' => ['id', 'name', 'email', 'activated', 'admin']
        ])->first();
        $type = '';
        // check is_admin?
        if ($user1->admin != 0) {
            $request = $this->Requests->get($id, [
                'contain' => ['Projects']
            ]);
            switch ($request->type) {
                case 1:
                    $type = "Quản lý hội viên";
                    break;
                case 2:
                    $type = "Pháp Lý";
                    break;
                case 3:
                    $type = "Xúc tiến";
                    break;
                case 4:
                    $type = "Vốn vay";
                    break;

                default:
                    # code...
                    break;
            }
            $title = $request['project']->title;
            $project_id = $request->project_id;
        } else {
            $this->redirect("/");
        }
        //action when submit form
        if ($this->request->is(['patch', 'post', 'put'])) {
            //check admintype != normal user and mod
            if ($user1->admin == 1 || $user1->admin == 2 || $user1->admin == 3 || $user1->admin == 4) {
                ini_set('max_execution_time', 300);
                $f_url = '';
                $data = $this->request->data;
                // echo $accept;
                // die();
                if ($accept == 0) {
                    //change status from 0 -> 1
                    $data['status'] = 1;

                    //check reply attachment
                    if (isset($this->request->data['repattach1'])) {
                        $fileOK = $this->attachfile('img/attach', $this->request->data['repattach1']);
                        if (!empty($fileOK['url'])) {
                            $file_url1 = ROOT_URL . $fileOK['url'];
                        }
                        if (!empty($fileOK['errors'])) {
                            $this->Flash->error($fileOK['errors']);
                            return $this->redirect(['controller' => 'Requests', 'action' => 'respond', $id]);
                        }
                    }
                    if (isset($this->request->data['repattach2'])) {
                        $fileOK1 = $this->attachfile('img/attach', $this->request->data['repattach2']);
                        if (!empty($fileOK['url'])) {
                            $file_url2 = ROOT_URL . $fileOK1['url'];
                        }
                        if (!empty($fileOK1['errors'])) {
                            $this->Flash->error($fileOK1['errors']);
                            return $this->redirect(['controller' => 'Requests', 'action' => 'respond', $id]);
                        }
                    }

                    //check url to save
                    if (isset($file_url1)) {
                        $data['repattach1'] = $file_url1;
                    } else {
                        $data['repattach1'] = $f_url;
                    }
                    if (isset($file_url2)) {
                        $data['repattach2'] = $file_url2;
                    } else {
                        $data['repattach2'] = $f_url;
                    }

                    //determine which admin accept the request
                    $data['admin_id'] = $user1->id;
                    $request = $this->Requests->patchEntity($request, $data);
                    if ($this->Requests->save($request)) {
                        $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 
                            'user_id' => $request->user_id]);
                        $admins = TableRegistry::get('Users');
                        $admins1 = $this->Users->find('all', [
                            'conditions' => ['admin' => 5],
                            'fields' => ['id', 'email', 'admin']
                        ])->first();
                        //push notification
                        $actionType = 22;
                        $notificationsTable = TableRegistry::get('Notifications');
                        $notificationData = $notificationsTable->newEntity();
                        $notificationData->user_id = $admins1->id;
                        $notificationData->action_user_id = $user['id'];
                        $notificationData->project_id = $project_id;
                        $notificationData->option_data = $optionData;
                        $notificationData->action_type = $actionType;
                        $notificationData->read_flg = 0;
                        $notificationData->created = date('Y-m-d H:i:s');
                        if (!$notificationsTable->save($notificationData)) {
                            $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                        }
                        $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $admins1->id, $user['id']);
                        $this->Flash->success(__('Lưu yêu cầu thành công.'));
                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The request could not be saved. Please, try again.'));
                } // if admin reject
                else {
                    //set status = 0
                    $data['status'] = 3;

                    //check rep attach
                    if (!empty($this->request->data['attach1'])) {
                        $fileOK = $this->attachfile('img/attach', $this->request->data['repattach1']);
                        if (!empty($fileOK['url'])) {
                            $file_url1 = ROOT_URL . $fileOK['url'];
                        }
                        if (!empty($fileOK['errors'])) {
                            $this->Flash->error($fileOK['errors']);
                            return $this->redirect(['action' => 'create']);
                        }
                    }


                    if (!empty($this->request->data['attach1'])) {
                        $fileOK1 = $this->attachfile('img/attach', $this->request->data['repattach2']);
                        if (!empty($fileOK['url'])) {
                            $file_url2 = ROOT_URL . $fileOK1['url'];
                        }
                        if (!empty($fileOK1['errors'])) {
                            $this->Flash->error($fileOK1['errors']);
                            return $this->redirect(['action' => 'create']);
                        }
                    }

                    if (isset($file_url1)) {
                        $data['repattach1'] = $file_url1;
                    } else {
                        $data['repattach1'] = $f_url;
                    }
                    if (isset($file_url2)) {
                        $data['repattach2'] = $file_url2;
                    } else {
                        $data['repattach2'] = $f_url;
                    }
                    // $data['user_id'] = $request->id;
                    $data['admin_id'] = $user1->id;
                    $request = $this->Requests->patchEntity($request, $data);
                    if ($this->Requests->save($request)) {
                        $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                        $admins = TableRegistry::get('Users');
                        // $admins1 =  $admins->find('all',array('condition'=>['admin'=> $type]))->first();
                        $admins1 = $this->Users->find('all', [
                            'conditions' => ['admin' => 5],
                            'fields' => ['id', 'email', 'admin']
                        ])->first();

                        $actionType = 23;
                        $notificationsTable = TableRegistry::get('Notifications');
                        $notificationData = $notificationsTable->newEntity();
                        $notificationData->user_id = $request->user_id;
                        $notificationData->action_user_id = $user['id'];
                        $notificationData->project_id = $project_id;
                        $notificationData->option_data = $optionData;
                        $notificationData->action_type = $actionType;  // Type of notification join project
                        $notificationData->read_flg = 0;
                        $notificationData->created = date('Y-m-d H:i:s');
                        // echo json_encode($notificationData);
                        // die();
                        if (!$notificationsTable->save($notificationData)) {
                            $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                        }
                        $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $request->user_id, $user['id']);
                        $this->Flash->success(__('Lưu yêu cầu thành công.'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The request could not be saved. Please, try again.'));
                    }
                }
            }

            //mod respond
            if ($user1->admin == 5 && $request->status == 1) {
                $data = $this->request->data;
                //mod accept
                if ($accept == 0) {
                    if (!empty($this->request->data('repcontent2'))) {
                        $data['status'] = 2;
                    }

                    $request = $this->Requests->patchEntity($request, $data);
                    if ($this->Requests->save($request)) {
                        $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 
                            'user_id' => $request->user_id]);
                        $actionType = 24;
                        $test = [];
                        //find all the receivers from table requests_projects
                        $receivers = $this->Requests_Projects->find('all', ['conditions' => ['request_id' => $request->id], 'fields' => ['request_id', 'project_id', 'user_id'], 'group' => ['project_id', 'user_id']]);
                        if (!empty($receivers)) {

                            foreach ($receivers as $receiver) {
                                $sender = '';
                                if (!empty($receiver->project_id) || !empty($receiver->user_id)) {
                                    if (!empty($receiver->project_id)) {
                                        //project that receive the request
                                        $receivep = $receiver->project_id;
                                        //find the owner of project
                                        $pnotis = $this->Projects->find('all', ['conditions' => ['id' => $receivep], 'fields' => ['user_id']]);
                                        if (!empty($pnotis)) {
                                            $pnotis = $pnotis->first()->user_id;
                                            //filter duplicate
                                            if (!in_array($pnotis, $test)) {
                                                array_push($test, $pnotis);
                                                $sender = $pnotis;
                                            }

                                            //push notification
                                            if (!empty($sender)) {
                                                $notificationsTable = TableRegistry::get('Notifications');
                                                $notificationData = $notificationsTable->newEntity();
                                                $notificationData->project_id = $project_id;
                                                $notificationData->option_data = $optionData;
                                                $notificationData->action_type = $actionType;
                                                $notificationData->read_flg = 0;
                                                $notificationData->created = date('Y-m-d H:i:s');
                                                $notificationData->user_id = $pnotis;
                                                $notificationData->action_user_id = $request->user_id;
                                                if (!$notificationsTable->save($notificationData)) {
                                                    $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                                                }
                                                $this->PushNoti($actionType, ['project_id' => $receivep, 'request_id' => $request->id], $receivep, $pnotis);
                                            }
                                        }
                                    } //if send to specific user_id (case send to role)
                                    else {
                                        $pnotis = $receiver->user_id;
                                        $sender = $pnotis;
                                        $actionType = 25;
                                        if (!empty($sender)) {
                                            $notificationsTable = TableRegistry::get('Notifications');
                                            $notificationData = $notificationsTable->newEntity();
                                            $notificationData->project_id = $project_id;
                                            $notificationData->option_data = $optionData;
                                            $notificationData->action_type = $actionType;  // Type of notification join project
                                            $notificationData->read_flg = 0;
                                            $notificationData->created = date('Y-m-d H:i:s');
                                            $notificationData->user_id = $pnotis;
                                            $notificationData->action_user_id = $request->user_id;
                                            if (!$notificationsTable->save($notificationData)) {
                                                $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                                            }
                                            $this->PushNoti($actionType, ['user_id' => $receiver->user_id, 'request_id' => $request->id], $receiver->user_id, $pnotis);
                                        }
                                    }
                                }
                            }
                        }
                        $this->Flash->success(__('Lưu yêu cầu thành công.'));
                        return $this->redirect(['action' => 'index']);
                    } else {

                        $this->Flash->error(__('The request could not be saved. Please, try again.'));
                    }
                } // mod reject
                else {
                    //set status = 0 => admin re-consider
                    if (!empty($this->request->data('repcontent2'))) {
                        $data['status'] = 0;
                    }
                    $request = $this->Requests->patchEntity($request, $data);
                    //push notification to corresponding admin
                    if ($this->Requests->save($request)) {
                        $optionData = json_encode(['project_id' => $project_id, 'request_id' => $request->id, 'user_id' => $request->user_id]);
                        $actionType = 23;
                        $notificationsTable = TableRegistry::get('Notifications');
                        $notificationData = $notificationsTable->newEntity();
                        $notificationData->user_id = $request->admin_id;
                        $notificationData->action_user_id = $user['id'];
                        $notificationData->project_id = $project_id;
                        $notificationData->option_data = $optionData;
                        $notificationData->action_type = $actionType;
                        $notificationData->read_flg = 0;
                        $notificationData->created = date('Y-m-d H:i:s');
                        if (!$notificationsTable->save($notificationData)) {
                            $this->Flash->error(__('Can not save notification to creator. Please try again laster.'));
                        }
                        $this->PushNoti($actionType, ['project_id' => $project_id, 'request_id' => $request->id], $request->admin_id, $user['id']);
                        $this->Flash->success(__('Lưu yêu cầu thành công.'));
                        return $this->redirect(['action' => 'list_send']);
                    }
                    $this->Flash->error(__('Không thể lưu yêu cầu, xin thử lại'));
                }
            } //not mod/ request status # 1
            else {
                $this->Flash->error(__('Bạn không có quyền thao tác với yêu cầu này.'));
                return $this->redirect(['action' => 'index']);
            }
        }
        //end form

        $attach1 = $this->request->attach1;
        $attach2 = $this->request->attach2;
        $admin = $user1->admin;
        $GroupsTable = TableRegistry::get('groups');
        $groups = $GroupsTable->find('list', ['limit' => 200, 'field' => 'name']);
        $CategoriesTable = TableRegistry::get('categories');
        $categories = $CategoriesTable->find('list', ['limit' => 200, 'field' => 'name']);
        $QuantitiesTable = TableRegistry::get('users_type');
        $quantities = $QuantitiesTable->find('list', ['limit' => 200, 'field' => 'name']);
        $LocationsTable = TableRegistry::get('districts');
        $locations = $LocationsTable->find('list', [
            'conditions' => ['id' => $request->receiver_id4],
            // 'order' => ['name' => 'ASC'],
            'fields' => 'name'
        ]);
        $proj_receivs = TableRegistry::get('projects');
        $proj_receiv = '';
        if (!empty($request->receiver_id5)) {
            $proj_receiv = $proj_receivs->find('all', [
                'conditions' => ['id' => $request->receiver_id5],
                'fields' => 'title'])->first()->title;
            if (empty($proj_receiv))
                $proj_receiv = '';
        }

        $this->set(compact('projects', 'proj_receiv', 'type', 'title', 'request', 'user1', 'admin', 'attach1,attach2', 'groups', 'categories', 'quantities', 'locations'));
        $this->set('_serialize', ['request']);

    }

    public function add()
    {
        $request = $this->Requests->newEntity();
        if ($this->request->is('post')) {
            $request = $this->Requests->patchEntity($request, $this->request->data);
            $request->project_id = 1;
            $request->user_id = $this->Auth->user('id');
            // $this->Request->data['project_id'] = 1;
            // $this->Request->data['user_id'] = $this->Auth->user('id');
            $this->uploadFiles('img/uploads', $this->request->data['attach1']);
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Yêu cầu thêm thành công.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Không thể thêm yêu cầu, vui lòng thử lại !'));
        }

        $users = $this->Requests->Users->find('list', ['limit' => 200]);
        $userID = $this->Auth->user('id');
        $ownerProjects = $this->Requests->Projects->find('all')->where(['user_id' => $userID]);
        $ProjectsTable = TableRegistry::get('Projects');
        $projects = $ProjectsTable->find('list', ['fields' => ['id', 'title'], 'keyField' => 'id', 'valueField' => 'title'])->toArray();
        // $scales = $ScalesTable->find('list', ['fields' => ['id', 'name'], 'keyField' => 'id', 'valueField' => 'name'])->toArray();
        $this->set(compact('request', 'projects', 'users', 'ownerProjects'));
        $this->set('_serialize', ['request']);
        $this->set('title', __('Thêm yêu cầu/kiến nghị'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Request id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $request = $this->Requests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $request = $this->Requests->patchEntity($request, $this->request->getData());
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Lưu yêu cầu thành công.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The request could not be saved. Please, try again.'));
        }
        $projects = $this->Requests->Projects->find('list', ['limit' => 200]);
        $users = $this->Requests->Users->find('list', ['limit' => 200]);
        $this->set(compact('request', 'projects', 'users'));
        $this->set('_serialize', ['request']);
        $this->set('title', __('Sửa yêu cầu/kiến nghị'));
    }


    public function listRequest()
    {
        $userId = $this->Auth->user('id');
        $this->loadModel('Requests');
        $requests = $this->Requests->getListRequestUser($userId);
        $this->set(compact('requests'));
        $this->set('_serialize', ['requests']);
        $this->set('title', __('Danh sách yêu cầu/kiến nghị'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Request id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $request = $this->Requests->get($id);
        if ($this->Requests->delete($request)) {
            $this->Flash->success(__('The request has been deleted.'));
        } else {
            $this->Flash->error(__('The request could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function list_send()
    {
        $userId = $this->Auth->user('id');
        if ($this->request->is('get')) {
            $group = $this->request->query('group');
            $search[] = '';
            $this->loadModel('Requests');
            $requestOwner = $this->Requests->getListOwnerRequestByUser($userId, $search);
            $status = $this->request->query('status');
            switch ($status) {
                case 1:
                    $status = 0;
                    break;
                case 2:
                    $status = 1;
                    break;
                case 3:
                    $status = 2;
                    break;
                default:
                    $status = 4;
                    break;
            }
            $type = $this->request->query('type1');
            switch ($type) {
                case 1:
                    $type = 2;
                    break;
                case 2:
                    $type = 4;
                    break;
                case 3:
                    $type = 3;
                    break;
                case 4:
                    $type = 1;
                    break;
                default:
                    $type = 5;
                    break;
            }

            $location = $this->request->query('location');
            $category = $this->request->query('category');
            $role = $this->request->query('role');
            // var_dump($this->request->data);
            // // echo $status;
            // $search = [];

            //search by projects
            $projectReceive = $this->request->query('projectReceive');
            $requestsByProject = [];
            if (!empty($projectReceive)) {
                $ProjectsTable = TableRegistry::get('Projects');
                $projects = $ProjectsTable->find('all', ['conditions' => ['title' => $projectReceive], 'fields' => 'id'])->first();
                if(!empty($projects)) {
                $searchproject = $projects->id;
                $RequestsProjectsTable = TableRegistry::get('RequestsProjects');
                $requests_projects = $RequestsProjectsTable->find('all', ['conditions' => ['project_id' => $searchproject], 'fields' => 'request_id']);
                $searchkey = [];
                $i = 1;
                foreach ($requests_projects as $key => $value) {
                    $request_id = $value->request_id;

                    if(!in_array($request_id, $searchkey)){
                        array_push($searchkey,$request_id);
                        $i = $i +1;
                    }

                }
                $search['Requests.id in'] = $searchkey;
                } else {
                     $search['Requests.id in'] = 0;
                }
            }

            $projectSend = $this->request->query('projectSend');
                if(!empty($projectSend)){
                    $ProjectsTable = TableRegistry::get('Projects');
                    $projects = $ProjectsTable->find('all', ['conditions' => ['title' => $projectSend], 'fields' => 'id'])->first();
                    if(!empty($projects)){
                        $searchproject = $projects->id;
                        $search['Requests.project_id'] = $searchproject;
                    }else{
                        $search['Requests.project_id'] = 0;
                    }
                }
            //end search by projects

            if (!empty($group && isset($group))) {
                $search['receiver_id2'] = $group;
            }
            if (isset($status) && $status != 4) {
                $search['Requests.status'] = $status;
            }
            if (!empty($type) && $type != 5) {
                $search['Requests.type'] = $type;
            }
            if (!empty($location) && isset($location)) {
                $search['receiver_id4'] = $location;
            }
            if (!empty($category) && isset($category)) {
                $search['receiver_id1'] = $category;
            }
            if (!empty($role) && isset($role)) {
                $search['receiver_id3'] = $role;
            }
            $requestOwner = $this->Requests->getListOwnerRequestByUser($userId, $search);
        }
        $CategoriesTable = TableRegistry::get('Categories');
        $categories = $CategoriesTable->find('list', ['limit' => 200, 'field' => 'name']);
        $GroupTable = TableRegistry::get('Groups');
        $groups = $GroupTable->find('list', ['limit' => 200, 'field' => 'name']);

        $UsersType = TableRegistry::get('UsersType');
        $usersType = $UsersType->find('list', ['limit' => 200, 'field' => 'name']);

        $LocationsTable = TableRegistry::get('districts');
        $locations = $LocationsTable->find('list', [
            'conditions' => ['country_id' => 235],
            'order' => ['name' => 'ASC']
        ]);
        //end filter
        $this->set([
            'requests' => $this->paginate($requestOwner),
            '_serialize' => ['requestOwner']
        ]);

        $this->set(compact('requestOwner', 'project', 'groups', 'usersType', 'locations', 'categories'));

        $this->set('title', __('Danh sách yêu cầu/kiến nghị đã gửi'));
        $this->render('list_owner_request');

    }



}