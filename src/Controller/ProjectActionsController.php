<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Request;
use App\Lib\ProjectsLib;
use Cake\Network\Http\Client;

//use App\Lib\phpFlickr;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class ProjectActionsController extends AppController {

    public $paginate = array();
    public $helpers = array('Paginator');
    public $components = array('PushNotification');

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadModel('Projects');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['share']);
    }

    // Coder: Giang Dien
    // Add action like, follow to system (type: 1 -> like; 2 -> follow)
    public function addActionToSystem() {
         if ($_REQUEST) {
            $type = $_REQUEST['type'];
            $project_id = $_REQUEST['project_id'];
            $user = $this->Auth->user();
			$this->loadModel('Projects');
            $currentProject = $this->Projects->find('all', ['conditions' => ['Projects.id' => $project_id ]])->contain('Users')->first();  
		    $blocksTable = TableRegistry::get('Blocks');	
			$block = $blocksTable->find()
						->where(['user_id' => $currentProject->user_id, 'blocked_id' => $user['id']])
						->first();
			$blockuser = $blocksTable->find()
						->where(['user_id' => $user['id'] , 'blocked_id' => $currentProject->user_id])
						->first();			
			if(!empty($block) || !empty($blockuser ))
			{
				if($type == 2)
				{
					 $result = ['result' => 0, 'status' => 0, 'message' => __('Could not follow this project')];  
				}
				else if($type == 1)
				{
					 $result = ['result' => 0, 'status' => 0, 'message' => __('Could not like this project')];  
				}
				  echo json_encode($result);
				 die();
			}
	
		    if (!empty($user)) {
                $this->loadModel('UserProjectActions');
                $projectAction = $this->UserProjectActions->getUserProjectAction(['user_id' => $user['id'], 'project_id' => $project_id, 'action_type' => $type]);
                if (empty($projectAction)) {
                    $dataAction = ['action_type' => $type, 'project_id' => $project_id, 'user_id' => $user['id'], 'created' => date('Y-m-d H:i:s')];
                    $respone = $this->UserProjectActions->addProjectAction($dataAction);
                    if ($respone == TRUE) {
                        $result = ['result' => 1, 'status' => 1, 'message' => __('Save action success')];
                        echo json_encode($result);
                        // Send notification to collaborators
                        $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                        $this->loadModel('UsersProjects');
                        $this->loadModel('Users');
                        $this->loadModel('Projects');
                        $currentProject = $this->Projects->find('all', ['conditions' => ['Projects.id' => $project_id]])->contain('Users')->first();
                        if($user['id'] != $currentProject->user_id)
						{
						if ($type == 2) {
                            // Follow Action
                            $this->sendNotificationForUserFollow(8, ['project_id' => $project_id, 'user_action_id' => $currentProject->user_id, 'action_type' => 8],$user['id']);
							$this->PushNoti(8, ['project_id' => $project_id, 'user_action_id' => $user['id'], 'action_type' => 8],$currentProject->user_id, $user['id']);				
						}
						else if($type == 1)
						{
							  $this->sendNotificationForUserFollow(7, ['project_id' => $project_id, 'user_action_id' => $currentProject->user_id, 'action_type' => 8],$user['id']);
							  $this->PushNoti(7, ['project_id' => $project_id, 'user_action_id' => $user['id'], 'action_type' => 8],$currentProject->user_id, $user['id']);
						}
                        $creatorUserNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $currentProject->user_id]])->first();
                        $actionType = ($type == 1) ? 7 : 8;
                        $optionData = json_encode(['project_id' => $project_id]);
						if (!empty($creatorUserNotificationSetting) && $creatorUserNotificationSetting->project_update_mobile_status == 1) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $currentProject->user_id;
                            $notificationData->action_user_id = $user['id'];
                            $notificationData->project_id = intval($project_id);
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType, ['project_id' => $project_id, 'user_action_id' => $user['id'], 'action_type' => $actionType], $currentProject->user_id, $user['id']);
                        }
                        if (!empty($creatorUserNotificationSetting) && $creatorUserNotificationSetting->project_update_email_status == 1) {
                            // send notification via mobile
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $user['first_name'] . ' ' . $user['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $currentProject->user->email, ['first_name' => $currentProject->user->first_name, 'last_name' => $currentProject->user->last_name, 'notification_content' => $contentMail], $subject);
                        }
						}
                    } else {
                        $result = ['result' => 0, 'status' => 0, 'message' => __('Save action fail')];
                        echo json_encode($result);
                    }
                } else {
                    $status = ($projectAction['status'] == 1) ? 0 : 1;
                    $respone = $this->UserProjectActions->updateProjectAction($projectAction['id'], ['status' => $status]);
                    if ($respone == TRUE) {
                        $result = ['result' => 1, 'status' => $status, 'message' => __('Save action success')];
                        echo json_encode($result);
                    } else {
                        $result = ['result' => 0, 'status' => 0, 'message' => __('Save action fail')];
                        echo json_encode($result);
                    }
                }
            } else {
                $result = ['result' => 0, 'status' => 0, 'message' => __('Please login before execute action')];
                echo json_encode($result);
            }
        } else {
            $result = ['result' => 0, 'status' => 0, 'message' => __('No match data')];
            echo json_encode($result);
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 13-01-2017
    // Function: send notification for collaborators
    public function sendNotificationForCollaborator() {
        if ($this->request->is('post')) {
            $projectId = $this->request->data['project_id'];
            $actionType = $this->request->data['action_type'];
			$comment_id = $this->request->data['comment_id'];
			$this->sendNotificationForFollowProject($projectId, $actionType,$comment_id);
            $userAction = $this->Auth->user();
			if($actionType == 9 || $actionType == 10)
			{
			$this->sendNotificationForUserFollow($actionType, ['project_id' => $projectId], $userAction['id']);
			}
            $this->loadModel('UsersProjects');
            $listCollaborators = $this->UsersProjects->getUserProjectsJoin(['project_id' => $projectId, 'status' => 1, 'type' => 2]);
            if (count($listCollaborators) > 0) {
                $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                foreach ($listCollaborators as $collaborator) {
                    if ($collaborator->user_id != $userAction['id']) {
                        $userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $collaborator->user_id]])->first();
                        $optionData = json_encode(['project_id' => $collaborator->project_id, 'comment_id' => $comment_id]);
                        $isSendNotificationByMail = FALSE;
                        $isSendNotificationByMobile = FALSE;
                        // check notification setting
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
                            $isSendNotificationByMail = TRUE;
                        }
                        if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
                            $isSendNotificationByMobile = TRUE;
                        }
                        if ($isSendNotificationByMobile == TRUE) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $collaborator->user_id;
                            $notificationData->action_user_id = $userAction['id'];
                            $notificationData->project_id = $collaborator->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
							$this->PushNoti($actionType, ['project_id' => $collaborator->project_id] , $collaborator->user_id, $userAction['id']);	                   
                        }
                        if ($isSendNotificationByMail == TRUE) {
                            // send notification via mobile
                            $userTable = TableRegistry::get('Users');
                            $userInfo = $userTable->find('all', ['conditions' => ['id' => $collaborator->user_id]])->first();
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $userAction['first_name'] . ' ' . $userAction['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
                }
                $responeData = ['status' => 1, 'message' => __('Success')];
                echo json_encode($responeData);
                die;
            }
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }



    // Coder: Giang Dien
    // Date: 25/10/2016
    // Delete an update of project
    public function deleteUpdateById() {
        if ($this->request->is(['post'])) {
            $id = $this->request->data['id'];
            $this->loadModel('ProjectUpdates');
            $update = $this->ProjectUpdates->get($id);
            $result = $this->ProjectUpdates->delete($update);
            echo $result;
        } else {
            echo __('No match data');
        }
        die;
    }
    // Coder: Giang Dien
    // Date: 25/10/2016
    // Add updates data into list
    public function addUpdatesAjax() {
        if ($this->request->is(['post'])) {
            if (isset($_POST['token']) && $_POST['token'] == session_id()) {
                $this->loadModel('ProjectUpdates');
                $this->request->data['created'] = date('Y-m-d H:i:s');
                $respone = $this->ProjectUpdates->addUpdate($this->request->data);
                if ($respone == 'TRUE') {
                    $result = ['status' => 1, 'message' => __('You just updated the project.')];
                    echo json_encode($result);
                } else {
                    $message = '';
                    foreach ($respone as $row) {
                        foreach ($row as $item) {
                            $message .= $item . '<br/>';
                        }
                    }
                    $result = ['status' => 0, 'message' => $message];
                    echo json_encode($result);
                }
            } else {
                echo __('No match data');
            }
        } else {
            echo __('No match data');
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 2016-10-26
    // Get form reply comment via ajax
    public function getFormReplyComment() {
        if ($this->request->is(['post'])) {
            $user = $this->Auth->user();
            $project_id = $this->request->data['project_id'];
            $comment_id = $this->request->data['comment_id'];
            $userId = $user['id'];
            $this->loadModel('Users');
            $user = $this->Users->get($userId);
            $this->set([
                'project_id' => $project_id,
                'comment_id' => $comment_id,
                'user' => $user,
                '_serialize' => ['project_id', 'comment_id', 'user']
            ]);
        } else {
            echo __('No match data');
            die;
        }
        $this->viewBuilder()->layout('ajax');
    }

    // Coder: Giang Dien
    // Date: 2016-10-26
    // Add new comment via ajax
    public function addCommentsAjax() {
        if ($this->request->is(['post'])) {
            $data = $this->request->data;
            $data['created'] = date('Y-m-d H:i:s');
            $this->loadModel('ProjectComments');
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ipRequest = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ipRequest = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ipRequest = $_SERVER['REMOTE_ADDR'];
            }
            $data['ip_comment'] = $ipRequest;
            $user = $this->Auth->user();
            $data['user_id'] = $user['id'];
            $respone = $this->ProjectComments->addComment($data);
            if ($respone == 'TRUE') {
                $result = ['status' => 1, 'message' => __('You just commented the project.')];
                echo json_encode($result);
                $projectTable = TableRegistry::get('Projects');
                $currentProject = $projectTable->get($data['project_id']);
                $this->sendNotificationForUserFollow(2, ['project_id' => $data['project_id'], 'user_action_id' => $user['id'], 'action_type' => 2], $user['id']);
                $userTable = TableRegistry::get('Users');
				//Send notification Creator
				if (!empty($currentProject)) {		
						$actionType = 2;
						$userAction = $this->Auth->user();
						
						$userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
							if ($currentProject->user_id != $userAction['id']) {	
								$userNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $currentProject->user_id]])->first();
								$optionData = json_encode(['project_id' =>$data['project_id']]);
								$isSendNotificationByMail = FALSE;
								$isSendNotificationByMobile = FALSE;
								// check notification setting
								if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_email_status == 1) {
									$isSendNotificationByMail = TRUE;
								}
								if (!empty($userNotificationSetting) && $userNotificationSetting->project_update_mobile_status == 1) {
									$isSendNotificationByMobile = TRUE;
								}
								if ($isSendNotificationByMobile == TRUE) {
									// add notification on web
									$notificationTable = TableRegistry::get('Notifications');
									$notificationData = $notificationTable->newEntity();
									$notificationData->user_id = $currentProject->user_id;
									$notificationData->action_user_id = $userAction['id'];
									$notificationData->project_id = $data['project_id'];
									$notificationData->option_data = $optionData;
									$notificationData->action_type = $actionType;
									$notificationData->created = date('Y-m-d H:i:s');
									$notificationTable->save($notificationData);
									$this->PushNoti($actionType, ['project_id' => $data['project_id']] , $currentProject->user_id, $userAction['id']);	                   
								}
								if ($isSendNotificationByMail == TRUE) {
									// send notification via mobile
									$userTable = TableRegistry::get('Users');
									$userInfo = $userTable->find('all', ['conditions' => ['id' => $currentProject->user_id]])->first();
									$template = 'notification';
									$from = [EMAIL_LOGIN => __('We The Projects')];
									$subject = __('[SME] Notification on SME');

									// GET CONTENT MAIL
									$contentMail = __('Send notification content');
									if (isset($actionType) && !empty($actionType)) {
										$name = $userAction['first_name'] . ' ' . $userAction['last_name'];
										$contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
									}
									// END: GET CONTENT MAIL

									$this->sendMail($template, $from, $userInfo->email, ['first_name' => $userInfo->first_name, 'last_name' => $userInfo->last_name, 'notification_content' => $contentMail], $subject);
								}
							}
					
				} 
				
                // Send notification
                if ($data['parent_comment_id'] != 0) {
                    // Reply comment
                    $commentReply = $this->ProjectComments->find('all', ['conditions' => ['id' => $data['parent_comment_id']]])->first();
                 
					if (!empty($commentReply)) {
                        // add notification to user comment reply
                        $userCommentReply = $userTable->get($commentReply->user_id);
                        $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                        $userCurrentNotificationSetting = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $commentReply->user_id]])->first();

                        $optionData = json_encode(['project_id' => $commentReply->project_id, 'comment_id' => $data['parent_comment_id']]);
                        $actionType = 6;
			if($user['id'] != $currentProject->user_id)
			{
                        if (!empty($userCurrentNotificationSetting) && $userCurrentNotificationSetting->project_update_mobile_status == 1) {
                            // add notification on web
                            $notificationTable = TableRegistry::get('Notifications');
                            $notificationData = $notificationTable->newEntity();
                            $notificationData->user_id = $currentProject->user_id;
                            $notificationData->action_user_id = $user['id'];
                            $notificationData->project_id = $commentReply->project_id;
                            $notificationData->option_data = $optionData;
                            $notificationData->action_type = $actionType;
                            $notificationData->created = date('Y-m-d H:i:s');
                            $notificationTable->save($notificationData);
			    $this->PushNoti($actionType, ['project_id' => $commentReply->project_id] , $currentProject->user_id, $user['id']);	                   
								
                        }

                        if (!empty($userCurrentNotificationSetting) && $userCurrentNotificationSetting->project_update_email_status == 1) {
                            // send notification via mobile
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $contentMail = __('Send  notification content');
                            if (isset($actionType) && !empty($actionType)) {
                                $name = $user['first_name'] . ' ' . $user['last_name'];
                                $contentMail = $this->getContentNotifySendMail($name, $actionType, $optionData);
                            }
                            // END: GET CONTENT MAIL

                            $this->sendMail($template, $from, $userCommentReply->email, ['first_name' => $userCommentReply->first_name, 'last_name' => $userCommentReply->last_name, 'notification_content' => $contentMail], $subject);
                        }
                    }
					}
                }
            } else {
                $message = '';
                foreach ($respone as $row) {
                    foreach ($row as $item) {
                        $message .= $item . '<br/>';
                    }
                }
                $result = ['status' => 0, 'message' => $message];
                echo json_encode($result);
            }
        } else {
            echo __('No match data');
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 2016-10-26
    // Delete comment via ajax
    public function deleteCommentAjax() {
        if ($this->request->is(['post'])) {
            $id = $this->request->data['comment_id'];
            $this->loadModel('ProjectComments');
            $comment = $this->ProjectComments->get($id);
            $result = $this->ProjectComments->delete($comment);
            echo $result;
        } else {
            echo __('No match data');
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 2016-10-31
    // Iframe content when share on other website
    public function share($id = NULL) {
        $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain(['Users'])->first();
        if (!empty($project['country_id'])) {
            $this->loadModel('Countries');
            $country = $this->Countries->get($project['country_id']);
        } else {
            $country = [];
        }
        if (!empty($project['state_id'])) {
            $this->loadModel('States');
            $state = $this->States->get($project['state_id']);
        } else {
            $state = [];
        }
        if (!empty($project['category_id'])) {
            $this->loadModel('Categories');
            $category = $this->Categories->get($project['category_id']);
        } else {
            $category = [];
        }
        if (!empty($project)) {
            $this->set([
                'project' => $project,
                'country' => $country,
                'state' => $state,
                'category' => $category,
                '_serilize' => ['project', 'country', 'state', 'category']
            ]);
            $this->viewBuilder()->layout('ajax');
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 29-12-2016
    // Function: Accept Joined Role
    public function acceptJoinRole() {
        if (isset($_GET['project_role_id']) && !empty($_GET['project_role_id']) && isset($_GET['user_action_id']) && !empty($_GET['user_action_id'])) {
            $projectRoleId = $_GET['project_role_id'];
            $ProjectRoleTable = TableRegistry::get('ProjectsRoles');
            $roleInfo = $ProjectRoleTable->find('all', ['conditions' => ['ProjectsRoles.id' => $projectRoleId]])->contain(['Projects'])->first();
            if (!empty($roleInfo)) {
                // Send notification for collaborator
                $userProjectTable = TableRegistry::get('UsersProjects');
                $collaborators = $userProjectTable->find('all', ['conditions' => ['project_id' => $roleInfo->project_id, 'UsersProjects.type' => 2, 'type' => 2, 'UsersProjects.status' => 1]])->contain(['Users']);
                $roleCounter = $this->_getNumberCollaboratorByRole($collaborators, $roleInfo->id);
                $this->loadModel('Notifications');
                if ($roleCounter < $roleInfo->quantity) {
                    $ProjectRoleTable->updateAll(['is_read' => 0], ['project_id' => $roleInfo->project_id, 'role_id' => $roleInfo->role_id]);
                    $optionData = ['project_id' => $roleInfo->project_id, 'project_role_id' => $projectRoleId, 'role_id' => $roleInfo->role_id];
                    $userTable = TableRegistry::get('Users');
                    $user = $userTable->get($_GET['user_action_id']);
                    $link = \Cake\Utility\Inflector::slug($roleInfo->project['title'], '-');
                    $link = '/projects/' . $link . '-' . $roleInfo->project_id;
                    foreach ($collaborators as $row) {
                        $userNotificationSettingTable = TableRegistry::get('UserNotificationSetting');
                        $userNotification = $userNotificationSettingTable->find('all', ['conditions' => ['user_id' => $row->user_id]])->first();
                        if (isset($userNotification->project_update_email_status) && $userNotification->project_update_email_status == 1) {
                            $template = 'notification';
                            $from = [EMAIL_LOGIN => __('We The Projects')];
                            $subject = __('[SME] Notification on SME');

                            // GET CONTENT MAIL
                            $name = $user->first_name . ' ' . $user->last_name;
                            $contentMail = $this->getContentNotifySendMail($name, 4, json_encode($optionData));
                            // END: GET CONTENT MAIL

                            if (!$this->sendMail($template, $from, $row['user']['email'], ['first_name' => $row['user']->first_name, 'last_name' => $row['user']->last_name, 'notification_content' => $contentMail], $subject)) {
                                $this->Flash->error(__('Send mail to creator fail.'));
                            }
                        }
                        if (isset($userNotification->project_update_mobile_status) && $userNotification->project_update_mobile_status == 1) {
                            $notificationData = $this->Notifications->newEntity();
                            $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' => $row->user_id, 'action_user_id' => $_GET['user_action_id'], 'option_data' => json_encode($optionData), 'action_type' => 4, 'read_flg' => 0, 'project_id' => $roleInfo->project_id, 'created' => date('Y-m-d H:i:s')]);
                            if (!$this->Notifications->save($newNotification)) {
                                $this->Flash->error(__('Add notification fail.'));
                            }
                        }
                    }
                    // End send notification for collaborator

                    $actionUser = $userTable->get($roleInfo->project['user_id']);
                    $notificationData = $this->Notifications->newEntity();
                    $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' => $user['id'], 'action_user_id' => $actionUser->id, 'option_data' => json_encode($optionData), 'action_type' => 5, 'read_flg' => 0, 'project_id' => $roleInfo->project_id, 'created' => date('Y-m-d H:i:s')]);
                    if (!$this->Notifications->save($newNotification)) {
                        $this->Flash->error(__('Add notification fail.'));
                    }
                    // Change status user project
                    $userProject = $userProjectTable->find('all', ['conditions' => ['user_id' => $_GET['user_action_id'], 'project_id' => $roleInfo->project_id, 'role_id' => $roleInfo->role_id]])->first();
                    $userProject->status = 1;
                    if ($userProjectTable->save($userProject)) {
                        $this->Flash->success(__('You have accepted a collaborator to join your project.'));
                    } else {
                        $this->Flash->error(__('Change status fail.'));
                    }
                    return $this->redirect($link);
                    die;
                } else {
                    $this->Flash->error(__('Change status fail.'));
                    return $this->redirect('/');
                    die;
                }
            } else {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            }
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    private function _getNumberCollaboratorByRole($list = [], $project_role_id = 0) {
        $counter = 0;
        foreach ($list as $row) {
            if ($row->project_role_id == $project_role_id) {
                $counter++;
            }
        }
        return $counter;
    }

    // Coder: Giang Dien
    // Date: 29-12-2016
    // Function: Deny Joined Role
    public function denyJoinRole() {
        if (isset($_GET['project_role_id']) && !empty($_GET['project_role_id']) && isset($_GET['user_action_id']) && !empty($_GET['user_action_id'])) {
            $projectRoleId = $_GET['project_role_id'];
            $ProjectRoleTable = TableRegistry::get('ProjectsRoles');
            $roleInfo = $ProjectRoleTable->find('all', ['conditions' => ['ProjectsRoles.id' => $projectRoleId]])->contain(['Projects'])->first();
            if (!empty($roleInfo)) {
                $this->loadModel('Notifications');
                $optionData = ['project_id' => $roleInfo->project_id, 'project_role_id' => $projectRoleId, 'role_id' => $roleInfo->role_id];
                $link = \Cake\Utility\Inflector::slug($roleInfo->project['title'], '-');
                $link = '/projects/' . $link . '-' . $roleInfo->project_id;
                $ProjectRoleTable->updateAll(['is_read' => 0], ['project_id' => $roleInfo->project_id, 'role_id' => $roleInfo->role_id]);
                $userTable = TableRegistry::get('Users');
                $user = $userTable->get($roleInfo->project['user_id']);
                $actionUser = $userTable->get($_GET['user_action_id']);
                $notificationData = $this->Notifications->newEntity();
                $newNotification = $this->Notifications->patchEntity($notificationData, ['user_id' => $actionUser->id, 'action_user_id' => $user['id'], 'option_data' => json_encode($optionData), 'action_type' => 11, 'read_flg' => 0, 'project_id' => $roleInfo->project_id, 'created' => date('Y-m-d H:i:s')]);
                if (!$this->Notifications->save($newNotification)) {
                    $this->Flash->error(__('Add notification fail.'));
                }
                $this->Flash->success(__('You have dennied a collaborator to join your project.'));
                return $this->redirect($link);
                die;
            } else {
                $this->Flash->error(__('No match data'));
                return $this->redirect('/');
            }
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

}
