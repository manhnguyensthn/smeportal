<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;


class EmailComponent extends Component {
    public $_emailFrom = "no-reply@vsick.xyz";
    public $_emailName = "We the projects";
    
    public function sendmail($to = "" , $name = "", $title = "", $body = "") {
        $email = new Email();
        $email->transport('default');
        try {
            $email->from([$this->_emailFrom => $this->_emailName])
                  ->to([$to => $name])
                  ->subject($title)                   
                  ->send($body);

        } catch (Exception $e) {

            echo 'Exception : ',  $e->getMessage(), "\n";

        }
    }
    
    
}