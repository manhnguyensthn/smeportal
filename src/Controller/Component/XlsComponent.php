<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class XlsComponent extends Component
{
    /*Export data to excel file, format *.xls */
    public function exportProject($model)
    {
        $GroupsTable = TableRegistry::get('Groups');
        $CategoriesTable = TableRegistry::get('Categories');
        $DistrictsTable = TableRegistry::get('Districts');
        $UsersTable = TableRegistry::get('Users');
        $ScalesTable = TableRegistry::get('Scales');
        $QuantitiesTable = TableRegistry::get('Quantities');
        $ProjectTable = TableRegistry::get('Projects');
        $UsersProjectsTable = TableRegistry::get('UsersProjects');
        $RolesTable = TableRegistry::get('Roles');
        $projects = $ProjectTable->find('all');

        $group_id = [];
        $category_id = [];
        $district_id = [];
        $user_id = [];
        $dataResponse = [];
        foreach ($projects as $list) {
            array_push($group_id, $list['group_id']);
            array_push($category_id, $list['category_id']);
            array_push($district_id, $list['district_id']);
            array_push($user_id, $list['user_id']);
            $value['id'] = $list['id'];
            $value['TieuDe'] = $list['title'];
            $groups = $GroupsTable->find('all', ['conditions' => ['id IN' => $list['group_id']], 'field' => 'name'])->first();
            $value['BanChapHanh'] = $groups['name'];
            $categories = $CategoriesTable->find('all', ['conditions' => ['id IN' => $list['category_id']], 'fields' => 'name'])->first();
            $value['LinhVuc'] = $categories['name'];
            $users = $UsersTable->find('all', ['conditions' => ['id IN' => $list['user_id']]])->first();
            $value['NguoiTao'] = $users['last_name'] . " " . $users['first_name'];
            if ($list['fee_status'] == 0)
                $value['HoiPhi'] = "Chua dong";
            else
                $value['HoiPhi'] = "Da dong";
            $scale = $ScalesTable->find('all', ['conditions' => ['id IN' => $list['scale_id']]])->first();
            $value['QuyMoVon'] = $scale['name'];
            $quantity = $QuantitiesTable->find('all', ['conditions' => ['id IN' => $list['quantity_id']]])->first();
            $value['QuyMoLaoDong'] = $quantity['name'];
            if(!empty($list['district_id'])){
                $districts = $DistrictsTable->find('all', ['conditions' => ['id IN' => $list['district_id']]])->first();
                $value['KhuVuc'] = $districts['name'];
            }
            $value['NgayTao'] = $list['created'];
            $value['BanQuanTri'] = "";
            if(!empty($list['id'])){
                $project_role = $UsersProjectsTable->find('all', ['conditions' => ['project_id IN' => $list['id']]]);
            }
            if (!empty($project_role)) {
                foreach ($project_role as $items) {
                    if(!empty($items['role_id'])){
                        $role = $RolesTable->find('all', ['conditions' => ['id' => $items['role_id']]])->first();
                    }
                    if(!empty($items['user_id'])){
                        $user_role = $UsersTable->find('all', ['conditions' => ['id' => $items['user_id']]])->first();
                    }
                    if(!empty($role)&&!empty($user_role)){
                        $value['BanQuanTri'] .= $role['role'] . " - " . $user_role['last_name'] . " " . $user_role['first_name'] . "<br>";
                    }
                }
            }
            $dataResponse[] = $value;
        }
//        $groups = $GroupsTable->find('all', ['conditions' => ['id IN' => $group_id]]);
//        $categories = $CategoriesTable->find('all', ['conditions' => ['id IN' => $category_id]]);
//        $districts = $DistrictsTable->find('all', ['conditions' => ['id IN' => $district_id]]);
//        $users = $UsersTable->find('all', ['conditions' => ['id IN' => $user_id]]);
//        $number = $projects->count();
        if (!empty($dataResponse)) {
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=DoanhNghiep.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo '<meta charset="utf-8" />';
            echo '<table border="1" id="content-table">';
            echo '<thead>';
            echo '<tr>';
            echo '<th>ID</th>';
            echo '<th>Tiêu đề</th>';
            echo '<th>Ban chấp hành</th>';
            echo '<th>Lĩnh vực</th>';
            echo '<th>Người tạo</th>';
            echo '<th>Khu Vực</th>';
            echo '<th>Quy Mô Vốn</th>';
            echo '<th>Quy Mô Lao Động</th>';
            echo '<th>Ban Quản Trị</th>';
            echo '<th>Hội phí</th>';
            echo '<th>Ngày Tạo</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach ($dataResponse as $data) {
                echo '<tr>';
                echo '<td>' . $data['id'] . '</td>';
                echo '<td>' . $data['TieuDe'] . '</td>';
                echo '<td>' . $data['BanChapHanh'] . '</td>';
                echo '<td>' . $data['LinhVuc'] . '</td>';
                echo '<td>' . $data['NguoiTao'] . '</td>';
                echo '<td>' . $data['KhuVuc'] . '</td>';
                echo '<td>' . $data['QuyMoVon'] . '</td>';
                echo '<td>' . $data['QuyMoLaoDong'] . '</td>';
                echo '<td>' . $data['BanQuanTri'] . '</td>';
                echo '<td>' . $data['HoiPhi'] . '</td>';
                echo '<td>' . $data['NgayTao'] . '</td>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
            exit();
        }
    }

    protected $_defaultConfig = [];
}

?>

