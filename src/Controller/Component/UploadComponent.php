<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Component;

use Cake\Controller\Component;

class UploadComponent extends Component {
    
    
    function up($image = null)
    {
        if(empty($image)) {
            return FALSE;
        }
        $files = array();
        $fileName = '';
        $fileType = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

        $ext = array('gif', 'jpeg', 'png', 'jpg');
        $folder = 'documents';
        if (!empty($image)) {
            if ($image['size'] > 2000000) {
                $errors = "Image size is more than 2 MB";
            } else if ($image['error'] == 1 && $image['size'] == 0 && $image['name'] != '') {
                $errors = "Image size is more than 2 MB";
            } else if (!in_array($fileType, $ext) && $image['name'] != '') {
                $errors = 'Please select file in jpg, jpeg, gif, png format.';
            } else {
                $errors = '';
            }

            if ($errors == '') {
                $uploadDir = WWW_ROOT . 'img/uploads';
                if (!file_exists($uploadDir) || !is_dir($uploadDir)) {
                    mkdir($uploadDir, 0777);
                }
                move_uploaded_file($image['tmp_name'], $uploadDir . DS . $image['name']);
            }
            $files['errors'] = $errors;
            $files['fileName'] = $fileName;
        }
        return $files;
    }
    public function resize($width, $height, $imageArray)
    {
        # Get original image x y
        $exif = exif_read_data($imageArray['tmp_name'], 0, true);
        $orientation = 1;
        if(isset($exif['IFD0']['Orientation'])){
            $orientation = $exif['IFD0']['Orientation'];
        }
        list($w, $h) = getimagesize($imageArray['tmp_name']);
        # calculate new image size with ratio
        $ratio = min($width / $w, $height / $h);
        # new file name
        $new_h = ceil($h * $ratio);
        $new_w = ceil($w * $ratio);
        $imageNameScale = $this->randomNumber(10) . '-' . $width . 'x' . $height . '-' . $imageArray['name'];
        $path = WWW_ROOT . 'upload/images/' . $imageNameScale;
        # read binary data from image file
        $imgString = file_get_contents($imageArray['tmp_name']);
        # create image from string
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($new_w, $new_h);

        imagecopyresampled($tmp, $image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);
         // Fix Orientation
        switch($orientation) {
            case 3:
                $tmp = imagerotate($tmp, 180, 0);
                break;
            case 6:
                $tmp = imagerotate($tmp, -90, 0);
                break;
            case 8:
                $tmp = imagerotate($tmp, 90, 0);
                break;
        }
        # Save image
        switch ($imageArray['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        # cleanup memory
        imagedestroy($image);
        imagedestroy($tmp);
        return $imageNameScale;
    }
}