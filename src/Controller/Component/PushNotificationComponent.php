<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Component;

use Cake\Controller\Component;

require_once ROOT . '/src/Lib/ApnsPHP/Autoload.php';

class PushNotificationComponent extends Component {

    public function send($deviceToken, $text = '', $options = []) {
        if (empty($deviceToken)) {
            return false;
        }

        if (isset($options['env'])) {
            $push = new \ApnsPHP_Push(
                    \ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, SERVER_CERTIFICATES_PRODUCT
            );
        } else {
            $push = new \ApnsPHP_Push(
                    \ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, SERVER_CERTIFICATES_SANBOX
            );
        }
        $push->setProviderCertificatePassphrase(CERTIFICATE_PASSPHRASE);

        try {
            $push->connect();
            if (is_array($deviceToken)) {
                foreach ($deviceToken as $token) {
                    $this->addPush($push, $token, $text, $options);
                }
            } else {
                $this->addPush($push, $deviceToken, $text, $options);
            }

            $push->send();
            $push->disconnect();
            $error = $push->getErrors();
            if (empty($error)) {
                return true;
            }
            return false;
        } catch (ApnsPHP_Exception $e) {
            $this->log($e->getMessage());
        }
    }

    public function addPush($push, $token, $text, $options = []) {
        $message = new \ApnsPHP_Message($token);
        $alert = (object) array(
                    'body' => $text,
                    // 'action-loc-key'    =>  'View',
                    'actions' => array(
                        (object) array(
                            'id' => 'delete',
                            'title' => 'Delete'
                        ),
                        (object) array(
                            'id' => 'reply',
                            'title' => 'Reply'
                        )
                    )
        );
        $message->setCategory(array(
            (object) array(
                'identifier' => 'reply',
                'title' => 'Reply'
            )
                )
        );

        $message->setText($text);
        if (isset($options['badge'])) {
            if (is_array($options['badge'])) {
                if (isset($options['badge'][$token])) {
                    $options['data']['badge'] = $options['badge'][$token];
                    $message->setBadge((int) $options['badge'][$token]);
                }
            } else {
                $message->setBadge((int) $options['badge']);
            }
        }
        if (isset($options['identifier'])) {
            $message->setCustomIdentifier($options['identifier']);
        }
        if (isset($options['sound'])) {
            $message->setSound($options['sound']);
        } else {
            $message->setSound('default');
        }
        if (isset($options['data'])) {
            $message->setCustomProperty('data', $options['data']);
        }
        $push->add($message);
    }

}
