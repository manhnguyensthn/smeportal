<?php

namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;
use Facebook;
use Cake\ORM\TableRegistry;
//App::import('LIBS', 'Google_Client', array('file' => 'libs' . DS . 'Google' . DS . 'src'. DS. 'Google_Client.php'));
/**
 * Token Controller
 *
 * @property \App\Model\Table\TokenTable $Token
 */
class TokenController extends AppController {
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['index','ajaxSNS','ajaxLinked','login_google','checkHtml']);
    }
    public function ajaxSNS(){
        $this->autoRender = FALSE;
        if ($this->request->is('post')) {
            $this->Users = TableRegistry::get('Users');
            if($this->Users->checkExitEmail($this->request->data['email'])) {
                $user = $this->Users->getUserbyEmail($this->request->data['email']);
                $exArr = [];
                if($this->request->data['sns'] == 1) { // facebook
                    $fb_id = $this->request->data['sns_id'];
                    $exArr =['fb_id' => "$fb_id"];
                }elseif ($this->request->data['sns'] == 2) { // linkedIn
                    $linked_id = $this->request->data['sns_id'];
                    $exArr =['linked_id' => "$linked_id"];
                }elseif ($this->request->data['sns'] == 3) {
                    $google_id = $this->request->data['sns_id'];
                    $exArr =['google_id' => "$google_id"];
                }
                $this->Users->updateAll(
                    $exArr, // fields
                    ['id' => $user->id]
                ); // conditions
                $this->Auth->setUser($user);
                echo json_encode(array('ret' => 'OK'));
                exit;
            }  else {
                if($this->request->data['sns'] == 1) { // facebook
                    $this->request->data['fb_id'] = $this->request->data['sns_id'];
                }elseif ($this->request->data['sns'] == 2) { // linkedIn
                    $this->request->data['linked_id'] = $this->request->data['sns_id'];
                }elseif ($this->request->data['sns'] == 3) {
                    $this->request->data['google_id'] = $this->request->data['sns_id'];
                }
                $users = $this->Users->newEntity();
                $users = $this->Users->patchEntity($users, $this->request->data);
                if ($this->Users->save($users)) {
                    $template = 'register';
                    $from = [EMAIL_LOGIN => 'We The Projects'];
                    $subject = __('[SME] Welcome');
                    if ($this->sendMail($template, $from, $this->request->data['email'], [], $subject)) {
                        $this->Auth->setUser($users);
                        echo json_encode(array('ret' => 'OK'));
                        exit;
                    }
                } else {
                    echo json_encode(array('ret' => 'NG'));
                    exit;
                }
            }
        }
        
    }
    public function login_google(){
        $this->autoRender = false;
        /*******Google ******/
        require_once(ROOT . DS . 'vendor' . DS  . 'Google' . DS . 'config.php');
        require_once(ROOT . DS . 'vendor' . DS  . 'Google' . DS . 'Google_Client.php');
        require_once(ROOT . DS . 'vendor' . DS  . 'Google' . DS . 'contrib' . DS . 'Google_PlusService.php');
        require_once(ROOT . DS . 'vendor' . DS  . 'Google' . DS . 'contrib' . DS . 'Google_Oauth2Service.php');
        $client = new Google_Client();
        $client->setScopes(array('https://www.googleapis.com/auth/plus.login','https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me'));
        $client->setApprovalPrompt('auto');
        $url = $client->createAuthUrl();
        return $this->redirect($url);
    }
    
    public function login_linkedin() {
        $this->autoRender = false;
    }
    public function linkedin() {
        $this->autoRender = false;
    }
    
    /**
     * View method
     *
     * @param string|null $id Token id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $token = $this->Token->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('token', $token);
        $this->set('_serialize', ['token']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $token = $this->Token->newEntity();
        if ($this->request->is('post')) {
            $token = $this->Token->patchEntity($token, $this->request->data);
            if ($this->Token->save($token)) {
                $this->Flash->success(__('The token has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The token could not be saved. Please, try again.'));
            }
        }
        $users = $this->Token->Users->find('list', ['limit' => 200]);
        $this->set(compact('token', 'users'));
        $this->set('_serialize', ['token']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Token id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $token = $this->Token->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $token = $this->Token->patchEntity($token, $this->request->data);
            if ($this->Token->save($token)) {
                $this->Flash->success(__('The token has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The token could not be saved. Please, try again.'));
            }
        }
        $users = $this->Token->Users->find('list', ['limit' => 200]);
        $this->set(compact('token', 'users'));
        $this->set('_serialize', ['token']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Token id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $token = $this->Token->get($id);
        if ($this->Token->delete($token)) {
            $this->Flash->success(__('The token has been deleted.'));
        } else {
            $this->Flash->error(__('The token could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function checkHtml() {
        $this->autoRender = FALSE;
        if(strcmp("Hello1","Hello1")) {
            echo 'Fail';
        }  else {
            echo 'OKE';
        }
    }

}
