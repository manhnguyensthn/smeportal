<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Request;
use App\Lib\ProjectsLib;
use Cake\Network\Http\Client;

//use App\Lib\phpFlickr;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 */
class ProjectsController extends AppController
{

    public $paginate = array();
    public $helpers = array('Paginator');
    public $components = array('PushNotification');

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Xls');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'getProjectInfoByTabAjax', 'detail', 'checkRoleInProjectAjax', 'checkVideoYoutubeExit', 'getMyProjects', 'blockFilterProject']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->set('title', 'Trang chủ - Hà Nội SME');
    }

    /**
     * discover method
     *
     * @param string|null.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $project = $this->Projects->get($id, [
            'contain' => ['Categories', 'Districts', 'Countries', 'Languages', 'Users', 'Stories']
        ]);

        $this->set('project', $project);
        $this->set('_serialize', ['project']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function create($id = null)
    {
        $this->set('title', __('Create project - We the projects'));
        $this->loadModel('States');
        $this->loadModel('Districts');
        if ($id) {
            $edit = 1;
            $project = $this->Projects->find('all', ['conditions' => ['id' => $id]])->first();
            $user = $this->Auth->user();
            if ($project['user_id'] != $user['id']) {
                $this->Flash->error(__('You Not Create project'));
                return $this->redirect('/');
            }
            if (!empty($project['country_id'])) {
                $states = $this->States->find('list', ['conditions' => ['country_id' => $project['country_id']], 'fields' => ['id', 'name']])->order(['name' => 'ASC']);
            } else {
                $states = [];
            }
            if (!empty($project['state_id'])) {
                $districts = $this->Districts->find('list', ['conditions' => ['state_id' => $project['state_id']], 'fields' => ['id', 'name']])->order(['name' => 'DESC']);
            } else {
                $districts = [];
            }
        } else {
            $edit = 0;
            $project = $this->Projects->newEntity();
            $districts = [];
            $states = [];
        }
        if (isset($_POST['title'])) {
            ini_set('max_execution_time', 300); //300 seconds = 5 minutes
            $error = FALSE;
            $img_url = '';
            $image_url = trim($this->request->data['image_url']);
            if (!empty($image_url)) {
                $avatar = '';
                list($type, $image_url) = explode(';', $image_url);
                list(, $image_url) = explode(',', $image_url);
                $image_url = base64_decode($image_url);
                $imageName = $_SERVER['DOCUMENT_ROOT'] . '/webroot/img/uploads/' . time() . '.png';
                file_put_contents($imageName, $image_url);
                $this->loadModel('Resize');
                $this->Resize->load($imageName);
                // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
                if (!empty($this->request->data['image_project']['tmp_name'])) {
                    list($width, $height) = getimagesize($this->request->data['image_project']['tmp_name']);
                } else {
                    $width = 0;
                    $height = 0;
                }
                if ($width > 1920 && $height > 1080) {
                    $this->Resize->resizeImage(1920, 1080, 'auto');
                } else {
                    $this->Resize->resizeImage(795, 720, 'auto');
                }
                $name = '/img/uploads/' . time() . '.jpg';
                $imageName_new = $_SERVER['DOCUMENT_ROOT'] . '/webroot/' . $name;
                // *** 3) Save image
                $this->Resize->saveImage($imageName_new, 1000);
                unlink($imageName);
            }
            if (!isset($name)) {
                if ($this->request->data['image_project']['error'] == 0) {
                    list($width, $height) = getimagesize($this->request->data['image_project']['tmp_name']);

                    if (($this->request->data['image_project']['size'] > MAX_SIZE_UPLOAD_FILE) || (!in_array($this->request->data['image_project']['type'], Configure::read('TYPE_IMAGE_VALIDATE')))) {
                        $this->Flash->error(__('Project image should be: jpeg, png, gif, bmp with min size of 720x720 resolution.'));
                        $error = true;

                    } else if ($width < 720 || $height < 720 || $width > 2500 || $height > 1500) {

                        $this->Flash->error(__('Project image should be: jpeg, png, gif, bmp with min size of 720x720 resolution.'));
                        $error = true;
                    } else {
                        $fileOK = $this->uploadFiles('img/uploads', $this->request->data['image_project']);
                        $img_url = ROOT_URL . $fileOK['url'];
                    }
                } else {
                    if (trim($this->request->data['image_project_url']) != '') {
                        if (@getimagesize(trim($this->request->data['image_project_url']))) {
                            $img_url = $this->request->data['image_project_url'];
                        } else {
                            $this->Flash->error(__('Invalid External Image URL'));
                            $error = true;
                        }
                    }
                }
            }
            // if (!empty($this->request->data['end_date']) && !empty($this->request->data['start_date']) && strtotime($this->request->data['end_date']) <= strtotime($this->request->data['start_date'])) {
            //     $error = true;
            //     $this->set('end_date_errror', 'Startdate should be greater than today and earlier than the enddate.');
            // } else {
            //     $startDateArr = explode('-', $this->request->data['start_date']);
            //     $this->request->data['start_date'] = $startDateArr[2] . '-' . $startDateArr[1] . '-' . $startDateArr[0];
            //     $endDateArr = explode('-', $this->request->data['end_date']);
            //     $this->request->data['end_date'] = $endDateArr[2] . '-' . $endDateArr[1] . '-' . $endDateArr[0];
            // }
            $video_url = $this->request->data['video_url'];
            if (!empty($video_url) && !$this->isLinkYoutube($video_url)) {
                $this->Flash->error(__('Invalid URL'));
                $project->errors('video_url', ['video_url' => ['valid-video_url' => __('Invalid Url')]], TRUE);
                $error = TRUE;
            } else {
                $project->video_url = $this->request->data['video_url'];
                $project->video_id = $this->request->data['video_id'];
            }
            if (!$error) {
                $this->request->data['user_id'] = $this->Auth->user('id');
                if (isset($name)) {
                    $this->request->data['image_url'] = ROOT_URL . $name;
                } else {
                    $this->request->data['image_url'] = $img_url;
                }
                $project = $this->Projects->patchEntity($project, $this->request->data);
                if ($this->Projects->save($project)) {
                    if (empty($id)) {
                        // Add project owner
                        $userProjectTable = TableRegistry::get('UsersProjects');
                        $userTable = TableRegistry::get('Users');
                        $currentUser = $userTable->get($this->Auth->user('id'));
                        $userProjectData = $userProjectTable->newEntity();
                        $userProjectData->user_id = $currentUser->id;
                        $userProjectData->project_id = $project->id;
                        $userProjectData->role_id = 1;
                        $userProjectData->project_role_id = 0;
                        $userProjectData->name = $currentUser->first_name . ' ' . $currentUser->last_name;
                        $userProjectData->user_picture = $currentUser->avatar;
                        $userProjectData->biography = $currentUser->biography;
                        $userProjectData->created = date('Y-m-d H:i:s');
                        $userProjectData->status = 1;
                        $userProjectData->saved = 0;
                        $userProjectData->offer = 0;
                        $userProjectData->type = 1;
                        $userProjectData->message_content = '';

                        if ($userProjectTable->save($userProjectData)) {
                            $this->sendNotificationForUserFollow(16, ['project_id' => $project->id, 'user_action_id' => $currentUser->id, 'action_type' => 16], $currentUser->id);
                            $this->Flash->success(__('Lưu thành công!'));
                            return $this->redirect(['action' => 'roles', $project->id]);
                        } else {
                            $this->Flash->error(__('Không thể lưu, vui lòng thử lại.!'));
                            return $this->redirect(['action' => 'roles', $project->id]);
                        }
                    } else {
                        $this->Flash->success(__('Lưu thành công!'));
                        return $this->redirect(['action' => 'roles', $project->id]);
                    }
                } else {
                    $this->Flash->error(__('Không thể lưu, vui lòng thử lại.'));
                }
            } else {
                $this->Flash->error(__('Không thể lưu, vui lòng thử lại..'));
            }
            if (!empty($this->request->data['country_id'])) {
                $states = $this->States->find('list', ['conditions' => ['country_id' => $this->request->data['country_id']], 'fields' => ['id', 'name']])->order(['name' => 'ASC']);
            }
            if (!empty($this->request->data['state_id'])) {
                $districts = $this->Districts->find('list', ['conditions' => ['state_id' => $this->request->data['state_id']], 'fields' => ['id', 'name']])->order(['name' => 'DESC']);
            }
        }
        $countries = $this->Projects->Countries->find('list', [['fields' => ['id', 'country_name']],
            'keyField' => 'id', 'valueField' => 'country_name'])->order(['country_name' => 'ASC']);

        $CategoriesTable = TableRegistry::get('Categories');
        $categories = $CategoriesTable->find('list', ['fields' => ['id', 'name', 'name_es', 'name_ja'], 'keyField' => 'id', 'valueField' => 'name'])->toArray();
        $ScalesTable = TableRegistry::get('Scales');
        $scales = $ScalesTable->find('list', ['fields' => ['id', 'name'], 'keyField' => 'id', 'valueField' => 'name'])->toArray();
        $QuantitiesTable = TableRegistry::get('Quantities');
        $quantities = $QuantitiesTable->find('list', ['fields' => ['id', 'name'], 'keyField' => 'id', 'valueField' => 'name'])->toArray();
        $this->set(compact('project', 'categories', 'scales', 'quantities', 'districts', 'countries', 'states', 'edit'));
        $this->set('_serialize', ['project']);
    }


    public function roles($project_id = null)
    {
        if (!$project_id) {
            return $this->redirect(['action' => 'create']);
        }
        $check = $this->checkProjectOfUser($this->Auth->user('id'), $project_id);
        if (!$check) {
            $this->Flash->error(__('You are not creator'));
            return $this->redirect(['action' => 'create']);
        }
        $errors = FALSE;
        $this->set('title', __('Add role - We the projects'));
        $this->loadModel('ProjectsRoles');
        $role = $this->ProjectsRoles->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['project_id'] = $project_id;
            $role = $this->ProjectsRoles->patchEntity($role, $this->request->data);
            $user = $this->Auth->user();
            //$this->sendNotificationForJoinProject($project_id, 20, $role->role_id);
            if ($this->ProjectsRoles->save($role)) {
                $this->Flash->success(__('Vai trò đã được thêm.'));
                $this->redirect($this->referer());
            } else {
                $errors = TRUE;
                $this->set('errors', $errors);
                $this->Flash->error(__('The project could not be saved. Please, try again.'));
            }
        }
        $roleProject = $this->ProjectsRoles->find('all')
            ->where(['project_id' => $project_id])
            ->contain(['Roles'])
            ->toArray();
        $this->loadModel('Roles');
        $a = array('GRIP 2 (MORE AS NEEDED)', 'ELECTRICIAN 2 (MORE AS NEEDED)', 'LEAD MAN', 'SET DRESSER (MORE AS NEEDED)', 'GANG BOSS', 'PA #2 (MORE AS NEEDED)');
        $roles = $this->Roles->find('list', [
            'fields' => ['id', 'role', 'role_es', 'role_ja'],
            'keyField' => 'id',
            'valueField' => 'role'
        ])->where([
            'role NOT IN' => $a
        ])->order(['role' => 'ASC'])->toArray();
        // pr($roles);
        $this->set(compact('roles', 'role', 'roleProject', 'slug_preview', 'check'));
    }

    public function deleterole()
    {
        if ($this->request->is('post')) {
            $id = $this->request->data['role_id'];
            $this->loadModel('ProjectsRoles');
            $this->request->allowMethod(['post', 'delete']);
            $role = $this->ProjectsRoles->get($id);
            $this->loadModel('UsersProjects');
            $listUserProjects = $this->UsersProjects->find('all', ['conditions' => ['project_id' => $role->project_id, 'role_id' => $role->role_id]]);
            foreach ($listUserProjects as $row) {
                $this->UsersProjects->delete($row);
            }
            if ($this->ProjectsRoles->delete($role)) {
                $this->Flash->success(__('The role has been deleted.'));
            } else {
                $this->Flash->error(__('The role could not be deleted. Please, try again.'));
            }
            die;
        } else {
            $this->Flash->error(__('No match data'));
            return $this->redirect('/');
        }
    }

    private function checkProjectOfUser($user_id, $project_id)
    {
        $project = $this->Projects->find()
            ->where(['id' => $project_id, 'user_id' => $user_id])
            ->first();
        if ($project) {
            return $project;
        } else {
            return FALSE;
        }
    }

    // Coder: Giang Dien
    // Date: 2016-11-08
    // Function: check role by project id
    public function checkRoleInProjectAjax()
    {
        if ($this->request->is('post')) {
            $project_id = $this->request->data['project_id'];
            $this->loadModel('ProjectsRoles');
            $listRoles = $this->ProjectsRoles->find('list', ['conditions' => ['project_id' => $project_id]])->toArray();
            if (count($listRoles) > 0) {
                $this->Flash->success(__('Project sadas saved.'));
                $result = ['status' => 1, 'next_link' => '/projects/milestones/' . $project_id];
                // $this->redirect(['action'=>'milestones',$project_id]);
            } else {
                $this->Flash->error(__('Role is required.'));
                $result = ['status' => 0, 'next_link' => '/projects/roles/' . $project_id];
            }
            echo json_encode($result);
        } else {
            echo __('No match data');
        }
        die;
    }

    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    private function getImageFromUrl($url = null, $filename = 'image.jpg')
    {
        if (trim($url) == '') {
            return FALSE;
        }

        $img = WWW_ROOT . 'img/uploads/' . $filename;
        $result = file_put_contents($img, file_get_contents($url));
        if ($result) {
            return 'img/uploads/' . $filename;
        } else {
            return FALSE;
        }
    }

    /**
     * Show About_you screens
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function about_you($project_id = null)
    {
        $this->set('title', __('About you - We the projects'));

        $user_id = $this->Auth->user('id');
        if (!$user_id) {
            return $this->redirect(['action' => 'login']);
        }
        $this->Users = TableRegistry::get('Users');
        // get user
        $user = $this->Users->get($user_id, [
            'contain' => ['Projects']
        ]);
        if (empty($user)) {
            return $this->redirect(['action' => 'index']);
        }
        if (!$project_id) {
            return $this->redirect(['action' => 'create']);
        }
        $check = $this->checkProjectOfUser($this->Auth->user('id'), $project_id);
        if (!$check) {
            $this->Flash->error(__('You are not creator'));
            return $this->redirect(['action' => 'create']);
        }
        $this->loadModel('UsersProjects');
        $projects = $this->UsersProjects->find('all', ['conditions' => ['UsersProjects.user_id' => $user_id, 'project_id' => $project_id]])->contain(['Projects']);
        $project = $projects->first();
        if (!empty($project)) {
            $name = $project->name;
            $biography = $project->biography;
            $currentAvatarPath = $this->getAvatarFilePathByURL($project->user_picture);
            $currentAvatarURL = $project->user_picture;

            $avatarURL = $this->getAvatarUrl($project->user_picture);
        } else {
            $name = $user ? $user->first_name . ' ' . $user->last_name : '';
            $biography = $user ? $user->biography : '';
            $currentAvatarPath = $this->getAvatarFilePathByURL($user ? $user->avatar : '');
            $currentAvatarURL = $user ? $user->avatar : '';

            $avatarURL = $this->getAvatarUrl($user->avatar);
        }
        $usersWebsites = TableRegistry::get('UsersWebsites');
        $websites = $usersWebsites->find('all', ['fields' => ['website_id', 'Websites.name'], 'contain' => 'Websites', 'conditions' => ['user_id' => $user_id]]);

        if ($this->request->is(['post', 'PUT'])) {
            if (empty($project)) {
                $project = $this->UsersProjects->newEntity();
                $project->user_id = $user_id;
                $project->project_id = $project_id;
            }
            $project->role_id = (int)$this->request->data['UsersProjects']['role_id'];
            $project->name = trim($this->request->data['UsersProjects']['name']);
            $project->biography = trim($this->request->data['UsersProjects']['biography']);

            $avatar = $this->request->data['UsersProjects']['user_picture'];
            $external_avatar_url = trim($this->request->data['UsersProjects']['external_user_picture_url']);
            $image_url = $this->request->data['image_url'];
            if (!empty($image_url)) {
                $avatar = '';
                list($type, $image_url) = explode(';', $image_url);
                list(, $image_url) = explode(',', $image_url);
                $image_url = base64_decode($image_url);
                $imageName = $_SERVER['DOCUMENT_ROOT'] . '/webroot/img/avatars/' . time() . '.png';
                file_put_contents($imageName, $image_url);
                $this->loadModel('Resize');
                $this->Resize->load($imageName);
                // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
                $this->Resize->resizeImage(1080, 1080, 'auto');
                $name_avatar = '/webroot/img/avatars/' . time() . '.jpg';
                $imageName_new = $_SERVER['DOCUMENT_ROOT'] . $name_avatar;
                // *** 3) Save image
                $this->Resize->saveImage($imageName_new, 1000);
                unlink($imageName);
            }
            // VALIDATE avatar
            $fileType = '';
            if ((!empty($avatar) && !empty($avatar['tmp_name']) && !empty($avatar['name']) || !empty($external_avatar_url)) && $this->_status) {
                $fileType = empty($external_avatar_url) ?
                    $this->getFileTypeByMimeType($avatar['type']) :
                    pathinfo(basename($external_avatar_url), PATHINFO_EXTENSION);

                $allowFileTypes = $this->getAllowedAvatarUploadedFileTypes();
                $avatarOK = true;

                if (!in_array($fileType, $allowFileTypes)) {
                    $avatarOK = false;
                }

                if (!empty($external_avatar_url)) {
                    $tmpDir = ini_get('upload_tmp_dir');
                    $tmpFullPath = tempnam($tmpDir, "tmp");

                    if ($this->saveRemoteImage($external_avatar_url, $tmpFullPath)) {
                        $this->request->data['UsersProjects']['user_picture'] = [];
                        $avatar = $this->request->data['UsersProjects']['user_picture'];
                        $avatar['tmp_name'] = $tmpFullPath;
                        $avatar['name'] = basename($external_avatar_url);
                    } else {
                        $avatarOK = false;
                    }
                }

                if ($avatarOK) {
                    list($width, $height) = getimagesize($avatar['tmp_name']);
                    $minimumDimension = $this->getMinimumAvatarDimension();

                    if (empty($width) || empty($height)) {
                        $avatarOK = false;
                    } else {
                        $avatarOK = ($width >= $minimumDimension['width']) && ($height >= $minimumDimension['height']);
                    }
                }

                if (!$avatarOK) {
                    $this->Flash->error(__('Profile image should be: jpeg, png, gif, bmp with minimum @1024x1024.'));
                    return $this->redirect($this->referer());
                    exit;
                }
            }
            // END VALIDATE avatar
            // UPDATE WEB SITE
            $this->_status = $this->updateWebsite($user_id) ? 1 : 0;
            if (!$this->_status) {
                $this->Flash->error(__('The profile could not be saved. Please, try again.'));
            }
            // END WEB SITE
            // BEGIN UPDATE USER
            $uniqueAvatarPath = '';
            if (!empty($avatar) && !empty($avatar['tmp_name']) && !empty($avatar['name'])) {
                $uniqueAvatarPath = $this->getUniqueAvatarFilePath($avatar['name'], $fileType, $user_id);
                $project->user_picture = $this->getAvatarURLByFilePath($uniqueAvatarPath);
            } else if (!empty($external_avatar_url)) {
                $uniqueAvatarPath = $this->getUniqueAvatarFilePath($avatar['name'], $fileType, $user_id);
                // $user->avatar = $external_avatar_url;
                $project->user_picture = $this->getAvatarURLByFilePath($uniqueAvatarPath);
            } else {
                $project->user_picture = $currentAvatarURL;
            }
            if (isset($name_avatar) && !empty($name_avatar)) {
                $this->request->data['UsersProjects']['user_picture'] = ROOT_URL . $name_avatar;
            } else {
                $this->request->data['UsersProjects']['user_picture'] = $project->user_picture;
            }
            $this->request->data['UsersProjects']['role_id'] = (int)$this->request->data['UsersProjects']['role_id'];
            $this->request->data['UsersProjects']['type'] = 1;
            $this->request->data['UsersProjects']['project_role_id'] = 0;
            $this->request->data['UsersProjects']['status'] = 1;
            $user_project = TableRegistry::get('UsersProjects');
            $query = $user_project->query();
            $query->update()
                ->set(['role_id' => $this->request->data['UsersProjects']['role_id']])
                ->where(['user_id' => $user_id, 'project_id' => $project_id, 'project_role_id' => 0])
                ->execute();
            $project = $this->UsersProjects->patchEntity($project, $this->request->data['UsersProjects']);
            if ($this->UsersProjects->save($project)) {
                if ((!empty($avatar) && !empty($avatar['tmp_name']) && !empty($avatar['name'])) || !empty($external_avatar_url)) {
                    $this->deleteOldAvatar($currentAvatarPath);
                    $this->saveNewAvatar($avatar['tmp_name'], $uniqueAvatarPath);
                }
                $this->Flash->success(__('Lưu hồ sơ thành công.'));
                return $this->redirect('/projects/milestones/' . $project_id);
            } else {
                $this->Flash->error(__('The profile could not be saved. Please, try again.'));
            }
            // END UPDATE USER
        }

//        $user = $this->UsersProjects->patchEntity($user, $this->request->data);
        $this->loadModel('Roles');
        $roles = $this->Roles->find('list', ['fields' => ['id', 'role', 'role_es', 'role_ja'],
            'keyField' => 'id', 'valueField' => 'role'])->order(['role' => 'ASC']);

        $this->set(compact('check', 'name', 'biography', 'project', 'avatarURL', 'project_id', 'websites', 'roles'));
    }

    public function milestones($project_id = null)
    {
        $this->set('title', __('Milestones - We the projects'));
        if (!$project_id) {
            return $this->redirect(['action' => 'create']);
        }
        $check = $this->checkProjectOfUser($this->Auth->user('id'), $project_id);
        if (!$check) {
            $this->Flash->error(__('You are not creator'));
            return $this->redirect(['action' => 'create']);
        }
        $projectTable = TableRegistry::get('Projects');
        $project = $projectTable->get($project_id);
        $milestones = NULL;
        $this->loadModel('ProjectUpdates');
        if ($this->request->is('post')) {
            $error = FALSE;
            $mileArr = isset($this->request->data['mile']) ? $this->request->data['mile'] : [];
            $this->sendNotificationForJoinProject($project_id, 10, '');
            foreach ($mileArr as $value) {
                $value['title'] = htmlspecialchars($value['title']);
                $value['content'] = htmlspecialchars($value['content']);
                $milestones = $this->ProjectUpdates->newEntity();
                $value['project_id'] = $project_id;
                $value['type'] = MILESTONE_TYPE;
                $milestones = $this->ProjectUpdates->patchEntity($milestones, $value);
                if (!$this->ProjectUpdates->save($milestones)) {
                    $error = TRUE;
                    break;
                }
            }
            if (count($mileArr) > 0) {
                $projectTable->patchEntity($project, ['is_completed' => 0]);
                $projectTable->save($project);
            }
            if ($error) {
                $this->Flash->error(__('Save error'));
            } else {
                $this->Flash->success(__('The milestone has been saved.'));
                $this->redirect('/projects/' . $this->CustomSlug($check->title) . '-' . $check->id);
            }
        }
        $mileStoneProject = $this->ProjectUpdates->find('all')
            ->where(['project_id' => $project_id, 'type' => 2])
            ->toArray();
        $this->set(compact('milestones', 'mileStoneProject', 'check', 'project'));
    }

    public function delmilestone($id = null)
    {
        $this->loadModel('ProjectUpdates');
        $milestone = $this->ProjectUpdates->get($id);
        if ($this->ProjectUpdates->delete($milestone)) {
            $this->Flash->success(__('The milestone has been deleted.'));
        } else {
            $this->Flash->error(__('The milestone could not be deleted. Please, try again.'));
        }
        // check and update project complete status
        $listMileStones = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['project_id' => $milestone->project_id, 'type' => 2]);
        $listMileStoneNotActive = $this->_getListProjectUpdateByTypeAndStatus($listMileStones, 2, 0);
        if (count($listMileStones) > 0 && count($listMileStoneNotActive) == 0) {
            $projectTable = TableRegistry::get('Projects');
            $project = $projectTable->get($milestone->project_id);
            $projectTable->patchEntity($project, ['is_completed' => 1]);
            $projectTable->save($project);
        }
        $this->redirect($this->referer());
    }

    public function test()
    {
        $this->loadModel('ProjectUpdates');
        $listMileStones = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['project_id' => 42, 'type' => 2]);
        $listMileStoneNotActive = $this->_getListProjectUpdateByTypeAndStatus($listMileStones, 2, 0);
        if (count($listMileStones) > 0 && count($listMileStoneNotActive) == 0) {
            $projectTable = TableRegistry::get('Projects');
            $project = $projectTable->get(42);
            $projectTable->patchEntity($project, ['is_completed' => 1]);
            echo 'oke';
        } else {
            echo 'not oke';
        }
        die;
    }

    private function _getListProjectUpdateByTypeAndStatus($list = [], $type = 1, $status = 1)
    {
        $listArr = [];
        foreach ($list as $row) {
            if ($row['type'] == $type && $row['status'] == $status) {
                $listArr[] = $row;
            }
        }
        return $listArr;
    }

    private function getAvatarFilePathByURL($url)
    {
        $retval = '';
        if (!empty($url) && strlen($url) > 0 &&
            strpos($url, rtrim(Router::url($this->request->webroot, true), '/')) !== false
        ) {
            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
            $fileName = basename($url);
            $retval = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', $fileName]);
        }

        return $retval;
    }

    private function getAvatarURLByFilePath($path)
    {
        $retval = '';
        if (!empty($path)) {
            $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
            $fileName = basename($path);
            $retval = join('/', [$webRootURL, 'img', 'avatars', $fileName]);
        }
        return $retval;
    }

    private function getFileTypeByMimeType($mime)
    {
        $retval = '';
        $mimes = [
            // images
            'image/bmp' => 'bmp',
            'image/gif' => 'gif',
            'image/jpeg' => 'jpeg',
            'image/pjpeg' => 'jpeg',
            'image/png' => 'png',
            // videos
            'video/quicktime' => 'mov',
            'video/mpeg' => 'mpeg',
            'application/x-troff-msvideo' => 'avi',
            'video/avi' => 'avi',
            'video/msvideo' => 'avi',
            'video/x-msvideo' => 'avi',
            'video/mp4' => 'mp4',
            'application/mp4' => 'mp4',
            'video/3gpp' => '3gp',
            'video/x-ms-wmv' => 'wmv',
            'video/x-flv' => 'flv'
        ];

        foreach ($mimes as $key => $value) {
            if ($key == trim($mime)) {
                $retval = $value;
                break;
            }
        }

        return $retval;
    }

    private function isUrlValid($url)
    {
        if (empty($url))
            return false;
        return preg_match("/^(https?:\\/\\/)(?:[a-z0-9-]+\\.)*((?:[a-z0-9-]+\\.)[a-z]+)$/i", $url);
    }

    public function isImage($url)
    {
        // echo "test";
        if (!getimagesize($url)) {
            return false;
        }
        return true;
    }

    private function getAllowedAvatarUploadedFileTypes()
    {
        return ['jpeg', 'png', 'gif', 'bmp'];
    }

    private function saveRemoteImage($url, $fullPath)
    {
        $ok = true;
        if (!isset($url) || !isset($fullPath) || empty($url) || empty($fullPath)) {
            $ok = false;
        }

        $ch = true;
        if ($ok) {
            $ch = @curl_init($url);
            if ($ch === false) {
                $ok = false;
            }
        }

        if ($ok) {
            @curl_setopt_array($ch, UsersController::CurlCommonOptions);
            @curl_setopt($ch, CURLOPT_HEADER, false);
            @curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            @curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

            $fp = @fopen($fullPath, 'wb');
            if ($fp !== false) {
                @curl_setopt($ch, CURLOPT_FILE, $fp);
                @curl_exec($ch);
                $errCode = @curl_errno($ch);
                @curl_close($ch);
                @fclose($fp);

                if ($errCode) {
                    $ok = false;
                    if (@file_exists($fullPath)) {
                        @unlink($fullPath);
                    }
                }
            } else {
                $ok = false;
            }
        }

        return $ok;
    }

    private function getMinimumAvatarDimension()
    {
        return ['width' => 1024, 'height' => 576];
    }

    private function updateWebsite($user_id)
    {
        $retval = true;
        $data = $this->request->data;

        $deletedWebsites = explode(',', $data['deletedWebsites']);
        $deletedWebsitesIds = [];

        $websitesTables = TableRegistry::get('Websites');
        $usersWebsitesTables = TableRegistry::get('UsersWebsites');
        $websiteEntity = null;
        $usersWebsiteEntity = null;

        // Websites to be deleted
        if (!empty($deletedWebsites)) {
            foreach ($deletedWebsites as $deletedWebsite) {
                $deletedWebsite = trim($deletedWebsite);
                if (!empty($deletedWebsite) && preg_match("/^[1-9][0-9]*$/D", $deletedWebsite)) {
                    array_push($deletedWebsitesIds, intval($deletedWebsite));
                }
            }
        }

        if (!empty($deletedWebsitesIds)) {
            $usersWebsitesTables->deleteAll([
                'UsersWebsite.website_id IN' => $deletedWebsitesIds,
                'UsersWebsite.user_id' => $user_id
            ]);

            $websitesTables->deleteAll([
                'Website.id IN' => $deletedWebsitesIds
            ]);
        }

        // Websites to be added
        $otherWebsites = [];
        if (isset($data['UsersWebsite'])) {
            $otherWebsites = $data['UsersWebsite'];
        }

        if (!empty($otherWebsites) && $retval) {
            foreach ($otherWebsites as $key => $value) {
                $trimmedValue = trim($value);
                if (!empty($trimmedValue) && !is_numeric($trimmedValue)) {
                    $websiteEntity = $websitesTables->newEntity();
                    $websiteEntity->name = $trimmedValue;
                    if (!$websitesTables->save($websiteEntity)) {
                        $retval = false;
                        break;
                    };

                    $usersWebsiteEntity = $usersWebsitesTables->newEntity();
                    $usersWebsiteEntity->user_id = $user_id;
                    $usersWebsiteEntity->website_id = $websiteEntity->id;

                    if (!$usersWebsitesTables->save($usersWebsiteEntity)) {
                        $retval = false;
                        break;
                    };
                }
            }
        }

        return $retval;
    }

    private function getUniqueAvatarFilePath($fileName, $fileType, $user_id)
    {
        $now = new \DateTime();
        $sanitizedFileName = md5(basename($fileName)) . '.' . $fileType;
        $uniqueFileName = join('_', [$user_id, $now->format('YmdHis'), $sanitizedFileName]);
        $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
        $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', $uniqueFileName]);

        return $fullPath;
    }

    private function deleteOldAvatar($fullPath)
    {
        if (!empty($fullPath)) {
            $oldAvatarFile = new File($fullPath);
            if ($oldAvatarFile->exists()) {
                $oldAvatarFile->delete();
            }
        }
    }

    private function saveNewAvatar($tmpFilePath, $fullPath)
    {
        if (!empty($tmpFilePath) && !empty($fullPath)) {
            $uploadedFile = new File($tmpFilePath);

            if ($uploadedFile->exists()) {
                if (is_uploaded_file($tmpFilePath)) {
                    move_uploaded_file($tmpFilePath, $fullPath);
                } else {
                    @rename($tmpFilePath, $fullPath);
                }
            }
        }
    }

    // View Project detail
//    Coder: Giang_Dien
//    Date: 2016/10/21
    public function detail()
    {
        $id = $this->request->param('id');
        $project = $this->Projects->find('all', ['conditions' => ['Projects.id' => $id]])->contain(['Users'])->first();
        $this->loadModel('UsersProjects');
        $this->loadModel('Roles');
        $UsersProjectRole = $this->UsersProjects->find('all', ['conditions' => ['project_id' => $id, 'project_role_id' => 0]])->contain(['Projects']);
        $UsersProjectRole = $UsersProjectRole->first();
        $RoleProjects = $this->Roles->find('all', ['conditions' => ['id' => $UsersProjectRole['role_id']]])->first();
        $role = $RoleProjects['role'];
        if (!empty($project)) {
            if (!empty($project['country_id'])) {
                $this->loadModel('Countries');
                $country = $this->Countries->get($project['country_id']);
            } else {
                $country = [];
            }
            if (!empty($project['state_id'])) {
                $this->loadModel('States');
                $state = $this->States->get($project['state_id']);
            } else {
                $state = [];
            }
            if (!empty($project['category_id'])) {
                $this->loadModel('Categories');
                $category = $this->Categories->get($project['category_id']);
            } else {
                $category = [];
            }
            $projectEntity = $this->Projects->patchEntity($project, ['viewed_counter' => ($project->viewed_counter + 1)]);
            $this->Projects->save($projectEntity);
            $user = $this->Auth->user();
            $blockUserTable = TableRegistry::get('Blocks');
            $blockUserList = $blockUserTable->find('all', ['conditions' => ['user_id' => $project->user_id, 'blocked_id' => $user['id']]])->toArray();
            $blockUser = $blockUserTable->find('all', ['conditions' => ['user_id' => $user['id'], 'blocked_id' => $project->user_id]])->toArray();
            if (!empty($blockUserList) || !empty($blockUser)) {
                $this->set('is_blocked', TRUE);
            } else {
                $this->set('is_blocked', FALSE);
            }
            $this->loadModel('UserProjectActions');
            $projectAction = $this->UserProjectActions->getListUserProjectAction(['user_id' => $user['id'], 'project_id' => $project['id'], 'status' => 1]);
            $this->loadModel('ProjectsRoles');
            $projectRoles = $this->ProjectsRoles->getProjectRoles(['project_id' => $project['id']]);
            $this->loadModel('UsersProjects');
            $usersProjects = $this->UsersProjects->getUserProjectsByOptions(['project_id' => $id, 'role_id != ' => 1]);
            $this->set('title', $project['title'] . __(' - We the projects'));
            $this->set('role', $role);
            $this->set(['current_url' => $this->referer()]);
            $this->set(['meta_description' => $project['synopsis']]);
            $this->set(['meta_image' => $project['image_url']]);
            $this->set([
                'message' => $this->_message,
                'data' => $project,
                'status' => $this->_status,
                'userAction' => $projectAction,
                'projectRoles' => $projectRoles,
                'country' => $country,
                'state' => $state,
                'category' => $category,
                'usersProjects' => $usersProjects,
                '_serialize' => ['message', 'data', 'status', 'userAction', 'country', 'state', 'category', 'usersProjects']
            ]);
        } else {
            $this->Flash->error(__('Project not found'));
            return $this->redirect('/');
        }
    }

    // Coder: Giang Dien
    // Date: 25/10/2016
    // get project info by tab via ajax
    public function getProjectInfoByTabAjax()
    {
        if ($this->request->is(['post'])) {
            $currentUser = $this->Auth->user();
            if (!empty($currentUser)) {
                $checkUser = 1;
            } else {
                $checkUser = 0;
            }
            $project_id = $this->request->data['project_id'];
            $option = $this->request->data['option'];
            if ($option == 'the_film') {
                // Get tab member in the project
                $project = $this->Projects->get($project_id);
                $this->loadModel('UsersProjects');
                $usersProjects = $this->UsersProjects->getUserProjectsByAllOptions(['project_id' => $project_id, 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);
                $this->set([
                    'checkUser' => $checkUser,
                    'message' => $this->_message,
                    'data' => $usersProjects,
                    'status' => $this->_status,
                    'project' => $project,
                    'option' => $option,
                    '_serialize' => ['message', 'data', 'status', 'option']
                ]);
            } else if ($option == 'updates') {
                // Get list updates of the project
                $project = $this->Projects->get($project_id);
                $this->loadModel('ProjectUpdates');
                $projectUpdates = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['ProjectUpdates.status' => 1, 'ProjectUpdates.project_id' => $project_id]);
                $projectMileStonesInactive = $this->ProjectUpdates->getListProjectUpdatesByOptions('all', ['ProjectUpdates.status' => 0, 'ProjectUpdates.type' => 2, 'ProjectUpdates.project_id' => $project_id]);
                $this->set([
                    'message' => $this->_message,
                    'checkUser' => $checkUser,
                    'data' => $projectUpdates,
                    'status' => $this->_status,
                    'option' => $option,
                    'project' => $project,
                    'projectMileStonesInactive' => $projectMileStonesInactive,
                    '_serialize' => ['message', 'data', 'status', 'option', 'project', 'projectMileStonesInactive']
                ]);
            } else {
                // Get list comments of the project
                $project = $this->Projects->get($project_id);
                $this->loadModel('ProjectComments');
                $projectComments = $this->ProjectComments->getListCommentAllFieldByProjectId($project_id);
                $this->set([
                    'checkUser' => $checkUser,
                    'message' => $this->_message,
                    'data' => $projectComments,
                    'status' => $this->_status,
                    'project' => $project,
                    'option' => $option,
                    '_serialize' => ['message', 'data', 'status', 'option', 'project']
                ]);
            }
        } else {
            echo __('No match data');
        }
        $this->viewBuilder()->layout('ajax');
    }

    private function getAvatarUrl($picture)
    {
        $webRootURL = rtrim(Router::url($this->request->webroot, true), '/');
        $avatarURL = join('/', [$webRootURL, 'img', 'avatars', 'default.png']);

        if (!empty($picture) && strlen($picture) > 0 && strpos($picture, $webRootURL) !== false) {
            $webRoot = rtrim(WWW_ROOT, DIRECTORY_SEPARATOR);
            $fullPath = join(DIRECTORY_SEPARATOR, [$webRoot, 'img', 'avatars', basename($picture)]);
            $avatarFile = new File($fullPath);
            if ($avatarFile->exists()) {
                $avatarURL = $picture;
            }
        } else if (!empty($picture) && strlen($picture) > 0) {
            $avatarURL = $picture;
        }
        return $avatarURL;
    }

    // Coder: Giang Dien
    // Date: 2016-11-09
    // Function: check video Youtube exit by id
    public function checkVideoYoutubeExit()
    {
        if ($this->request->is('post')) {
            $videoID = $this->request->data['videoID'];
            $headers = get_headers('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v=' . $videoID);
            if (is_array($headers) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/', $headers[0]) : false) {
                // video exists
                echo 'TRUE';
            } else {
                // video does not exist
                echo 'FALSE';
            }
        } else {
            echo __('No match data');
        }
        die;
    }

    // Coder: Giang Dien
    // Date: 2016-11-11
    // Function: Get list project of current user
    public function getMyProjects()
    {
        $userId = $this->Auth->user('id');
        $listProjects = $this->Projects->find('all', ['conditions' => ['user_id' => $userId], 'limit' => 1])->order(['created' => 'DESC'])->first();
        return $listProjects;
    }

    // Coder:Nam Pham
    // Date: 2017-4-12
    public function getMyJoinProjects()
    {
        $userId = $this->Auth->user('id');
        $this->loadModel('UsersProjects');
        $usersjoinProjects = $this->UsersProjects->getUserProjectsByAllOptions(['user_id' => $userId, 'UsersProjects.status' => 1, 'UsersProjects.type' => 2], 0, 0, ['Roles', 'Users']);
        return $usersjoinProjects;
    }
    // Coder:Nam Pham
    // Date: 2017-4-12
    public function getNameProjects($id)
    {
        $userId = $this->Auth->user('id');
        $this->loadModel('Projects');
        $nameproject = $this->Projects->find('all', ['conditions' => ['id' => $id], 'limit' => 1])->order(['created' => 'DESC'])->first();
        return $nameproject;
    }

    public function updateIconRole()
    {
        $iconArr = ['creative_deverlopment.png', 'production_lead.png', 'camera_department.png', 'electric.png', 'grip.png', 'sound_department.png', 'art_department.png', 'contruction_department.png', 'special_effects.png', 'stunts.png', 'vanities.png', 'vtr_continuity.png', 'location_transport.png', 'production_support.png', 'post_production.png', 'post_sound_and_music.png'];
        $this->loadModel('Roles');
        $list = $this->Roles->getRolesByOptions();
        foreach ($list as $role) {
            $iconLink = ROOT_URL . 'img/role_icons/' . $iconArr[$role->group - 1];
            $dataEntyti = $this->Roles->patchEntity($role, ['icon' => $iconLink]);
            if ($this->Roles->save($dataEntyti)) {
                echo $role->role . '---success<br/>';
            } else {
                echo $role->role . '---fail<br/>';
            }
        }
        die;
    }

    //2017-6-26
    public function getProjectByFee()
    {
        $userId = $this->Auth->user('id');
        $this->loadModel('Users');
        $adminType = $this->Users->getAdminType($userId);
        // var_dump($adminType);
        $projects = [];
        $search = [];
        if ($adminType == 1 || $adminType == 2 || $adminType == 3 || $adminType == 4 || $adminType == 5) {
            if ($this->request->is('get')) {
                $group = $this->request->query('group');
                $fee_status = $this->request->query('fee_status');
                switch ($fee_status) {
                    case 1:
                        $fee_status = 0;
                        break;
                    case 2:
                        $fee_status = 1;
                        break;
                    default:
                        $fee_status = 4;
                        break;
                }
                $location = $this->request->query('location');
                // var_dump($location);
                $category = $this->request->query('category');
                if (!empty($group && isset($group))) {
                    $search['group_id'] = $group;
                }
                if (isset($fee_status) && $fee_status != 4) {
                    $search['fee_status'] = $fee_status;
                }
                if (!empty($category) && isset($category)) {
                    $search['category_id'] = $category;
                }
                if (!empty($location) && isset($location)) {
                    $search['district_id'] = $location;
                }
            }
            $this->loadModel('Projects');
            $projects = $this->Projects->find('all', ['conditions' => $search, 'contain' => ['Categories', 'Users']]);
            $GroupTable = TableRegistry::get('Groups');
            $groups = $GroupTable->find('list', ['limit' => 200, 'field' => 'name']);
            $CategoriesTable = TableRegistry::get('Categories');
            $categories = $CategoriesTable->find('list', ['limit' => 200, 'field' => 'name']);
            $LocationsTable = TableRegistry::get('districts');
            $locations = $LocationsTable->find('list', [
                'conditions' => ['country_id' => 235],
                'order' => ['name' => 'ASC']
            ]);
            $this->set(compact('projects', 'groups', 'categories', 'locations'));
            $this->set([
                'projects' => $this->paginate($projects),
                '_serialize' => ['projects']
            ]);
            $this->set('title', __('Danh sách doanh nghiệp'));
            $this->render('list_fee');
        } else {
            $this->redirect('/');
        }
    }

    public function exportProjects()
    {
        $user = $this->Auth->user();
        $userid = $user['id'];
        if ($user['admin'] == 0) {
            $this->Flash->error(__("Không có quyền truy cập !"));
            $this->redirect("/");
        } else {
            $this->Xls->exportProject('Projects');
        }
    }

    public function exportProjectsForMobile()
    {
        if ($this->request->is('get')) {
            $ck = $this->request->query('xls');
            $tokenTable = TableRegistry::get('Tokens');
            $token = $tokenTable->find('all', [
                'conditions' => ['token' => $ck], 'fields' => 'user_id'
            ])->first();
            $userId = $token->user_id;
            $userTable = TableRegistry::get('Users');
//            $adminType = $userTable->find('all', ['conditions' => ['id' => $userId], 'fields' => 'admin'])->first();
            $user = $userTable->find('all', ['conditions' => ['id' => $userId]])->first();

            if ($user['admin'] == 0) {
                $this->Flash->error(__("Không có quyền truy cập !"));
                $this->redirect("/");
            } else {
                $this->Xls->exportProject('Projects');
            }
        }

    }

}
