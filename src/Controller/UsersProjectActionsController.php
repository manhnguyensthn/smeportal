<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\Core\Configure;

/**
 * UsersProjectAction Controller
 *
 * @property \App\Model\Table\UsersProjectsTable $UsersProjects
 */
class UsersProjectActionsController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow([]);
    }


    /**
     * check Love Follow Project
     * @author Roxannie Nguyen jr
     * @return boolean
     */
    public function checkUserProjectAction($ItemId, $TypeId = 1){
    	$UserId = $this->Auth->user('id');
    	if(!$UserId) return false;
    	if(!$ItemId) return false;
    	if(!$TypeId) return false;

    	$this->loadModel('UserProjectActions');
    	$aParam = array(
    		'user_id' => (int)$UserId,
    		'project_id' => (int)$ItemId,
    		'action_type' => (int)$TypeId,
    		'status' => 1,
    	);
    	$projectAction = $this->UserProjectActions->getListUserProjectAction($aParam);
    	if(count($projectAction)){
    		return true;
    	}
    	return false;
    }

    /**
     * get template Love Project
     * @author Roxannie Nguyen jr
     * @param int $ItemId
     * @param string $TemPlate
     * @param string $TemPlate
     * @return string
     */
    public function templateLoveProject($ItemId, $TemPlate = '', $ListUsersActions = null) {
        $aUser = $this->Auth->user();
        $sHtml = '';
        $class = '';
        $bLove = false;
        switch ($TemPlate) {
            case 'lg': $class .= ' font-lg '; break;
        }

        if($aUser){
			$ItemId = isset($ItemId) ? $ItemId : 0;
            if($ListUsersActions == null){
                $bLove  = $this->checkUserProjectAction($ItemId);
            }else{
                foreach ($ListUsersActions as $UsersAction){
                    if ($UsersAction->project_id == $ItemId && $UsersAction->user_id == $aUser['id'] && $UsersAction->action_type == 1){
                        $bLove = true;
                        break;
                    }
                }
            }
            
            if(!$bLove) $class .= ' dislike ';
            $sHtml .= '<a href="javascript:void(0);" onclick="javascript:add_action_to_system(\'1\',\''.$ItemId.'\');" title="Liked it!" class="love_project_'.$ItemId.' template-love '.$class.'"></a>';
        }else{
            $class .= ' dislike ';
            $sHtml .= '<a href="javascript:void(0);" title="Liked it!" class="template-love '.$class.'"></a>';
        }
        return $sHtml;
    }
}