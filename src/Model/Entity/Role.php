<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Lib\CoreLib;

/**
 * Role Entity
 *
 * @property int $id
 * @property string $role
 * @property int $role_type
 * @property int $lang_id
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Language $language
 * @property \App\Model\Entity\User[] $users
 */
class Role extends Entity
{

 
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
