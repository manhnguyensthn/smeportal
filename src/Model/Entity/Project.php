<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Project Entity
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $description
 * @property int $category_id
 * @property int $user_id
 * @property int $district_id
 * @property int $country_id
 * @property int $status
 * @property int $type
 * @property int $process
 * @property int $process_type
 * @property int $lang_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Language $language
 * @property \App\Model\Entity\Story[] $stories
 */
class Project extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
