<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsersWork Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $work_title
 * @property string $work_description
 * @property string $work_url
 * @property string $work_thumbnail
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 */
class UsersWork extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
