<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Request Entity
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $type
 * @property int $project_id
 * @property int $user_id
 * @property int $receiver_id1
 * @property int $receiver_id2
 * @property int $receiver_id3
 * @property string $attach1
 * @property string $attach2
 * @property string $repattach1
 * @property string $repattach2
 * @property string $repcontent1
 * @property string $repcontent2
 * @property int $status
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\User $user
 */
class Request extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
