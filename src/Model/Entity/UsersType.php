<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher; //include this line
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $avatar
 * @property string $token_device
 * @property string $email
 * @property \Cake\I18n\Time $birthday
 * @property int $district_id
 * @property int $country_id
 * @property string $postcode
 * @property string $about_me
 * @property string $password
 * @property string $name
 * @property int $age
 * @property int $sex
 * @property string $fb_id
 * @property string $google_id
 * @property string $linked_id
 * @property \Cake\I18n\Time $time_limit
 * @property int $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Token[] $token
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Fb $fb
 * @property \App\Model\Entity\Google $google
 * @property \App\Model\Entity\Linked $linked
 * @property \App\Model\Entity\Friend[] $friends
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\SearchedHistory[] $searched_history
 * @property \App\Model\Entity\Role[] $roles
 */
class UsersType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
}
