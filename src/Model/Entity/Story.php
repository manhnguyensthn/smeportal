<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Story Entity
 *
 * @property int $id
 * @property int $project_id
 * @property string $video
 * @property string $video_image_overlay
 * @property string $overview
 * @property int $pharse
 * @property string $pitch
 * @property int $lang_id
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Language $language
 */
class Story extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
