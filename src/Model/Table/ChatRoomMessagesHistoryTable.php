<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * States Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\HasMany $Districts
 *
 * @method \App\Model\Entity\State get($primaryKey, $options = [])
 * @method \App\Model\Entity\State newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\State[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\State|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\State patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\State[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\State findOrCreate($search, callable $callback = null)
 */
class ChatRoomMessagesHistoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('chat_room_messages_history');
        $this->primaryKey('id');
        
        $this->belongsTo('ChatRooms', [
            'foreignKey' => 'room_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ChatMessagesHistory', [
            'foreignKey' => 'message_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'sender_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        return $validator;
    }
    
    // Coder: Giang Dien
    // Date: 04-01-2017
    // Function: get room messages by options
    public function getMessagesByOptions($conditions = [], $fields = [], $limit = 0, $offset = 0, $contain = [], $sort = ['ChatRoomMessagesHistory.id' => 'DESC']) {
        if (!empty($fields)) {
            if ($limit != 0) {
                return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'limit' => $limit, 'offset' => $offset, 'contain' => $contain])->order($sort)->toArray();
            } else {
                return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'contain' => $contain])->order($sort)->toArray();
            }
        } else {
            if ($limit != 0) {
                return $this->find('all', ['conditions' => $conditions, 'limit' => $limit, 'offset' => $offset, 'contain' => $contain])->order($sort)->toArray();
            } else {
                return $this->find('all', ['conditions' => $conditions,'contain' => $contain])->order($sort)->toArray();
            }
        }
    }
}

