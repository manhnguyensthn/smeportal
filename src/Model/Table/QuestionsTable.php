<?php
/**
 * Created by PhpStorm.
 * User: vietis
 * Date: 7/10/2017
 * Time: 2:50 PM
 */
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TermOfUseTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('questions');
        $this->displayField('content');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }


}


?>
