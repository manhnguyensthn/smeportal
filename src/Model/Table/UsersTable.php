<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher; //include this line

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Districts
 * @property \Cake\ORM\Association\BelongsTo $States
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Fbs
 * @property \Cake\ORM\Association\BelongsTo $Googles
 * @property \Cake\ORM\Association\BelongsTo $Linkeds
 * @property \Cake\ORM\Association\HasMany $Friends
 * @property \Cake\ORM\Association\HasMany $Notifications
 * @property \Cake\ORM\Association\HasMany $Projects
 * @property \Cake\ORM\Association\HasMany $SearchedHistory
 * @property \Cake\ORM\Association\HasMany $Token
 * @property \Cake\ORM\Association\BelongsToMany $Projects
 * @property \Cake\ORM\Association\BelongsToMany $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

class UsersTable extends Table {

    protected $_virtual = ['full_name'];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Friends', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Projects', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('SearchedHistory', [
            'foreignKey' => 'user_id'
        ]);
//        $this->hasMany('Token', [
//            'foreignKey' => 'user_id'
//        ]);
        $this->belongsToMany('Projects', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'project_id',
            'joinTable' => 'users_projects'
        ]);
        $this->belongsToMany('Roles', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'users_roles'
        ]);
        $this->hasMany('Followings', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Users_type', [
            'foreignKey' => 'users_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->add('email', 'valid-email', ['rule' => 'email', 'message' => __('Invalid email format. Must be, ex: john.smith@gmail.com, nakatajim@fuji.co.jp.')])
                ->requirePresence('email', 'create')
                ->notEmpty('email', __('Không được để trống.'));
        $validator
                ->add('secondary_email', 'valid-email', ['rule' => 'email', 'message' => __('Invalid email format. Must be, ex: john.smith@gmail.com, nakatajim@fuji.co.jp.')]);
        $validator
                ->add('confirm_email', 'valid-email', ['rule' => 'email', 'message' => __('Invalid email format. Must be, ex: john.smith@gmail.com, nakatajim@fuji.co.jp.')])
                ->add('confirm_email', 'compareWith', [
                    'rule' => ['compareWith', 'email'],
                    'message' => __('Email does not match.')
                        ]
        )
		->notEmpty('confirm_email', __('Không được để trống.'));
        $validator
                ->add("first_name", [
                    "checkHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        "message" => __("Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode")
                    ]
                        ]
                )
                ->add('first_name', 'maxLength', [
                    'rule' => ['maxLength', 20],
                    "message" => __("Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode")
                        ]
                )
                ->add("first_name", [
                    "custom" => [
                        "rule" => [$this, "customCheckNameEmpty"], //add the new rule 'customFunction' to cedula field
                        "message" => __("Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode")
                    ]
                        ]
                )
                ->notEmpty('first_name', __('Không được để trống.'));

        $validator
                ->add("last_name", [
                    "checkHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        "message" => __("Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode")
                    ]
                        ]
                )
                ->add('last_name', 'maxLength', [
                    'rule' => ['maxLength', 20],
                    "message" => __("Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode")
                        ]
                )
                ->add("last_name", [
                    "custom" => [
                        "rule" => [$this, "customCheckNameEmpty"], //add the new rule 'customFunction' to cedula field
                        "message" => __("Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode")
                    ]
                        ]
                )
                ->notEmpty('last_name', __('Không được để trống.'));
//                ->allowEmpty('last_name');
//        $validator
//            ->requirePresence('about_me', 'create')
//            ->notEmpty('about_me');
//
        $validator
                ->requirePresence('password', 'create')
                ->add('password', 'alphaNumeric', [
                    'rule' => ['alphaNumeric'],
                    'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                        ]

                )
                ->add('password', 'minLength', [
                    'rule' => ['minLength', 8],
                    'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                        ]
                )
                ->add('password', 'maxLength', [
                    'rule' => ['maxLength', 16],
                    'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                        ]
                )
                ->add("password", [
                    "custom" => [
                        "rule" => [$this, "customCheckFormatPassWord"], //add the new rule 'customFunction' to cedula field
                        'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                    ]
                        ]
                )
                 ->notEmpty('password', __('Không được để trống.'));
        $validator
                ->add('confirm_password', 'compareWith', [
                    'rule' => ['compareWith', 'password'],
                    'message' => __('Password does not match.')
                        ]
        );
        $validator
                ->add('new_password', 'alphaNumeric', [
                    'rule' => ['alphaNumeric'],
                    'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                        ]
                )
                ->add('new_password', 'minLength', [
                    'rule' => ['minLength', 8],
                    'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                        ]
                )
                ->add('new_password', 'maxLength', [
                    'rule' => ['maxLength', 16],
                    'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                        ]
                )
                ->add("new_password", [
                    "custom" => [
                        "rule" => [$this, "customCheckFormatPassWord"], //add the new rule 'customFunction' to cedula field
                        'message' => __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.')
                    ]
                        ]
        )
			 ->notEmpty('new_password', __('Không được để trống.'));
        $validator
                ->add('cf_newpassword', 'compareWith', [
                    'rule' => ['compareWith', 'new_password'],
                    'message' => __('Password does not match.')
                        ]
        );
        $validator
                ->add("biography", [
                    "checkHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'message' => __('Dude, tl:dr! Please keep the bio under 500 characters. Accepted for unicode chars (Japanese/Chinese/Saudi Arabia…)')
                    ]
                        ]
                )
                ->add('biography', 'maxLength', [
                    'rule' => ['maxLength', 600],
                    'message' => __('Dude, tl:dr! Please keep the bio under 500 characters. Accepted for unicode chars (Japanese/Chinese/Saudi Arabia…)')
                        ]
                )
                ->allowEmpty('biography');
        $validator
                ->add('card_number', 'numeric', [
                    'rule' => 'numeric',
                    'message' => __('Please enter only numbers')
                        ]
                )
                ->add('card_number', 'maxLength', [
                    'rule' => ['maxLength', 16],
                    'message' => __('Invalid card number format. Must be 16 chars length max, 13 chars length min. Including from 0-9.')
                        ]
                )
                ->add('card_number', 'minLength', [
                    'rule' => ['minLength', 13],
                    'message' => __('Invalid card number format. Must be 16 chars length max, 13 chars length min. Including from 0-9.')
                        ]
        );

        $validator
                ->add('card_owner_name', [
                    "custom" => [
                        "rule" => [$this, "customCheckFormatCardOwnerName"], //add the new rule 'customFunction' to cedula field
                        "message" => __("Invalid card owner name format. Must be 26 chars length max. Including from A-Z, a-z, space, 0-9.")
                    ]
                        ]
                )
                ->add('card_owner_name', 'maxLength', [
                    'rule' => ['maxLength', 26],
                    'message' => __('Invalid card owner name format. Must be 26 chars length max. Including from A-Z, a-z, space, 0-9.')
                        ]
        );
        $validator
                ->add('expiry_month', 'numeric', [
                    'rule' => 'numeric',
                    'message' => __('Please select Expiry Month')
                        ]
        );
        $validator
                ->add('expiry_year', 'numeric', [
                    'rule' => 'numeric',
                    'message' => __('Please select Expiry Year')
                        ]
        );

//
//        $validator
//            ->integer('sex')
//            ->requirePresence('sex', 'create')
//            ->notEmpty('sex');
//
//        $validator
//            ->requirePresence('token', 'create')
//            ->notEmpty('token');
//
//        $validator
//            ->dateTime('time_limit')
//            ->requirePresence('time_limit', 'create')
//            ->notEmpty('time_limit');
//
//        $validator
//            ->integer('status')
//            ->requirePresence('status', 'create')
//            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email'], __('This email has been registered.')));

        return $rules;
    }

    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+>/", $string, $m) != 1;
    }

    public function customCheckNameEmpty($str = "") {
        if (trim($str) == "") {
            return FALSE;
        }
        return TRUE;
    }

    public function customCheckFormatPassWord($string = "") {
        if (!preg_match("#.*^(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $string)) {
            return FALSE;
        }
        return TRUE;
    }

    public function customCheckFormatCardOwnerName($string = NULL) {
        if (!preg_match('#^(?=.*[A-Za-z0-9])[A-Za-z0-9 _]*$#', $string)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function checkExitEmail($email = '') {
        if ($email == "")
            return FALSE;

        $query = $this->find('all', [
            'conditions' => ['Users.email' => $email]
        ]);
        $row = $query->count();
        if ($row > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getUserbyEmail($email = '') {
        if ($email == "")
            return FALSE;
        $query = $this->find('all', [
            'conditions' => ['Users.email' => $email]
        ]);
        $row = $query->first();
			if (strlen($email) != strlen(utf8_decode($email)))
		{
			$row = '';
		}
        return $row;
    }

    // function login
    function loginApi($email = NULL, $password = NULL) {
        if (!$email)
            return FALSE;
        $user = $this->getUserbyEmail($email);
        if (empty($user))
            return FALSE;
        if (password_verify($password, $user->password)) {
            return $user;
        } else {
            return FALSE;
        }
    }

    // Input:
    // options of getting result (all,list)
    // array of conditions
    // array of contain
    // Output: List users by options
    public function getListUsersByOptions($option = NULL,$conditions = [],$contains = []){
        $result = $this->find($option,['conditions'=>$conditions])->contain($contains)->toArray();
        return $result;
    }

    // Input: array data insert
    // Output: return TRUE  --> insert success
    // return array errors --> insert fail
    public function addUser($data = []) {
        $entity = $this->newEntity();
        $userData = $this->patchEntity($entity, $data);
        if ($this->save($userData)) {
            return TRUE;
        } else {
            return $userData->errors();
        }
    }

    // Input: user id, array data update
    // Output: return TRUE  --> update success
    // return array errors --> update fail
    public function updateUser($user_id, $data = []) {
        $entity = $this->get($user_id);
        $userData = $this->patchEntity($entity, $data);
        if ($this->save($userData)) {
            return TRUE;
        } else {
            return $userData->errors();
        }
    }

    protected function _getFullName(){
        return $this->_properties['first_name'] . '  ' . $this->_properties['last_name'];
    }

    public function getAdminType($id)
    {
      $type = $this->find('all',['conditions'=>['id'=>$id],'fields'=>'admin'])->first();
      return $type['admin'];
    }

    public function getUserType($id)
    {
      $type = $this->find('all',['conditions'=>['id'=>$id],'fields'=>'users_type_id'])->first();
      return $type['users_type_id'];
    }
}
