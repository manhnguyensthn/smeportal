<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Languages
 * @property \Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\Role get($primaryKey, $options = [])
 * @method \App\Model\Entity\Role newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Role[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Role|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Role patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Role[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Role findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('roles');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Languages', [
            'foreignKey' => 'lang_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'role_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_roles'
        ]);
        $this->belongsToMany('UsersProjects', [
            'foreignKey' => 'role_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_projects'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('role', 'create')
            ->notEmpty('role');

        $validator
            ->integer('role_type')
            ->requirePresence('role_type', 'create')
            ->notEmpty('role_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['lang_id'], 'Languages'));
        return $rules;
    }
    
    // Input: parameter array
    // Output: List role
    public function getRolesByOptions($options = array(),$limit = 0,$offset = 0){
        if (!empty($limit)){
            return $this->find('all',['conditions'=>$options,'limit'=>$limit, 'offset'=>$offset])->order(['role' => 'ASC'])->toArray();
        }
        else{
            return $this->find('all',['conditions'=>$options])->order(['role' => 'ASC'])->toArray();
        }
    }
	
	public function getRolesByOptionsSort($options = array(),$limit = 0,$offset = 0,$check = 'role'){
        if (!empty($limit)){
            return $this->find('all',['conditions'=>$options,'limit'=>$limit, 'offset'=>$offset])->order([$check => 'ASC'])->toArray();
        }
        else{
            return $this->find('all',['conditions'=>$options])->order(['role' => 'ASC'])->toArray();
        }
    }
     public function getRoles($role_id = null) {
        if ($role_id == null) {
            return $this->find('all');
        } else {
            return $this->get($role_id);
        }
    }
    
}
