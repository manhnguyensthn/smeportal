<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Notifications Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Notification get($primaryKey, $options = [])
 * @method \App\Model\Entity\Notification newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Notification[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Notification|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Notification[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Notification findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserNotificationSettingTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('user_notification_setting');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
     // Input: User id
    // Output: array notification info by current user
    public function getNotificationByUser($user_id = NULL){
        return $this->find('all', ['conditions' => ['user_id' => $user_id]])->first();;
    }
    
    // Input: array data insert
    // Output: return TRUE  --> insert success
    // return array errors  --> insert fail
    public function addUserNotificationSetting($data = []){
        $entity = $this->newEntity();
        $userNotificationSetting = $this->patchEntity($entity, $data);
        if ($this->save($userNotificationSetting)){
	
            return TRUE;
        }
        else{
            return $userNotificationSetting->errors();
        }
    }
    
    // Input: row id, array data update
    // Output: return TRUE  --> update success
    // return array errors   --> update fail
    public function updateUserNotificationSetting($id = NULL,$data = []){
        $entity = $this->get($id);
        $userNotificationSetting = $this->patchEntity($entity, $data);
        if ($this->save($userNotificationSetting)){
            return TRUE;
        }
        else{
            return $userNotificationSetting->errors();
        }
    }
}
