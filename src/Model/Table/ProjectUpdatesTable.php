<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectUpdates Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Projects
 *
 * @method \App\Model\Entity\ProjectUpdates get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProjectUpdates newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProjectUpdates[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProjectUpdates|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProjectUpdates patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectUpdates[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectUpdates findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectUpdatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('project_updates');
        $this->primaryKey(['id']);
        $this->addBehavior('Timestamp');
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->notEmpty('title','content');
        $validator
            ->add("title", [
                "checkHtml" => [
                    "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                    'message' => __("Update subject is required. Min length of 15 chars, max length of 60 chars. HTML or script tags is not allowed.")
                ]
                ]
            )
            ->add('title', 'minLength', [
                    'rule' => ['minLength', 1],
                    'message' => __("Update subject is required. Min length of 15 chars, max length of 60 chars. HTML or script tags is not allowed.")
                ]
            )
            ->add('title', 'maxLength', [
                    'rule' => ['maxLength', 60],
                    'message' => __("Update subject is required. Min length of 15 chars, max length of 60 chars. HTML or script tags is not allowed.")
                ]
            )
            ->notEmpty('title');
        
        $validator
            ->add("content", [
                "checkHtml" => [
                    "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                    'message' => __("Update content is required. Max title length of 60 chars. Max content length of 150 char. HTML or script tags is not allowed.")
                ]
                ]
            )
            ->add('content', 'maxLength', [
                    'rule' => ['maxLength', 120],
                    'message' => __("Update content is required. Max title length of 60 chars. Max content length of 150 char. HTML or script tags is not allowed.")
                ]
            )
            ->notEmpty('content');
        
        return $validator;
    }


    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+/", $string, $m) != 1;
    }
    
    // Input: project id, number rows return
    // Output: array list updates of project
    public function getUpdatesByProjectId($project_id = NULL,$limit = 5){
        if ($limit != -1){
            return $this->find('all', ['conditions' => ['project_id' => $project_id], 'limit' => $limit])->order(['id' => 'DESC'])->toArray();
        }
        else{
            return $this->find('all', ['conditions' => ['project_id' => $project_id]])->order(['id' => 'DESC'])->toArray();
        }
    }
    
    // Input:
    // options of getting result (all,list) 
    // array of conditions
    // array of contain
    // Output: List project updates by options
    public function getListProjectUpdatesByOptions($option = NULL, $conditions = [], $contains = [], $limit = 0, $offset = 0, $sort = ['ProjectUpdates.created' => 'DESC']) {
        if (empty($limit)) {
            $result = $this->find($option, ['conditions' => $conditions])->contain($contains)->order($sort)->toArray();
        } else {
            $result = $this->find($option, ['conditions' => $conditions, 'limit' => $limit, 'offset' => $offset])->contain($contains)->order($sort)->toArray();
        }
        return $result;
    }
    
    public function getProjectUpdate($conditions = [], $sort = ['ProjectUpdates.created' => 'DESC']){
        return $this->find('all', [
                    'conditions' => $conditions,
                    'fields' => [
                        'ProjectUpdates.id', 'ProjectUpdates.project_id', 'ProjectUpdates.title', 
                        'ProjectUpdates.content', 'ProjectUpdates.created', 'ProjectUpdates.type', 
                        'ProjectUpdates.status', 'Project.start_date', 'Project.end_date'        
                    ],
                ])->join([
                    'table' => 'projects',
                    'alias' => 'Project',
                    'type' => 'INNER',
                    'conditions' => 'ProjectUpdates.project_id = Project.id',
                ])->order($sort);
    }

    

    // Input: array data insert
    // Output: return TRUE  --> insert success
    // return list errors --> insert fail
    public function addUpdate($data = []){
        $entity = $this->newEntity();
        $updates = $this->patchEntity($entity, $data);
        if ($this->save($updates)){
            return TRUE;
        }
        else{
            return $updates->errors();
        }
    }
}
