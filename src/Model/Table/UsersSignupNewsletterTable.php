<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;
use Cake\Auth\DefaultPasswordHasher; //include this line

/**
 * Users Model
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

class UsersSignupNewsletterTable extends Table {
	public function initialize(array $config) {
		parent::initialize($config);

        $this->table('users_signup_newsletter');
        $this->displayField('email');
        $this->primaryKey('id');

	}

	/**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {

        $validator->integer('id')->allowEmpty('id', 'create');
        $validator->add('email', 'valid-email', array(
        	'rule' => 'email',
        	'message' => __('Invalid email format. Must be, ex: john.smith@gmail.com, nakatajim@fuji.co.jp.'),
        ))->requirePresence('email', 'create')->notEmpty('email');

        return $validator;
    }

    public function checkExitEmail($email = '') {
        if ($email == "") return FALSE;
        $Total = $this->find('all', [ 'conditions' => ['UsersSignupNewsletter.email' => $email] ])->count();
        return $Total;
    }
}