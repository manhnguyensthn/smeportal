<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Project Comments Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Projects
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ProjectComments get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProjectComments newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProjectComments[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProjectComments|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProjectComments patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectComments[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectComments findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectCommentsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('project_comments');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('prject_comments.id')
                ->allowEmpty('project_comments.id', 'project_comments.created');
        $validator
                ->add("comment", [
                    "checkHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'last' => true,
                        'message' => __("Comment content is required to send. Max length of 120 chars. HTML or script tags is not allowed.")
                    ],
                    'maxLength' => [
                        'rule' => ['maxLength', 120],
                        'message' => __("Comment content is required to send. Max length of 120 chars. HTML or script tags is not allowed.")
                    ]
                        ]
                )
                ->notEmpty('comment', _("Comment content is required to send. Max length of 120 chars. HTML or script tags is not allowed."));
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));
        return $rules;
    }

    public function customCheckCommentEmpty($comment = '') {
        if (trim($comment) == '') {
            return false;
        }
        return true;
    }

    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+>/", $string, $m) != 1;
    }
    
    
    public function getListCommentAllFieldByProjectId($project_id = NULL){
        return $this->find('all', ['conditions' => ['project_id' => $project_id]])->order(['ProjectComments.created'=>'DESC'])->contain(['Users'])->toArray();
    }
    
    
    public function getListCommentByOptions($option = 'all',$conditions = [],$contains = [],$limit = 0,$offset = 0,$sort = ['ProjectComments.created'=>'DESC']){
        if (empty($limit)) {
            $result = $this->find($option, ['conditions' => $conditions])->contain($contains)->order($sort)->toArray();
        } else {
            $result = $this->find($option, ['conditions' => $conditions, 'limit' => $limit, 'offset' => $offset])->contain($contains)->order($sort)->toArray();
        }
        return $result;
    }

    // Input: project id
    // Output: list comments of project
    // Get list comment by project
    public function getListCommentByProjectId($project_id = null) {
        return $this->find('all', [
                    'conditions' => ['project_id' => $project_id],
                    'fields' => [
                        'ProjectComments.id', 'ProjectComments.project_id', 'ProjectComments.comment', 
                        'ProjectComments.created', 'ProjectComments.parent_comment_id',
                        'user.name', 'user.avatar','user.first_name','user.last_name','user.id'        
                    ]
                ])->join([
                    'table' => 'users',
                    'alias' => 'user',
                    'type' => 'LEFT',
                    'conditions' => 'user.id = ProjectComments.user_id',
        ]);
    }

    // Input: data insert (project_id,user_id,comment,ip_comment,'created')
    // Output: return TRUE  --> insert success
    // return list errors --> insert fail
    public function addComment($data = []) {
        $entity = $this->newEntity();
        $comment = $this->patchEntity($entity, $data);
        if ($this->save($comment)) {
            // Insert comment success
            return TRUE;
        } else {
            // Insert fail return list bugs
            return $comment->errors();
        }
    }

}
