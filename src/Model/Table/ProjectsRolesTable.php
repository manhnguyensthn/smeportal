<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Collection\Collection;

/**
 * UsersProjects Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Projects
 *
 * @method \App\Model\Entity\UsersProject get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersProject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersProject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersProject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsRolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('project_roles');
//        $this->displayField('role_id');
        $this->primaryKey(['id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->add("description", [
                "checkHtml" => [
                    "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                    'message' => __("Phải nhập mô tả vị trí. Độ dài tối đa 120 ký tự. Không được phép nhập thẻ HTML hoặc thẻ script.")
                ]
                ]
            )
            ->notBlank('description',__("Phải nhập mô tả vị trí. Độ dài tối đa 120 ký tự. Không được phép nhập thẻ HTML hoặc thẻ script."))
            ->add('description', 'maxLength', [
                    'rule' => ['maxLength', 150],
                    'message' => __("Phải nhập mô tả vị trí. Độ dài tối đa 120 ký tự. Không được phép nhập thẻ HTML hoặc thẻ script.")
                ]
            )
            ->notEmpty('description',__('Không được để trống.'));
        $validator
            ->naturalNumber('quantity', __('Quantity must to allow a number'))
            ->add("quantity", [
                "checkCount" => [
                    "rule" => [$this, "checkQantity"], //add the new rule 'customFunction' to cedula field
                    'message' => __('Quantity must to allow a number')
                ]
                ]
            )
            ->notEmpty('quantity',__('Không được để trống.'));
        $validator
            ->requirePresence('role_id', 'create')
            ->notEmpty('role_id',__('Không được để trống.'));
        return $validator;
    }

    public function checkQantity($number = 0){
        if($number <= 0) {
            return FALSE;
        }
        if($number > 2147483647){
            return FALSE;
        }
        return true;
    }

    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+>/", $string, $m) != 1;
    }
    
    // Input: array params (project_id,role_id,quantity,....)
    // Output: list array roles info
    public function getProjectRoles($params = [],$contains = ['Roles'],$limit = 0,$offset = 0,$sort = ['ProjectsRoles.created'=>'DESC']){
        if (empty($limit)){
            return $this->find('all', ['conditions' => $params])->contain($contains)->order($sort)->toArray();
        }
        else{
            return $this->find('all', ['conditions' => $params, 'limit'=>$limit,'offset'=>$offset])->contain($contains)->order($sort)->toArray();
        }
    }
    
    public function getSumQuantityByProject($params = [], &$aProject = array()){
        $roles              = $this->find('all', ['conditions' => $params])->contain(['Roles'])->toArray();
        $aProject['aRoles'] = $roles;
        $collection         = new Collection($roles);
        return $collection->sumOf('quantity');
    }
    
    public function getListProjectIdsByOptions($conditions = []) {
        $userRoles = $this->find('list', ['conditions' => $conditions, 'group' => 'project_id', 'keyField' => 'id', 'valueField' => 'project_id'])->toArray();
        $result = [];
        foreach ($userRoles as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    // Input: array params (project_id,role_id,quantity,....)
    // Output: list array roles info
    public function getProjectRolesByField($params = [],$contains = ['Roles'],$fields = [],$limit = 0,$offset = 0,$sort = ['ProjectsRoles.created'=>'DESC']){
        if (empty($limit)){
            return $this->find('all', ['conditions' => $params])->select($fields)->contain($contains)->order($sort)->toArray();
        }
        else{
            return $this->find('all', ['conditions' => $params, 'limit'=>$limit,'offset'=>$offset])->select($fields)->contain($contains)->order($sort)->toArray();
        }
    }
}
