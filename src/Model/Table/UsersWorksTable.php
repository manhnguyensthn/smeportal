<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersWorks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\UsersWork get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersWork newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersWork[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersWork|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersWork patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersWork[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersWork findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersWorksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users_works');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->add('work_title', 'maxLength', [
                'rule' => ['maxLength', 25],
                 "message" => __("Title is required. Max length is 25 characters.")
                    ]
            )
            ->allowEmpty('work_title');

        $validator
            ->allowEmpty('work_description');

        $validator
            ->allowEmpty('work_url');

        $validator
            ->allowEmpty('work_thumbnail');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
    
    // Input: array data insert
    // Output: return TRUE  --> insert success
    // return list errors --> insert fail
    public function addWork($data = []){
        $entity = $this->newEntity();
        $work = $this->patchEntity($entity, $data);
        if ($this->save($work)){
            return TRUE;
        }
        else{
            return $work->errors();
        }
    }
}
