<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Token Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Token get($primaryKey, $options = [])
 * @method \App\Model\Entity\Token newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Token[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Token|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Token patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Token[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Token findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FollowingsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->table('followings');
        $this->primaryKey(['user_id', 'following_id']);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
//        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getFollowerFollowing($conditions = [], $fields = [], $limit = 0, $offset = 0, $valueField = 'following_id') {
        if ($limit != 0) {
            return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'limit' => $limit, 'offset' => $offset, 'valueField' => $valueField])->toArray();
        } else {
            return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'valueField' => $valueField])->toArray();
        }
    }

    public function getTotalFollowingFollower($UserId) {
        $followings = array();
        $followers = array();
        if ($UserId) {
            $UsersTable = TableRegistry::get('Users');
            $followings = $this->getFollowerFollowing(['user_id' => $UserId, 'following_id !=' => 0, 'connection' => 2]);
            if (!empty($followings)) {
                foreach ($followings as $key => $following) {
                    if (!$UsersTable->exists(['id' => $following->following_id])) {
                        unset($followings[$key]);
                    }
                }
            }

            $followers = $this->getFollowerFollowing(['following_id' => $UserId, 'user_id !=' => 0, 'connection' => 2]);
            if (!empty($followers)) {
                foreach ($followers as $key => $follower) {
                    if (!$UsersTable->exists(['id' => $follower->user_id])) {
                        unset($followers[$key]);
                    }
                }
            }
        }

        $Return = array();
        $Return['TotalFollowing'] = count($followings);
        $Return['TotalFollower'] = count($followers);
        $Return['followers'] = $followers;
        $Return['followings'] = $followings;
        return $Return;
    }

    public function getFollowUsers($conditions = [], $limit = 0, $offset = 0, $contain = []) {
        if ($limit != 0) {
            return $this->find('all', ['conditions' => $conditions,'limit' => $limit, 'offset' => $offset, 'contain' => $contain])->toArray();
        } else {
            return $this->find('all', ['conditions' => $conditions, 'contain' => $contain])->toArray();
        }
    }

}
