<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Projects Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Districts
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $Languages
 * @property \Cake\ORM\Association\HasMany $Stories
 * @property \Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\Project get($primaryKey, $options = [])
 * @method \App\Model\Entity\Project newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Project[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Project|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Project patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Project[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Project findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('projects');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id'
        ]);
        $this->belongsTo('Scales', [
            'foreignKey' => 'scale_id'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Languages', [
            'foreignKey' => 'lang_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('ProjectsRoles', [
            'foreignKey' => 'id',
            'bindingKey' => 'project_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('UsersProjects', [
            'foreignKey' => 'id',
            'bindingKey' => 'project_id',
            'joinType' => 'LEFT',
        ]);

        $this->belongsTo('UserProjectActions', [
            'foreignKey' => 'id',
            'bindingKey' => 'project_id',
            'joinType' => 'LEFT',
        ]);

        $this->hasMany('Stories', [
            'foreignKey' => 'project_id',
        ]);
        $this->belongsTo('Quantities', [
            'foreignKey' => 'quantity_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create', 'video_url', 'image_url', 'tags');

        $validator
                ->requirePresence('title', 'create')
                ->add('title', 'maxLength', [
                    'rule' => ['maxLength', 60],
                    'message' => __("Title is required. Max length of 60 chars. HTML or script tags is not allowed.")
                        ]
                )
                ->add("title", [
                    "customCheckHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'message' => __("Title is required. Max length of 60 chars. HTML or script tags is not allowed.")
                    ]]
                )
                ->notBlank('title', __('Title is required. Max length of 60 chars. HTML or script tags is not allowed.'))
                ->notEmpty('title', __('Title is required. Max length of 60 chars. HTML or script tags is not allowed.'));
        // $validator
        //         ->url('image_url', __('Project image should be: jpeg, png, gif, bmp with min size of 640x1136 resolution./ Invalid URL.'))
        //         ->allowEmpty('image_url', TRUE);
        $validator
                ->add("description", [
                    "customCheckHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'message' => __("Bắt buộc nhập tóm tắt. Tối đa 150 ký tự, không được nhập thẻ HTML hoặc thẻ script.")
                    ]]
        )
		 ->notEmpty('description', __('Không được để trống.'));
        $validator
                ->add("tags", [
                    "checkTags" => [
                        "rule" => [$this, "checkTags"], //add the new rule 'customFunction' to cedula field
                        'message' => __("HTML or script tags is not allowed. Maximum of 5 tags.")
                    ]]
                )
                ->add("tags", [
                    "customCheckHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'message' => __("HTML or script tags is not allowed. Maximum of 5 tags.")
                    ]]
                )
                ->allowEmpty('tags', TRUE);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['district_id'], 'Districts'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        // $rules->add($rules->existsIn(['group_id'], 'Groups'));
        $rules->add($rules->existsIn(['scale_id'], 'Scales'));
        $rules->add($rules->existsIn(['lang_id'], 'Languages'));

        return $rules;
    }

    public function checkTags($string = "") {
        $tags = explode(",", $string);
        if (count($tags) > 5) {
            return FALSE;
        }
        return TRUE;
    }

    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+/", $string, $m) != 1;
    }

    /**
     * get Project Featured
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getProject($ProjectId = 0, $CoundExt = array()) {
        if($ProjectId){
            $cound = array('Projects.id' => (int)$ProjectId);
            if($CoundExt) $cound = array_merge($cound, (array)$CoundExt);
            $aRow = $this->find('all')
                ->where($cound)
                ->contain(['Users', 'Categories', 'States', 'Districts', 'Countries'])
                ->first();
            return $aRow;
        }
        return false;
    }

    /**
     * get Project Featured
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getFeaturedProject($aCound = array(), $sOrder = 'rand()') {
        $aRow = $this->find('all')
            ->where($aCound)
            ->order($sOrder)
            ->contain(['Users', 'Categories', 'States', 'Countries'])
            ->first();
        return $aRow;
    }

    // Input:
    // options of getting result (all,list)
    // array of conditions
    // array of contain
    // Output: List projects by options
    public function getListProjectsByOptions($option = NULL, $conditions = [], $contains = [], $limit = 0, $offset = 0, $sort = ['Projects.created' => 'DESC'], $distinct = NULL) {
        if (empty($limit)) {
            if (empty($distinct)) {
                $result = $this->find($option, ['conditions' => $conditions])->contain($contains)->order($sort)->toArray();
            } else {
                $result = $this->find($option, ['conditions' => $conditions])->contain($contains)->order($sort)->distinct($distinct)->toArray();
            }
        } else {
            if (empty($distinct)) {
                $result = $this->find($option, ['conditions' => $conditions, 'limit' => $limit, 'offset' => $offset])->contain($contains)->order($sort)->toArray();
            } else {
                $result = $this->find($option, ['conditions' => $conditions, 'limit' => $limit, 'offset' => $offset])->contain($contains)->order($sort)->distinct($distinct)->toArray();
            }
        }
        return $result;
    }

    public function getAllProjectFolder($conditions = [], $fields = [], $limit = 0, $offset = 0,  $sort = ['Projects.created' => 'DESC']) {
        if ($limit != 0) {
            return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'limit' => $limit, 'offset' => $offset])->order($sort);
        } else {
            return $this->find('all', ['conditions' => $conditions, 'fields' => $fields])->order($sort);
        }
    }

    public function getOneProjectFolder($conditions = [], $fields = []) {
        return $this->find('all', ['conditions' => $conditions, 'fields' => $fields])->first();
    }

    public function getListProjectsAndCountJoinedForAPI($option = NULL, $conditions = [], $contains = [], $limit = 0, $offset = 0, $sort = ['Projects.created' => 'DESC']) {
        $query = $this->find($option, ['conditions' => $conditions, 'limit' => $limit, 'offset' => $offset])->contain($contains);
        $result = $query->select(['Projects.id', 'Projects.title', 'Projects.description', 'Projects.image_url', 'Projects.video_id', 'Projects.pitch', 'Projects.country_id', 'Projects.state_id', 'Projects.category_id', 'Users.first_name', 'Users.last_name', 'joined_counter' => $query->func()->count('UsersProjects.project_id')])->distinct('UsersProjects.project_id')->order($sort)->toArray();
        return $result;
    }
	public function getAllProjectUsers($user_id) {
        return $this->find('all', ['conditions' =>['user_id'=>$user_id]])->toArray();
    }

    public function getProjectByID($id)
    {
      return $this->find('all',['limit' => 10,'conditions'=>['id'=>$id]])->first();
    }

    public function getProjectByRequest($id){
        return $this->find('all',['conditions'=>['r.id'=>$id]])->join(
        ['requests_projects'=>[
          'table'=>'requests_projects',
          'alias'=>'rp',
          'type'=>'inner',
          'conditions'=>['Projects.id = rp.project_id'],
          ],
          'requests'=>[
             'table'=>'requests',
             'alias'=>'r',
             'type'=>'inner',
             'conditions'=>['r.id = rp.request_id']
            ]  
            ])->group('rp.project_id');
        // return $this->find('all',['contain'=>'RequestsProjects']);
    }
}
