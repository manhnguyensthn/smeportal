<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SearchedHistory Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\SearchedHistory get($primaryKey, $options = [])
 * @method \App\Model\Entity\SearchedHistory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SearchedHistory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SearchedHistory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SearchedHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SearchedHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SearchedHistory findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SearchedHistoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('searched_history');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('keyword');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * get User History Search
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getUserHistorySearchs($iUserId, $iLimit = '', $sOrder = 'created DESC'){
        if($iUserId){
            $aCound = array( 'user_id' => (int)$iUserId );
            $aRows  = $this->find('all')->where($aCound);
            $iLimit = (int)$iLimit;
            if($iLimit) $aRows->limit($iLimit);
            return $aRows->order($sOrder)->toList();
        }
        return false;
    }
    
    // Input:
    // options of getting result (all,list) 
    // array of conditions
    // array of contain
    // Output: List search history
    public function getListHistorySearchByOptions($option = NULL,$conditions = [],$contains = []){
        $result = $this->find($option,['conditions'=>$conditions])->contain($contains)->toArray();
        return $result;
    }
}
