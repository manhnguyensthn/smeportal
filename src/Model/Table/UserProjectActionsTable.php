<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserProjectActions Model
 *
 *
 * @method \App\Model\Entity\UserProjectActions get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserProjectActions newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserProjectActions[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserProjectActions|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserProjectActions patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserProjectActions[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserProjectActions findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserProjectActionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('user_project_actions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
    
    // Input: array param (project_id,action_type,user_id,...)
    // Output: array action info
    public function getUserProjectAction($params = []){
        return $this->find('all', ['conditions' => $params])->first();
    }
    
    // Input: array param (project_id,action_type,user_id,...)
    // Output: list array action info
    public function getListUserProjectAction($params = [],$contains = [],$limit = 0,$offset = 0,$sort = ['UserProjectActions.created'=>'DESC']){
        if (empty($limit)){
            return $this->find('all', ['conditions' => $params])->contain($contains)->order($sort)->toArray();
        }
        else{
            return $this->find('all', ['conditions' => $params,'limit'=>$limit,'offset'=>$offset])->contain($contains)->order($sort)->toArray();
        }
    }
	 public function getListUserFollowProjectAction($params = []){
			return $this->find('all', ['conditions' => $params])->order(['UserProjectActions.created' => 'DESC'])->toArray();
    }
    
    // Input: array data insert
    // Output: return TRUE  --> insert success
    // return array errors  --> insert fail
    public function addProjectAction($data = []){
        $entity = $this->newEntity();
        $projectActionData = $this->patchEntity($entity, $data);
        if ($this->save($projectActionData)){
            return TRUE;
        }
        else{
            return $projectActionData->errors();
        }
    }
    
    // Input: prject action id, array data update
    // Output: return TRUE  --> update success
    // return array errors --> update fail
    public function updateProjectAction($projectActionId = NULL,$data = []){
        $entity = $this->get($projectActionId);
        $projectAction = $this->patchEntity($entity, $data);
        if ($this->save($projectAction)){
            return TRUE;
        }
        else{
            return $projectAction->errors();
        }
    }
    
    public function getAllUserProjectActionFolder($conditions = [], $fields = [], $limit = 0, $offset = 0,  $sort = ['UserProjectActions.created' => 'DESC']){
        if($limit !=0){
            return $this->find('all',['conditions' => $conditions, 'fields' => $fields, 'limit' => $limit, 'offset' => $offset])->order($sort);
        }else{
            return $this->find('all',['conditions' => $conditions, 'fields' => $fields])->order($sort);
        }
    }
    
    public function getCountUserProjectActions($conditions = array()) {
        return $this->find('all', ['conditions' => $conditions])->count();
    }
}