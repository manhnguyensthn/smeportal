<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersProjects Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Projects
 *
 * @method \App\Model\Entity\UsersProject get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersProject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersProject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersProject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersProjectsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users_projects');
        $this->displayField('user_id');
//        $this->primaryKey(['user_id', 'project_id', 'role_id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function validationDefault(Validator $validator) {

        $validator
                ->integer('id')
                ->allowEmpty('id', 'user_picture', 'biography', 'created');
        $validator
                ->requirePresence('name', 'create')
                ->add('name', 'maxLength', [
                    'rule' => ['maxLength', 20],
                    'message' => __("Tên bắt buộc từ 1-20 ký tự. Bao gồm các ký tự A-Z, a-z, khoảng trắng, 0-9, ký tự Unicode (Japanese, Chinese...).")
                        ]
                )
                ->add("name", [
                    "customCheckHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'message' => __("Tên bắt buộc từ 1-20 ký tự. Bao gồm các ký tự A-Z, a-z, khoảng trắng, 0-9, ký tự Unicode (Japanese, Chinese...).")
                    ]]
                )
                ->notBlank('name', __("Tên bắt buộc từ 1-20 ký tự. Bao gồm các ký tự A-Z, a-z, khoảng trắng, 0-9, ký tự Unicode (Japanese, Chinese...)."))
                ->notEmpty('name', __("Tên bắt buộc từ 1-20 ký tự. Bao gồm các ký tự A-Z, a-z, khoảng trắng, 0-9, ký tự Unicode (Japanese, Chinese...)."));
        $validator
                ->add('biography', 'maxLength', [
                    'rule' => ['maxLength', 500],
                    'message' => __("Dude, tl:dr! Please keep the bio under 500 characters.  (Unicode characters are acceptable such as Japanese/Chinese/Arabic…")
                        ]
                )
                ->add("biography", [
                    "customCheckHtml" => [
                        "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                        'message' => __("Dude, tl:dr! Please keep the bio under 500 characters.  (Unicode characters are acceptable such as Japanese/Chinese/Arabic…")
                    ]]
                )
                ->allowEmpty('biography');
        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));
        return $rules;
    }

    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+/", $string, $m) != 1;
    }

    // Input: project id
    // Output: array list member, position in team sort by created time
    public function getUsersByProjectId($project_id = NULL) {
        return $this->find('all', ['conditions' => ['project_id' => $project_id, 'role_id != ' => 1]])->order(['UsersProjects.created' => 'DESC'])->contain(['Roles'])->toArray();
    }

    // Input: parameter array
    // Output: List Project Users
    public function getUserProjectsByOptions($options = array(), $limit = 0, $offset = 0) {
        if (empty($limit)) {
            return $this->find('all', ['conditions' => $options])->order(['UsersProjects.created' => 'DESC'])->contain(['Roles'])->toArray();
        } else {
            return $this->find('all', ['conditions' => $options, 'limit' => $limit, 'offset' => $offset])->order(['UsersProjects.created' => 'DESC'])->contain(['Roles'])->toArray();
        }
    }
	public function getUserProjectsJoin($options = array(), $limit = 0, $offset = 0) {
        if (empty($limit)) {
            return $this->find('all', ['conditions' => $options])->order(['UsersProjects.created' => 'DESC'])->contain(['Roles'])->distinct('user_id')->toArray();
        } else {
            return $this->find('all', ['conditions' => $options, 'limit' => $limit, 'offset' => $offset])->order(['UsersProjects.created' => 'DESC'])->contain(['Roles'])->toArray();
        }
    }

    // Input: parameter array
    // Output: List Project Users
    public function getUserProjectsByAllOptions($options = array(), $limit = 0, $offset = 0, $contains = ['Roles'], $distinct = NULL) {
        if (empty($limit)) {
            if (!empty($distinct)) {
                return $this->find('all', ['conditions' => $options])->order(['UsersProjects.created' => 'DESC'])->contain($contains)->distinct($distinct)->toArray();
            } else {
                return $this->find('all', ['conditions' => $options])->order(['UsersProjects.created' => 'DESC'])->contain($contains)->toArray();
            }
        } else {
            if (!empty($distinct)) {
                return $this->find('all', ['conditions' => $options, 'limit' => $limit, 'offset' => $offset])->order(['UsersProjects.created' => 'DESC'])->contain($contains)->distinct($distinct)->toArray();
            } else {
                return $this->find('all', ['conditions' => $options, 'limit' => $limit, 'offset' => $offset])->order(['UsersProjects.created' => 'DESC'])->contain($contains)->toArray();
            }
        }
    }

    // Input: parameter array
    // Output: Number of List Project Users
    public function getCountUserProjectsByOptions($options = array()) {
        return $this->find('all', ['conditions' => $options])->order(['UsersProjects.created' => 'DESC'])->contain(['Roles'])->count();
    }

    // Input: data array()
    // Output: 
    // - Return TRUE  --> Insert success
    // - Return array error  --> Insert fail
    public function addUserProject($data = array()) {
        $entity = $this->newEntity();
        $userProjectData = $this->patchEntity($entity, $data);
        if ($this->save($userProjectData)) {
            return TRUE;
        } else {
            return $userProjectData->errors();
        }
    }

    public function getListProjectIdsByOptions($conditions = []) {
        $userRoles = $this->find('list', ['conditions' => $conditions, 'group' => 'project_id', 'keyField' => 'id', 'valueField' => 'project_id'])->toArray();
        $result = [];
        foreach ($userRoles as $row) {
            $result[] = $row;
        }
        return $result;
    }

    public function getOneUserProjectFolder($conditions = [], $fields = [], $contain = []) {
        return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'contain' => $contain])->toArray();
    }

    public function getAllUserProjectFolder($conditions = [], $fields = [], $limit = 0, $offset = 0, $contain = [], $sort = ['UsersProjects.created' => 'DESC'], $distinct = FALSE) {
        if ($distinct == TRUE) {
            if ($limit != 0) {
                return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'limit' => $limit, 'offset' => $offset, 'contain' => $contain])->distinct('project_id')->order($sort)->toArray();
            } else {
                return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'contain' => $contain])->distinct('project_id')->order($sort)->toArray();
            }
        } else {
            if ($limit != 0) {
                return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'limit' => $limit, 'offset' => $offset, 'contain' => $contain])->order($sort)->toArray();
            } else {
                return $this->find('all', ['conditions' => $conditions, 'fields' => $fields, 'contain' => $contain])->order($sort)->toArray();
            }
        }
    }

    public function getListProjectIdSortByJoined() {
        $query = $this->find('all');
        $data = $query->select(['project_id', 'joined_counter' => $query->func()->count('project_id')])->distinct('project_id')->order(['joined_counter' => 'DESC'])->toArray();
        $result = [];
        foreach ($data as $row) {
            $result[] = $row['project_id'];
        }
        return $result;
    }
	 public function getMessage_content($conditions = []) 
	   {   
		return $this->find('all', ['conditions' => $conditions])->toArray();
		}
	public function getAllProejectJoinUser($user_id)
	{	
		return $this->find('all', ['conditions' => ['user_id' => $user_id,'status' => 1]])->toArray();	   
	}			
		
}
