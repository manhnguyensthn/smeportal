<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ValidationCodes Model
 *
 * @method \App\Model\Entity\ValidationCode get($primaryKey, $options = [])
 * @method \App\Model\Entity\ValidationCode newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ValidationCode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ValidationCode|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ValidationCode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ValidationCode[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ValidationCode findOrCreate($search, callable $callback = null)
 */class ValidationCodesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('validation_codes');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('code', 'create')            ->notEmpty('code')            ->add('code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
        $validator
            ->integer('status')            ->allowEmpty('status');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['code']));

        return $rules;
    }
}
