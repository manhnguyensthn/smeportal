<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersProjects Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Projects
 *
 * @method \App\Model\Entity\UsersProject get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersProject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersProject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersProject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersProject findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsMilestonesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('project_milestones');
        $this->primaryKey(['id']);

        $this->addBehavior('Timestamp');
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->add("title", [
                "checkHtml" => [
                    "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                    'message' => __('Title is required. Max length of 60 chars.HTML or script tags is not allowed.')
                ]
                ]
            )
            ->add('title', 'maxLength', [
                    'rule' => ['maxLength', 60],
                    'message' => __("Title is required. Max length of 60 chars.HTML or script tags is not allowed.")
                ]
            )
            ->add('title', 'minLength', [
                    'rule' => ['minLength', 1],
                    'message' => __("Title is required. Max length of 60 chars.HTML or script tags is not allowed.")
                ]
            )
            ->notEmpty('title');
        
        $validator
            ->add("description", [
                "checkHtml" => [
                    "rule" => [$this, "customCheckHtml"], //add the new rule 'customFunction' to cedula field
                    'message' => __("Max length of 120 chars. HTML or script tags is not allowed.")
                ]
                ]
            )
            ->add('description', 'maxLength', [
                    'rule' => ['maxLength', 120],
                    'message' => __("Max length of 120 chars. HTML or script tags is not allowed.")
                ]
            )
            ->notEmpty('description');
        return $validator;
    }

    public function customCheckHtml($string = "") {
        return preg_match("/<[^<]+>/", $string, $m) != 1;
    }
}