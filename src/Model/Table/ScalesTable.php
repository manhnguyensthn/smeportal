<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Scales Model
 *
 * @property \Cake\ORM\Association\HasMany $Projects
 *
 * @method \App\Model\Entity\Scale get($primaryKey, $options = [])
 * @method \App\Model\Entity\Scale newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Scale[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Scale|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Scale patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Scale[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Scale findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ScalesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('scales');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Projects', [
            'foreignKey' => 'scale_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    public function getListScalesByOptions($options = array()){
        return $this->find('all',['conditions'=>$options])->toArray();
    }
    
    public function getScales($scale_id= null) {
        if ($scale_id == null) {
            return $this->find('all');
        } else {
            return $this->get($scale_id);
        }
    }
}
