<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * UsersRoles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\UsersRole get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersRole newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersRole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersRole|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersRole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersRole[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersRole findOrCreate($search, callable $callback = null)
 */
class RequestsProjects extends Table {

  public function initialize(array $config) {
      parent::initialize($config);

      $this->table('requests_projects');
      $this->displayField('request_id');
      $this->primaryKey(['request_id', 'project_id']);

      $this->belongsTo('Requests', [
          'foreignKey' => 'request_id',
          'joinType' => 'INNER'
      ]);
      $this->belongsTo('Projects', [
          'foreignKey' => 'project_id',
          'joinType' => 'INNER'
      ]);
  }

  public function buildRules(RulesChecker $rules) {
      $rules->add($rules->existsIn(['request_id'], 'Requests'));
      $rules->add($rules->existsIn(['project_id'], 'Projects'));

      return $rules;
  }

  public function validationDefault(Validator $validator) {

  }

  // public function getProjectReceive($request_id, $requests_projects_id)
  // {
  //   return $this->find('all',['conditions'=>['request_id'=>$request_id,'id'=>$requests_projects_id]]);
  // }

}
