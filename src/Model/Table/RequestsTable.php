<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


use App\Controller\Api\ApiController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

/**
 * Requests Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Projects
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Request get($primaryKey, $options = [])
 * @method \App\Model\Entity\Request newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Request[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Request|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Request[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Request findOrCreate($search, callable $callback = null, $options = [])
 */
class RequestsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('requests');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    // public $validate = array(
    //     'title' => array(
    //         'rule' => array('custom', '[a-zA-Z]'),
    //         'message' => 'Please enter an infinite number.'
    //     )
    // );
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->integer('type')
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->integer('receiver_id1')
            ->allowEmpty('receiver_id1');

        $validator
            ->integer('receiver_id2')
            ->allowEmpty('receiver_id2');

        $validator
            ->integer('receiver_id3')
            ->allowEmpty('receiver_id3');

        $validator
            ->allowEmpty('attach1');

        $validator
            ->allowEmpty('attach2');

        $validator
            ->allowEmpty('repattach1');

        $validator
            ->allowEmpty('repattach2');

        $validator
            ->allowEmpty('repcontent1');

        $validator
            ->allowEmpty('repcontent2');

        $validator
            ->integer('status');
            // ->requirePresence('status', 'create')
            // ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function getCountRequests($conditions)
    {
      return $this->find('all',['conditions'=>$conditions])->count();
    }

    // public function getListRequestUserAPI($id){
    //   $conn = ConnectionManager::get('default');
    //   $query = 'Select *
    //             From requests
    //             inner join (select request_id from requests_projects where project_id in(select id from projects  where user_id = '.$id.') group by request_id) as A
    //             on requests.id = A.request_id
    //             where status = 2';
    //   return $stmt = $conn->execute($query);
    // }

    public function getListRequestUser($id,$conditions)
    {

      $conditions['Requests.status']=2;
      return $this->find('all',['conditions'=>$conditions])->join(
        ['requests_projects'=>[
          'table'=>'requests_projects',
          'alias'=>'rp',
          'type'=>'inner',
          'conditions'=>['Requests.id = rp.request_id'],
          ],
          'projects'=>[
             'table'=>'projects',
             'alias'=>'p',
             'type'=>'inner',
             'conditions'=>['p.id = rp.project_id','p.user_id'=>$id]
            ]  
            ])->group('rp.request_id');
      
    }

    public function getListOwnerRequestByUser($id,$conditions)
    {
      $conditions['Requests.user_id'] = $id;

      return $this->find('all',['conditions'=>$conditions,'contain'=>['Projects']]);
    }

    public function getListRequestByAdminSME($admin,$conditions)
    {
      if($admin==5)
      {
        return $this->find('all',['conditions'=>$conditions,'contain'=>['Projects']]);
      }elseif($admin==1||$admin==2||$admin==3||$admin==4)
      {
        $conditions['Requests.type'] = $admin;
        return $this->find('all',['conditions'=>$conditions,'contain'=>['Projects']]);

      }else{
        return null;
      }
    }
}
