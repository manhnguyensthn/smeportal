<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * UsersRoles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\UsersRole get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersRole newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersRole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersRole|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersRole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersRole[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersRole findOrCreate($search, callable $callback = null)
 */
class UsersRolesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users_roles');
        $this->displayField('user_id');
        $this->primaryKey(['user_id', 'role_id']);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator->add('role_id', [
            'unique' => [
                'rule' => ['validateUnique', ['scope' => 'user_id']],
                'provider' => 'table',
                'message' => __('Both roles are the same.')
            ]
        ]);

        return $validator;
    }

    /**
     * get Roles User
     * @author Roxannie Nguyen jr
     * @return array
     */
    public function getUserRoles($iUserId) {
        if ($iUserId) {
            $aCound = array('user_id' => (int) $iUserId);
            return $this->find('all')->where($aCound)->toList();
        }
        return false;
    }

    // Input:
    // options of getting result (all,list) 
    // array of conditions
    // array of contain
    // Output: List user roles
    public function getListUsersRolesByOptions($option = NULL, $conditions = [], $contains = []) {
        $result = $this->find($option, ['conditions' => $conditions])->contain($contains)->toArray();
        return $result;
    }

    public function findUserRolesByKeyword($conditions = [], $limit = 0, $offset = 0) {
        if ($limit != 0) {
            $userRoles = $this->find('all', ['conditions' => $conditions, 'fields' => ['Users.id', 'Users.name', 'Users.avatar'], 'limit' => $limit, 'offset' => $offset, 'contain' => ['Users']])->toArray();
        } else {
            $userRoles = $this->find('all', ['conditions' => $conditions, 'fields' => ['Users.id', 'Users.name', 'Users.avatar'], 'contain' => ['Users']])->toArray();
        }
        return $userRoles;
    }
   

}
