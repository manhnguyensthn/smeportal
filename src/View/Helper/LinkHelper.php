<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;


class LinkHelper extends UrlHelper {
    
    public function build($url = null, $full = false) {
        if(!isset($url['language']) && isset($this->request->params['language'])) {
          $url['language'] = $this->request->params['language'];
        }
        return parent::build($url, $full);
   }
}