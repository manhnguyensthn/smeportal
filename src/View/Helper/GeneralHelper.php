<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;
use App\Lib\ProjectsLib;

//use Cake\View\Helper\UrlHelper;

class GeneralHelper extends Helper {

    public $helpers = ['Tool', 'AuthUser', 'Url'];

    /**
     * get default Message
     * @author Duc Nguyen
     * @return json
     */
    public function getNoItemMessage($sText = '') {
        ProjectsLib::getNoItemMessage($sText);
    }

    /**
     * Get Image
     * @author Roxannie Nguyen jr.
     * @param string $sImgPath: Path Image
     * @param string $sAlt: Alt
     * @param string $sType: Type Image
     * @param boolean $bIsBorderBg
     * @param string $sClass: Selector class of Image
     * @return json
     */
    public function getImage($sImgPath = '', $sAlt = '', $sType = '', $bIsBorderBg = false, $bHrmlImage = true, $sClass = 'image-default') {
        return ProjectsLib::getImage($sImgPath, $sAlt, $sType, $bIsBorderBg, $bHrmlImage, $sClass);
    }

    /**
     * full name
     * @author Roxannie Nguyen jr
     * @param array $aUser: info User container first_name and last_name
     * @param boolean $bTemplate: if true echo full_name, fale push full_name to array $aUser
     * @return N/A
     */
    public function getFullName(&$aUser = null, $bTemplate = true) {
        return ProjectsLib::getFullName($aUser, $bTemplate);
    }

    /**
     * full name
     * @author Roxannie Nguyen jr
     * @param array $aProject: info Project container id,name
     * @param boolean $bTemplate: if true echo link project, fale push link project to array $aProject
     * @return N/A
     */
    public function getLinkProject(&$aProject = null, $bTemplate = true) {
        if (isset($aProject->id) && isset($aProject->title)) {
            $sTitle = $this->Tool->slug($aProject->title) . '-' . $aProject->id;
            $sLink = '/projects/' . $sTitle;
            if ($bTemplate) {
                echo $sLink;
            } else {
                $aProject['url_project'] = $sLink;
            }
            
            return $sLink;
        }
        return false;
    }

    /**
     * get Day Left
     * @author Roxannie Nguyen jr
     * @param array $aProject: info Project container start_date, end_date
     * @param boolean $bTemplate: if true echo link project, fale push link project to array $aProject
     * @return N/A
     */
    public function getDayLeft(&$aProject = null, $bTemplate = true) {
        if (!empty($aProject['end_date'])) {
            $iLeftDay = $this->AuthUser->_sub_datetime(date('Y-m-d', strtotime($aProject['end_date'])), date('Y-m-d'));
        } else {
            $iLeftDay = 0;
        }

        if($iLeftDay < 0) $iLeftDay = 0;

        if ($bTemplate) {
            if($iLeftDay <= 0){
                echo "<span class='greenlit'>".__('Greenlit')."</span>";
            }else{
                echo sprintf(__('%s days left'), $iLeftDay);
            }
        } else {
            $aProject['iLeftDay'] = $iLeftDay;
        }
    }

    public function getRoleJoin($aProject = null){
        if(isset($aProject) && !empty($aProject)){
            $sHtml = '';
            if(($aProject->iTotalRoleJoined >= $aProject->iQuantity) && !empty($aProject->iTotalRoleJoined)){
                $sHtml .= "<span class='filled'>".__('Filled')."</span>";
            }else{
                $sHtml .= sprintf(__('%s of %s joined'), $aProject->iTotalRoleJoined, $aProject->iQuantity);
            }
            echo $sHtml;
        }
    }

    /**
     * get Day Left
     * @author Roxannie Nguyen jr
     * @param array $aProject: info Project container start_date, end_date
     * @param boolean $bTemplate: if true echo link project, fale push link project to array $aProject
     * @return N/A
     */
    public function getLinkCategory($aCategry = null) {
        if(isset($aCategry) && !empty($aCategry)){
            return 'discover/index?filterCategory=' . $aCategry->id . '|' . $aCategry->name .'|1|2';
        }
        return 'javascript:void(0);';
    }

    /**
     * get avata
     * @author Roxannie Nguyen jr
     * @param array $aUser
     * @param string $sLink
     * @return N/A
     */
    public function getAvata($aUser = null, $sLink = '') {
        $sHtml = '<div class="img_avata"><div class="img_user"><a '.($sLink ? 'href="'.$sLink.'"' : '').'>';
        if(isset($aUser->avatar) && !empty($aUser->avatar)){
            $sHtml .= '<img src="'.$aUser->avatar.'" alt="'.$aUser->name.'" title="'.$aUser->name.'" />';
        }else{
            $sHtml .= '<img src="/img/avatar_default.jpg" alt="Avatar" title="Avatar" />';
        }
        $sHtml .= '</a></div></div>';
        return $sHtml;
    }

    /**
     * template Following
     * @author Roxannie Nguyen jr
     * @param int $ItemId
     * @param string $TemPlate
     * @return N/A
     */
    public function templateFollowingCollaborator($ItemId, $TemPlate = '', $TypeId = 'following') {
        $sHtml   = '';
        if($TemPlate == 'no_action'){
            $sHtml .= '<div class="template-following following_'.$ItemId.'">';
                $sHtml .= '<a href="javascript:void(0)">';
                    $sHtml .= __('Following');
                $sHtml .= '</a>';
            $sHtml .= '</div>';
            return $sHtml;
        }

        $aUser   = $this->AuthUser->_getUser();
        $class   = '';
        $bFollow = false;
        switch ($TemPlate) {
            case 'lg': $class .= ' font-lg '; break;
        }
        if($aUser && $aUser['id'] != $ItemId){

            $blocksTable = TableRegistry::get('Blocks');
            $isBlock = $blocksTable->find()
                ->where(['user_id' => $aUser['id'], 'blocked_id' => $ItemId])
                ->orWhere(['user_id' => $ItemId, 'blocked_id' => $aUser['id']])
                ->count();

            if(!$isBlock){
                $followingsTable = TableRegistry::get('Followings');
                if($TypeId == 'follower'){
                    $aCound = ['following_id' => $aUser['id'], 'user_id' => $ItemId, 'connection' => 2];
                }else{
                    $aCound = ['following_id' => $ItemId, 'user_id' => $aUser['id'], 'connection' => 2];
                }

                $followers = $followingsTable->find('all')->where($aCound)->first();
                $connection = 1;
                if($followers){
                    $bFollow = true;
                    $connection = $followers->connection;
                }else{
                    $class .= ' unfollow ';
                }
                $sHtml .= '<div class="template-following following_'.$ItemId.'">';

                    $sActionJs = 'addFollowing(this, \''.$ItemId.'\', \''.$connection.'\', \''.$TypeId.'\'); return false;';
                    $sHtml .= '<a href="javascript:void(0)" onclick="'.$sActionJs.'" class="'.$class.'">';
                        $sHtml .= (!$bFollow ? __('Follow +') : __('Following'));
                    $sHtml .= '</a>';

                $sHtml .= '</div>';
            }
        }
        return $sHtml;
    }

    /**
     * template Save
     * @author Roxannie Nguyen jr
     * @param int $UserId: 
     * @param boolean $bTemplate: if true echo link project, fale push link project to array $aProject
     * @return N/A
     */
    public function templateSaveCollaborator($UserId, $ProjectId, $RoleId) {
        $aUser = $this->AuthUser->_getUser();
        $sHtml = '';
        $class = '';
        if($aUser && $aUser['id'] != $UserId){
            $sHtml = '<a href="javascript:void(0);" onclick="actionSave(this, \''.$UserId.'\', \''.$ProjectId.'\', \''.$RoleId.'\'); return false;" class="action-item" style="width: 150px; background: #fbce58;">'.__('Save').' <i class="fa fa-star" aria-hidden="true"></i></a>';
        }
        return $sHtml;
    }
    
    /**
     * template Save
     * @author Roxannie Nguyen jr
     * @param int $UserId: 
     * @param int $ProjectId
     * @param int $RoleId
     * @return html
     */
    public function templateOfferCollaborator($UserId, $ProjectId, $RoleId){
        $aUser = $this->AuthUser->_getUser();
        $sHtml = '';
        $class = '';
        if($aUser && $aUser['id'] != $UserId){
            $sHtml .= '<a href="javascript:void(0);" onclick="actionOffer(this, \''.$UserId.'\', \''.$ProjectId.'\', \''.$RoleId.'\'); return false;" class="action-item" style="width: 150px; background: #62bc55;">'.__('Chỉ định').' <i class="fa fa-check-circle" aria-hidden="true"></i></a>';
        }
        return $sHtml;
    }

    /**
     * template Block
     * @author Roxannie Nguyen jr
     * @param int $UserId: 
     * @return html
     */
    public function templateBlockUser($UserId, $TypeId = 'block'){
        $aUser = $this->AuthUser->_getUser();
        $sHtml = '';
        $class = '';
        if($aUser && $aUser['id'] != $UserId){
            $blocksTable = TableRegistry::get('Blocks');
            $isBlock = $blocksTable->find()
                ->where(['user_id' => $aUser['id'], 'blocked_id' => $UserId])
                ->orWhere(['user_id' => $UserId, 'blocked_id' => $aUser['id']])
                ->count();

            if(!$isBlock){
                if($TypeId == 'block'){
                    $sHtml .= '<div class="template-block"><a href="javascript:void(0)" onclick="confirmBox(this, \''.__('Are you sure you want to block?').'\'); return false;" data-action="actionBlockCollaborator(this, \''.$UserId.'\');" class="icon_block" title="'.__('Block').'">'.__('Block').'</a></div>';
                }
            }else{
                if($TypeId == 'unblock'){
                    $sHtml .= '<div class="template-block unblock"><a href="javascript:void(0)" onclick="confirmBox(this, \''.__('Are you sure you want to Unblock?').'\'); return false;" data-action="actionBlockCollaborator(this, \''.$UserId.'\', \'1\');" title="'.__('Unblock').'">'.__('Unblock').'</a></div>';
                }
            }
        }
        return $sHtml;
    }
}