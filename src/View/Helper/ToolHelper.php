<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Utility\Inflector;
//use Cake\View\Helper\UrlHelper;

class ToolHelper extends Helper {
	public function stripUnicode($string) {
		$unicode = array(
			'a' => 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
			'd' => 'đ|Đ',
			'e' => 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
			'i' => 'ì|í|ị|ỉ|ĩ|Í|Ì|Ỉ|Ĩ|Ị',
			'o' => 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
			'u' => 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ',
			'y' => 'ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ'
		);
		foreach($unicode as $nonUnicode => $uni){
			$string = preg_replace("/($uni)/", $nonUnicode, $string);
		}
		return $string;
                //exit;
	}
	
	public function slug($string, $character = '-') {
            $string = $this->stripUnicode($string);
            $string = Inflector::slug($string, $character);
            return strtolower($string);
        }
        
	public function generate_code(){
		$rand_number = rand(1000000, 9999999);
		$code = md5($rand_number);
		return $code;
	}
	
	public function RecursiveCategories($array) {
		//pr($array);exit;
		if (count($array)) { 
			
			foreach ($array as $vals) { 
				
				echo "<li>\n";
				echo '<a href="/'.$vals['Category']['slug'].'.html" >'. $vals['Category']['name'] .'</a>'."\n";
				if (count($vals['children'])) { 
					echo '<ul class="menu2">'."\n";
						foreach($vals['children'] as $child){
							echo "<li>\n";
							echo '<a href="/'.$child['Category']['slug'].'.html" >'. $child['Category']['name'] .'</a>'."\n";
							echo "</li>\n"; 
						}
					
					echo "</ul>\n";
				} 
				echo "</li>\n"; 

				// echo '<a href="/'.$vals['Category']['slug'].'.html">'. $vals['Category']['name'] .'</a>'."\n";
				// echo "\n".'<ul class="menu2">'."\n";
				// if (count($vals['children'])) { 
					// $this->RecursiveCategories($vals['children']); 
				// } 
				// echo "</ul>\n"; 
				
			} 
			
		} 
	
	}
	
}
?>