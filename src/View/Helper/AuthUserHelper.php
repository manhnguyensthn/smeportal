<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

/**
 * Helper to access auth user data.
 *
 * @author Mark Scherer
 */
class AuthUserHelper extends Helper {

    use StringTemplateTrait;

    /**
     * AuthUserHelper::_getUser()
     *
     * @return array
     */
    public function _getUser() {
        if (!isset($this->_View->viewVars['authUser'])) {
            return FALSE;
        }
        return $this->_View->viewVars['authUser'];
    }

    public function _sub_datetime($date1 = NULL, $date2 = NULL) {
//        Format date1: YYYY-MM-DD or YYYY/MM/DD
//        Format date2: YYYY-MM-DD or YYYY/MM/DD        
        $days = (strtotime($date1) - strtotime($date2)) / (60 * 60 * 24);
        return $days;
        // return day
    }

//    Input: list actions by current user, type action
//    output: TRUE or FALSE
    public function _check_user_project_action($listActions = array(), $type = 0) {
        foreach ($listActions as $action) {
            if ($action['action_type'] == $type) {
                return TRUE;
            }
        }
        return FALSE;
    }

    // Input: list comments for project, id of parent comment
    // Output: list comments by parent input
    public function _get_list_comment_by_parent_id($list_comments = array(), $parent_id = NULL) {
        $result = [];
        foreach ($list_comments as $comment) {
            if ($comment['parent_comment_id'] == $parent_id) {
                $result[] = $comment;
            }
        }
        return $result;
    }

    // Input: dateTime
    // Output: day ago or hour ago or minute
    public function _get_time_ago($datetimeInput = NULL) {
        $currentDateTime = date('Y-m-d H:i:s');
        $days = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60 * 24);
        if ($days >= 30) {
            return round($days/30) . __(' months ago');
        } else {
            if ($days >= 1) {
                return round($days) . __(' days ago');
            } else {
                $hours = (strtotime($currentDateTime) - strtotime($datetimeInput)) / (60 * 60);
                if ($hours >= 1) {
                    return round($hours) . __(' hours ago');
                } else {
                    $minutes = (strtotime($currentDateTime) - strtotime($datetimeInput)) / 60;
					if($minutes < 1) {
					return 0 . __(' minute ago');
					}
					else
					{
						 return round($minutes) . __(' minutes ago');
					}
						return round($minutes) . __(' minutes ago');
					}
            }
        }
    }

    // Input: List Projects Role
    // Output: Number of quantity member
    public function _get_total_quantity_member_by_projects($projectRoles = array()) {
        $counter = 0;
        foreach ($projectRoles as $row) {
            $counter = $counter + $row['quantity'];
        }
        return $counter;
    }

    // Input: List all collaborator request
    // Output: List collaborator by status
    public function _get_list_collaborator_by_status($listCollaborator = [], $status = 1) {
        $dataRespone = [];
        foreach ($listCollaborator as $row) {
            if ($row['status'] == $status) {
                $dataRespone[] = $row;
            }
        }
        return $dataRespone;
    }

    // Input: List all collaborator request
    // Output: True/False
    public function _check_user_request($listCollaborator = [], $user_id = NULL, $project_role_id = NULL) {
        foreach ($listCollaborator as $row) {
            if ($row['user_id'] == $user_id && $row['project_role_id'] == $project_role_id) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function _check_is_member_of_project($listMembers = array(), $current_user_id = 0) {
        foreach ($listMembers as $item) {
            if ($item['user_id'] == $current_user_id) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function _character_limiter($str, $n = 500, $end_char = '&#8230;') {
        if (strlen($str) < $n) {
            return $str;
        }

        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

        if (strlen($str) <= $n) {
            return $str;
        }

        $out = "";
        foreach (explode(' ', trim($str)) as $val) {
            $out .= $val . ' ';

            if (strlen($out) >= $n) {
                $out = trim($out);
                return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
            }
        }
    }

    public function _limit_by_length_string($str = NULL, $lenght = 0) {
        if (strlen($str) <= $lenght) {
            return $str;
        } else {
            return substr($str, 0, $lenght) . '...';
        }
    }

    public function _check_user_action_by_project_and_user($listActions = [], $project_id = 0, $user_id = 0, $action_type = 1) {
        foreach ($listActions as $row) {
            if ($row['project_id'] == $project_id && $row['user_id'] == $user_id && $row['action_type'] == $action_type) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function _get_role_open_in_list($listUserJoined = [], $project_id = 0, $project_role_id = 0) {
        $counter = 0;
        foreach ($listUserJoined as $row) {
            if ($row['project_id'] == $project_id && $row['status'] == 1 && $row['project_role_id'] == $project_role_id) {
                $counter++;
            }
        }
        return $counter;
    }

}
