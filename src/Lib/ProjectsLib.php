<?php
namespace App\Lib;
use App\Controller\AppController;

if ( !class_exists('Projects') ) {
    class ProjectsLib{
        /**
         * Get Image
         * @author Roxannie Nguyen jr.
         * @param string $sImgPath: Path Image
         * @param string $sAlt: Alt
         * @param string $sType: Type Image
         * @param boolean $bIsBorderBg
         * @param string $sClass: Selector class of Image
         * @return json
         */
        public static function getImage($sImgPath = '', $sAlt = '', $sType = '', $bIsBorderBg = false, $bHrmlImage = true, $sClass = 'image-default'){
            $sHtml = '';
            $bImageDefault = false;
            if($sImgPath){
//                list($width, $height) = @getimagesize($sImgPath);
//                if($width && $height){
//                    $bImageDefault = true;
//                }
                 $bImageDefault = true;
            }

            if($bImageDefault){
                if($bHrmlImage){
                    $sHtml .= '<img class="'.$sClass.'" src="'.$sImgPath.'" alt="'.strip_tags($sAlt).'">';
                }else{
                    $sHtml .= '<span class="'.$sClass.' background-image" style="background-image: url('.$sImgPath.');" title="'.strip_tags($sAlt).'"></span>';
                }
            }else{
                $sHtml .= '<div class="no-image'.($bIsBorderBg ? ' is_border_bg' : '').'">';
                    $sHtml .= '<span style="background-image: url(/img/project_default.jpg);" class="image-default"></span>';
                    //$sHtml .= '<img src="/img/project_default.jpg" class="image-default" alt="image default">';
                $sHtml .= '</div>';
            }

            return $sHtml;
        }

        /**
         * get default Message
         * @author Duc Nguyen
         * @return json
         */
        public static function getNoItemMessage($sText = ''){
            $sHtml = '<div class="no-item-message">'.($sText ? strip_tags(trim($sText)) : __('No content display')).'</div>';
            echo $sHtml;
        }

        /**
         * full name
         * @author Roxannie Nguyen jr
         * @param array $aUser: info User container first_name and last_name
         * @param boolean $bTemplate: if true echo full_name, fale push full_name to array $aUser
         * @return N/A
         */
        public static function getFullName(&$aUser = null, $bTemplate = true) {
            if (isset($aUser->first_name) && isset($aUser->last_name)) {
                if ($bTemplate) {
                    $sHtml = '';
                    $sHtml .= $aUser->first_name . ' ' . $aUser->last_name;
                    echo $sHtml;
                } else {
                    $aUser['full_name'] = $aUser->first_name . ' ' . $aUser->last_name;
                    return $aUser->first_name . ' ' . $aUser->last_name;
                }
            }
        }
    }
}