<?php
namespace App\Lib;
use App\Controller\AppController;
use Cake\Core\Configure;

if ( !class_exists('CoreLib') ) {
	class CoreLib{
		/**
         * get Data Language full
         * @author Roxannie Nguyen jr
         * @param array $aUser: info User container first_name and last_name
         * @param boolean $bTemplate: if true echo full_name, fale push full_name to array $aUser
         * @return N/A
         */
        public static function getDataLanguage(){
            return array(
                'en' => array(
                    'code' => 'en_US',
                    'name' => __('English')
                ),
                'es' => array(
                    'code' => 'es_US',
                    'name' => __('Spanish'),
                ),
                'ja' => array(
                    'code' => 'ja_JP',
                    'name' => __('Japan')
                ),
                'vn' => array(
                    'code' => 'vn_VN',
                    'name' => __('Vietnamese')
                ),
            );
        }

        /**
         * get Language Active
         * @author Roxannie Nguyen jr
         * @param array $aUser: info User container first_name and last_name
         * @param boolean $bTemplate: if true echo full_name, fale push full_name to array $aUser
         * @return N/A
         */
        public static function getLanguageActive(){
            $aDataLanguages = CoreLib::getDataLanguage();
            if(isset($_POST['language']) && array_key_exists($_POST['language'], $aDataLanguages)){
                $dataLanguage = $_POST['language'];
            }else{
                $dataLanguage   = (isset($_SESSION['Config']['language']) ? $_SESSION['Config']['language'] : 'en');
                if(!array_key_exists($dataLanguage, $aDataLanguages)){
                    $dataLanguage = 'en';
                }
            }
            return $dataLanguage;
        }

        /**
         * get Language full
         * @author Roxannie Nguyen jr
         * @param array $aUser: info User container first_name and last_name
         * @param boolean $bTemplate: if true echo full_name, fale push full_name to array $aUser
         * @return N/A
         */
        public static function getLanguage($bAllLang = true){
			$aDataLanguages = CoreLib::getDataLanguage();			
			$dataLanguage   = (isset($_SESSION['Config']['language']) ? $_SESSION['Config']['language'] : 'en');
			if(!array_key_exists($dataLanguage, $aDataLanguages)){
				$dataLanguage = 'en';
			}
            foreach ($aDataLanguages as $key => $aLanguage) {
                if($dataLanguage == $key){
                	$aDataLanguages[$key]['active'] = true;
                }
            }
            if($bAllLang){
            	return $aDataLanguages;
            }else{
            	return $aDataLanguages[$dataLanguage];
            }
        }

	}
}