<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;

/**
 * Simple console wrapper around Psy\Shell.
 */
class RunCronjobQueueShell extends Shell {
    public function main()
    {
        // Run move old notification chat to notification chat history
        $rootUrl = 'https://testlab.vietis.com.vn:488/';
        $link = $rootUrl.'Cronjob/runCronjobQueue';
        file_get_contents($link);
    }
}
?>