<?php

use Cake\Core\Configure;

#define key firebase
define('FIREBASE_SERVICE_EMAIL', 'wtpfirebase@gmail.com');
define('FIREBASE_PRIVATE_KEY', '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDg0ADiW4sNIse6\nkiESXTs/XYp2qZ7jVw07T+JYS5rGT6faId3TNkHm62pFLx30P/+AmtnSNtR37KBK\n0ncrCLOSKft9uLf4gHOL7+zJyvWWVJB2qWE/6nHu1ChrI7IiZGESIHlsi3gWqWDF\nQcH+M2G5TqqO8fnF1PIMTJfBalfH7wkUgoF32r8S0bzD3e26GS+EdZJvkMGHCmBc\nspMxhIvJce+k0QvYBLKw6rHwN6KH89RgrMStq1EJ3mpQIXwfArIVWYXsl/MjTp3N\nrtaDMyhlfUoL3/dR9Mb//E01aLoRT1yqSgOUlvc4036bt1IEkgDPuMp2p6SsxFtk\nlgBQU8vxAgMBAAECggEAcNqaBweTVyXFg7gFmBVrP5TyhD0EAPXbM5IXeuGBMdoc\n1di4ZWXQUV6OO2q6q7lQxrt1axyRLh6AHO9oWRcGJfKjR+ppl/rLhk0L3gTfofaT\niq3entpPFWg1XgtOSgt/2zyoF8AvqZifdoZbHwOmJAMEhbuc+h264Ee2yqZaqIc6\nQ8LwOQevBJuCIBxUIY0PwqcbGXzwnxqBX69ykseJXabikV7NJ4vAMXHoVaKmioFk\n/0k7JEU584+DbWvL+Kum00ex8GwL32EQP2qDgMFZZgtYB2o/mUvUpbEr9pO+ljDV\nDZW6y5QXapfbeNtexYolxiMHHLgnsWmMjuyOGU3MAQKBgQD6De56KcTbzBw1xD95\nNrps7fBWefxO1yOV2Oq18w66OisuZcfqQhwuvJ+EkOHzF8IudOpvYUqMyTngFwd8\neWsaqzoTIeTNtOSSq7olCybheHJUex3cUUOy1axo5Aq1Fi4mhabjPgZS1XczIC5E\nGIAOGMaOBuzrDCA4PR3wicWiwQKBgQDmKGz7TG/DMvSw44JQ1CG4b3t0dpBcpl5L\nQaPphXD0P2YA03R+p/Y6cSM0uLo6L+GY18hSUG3iYjs8BUSZQ3DMIE/yt4rIJwc7\nAf/gL7KeYMENDzImTgkQO0rgJCTjH+i7dVzxZ/sLgHX3FHzVLFRYIpR55VXUEHM5\nReK0linlMQKBgQCJbPKtrRsF+W1fl1WTFqLqjvJRdIVYByEFj8ue59AmRSXKQCHY\n+zBl90BsKPwlD9+mjJbzU7MXINhv8fgpTsmwRH6MWUpYS5x7h2msRi6+J+Ydy/Qj\nS347YPd6RiokT+ZZQYbwyPvMtojmTj9fKV/nX3JwSBoZK/bhMaWstuspgQKBgCwy\nbSMgvYGFgnmJZtfwtHz/Y3ys2/H+vLLE6x4H0vYESqkz8fX0eRMl8LXKF3DfCvpX\nzY8JBODpLijLdi3cRofcWJ3qYwsme3v4WVOKtUk2mTvRxDaY26KzUCCUsESxNvbw\nhaWR6yJ9S6nj1Ej3V0cJdOunf4DWm9nOsscs4uSxAoGAWvQcmmjStcBFZn/sjBm8\ngsqHCVlfAUsScRSutDJD1lI687p6tvR3sHds35cASxLPA0rors0t8rAYDOSGh4ds\nCiXM2gUSr47A5THhz1feGaklQZVS4JndQt0W/Xm0e8v5D+UPyKNQOTJQElx8X9G6\nI+Epi5y1j9ZICMwkT6Y4hs0=\n-----END PRIVATE KEY-----\n');

# define value
define('NEWSLETTER_ON', 1);
define('NEWSLETTER_OFF', 0);

#define platform
define('PLATFORM_WEB', 2);
define('PLATFORM_ANDROID', 1);
define('PLATFORM_IOS', 0);
# define your IP
define('YOUR_IP', isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1');

// define debug
define('DEBUG_MODE', TRUE);
define('EMAIL_ADMIN', 'si.nguyen@vietis.com.vn');

ini_set('default_charset', 'UTF-8');
ini_set('display_errors', 1);
//ini_set("log_errors", "On");
// Define update, milestone
define('UPDATE_TYPE', 1);
define('MILESTONE_TYPE', 2);

if ($_SERVER['SERVER_PORT'] == 80) {
    $protocol = 'http';
    $port = '';
} else {
    $protocol = 'https';
    $port = ':' . $_SERVER['SERVER_PORT'];
}
define('HTTP_PROTOCOL', $protocol);
define('HTTP_PORT', $port);
define('CALLBACK_PROTOCOL', 'https'); //callback function must be predifined
define('SERVER_NAME', $_SERVER['SERVER_NAME']);
if ($_SERVER['SERVER_PORT'] == 80) {
    define('ROOT_URL', HTTP_PROTOCOL . "://" . SERVER_NAME . "/");
} else {
    define('ROOT_URL', HTTP_PROTOCOL . "://" . SERVER_NAME . ':' . $_SERVER['SERVER_PORT'] . "/");
}
//define('ROOT_URL', HTTP_PROTOCOL . "://".SERVER_NAME."/");
define('ROOT_URL_CALLBACK', CALLBACK_PROTOCOL . "://" . SERVER_NAME . "/");
/* Thanks mail */
define('DEFAULT_LANG', 'ja');
define('RETURN_URL', HTTP_PROTOCOL . "://" . SERVER_NAME . $_SERVER["REQUEST_URI"]);

#define FB
define('FB_APP_VERSION', 'v2.7');
//define('FB_APP_ID', '1238876156152048');
define('FB_APP_ID', '1115369258511279');
//define('FB_APP_SECRET', '0cdce1ae9414161dfb9b65a088da59c4');
define('FB_APP_SECRET', '16e35b358422c8f8c57b0d38423f669a');
define('FB_APP_PERMISSON', 'email,user_birthday,user_friends');

#define LinkedIn
//define('LINKEDIN_APP_ID', '81ywf0re8cx25g');
//define('LINKEDIN_APP_SECRET', 'KbLHtxx17DgqfqVl');

define('LINKEDIN_APP_ID', '81v0ailg9dhctb');
define('LINKEDIN_APP_SECRET', 'LWxWvELLV9IpqAxn');

$locaList = array('127.0.0.1', "::1");
if (in_array($_SERVER['REMOTE_ADDR'], $locaList)) {
    define('DB_HOST', 'localhost');
    define('DB_LOGIN', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'cpv_wtp');
    define('EMAIL_LOGIN', 'wtpfirebase@gmail.com');
    define('EMAIL_PASS', 'wtp123456');

    define('GOOGLE_APP_NAME', 'WTP Project');
    define('GOOGLE_OAUTH_CLIENT_ID', '671664296365-2inhlnak3cn9fojmp91hmj74pp15iaqu.apps.googleusercontent.com');
    define('GOOGLE_OAUTH_CLIENT_SECRET', '43uhs-SCwozkYeo2zYH576L5');
} elseif ($_SERVER['SERVER_PORT'] == '488') {
    define('DB_HOST', 'localhost');
    define('DB_LOGIN', 'root');
    define('DB_PASS', 'vietis@123');
    define('DB_NAME', 'wtp_dev');
    define('EMAIL_LOGIN', 'wtpfirebase@gmail.com');
    define('EMAIL_PASS', 'wtp123456');

    define('GOOGLE_APP_NAME', 'WTP Project');
    define('GOOGLE_OAUTH_CLIENT_ID', '671664296365-2inhlnak3cn9fojmp91hmj74pp15iaqu.apps.googleusercontent.com');
    define('GOOGLE_OAUTH_CLIENT_SECRET', '43uhs-SCwozkYeo2zYH576L5');
} elseif ($_SERVER['SERVER_PORT'] == '8443') {
    define('DB_HOST', 'localhost');
    define('DB_LOGIN', 'root');
    define('DB_PASS', 'vietis@123');
    define('DB_NAME', 'wtp_test');
    define('EMAIL_LOGIN', 'wtpfirebase@gmail.com');
    define('EMAIL_PASS', 'wtp123456');

    define('GOOGLE_APP_NAME', 'WTP Project Test');
    define('GOOGLE_OAUTH_CLIENT_ID', '737042260650-7239495gnv2vtgkrldq2nfistgv3hgfi.apps.googleusercontent.com');
    define('GOOGLE_OAUTH_CLIENT_SECRET', 'sZKmiZ0YnnnKLoplPDwiB0nA');
} elseif ($_SERVER['SERVER_PORT'] == '8008') {
    define('DB_HOST', 'localhost');
    define('DB_LOGIN', 'root');
    define('DB_PASS', 'vietis@123');
    define('DB_NAME', 'wtp_real');
    define('EMAIL_LOGIN', 'wtpfirebase@gmail.com');
    define('EMAIL_PASS', 'wtp123456');

    define('GOOGLE_APP_NAME', 'WTP Project Real');
    define('GOOGLE_OAUTH_CLIENT_ID', '544862986363-rirdrdl6ih75vg8b33blfg546v4rp5e7.apps.googleusercontent.com');
    define('GOOGLE_OAUTH_CLIENT_SECRET', 'Gt3nyVC3M3U9nyh5-bHN6BTe');
} else {
    define('DB_HOST', 'localhost');
    define('DB_LOGIN', 'root');
    define('DB_PASS', 'z9GCG45XI2Y3gL4x');
    define('DB_NAME', 'wtp_real');
    define('EMAIL_LOGIN', 'wtpfirebase@gmail.com');
    define('EMAIL_PASS', 'wtp123456');
}

//Google App Details
define('GOOGLE_OAUTH_REDIRECT_URI', ROOT_URL . 'token/google');
define("GOOGLE_SITE_NAME", ROOT_URL);

#define file upload
define('MAX_SIZE_UPLOAD_FILE', '52428800');

Configure::write('IMAGE_EXT_ARR', [
    'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'bmp', 'BMP'
]);
Configure::write('TYPE_IMAGE_VALIDATE', [
    'image/jpeg', 'image/png', 'image/gif', 'image/bmp'
]);

define('PROJECT_STATUS_HIDE', 0);
define('PROJECT_STATUS_SHOW', 1);
define('PROJECT_STATUS_SHOW_FEATURED', 2);
define('TOKEN_GOOGLE_MAP', 'AIzaSyCVNvGYwFKsfbshoDuBNn2tBYSkBPZdCc4');
define('TOKEN_DROPBOX', 'snlBL-hx4lAAAAAAAAAAG3i9fPgcvI-P-b63L9iLRrqaR6wIA2kHxsHgT87KS_6v');

define('SERVER_CERTIFICATES_PRODUCT', 'cert/wtpdev.pem');
define('SERVER_CERTIFICATES_SANBOX', 'cert/wtpdev.pem');
define('CERTIFICATE_PASSPHRASE', 'wtpdev');
