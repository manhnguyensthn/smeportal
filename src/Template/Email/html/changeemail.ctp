<p><strong style="color: #eea236">Hi <?php echo isset($first_name) ? $first_name : ''; ?> <?php echo isset($last_name) ? $last_name : ''; ?>,</strong></p>
<p><?php echo __('Click the link below to confirm change of your email address on SME:'); ?></p>
<p><a target="_blank" href="<?php echo ROOT_URL; ?>/users/confirmChangeEmail?token=<?php echo base64_encode($email); ?>"><?php echo __('Confirm my email change'); ?></a></p>
<p><?php echo __('If you did NOT request this change please forward this message to info@wetheproject.com. You may also ignore this message.'); ?></p>
<p><?php echo __('Cheers,'); ?></p>
<p><strong><?php echo __('The SME Team'); ?></strong></p>