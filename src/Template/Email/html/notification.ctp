<p><strong style="color: #eea236">
<?php
	$name = (isset($first_name) ? $first_name : '') . ' ' . (isset($last_name) ? $last_name : '');
	echo sprintf(__('Dear %s,'), $name);
?>
</strong></p>
<p><?php echo isset($notification_content) ? $notification_content : ''; ?></p>
<p><?php echo __('Best,'); ?></p>
<p><strong><?php echo __('The SME team.'); ?></strong></p>