<p><?= 'Dear '.$user->first_name.' '.$user->last_name; ?></p>
<?= $content; ?>

<p><?php echo __('Go a head and find more:'); ?></p>
<p><a href="<?= ROOT_URL.'discover/index'; ?>"><?= ROOT_URL.'discover/index'; ?></a></p>

<p><?php echo __('Best,') ?></p>
<p><?php echo __('The SME team') ?></p>