<p><?php echo __('Dear,') ?></p>
<p><?php echo __('We have been requested to reset your password.'); ?></p>
<p><?php echo __('Please access to following URL to activate the reset-password process. This forwards you to reset-password page.'); ?></p>
<p>URL: <a href="<?php echo $link; ?>"><?php echo $link; ?></a></p>
<p><?php echo __('After resetting password successfully, system forwards you to login page.'); ?></p>
<br />
<p><?php echo __('Regards,') ?></p>
<p><?php echo __('SME Team') ?></p>