<p><?php echo __('Welcome to We The Project!') ?></p>
<p><?php echo __('We have created your new account as requested.'); ?></p>
<p><?php echo __('Go ahead and discover some new projects now at:'); ?></p>
<p>URL: <?php echo ROOT_URL; ?></p>
<br />
<p><?php echo __('Regards,') ?></p>
<p><?php echo __('SME Team') ?></p>