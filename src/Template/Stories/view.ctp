<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Story'), ['action' => 'edit', $story->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Story'), ['action' => 'delete', $story->id], ['confirm' => __('Are you sure you want to delete # {0}?', $story->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Stories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Story'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="stories view large-9 medium-8 columns content">
    <h3><?= h($story->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Project') ?></th>
            <td><?= $story->has('project') ? $this->Html->link($story->project->title, ['controller' => 'Projects', 'action' => 'view', $story->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Video') ?></th>
            <td><?= h($story->video) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Video Image Overlay') ?></th>
            <td><?= h($story->video_image_overlay) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Overview') ?></th>
            <td><?= h($story->overview) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Language') ?></th>
            <td><?= $story->has('language') ? $this->Html->link($story->language->name, ['controller' => 'Languages', 'action' => 'view', $story->language->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($story->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pharse') ?></th>
            <td><?= $this->Number->format($story->pharse) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($story->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Pitch') ?></h4>
        <?= $this->Text->autoParagraph(h($story->pitch)); ?>
    </div>
</div>
