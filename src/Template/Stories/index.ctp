<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Story'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="stories index large-9 medium-8 columns content">
    <h3><?= __('Stories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('project_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('video') ?></th>
                <th scope="col"><?= $this->Paginator->sort('video_image_overlay') ?></th>
                <th scope="col"><?= $this->Paginator->sort('overview') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pharse') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lang_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($stories as $story): ?>
            <tr>
                <td><?= $this->Number->format($story->id) ?></td>
                <td><?= $story->has('project') ? $this->Html->link($story->project->title, ['controller' => 'Projects', 'action' => 'view', $story->project->id]) : '' ?></td>
                <td><?= h($story->video) ?></td>
                <td><?= h($story->video_image_overlay) ?></td>
                <td><?= h($story->overview) ?></td>
                <td><?= $this->Number->format($story->pharse) ?></td>
                <td><?= $story->has('language') ? $this->Html->link($story->language->name, ['controller' => 'Languages', 'action' => 'view', $story->language->id]) : '' ?></td>
                <td><?= h($story->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $story->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $story->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $story->id], ['confirm' => __('Are you sure you want to delete # {0}?', $story->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
