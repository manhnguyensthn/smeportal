<?php if ($current_lang == 'en_US'): ?>
<div class="container" style="min-height: 500px">
        <div class="page-header"><h1>Support and Contact</h1></div>
        <div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
            <p>If you didn’t find an answer to your question on our FAQ page first and need additional support, then please drop us a line at <a href="mailto:support@wetheproject.com">support@wetheproject.com</a></p>
            <p>For the press, sales or other non-technical support, please contact us at <a href="mailto:info@wetheproject.com">info@wetheproject.com</a></p>
        </div>
    </div>
<?php elseif ($current_lang == 'es_US'): ?>
    <div class="container" style="min-height: 500px">
        <div class="page-header"><h1>Support and Contact</h1></div>
        <div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
            <p>Si tiene más preguntas, consulte nuestra página de preguntas más frecuentes(FAQ) y si necesita adicional informacion, por favor envienos una email a : <a href="mailto:support@wetheproject.com">support@wetheproject.com</a></p>
            <p>Para la prensa , ventas, u otros servicio  de informacion , por favor contactenos  a la siguiente direccion <a href="mailto:info@wetheproject.com">info@wetheproject.com</a></p>
        </div>
    </div>
<?php else: ?>
    <div class="container" style="min-height: 500px">
        <div class="page-header"><h1>お問い合わせ先</h1></div>
        <div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
            <p>質問への解決策が「よくある質問一覧」で見つからなかった際は、メールでお問い合わせ下さい <a href="mailto:support@wetheproject.com">support@wetheproject.com</a></p>
            <p>お使いのパソコンや電子機器、および操作に関する質問は受け付けておりません</p>
            <p>ご意見、感想またはセースルは、こちらにメールを送信して下さい <a href="mailto:info@wetheproject.com">info@wetheproject.com</a></p>
        </div>
    </div>
<?php endif; ?>
