<?php if ($current_lang == 'en_US'): ?>
    <div class="container">
        <div class="page-header"><h1>What is We The Project ?</h1></div>
        <div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
            <h3>What is We the Project?</h3>
            <p>In the most simplest of terms, We the Project (SME) is a way for like-minded people to connect and collaborate in order to develop a project.  SME is mainly focused on those developing content.</p>

            <h3>Content is King.</h3>
            <p>What is content?  Anything that is used to communicate ideas, to tell a story, to develop marketing, to create a brand, or to entertain.  We the Project is a new model on project development, in order to develop content that’s needed for all media.   Look at all the amazing content that is available on the various media forms, it’s not just TV or going out to the movies anymore.  Content is on all social media, video sharing platforms, retailer website, news channels, etc. – it’s everywhere, and people are hungry for good content.</p>

            <p>What’s different about SME, compared to other networking sites or collaboration sites?</p>

            <h3>PEOPLE.</h3>
            <p>We the Project was developed by seasoned content producers from many fields.</p>
            <p>They have shared their varied experiences and insight to develop the workflow of the site.  The have also helped to grow amazing networks of people -  which is growing larger every day as more people join.  The people who are on this site are experienced professionals as well as absolute beginners, and all those in between.  It’s a way to give beginners opportunities, and for the more experienced to gain a new perspective.  There’s something for everyone, no matter where you are in your professional experience.</p>

            <h3>STRUCTURE.</h3>
            <p>We’ve developed this site to be user friendly – it’s easy to find projects and  collaborators,  it’s great for communication, and you can efficiently manage your project collaborations within the site/app. There are many tools to help you find and to develop opportunities, whether you’re a creator or a collaborator. We’ve also introduced a new model of compensation – a way that everyone that works on a project can share in the profit, if a project is creating income. It’s a much more fair way to reap the rewards for your hard work.</p>

            <h3>ATTITUDE.</h3>
            <p>We believe that everyone has an important story to tell, and we want to create opportunity to help people develop content, no matter what their experience level is. You don’t need to have an “in” at a major movie studio to make a movie. With The technology that is available today, it’s all very DIY, and you don’t need a million dollars. It just takes one good idea. Plus, lots of hard work, of course. We believe in the power of collaboration is incredible, and can create incredible things. Here’s the place to start, right here, on We the Project.</p>
            <p>If you have more questions, check out our FAQs page.</p>
        </div>
    </div>
<?php elseif ($current_lang == 'es_US'): ?>
    <div class="container">
        <div class="page-header"><h1>¿Qué es We The Project? </h1></div>
        <div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
            <h3>¿Qué es We The Project? </h3>
            <p>En el más simple de los términos, We The Project (SME) es una forma de personas con ideas afines para conectar y colaborar con el fin de desarrollar un proyecto. SME se centra principalmente en aquellos que desarrollan contenido.  </p>

            <h3>El contenido es Rey. </h3>
            <p>¿Qué es el contenido? Todo lo que se utiliza para comunicar ideas, contar una historia, desarrollar marketing, crear una marca o entretener. We The Project es un nuevo modelo de desarrollo de proyectos, para desarrollar el contenido que se necesita para todos los medios. Explore , todo el contenido increíble que está disponible en los diversos formularios de medios de comunicación, no es sólo la televisión o cuando sale en el cine  El contenido está en todas las redes sociales, plataformas de distribución de video, sitio web de minoristas, canales de noticias, etc.  Está en todas partes, y la gente está hambrienta de buen contenido.  ¿Qué diferencia hay respecto a SME, en comparación con otros sitios de redes o sitios de colaboración? </p>

            <h3>GENTE.</h3>
            <p>SME fue desarrollado por productores de contenido experimentado de muchos campos. 
Han compartido sus variadas experiencias y conocimientos para desarrollar el flujo de trabajo del sitio. También han ayudado a crecer redes asombrosas con  gente , y cada vez es mayor, como diariamente mas gente se conecta por este medio.</p>
            <p>Las personas que están en este sitio son profesionales experimentados, así como principiantes absolutos, y todos aquellos en el medio. Es una manera de dar a los principiantes oportunidades, y para los más experimentados para obtener una nueva perspectiva. Hay algo para todos, sin importar dónde se encuentre en su experiencia profesional. </p>

            <h3>ESTRUCTURA.</h3>
            <p>Hemos desarrollado este sitio para ser fácil de usar .Es fácil de  encontrar proyectos y colaboradores, es ideal para la comunicación, y puede administrar eficientemente su colaboraciones de proyectos dentro del sitio / aplicación (app) . Hay muchas herramientas para ayudarle a encontrar  y para que pueda usted  desarrollar oportunidades,  ya sea usted un creador o un colaborador.</p>
            <p>También hemos introducido un nuevo modelo de compensacion  
 Es una forma en que todos los que trabajan en un proyecto sean creadores o colaboradores  del projecto puedan participar en la ganacia economica , si es que  el proyecto genera ganacias . Es una manera muy justa de cosechar las recompesas por su trabajo duro . </p>

            <h3>ACTITUD.</h3>
            <p>Creemos que cada uno tiene una historia importante que contar, y queremos crear oportunidad de ayudar a las personas a desarrollar contenido, sin importar su nivel de experiencia.</p>
            <p>No es necesario tener  'una entrada’ a un estudio de cine para hacer una película. Con la tecnología que está disponible hoy en día, es todo mas   facil  (DIY), y no necesita un millón de dolares , solo se necesita una buena idea. Además, un montón de trabajo duro, por supuesto.  
SME,creemos que el poder de la colaboración es increíble, y puede crear cosas increíbles.   Aquí en We The Project  es el lugar para empezar inmediatamente  su proyecto . </p>
            <p>Si tiene más preguntas, consulte nuestra página de preguntas más frecuentes(FAQ)</p>
        </div>
    </div>
<?php else: ?>
    <div class="container">
        <div class="page-header"><h1>We the Projectについて</h1></div>
        <div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
            <h3>We the Projectについて</h3>
            <p>私たち（ＷＴＰ）が一番大切にしていることは、“最も簡単な方法で、同じ考えや同じ目的を持った人たちを結びつける”ということです。</p>

            <h3>重要なのは内容</h3>
            <p>一体どんな内容なのか。</p>
            <p>全て楽しむために自分の考えを伝える、物語を語る、マーケティングの拡大、ブランドを作る。</p>

            <p>これは今までになかった新しいプロジェクトです。私たちは、全てのメディアから必要とされ、そして満足してもらうためにプロジェクトを発展させています。
そして今、この驚くべきプロジェクト内容を見て下さい。それはテレビを見ることでも、映画を見ることでもありません。</p>
            <p>動画のシェア、有料サイト、ニュース番組…これらは、ネット上に溢れています。
でも人々は、今までになく、そしてより良いものを心から欲しがっています。</p>
            <p>ＷＴＰは他のソーシャルサイトと、どう違うの？</p>

            <h3>人びと</h3>
            <p>このプロジェクトは経験豊かな、異なる分野のスタッフによって開発されました。プロジェクトを開発するために、彼らは自分たちの持っている全ての知識、そして豊かな経験を共有しました。</p>
            <p>そして多くの人々が交流し、さらにそのネットワークを拡大させるお手伝いをします。
私たちは、初心者の方でも安心してお使いいただけるように、サポートします。そして、初心者の方にも様々なチャンスと、様々な体験をしていただけるプロジェクトです。
このプロジェクトを使えば、あなたは、いつどこでも素晴らしい体験ができます。</p>

            <h3>構造</h3>
            <p>私たちは、とても身近で親しみやすい、このサイトを開発しました。</p>
            <p>このサイト/アプリで、プロジェクトと共同製作者を簡単に見つける事ができ、またそこから素晴らしい関係を築きます。また、効率よく　あなたのプロジェクトをマネンジメントできます。</p>
            <p>ここには、あなたが製作者もしくは共同製作者のどちらの立場でも、より素晴らしい繋がりを見つける為のたくさんの手段があります。</p>
            <p>私たちは、新型の補償プロジェクトもプロディースしました。</p>
            <p>それは、その新型補償プロジェクトが利益を生み出した際、プロジェクト参加者が誰でも利益を得られる。</p>
            <p>また、新型補償プロジェククトでは、あなたが利益の分け前を決める事ができます。</p>
            <p>それは、激務をして得るよりもさらに効率的な手段です。</p>

            <h3>心構え</h3>
            <p>私たちは、誰もが皆「大切な語るべき物語」を持っていると信じています。
それは今までの経験に関係なく、ゼロからでも、利用者全員が満足できるものを提供したいと考えています。</p>
            <p>あなたは、映画を製作するために映画スタジオを持つ必要はありません。今は「Ｄ.Ｉ.Ｙ」は珍しくありません。なぜならば、大金を払う必要がないからです。</p>
            <p>＊Ｄ.Ｉ.Ｙとは専門業者ではない人が自分で何かを作ったり、修繕したりすること。 英語のDo It Yourself（ドゥ イット ユアセルフ）の略語。「自身でやる」という意味。</p>
            <p>Ｄ.Ｉ.Ｙはとても良い手段です。確かに作業は想像以上に大変かもしれません。</p>
            <p>しかし私たちは、共同制作をする事で、あなたを困難から救い、そしてさらに驚くべき素晴らしい物を作ると確認しています。</p>
            <p>では、今からあなたのプロジェクトを開始させましょう！！</p>
            <p>（ここをクリックして進んでください）</p>
            <p>何かご不明な点、ご質問がある方はＱandAを参照してください。</p>
        </div>
    </div>
<?php endif; ?>
