<div class="container">
	<div class="page-header"><h1>Privacy Policy</h1></div>
	<div class="template-content-page" style="margin: 0px auto; line-height: 23px; text-align: justify;">
		<h3>What We Collect</h3>
		<p>In order for you to create an account on WeTheProject and use our services, we need to collect and process some information. Depending on your use of the Services, that may include:</p>
		<ul>
			<li>information (such as your name, email and postal addresses, telephone number, and country of residence) that you provide by completing forms on WeTheProject, including if you register as a user of the Services, subscribe to our newsletters, upload or submit any material through WeTheProject, or request any information;</li>
			<li>your login and password details, in connection with the account sign-in process;</li>
			<li>details of any requests or transactions you make through the Services;</li>
			<li>information about your activity on and interaction with WeTheProject, such as your IP address and the type of device or browser you use;</li>
			<li>communications you send to us (for example, when you ask for support, send us questions or comments, or report a problem); and</li>
			<li>information that you submit to WeTheProject in the form of comments, contributions to discussions, or messages to other users.</li>
		</ul>


		<h3>What’s Private</h3>
		<p>This data will not be publicly displayed or revealed to other users:</p>
		<ul>
			<li>any payment information you provide</li>
			<li>your password details</li>
			<li>your IP address</li>
			<li>your phone number</li>
			<li>communications you send to us (for example, when you ask for support, send us questions or comments, or report a problem)</li>
		</ul>
		<p>We don’t give your personal information to any third-party services, except when it’s necessary to provide WeTheProject’s Services (like when we partner with payment processors). When we share data with third-party services that support our delivery of the WeTheProject Services, we require that they protect your personal information to the same standards we do.</p>
		<p>We do reserve the right to disclose personal information when we believe that doing so is reasonably necessary to comply with the law or law enforcement, to prevent fraud or abuse, or to protect WeTheProject’s legal rights.</p>


		<h3>What’s Shared Only with Creators & Collaborators</h3>
		<p>When you join a project, the project’s creator will know your account name and have communication access on the WeTheProject site. Creators never receive collaborators’ personal contact information, banking details or other payment information.</p>
		<p>If a project you have joined is successfully staffed, the creator will receive the email address associated with your WeTheProject account. They may also send you a survey requesting information needed to fulfill your role. (For instance, they may need your mailing address, or T-shirt size.) Any information you provide in such surveys will be received by the creator.</p>
		<p>Creators have the option to appoint collaborators to help manage their project. These collaborators may be able to access some of the information available to creators (including collaborators’ names, email addresses, pledge amounts, reward selections, and survey responses), and are required to treat collaborators’ personal information with the same care and respect as creators are.</p>
		<p>Creators and their collaborators may also receive anonymized information about the ways people visit and interact with their project pages, in the form of routine traffic analytics. You can choose not to be included in Google Analytics here.</p>
		

		<h3>What’s Public</h3>
		<p>When you create an account, we create a basic profile page for you on WeTheProject, containing your account name, the date the account was created, and a list of projects you have joined, launched or follow. Whenever your account name appears on WeTheProject (for instance, when you post comments, send messages, or back projects), people can click your account name to see your profile. The profile is not searchable on WeTheProject or via search engines like Google. Here are some of the things that will be publicly viewable on your profile, or elsewhere on WeTheProject:</p>
		<ul>
			<li>the account name, and the date it was created</li>
			<li>any information you choose to add to your profile (like a picture, bio, or your location)</li>
			<li>projects you've joined (but not agreement details)</li>
			<li>projects you've launched</li>
			<li>any comments you’ve posted on WeTheProject</li>
			<li>if you have "Liked" a project update</li>
		</ul>
		<p>Please note that each project page lists the community of users who have backed the project.</p>
		<p>Creators are also asked to verify their identities before launching a project. Once this has been done, the creator’s Verified Name will be publicly displayed on their account profile and on any projects they launch.</p>
		

		<h3>Uses of Your Personal Information</h3>
		<p>We will use the personal information you provide to:</p>
		<ul>
			<li>identify you when you sign in to your account;</li>
			<li>enable us to provide you with the Services;</li>
			<li>send you marketing communications we think you may find useful, but only in accordance with your email preferences;</li>
			<li>present projects to you when you use the Services which we believe will be of interest based on your geographic location and previous use of the Services;</li>
			<li>administer your account with us;</li>
			<li>enable us to contact you regarding any question you make through the Services;</li>
			<li>analyze the use of the Services and the people visiting to improve our content and Services; and</li>
			<li>use for other purposes that we may disclose to you when we request your information.</li>
		</ul>
		<p>We take securing your data and preserving your privacy very seriously. We never post anything to your Facebook, Twitter, or other third-party accounts without your permission. WeTheProject Project Creators never receive collaborators’ credit card details or other payment information. We do not and will not sell your data.</p>


		<h3>Email</h3>
		<p>We want to communicate with you only if you want to hear from us. We try to keep emails to a minimum and give you the ability to opt out of any marketing communications we send.</p>
		<p>We will send you email relating to your personal transactions. You will also receive certain marketing email communications, in accordance with your preferences, and from which you may opt out at any time.</p>
		<p>We may send you service-related announcements on the rare occasions when it is necessary to do so.</p>


		<h3>Wrap-up</h3>
		<p>To modify or delete the personal information you have provided to us, please log in and update your profile. We may retain certain information as required by law or for necessary business purposes.</p>
		<p>On request, we will give you a copy of all the personal information about you that we hold. This information is subject to a fee not exceeding the prescribed fee permitted by law.</p>
		<p>People under 18 (or the legal age in your jurisdiction) are not permitted to use WeTheProject on their own, and so this privacy policy makes no provision for their use of WeTheProject.</p>
		<p>Information that you submit through the Services may be transferred to countries other than where you reside (for example, to our servers in the United States). We will protect all information we receive in accordance with this privacy policy.</p>

		<h3>Questions?</h3>
		<p>If you have questions or suggestions, please contact us.</p>

		<p><em>Updated: December 2016</em></p>
	</div>
</div>