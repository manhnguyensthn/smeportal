<div class="col-md-3 vItem" id='work-<?php echo $work_string; ?>'>
    <a class="remove-work" href="javascript:delete_user_work('<?php echo $work_string; ?>','remove');" title="<?php echo __('Delete this work'); ?>"><i class='fa fa-times'></i></a>
    <div class=" itemSolid">
        <?php echo $this->Form->input('Portfolio.video[]',['class' => 'form-control','type'=> 'hidden','value'=>$YoutubeEmbeded]); ?>
        <?php echo $this->Form->input('Portfolio.work_title[]',['class' => 'form-control','type'=> 'hidden','value'=>$work_title]); ?>
        <?php echo $this->Form->input('Portfolio.work_video_url[]',['class' => 'form-control','type'=> 'hidden','value'=>  isset($work_video_url) ? $work_video_url : '']); ?>
        <?php echo $this->Form->input('Portfolio.work_video_id[]',['class' => 'form-control','type'=> 'hidden','value'=>  isset($work_video_id) ? $work_video_id : '']); ?>
        <?php echo $this->Form->input('Portfolio.work_description[]',['class' => 'form-control','type'=> 'hidden','value'=>$work_description]); ?>
        <div class="embed">
            <?php echo isset($YoutubeEmbeded) ? $YoutubeEmbeded : ''; ?>
        </div>
        <span><?php echo isset($work_title) ? strip_tags($work_title) : ''; ?></span>
    </div>
</div>