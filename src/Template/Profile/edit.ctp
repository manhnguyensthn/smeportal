<?php
use Cake\Routing\Router;

echo $this->Html->css('croppie');
echo $this->Html->script('croppie');
 ?>

<div class="users_form_edit" id="users_form_edit" style=" border: none;margin-bottom: 50px">
    <div class="content-setting" style="width: 100%;">
        <h2><?php echo __('Settings'); ?></h2>
        <div class="clearfix"></div>

        <?php echo $this->element('Profile/profile_tab'); ?>
        
        <div class="clearfix"></div>
        <div class="col-custom col-lg-12">
            <?= $this->Form->create($user, ['type' => 'file', 'id' => 'Users_EditProfile']) ?>
            <div class="input text col-md-7">
                <?php echo $this->Form->input('Users.first_name', array('div' => false, 'class' => 'form-control', 'label' => __('First Name'))); ?>
            </div>
            <div class="input text col-md-7">
                <?php echo $this->Form->input('Users.last_name', array('div' => false, 'class' => 'form-control', 'label' => __('Last Name'))); ?>
            </div>
            <div class="form-group col-md-7 profile">
                <label for="profile-picture">
                    <?php echo __('Profile Picture'); ?><br>
                </label>
                <div style = "clear:both;"></div>
             <div class = "crop_images" id= "crop_images" style = "display:none;">
                <h3> <?php echo __('Crop image') ?> </h3>
                    <div id= "crop">
                    </div>
                    <a class="btn upload-result" onclick = "javascript:void(0)"> <?php echo __('Save image') ?></a>
                </div>
                <div id="profile-picture" class="btn btn-default btn-file" <?php 
                
            
                if(!empty($avatarURL))
                {
                list($width, $height) = getimagesize($avatarURL);
                if($width < $height)
                {
                    $height = 541*$height/$width.'px';
                    $width = '100%';
                }
                else if($width > $height)
                {
                    $width = 541*$width/$height.'px';
                    $height = '100%';
                }
                else
                {
                    $width = '100%';
                    $height = '100%';
                }
                }
                echo !empty($avatarURL) ? 'style="background-size: '.$width.' '.$height.'; background-image: url(' . $avatarURL . ');"' : ''; 
                ?>  >
                    <?php echo $this->Form->input('Users_CurrentImage', ['type' => 'hidden', 'value' => $avatarURL]); ?>
                    <div class="centered-text">
                        <span><?php echo __('Drag and drop') ?></span>
                        <div class="clearfix"></div>
                        <span><?php echo __('Or'); ?></span>
                        <div class="clearfix"></div>
                        <span><?php echo __('Chosse an image from your computer') ?></span>
                        <div class="clearfix"></div>
                        <br />
                        <p style="color: red;margin-top: -15px;">(<?php echo __('for iOS appearance, 2500x1500, 3MB image is maximum'); ?>)</p>
                        <span><small><?php echo __('JPEG,PNG, GIF, or BMP 50MB file limit<br>At least 1024x1024 pixels 16:9 aspect ratio') ?></small></span>
                        <div class="clearfix"></div>
                        <small id="fileNameUpload"></small>
                    </div>
                    <input type="file" name="Users[avatar]" class="custom_file" maxlength="100" id="users-avatar" value="">
                    <input type="hidden" name="Users[external_avatar_url]" id="users-external-avatar-url" value="">
                    <input type="hidden" name="Users[users_avatar_url]" id="users-avatar-url" value="">
                    <div class="col-md-12 extendbtn">
                        <div class="col-md-6"><button type="button" id="Users_ImageProfile_UploadFromComputer" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('Upload from computer'); ?></button></div>
                        <div class="col-md-6"><button type="button" id="Users_ImageProfile_ExternalURL" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('External URL'); ?></button></div>
                    </div>
                </div>
                <div class="error-message"><?php echo $this->Form->error('external_user_picture_url') ?></div>
            </div>



            <div class="clearfix"></div>
            <div class="group-location col-md-7">
                <?php
                echo $this->Form->input('Countries', [
                    'class' => 'form-control',
                    'label' => __('Location'),
                    'type' => 'select',
                    'options' => $countries,
                    'empty' => __('-- Select Country --'),
                    'default' => $user->country_id,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]);

                echo $this->Form->input('States', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'type' => 'select',
                    'options' => $states,
                    'empty' => __('-- Select Province/State --'),
                    'default' => ($user->state_id == null && $user->country_id > 0) ? -1 : ($user->country_id == 0 ? 0 : $user->state_id),
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-right {{type}}{{required}}"><label for="states" style="color:transparent">.</label>{{content}}</div>'
                    ]
                ]);

                echo $this->Form->input('Districts', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'type' => 'select',
                    'options' => $districts,
                    'empty' => __('-- Select City --'),
                    'default' => $user->district_id,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]);

                echo $this->Form->input('Users.postal_code', ['class' => 'form-control','type' =>'hidden', 'label' => FALSE,
                    'placeholder' => __('Postal Code'), 'readonly' => TRUE, 'value' => $districtPostalCode,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-right {{type}}{{required}}">{{content}}</div>'
                    ]]
                );
                ?>
                <div class="clearfix" style="margin-bottom: 30px;"></div>
                <?php
                echo $this->Form->input('Roles.role0', array(
                    'class' => 'form-control', 'label' => __('Choose your role'),
                    'type' => 'select', 'options' => $roles, 'empty' => '-- Chọn vai trò 1 --', 'default' => $role1->role_id
                ));
                echo $this->Form->input('Roles.role1', array(
                    'class' => 'form-control', 'label' => FALSE,
                    'type' => 'select', 'options' => $roles, 'empty' => '-- Chọn vai trò 2 --', 'default' => $role2->role_id
                ));
                ?>
            </div>
            <div class="clearfix" style="margin-bottom: 20px;"></div>
            <div class="input texarea col-md-7">
                <?php
                echo '<label for="biography">'.__('Biography').'</label>';
                echo $this->Form->textarea('Users.biography', array('maxlength' => 499, 'class' => 'form-control', 'rows' => 7,'onkeyup'=> 'countCharDescriptionprofile(this);', 'label' => __('Biography')));
                ?>
                <div id="Users_EditProfile_BioRemainCharCount"  ><?php echo $bioRemainCharCounter ?></div>
            </div>

            <div class="clearfix"></div>
            <div class="input text col-md-10" style="margin-bottom: 0px;">
                <label for="website"><?php echo __('Websites'); ?></label>
            </div>
            <div class="input text col-md-7" style="margin-bottom: 0px;">
                <input id="webinfo" type="text" class="form-control">
            </div>
            <div class="input text col-md-2" style="margin-bottom: 16px;">
                <button id="addweb" class="btn btn-warning btn-yellow-shadow" type="button" style="color: #080707; width:100%;"><?php echo __('add'); ?></button>
            </div>

            <div class="clearfix"></div>
            <div class="input text col-md-7">
                <div id="websiteInfo">
                    <?php $count = 1 ?>
                    <?php foreach ($websites as $website): ?>
                        <div class="alert alert-default" role="alert">
                            <?php echo $website->website->name ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <input type="hidden" name="UsersWebsite[no<?php echo $count ?>]" value="<?php echo $website->website_id ?>">
                        </div>
                        <?php ++$count ?>
                    <?php endforeach ?>
                </div>
                <input type="hidden" name="deletedWebsites" id="deletedWebsites" value="">
            </div>

            <div class="clearfix"></div>
            <div class="input text col-lg-12">
                <label for="user-portfolio"><?php echo __('Portfolio'); ?></label>
                <div class="clearfix"></div>
                <div class="row" id="list-works">
                    <?php foreach ($works as $work): ?>
                        <div class="col-md-3 vItem" id="work-<?php echo $work->id; ?>">
                            <a class="remove-work" href="javascript:delete_user_work('<?php echo $work->id; ?>','delete');" title="<?php echo __('Delete this work'); ?>"><i class='fa fa-times'></i></a>
                            <div class=" itemSolid">
                                <div class="embed">
                                    <?php if (!empty($work->work_video_id)): ?>
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $work->work_video_id; ?>" frameborder="0" ></iframe>
                                    <?php else: ?>
                                        <?php echo $work->work_url; ?>
                                    <?php endif; ?>
                                </div>
                                <p><?php echo strip_tags($work->work_title); ?></p>
                            </div>
                        </div>
                        <div style = "display:none;">
                        <?php if (!empty($work->work_video_id)): ?>
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/cX0R3mXaod8" frameborder="0" ></iframe>
                                    <?php else: ?>
                                        <?php echo $work->work_url; ?>
                                    <?php endif; ?>
                                    </div>
                    <?php endforeach ?>
                    <div class="col-md-3 vItem" id="box-add-portfolio" onclick="reset_box_add_work();" data-toggle="modal" data-target="#myModal">
                        <div class=" btn-add-video">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="clearfix"></div>
            <button type="submit" class="btn btn-warning pull-right" ><?php echo __('Save Changes'); ?></button>

            <!-- User Work Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><?= __('Upload New Work') ?></h4>
                        </div>

                        <div class="modal-body" id="frm-add-newwork-content">
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <div class="row" id="iframe-video"></div>
                                    <input type="hidden" name="UsersWork[external_video_url]" id="userswork-external-video-url" value="">
                                    <input type="hidden" name="UsersWork[work_video_id]" id="work-video-id" value="">
                                </div>
                                <div class="row" style="position: absolute;width: 100%;top: 100px;">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <button id="UsersWork_ExternalURL" type="button" style="width:100%" class="btn btn-warning btn-yellow-shadow"><?= __('External URL') ?></button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userswork-title"><?php echo __('Title'); ?></label>
                                <input type="text" name="UsersWork[title]" maxlength="25" class="form-control" id="userswork-title">
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <label for="description"><?= __('Description') ?></label>
                                <?php echo $this->Form->textarea('UsersWork.description', ['class' => 'form-control', 'label' => 'Description', 'id' => 'userswork-description']) ?>
                            </div>
                        </div>
                        <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                            <button id="Users_NewWork_Post" type="button" class="btn btn-warning btn-yellow-shadow Users_NewWork_Post Users" style="width:20%"><?= __('Post') ?></button>
                        </div>
                        <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;display:none">
                            <button  type="button" class="btn btn-warning btn-yellow-shadow Users_NewWork" style="width:160px"><?= __('Post') ?></button>
                        </div>

                    </div>
                </div>
            </div>

            <!-- External Avatar URL -->
            <div class="modal fade" id="externalURLModal" tabindex="-1" role="dialog" aria-labelledby="externalURLModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="externalURLModalLabel"><?= __('External URL') ?></h4>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="userswork-title"><?php echo __('URL'); ?></label>
                                <input type="text" maxlength="255" class="form-control" id="users-external-url">
                                <div class="error-message" id="msgImg"></div>
                            </div>
                        </div>

                        <input type="hidden" id="Users_ExternalURL_Type" value="">
                        <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                            <button id="Users_ExternalURL_Post" type="button" class="btn btn-warning btn-yellow-shadow" style="width:160px"><?= __('Post') ?></button>
                        </div>

                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<!-- External Avatar URL -->
<div class="modal fade" id="externalVideoURLModal" tabindex="-1" role="dialog" aria-labelledby="externalVideoURLModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="externalURLModalLabel"><?= __('External URL') ?></h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="users-external-video-url"><?php echo __('External URL'); ?></label>
                    <input type="text" maxlength="255" class="form-control" id="users-external-video-url">
                    <div class="error-message" id="msgVideo"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="color: #a5a5a5;"><?php echo __('Example: https://www.youtube.com/watch?v=cX0R3mXaod8'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                <div class="row" style="margin-top: -25px;">
                    <div class="col-md-8 text-left"></div>
                    <div class="col-md-4">
                        <button id="Users_ExternalVideoURL_Post" type="button" class="btn btn-warning btn-launch" ><?= __('Post') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class = "popup-w"></div>
<?php echo $this->Html->script('/js/jquery-loading-overlay/src/loadingoverlay.min'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#countries').on('change', function () {
            var country_id = $('#countries').val();
            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getStates"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#districts');
                    var states = $('#states');
                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var newStates = {"<?= __('-- Select Province/State --') ?>": ""};

                    var stateList = data['data'];

                    if (stateList.length > 0) {
                        for (var idx = 0; idx < stateList.length; ++idx) {
                            newStates[stateList[idx]['States__name']] = stateList[idx]['States__id'];
                        }
                    }

                    states.empty();
                    $.each(newStates, function (key, value) {
                        states.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                    alertBox('<?= __('Unable to get state list. Please, try again.') ?>');
                }
            });
        });

        $('#states').on('change', function () {
            var country_id = $('#countries').val();
            var state_id = $(this).val();

            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getDistricts"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id,
                    state_id: state_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#districts');
                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var districtList = data['data'];

                    if (districtList.length > 0) {
                        for (var idx = 0; idx < districtList.length; ++idx) {
                            newDistricts[districtList[idx]['Districts__name']] = districtList[idx]['Districts__id'];
                        }
                    }

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                    alertBox('<?= __('Unable to get city list. Please, try again.') ?>');
                }
            });
        });

        $('#districts').on('change', function () {
            var district_id = $(this).val();

            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getPostalCode"]) ?>',
                type: 'POST',
                data: {
                    district_id: district_id
                },
                dataType: 'json',
                success: function (data) {
                    var postalCode = $('#users-postal-code');

                    if (data.status) {
                        postalCode.val(data.postal_code);
                    } else {
                        postalCode.val("");
                    }
                },
                error: function (e) {
                    alertBox('<?= __('Unable to get postal code. Please, try again.') ?>');
                }
            });
        });

        $('#Users_EditProfile').on('click', 'div#websiteInfo > div.alert.alert-default > button.close', function () {
            var currentDeletedWebsites = $('#deletedWebsites').val();
            var currentDeletedWebsitesArray = [];
            if (currentDeletedWebsites != '') {
                currentDeletedWebsitesArray = currentDeletedWebsites.split(',');
            }

            var deletedWebsite = $(this).parent().find('input[type="hidden"]');
            var deletedValue = $.trim(deletedWebsite.prop('value'));

            // Dont pay attention to newly added websites
            if (deletedValue != '' && deletedValue == deletedValue.replace(/\D/g, '')) {
                currentDeletedWebsitesArray.push(parseInt(deletedValue));
            }

            if (currentDeletedWebsitesArray.length > 0) {
                $('#deletedWebsites').val(currentDeletedWebsitesArray.join(','));
            } else {
                $('#deletedWebsites').val('');
            }
        })
        $('#Users_ImageProfile_UploadFromComputer').click(function (event) {
            event.preventDefault();
            $('#image[type=file]').trigger('click');
        });
        $("#profile-picture input[type=file]").change(function () {
            var fileName = this.value.split(/(\\|\/)/g).pop();
            if (fileName.length > 18) {
                fileName = fileName.substr(0, 8) + "..." + fileName.substr(fileName.length - 9, fileName.length - 1);
            }
            readURL(this);
            $("#fileNameUpload").text(fileName);
            $("input#image_profile_url").attr('value', '');
        });
        function getFileExtension(filename) {
            return filename.split('.').pop();
        }
        function readURL(input) {
            var arr = ['jpg', 'png', 'gif', 'bmp', 'PNG'];
            var ext = getFileExtension(input.files[0].name);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onloadend = function (e) { 
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#Users_ImageProfile').on('error', function () {
            if ($('#Users_ImageProfile').attr('src') != $("#users-currentimage").val()) {
                $('#Users_ImageProfile').attr('src', $("#users-currentimage").val());
            }
        });

        $("#users-avatar").change(function () {
            $("#users-external-avatar-url").val("");
  
        });

        $("#users-video").change(function () {
            var arr = ['mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv'];
            var ext = getFileExtension($(this).val());
            if ($.inArray(ext, arr) == -1) {
                alertBox('<?= __("Media video should be mov, mpeg, avi, mp4, 3gp, wmv, flv; size of 1-100MB.") ?> ');
                $(this).val("");
                return;
            }
            $('#userswork-external-video-url').val("");
        });

        $("#Users_ImageProfile_Cancel").on("click", function () {
            $("#users-avatar").val("");
            $('#users-external-avatar-url').val("");
            $('#Users_ImageProfile').attr('src', $("#users-currentimage").val());
        });

        $("#Users_EditProfile > div.input.texarea.col-md-9 > textarea").on("cut paste keyup", function () {
            var remainCharsCount = 500 - $(this).val().length;
            if (remainCharsCount < 0) {
                remainCharsCount = 0;
            }
            $("#Users_EditProfile_BioRemainCharCount").text(remainCharsCount);
        });

        $("#Users_NewWork_Post").on('click', function () {  
            
            var userWorkVideoUrl = $('#userswork-external-video-url').val();
            var userWorkVideoID = $('#work-video-id').val();
            var userWorkTitle = $('#myModal #userswork-title').val();
            var YoutubeEmbeded = $('#iframe-video').html();
            var workDescription = $('#myModal #userswork-description').val();
            if (YoutubeEmbeded != '') {
                if (userWorkTitle.length >= 1 && userWorkTitle.length <= 25) {
                    if (isHTML(userWorkTitle)) {
                
                       alertBoxprofile('<?php echo __('Title is required. Max length is 25 characters. No support HTML tags.'); ?>'); 
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/profile/getWorkPortfolioAjax',
                        data: {
                            'YoutubeEmbeded': YoutubeEmbeded,
                            'work_title': userWorkTitle,
                            'work_description': workDescription,
                            'userWorkVideoUrl': userWorkVideoUrl,
                            'userWorkVideoID': userWorkVideoID
                        },
                        success: function (respone) {
                            $('#box-add-portfolio').before(respone);
                        }
                    });
                    $('#myModal').modal('toggle');
                } else {
                
                    alertBoxprofile('<?php echo __('Title is required. Max length is 25 characters.'); ?>');
                }
            } else {
                alertBoxprofile('<?php echo __('External URL required!'); ?>');
                
            }
        });

        $('#Users_ImageProfile_Cancel').css({display: "none"});
        $('.avatarArea img').css({cursor: "pointer"}).mouseenter(function () {
            $(this).css({opacity: "0.5"});
            $('#Users_ImageProfile_Cancel').stop().fadeIn(200);
        }).mouseleave(function () {
            $(this).css({opacity: "1"});
            $('#Users_ImageProfile_Cancel').stop().fadeOut(200);
        });

        $("#Users_ImageProfile_UploadFromComputer").on('click', function () {
            $("#users-avatar").click();
        });

        $("#UsersWork_UploadFromComputer").on('click', function () {
            $("#users-video").click();
        });

        var USER_PROFILE_EXTERNAL_AVARTAR_URL = 0;
        var USER_PROFILE_EXTERNAL_VIDEO_URL = 1;

        $("#UsersWork_ExternalURL").on('click', function () {
            $("#msgImg").text('');
            $('#users-external-video-url').val("");
            $("#Users_ExternalURL_Type").val(USER_PROFILE_EXTERNAL_VIDEO_URL);
            $("#myModal").modal('hide');
            $("#externalVideoURLModal").modal('show');
        });

        $('#Users_ExternalVideoURL_Post').click(function () {
            var video_url = $('#users-external-video-url').val();
            if (!isYoutubeURL(video_url)) {
                $("#msgVideo").text('<?php echo __('Invalid URL'); ?>');
                return false;
            } else {
                var videoID = youtube_parser(video_url);
                $.ajax({
                    type: 'POST',
                    url: '/projects/checkVideoYoutubeExit',
                    data: {
                        'videoID': videoID
                    },
                    success: function (respone) {
                        if (respone == 'TRUE') {
                            $('#userswork-external-video-url').val(video_url);
                            $('#work-video-id').val(videoID);
                            $('#iframe-video').html('<iframe width="560" height="320" src="https://www.youtube.com/embed/' + videoID + '" frameborder="0" allowfullscreen></iframe>');
                            $('#externalVideoURLModal').modal('hide');
                            $("#myModal").modal('show');
                            return true;
                        } else {
                            $("#msgVideo").text('<?php echo __('Invalid URL'); ?>');
                            return false;
                        }
                    }
                });
            }
        });

        $("#Users_ImageProfile_ExternalURL").on('click', function () {
            $("#msgImg").text('');
            $("#users-external-url").val("");
            $("#Users_ExternalURL_Type").val(USER_PROFILE_EXTERNAL_AVARTAR_URL);
            $("#externalURLModal").modal('show');
        });

        $("#Users_ExternalURL_Post").on('click', function () {
            var externalURLType = $("#Users_ExternalURL_Type").val();
            var externalURL = $("#users-external-url").val() || '';
            var arrImg = ['jpg', 'png', 'gif', 'bmp'];
            // var arrClip = ['mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv'];
            var ext = getFileExtension(externalURL);
            if (externalURL == null ||
                    externalURL === undefined ||
                    typeof externalURL === 'undefined' ||
                    $.trim(externalURL) == "" ||
                    (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL && $.inArray(ext, arrImg) == -1) ||
                    (externalURLType == USER_PROFILE_EXTERNAL_VIDEO_URL && !isYoutubeURL(externalURL))) {
                if (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL) {
                    $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                } else {
                    $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                }
                if(externalURLType ===0)
                {
                    $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                }
                return;
            }
            imageExists(externalURL, function(exists) {
            if(exists === true)
            {
                check = 1;
            }
            else
            {
                check = 2;
            }
             });
            if(check === 2)
            {
                $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                return false;
            }
            var checkURL = $("<div>", {
                id: "externalURLCheck",
                css: {
                    "font-size": "18px",
                    "margin-top": "140px"
                },
                text: "<?= __('Checking URL... Please wait.') ?>"
            });
            if (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL) {
                // $('#users-external-avatar-url').val("");
                var img = new Image();
                $.LoadingOverlay('show', {zIndex: 9999, custom: checkURL});
                img.onload = function () {
                    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                        $("#msgImg").text('Invalid URL');
                    } else {
                        $("#users-avatar").val("");
                        $("#profile-picture").css('background-image', 'url(' + this.src + ')');
                        $("#profile-picture .centered-text").css('visibility', 'hidden');
                        $('#users-external-avatar-url').val(this.src);
                        $('.cr-boundary').hide();
                        $('.cr-slider-wrap').hide();
                        $('#crop_images').hide();
                        $("#users-avatar-url").val("");
                    }
                    $.LoadingOverlay('hide');
                }
                img.onerror = function () {
                    $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                    $.LoadingOverlay('hide');
                }
                img.src = externalURL;
            } else {
                $.LoadingOverlay('show', {zIndex: 9999, custom: checkURL});
                $.ajax({
                    url: '<?= $this->Url->build([ "controller" => "Profile", "action" => "urlExists"]) ?>',
                    type: 'POST',
                    data: {
                        url: externalURL
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.status) {
                            var youtubeId = youtube_parser(externalURL);
                            $("#myModal .btn-file").css('background-image', 'url(https://i.ytimg.com/vi/' + youtubeId + '/hqdefault.jpg)');
                            $('#userswork-external-video-url').val(externalURL);
                            $("#users-video").val("");
                        } else {
                            $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                        }
                        $.LoadingOverlay('hide');
                        $("#myModal").modal('show');
                    },
                    error: function (e) {
                        $("#msgImg").text('<?php echo __('Invalid URL'); ?>');
                        $.LoadingOverlay('hide');
                        $("#myModal").modal('show');
                    }
                });
            }
            $("#externalURLModal").modal('hide');
        });
    });
      function imageExists(url, callback) {
            var img = new Image();
            img.onload = function() { callback(true); };
            img.onerror = function() { callback(false); };
            img.src = url;
          }
    function reset_box_add_work() {
        $('#myModal textarea').val('');
        $('#myModal input').val('');
        $("#myModal #iframe-video").html('');
    }

    function show_guide_get_embed_code() {
        $('#guide-get-embeded').toggle();
    }

    function isHTML(str) {
        return /^<.*?>$/.test(str) && !!$(str)[0];
    }
$uploadCrop = $('#crop').croppie({
    enableExif: true,
    viewport: {
        width: 300,
        height: 300,
        type: 'square'
    },
    boundary: {
        width: 400,
        height: 400,
    }
});

var _URL = window.URL || window.webkitURL;
$('.cr-boundary').hide();
$('.cr-slider-wrap').hide();
$('#users-avatar').on('change', function () { 
$('.cr-boundary').hide();
$('.cr-slider-wrap').hide();
$('#crop_images').hide();
var file, img;
if ((file = this.files[0])) {
    img = new Image();
    img.onload = function () {
        if( this.width < 480 || this.height < 480  || this.width > 2500 || this.height > 1500)
        {
            alertBox('<?php echo __('Chọn 1 ảnh có độ phân giải tối thiểu 480x480, tối đa 2500x1500'); ?>');
            return false;
        }
        else
        {
            $('.cr-boundary').show();
            $('.cr-slider-wrap').show();
            $('#crop_images').show();
            $('.popup-w').show();
        }
       
    };
    img.src = _URL.createObjectURL(file);
}

    var reader = new FileReader();
    reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
            url: e.target.result
        }).then(function(){
            console.log('jQuery bind complete');
        });
        
    }
    reader.readAsDataURL(this.files[0]);
});
$('.upload-result').on('click', function (ev) {
    $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {
            $("#profile-picture").css('background-image', 'url(' + resp + ')');
            $("#profile-picture .centered-text").css('visibility', 'hidden');
            $('#users-avatar-url').attr('value', resp);
            $('.cr-boundary').hide();
            $('.cr-slider-wrap').hide();
            $('#crop_images').hide();
            $('.popup-w').hide();
    });
});
function countCharDescriptionprofile(val) {
    var lend = val.value.length;
    console.log(lend);
    if (lend > 499) {
      val.value = val.value.substring(1, 499);
    } else {
      $('#Users_EditProfile_BioRemainCharCount').text(499 - lend);
    }
};
function close_popup()
{
    $(".wrapper_model").hide();
}
</script>

<style>
h3 {
	background: #eb6500;
    padding: 15px;
    text-align: center;
    margin-top: 0px;
}
#crop_images{
background: #4a4848;
}
.upload-result {
    float: right;
    margin: 15px;
	background:#eb6500;
	color: #333;
}
.popup-w {
	display:none;
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(7,36,40, 0.5);
    top: 0;
    left: 0;
    z-index: 9;
}
#crop_images
{
	display:none;
    width: 600px;
    height: 560px;
    box-sizing: border-box;
    text-align: center;
    position: fixed;
    top: 40%;
    left: 50%;
    margin-top: -190px;
    margin-left: -285px;
    z-index: 999;
    color: black;
}
#profile-picture {
    background-position: 50%;
    height: 541px;
    width: 600px;
}
</style>
