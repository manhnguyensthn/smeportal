<style type="text/css">
    .modal-open{ overflow: inherit; }
    body{ padding-right: 0 !important; }
</style>
<div class="metting page_asset">
    <?php echo $this->element('Mettings/title_metting'); ?>
    <?php echo $this->element('Mettings/tab_metting'); ?>
	
<!-- 	<?php if($bOwner): ?>
	<div class="container">
		<div class="form-upload-file">
			<form action="javascript:void(0)" method="post" enctype="multipart/form-data">
				<input type="file" name="val[file_select]" onchange="uploadFileDropbox(this, '<?php echo $Project->id; ?>');" class="input_hidden select_file_upload">
				<button type="submit" class="btn-launch button_upload_file" onclick="clickFileInput(this);" data-input=".select_file_upload" style="float: none;color: #080707; margin: 18px 0;">
					<span class="default">
						<i class="fa fa-cloud-upload" aria-hidden="true"></i> <?php echo __('Upload'); ?>
					</span>
					<span class="loadding hidden">
						<i class="fa fa-spinner fa-pulse fa-fw"></i> <?php echo __('Loading...'); ?>
					</span>
				</button>
			</form>
		</div>
		<p><em><?php echo __("Note: User can upload text, pdf, image files (jpg, png, tiff, gif with maximum 3MB of 2500x1500 pixel resolution); video files (mov, mpeg, avi, mp4, 3gp, wmv, flv) with a size of 1KB-100MB"); ?></em></p>
	</div>
	<?php endif; ?> -->
	
<!-- 	<div class="container">
		<div class="template_table_assets">
		<?php $sClassDisplay = ''; ?>
		<?php if(!isset($ListFolders['entries']) || empty($ListFolders['entries'])): ?>
			<?php $sClassDisplay = 'hidden'; $this->General->getNoItemMessage(); ?>
		<?php endif; ?>
			<div class="wrapper_table <?php echo $sClassDisplay; ?>">
				<div class="table_row th">
					<div class="table_cell" style="width: 124px;">&nbsp;</div>
					<div class="table_cell name" style="width: 300px;"><?php echo __('Name'); ?></div>
					<div class="table_cell"><?php echo __('Owner'); ?></div>
					<div class="table_cell"><?php echo __('Date'); ?></div>
					<div class="table_cell"><?php echo __('File Size'); ?></div>
				</div>
				<div class="table_body">
					<?php foreach ($ListFolders['entries'] as $key => $ListFolder): ?>
						<?php
							echo $this->element('Mettings/item_asset', array(
								'ListFolder' => $ListFolder,
								'project_id' => $Project->id,
								'user_id'    => $Project->user->id,
							));
						?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div> -->
	<script src="http://code.highcharts.com/highcharts.js"></script>
<div class="row" style="margin-left: 0; margin-right: 0">
  <h2>Các yêu cầu/kiến nghị mới</h2>
  <div class="col-md-6">
    <table class="table table-bordered">
      <tr>
        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('title') ?></th>
        <th scope="col"><?= $this->Paginator->sort('type') ?></th>
        <th scope="col"><?= $this->Paginator->sort('project_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('status') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th>
      </tr>
      <?php foreach ($requests as $request): ?>
      <tr>
          <td><?= $this->Number->format($request->id) ?></td>
          <td><?= h($request->title) ?></td>
          <td>
          <?php if(($this->Number->format($request->type))==1): ?>
          <?php echo "Quản lý hội viên" ?>
          <?php elseif(($this->Number->format($request->type))==2): ?>
          <?php echo "Pháp lý" ?>
          <?php elseif(($this->Number->format($request->type))==3): ?>
          <?php echo "XTTM" ?>
          <?php else: ?>
          <?php echo "Vốn vay" ?>
          <?php endif; ?>
          </td>
          <td><?= $request->has('project') ? $this->Html->link($request->project->title, ['controller' => 'Projects', 'action' => 'view', $request->project->id]) : '' ?></td>
          <td><?= $this->Number->format($request->status) ?></td>
          <td class="actions">
              <?= $this->Html->link(__('View'), ['action' => 'respond', $request->id]) ?>
              <!-- <?= $this->Html->link(__('Edit'), ['action' => 'edit', $request->id]) ?>
              <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $request->id], ['confirm' => __('Are you sure you want to delete # {0}?', $request->id)]) ?> -->
          </td>
      </tr>
      <?php endforeach; ?>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
    </div>
  </div>
  <div class="col-md-6 pull-left" style="margin-top: 0;">
    <div id="container" style="width:100%; height:300px;"></div>
  </div>
</div>
<div class="row" style="margin-left: 0;">

    <div class="col-md-6">
      <h2>Tình trạng doanh nghiệp</h2>
      <table class="table table-bordered">
        <tr>
          <td>Tên doanh nghiệp</td>
          <td>Khu vực</td>
          <td>Đóng hội phí</td>
        </tr>
        <tr>
          <td>Công ty ABC</td>
          <td>Miền bắc</td>
          <td>Đã đóng hội phí</td>
        </tr>
      </table>
      <div class="paginator">
          <ul class="pagination">
              <?= $this->Paginator->first('<< ' . __('first')) ?>
              <?= $this->Paginator->prev('< ' . __('previous')) ?>
              <?= $this->Paginator->numbers() ?>
              <?= $this->Paginator->next(__('next') . ' >') ?>
              <?= $this->Paginator->last(__('last') . ' >>') ?>
          </ul>
      </div>
    </div>
    <div class="col-md-6">
      <h3>Bộ lọc</h3>
      <div class="row">
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Kiểu yêu cầu'),
            'type' => 'text',
            'options' => ['Pháp lý','Vốn vay','Xúc tiến','Quản lý hội viên'],
            'empty' => __('-- Từ khóa --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Nhóm'),
            'type' => 'select',
            'options' => $groups,
            'empty' => __('-- Chọn nhóm --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Tình trạng'),
            'type' => 'select',
            'options' => ['Pháp lý','Vốn vay','Xúc tiến','Quản lý hội viên'],
            'empty' => __('-- Chọn tình trạng --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
      </div>
      <div class="row">
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Doanh nghiệp'),
            'type' => 'select',
            'options' => ['Pháp lý','Vốn vay','Xúc tiến','Quản lý hội viên'],
            'empty' => __('-- Doanh nghiệp --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Khu vực'),
            'type' => 'select',
            'options' => ['Pháp lý','Vốn vay','Xúc tiến','Quản lý hội viên'],
            'empty' => __('-- Chọn khu vực --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Độ lớn vốn'),
            'type' => 'select',
            'options' => $scales,
            'empty' => __('-- Chọn độ lớn vốn --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
      </div>
      <div class="row">
        <?php echo $this->Form->input('type', [
            'class' => 'form-control',
            'label' => __('Mật độ yêu cầu'),
            'type' => 'select',
            'options' => ['Pháp lý','Vốn vay','Xúc tiến','Quản lý hội viên'],
            'empty' => __('-- Chọn mật độ --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-3 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
      </div>
    </div>
</div>
<script type="text/javascript">
Highcharts.chart('container', {
  chart: {
      type: 'column'
  },
  title: {
      text: 'Thống kê'
  },
  xAxis: {
      categories: ['Quản lý HV', 'Pháp lý', 'XTTM', 'Vốn vay']
  },
  yAxis: {
      min: 0,
      title: {
          text: ''
      },
      stackLabels: {
          enabled: true,
          style: {
              fontWeight: 'bold',
              color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          }
      }
  },
  legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false
  },
  tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
  },
  plotOptions: {
      column: {
          stacking: 'normal',
          dataLabels: {
              enabled: true,
              color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
          }
      }
  },
  series: [{
      name: 'Chưa xử lý',
      data: [<?= $manage ?>, <?= $juridical ?>, <?= $commerce ?>, <?= $loans ?>]
  }, {
      name: 'Đang xử lý',
      data: [<?= $manageProcessing ?>, <?= $juridicalProcessing ?>, <?= $commerceProcessing ?>, <?= $loansProcessing ?>]
  }, {
      name: 'Đã xử lý',
      data: [<?= $manageProcessed ?>, <?= $juridicalProcessed ?>, <?= $commerceProcessed ?>, <?= $loansProcessed ?>]
  }]
});
</script>
</div>