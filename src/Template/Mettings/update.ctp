<style type="text/css">
    .modal-open{ overflow: inherit; }
    body{ padding-right: 0 !important; }
</style>
<div class="metting page_update">
    <?php echo $this->element('Mettings/title_metting'); ?>
    <?php echo $this->element('Mettings/tab_metting'); ?>
    <div class="container">
        <div class="title_small"><?php echo __('Updates'); ?></div>
        <div class="time-line">
            <div class="line"></div>
            <div class="wrapper-timeline">
                <div class="timeline-add dropdown">
                    <a href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="horizontal"></span>
                        <span class="vertical"></span>
                    </a>

                    <?php if($bOwner && $Project): ?>
                    <div class="form-add dropdown-menu" aria-labelledby="dLabel">
                        <div class="item update">
                            <div class="title"><a href="javascript:void(0);"><?php echo __('Add Update'); ?></a></div>
                            <form action="javascript:void(0);" method="POST" onsubmit="addProjectUpdate(this, '<?php echo $Project['id']; ?>', 'add'); return false;">
                                <div class="form-group">
                                    <input type="text" name="title" placeholder="<?php echo __('Update Title'); ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <textarea name="description" id="" placeholder="<?php echo __('Description'); ?>" class="form-control"></textarea>
                                </div>

                                <div class="text-right">
                                    <button type="submit"><?php echo __('Add'); ?></button>
                                </div>
                            </form>
                        </div>
                        <?php if(isset($ProjectDataMilestone) && !empty($ProjectDataMilestone)): ?>
                        <div class="item milestone">
                            <div class="title"><a href="javascript:void(0);" aria-labelledby="dLabel"><?php echo __('Add Milestone'); ?></a></div>
                            <form action="javascript:void(0);" method="POST" style="border-color: #f2e9cf;" onsubmit="updateProjectMilestone(this, '<?php echo $ProjectDataMilestone['id']; ?>', 'add'); return false;">
                                <div class="form-group">
                                    <input type="text" name="title" placeholder="<?php echo __('Update Milestone'); ?>" class="form-control" value="<?php echo $ProjectDataMilestone->title; ?>">
                                </div>
                                <div class="form-group">
                                    <textarea name="description" placeholder="<?php echo __('Description'); ?>" class="form-control"><?php echo $ProjectDataMilestone->content; ?></textarea>
                                </div>
                                <div class="text-right">
                                    <button type="submit"><?php echo __('Add'); ?></button>
                                </div>
                            </form>
                        </div>
                        <?php endif; ?>
                    </div>
                    <script>
                        $('.timeline-add .form-add .item .title a').click(function(event) {
                            $('.timeline-add .form-add .item').removeClass('active');
                            $(this).closest('.item').addClass('active');
                            return false;
                        });
                    </script>
                    <?php endif; ?>
                </div>
                
                <div class="wrapper-content">
                    <ul class="clearfix">
                        <?php if(isset($ProjectUpdates) && !empty($ProjectUpdates)): ?>
                            <?php foreach ($ProjectUpdates as $key => $ProjectUpdate): ?>
                                <?php
                                    echo $this->element('Mettings/item_update', array(
                                        'ProjectUpdate' => $ProjectUpdate,
                                    ));
                                ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>

            </div>

            <div class="timeline-start">
                <div class="milestone-circle"></div>
                <p class="title-timeline text-center" style="margin: 0;"><?php echo __('Project Start'); ?></p>
                <span><?php
                    if(isset($Project->created) && !empty($Project->created)){
                        echo date("F j", strtotime($Project->created));
                    }
                ?></span>
            </div>
        </div>
    </div>
</div>