<?= $this->Html->css(['fullcalendar/fullcalendar.min']) ?>
<?= $this->Html->script(['fullcalendar/moment.min', 'fullcalendar/fullcalendar.min']) ?>

    <div class="container">
		<div id="calendar" style="max-width: 900px; margin: 20px auto 0; width: 100%;"></div>

		<ul class="template_list_project clearfix">
			<li class="item clearfix">
				<div class="name_category is_category">
					<span class="name">Movie Shoot Begins</span>
					<span class="category">Studio</span>
				</div>
				<div class="date clearfix">
					<div class="hours">8</div>
					<div class="minute">
						<span>30</span>
						<span>AM</span>
					</div>
				</div>
			</li>
			<li class="item clearfix">
				<div class="name_category">
					<span class="name">Coffee Break</span>
				</div>
				<div class="date clearfix">
					<div class="hours">10</div>
					<div class="minute">
						<span>00</span>
						<span>AM</span>
					</div>
				</div>
			</li>
		</ul>
    </div>
</div>
<script type="text/javascript">
	$('#calendar').fullCalendar({
		header: {
			left: 'prev',
			center: 'title',
			right: 'next',
		},
		height: 'auto',
		buttonIcons : {
			prev: 'left-single-arrow',
		    next: 'right-single-arrow',
		}
	});
</script>