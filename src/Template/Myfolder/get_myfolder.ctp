<div class="container">
    <div class="row">
        <div class="page-header col-md-12" style="border-bottom: none;">
            <h1><?php echo __('Doanh nghiệp'); ?></h1>
        </div>
        <div class="col-md-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" id="list-myfolder-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#created" aria-controls="created" role="tab" data-toggle="tab"><?php echo __('Created Projects'); ?></a></li>
                <li role="presentation"><a href="#joined" aria-controls="joined" role="tab" data-toggle="tab" onclick="get_my_folder_by_type('1','#joined');" ><?php echo __('Joined Projects'); ?></a></li>
                <li role="presentation"><a href="#followed" aria-controls="followed" role="tab" data-toggle="tab" onclick="get_my_folder_by_type('2','#followed');" ><?php echo __('Followed Projects'); ?></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="created">
                    <div class="row myfolder-tab-content">
                        <div class="col-md-12">
                            <div class="row">
                                <?php if (count($listCreatedProjects) > 0): ?>
                                    <h2><?php echo __('Created Newest Projects'); ?></h2>
                                    <div class="col-md-12 feature-myfolder-project" style="background-image: url(<?php echo !empty($listCreatedProjects[0]->image_url) ? $listCreatedProjects[0]->image_url : '/img/project_default.jpg'; ?>);">
                                        <a href="/projects/<?php echo $this->Tool->slug($listCreatedProjects[0]->title) . '-' . $listCreatedProjects[0]->id; ?>" class="box-link"></a>
                                        <a href="/applicant-<?php echo $listCreatedProjects[0]->id; ?>">
                                            <div class="group-avatars">
                                                <ul class="list-avatars">
                                                    <?php foreach ($listCreatedProjects[0]->users as $row): ?>
                                                        <li><img src="<?php echo !empty($row->user_picture) ? $row->user_picture : '/img/avatar_default.jpg'; ?>" title="<?php echo $row->name; ?>" alt="<?php echo $row->name; ?>" /></li>
                                                    <?php endforeach; ?>
                                                    <?php if (count($listCreatedProjects[0]->users) < 4): ?>
                                                        <?php for ($i = 1; $i<= (4 - count($listCreatedProjects[0]->users)); $i++): ?>
                                                            <li><img src="/img/avatar_default.jpg" title="Avatar" alt="Avatar" /></li>
                                                        <?php endfor; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </a>
                                        <a href="/chat-room/<?php echo $listCreatedProjects[0]->id; ?>/0" class="icon-chatting"><img src="/img/icon_chatting.png" title="Chatting" alt="Chatting" /><?php echo !empty($listCreatedProjects[0]->count_notify_chat) ? '<span class="notification-number">'.$listCreatedProjects[0]->count_notify_chat.'</span>' : ''; ?></a>
                                        <a href="/notifications/seeall?projectid=<?php echo $listCreatedProjects[0]->id; ?>" class="icon-notificaton"><img src="<?php echo !empty($listCreatedProjects[0]->count_notify) ? '/img/icon_bell.png' : '/img/icon_bell_none.png'; ?>" title="Notification" alt="Notification" /><?php echo !empty($listCreatedProjects[0]->count_notify) ? '<span class="notification-number">'.$listCreatedProjects[0]->count_notify.'</span>' : ''; ?></a>
                                        <a href="/projects/<?php echo $this->Tool->slug($listCreatedProjects[0]->title) . '-' . $listCreatedProjects[0]->id; ?>"><h5 class="project-title"><?php echo $listCreatedProjects[0]->title; ?></h5></a>
                                    </div>
                                <?php endif; ?>
                                <?php if (count($listCreatedProjects) > 1): ?>
                                    <h2><?php echo __('Other Projects'); ?></h2>
                                    <div class="col-md-12">
                                        <div class="row list-projects-folder" id="list-myfolder-created">
                                            <?php
                                            $i = 0;
                                            foreach ($listCreatedProjects as $project):
                                                $i++;
                                                ?>
                                                <?php if ($i > 1):
                                                    $avatar = (count($project->users) > 0 && !empty($project->users[0]->user_picture)) ? $project->users[0]->user_picture : '/img/avatar_default.jpg';
                                                    $member_name = (count($project->users) > 0) ? $project->users[0]->name : 'Avatar';
                                                    ?>
                                                    <div class="col-md-4 project-folder-item">
                                                        <div class="row">
                                                            <div class="col-md-12 box-item" style="background-image: url(<?php echo!empty($project->image_url) ? $project->image_url : '/img/project_default.jpg'; ?>);">
                                                                <a href="/applicant-<?php echo $project->id; ?>" class="applicant-avatar"><img src="<?php echo $avatar; ?>" alt="<?php echo $member_name; ?>" title="<?php echo $member_name; ?>" /></a>
                                                                <a href="/projects/<?php echo $this->Tool->slug($project->title) . '-' . $project->id; ?>" class="box-link"></a>




                                                                <a href="/notifications/seeall?projectid=<?php echo $project->id; ?>"><span class="icon-notificaton"><img src="<?php echo !empty($project->count_notify) ? '/img/icon_bell.png' : '/img/icon_bell_none.png'; ?>" /><?php echo !empty($project->count_notify) ? '<span class="notification-number">'.$project->count_notify.'</span>' : ''; ?></span></a>

                                                                <a href="/projects/<?php echo $this->Tool->slug($project->title) . '-' . $project->id; ?>"><h5 class="project-name"><?php echo $project->title; ?></h5></a>
                                                                <a href="/chat-room/<?php echo $project->id; ?>/0" class="icon-chatting"><img style = "margin-top: -150px;width: 50px;float: right;position: relative;margin-right: -8px;" src="/img/icon_chatting.png" title="Chatting" alt="Chatting" /><?php echo !empty($listCreatedProjects[0]->count_notify_chat) ? '<span class="notification-number" style = "display:none;">'.$listCreatedProjects[0]->count_notify_chat.'</span>' : ''; ?></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="row text-center load-more" id="list-myfolder-created-load-more">
                                            <img src="/img/loading.gif" alt="Loading" title="Loading" />
                                            <input type="hidden" name="current_page" value="1" />
                                        </div>
                                        <?php if ($i == 7): ?>
                                            <div class="row">
                                                <div class="col-md-12 text-center"><button class="btn btn-launch see-more-button" onclick="load_more_my_folder('#list-myfolder-created','0','#created');" ><?php echo __('See more'); ?></button></div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="joined">
                    <div class="row myfolder-tab-content">

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="followed">
                    <div class="row myfolder-tab-content">

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
