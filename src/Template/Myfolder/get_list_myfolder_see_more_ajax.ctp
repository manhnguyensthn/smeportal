<?php
foreach ($listProjects as $project):
    $avatar = (count($project->users) > 0 && !empty($project->users[0]->user_picture)) ? $project->users[0]->user_picture : '/img/avatar_default.jpg';
    $member_name = (count($project->users) > 0) ? $project->users[0]->name : 'Avatar';
    ?>
    <div class="col-md-4 project-folder-item">
        <div class="row">
            <div class="col-md-12 box-item" style="background-image: url(<?php echo!empty($project->image_url) ? $project->image_url : '/img/project_default.jpg'; ?>);">
                <a href="/applicant-<?php echo $project->id; ?>" class="applicant-avatar"><img src="<?php echo $avatar; ?>" alt="<?php echo $member_name; ?>" title="<?php echo $member_name; ?>" /></a>
                <a href="/projects/<?php echo $this->Tool->slug($project->title) . '-' . $project->id; ?>" class="box-link"></a>
                <a href="/notifications/seeall?projectid=<?php echo $project->id; ?>"><span class="icon-notificaton"><img src="<?php echo!empty($project->count_notify) ? '/img/icon_bell.png' : '/img/icon_bell_none.png'; ?>" /><?php echo!empty($project->count_notify) ? '<span class="notification-number">' . $project->count_notify . '</span>' : ''; ?></span></a>
                <a href="/projects/<?php echo $this->Tool->slug($project->title) . '-' . $project->id; ?>"><h5 class="project-name"><?php echo $project->title; ?></h5></a>
                <a href="/chat-room/<?php echo $project->id; ?>/0" class="icon-chatting"><img style = "margin-top: -150px;width: 50px;float: right;position: relative;margin-right: -8px;" src="/img/icon_chatting.png" title="Chatting" alt="Chatting" /><?php echo !empty($listCreatedProjects[0]->count_notify_chat) ? '<span class="notification-number" style = "display:none;">'.$listCreatedProjects[0]->count_notify_chat.'</span>' : ''; ?></a>
            </div>
        </div>
    </div>
<?php endforeach; ?>