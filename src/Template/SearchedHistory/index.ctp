<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Searched History'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="searchedHistory index large-9 medium-8 columns content">
    <h3><?= __('Searched History') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('keyword') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($searchedHistory as $searchedHistory): ?>
            <tr>
                <td><?= $this->Number->format($searchedHistory->id) ?></td>
                <td><?= $searchedHistory->has('user') ? $this->Html->link($searchedHistory->user->name, ['controller' => 'Users', 'action' => 'view', $searchedHistory->user->id]) : '' ?></td>
                <td><?= h($searchedHistory->keyword) ?></td>
                <td><?= h($searchedHistory->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $searchedHistory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $searchedHistory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $searchedHistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $searchedHistory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
