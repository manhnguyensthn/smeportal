<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Searched History'), ['action' => 'edit', $searchedHistory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Searched History'), ['action' => 'delete', $searchedHistory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $searchedHistory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Searched History'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Searched History'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="searchedHistory view large-9 medium-8 columns content">
    <h3><?= h($searchedHistory->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $searchedHistory->has('user') ? $this->Html->link($searchedHistory->user->name, ['controller' => 'Users', 'action' => 'view', $searchedHistory->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Keyword') ?></th>
            <td><?= h($searchedHistory->keyword) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($searchedHistory->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($searchedHistory->created) ?></td>
        </tr>
    </table>
</div>
