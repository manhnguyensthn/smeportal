<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= isset($title) ? $title : $this->fetch("title") ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css(['bootstrap.min', 'font-awesome.min', 'flexslider', 'reset', 'style']) ?>
    <?php echo $this->element('Block/language_script'); ?>
        <?= $this->Html->script(['jquery.min', 'bootstrap-min', 'jquery.flexslider', 'app','highcharts']) ?>
        <meta property="og:url"           content="<?php echo isset($current_url) ? $current_url : '/'; ?>" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="<?php echo isset($title) ? $title : $this->fetch("title") ?>" />
        <meta property="og:description"   content="<?php echo isset($meta_description) ? $meta_description : ''; ?>" />
        <meta property="og:image"         content="<?php echo (isset($meta_image) && !empty($meta_image)) ? $meta_image : ROOT_URL.'img/logo.png'; ?>" />
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <?= $this->fetch('script') ?>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <script src="../../webroot/js/highcharts.js"></script>
    </head>
    <body>
        <div class="wrapper_model"></div>
        <?php echo $this->element('Block/location'); ?>
        
        <?php
        if ($this->AuthUser->_getUser()) {
            $user = $this->AuthUser->_getUser();
            echo $this->element('header_front_in');
        } else {
            echo $this->element('header_front_out');
        }
        ?>
        <div id="content">
            <?= $this->Flash->render() ?>
            <?= $this->Flash->render('logout_duplicate') ?>
            <?= $this->fetch('content') ?>
        </div>
        <?php echo $this->element('footer_front'); ?>
        <script>
            $('.dropdown-toggle').dropdown();
        </script>
        <script type="text/javascript">
            jQuery(window).load(function ($) {
                jQuery('.alert-warning, .alert-success').removeAttr('onclick').click(function () {
                    jQuery(this).slideUp(500);
                });
                setTimeout(function () {
                    jQuery('.alert-warning, .alert-success').slideUp(500);
                }, 5000);
            });
        </script>
    </body>
</html>
