<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $this->fetch('title'); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?= $this->Html->css(['bootstrap.min','admin/AdminLTE.min','admin/_all-skins.min','admin/blue', 'admin/admin']); ?>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?= $this->Html->script(['jquery.min']);?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <div class="flash_message">
                <?= $this->Flash->render(); ?>
            </div>
            <?= $this->element('admin/header'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?= $this->element('admin/navbar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?= $this->fetch('content') ?>
            </div>
            <?= $this->element('admin/footer'); ?>
        </div>
        <!-- ./wrapper -->

        <?= $this->Html->script(['bootstrap-min','admin/app.min']); ?>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('.flash_message').stop().slideDown(1000);
                setTimeout(function () {
                    jQuery('.flash_message').slideUp(1000);
                }, 5000);
            });
            var liArr = $('.sidebar-menu').find('li').find('a');
            $.each(liArr, function(i,v){
                var params = $(v).attr('href').split('/');
                var uri = window.location.pathname.substring(1).split('/');
                
                if($(v).attr('href') === '/'+ window.location.pathname.substring(1)){
                    $(v).parent('li').addClass('active');
                }else if(params[2] === uri[1]){
                    $(v).parent('li').addClass('active');
                }else{
                    $(v).parent('li').removeClass('active');
                }
            });
        </script>
    </body>
</html>