<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= isset($title) ? $title : $this->fetch("title") ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css(['bootstrap.min', 'font-awesome.min', 'flexslider', 'reset', 'style']) ?>
        <?= $this->Html->script(['jquery.min', 'bootstrap-min', 'jquery.flexslider', 'app']) ?>
        <script type="text/javascript" src="https://platform.linkedin.com/in.js">
            api_key: 789eiasoam5s0p
            authorize: true
        </script>
        <meta property="og:url"           content="<?php echo isset($current_url) ? $current_url : '/'; ?>" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="<?php echo isset($title) ? $title : $this->fetch("title") ?>" />
        <meta property="og:description"   content="<?php echo isset($meta_description) ? $meta_description : ''; ?>" />
        <meta property="og:image"         content="<?php echo (isset($meta_image) && !empty($meta_image)) ? $meta_image : ROOT_URL.'img/logo.png'; ?>" />
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>

        <div class="wrapper_model" style = "display:none"><div class="modal fade alert-box alert in" tabindex="-1" role="dialog" style="display: block; padding-right: 17px;"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button onclick="close_popup_login();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel"></h4></div><div class="modal-body"><div class="row"><div class="col-xs-12 text-center alert-content"><p><strong><?php echo __('This email has been registered.'); ?> </strong></p></div></div><div class="row"><div class="col-xs-12 text-center"><button class="btn btn-launch" onclick="close_popup_login();" data-dismiss="modal" aria-label="Close" style="float: none;"><?php echo __('ok'); ?></button></div></div></div></div></div></div></div>
            <div class= "modal-backdrop fade in" style = "display:none"></div> 
        <?php
        if ($this->AuthUser->_getUser()) {
            $user = $this->AuthUser->_getUser();
            echo $this->element('header_front_in');
        } else {
            echo $this->element('header_front_out');
        }
        ?>
        <div id="content">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
        <?php echo $this->element('footer_front'); ?>
        <script>
            function gPSignInCallback(e) {
//                console.log(e);
//        return false;
                if (e["status"]["signed_in"]) {
                    gapi.client.load("plus", "v1", function () {
                        if (e["access_token"]) {
                            if (!e['_aa']) {
                                getProfile();
                            }
                        } else if (e["error"]) {
//                            console.log("There was an error: " + e["error"]);
                        }
                    });
                } else {
//                    console.log("Sign-in state: " + e["error"]);
                }
            }
            function getProfile() {
                var req = gapi.client.plus.people.get({
                    userId: "me"
                });
                req.execute(function (res) {
//                    console.log(res);
//            return false;
                    if (res.error) {
                        return
                    } else if (res.id) {
                        var email = res['emails'].filter(function (v) {
                            return v.type === 'account';
                        })[0].value;
//                console.log(e);
                        var name = res.displayName;
                        var google_id = res.id;
                        var first_name = res.name.familyName;
                        var last_name = res.name.givenName;
                        var imageUrl = res.image.url;
                        var avatar = imageUrl.replace("photo.jpg?sz=50", "photo.jpg?sz=150");
                        // if (loop == 0) {
                            pushUsersInfo(name, first_name, last_name, email, avatar, google_id, 2);
                            // loop = 1;
                        // }
                        return;
                    }
                });
            }
            (function () {
                var e = document.createElement("script");
                e.type = "text/javascript";
                e.async = true;
                e.defer = true;
                e.src = "https://apis.google.com/js/client:platform.js";
                var t = document.getElementsByTagName("script")[0];
                t.parentNode.insertBefore(e, t)
            })();
        </script>
        <script type="text/javascript">
            jQuery(window).load(function ($) {
                jQuery('.alert-warning, .alert-success').removeAttr('onclick').click(function () {
                    jQuery(this).slideUp(500);
                });
                setTimeout(function () {
                    jQuery('.alert-warning, .alert-success').slideUp(500);
                }, 5000);
            });
        </script>
    </body>
</html>
