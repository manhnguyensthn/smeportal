<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="content">
    <div class="users_form_edit" id="users_form_edit">
        <div class="content-setting" style="width: 100%;">
            <h2><?php echo __('Settings'); ?></h2>
            <div class="clearfix"></div>
            <div class="menu-child">
                <ul class="ulMenu">
                    <li class="menuLink active"><a href="#"><?php echo __('Edit Profile'); ?></a></li>
                    <li class="menuLink "><a href="#"><?php echo __('Account Detail'); ?></a></li>
                    <li class="menuLink "><a href="#"><?php echo __('Notification'); ?></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-8" style="margin-top: 50px;">
                <?= $this->Form->create($user) ?>
                <?php
                    echo $this->Form->input('Users.first_name',array('class' => 'form-control','label' => __('First Name')));
                    echo $this->Form->input('Users.last_name',array('class' => 'form-control','label' => __('Last Name')));
//                    echo '<div class="file-form">';
//                    echo $this->Form->input('Users.avatar',array('type' => 'file','class' => 'custom_file','label' => __('Profile Picture')));
//                    echo '<div class="overlide"></div></div>'
                ?>
                <div class="input text ">
                    <label for="user-avatar"><?php echo __('Profile Picture'); ?></label>
                    <div class="btn btn-default btn-file">
                        <div class="centered-text">
                            <span><?php echo __('Drag Picture Here'); ?></span>
                            <div class="clearfix"></div>
                            <span><?php echo __('Or'); ?></span>
                            <div class="clearfix"></div>
                            <span class="label label-warning"><?php echo __('Upload from computer'); ?></span>
                        </div>

                        <input type="file" name="Users[avatar]" class="custom_file" maxlength="100" id="users-avatar" value="">
                    </div>
                </div>
                
                <div class="clearfix"></div>
                <div class="group-location">
                    <?php 
                        echo $this->Form->input('Users.country_id',array('type' => 'text','class' => 'form-control','placeholder' => __('Country'),'label' => __("Location") ));
                        echo $this->Form->input('Users.district_id',array('type' => 'text','class' => 'form-control',
                            'templates' => [
                                'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>',
                            ],
                            'placeholder' => __('City'),'label' => FALSE));
                        echo $this->Form->input('Users.postcode',['class' => 'form-control','label' => FALSE,'placeholder' => __('Postal Code'),
                                'templates' => [
                                    'inputContainer' => '<div class="col-md-6 input padd-right {{type}}{{required}}">{{content}}</div>',
                                ]] 
                            );
                    ?>
                    <div class="clearfix" style="margin-bottom: 30px;"></div>
                    <?php
                        echo $this->Form->input('Roles.role0',array(
                            'class' => 'form-control','label' => __('Choose your role'),
                            'type' => 'select','options' => [1=>'Role X',2=>'Role Y'],'empty' => 'Role 1'
                        ));
                       echo $this->Form->input('Roles.role1',array(
                            'class' => 'form-control','label' => FALSE,
                            'type' => 'select','options' => [1=>'Role X',2=>'Role Y'],'empty' => 'Role 2'
                        ));
                    ?>
                </div>
                <div class="clearfix" style="margin-bottom: 20px;"></div>
                <div class="input texarea">
                    <?php 
                        echo '<label for="biography">Biography</label>';
                        echo $this->Form->textarea('Users.biography',array('class' => 'form-control','rows' => 7,'placeholder' => __('Biography') ,'label' => __('Biography')));
                    ?>
                </div>
                <div class="input text">
                    <label for="website"><?php echo __('Website'); ?></label>
                    <div class="input-group" style="margin-bottom: 15px;">
                        <input type="text" id="webinfo" class="form-control" placeholder="Website">
                        <span class="input-group-btn">
                            <button class="btn btn-warning" id="addweb" type="button"><?php echo __('Add'); ?></button>
                        </span>
                    </div>
                    <div id="websiteInfo">
                    
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="input text">
                    <label for="user-portfolio"><?php echo __('Thành tích'); ?></label>
                    <div class="clearfix"></div>
                    <div class="col-md-6 btn-add-video" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-warning pull-right"><?php echo __('Save Changes'); ?></button>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-custom">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('Thêm thành tích'); ?></h4>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <div class="btn btn-default btn-file">
                    <div class="centered-text">
                        <span><?php echo __('Drag Picture Here'); ?></span>
                        <div class="clearfix"></div>
                        <span><?php echo __('Or'); ?></span>
                        <div class="clearfix"></div>
                        <span class="label label-warning"><?php echo __('Upload from computer'); ?></span>
                    </div>
                    <input type="file" name="Users[video]" class="custom_file" maxlength="100" id="users-video" value="">
                </div>
            </div>
          <div class="form-group">
            <?php echo $this->Form->input('title',['class' => 'form-control','label' => 'Title']) ?>
          </div>
          <div class="form-group">
              <label for="description"><?php echo __('Description'); ?></label>
              <?php echo $this->Form->textarea('description',['class' => 'form-control','label' => 'Description']) ?>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning btn-post-video"><?php echo __('Post'); ?></button>
      </div>
    </div>
  </div>
</div>