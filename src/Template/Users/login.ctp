<div class="content content_login">
    <div class="content-signup">
        <div class="content-login">
        <h1><?php echo __('Log In'); ?></h1>
        <?= $this->Flash->render('auth') ?>
        <?= $this->Form->create() ?>
        <?= $this->Form->input('email', array('class' => 'form-control', 'placeholder' => __("Email"), 'label' => FALSE, 'value'=> isset($cookieData['email']) ? $cookieData['email'] : '')) ?>
        <?= $this->Form->input('password', array('class' => 'form-control', 'placeholder' => __("Password"), 'label' => FALSE, 'value'=> isset($cookieData['password']) ? $cookieData['password'] : '')) ?>
        <?= $this->Form->input('devicetoken', array('type' => "hidden", 'value' => YOUR_IP)) ?>
        <?= $this->Form->input('platform', array('type' => "hidden", 'value' => PLATFORM_WEB)) ?>
        <label>
            <a style="color: #4dbfc1;font-weight: normal" href="/users/forgot_pass"><?php echo __('Forgot your password?'); ?></a>
        </label>
        <button class="btn btn-warning btn-login btn-signup">
            <span><?php echo __('Log in'); ?></span>
        </button>
        <div class="checkbox-login checkbox-signup" style="margin-top: 20px;">
            <label>
                <input name="remember" type="checkbox" value="<?php echo isset($rememberMe) ? $rememberMe : 0; ?>" <?php echo (isset($rememberMe) && $rememberMe == 1) ? 'checked' : ''; ?> /><span><?php echo __('Remember me') ?></span>
            </label>
        </div>
        <?= $this->Form->end() ?>
        </div>
        <div class="content-signup-fb hidden">
        <h1><?php echo $termofuse->title; ?></h1>  
        <p class="note-re"><b><?php echo __('Bạn cần cuộn hết nội dung để tiếp tục.'); ?></b></p>
        <div class="content-term content-term-fb">
               <?= $this->element('term_of_user') ?>
        </div>
        <div class="checkbox-login checkbox-signup-fb hidden">
            <label>
                <input name="receive_newletter" id="checkbox-submit-fb" type="checkbox" value="0" ><span style="width:420px"><?php echo __('I have read and agree to all the terms and conditions above.') ?></span>
            </label>
        </div>
        <button type="button" class="btn btn-warning btn-login btn-signup-fb hidden" >
            <span><?php echo __('Become a member'); ?></span>
        </button>    
        </div>  
        <hr class="hr-signup">
        <div class="btn-group-sns">
            <a id="btn-1" href="javascript:void(0)"  class="btn btn-block btn-social btn-facebook">
                <span class="fa fa-facebook-official"></span>
                <?php echo __("Log in with Facebook") ?> <i class="fa fa-spinner fa-spin" style="font-size:24px; margin-left: 20px; display: none"></i>
            </a>
            <a id="btn-2" href="javascript:void(0)" onclick="javascript:googleAuth()" class="btn btn-block btn-social btn-google">
                <span class="fa fa-google-plus"></span><?php echo __(" Log in with Google +") ?> <i class="fa fa-spinner fa-spin" style="font-size:24px; margin-left: 20px; display: none"></i>
            </a>
        </div>
        <h5><?php echo __("Chúng tôi sẽ không đăng gì khi chưa có sự đồng ý của bạn") ?></h5>
        <hr class="hr-signup">
        <div class="signup-login login">
            <span><?php echo __("New to We the Project?") ?></span><a style="color: #4dbfc1;font-weight: normal" href="/users/register"> <?php echo __("Sign up") ?></a>
        </div>
    </div>
</div>
<input type="hidden" id="check_type" value="0">
<script>
            var loop = 0;
             // Check exist email
            function pushUsersInfo(name, first_name, last_name, email, avatar, sns_id, sns_type) { 
                   var check = $("#checkbox-submit-fb").is(":checked");
                   $.ajax({
                             type: 'POST',
                             dataType: 'json',
                             url: '<?php echo ROOT_URL . 'users/Checkmail' ?>',
                             data: {
                                 email: email
                             },
                             success: function (result) {
                                 if(result == 1 || check == true) {
                                    AddUserInfo(name, first_name, last_name, email, avatar, sns_id, sns_type);
                                 } else {
                                     $('.content-signup-fb').removeClass('hidden');
                                     $('.content-login').addClass('hidden');
                                     loop = 1;
                                     return false;
                                 } 
                             }
                         });
                         return false;
            } 
            // Show checkbox register fb, g+,    
             // Show checkbox register fb, g+,    
            $('.content-term-fb').scroll(function() {
                 if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    $('.checkbox-signup-fb').removeClass('hidden'); 
                  } 
            }); 
            function AddUserInfo(name, first_name, last_name, email, avatar, sns_id, sns_type) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '<?php echo ROOT_URL . 'users/ajaxSNS' ?>',
                    data: {
                        name: name,
                        first_name: first_name,
                        last_name: last_name,
                        email: email,
                        sns_id: sns_id,
                        sns: sns_type,
                        avatar: avatar
                    },
                    beforeSend: function (xhr) {
                        $("#btn-" + sns_type + " i").show();
                    },
                    success: function (data) {
                        if (data.ret === "OK") {
                            window.location = "<?php echo ROOT_URL; ?>";
                        } else {
                            $("#btn-" + sns_type + " i").hide();
                            $('.modal-backdrop').show();
			    $('.wrapper_model').show();
                            return false;
                        }
                    }
                });
            }
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '<?php echo FB_APP_ID; ?>',
                    xfbml: true,
                    version: '<?php echo FB_APP_VERSION ?>'
                });
                $("#btn-1.btn-facebook").click(function () {
                   var check = $("#checkbox-submit-fb").is(":checked");
                   if(loop == 1 && check == false) {
                         $('.popup-check').removeClass('hidden');
                         return false;
                   }
                    $("#check_type").val('fb'); 
                    FB.getLoginStatus(function (response) {
                        if (response.status === 'connected') {
                            FB.api('/me', {locale: 'en_US', fields: 'name, email, last_name, first_name'},
                                    function (response) {
                                        var name = response.name;
                                        var first_name = response.first_name;
                                        var last_name = response.last_name;
                                        var email = response.email;
                                        var fb_id = response.id;
                                        var avatar = "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                                        pushUsersInfo(name, first_name, last_name, email, avatar, fb_id, 1);
                                    }
                            );
                        } else {
                            FB.login(function (response) {
                                if (response.authResponse) {
                                    FB.api('/me', {locale: 'en_US', fields: 'name, email, first_name, last_name'},
                                            function (response) {
                                                var name = response.name;
                                                var first_name = response.first_name;
                                                var last_name = response.last_name;
                                                var email = response.email;
                                                var fb_id = response.id;
                                                var avatar = "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                                                pushUsersInfo(name, first_name, last_name, email, avatar, fb_id, 1);
                                            }
                                    );
                                } else {
                                    return false;
                                }
                            }, {
                                scope: 'email,user_friends',
                                return_scopes: true
                            });
                        }
                    });
                });
            };
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
//    login with linkedin
            function displayProfiles(profiles) {
                var member = profiles.values[0];
                var name = member.firstName + member.lastName;
                var first_name = member.firstName;
                var last_name = member.lastName;
                var email = member.emailAddress;
                var linked_id = member.id;
                var avatar = member.pictureUrl;
                pushUsersInfo(name, first_name, last_name, email, avatar, linked_id, 3);
            }
            function liAuth() {
                 var check = $("#checkbox-submit-fb").is(":checked");
                   if(loop == 1 && check == false) {
                         $('.popup-check').removeClass('hidden');
                         return false;
                   }
                $("#check_type").val('li'); 
                IN.User.authorize(function () {
                    IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "picture-url").result(displayProfiles);
                });
            }
//    login with google
            function googleAuth() {
                 var check = $("#checkbox-submit-fb").is(":checked");
                   if(loop == 1 && check == false) {
                         $('.popup-check').removeClass('hidden');
                         return false;
                   }
                $("#check_type").val('gg'); 
                gapi.auth.signIn({
                    'clientid': '<?php echo GOOGLE_OAUTH_CLIENT_ID; ?>',
                    'cookiepolicy': "<?php echo ROOT_URL; ?>",
                    'approvalprompt': 'auto',
                    'prompt': 'consent',
                    'display': 'page',
                    'access_type': 'offline',
                    'requestvisibleactions': "http://schemas.google.com/AddActivity",
//            scope: "https://www.googleapis.com/auth/plus.login email"
                    'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read',
                    'callback': gPSignInCallback,
                });
            }
            function gPSignInCallback(e) {
//                console.log(e);
//        return false;
                if (e["status"]["signed_in"]) {
                    gapi.client.load("plus", "v1", function () {
						//console.log(e);
						//return false;			
                        if (e["access_token"]) {
							 if (!e['_aa']) {
                                getProfile();
							 }
                        } else if (e["error"]) {
//                            console.log("There was an error: " + e["error"]);
                        }
                    });
                } else {
//                    console.log("Sign-in state: " + e["error"]);
                }
            }
            function getProfile() {

                var req = gapi.client.plus.people.get({
                    userId: "me"
                });
			
                req.execute(function (res) {
		
                    //console.log(res);
            //return false;
                    if (res.error) {
                        return
                    } else if (res.id) {
						
                        var email = res['emails'].filter(function (v) {
                            return v.type === 'account';
                        })[0].value;
				
//                console.log(e);
                        var name = res.displayName;
                        var google_id = res.id;
                        var first_name = res.name.familyName;
                        var last_name = res.name.givenName;
                        var imageUrl = res.image.url;
                        var avatar = imageUrl.replace("photo.jpg?sz=50", "photo.jpg?sz=150");
                        pushUsersInfo(name, first_name, last_name, email, avatar, google_id, 2);
                        return;
                    }
                });
            }
             function close_popup_login()
            {
                    $('.modal-backdrop').hide();
                    $('.wrapper_model').hide();
            }
            document.getElementById('checkbox-submit-fb').onclick = function(e){
                if (this.checked){
                    $('.btn-signup-fb').removeClass('hidden');
                }
                else{
                    $('.btn-signup-fb').addClass('hidden');
                }
            };  
             // handing click button become a member for fb, g+, linkedin 
     $('.btn-signup-fb').click(function(){
              var check_type = $('#check_type').val();
              if(check_type === 'fb') {
                   $( "#btn-1.btn-facebook" ).trigger( "click" );
              } else if (check_type === 'gg') {
                  googleAuth();
              } else {
                  liAuth();
              }
     });
     function close_popup_check() {
            $('.popup-check').addClass('hidden');
        }
</script>
<div class="popup-check hidden">
<div class="wrapper_model" style="">
<div class="modal fade alert-box alert in" tabindex="-1" role="dialog" style="display: block; padding-right: 17px;">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button onclick="close_popup_check();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center alert-content">
                        <p><strong><?php echo __('Bạn phải click chuột để đồng ý với điều khoản sử dụng trước khi trở thành thành viên.');?></strong></p>
                    </div></div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-launch" onclick="close_popup_check();" data-dismiss="modal" aria-label="Close" style="float: none;">ok</button>
                    </div></div></div></div></div></div></div>
<div class="modal-backdrop fade in" ></div>
</div>
