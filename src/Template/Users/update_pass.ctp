<div class="content-signup" style="padding: 20px;">
    <?= $this->Form->create($users) ?>
    <h1><?php echo __('Reset Password'); ?></h1>
        <?= $this->Form->input('password',array('value' => '','class' => 'form-control', 'required'=>true,'placeholder' => __("Password"),'label' => FALSE)) ?>
        <?= $this->Form->input('confirm_password',array('type' => 'password','class' => 'form-control', 'required'=>true,'placeholder' => __("Re-Password"),'label' => FALSE)) ?>
    <button type="submit" class="btn btn-warning btn-login btn-signup">
        <span><?php echo __('Submit'); ?></span>
    </button>
        <?= $this->Form->end() ?>
</div>