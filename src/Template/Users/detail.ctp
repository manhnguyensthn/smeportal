<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="users_form_edit account_detail" id="users_form_edit" style=" border: none;margin-bottom: 50px">
    <div class="content-setting" style="width: 100%;">
        <h2><?php echo __('Settings'); ?></h2>
        <div class="clearfix"></div>

        <?php echo $this->element('Profile/profile_tab'); ?>

        <div class="clearfix"></div>
        <div class="col-lg-12 col-custom">
            <div class="col-md-8">
                <?php if (empty($data['fb_id']) && empty($data['google_id']) && empty($data['linked_id'])): ?>
                    <?= $this->Form->create($data, ['type' => 'file', 'id' => 'Users_ChangeEmail']); ?>
                    <?php echo $this->Form->input('Users.secondary_email', array('div' => false, 'class' => 'form-control', 'label' => __('Thông báo qua email'))); ?>
                    <div class="clearfix"></div>
                    <button type="submit" name="updateEmail" value="1"
                            class="btn btn-warning btn-launch btn-custom pull-right"
                            style="color: #080707;margin: 0px;"><?php echo __('Update Email'); ?></button>
                    <div class="clearfix"></div>
                    <?= $this->Form->end(); ?>
                <?php else: ?>
                    <?php echo $this->Form->input('Users.secondary_email', array('div' => false, 'class' => 'form-control', 'label' => __('Email'), 'readonly' => 'true')); ?>
                    <div class="clearfix"></div>
                    <button type="button" name="updateEmail" value="1"
                            class="btn btn-warning btn-launch btn-custom pull-right"
                            style="color: #080707;margin: 0px;"><?php echo __('Update Email'); ?></button>
                    <div class="clearfix"></div>
                <?php endif; ?>
                <div class="page-header">
                    <h1><?php echo __('Change Password'); ?></h1>
                </div>
                <div class="clearfix"></div>
                <?php if (empty($data['fb_id']) && empty($data['google_id']) && empty($data['linked_id'])): ?>
                    <?= $this->Form->create($data, ['type' => 'file', 'id' => 'Users_ChangePassword']); ?>
                    <div class="input-group-pss">
                        <?php echo $this->Form->input('Users.cf_password', array('autocomplete' => "off", 'div' => false, 'id' => 'passwordCf', 'required' => TRUE, 'type' => 'password', 'class' => 'form-control col-md-7', 'label' => __('Confirm Password'))); ?>
                        <span id="successPass" class="glyphicon glyphicon-ok form-control-feedback"
                              aria-hidden="true"></span>
                        <span id="errorPass" class="glyphicon glyphicon-remove form-control-feedback text-danger"
                              aria-hidden="true" <?php echo isset($data['error_password']) ? 'style="display: block"' : ''; ?> ></span>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <?php echo $this->Form->input('Users.password', array('div' => false, 'value' => isset($data['new_password']) ? $data['new_password'] : '', 'type' => 'password', 'class' => 'form-control newpass', 'label' => __('New Password'))); ?>
                    <?php echo $this->Form->input('Users.confirm_password', array('div' => false, 'type' => 'password', 'class' => 'form-control newpass', 'label' => __('Retype Password'))); ?>
                    <div class="clearfix"></div>
                    <button type="submit" name="updatePass" value="1"
                            class="btn btn-warning btn-launch btn-custom pull-right"
                            style="color: #080707;"><?php echo __('Change Password'); ?></button>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                <?php else: ?>
                    <div class="input-group-pss">
                        <?php echo $this->Form->input('Users.cf_password', array('autocomplete' => "off", 'div' => false, 'id' => 'passwordCf', 'required' => TRUE, 'type' => 'password', 'class' => 'form-control col-md-7', 'label' => __('Confirm Password'), 'readonly' => 'true')); ?>
                        <span id="successPass" class="glyphicon glyphicon-ok form-control-feedback"
                              aria-hidden="true"></span>
                        <span id="errorPass" class="glyphicon glyphicon-remove form-control-feedback text-danger"
                              aria-hidden="true" <?php echo isset($data['error_password']) ? 'style="display: block"' : ''; ?> ></span>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <?php echo $this->Form->input('Users.password', array('div' => false, 'value' => isset($data['new_password']) ? $data['new_password'] : '', 'type' => 'password', 'class' => 'form-control newpass', 'label' => __('New Password'), 'readonly' => 'true')); ?>
                    <?php echo $this->Form->input('Users.confirm_password', array('div' => false, 'type' => 'password', 'class' => 'form-control newpass', 'label' => __('Retype Password'))); ?>
                    <div class="clearfix"></div>
                    <button type="button" name="updatePass" value="1"
                            class="btn btn-warning btn-launch btn-custom pull-right"
                            style="color: #080707;"><?php echo __('Change Password'); ?></button>
                    <div class="clearfix"></div>
                <?php endif; ?>
                <hr style="border-top: 0;">
                <div class="clearfix"></div>

                <!-- <?= $this->Form->create($data, ['type' => 'file', 'id' => 'Users_EditProfile']); ?> -->
                <!-- <div id="paymentArea">
                    <div class="page-header">
                        <h1><?php echo __('Payment Information'); ?></h1>
                    </div>
                    <div class="clearfix"></div>
                    <ul class="payment">
                        <li class="payItem"><?php echo __('We Accept'); ?>:</li>
                        <li class="payItem"><img src="/img/visa.png" class="img-responsive"></li>
                        <li class="payItem"><img src="/img/master.png" class="img-responsive"></li>
                        <li class="payItem"><img src="/img/american.png" class="img-responsive"></li>
                    </ul>
                    <div class="clearfix"></div>
                    <hr style="border-top: 0;">
                    <div class="clearfix"></div>
                    <?php echo $this->Form->input('Users.card_number', array('div' => false, 'class' => 'form-control', 'placeholder' => __("Card Number"), 'label' => FALSE)); ?>
                    <?php echo $this->Form->input('Users.card_owner_name', array('div' => false, 'class' => 'form-control', 'placeholder' => __("Name on Card"), 'label' => FALSE)); ?>
                    <div class="form-group">
                        <?php
                $listMonths = array();
                for ($i = 1; $i <= 12; $i++) {
                    $listMonths[$i] = ($i >= 10) ? $i : '0' . $i;
                }
                ?>
                        <div class="col-md-6 padd-left">
                            <?php
                echo $this->Form->input('Users.expiry_month', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'type' => 'select',
                    'options' => $listMonths,
                    'empty' => __('Expiry Month'),
                ]);
                ?>
                        </div>
                        <?php
                $listYears = array();
                for ($i = date('Y'); $i <= date('Y') + 20; $i++) {
                    $listYears[$i] = $i;
                }
                ?>
                        <div class="col-md-6 padd-right">
                            <?php
                echo $this->Form->input('Users.expiry_year', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'type' => 'select',
                    'options' => $listYears,
                    'empty' => __('Expiry Year'),
                ]);
                ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-2 padd-left">
                            <?php
                echo $this->Form->input('Users.cvv_number', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'maxlength' => 3,
                    'minlength' => 3,
                    'placeholder' => __("CVV")
                ]);
                ?>
                        </div>
                        <div class="col-lg-10">
                            <img src="/img/card_info.png" class="img-responsive" style="height: 45px;" />
                        </div>
                    </div>
                </div> -->
                <!-- </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <button type="submit" name="saveAll" value="1" class="btn btn-warning btn-launch btn-custom pull-right" style="color: #080707;"><?php echo __('Save Changes'); ?></button>
            </div> -->
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var passwordcf = "";
        $("#passwordCf").focusout(function () {
            passwordcf = $("#passwordCf").val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo ROOT_URL . 'users/checkP' ?>',
                data: {
                    p: passwordcf
                },
                success: function (data) {
                    if (data.ret === "OK") {
                        $("#successPass").css("display", "block");
                        $("#errorPass").hide();
                    } else {
                        $("#successPass").css("display", "none");
                        $("#errorPass").show();
                    }
                }
            });
        });
    </script>
