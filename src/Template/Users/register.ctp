<style>
.content{
  margin-top:-20px ;
}
.content-signup{
  border: 1px solid #c7c8ca;
  border-radius: 8px;
  margin-left: 55px;
}
.content-signup input{
    background-color: #e7e8e9;
    border-radius: 10px;
    display: inline-block;
}
.checkbox-signup input{
    height: 18px; float: left;
}

</style>
<div class="content">
    <div class="content-signup" >
        <div class="content-signup-continue">
        <h1><?php echo __('Sign up'); ?></h1>
        <?= $this->Form->create($data) ?>
        <div style="display:none;"> <input type="hidden" name="_method" value="POST"></div>       
        <div class="input text required"><input type="text" name="first_name" class="form-control" placeholder="Họ"  maxlength="20" id="first_name"><div class="error-message" id="error-message-first-name"></div></div>        
        <div class="input text required"><input type="text" name="last_name" class="form-control" placeholder="Tên"  maxlength="20" id="last_name"><div class="error-message" id="error-message-last-name"></div></div>        
        <div class="input email required"><input type="email" name="email" class="form-control" placeholder="Email"  maxlength="100" id="email"><div class="error-message" id="error-message-email"></div></div>        
        <div class="input text required"><input type="text" name="confirm_email" class="form-control" placeholder="Lặp lại email"  id="confirm_email"><div class="error-message" id="error-message-confirm-email"></div></div>        
        <div class="input password required"><input type="password" name="password" class="form-control" placeholder="Mật khẩu"  id="password"><div class="error-message" id="error-message-password"></div></div>        
        <div class="input password required"><input type="password" name="confirm_password" class="form-control"  placeholder="Lặp lại mật khẩu" id="confirm_password"><div class="error-message" id="error-message-confirm-password"></div></div>        
        <div class="checkbox-login checkbox-signup">
            <label>
                <input  name="receive_newletter" type="checkbox" value="0" ><span><?php echo __('Receive our weekly updates and newsletters') ?></span>
            </label>
        </div>
        <button type="submit" class="btn btn-warning btn-login btn-signup btn-continue" >
            <span><?php echo __('Tiếp tục'); ?></span>
        </button>
        </div>
        <div class="content-signup-submit hidden">
        <h1><?php echo $termofuse->title; ?></h1>
        <p class="note-re"><b><?php echo __('Bạn cần cuộn hết nội dung để tiếp tục.'); ?></b></p>
        <?= $this->Form->create($data) ?>        
        <div class="content-term content-term-submit hidden">
               <?= $this->element('term_of_user') ?>
        </div>
        <div class="content-term content-term-fb  hidden">
               <?= $this->element('term_of_user') ?>
        </div>
        <div class="checkbox-login checkbox-signup-submit hidden">
            <label>
                <input name="receive_newletter" id="checkbox-submit" type="checkbox" value="0" ><span style="width:420px"><?php echo __('Tôi đã đọc và đồng ý với tất cả các điều khoản và điều kiện ở trên.') ?></span>
            </label>
        </div>
        <button type="submit" class="btn btn-warning btn-login btn-signup-submit hidden" >
            <span><?php echo __('Become a member'); ?></span>
        </button>
        <div class="checkbox-login checkbox-signup-fb hidden">
            <label>
                <input name="receive_newletter" id="checkbox-submit-fb" type="checkbox" value="0" ><span style="width:420px"><?php echo __('Tôi đã đọc và đồng ý với tất cả các điều khoản và điều kiện ở trên.') ?></span>
            </label>
        </div>
        <button type="button" class="btn btn-warning btn-login btn-signup-fb hidden" >
            <span><?php echo __('Become a member'); ?></span>
        </button>    
        </div>    
        <?= $this->Form->end() ?>
        <hr class="hr-signup">
        <div class="btn-group-sns">
            <a id="btn-1" href="javascript:void(0)" class="btn btn-block btn-social btn-facebook">
                <span class="fa fa-facebook-official"></span>
                <?php echo __("Log in with Facebook") ?> <i class="fa fa-spinner fa-spin" style="font-size:24px; margin-left: 20px; display: none"></i>
            </a>
            <a id="btn-2" href="javascript:void(0)" onclick="javascript:googleAuth()" class="btn btn-block btn-social btn-google">
                <span class="fa fa-google-plus"></span><?php echo __(" Log in with Google +") ?> <i class="fa fa-spinner fa-spin" style="font-size:24px; margin-left: 20px; display: none"></i>
            </a>
        </div>
        <h5><?php echo __("Chúng tôi sẽ không đăng gì khi chưa có sự đồng ý của bạn") ?></h5>
        <hr class="hr-signup">
        <div class="signup-login login">
            <span><?php echo __("Already a member?") ?></span><a href="/users/login"> <?php echo __("Log in") ?></a>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.checkbox-signup input[name=receive_newletter]').change(function () {
            var receive_status = $(this).val();
            if (receive_status == '1') {
                $(this).val('0');
            } else {
                $(this).val('1');
            }
        });
    });
    //validate register
    $('.btn-signup').click(function () {
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();
        var confirm_email = $('#confirm_email').val();
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        var re = /(?=.{8,16})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/;
        document.getElementById('error-message-first-name').innerHTML = '';
        document.getElementById('error-message-last-name').innerHTML = '';
        document.getElementById('error-message-email').innerHTML = '';
        document.getElementById('error-message-confirm-email').innerHTML = '';
        document.getElementById('error-message-password').innerHTML = '';
        document.getElementById('error-message-confirm-password').innerHTML = '';
        var chaos = new Array ("'","~","@","#","$","%","^","&","*",";","/","\\","|");
        var sum = chaos.length;
        for (var i in chaos) {if (!Array.prototype[i]) {sum += password.lastIndexOf(chaos[i])}}
        if (first_name == '') {
            document.getElementById('error-message-first-name').innerHTML = "<?php echo __('Trường này không được phép để trống.'); ?>";
            $('#first_name').focus();
            return false;
        } else if (last_name == '') {
            document.getElementById('error-message-last-name').innerHTML = "<?php echo __('Trường này không được phép để trống.'); ?>";
            $('#last_name').focus();
            return false;
        } else if (email == '') {
            document.getElementById('error-message-email').innerHTML = "<?php echo __('Trường này không được phép để trống.'); ?>";
            $('#email').focus();
            return false;
        } else if (confirm_email == '') {
            document.getElementById('error-message-confirm-email').innerHTML = "<?php echo __('Trường này không được phép để trống.'); ?>";
            $('#confirm_email').focus();
            return false;
        } else if (password == '') {
            document.getElementById('error-message-password').innerHTML = "<?php echo __('Trường này không được phép để trống.'); ?>";
            $('#password').focus();
            return false;
        } else if (confirm_password == '') {
            document.getElementById('error-message-confirm-password').innerHTML = "<?php echo __('Trường này không được phép để trống.'); ?>";
            $('#confirm_password').focus();
            return false;
        } else if(first_name.length >20) {
            document.getElementById('error-message-first-name').innerHTML = "<?php echo __('Họ phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode.'); ?>";
            $('#first_name').focus();
            return false;
        } else if(last_name.length >20) {
            document.getElementById('error-message-last-name').innerHTML = "<?php echo __('Tên phải từ 1-20 ký tự. Bao gồm A-Z, a-z. khoảng trắng, 0-9. Chấp nhận ký tự unicode.'); ?>";
            $('#last_name').focus();
            return false;
        } else if(!validateEmail(email)) {
            document.getElementById('error-message-email').innerHTML = "<?php echo __('Định dạng email không hợp lệ. Phải là, ví dụ: john.smith@gmail.com, nakatajim@fuji.co.jp.'); ?>";
            $('#email').focus();
            return false;
        } else if(!validateEmail(confirm_email)) {
            document.getElementById('error-message-confirm-email').innerHTML = "<?php echo __('Định dạng email không hợp lệ. Phải là, ví dụ: john.smith@gmail.com, nakatajim@fuji.co.jp.'); ?>";
            $('#confirm_email').focus();
            return false;
        } else if(!re.test(password) || sum) {
            document.getElementById('error-message-password').innerHTML = "<?php echo __('Mật khẩu phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.'); ?>";
            $('#password').focus();
            return false;
        } else if(!re.test(confirm_password)) {
            document.getElementById('error-message-confirm-password').innerHTML = "<?php echo __('Mật khẩu nhập lại phải từ 8-16 ký tự, bao gồm các chữ cái A-Z, a-z, 0-9 và không chứa dấu cách. Bắt buộc có 1 chữ hoa, 1 chữ số.'); ?>";
            $('#confirm_password').focus();
            return false;
        } else if(email !== confirm_email) {
            document.getElementById('error-message-confirm-email').innerHTML = "<?php echo __('Email nhập lại không trùng khớp với email.'); ?>";
            $('#confirm_email').focus();
            return false;
        } else if(password !== confirm_password) {
            document.getElementById('error-message-confirm-password').innerHTML = "<?php echo __('Mật khẩu nhập lại không trùng khớp với mật khẩu nhẩu.'); ?>";
            $('#confirm_password').focus();
            return false;
        } else {
               $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '<?php echo ROOT_URL . 'users/Checkmail' ?>',
                    data: {
                        email: email
                    },
                    success: function (result) {  
                        if(result == 1) {
                           document.getElementById('error-message-email').innerHTML = "<?php echo __('Email này đã đăng ký.'); ?>";
                           $('#email').focus();
                           return false;
                        } else {
                           $('#check_register').val(1);
                           $('.content-signup-continue').addClass('hidden');
                           $('.content-signup-submit').removeClass('hidden');
                           $('.content-term-submit').removeClass('hidden');
                        }
                    }
                });
        }
        return false;
    });
    // End validate register
    // Close popup
    function close_popup_login()
         {
                 window.location = '/users/login';
         }
    // Show checkbox register     
    $('.content-term-submit').scroll(function() {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 20) {
            $('.checkbox-signup-submit').removeClass('hidden'); 
          }
    }); 
    // Show checkbox register fb, g+,    
    $('.content-term-fb').scroll(function() {
         if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 20) {
            $('.checkbox-signup-fb').removeClass('hidden'); 
          } 
    }); 
    // Check exist email
   function pushUsersInfo(name, first_name, last_name, email, avatar, sns_id, sns_type) {   
          $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '<?php echo ROOT_URL . 'users/Checkmail' ?>',
                    data: {
                        email: email
                    },
                    success: function (result) {
                        if(result == 0) {
                           AddUserInfo(name, first_name, last_name, email, avatar, sns_id, sns_type);
                        } else {
                            $('.modal-backdrop').show();
                            $('.wrapper_model').show();
                            return false;
                        }
                    }
                });
                return false;
    } 
    // handing click button become a member for fb, g+, linkedin 
     $('.btn-signup-fb').click(function(){
              var check_type = $('#check_type').val();
              if(check_type === 'fb') {
                   $( "#btn-1.btn-facebook" ).trigger( "click" );
              } else if (check_type === 'gg') {
                  googleAuth();
              } else {
                  liAuth();
              }
     });
     // Add user and login   
     function AddUserInfo(name, first_name, last_name, email, avatar, sns_id, sns_type) { 
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '<?php echo ROOT_URL . 'users/ajaxSNS' ?>',
                    data: {
                        name: name,
                        first_name: first_name,
                        last_name: last_name,
                        email: email,
                        sns_id: sns_id,
                        sns: sns_type,
                        avatar: avatar
                    },
                    beforeSend: function (xhr) {
                        $("#btn-" + sns_type + " i").show();
                    },
                    success: function (data) {
                        if (data.ret === "OK") {
                            window.location = "<?php echo ROOT_URL; ?>";
                        } else {
                            $("#btn-" + sns_type + " i").hide();
                            $('.modal-backdrop').show();
                            $('.wrapper_model').show();
                            return false;
                        }
                    }
                });
                return false;
            }
     // handing event click register fb 
      var loop = 0;
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '<?php echo FB_APP_ID; ?>',
                    xfbml: true,
                    version: '<?php echo FB_APP_VERSION ?>'
                });
                $("#btn-1.btn-facebook").click(function () {
                var check_register = $("#check_register").val(); 
                if(check_register == 1) {
                    return false;
                }   
                $("#check_type").val('fb'); 
                if(loop == 0 ) {
                       $('.content-signup-continue').addClass('hidden');
                       $('.content-signup-submit').removeClass('hidden');
                       $('.content-term-fb').removeClass('hidden');
                       loop = 1;
                       return false;
                }
                var check = $("#checkbox-submit-fb").is(":checked"); 
                if(check == true) {
                } else {
                   $('.popup-check').removeClass('hidden');
                   return false;
                }
                FB.getLoginStatus(function (response) {
                        if (response.status === 'connected') {
                            FB.api('/me', {locale: 'en_US', fields: 'name, email, last_name, first_name'},
                                    function (response) {
                                        var name = response.name;
                                        var first_name = response.first_name;
                                        var last_name = response.last_name;
                                        var email = response.email;
                                        var fb_id = response.id;
                                        var avatar = "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                                        pushUsersInfo(name, first_name, last_name, email, avatar, fb_id, 1);
                                    }
                            );
                        } else {
                            FB.login(function (response) {
                                if (response.authResponse) {
                                    FB.api('/me', {locale: 'en_US', fields: 'name, email, first_name, last_name'},
                                            function (response) {
                                                var name = response.name;
                                                var first_name = response.first_name;
                                                var last_name = response.last_name;
                                                var email = response.email;
                                                var fb_id = response.id;
                                                var avatar = "https://graph.facebook.com/" + fb_id + "/picture?type=large";
                                                pushUsersInfo(name, first_name, last_name, email, avatar, fb_id, 1);
                                            }
                                    );
                                } else {
                                    return false;
                                }
                            }, {
                                scope: 'email,user_friends',
                                return_scopes: true
                            });
                        }
                    });
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
//    login with linkedin
            function displayProfiles(profiles) {
                var member = profiles.values[0];
                var name = member.firstName + member.lastName;
                var first_name = member.firstName;
                var last_name = member.lastName;
                var email = member.emailAddress;
                var linked_id = member.id;
                var avatar = member.pictureUrl;
                pushUsersInfo(name, first_name, last_name, email, avatar, linked_id, 3);
            }
            function liAuth() {
                var check_register = $("#check_register").val(); 
                if(check_register == 1) {
                    return false;
                }  
                $("#check_type").val('li'); 
                if(loop == 0 ) {
                       $('.content-signup-continue').addClass('hidden');
                       $('.content-signup-submit').removeClass('hidden');
                       $('.content-term-fb').removeClass('hidden');
                       loop = 1;
                       return false;
                }
                var check = $("#checkbox-submit-fb").is(":checked"); 
                if(check == true) {
                } else {
                   $('.popup-check').removeClass('hidden');
                   return false;
                }
                IN.User.authorize(function () {
                    IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "picture-url").result(displayProfiles);
                });
            }
//    login with google
            function googleAuth() {
                var check_register = $("#check_register").val(); 
                if(check_register == 1) {
                    return false;
                }   
                $("#check_type").val('gg'); 
                if(loop == 0 ) {
                       $('.content-signup-continue').addClass('hidden');
                       $('.content-signup-submit').removeClass('hidden');
                       $('.content-term-fb').removeClass('hidden');
                       loop = 1;
                       return false;
                }
                var check = $("#checkbox-submit-fb").is(":checked"); 
                if(check == true) {
                } else {
                   $('.popup-check').removeClass('hidden');
                   return false;
                }
                gapi.auth.signIn({
                    'clientid': '<?php echo GOOGLE_OAUTH_CLIENT_ID; ?>',
                    'cookiepolicy': "<?php echo ROOT_URL; ?>",
                    'approvalprompt': 'auto',
                    'prompt': 'consent',
                    'display': 'page',
                    'access_type': 'offline',
                    'requestvisibleactions': "http://schemas.google.com/AddActivity",
//            scope: "https://www.googleapis.com/auth/plus.login email"
                    'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read',
                    'callback': gPSignInCallback,
                });
            }
            function gPSignInCallback(e) {
//                console.log(e);
//        return false;
                if (e["status"]["signed_in"]) {
                    gapi.client.load("plus", "v1", function () {
						//console.log(e);
						//return false;			
                        if (e["access_token"]) {
							 if (!e['_aa']) {
                                getProfile();
							 }
                        } else if (e["error"]) {
//                            console.log("There was an error: " + e["error"]);
                        }
                    });
                } else {
//                    console.log("Sign-in state: " + e["error"]);
                }
            }
            function getProfile() {

                var req = gapi.client.plus.people.get({
                    userId: "me"
                });
			
                req.execute(function (res) {
		
                    //console.log(res);
            //return false;
                    if (res.error) {
                        return
                    } else if (res.id) {
						
                        var email = res['emails'].filter(function (v) {
                            return v.type === 'account';
                        })[0].value;
				
//                console.log(e);
                        var name = res.displayName;
                        var google_id = res.id;
                        var first_name = res.name.familyName;
                        var last_name = res.name.givenName;
                        var imageUrl = res.image.url;
                        var avatar = imageUrl.replace("photo.jpg?sz=50", "photo.jpg?sz=150");
                        pushUsersInfo(name, first_name, last_name, email, avatar, google_id, 2);
                        return;
                    }
                });
            }        
    document.getElementById('checkbox-submit').onclick = function(e){
                if (this.checked){
                    $('.btn-signup-submit').removeClass('hidden');
                }
                else{
                    $('.btn-signup-submit').addClass('hidden');
                }
            };       
     document.getElementById('checkbox-submit-fb').onclick = function(e){
                if (this.checked){
                    $('.btn-signup-fb').removeClass('hidden');
                }
                else{
                    $('.btn-signup-fb').addClass('hidden');
                }
            };        
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
function alertBoxLogin(Message){
                ok = "<?php echo __('ok');?>";
                var sHtml = '';
                sHtml += '<div class="modal fade alert-box alert" tabindex="-1" role="dialog">';
                        sHtml += '<div class="modal-dialog" role="document">';
                                sHtml += '<div class="modal-content">';
                                        sHtml += '<div class="modal-header">';
                                                sHtml += '<button onclick="close_popup();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Ã—</span></button>';
                                                sHtml += '<h4 class="modal-title" id="myModalLabel"></h4>';
                                        sHtml += '</div>';
                                        sHtml += '<div class="modal-body">';
                                                sHtml += '<div class="row">';
                                                        sHtml += '<div class="col-xs-12 text-center alert-content"><p><strong>' + Message + '</strong></p></div>';
                                                sHtml += '</div>';
                                                sHtml += '<div class="row">';
                                                        sHtml += '<div class="col-xs-12 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close" style="float: none;" >'+ok+'</button></div>';
                                                sHtml += '</div>';
                                        sHtml += '</div>';
                                sHtml += '</div>';
                        sHtml += '</div>';
                sHtml += '</div>';

                $('.wrapper_model').html(sHtml);
                $('.alert-box.alert').modal();
                $('.alert-box.alert').on('shown.bs.modal', function(e) {
                });
                $('.alert-box.alert').on('hidden.bs.modal', function(e) {
                        $('body').css({ 'padding-right' : '0px' });
                });
        }
        function close_popup_check() {
            $('.popup-check').addClass('hidden');
        }
</script>
<input type="hidden" id="check_register" value="0">
<input type="hidden" id="check_type" value="0">
<div class="popup-check hidden">
<div class="wrapper_model" style="">
<div class="modal fade alert-box alert in" tabindex="-1" role="dialog" style="display: block; padding-right: 17px;">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button onclick="close_popup_check();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center alert-content">
                        <p><strong><?php echo __('Bạn phải click chuột để đồng ý với điều khoản sử dụng trước khi trở thành thành viên.');?></strong></p>
                    </div></div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-launch" onclick="close_popup_check();" data-dismiss="modal" aria-label="Close" style="float: none;">ok</button>
                    </div></div></div></div></div></div></div>
<div class="modal-backdrop fade in" ></div>
</div>