<style>
    .content {
        margin-top: -20px;
    }

    .content-signup {
        border: 1px solid #c7c8ca;
        border-radius: 8px;
        margin-left: 80px;
    }
    .content-signup h1{
        margin-left: 60px;
    }

    .content-signup input {
        margin-left: 35px;
        margin-bottom: -20px;
        height: 50px;
        border: 1px solid #c7c8ca;
        background-color: #e7e8e9;
        border-radius: 10px;
        display: inline-block;
        width: 75%;
    }
    .content-signup button{
        margin-bottom: 10px;
    }
</style>
<div class="container">
    <div class="col-md-3"></div>
    <div class="col-md-4 content-signup">
        <?= $this->Form->create('Users', ['id' => 'forgotPass']) ?>
        <h1><?php echo __('Quên mật khẩu'); ?></h1>
        <?= $this->Form->input('email', array('type' => 'text', 'class' => 'form-control', 'required' => true, 'placeholder' => __("Email"), 'label' => FALSE)) ?>
        <center>
            <i id="loading" class="fa fa-spinner fa-spin" style="font-size:100px; margin-left: 20px; display: none"></i>
        </center>
        <button type="submit" id="btnForgot" class="btn btn-warning btn-login btn-signup">
            <span><?php echo __('Gửi yêu cầu'); ?></span>
        </button>
        <?= $this->Form->end() ?>
    </div>
    <script type="text/javascript">
        $("#btnForgot").click(function () {
            $("#loading").show();
            $(this).hide();
        });
    </script>
    <div class="col-md-4"></div>
</div>