<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="users_form_edit" id="users_form_edit" style=" border: none;margin-bottom: 50px">
  <div class="content-setting" style="width: 100%;">
    <h2>Settings</h2>
    <div class="clearfix"></div>
    
    <?php echo $this->element('Profile/profile_tab'); ?>

    <div class="clearfix"></div>
    <?= $this->Form->create($data) ?>
    <div class="col-lg-12 col-custom">
      <div class="col-md-8">
        <div class="table-responsive"> 
          <table id="notifications" class="table"> 
            <thead> 
              <tr style="white-space: nowrap;"> 
                <th><?php echo __('Social Notifications'); ?></th> 
                <th><?php echo __('Email'); ?></th>
                <th><?php echo __('Mobile'); ?></th> 
              </tr> 
            </thead> 
            <tbody> 
              <tr> 
                <td class="textNtf"> 
                  <span><?php echo __('Replies to my comments'); ?></span>
                </td>
                <td width="70" class="text-center iconMail <?php echo empty($data['replies_comment_email_status']) ? 'gray' : 'yellow'; ?> notification-status" data="replies_comment_email_status" >
                  <?php echo $this->Form->input('replies_comment_email_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mail"></label>
                </td> 
                <td width="70" class="text-center iconMobile <?php echo empty($data['replies_comment_mobile_status']) ? 'gray' : 'yellow'; ?> notification-status" data="replies_comment_mobile_status" >
                  <?php echo $this->Form->input('replies_comment_mobile_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mobile"></label>
                </td> 
              </tr> 
              <tr> 
                <td class="textNtf"> 
                  <span><?php echo __('Private Messages'); ?></span>
                </td> 
                <td width="70" class="text-center iconMail <?php echo empty($data['private_message_email_status']) ? 'gray' : 'yellow'; ?> notification-status" data="private_message_email_status" >
                  <?php echo $this->Form->input('private_message_email_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mail"></label>
                </td> 
                <td width="70" class="text-center iconMobile <?php echo empty($data['private_message_mobile_status']) ? 'gray' : 'yellow'; ?> notification-status" data="private_message_mobile_status">
                  <?php echo $this->Form->input('private_message_mobile_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mobile"></label>
                </td> 
              </tr>
              <tr> 
                <td class="textNtf"> 
                  <span><?php echo __('Group Messages'); ?></span>
                </td>
                <td width="70" class="text-center iconMail <?php echo empty($data['group_message_email_status']) ? 'gray' : 'yellow'; ?> notification-status" data="group_message_email_status">
                  <?php echo $this->Form->input('group_message_email_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mail"></label>
                </td>
                <td width="70" class="text-center iconMobile <?php echo empty($data['group_message_mobile_status']) ? 'gray' : 'yellow'; ?> notification-status" data="group_message_mobile_status">
                  <?php echo $this->Form->input('group_message_mobile_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mobile"></label>
                </td> 
              </tr> 
              <tr> 
                <td class="textNtf"> 
                  <span><?php echo __('New Followers'); ?></span>
                </td>
                <td width="70" class="text-center iconMail <?php echo empty($data['new_follower_email_status']) ? 'gray' : 'yellow'; ?> notification-status" data="new_follower_email_status">
                  <?php echo $this->Form->input('new_follower_email_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mail"></label>
                </td>
                <td width="70" class="text-center iconMobile <?php echo empty($data['new_follower_mobile_status']) ? 'gray' : 'yellow'; ?> notification-status" data="new_follower_mobile_status">
                  <?php echo $this->Form->input('new_follower_mobile_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mobile"></label>
                </td>
              </tr> 
            </tbody> 
          </table> 
          <hr style="border-color: transparent">
          <table id="notifications" class="table"> 
            <thead> 
              <tr style="white-space: nowrap;"> 
                <th><?php echo __('Project Notifications'); ?></th> 
                <th style="visibility: hidden"><?php echo __('Email'); ?></th>
                <th style="visibility: hidden"><?php echo __('Mobile'); ?></th> 
              </tr> 
            </thead> 
            <tbody> 
              <tr> 
                <td class="textNtf"> 
                  <span><?php echo __('Project Updates'); ?></span>
                </td>
                <td width="70" class="text-center iconMail <?php echo empty($data['project_update_email_status']) ? 'gray': 'yellow'; ?> notification-status" data="project_update_email_status">
                  <?php echo $this->Form->input('project_update_email_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mail"></label>
                </td> 
                <td width="70" class="text-center iconMobile <?php echo empty($data['project_update_mobile_status']) ? 'gray' : 'yellow'; ?> notification-status" data="project_update_mobile_status">
                  <?php echo $this->Form->input('project_update_mobile_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mobile"></label>
                </td> 
              </tr> 
              <tr> 
                <td class="textNtf"> 
                  <span><?php echo __('Followed Member joins a project'); ?></span>
                </td> 
                <td width="70" class="text-center iconMail <?php echo empty($data['member_join_project_email_status']) ? 'gray' : 'yellow'; ?> notification-status" data="member_join_project_email_status">
                  <?php echo $this->Form->input('member_join_project_email_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mail"></label>
                </td> 
                <td width="70" class="text-center iconMobile <?php echo empty($data['member_join_project_mobile_status']) ? 'gray' : 'yellow'; ?> notification-status" data="member_join_project_mobile_status">
                  <?php echo $this->Form->input('member_join_project_mobile_status', ['type' => 'hidden','class'=>'notification_status']); ?>
                  <label class="label label_mobile"></label>
                </tr> 
              </tbody> 
            </table> 
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
          <button type="submit" class="btn btn-warning btn-launch btn-custom pull-right" style="color: #080707;"><?php echo __('Save Changes'); ?></button>
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function () {
      $('#notifications .notification-status').click(function () {
        var data_id = $(this).attr('data');
        var current_status = $(this).find('input').val();
        if (current_status == '1') {
          $(this).find('input').val('0');
          $(this).addClass('gray');
          $(this).removeClass('yellow');
        } else {
          $(this).find('input').val('1');
          $(this).addClass('yellow');
          $(this).removeClass('gray');
        }
      });
    });
  </script>