<?php

use Cake\Routing\Router; ?>

<div class="users_form_edit" id="users_form_edit" style=" border: none;margin-bottom: 50px">
    <div class="content-setting" style="width: 100%;">
        <h2><?php echo __('Settings'); ?></h2>
        <div class="clearfix"></div>
        <div class="menu-child">
            <ul class="ulMenu">
                <li class="menuLink active"><a href="/users/profile"><?php echo __('Edit Profile'); ?></a></li>
                <li class="menuLink "><a href="/users/detail"><?php echo __('Account Details'); ?></a></li>
                <li class="menuLink "><a href="/users/notifications"><?php echo __('Notifications'); ?></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="col-custom col-lg-12">
            <?= $this->Form->create($user, ['type' => 'file', 'id' => 'Users_EditProfile']) ?>
            <div class="input text col-md-7">
                <?php echo $this->Form->input('Users.first_name', array('div' => false, 'class' => 'form-control', 'label' => __('First Name'))); ?>
            </div>
            <div class="input text col-md-7">
                <?php echo $this->Form->input('Users.last_name', array('div' => false, 'class' => 'form-control', 'label' => __('Last Name'))); ?>
            </div>
            <div class="input text col-md-7" style="position: relative;">
                <label for="user-avatar"><?php echo __('Profile Picture'); ?></label>
                <div class="clearfix"></div>
                <div class="avatarArea">
                    <?php echo $this->Html->image($avatarURL, ['id' => 'Users_ImageProfile', 'alt' => 'User Avatar', 'fullBase' => true, 'width' => '30%', 'height' => '30%']); ?>
                    <?php echo $this->Form->input('Users_CurrentImage', ['type' => 'hidden', 'value' => $avatarURL]); ?>
                    <div class="clearfix"></div>
                    <button class="btn btn-warning" id="Users_ImageProfile_Cancel" type="button" style="color: #080707;">×</button>
                </div>
                <div class="btn btn-default btn-file">
                    <div class="centered-text">
                        <span><?= __('Drag Picture Here') ?></span>
                        <div class="clearfix"></div>
                        <span><?= __('Or') ?></span>
                        <div class="clearfix"></div>
                    </div>
                    <input type="file" name="Users[avatar]" class="custom_file" maxlength="100" id="users-avatar" value="">
                    <input type="hidden" name="Users[external_avatar_url]" id="users-external-avatar-url" value="">
                </div>

                <div class="row" style="position: absolute; width: 100%; bottom: 30px;">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <button type="button" id="Users_ImageProfile_UploadFromComputer" style="width: 100%" class="btn btn-warning btn-yellow-shadow"><?= __('Upload from computer') ?></button>
                    </div>
                    <div class="col-md-4">
                        <button type="button" id="Users_ImageProfile_ExternalURL" style="width: 100%" class="btn btn-warning btn-yellow-shadow"><?= __('External URL') ?></button>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="group-location col-md-7">
                <?php
                echo $this->Form->input('Countries', [
                    'class' => 'form-control',
                    'label' => __('Location'),
                    'type' => 'select',
                    'options' => $countries,
                    'empty' => __('-- Select Country --'),
                    'default' => $user->country_id,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]);

                echo $this->Form->input('States', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'type' => 'select',
                    'options' => $states,
                    'empty' => __('-- Select Province/State --'),
                    'default' => ($user->state_id == null && $user->country_id > 0) ? -1 : ($user->country_id == 0 ? 0 : $user->state_id),
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-right {{type}}{{required}}"><label for="states" style="color:transparent">.</label>{{content}}</div>'
                    ]
                ]);

                echo $this->Form->input('Districts', [
                    'class' => 'form-control',
                    'label' => FALSE,
                    'type' => 'select',
                    'options' => $districts,
                    'empty' => __('-- Select City --'),
                    'default' => $user->district_id,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]);

                echo $this->Form->input('Users.postal_code', ['class' => 'form-control', 'label' => FALSE,
                    'placeholder' => __('Postal Code'), 'readonly' => TRUE, 'value' => $districtPostalCode,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-6 input padd-right {{type}}{{required}}">{{content}}</div>'
                    ]]
                );
                ?>
                <div class="clearfix" style="margin-bottom: 30px;"></div>
                <?php
                echo $this->Form->input('Roles.role0', array(
                    'class' => 'form-control', 'label' => __('Choose your role'),
                    'type' => 'select', 'options' => $roles, 'empty' => '-- Select Role 1 --', 'default' => $role1->role_id
                ));
                echo $this->Form->input('Roles.role1', array(
                    'class' => 'form-control', 'label' => FALSE,
                    'type' => 'select', 'options' => $roles, 'empty' => '-- Select Role 2 --', 'default' => $role2->role_id
                ));
                ?>
            </div>
            <div class="clearfix" style="margin-bottom: 20px;"></div>
            <div class="input texarea col-md-9">
                <?php
                echo '<label for="biography">Biography</label>';
                echo $this->Form->textarea('Users.biography', array('maxlength' => 500, 'class' => 'form-control', 'rows' => 7, 'label' => __('Biography')));
                ?>
                <div id="Users_EditProfile_BioRemainCharCount" ><?php echo $bioRemainCharCounter ?></div>
            </div>

            <div class="clearfix"></div>
            <div class="input text col-md-10" style="margin-bottom: 0px;">
                <label for="website"><?php echo __('Websites'); ?></label>
            </div>
            <div class="input text col-md-7" style="margin-bottom: 0px;">
                <input id="webinfo" type="text" class="form-control">
            </div>
            <div class="input text col-md-2" style="margin-bottom: 16px;">
                <button id="addweb" class="btn btn-warning btn-yellow-shadow" type="button" style="color: #080707; width:100%;">add</button>
            </div>

            <div class="clearfix"></div>
            <div class="input text col-md-7">
                <div id="websiteInfo">
                    <?php $count = 1 ?>
                    <?php foreach ($websites as $website): ?>
                        <div class="alert alert-default" role="alert">
                            <?php echo $website->website->name ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <input type="hidden" name="UsersWebsite[no<?php echo $count ?>]" value="<?php echo $website->website_id ?>">
                        </div>
                        <?php ++$count ?>
                    <?php endforeach ?>
                </div>
                <input type="hidden" name="deletedWebsites" id="deletedWebsites" value="">
            </div>

            <div class="clearfix"></div>
            <div class="input text col-lg-12">
                <label for="user-portfolio"><?php echo __('Thành tích'); ?></label>
                <div class="clearfix"></div>
                <div class="row" id="list-works">
                    <?php foreach ($works as $work): ?>
                        <div class="col-md-3 vItem">
                            <div class=" itemSolid">
                                <?php if (strpos($work->work_url, rtrim(Router::url($this->request->webroot, true), '/')) !== false) { ?>
                                    <video class="videoItem" controls>
                                        <source src="<?php echo $work->work_url; ?>" >
                                        <?php echo __('Your browser does not support the video tag.'); ?>
                                    </video>
                                <?php } else { ?>
                                    <iframe class="videoItem" src="<?php echo $work->work_url; ?>" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen>
                                    </iframe>
                                <?php } ?>
                                <span><?php echo $work->work_title; ?></span>
                            </div>
                        </div>
                    <?php endforeach ?>
                    <div class="col-md-3 vItem" data-toggle="modal" data-target="#myModal">
                        <div class=" btn-add-video">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="clearfix"></div>
            <button type="submit" class="btn btn-warning pull-right" ><?php echo __('Save Changes'); ?></button>

            <!-- User Work Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><?= __('Thêm thành tích') ?></h4>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <input type="hidden" name="UsersWork[external_video_url]" id="userswork-external-video-url" value="">
                                </div>
                                <div class="row" style="position: absolute;width: 100%;top: 100px;">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <button id="UsersWork_ExternalURL" type="button" style="width:100%" class="btn btn-warning btn-yellow-shadow"><?= __('External URL') ?></button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userswork-title"><?php echo __('Title'); ?></label>
                                <input type="text" name="UsersWork[title]" maxlength="25" class="form-control" id="userswork-title">
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <label for="description"><?= __('Description') ?></label>
                                <?php echo $this->Form->textarea('UsersWork.description', ['class' => 'form-control', 'label' => 'Description', 'id' => 'userswork-description']) ?>
                            </div>
                        </div>
                        <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                            <button id="Users_NewWork_Post" type="button" class="btn btn-warning btn-yellow-shadow" style="width:20%"><?= __('Post') ?></button>
                        </div>

                    </div>
                </div>
            </div>

            <!-- External Avatar URL -->
            <div class="modal fade" id="externalURLModal" tabindex="-1" role="dialog" aria-labelledby="externalURLModalLabel" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header modal-header-custom">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="externalURLModalLabel"><?= __('External URL') ?></h4>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="userswork-title">URL</label>
                                <input type="text" maxlength="255" class="form-control" id="users-external-url">
                                <div class="error-message" id="msgImg"></div>
                            </div>
                        </div>

                        <input type="hidden" id="Users_ExternalURL_Type" value="">
                        <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                            <button id="Users_ExternalURL_Post" type="button" class="btn btn-warning btn-yellow-shadow" style="width:20%"><?= __('Post') ?></button>
                        </div>

                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?php echo $this->Html->script('/js/jquery-loading-overlay/src/loadingoverlay.min'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#countries').on('change', function () {
            var country_id = $('#countries').val();
            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getStates"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#districts');
                    var states = $('#states');
                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var newStates = {"<?= __('-- Select Province/State --') ?>": ""};

                    var stateList = data['data'];

                    if (stateList.length > 0) {
                        for (var idx = 0; idx < stateList.length; ++idx) {
                            newStates[stateList[idx]['States__name']] = stateList[idx]['States__id'];
                        }
                    }

                    states.empty();
                    $.each(newStates, function (key, value) {
                        states.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                    alert('<?= __('Unable to get state list. Please, try again.') ?>');
                }
            });
        });

        $('#states').on('change', function () {
            var country_id = $('#countries').val();
            var state_id = $(this).val();

            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getDistricts"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id,
                    state_id: state_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#districts');
                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var districtList = data['data'];

                    if (districtList.length > 0) {
                        for (var idx = 0; idx < districtList.length; ++idx) {
                            newDistricts[districtList[idx]['Districts__name']] = districtList[idx]['Districts__id'];
                        }
                    }

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                    alert('<?= __('Unable to get city list. Please, try again.') ?>');
                }
            });
        });

        $('#districts').on('change', function () {
            var district_id = $(this).val();

            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getPostalCode"]) ?>',
                type: 'POST',
                data: {
                    district_id: district_id
                },
                dataType: 'json',
                success: function (data) {
                    var postalCode = $('#users-postal-code');

                    if (data.status) {
                        postalCode.val(data.postal_code);
                    } else {
                        postalCode.val("");
                    }
                },
                error: function (e) {
                    alert('<?= __('Unable to get postal code. Please, try again.') ?>');
                }
            });
        });

        $('#Users_EditProfile').on('click', 'div#websiteInfo > div.alert.alert-default > button.close', function () {
            var currentDeletedWebsites = $('#deletedWebsites').val();
            var currentDeletedWebsitesArray = [];
            if (currentDeletedWebsites != '') {
                currentDeletedWebsitesArray = currentDeletedWebsites.split(',');
            }

            var deletedWebsite = $(this).parent().find('input[type="hidden"]');
            var deletedValue = $.trim(deletedWebsite.prop('value'));

            // Dont pay attention to newly added websites
            if (deletedValue != '' && deletedValue == deletedValue.replace(/\D/g, '')) {
                currentDeletedWebsitesArray.push(parseInt(deletedValue));
            }

            if (currentDeletedWebsitesArray.length > 0) {
                $('#deletedWebsites').val(currentDeletedWebsitesArray.join(','));
            } else {
                $('#deletedWebsites').val('');
            }
        })
        function getFileExtension(filename) {
            return filename.split('.').pop();
        }

        function readURL(input) {
            var arr = ['jpg', 'png', 'gif', 'bmp'];
            var ext = getFileExtension(input.files[0].name);
            if ($.inArray(ext, arr) == -1) {
                alert("<?= __('Profile image should be: jpeg, png, gif, bmp with minimum @1024x576.') ?>");
                return;
            }
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onerror = function (e) {
                    $('#Users_ImageProfile').attr('src', $("#users-currentimage").val());
                }
                reader.onloadend = function (e) {
                    if (e.target.result.indexOf('data:image') !== -1) {
                        $('#Users_ImageProfile').attr('src', e.target.result);
                    } else {
                        $('#Users_ImageProfile').attr('src', $("#users-currentimage").val());
                    }
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#Users_ImageProfile').on('error', function () {
            if ($('#Users_ImageProfile').attr('src') != $("#users-currentimage").val()) {
                $('#Users_ImageProfile').attr('src', $("#users-currentimage").val());
            }
        });

        $("#users-avatar").change(function () {
            $("#users-external-avatar-url").val("");
            readURL(this);
        });

        $("#users-video").change(function () {
            var arr = ['mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv'];
            var ext = getFileExtension($(this).val());
            if ($.inArray(ext, arr) == -1) {
                alert('<?= __("Media video should be mov, mpeg, avi, mp4, 3gp, wmv, flv; size of 1-100MB.") ?> ');
                $(this).val("");
                return;
            }
            $('#userswork-external-video-url').val("");
        });

        $("#Users_ImageProfile_Cancel").on("click", function () {
            $("#users-avatar").val("");
            $('#users-external-avatar-url').val("");
            $('#Users_ImageProfile').attr('src', $("#users-currentimage").val());
        });

        $("#Users_EditProfile > div.input.texarea.col-md-9 > textarea").on("cut paste keyup", function () {
            var remainCharsCount = 500 - $(this).val().length;
            if (remainCharsCount < 0) {
                remainCharsCount = 0;
            }
            $("#Users_EditProfile_BioRemainCharCount").text(remainCharsCount);
        });

        $("#Users_NewWork_Post").on('click', function () {
            var linkYoutube = $('#userswork-external-video-url').val();
            var videoId = youtube_parser(linkYoutube);
            var linkYoutubeImage = 'https://www.youtube.com/embed/'+videoId;
            var userWorkTitle = $('#userswork-title').val();
            $.ajax({
                type: 'POST',
                url: '/profile/getWorkPortfolioAjax',
                data: {
                    'linkYoutubeImage': linkYoutubeImage,
                    'work_title': userWorkTitle
                },
                success: function (respone) {
                    $('#list-works').append(respone);
                }
            });
            $('#myModal').modal('toggle');
        });

        $('#Users_ImageProfile_Cancel').css({display: "none"});
        $('.avatarArea img').css({cursor: "pointer"}).mouseenter(function () {
            $(this).css({opacity: "0.5"});
            $('#Users_ImageProfile_Cancel').stop().fadeIn(200);
        }).mouseleave(function () {
            $(this).css({opacity: "1"});
            $('#Users_ImageProfile_Cancel').stop().fadeOut(200);
        });

        $("#Users_ImageProfile_UploadFromComputer").on('click', function () {
            $("#users-avatar").click();
        });

        $("#UsersWork_UploadFromComputer").on('click', function () {
            $("#users-video").click();
        });

        var USER_PROFILE_EXTERNAL_AVARTAR_URL = 0;
        var USER_PROFILE_EXTERNAL_VIDEO_URL = 1;

        $("#UsersWork_ExternalURL").on('click', function () {
            $("#msgImg").text('');
            $("#users-external-url").val("");
            $("#Users_ExternalURL_Type").val(USER_PROFILE_EXTERNAL_VIDEO_URL);
            $("#myModal").modal('hide');
            $("#externalURLModal").modal('show');
        });

        $("#Users_ImageProfile_ExternalURL").on('click', function () {
            $("#msgImg").text('');
            $("#users-external-url").val("");
            $("#Users_ExternalURL_Type").val(USER_PROFILE_EXTERNAL_AVARTAR_URL);
            $("#externalURLModal").modal('show');
        });

        $("#Users_ExternalURL_Post").on('click', function () {
            var externalURLType = $("#Users_ExternalURL_Type").val();
            var externalURL = $("#users-external-url").val() || '';
            var arrImg = ['jpg', 'png', 'gif', 'bmp'];
            // var arrClip = ['mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv'];
            var ext = getFileExtension(externalURL);
            if (externalURL == null ||
                    externalURL === undefined ||
                    typeof externalURL === 'undefined' ||
                    $.trim(externalURL) == "" ||
                    (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL && $.inArray(ext, arrImg) == -1) ||
                    (externalURLType == USER_PROFILE_EXTERNAL_VIDEO_URL && !isYoutubeURL(externalURL))) {
                if (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL) {
                    $("#msgImg").text('Invalid URL');
                } else {
                    $("#msgImg").text('Invalid URL');
                }
                return;
            }

            var checkURL = $("<div>", {
                id: "externalURLCheck",
                css: {
                    "font-size": "18px",
                    "margin-top": "140px"
                },
                text: "<?= __('Checking URL... Please wait.') ?>"
            });
            if (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL) {
                // $('#users-external-avatar-url').val("");
                var img = new Image();
                $.LoadingOverlay('show', {zIndex: 9999, custom: checkURL});
                img.onload = function () {
                    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                        $("#msgImg").text('Invalid URL');
                    } else {
                        $("#users-avatar").val("");
                        $('#Users_ImageProfile').attr('src', this.src);
                        $('#users-external-avatar-url').val(this.src);
                    }
                    $.LoadingOverlay('hide');
                }
                img.onerror = function () {
                    $("#msgImg").text('Invalid URL');
                    $.LoadingOverlay('hide');
                }
                img.src = externalURL;
            } else {
                $.LoadingOverlay('show', {zIndex: 9999, custom: checkURL});
                $.ajax({
                    url: '<?= $this->Url->build([ "controller" => "Users", "action" => "urlExists"]) ?>',
                    type: 'POST',
                    data: {
                        url: externalURL
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.status) {
                            var youtubeId = youtube_parser(externalURL);
                            $("#myModal .btn-file").css('background-image', 'url(https://i.ytimg.com/vi/' + youtubeId + '/hqdefault.jpg)');
                            $('#userswork-external-video-url').val(externalURL);
                            $("#users-video").val("");
                        } else {
                            $("#msgImg").text('Invalid URL');
                        }
                        $.LoadingOverlay('hide');
                        $("#myModal").modal('show');
                    },
                    error: function (e) {
                        $("#msgImg").text('Invalid URL');
                        $.LoadingOverlay('hide');
                        $("#myModal").modal('show');
                    }
                });
            }
            $("#externalURLModal").modal('hide');
        })
    });
</script>