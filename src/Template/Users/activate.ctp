<style>
.content{
  margin-top:-20px ;
}
.content-signup{
  border: 1px solid #c7c8ca;
  border-radius: 8px;
  margin-left: 80px;
}
.content-signup input{
    margin-left: 155px;
    height: 50px;
    background-color: #e7e8e9;
    border-radius: 10px;
    display: inline-block;
    width: 75%;
}
.checkbox-signup input{
    width: 18px;
    margin-left: 30px;
    margin-bottom: 10px;
    height: 18px; float: left;
}
.btn-group-sns{
  margin-left: 55px;
}
</style>
<div class="content-signup" style="padding: 20px; margin-left: 460px;">
    <div class="contain" style="margin-left: 60px">
        <?= $this->Form->create(null) ?>
            <?= $this->Form->input('code',array('type' => 'text','class' => 'form-control', 'style'=>'margin-left:20px','required'=>true,'placeholder' => __("Code"),'label' => FALSE)) ?>
        <center>
            <i id="loading" class="fa fa-spinner fa-spin" style="font-size:100px; margin-left: 20px; display: none"></i>
        </center>
        <button type="submit" id="btnForgot" class="btn btn-warning btn-login btn-signup">
            <span><?php echo __('Activate'); ?></span>
        </button>
            <?= $this->Form->end() ?>
    </div>
</div>
<!-- <script type="text/javascript">
    $("#btnForgot").click(function (){
        $("#loading").show();
       $(this).hide();
    });
</script> -->