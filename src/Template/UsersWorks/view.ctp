<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Work'), ['action' => 'edit', $usersWork->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Work'), ['action' => 'delete', $usersWork->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersWork->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Works'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Work'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersWorks view large-9 medium-8 columns content">
    <h3><?= h($usersWork->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $usersWork->has('user') ? $this->Html->link($usersWork->user->name, ['controller' => 'Users', 'action' => 'view', $usersWork->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Work Title') ?></th>
            <td><?= h($usersWork->work_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Work Description') ?></th>
            <td><?= h($usersWork->work_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Work Url') ?></th>
            <td><?= h($usersWork->work_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Work Thumbnail') ?></th>
            <td><?= h($usersWork->work_thumbnail) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersWork->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($usersWork->created) ?></td>
        </tr>
    </table>
</div>
