<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users Work'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersWorks index large-9 medium-8 columns content">
    <h3><?= __('Users Works') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('work_title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('work_description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('work_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('work_thumbnail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersWorks as $usersWork): ?>
            <tr>
                <td><?= $this->Number->format($usersWork->id) ?></td>
                <td><?= $usersWork->has('user') ? $this->Html->link($usersWork->user->name, ['controller' => 'Users', 'action' => 'view', $usersWork->user->id]) : '' ?></td>
                <td><?= h($usersWork->work_title) ?></td>
                <td><?= h($usersWork->work_description) ?></td>
                <td><?= h($usersWork->work_url) ?></td>
                <td><?= h($usersWork->work_thumbnail) ?></td>
                <td><?= h($usersWork->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersWork->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersWork->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersWork->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersWork->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
