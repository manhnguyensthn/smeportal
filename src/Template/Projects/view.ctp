<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Project'), ['action' => 'edit', $project->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project'), ['action' => 'delete', $project->id], ['confirm' => __('Are you sure you want to delete # {0}?', $project->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Districts'), ['controller' => 'Districts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New District'), ['controller' => 'Districts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stories'), ['controller' => 'Stories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Story'), ['controller' => 'Stories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="projects view large-9 medium-8 columns content">
    <h3><?= h($project->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($project->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($project->image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($project->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category') ?></th>
            <td><?= $project->has('category') ? $this->Html->link($project->category->name, ['controller' => 'Categories', 'action' => 'view', $project->category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('District') ?></th>
            <td><?= $project->has('district') ? $this->Html->link($project->district->name, ['controller' => 'Districts', 'action' => 'view', $project->district->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= $project->has('country') ? $this->Html->link($project->country->id, ['controller' => 'Countries', 'action' => 'view', $project->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Language') ?></th>
            <td><?= $project->has('language') ? $this->Html->link($project->language->name, ['controller' => 'Languages', 'action' => 'view', $project->language->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($project->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Id') ?></th>
            <td><?= $this->Number->format($project->user_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($project->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($project->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Process') ?></th>
            <td><?= $this->Number->format($project->process) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Process Type') ?></th>
            <td><?= $this->Number->format($project->process_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($project->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($project->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Stories') ?></h4>
        <?php if (!empty($project->stories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Project Id') ?></th>
                <th scope="col"><?= __('Video') ?></th>
                <th scope="col"><?= __('Video Image Overlay') ?></th>
                <th scope="col"><?= __('Overview') ?></th>
                <th scope="col"><?= __('Pharse') ?></th>
                <th scope="col"><?= __('Pitch') ?></th>
                <th scope="col"><?= __('Lang Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($project->stories as $stories): ?>
            <tr>
                <td><?= h($stories->id) ?></td>
                <td><?= h($stories->project_id) ?></td>
                <td><?= h($stories->video) ?></td>
                <td><?= h($stories->video_image_overlay) ?></td>
                <td><?= h($stories->overview) ?></td>
                <td><?= h($stories->pharse) ?></td>
                <td><?= h($stories->pitch) ?></td>
                <td><?= h($stories->lang_id) ?></td>
                <td><?= h($stories->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Stories', 'action' => 'view', $stories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Stories', 'action' => 'edit', $stories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stories', 'action' => 'delete', $stories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($project->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Avatar') ?></th>
                <th scope="col"><?= __('Token Device') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Birthday') ?></th>
                <th scope="col"><?= __('District Id') ?></th>
                <th scope="col"><?= __('Country Id') ?></th>
                <th scope="col"><?= __('Postcode') ?></th>
                <th scope="col"><?= __('About Me') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Age') ?></th>
                <th scope="col"><?= __('Sex') ?></th>
                <th scope="col"><?= __('Fb Id') ?></th>
                <th scope="col"><?= __('Google Id') ?></th>
                <th scope="col"><?= __('Linked Id') ?></th>
                <th scope="col"><?= __('Token') ?></th>
                <th scope="col"><?= __('Time Limit') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($project->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->avatar) ?></td>
                <td><?= h($users->token_device) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->birthday) ?></td>
                <td><?= h($users->district_id) ?></td>
                <td><?= h($users->country_id) ?></td>
                <td><?= h($users->postcode) ?></td>
                <td><?= h($users->about_me) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->age) ?></td>
                <td><?= h($users->sex) ?></td>
                <td><?= h($users->fb_id) ?></td>
                <td><?= h($users->google_id) ?></td>
                <td><?= h($users->linked_id) ?></td>
                <td><?= h($users->token) ?></td>
                <td><?= h($users->time_limit) ?></td>
                <td><?= h($users->status) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
