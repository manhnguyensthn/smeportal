<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">

</style>
<div>
    <div class="container cprojects">

        <div class="col-md-9 contentForm">
            <div class="text-center">
                <ul class="clearfix template_tab_create_project">
                    <li class="item miles1"><a
                                href="/projects/edit/<?php echo $check->id ?>"><?php echo __('Project'); ?></a></li>
                    <li class="item miles2"><a
                                href="/projects/roles/<?php echo $check->id ?>"><?php echo __('Open Roles'); ?></a></li>
                    <!-- <li class="item miles3"><a href="/projects/about_you/<?php echo $check->id ?>"><?php echo __('About You'); ?></a></li> -->
                    <li class="item active miles4"><a
                                href="/projects/milestones/<?php echo $check->id ?>"><?php echo __('Milestones'); ?></a>
                    </li>
                </ul>
                <a href="/projects/<?php echo $this->Tool->slug($check->title) . '-' . $check->id ?>"
                   class="btn btn-warning btn-launch btn-preview"
                   style="margin:auto; display:inline-block; float: right;"><?php echo __('Preview'); ?></a>
            </div>
            <div class="clearfix"></div>

            <div style="border: 1px solid #918f8f; padding: 15px">
                <div class="page-header">
                    <div class="miles">
                        <h2><?php echo __('Milestones'); ?></h2>
                        <span class="miles-subtitle"><?php echo __('Thêm thành tích.') ?></span>
                    </div>
                </div>
                <p class="labelMilestone"><?php echo __('Project Start'); ?></p>
                <?php echo $this->Form->create($milestones, ['name' => 'frm_update_milestone', 'id' => 'frm_milestone']); ?>
                <div id="roleList">
                    <ul id="MileList" style="padding: 0">
                        <?php foreach ($mileStoneProject as $value) { ?>
                            <li class="mile-item" id="mile-<?php echo $value['id']; ?>">
                                <h3><?php echo $value['title'] ?></h3>
                                <span><?php echo $value['description'] ?></span>
                                <a href="javascript:confirmRemoveMileStone(<?php echo $value['id']; ?>,1);"
                                   class="delete-role">X</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="form-group col-md-12">
                    <div class="input text col-md-6" style="margin-bottom: 15px;">
                        <input id="title" type="text" autofocus="" class="form-control"
                               placeholder="<?php echo __('Tên thành tích') ?>"/>
                        <div class="error-message" id="msgTit"></div>
                    </div>
                    <div class="input text col-md-10">
                        <input id="description" type="text" class="form-control"
                               placeholder="<?php echo __('Add a description') ?>"/>
                        <div class="error-message" id="msgDes"></div>
                    </div>
                    <div class="input text col-md-2 padd-all" style="margin-bottom: 16px;">
                        <button id="addMiles" class="btn btn-warning btn-launch btn-custom pull-right" type="button"
                                style="color: #080707; width:100%; margin: 0px"><?php echo __('add') ?>
                        </button>
                    </div>
                </div>
<!--                <p class="labelMilestone">--><?php //echo __('Hoàn thành doanh nghiệp'); ?><!--</p>-->
                <button type="submit" class="btn btn-warning btn-launch btn-custom pull-right"
                        style="color: #080707;"><?php echo __('Lưu và tiếp tục'); ?></button>
<!--                <button type="button"-->
<!--                        class="btn btn-warning btn-launch btn-custom pull-right --><?php //echo ($project->is_completed == 0) ? 'btn-incomplete' : ''; ?><!--"-->
<!--                        style="color: #080707;">--><?php //echo __('Hoàn thành doanh nghiệp'); ?><!--</button>-->
                <div class="clearfix"></div>
                <?php echo $this->Form->end(); ?>
            </div>

            <div class="clearfix"></div>
            <br><br>

        </div>
    </div>
</div>
<div class="modal fade" id="alert-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center alert-content"><p>
                            <strong><?php echo __('Are you sure you want to delete this milestone?'); ?></strong></p>
                    </div>
                    <input name="item_id" type="hidden" value="0" id="item_id"/>
                    <input name="delete_opt" type="hidden" value="1" id="delete_opt"/>
                    <input name="confirm_option" type="hidden" value="delete_milestone" id="confirm-option"/>
                </div>
                <div class="row">
                    <div class="col-xs-6 text-center">
                        <button class="btn btn-launch" data-dismiss="modal"
                                aria-label="Close"><?php echo __('No'); ?></button>
                    </div>
                    <div class="col-xs-6 text-center">
                        <button class="btn btn-launch btn-accept"
                                onclick="process_confirm_box();"><?php echo __('Yes'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var title = "";
        var description = "";
        var cout = 0;
        $("#addMiles").click(function () {
            title = $("#title").val();
            description = $("#description").val();
            if (validMileStone(title, description)) {
                return true;
            }

            title = htmlspecialchars(title);
            description = htmlspecialchars(description);
            var items = "<li class='mile-item' id='mile-" + cout + "'>" +
                "<h3>" + title + "</h3>" +
                "<input type='hidden' name='mile[" + cout + "][title]' value='" + title + "' />" +
                "<span>" + description + "</span>" +
                "<input type='hidden' name='mile[" + cout + "][content]' value='" + description + "' />" +
                "<a href='javascript:void(0)' onclick='confirmRemoveMileStone(" + cout + ",0)' class='delete-role'>X</a>" +
                "</li>";
            $("#MileList").append(items);
            title = $("#title").val('');
            description = $("#description").val('');
            cout++;
        });

        $('#alert-box .btn-accept').click(function () {
            var row_id = $('#row_id').val();
            var delete_option = $('#delete_option').val();
            if (delete_option == '1') {
                window.location = '/projects/delmilestone/' + row_id;
            }
            else {
                $('#alert-box').modal('hide');
                $("#MileList li#mile-" + row_id).remove();
            }
        });

    });
    $('#frm_milestone').submit(function () {
        var count = 0;
        $('ul#MileList li.mile-item').each(function () {
            count = count + 1;
        });
        if (count == 0) {
            alert('<?php echo __('Please create at least one milestone.'); ?>');
            return false;
        }
    });

    $('.miles1').mouseover(function () {
        $('.miles1').addClass('active1_miles1');
    });
    $('.miles1').mouseout(function () {
        $('.miles1').removeClass('active1_miles1');

    });
    $('.miles2').mouseover(function () {
        $('.miles2').addClass('active2_miles2');
        $('.miles1').addClass('active1_miles2');

    });
    $('.miles2').mouseout(function () {
        $('.miles2').removeClass('active2_miles2');
        $('.miles1').removeClass('active1_miles2');
    });
    $('.miles3').mouseover(function () {
        $('.miles3').addClass('active3_miles3');
        $('.miles2').addClass('active2_miles3');

    });
    $('.miles3').mouseout(function () {
        $('.miles3').removeClass('active3_miles3');
        $('.miles2').removeClass('active2_miles3');
    });

</script>