<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>
    .excel{
        margin-bottom: 20px;
    }
</style>
<div class="container-fluid">
    <div class="col-md-12">
        <h3>Danh sách doanh nghiệp</h3>

        <form class="excel" method="get" action="projects/exportProjects">
            <button type="submit" class="btn btn-warning">Xuất file excel</button>
        </form>
        <table class="table table-bordered" id="myTable">
            <tr>
                <th>Mã</th>
                <th>Tên doanh nghiệp</th>
                <th>Lĩnh vực</th>
                <th>Người tạo</th>
                <th>Hội phí</th>
                <th>Ngày thành lập</th>
            </tr>
            <?php $stt = 1; ?>
            <?php foreach ($projects as $project): ?>
                <tr>
                    <td><?php echo $project['id']; ?></td>
                    <td><?php echo $project['title'] ?></td>
                    <td><?= $project->category['name'] ?></td>
                    <td><?= $project->user['last_name'] ?> <?= " ".$project->user['first_name'] ?></td>
                    <?php if ($project['fee_status'] == 0): ?>
                        <td>Chưa đóng hội phí</td>
                    <?php elseif ($project['fee_status'] == 1): ?>
                        <td>Đã đóng hội phí</td>
                    <?php endif; ?>
                    <td><?php echo $project['created'] ?></td>

                </tr>
                <?php $stt++; ?>
            <?php endforeach; ?>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div>
    </div>

    <div class="col-md-12">
        <h3>Bộ lọc</h3>
        <div class="">
            <?= $this->Form->create(null, ['type' => 'get']) ?>
            <?php echo $this->Form->input('group', [
                'class' => 'form-control',
                'label' => __('Nhóm'),
                'type' => 'select',
                'options' => $groups,
                'empty' => __('-- Chọn ban chấp hành --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <?php echo $this->Form->input('fee_status', [
                'class' => 'form-control',
                'label' => __('Tình trạng'),
                'type' => 'select',
                'options' => [1 => 'Chưa đóng hội phí', 2 => 'Đã đóng hội phí'],
                // 'value' => 4,
                'empty' => __('-- Chọn tình trạng --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <?php echo $this->Form->input('location', [
                'class' => 'form-control',
                'label' => __('Khu vực'),
                'type' => 'select',
                'options' => $locations,
                'empty' => __('-- Chọn khu vực --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <?php echo $this->Form->input('category', [
                'class' => 'form-control',
                'label' => __('Lĩnh vực'),
                'type' => 'select',
                'options' => $categories,
                'empty' => __('-- Chọn lĩnh vực --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <!-- <?php echo $this->Form->input('role', [
                'class' => 'form-control',
                'label' => __('Vai trò'),
                'type' => 'select',
                'options' => $usersType,
                'empty' => __('-- Vai tro--'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?> -->
        </div>
        <div class>
            <button type="submit" id="btn-submit" status="0" class="btn btn-warning btn-launch btn-custom "
                    style="color: #080707;">Lọc dữ liệu
            </button>
            <!-- <?= $this->Form->button(__('Lọc'), ["class" => "btn btn-lg-6 pull-right", "id" => "confirm"]) ?> -->
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
</script>