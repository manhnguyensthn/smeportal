<div>
    <div class="container cprojects">
        <div class="col-md-9 contentForm">
            <div class="text-center">
                <ul class="clearfix template_tab_create_project">
                    <li class="item item_role1 "><a href="/projects/edit/<?php echo $check->id ?>"><?php echo __('Project'); ?></a></li>
                    <li class="item item_role2 active"><a href="/projects/roles/<?php echo $check->id ?>"><?php echo __('Open Roles'); ?></a></li>
                    <!-- <li class="item item3 "><a href="/projects/about_you/<?php echo $check->id ?>"><?php echo __('About You'); ?></a></li> -->
                    <li class="item item3 item4"><a href="/projects/milestones/<?php echo $check->id ?>"><?php echo __('Milestones'); ?></a></li>
                </ul>
                <a href="/projects/<?php echo $this->Tool->slug($check->title) . '-' . $check->id ?>" class="btn btn-warning btn-launch btn-preview" style="margin:auto; display:inline-block; float: right;"><?php echo __('Preview'); ?></a>
            </div>

            <div class="clearfix"></div>
            <div class="row addRoleArea">
                <div id="roleList">
                    <div class="header-page">
                        <center>
                            <h2><?php echo __('Add Roles'); ?></h2>
                            <div class="clearfix"></div>
                            <p style="opacity: 0.4"><?php echo __('Create a list of people needed to make your project a reality'); ?></p>
                        </center>
                    </div>
                    <?php if (count($roleProject) == 0) { ?>
                        <center>
                            <p style="margin: 40px auto 20px;"><?php echo __('There are no Roles currently created'); ?></p>
                            <p style="margin-bottom: 40px;">
                                <?php echo __('Roles are a the people you need to make your <br> project a reality. Be clear of what you require for <br> the role to make finding people easier. '); ?>
                            </p>
                        </center>
                    <?php } else { ?>
                        <ul>
                            <?php foreach ($roleProject as $value) { ?>
                                <li>
                                    <h3><?php echo $value['quantity'] . ' ' . $value['role']['role'] ?> </h3>
                                    <span><?php echo $value['description'] ?></span>
                                    <a href="javascript:confirm_detele_role('<?php echo $value['id']; ?>');" class="delete-role" >X</a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <center><button type="button" id="actionAdd" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('Create New Role'); ?></button></center>
                    <div class="clearfix"></div>
                    <a href="javascript:check_role_in_project(<?php echo $check->id; ?>, <?php echo count($roleProject); ?>);" class="btn btn-warning btn-launch btn-custom pull-right"
                       style="color: #080707;"><?php echo __('Save and Continue') ?></a>
                    <div class="clearfix"></div>
                </div>
                <div id="addRoles" class="addRoles hidden" style="padding: 15px;">
                    <?php echo $this->Form->create($role) ?>
                    <div class="form-group" style="position: relative">
                        <label for="role">
                            <?php echo __('Role'); ?><br>
                            <small><?php echo __('Choose the role that best describes who you need for your project.') ?></small>
                        </label>
                        <div class="col-md-10" style="padding: 0">
                            <?php
                            echo $this->Form->input('role_id', array(
                                'div' => false, 'type' => 'select',
                                'class' => 'form-control', 'options' => $roles,
                                'label' => FALSE, 'required' => true, 'empty' => __('Choose role'),
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="form-group" id="character_name" style="position: relative;display: none">
                        <label for="character_name">
                            <?php echo __('Character Name'); ?><br>
                            <small><?php echo __('The name of the character in your project.') ?></small>
                        </label>
                        <div class="col-md-10" style="padding: 0">
                            <?php
                            echo $this->Form->input('charactor_name', array(
                                'div' => false, 'type' => 'text',
                                'class' => 'form-control',
                                'label' => false
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="form-group" style="position: relative">
                        <label for="description">
                            <?php echo __('Role Description'); ?><br>
                            <small><?php echo __('Provide a short description of what is needed from the role and anything that may not be apparent') ?></small>
                        </label>
                        <div class="row">
                            <div class="col-md-10" style="padding: 0px 11px;">
                                <?php echo $this->Form->textarea('description', array('div' => false, 'class' => 'form-control','style' => 'resize: vertical', 'maxLength' => 150, 'label' => FALSE, 'required' => true, 'onkeyup' => "countCharDescription(this)")); ?>
                            </div>
                        </div>
                        <div class="charCount charDes" style="top:165px;right: 160px">150</div>
                        <div class="row error-message"><?php echo $this->Form->error('description') ?></div>
                    </div>
                    <div class="form-group" style="position: relative">
                        <label for="quanlity">
                            <?php echo __('Quantity'); ?><br>
                            <small><?php echo __('How many of the chosen role do you need for your project?') ?></small>
                        </label>
                        <div class="col-md-10" style="padding: 0">
                            <?php echo $this->Form->input('quantity', array('type' => 'number', 'div' => false, 'class' => 'form-control', 'label' => FALSE, 'required' => true)); ?>
                        </div>
                        <br><br><br>
                        <div class="error-message" id="msgQuantity"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-right" style="margin: 18px 30px;">
                        <button type="button" class="btn btn-link pull-left" style="color: #020202;margin-right: 20px;"><?php echo __('Cancel') ?></button>
                        <button type="submit" class="btn btn-warning btn-launch btn-custom pull-left" style="color: #080707; margin: 0; width: 190px;margin-right: 15px;"><?php echo __('Add Role') ?></button>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="alert-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center alert-content"><p><strong><?php echo __('Are you sure you want to delete this role?'); ?></strong></p></div>
                    <input name="item_id" type="hidden" value="0" id="item_id" />
                    <input name="confirm_option" type="hidden" value="delete_role" id="confirm-option" />
                </div>
                <div class="row">
                    <div class="col-xs-6 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close" ><?php echo __('No'); ?></button></div>
                    <div class="col-xs-6 text-center"><button class="btn btn-launch btn-accept" onclick="process_confirm_box();" ><?php echo __('Yes'); ?></button></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#quantity").change(function () {
            var quantity = $("#quantity").val();
            if (quantity <= 0) {
                $("#msgQuantity").text("Chỉ nhập số.");
                return;
            } else {
                $("#msgQuantity").text("");
                $('.error-message').find('ul').hide(200);
            }

            if (quantity >  2147483647){
                $("#msgQuantity").text(" Không được nhập số lớn hơn 2147483647");
                return false;
            }
        })
        $("#actionAdd").on("click", function () {
            $("#roleList").addClass('hidden');
            $("#addRoles").removeClass('hidden');
        });
        $(".btn-link").click(function () {
            $("#roleList").removeClass('hidden');
            $("#addRoles").addClass('hidden');
        })
<?php if (isset($errors)) { ?>
            $("#actionAdd").trigger("click");
<?php } ?>

        //si.nguyen: 2016/11/25
        //add character name for Actor Role

        $('select[name="role_id"]').change(function () {
            if ($(this).val() == 88) {
                $('#character_name').show();
            } else {
                $('#character_name').hide();
            }
        });
    });
	$('.item_role1').mouseover(function() {
				$('.item_role1').addClass('active1_role1');
			});
			$('.item_role1').mouseout(function() {
				$('.item_role1').removeClass('active1_role1');
				
			});
				$('.item3').mouseover(function() {
				$('.item3').addClass('active3_role3');
				$('.item_role2').addClass('active2_role3');

			});
			$('.item3').mouseout(function() {
				$('.item3').removeClass('active3_role3');
				$('.item_role2').removeClass('active2_role3');
			});
				$('.item4').mouseover(function() {
				$('.item4').addClass('active4_role4');
				$('.item3').addClass('active3_role4');

			});
			$('.item4').mouseout(function() {
				$('.item4').removeClass('active4_role4');
				$('.item3').removeClass('active3_role4');
			});
</script>