<?php
echo $this->Html->css('croppie');
echo $this->Html->script('croppie');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!empty($check)) {
    $slug = $this->Tool->slug($check->title).'-'.$check->id;
}
else{
    $slug = '#';
}
?>
<div>
    <div class="container cprojects">
        <a href="/projects/<?php echo $slug; ?>" class="btn btn-warning btn-launch btn-preview"><?php echo __('Preview'); ?></a>
        <div class="col-md-9 contentForm">
            <div class="text-center">
                <ul class="clearfix template_tab_create_project">
                    <li class="item item_about1"><a href="/projects/edit/<?php echo isset($project_id) ? $project_id : 0; ?>"><?php echo __('Project'); ?></a></li>
                    <li class="item item_about2"><a href="/projects/roles/<?php echo isset($project_id) ? $project_id : 0; ?>"><?php echo __('Open Roles'); ?></a></li>
                    <li class="item active item_about3"><a href="/projects/about_you/<?php echo isset($project_id) ? $project_id : 0; ?>"><?php echo __('About You'); ?></a></li>
                    <li class="item item_about4"><a href="/projects/milestones/<?php echo isset($project_id) ? $project_id : 0; ?>"><?php echo __('Milestones'); ?></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <div style="border: 1px solid #918f8f; padding: 15px">
                <?php echo $this->Form->create($project, ['type' => 'file', 'id' => 'Users_EditProject']); ?>
                <div class="form-group col-md-12" style="position: relative">
                    <label for="title">
                        <?php echo __('Name'); ?><br>
                    </label>

                    <?php
                    echo $this->Form->input('UsersProjects.name', [
                        'type' => 'text', 'class' => 'form-control', 'label' => FALSE,
                        'value' => $name,
                    ]);
                    ?>

                </div>
                <div class="form-group col-md-12">
                    <label for="profile-picture">
                        <?php echo __('Profile Picture'); ?><br>
                    </label>
					 <div class = "crop_images" id= "crop_images">
					<h3>Crop image</h3>
						<div id= "crop">
						</div>
						<a class="btn upload-result" onclick = "javascript:void(0)">Save image</a>
					</div>
                    <div id="profile-picture" class="btn btn-default btn-file" <?php echo!empty($avatarURL) ? 'style="background-image: url(' . $avatarURL . ');"' : ''; ?>  >
                        <?php echo $this->Form->input('Users_CurrentImage', ['type' => 'hidden', 'value' => $avatarURL]); ?>
                        <div class="centered-text">
                            <span><?php echo __('Drag and drop') ?></span>
                            <div class="clearfix"></div>
                            <span>Or</span>
                            <div class="clearfix"></div>
                            <span><?php echo __('Chosse an image from your computer') ?></span>
                            <div class="clearfix"></div>
                            <br>
                            <p style="color: red;margin-top: -15px;">(<?php echo __('for iOS appearance, 2500x1500, 3MB image is maximum'); ?>)</p>
                            <span><small><?php echo __('JPEG,PNG, GIF, or BMP 50MB file limit<br>At least 1024x1024 pixels 16:9 aspect ratio') ?></small></span>
                            <div class="clearfix"></div>
                            <small id="fileNameUpload"></small>
                        </div>
                        <input type="file" name="UsersProjects[user_picture]" id="image">
                        <input type="hidden" name="UsersProjects[external_user_picture_url]" id="image_profile_url">
                        <div class="col-md-12 extendbtn">
                            <div class="col-md-6"><button type="button" id="Users_ImageProfile_UploadFromComputer" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('Upload from computer'); ?></button></div>
                            <div class="col-md-6"><button type="button" id="Users_ImageProfile_ExternalURL" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('External URL'); ?></button></div>
                        </div>
                    </div>
                    <div class="error-message"><?php echo $this->Form->error('external_user_picture_url') ?></div>
                </div>

                <div class="form-group">
                    <label for="role_id">
                            <?php echo __('Role'); ?><br>
                        <small><?php echo __('Choose the role that best describes who you need for your project.') ?></small>
                    </label>
                    <div class="col-md-12" style="padding: 0">
                            <?php 
                            echo $this->Form->input('UsersProjects.role_id', array(
                            'div' => false, 'type' => 'select',
                            'class' => 'form-control', 'options' => $roles,
                            'label' => FALSE,'required' => true,'empty' => __('Choose role'),
                            )); 
                            ?>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <label for="biography"><?php echo __('Biography'); ?><br/><small><?php echo __('A short description about you'); ?></small></label>
                    <?php
                    echo $this->Form->textarea('UsersProjects.biography', array('class' => 'form-control', 'rows' => 7,
                        'maxlength' => 500,
                        'style' => 'resize: vertical',
                        'value' => $biography));
                    ?>
                </div>

                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="input text">
                        <label for="website"><?php echo __('Websites'); ?></label>
                    </div>

                    <div class="input text col-md-7" style="padding-left: 0px">
                        <input id="webinfo" type="text" class="form-control"/>
                    </div>
                    <div class="input text col-md-2" style="margin-bottom: 16px;">
                        <button id="addweb" class="btn btn-warning btn-launch btn-custom pull-right" type="button"
                                style="color: #080707; width:100%; margin: 0px"><?php echo __('add'); ?>
                        </button>
                    </div>

                    <div class="input text col-md-7" style="padding-left: 0px">
                        <div id="websiteInfo">
                            <?php $count = 1 ?>
                            <?php foreach ($websites as $website): ?>
                            <div class="alert alert-default" role="alert">
                                    <?php echo $website->website->name ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <input type="hidden" name="UsersWebsite[no<?php echo $count ?>]" value="<?php echo $website->website_id ?>">
                            </div>
                                <?php ++$count ?>
                            <?php endforeach ?>
                        </div>
                        <input type="hidden" name="deletedWebsites" id="deletedWebsites" value="">
                    </div>
                    <div class="clearfix"></div>

                </div>

                <button type="submit" class="btn btn-warning btn-launch btn-custom pull-right"
                        style="color: #080707;"><?php echo __('Save and Continue') ?></button>
                <div class="clearfix"></div>

            </div>

            <div class="clearfix"></div>
            <br><br>

        </div>

        <!-- External Avatar URL -->
        <div class="modal fade" id="externalURLModal" tabindex="-1" role="dialog" aria-labelledby="externalURLModalLabel" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-header modal-header-custom">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="externalURLModalLabel"><?= __('External URL') ?></h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="userswork-title"><?php echo __('URL'); ?></label>
                            <input type="text" maxlength="255" class="form-control" id="users-external-url">
                            <div class="error-message" id="msgImg"></div>
                        </div>
                    </div>

                    <input type="hidden" id="Users_ExternalURL_Type" value="">
					<input type="hidden" name="image_url" id="image_url" value="" >
                    <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                        <button id="Users_ExternalURL_Post" type="button" class="btn btn-warning btn-yellow-shadow" style="width:20%"><?= __('Post') ?></button>
                    </div>

                </div>
            </div>
        </div>

        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class = "popup-w"></div>

<?php echo $this->Html->script('/js/jquery-loading-overlay/src/loadingoverlay.min'); ?>
<script type="text/javascript">
    $(document).ready(function () {

        $('#Users_EditProject').on('click', 'div#websiteInfo > div.alert.alert-default > button.close', function () {
            var currentDeletedWebsites = $('#deletedWebsites').val();
            var currentDeletedWebsitesArray = [];
            if (currentDeletedWebsites != '') {
                currentDeletedWebsitesArray = currentDeletedWebsites.split(',');
            }

            var deletedWebsite = $(this).parent().find('input[type="hidden"]');
            var deletedValue = $.trim(deletedWebsite.prop('value'));

            // Dont pay attention to newly added websites
            if (deletedValue != '' && deletedValue == deletedValue.replace(/\D/g, '')) {
                currentDeletedWebsitesArray.push(parseInt(deletedValue));
            }

            if (currentDeletedWebsitesArray.length > 0) {
                $('#deletedWebsites').val(currentDeletedWebsitesArray.join(','));
            } else {
                $('#deletedWebsites').val('');
            }
        })
        $('#Users_ImageProfile_UploadFromComputer').click(function (event) {
            event.preventDefault();
            $('#image[type=file]').trigger('click');
        });
        $("#profile-picture input[type=file]").change(function () {
            var fileName = this.value.split(/(\\|\/)/g).pop();
            if (fileName.length > 18) {
                fileName = fileName.substr(0, 8) + "..." + fileName.substr(fileName.length - 9, fileName.length - 1);
            }
            readURL(this);
            $("#fileNameUpload").text(fileName);
            $("input#image_profile_url").attr('value', '');
        });
        function getFileExtension(filename) {
            return filename.split('.').pop();
        }
        function readURL(input) {
            var arr = ['jpg', 'png', 'gif', 'bmp', 'PNG'];
            var ext = getFileExtension(input.files[0].name);
            if ($.inArray(ext, arr) == -1) {
                alertBox("<?= __('Ảnh đại diện phải có định dạng: jpeg, png, gif, bmp phân giải tối thiểu @1024x576.') ?>");
                return false;
            }
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    $("#profile-picture").css('background-image', 'url(' + e.target.result + ')');
                    $("#profile-picture .centered-text").css('visibility', 'hidden');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }


        $("#Users_EditProject > div.input.texarea.col-md-9 > textarea").on("cut paste keyup", function () {
            var remainCharsCount = 500 - $(this).val().length;
            if (remainCharsCount < 0) {
                remainCharsCount = 0;
            }
            $("#Users_EditProfile_BioRemainCharCount").text(remainCharsCount);
        });


        $('#Users_ImageProfile_Cancel').css({display: "none"});
        $('.avatarArea img').css({cursor: "pointer"}).mouseenter(function () {
            $(this).css({opacity: "0.5"});
            $('#Users_ImageProfile_Cancel').stop().fadeIn(200);
        }).mouseleave(function () {
            $(this).css({opacity: "1"});
            $('#Users_ImageProfile_Cancel').stop().fadeOut(200);
        });

        $("#Users_ImageProfile_UploadFromComputer").on('click', function () {
            $("#users-avatar").click();
        });

        var USER_PROFILE_EXTERNAL_AVARTAR_URL = 0;
        var USER_PROFILE_EXTERNAL_VIDEO_URL = 1;

        $("#UsersWork_ExternalURL").on('click', function () {
            $("#users-external-url").val("");
            $("#Users_ExternalURL_Type").val(USER_PROFILE_EXTERNAL_VIDEO_URL);
            $("#myModal").modal('hide');
            $("#externalURLModal").modal('show');
        });

        $("#Users_ImageProfile_ExternalURL").on('click', function () {
            $("#users-external-url").val("");
            $("#Users_ExternalURL_Type").val(USER_PROFILE_EXTERNAL_AVARTAR_URL);
            $("#externalURLModal").modal('show');
            $('#msgImg').html('');
        });

        $("#Users_ExternalURL_Post").on('click', function () {
            var externalURLType = $("#Users_ExternalURL_Type").val();
            var externalURL = $("#users-external-url").val() || '';
            var arrImg = ['jpg', 'png', 'gif', 'bmp'];
            // var arrClip = ['mov', 'mpeg', 'avi', 'mp4', '3gp', 'wmv', 'flv'];
            var ext = getFileExtension(externalURL);

            if (externalURL == null ||
                    externalURL === undefined ||
                    typeof externalURL === 'undefined' ||
                    $.trim(externalURL) == "" ||
                    (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL && $.inArray(ext, arrImg) == -1) ||
                    (externalURLType == USER_PROFILE_EXTERNAL_VIDEO_URL && !isYoutubeURL(externalURL))) {
                if (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL) {
                    $('#msgImg').html('<?php echo __('Invalid URL'); ?>');
                } else {
                    $('#msgImg').html('<?php echo __('Invalid URL'); ?>');
                }
                return;
            }

            var checkURL = $("<div>", {
                id: "externalURLCheck",
                css: {
                    "font-size": "18px",
                    "margin-top": "140px"
                },
                text: "<?= __('Checking URL... Please wait.') ?>"
            });
            if (externalURLType == USER_PROFILE_EXTERNAL_AVARTAR_URL) {
                // $('#users-external-avatar-url').val("");
                var img = new Image();
                $.LoadingOverlay('show', {zIndex: 9999, custom: checkURL});
                img.onload = function () {
                    if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                        $('#msgImg').html('<?php echo __('Invalid URL'); ?>');
                    } else {
                        $("#users-avatar").val("");
                        $("#profile-picture").css('background-image', 'url(' + this.src + ')');
                        $("#profile-picture .centered-text").css('visibility', 'hidden');
                        $('#users-external-avatar-url').val(this.src);
                        $("input#image_profile_url").val(this.src);
                    }
                    $.LoadingOverlay('hide');
                }
                img.onerror = function () {
                    $('#msgImg').html('<?php echo __('Invalid URL'); ?>');
                    $.LoadingOverlay('hide');
                }
                img.src = externalURL;
            }
            $("#externalURLModal").modal('hide');
        })
    });
	
	$uploadCrop = $('#crop').croppie({
    enableExif: true,
    viewport: {
        width: 300,
        height: 300,
        type: 'rectangle'
    },
    boundary: {
        width: 400,
        height: 400,
    }
});

var _URL = window.URL || window.webkitURL;
$('.cr-boundary').hide();
$('.cr-slider-wrap').hide();
$('#image').on('change', function () { 
$('.cr-boundary').hide();
$('.cr-slider-wrap').hide();
$('#crop_images').hide();
var file, img;
if ((file = this.files[0])) {
	img = new Image();
	img.onload = function () {
		if( this.width < 720 || this.height < 720 || this.width > 2500 || this.height > 1500)
		{
			alertBox("<?php echo __('Chọn một ảnh từ máy tính của bạn, độ phân giải tối thiểu 720x720 pixels và tối đa 2500x1500 pixels') ?>");
			return false;
		}
		else
		{
			$('.cr-boundary').show();
			$('.cr-slider-wrap').show();
			$('#crop_images').show();
			$('.popup-w').show();
		}
	   
	};
	img.src = _URL.createObjectURL(file);
}

	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    	});
    	
    }
    reader.readAsDataURL(this.files[0]);
});
$('.upload-result').on('click', function (ev) {
	var img = document.getElementById('image'); 
	var width = img.clientWidth;
	var height = img.clientHeight;
	$uploadCrop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {
		    $("#profile-picture").css('background-image', 'url(' + resp + ')');
			$("#profile-picture .centered-text").css('visibility', 'hidden');
			$("input#image_url").val(resp);
			$('.cr-boundary').hide();
			$('.cr-slider-wrap').hide();
			$('#crop_images').hide();
			$('.popup-w').hide();
	});
});

	
	$('.item_about1').mouseover(function() {
		$('.item_about1').addClass('active1_1');
		});
		$('.item_about1').mouseout(function() {
		$('.item_about1').removeClass('active1_1');

		});
		$('.item_about2').mouseover(function() {
		$('.item_about2').addClass('active2_2');
		$('.item_about1').addClass('active1_2');

		});
		$('.item_about2').mouseout(function() {
		$('.item_about2').removeClass('active2_2');
		$('.item_about1').removeClass('active1_2');
		});
		$('.item_about4').mouseover(function() {
		$('.item_about4').addClass('active4_4');
		$('.item_about3').addClass('active3_4');

		});
		$('.item_about4').mouseout(function() {
		$('.item_about4').removeClass('active4_4');
		$('.item_about3').removeClass('active3_4');
		});
</script>

<style>
h3 {
	background: #eb6500;
    padding: 15px;
    text-align: center;
    margin-top: 0px;
}
#crop_images{
background: #4a4848;
}
.upload-result {
    float: right;
    margin: 15px;
	background:#eb6500;
	color: #333;
}
.popup-w {
	display:none;
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(7,36,40, 0.5);
    top: 0;
    left: 0;
    z-index: 9;
}
#crop_images
{
	display:none;
    width: 600px;
    height: 560px;
    box-sizing: border-box;
    text-align: center;
    position: fixed;
    top: 40%;
    left: 50%;
    margin-top: -190px;
    margin-left: -285px;
    z-index: 999;
    color: black;
}
#profile-picture {
    background-position: 50%;
}
</style>