<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo $this->Html->css('bootstrap-datetimepicker.min');
echo $this->Html->script('bootstrap-datetimepicker.min');
echo $this->Html->css('bootstrap-tagsinput');
echo $this->Html->script('bootstrap-tagsinput');
echo $this->Html->css('croppie');
echo $this->Html->script('croppie');
?>
<?php  
function isImage($url){
        echo "test";
        if (!getimagesize($url)) {
           return false;
        }
        return true;
    }

?>

<div>
    <div class="container cprojects">
        <!--        <a href="#" class="btn btn-warning btn-launch btn-preview">Preview</a>-->
        <div class="col-md-9 contentForm">
            <!-- step section -->
            <div class="text-center">
                <ul class="clearfix template_tab_create_project">
                    <li class="item item1 active"><a href="javascript:void(0)"><?php echo __('Project'); ?></a></li>
                    <li class="item item2"><a href="<?php echo isset($project['id']) ? '/projects/roles/' . $project['id'] : '#'; ?>"><?php echo __('Open Roles'); ?></a></li>
                    <!-- <li class="item item3 "><a href="<?php echo isset($project['id']) ? '/projects/about_you/' . $project['id'] : '#'; ?>"><?php echo __('About You'); ?></a></li> -->
                    <li class="item item3 item4"><a href="<?php echo isset($project['id']) ? '/projects/milestones/' . $project['id'] : '#'; ?>"><?php echo __('Milestones'); ?></a></li>
                </ul>
            </div>
            <!-- end step -->

            <div class="clearfix"></div>

            <div class="clearfix"></div>
            <!-- form page 1 -->
            <?php echo $this->Form->create($project, ['type' => 'file', 'id' => 'creaetProject']) ?>

            <!-- input for title -->
            <div class="form-group" style="position: relative">
                <label for="title">
                    <?php echo __('Project Title'); ?><br>
                    <small><?php echo __('Give your project a name') ?></small>
                </label>
                <?php echo $this->Form->input('title', array('maxLength' => 60, 'autofocus' => true, 'div' => false, 'class' => 'form-control', 'label' => FALSE, 'onkeyup' => "countCharTitle(this)")); ?>
                <div class="charCount charTitle"><?php echo (strlen($project['title']) < 60) ? (60 - strlen($project['title'])) : 0; ?></div>
            </div>
            <!-- end input for title -->


            <!-- start input for video external source -->
            <div class="form-group">
                <label for="video_project">
                    <?php echo __('Project Video'); ?><br>
                    <small><?php echo __('Upload a video about your project, keep it 2-3 minutes.') ?></small>
                </label>
                <div id="video_project" class="btn-file">
                    <div id="iframe-video">
                        <?php echo (isset($project['video_id']) && !empty($project['video_id'])) ? '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $project['video_id'] . '" frameborder="0" allowfullscreen></iframe>' : ''; ?>
                    </div>
                    <div class="centered-text">
                        <center>
                            <button type="button" data-toggle="modal" data-target="#externalVideoURLModal" class="btn btn-warning btn-launch btn-custom" id="btn-show-video" style="float: none;color: #080707;"><?php echo __('External YouTube URL'); ?></button>
                        </center>
                    </div>
                </div>
                <input type='hidden' name='video_url' value='<?php echo isset($project['video_url']) ? $project['video_url'] : ''; ?>' id='video-url' />
                <input type='hidden' name='video_id' value='<?php echo isset($project['video_id']) ? $project['video_id'] : ''; ?>' id='video-id' />
                <div class="error-message"><?php echo $this->Form->error('video_url') ?></div>
            </div>

            <!-- end input for video external source -->


            <!-- project image -->
            <div class="form-group">
                <label for="image_project">
                    <?php echo __('Project Image'); ?><br>
                    <small><?php echo __('Upload an image that represents your project. and will overlay your video') ?></small>
                </label>
                <div class = "crop_images" id= "crop_images">
				<h3>Crop image</h3>
					<div id= "crop">
					</div>
					<a class="btn upload-result" onclick = "javascript:void(0)">Save image</a>
				</div>
                <div id="image_project" class="btn btn-default btn-file" <?php echo!empty($project['image_url']) ? 'style="background-image: url(' . $project['image_url'] . ');"' : ''; ?>  >
                    <div class="centered-text">
                        <span><?php echo __('Drag and Drop') ?></span>
                        <div class="clearfix"></div>
                        <span><?php echo __('Or'); ?></span>
                        <div class="clearfix"></div>
                        <span><?php echo __('Chọn một ảnh từ máy tính của bạn, độ phân giải tối thiểu 720x720 pixels và tối đa 2500x1500 pixels.') ?></span>
                        <div class="clearfix"></div>
                        <br>
                        <p><?php echo __('We recommend a 16:9 (1.778) aspect ratio.') ?></p>
                        <div class="clearfix"></div>
                        <br>
                        <p><?php echo __('Acceptable file formats are JPEG, PNG, GIF, BMP with a 50MB limit.') ?><br /><?php echo __('To display correctly on iOS, only a  max file size of  2500x1500 at 3MB allowed.'); ?></p>
                        <div class="clearfix"></div>
                        <small id="fileNameUpload"></small>
                    </div>
                    <input type="file" name="image_project" id="image">
                    <input type="hidden" name="image_project_url" id="image_project_url" value="<?php echo!empty($project['image_url']) ? $project['image_url'] : ''; ?>" >
                    <input type="hidden" name="image_url" id="image_url" value="" >
					<div class="col-md-12 extendbtn">
                        <div class="col-md-6"><button type="button" id="browseImg" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('Upload from computer'); ?></button></div>
                        <div class="col-md-6"><button type="button" data-toggle="modal" data-target="#myModalImg" class="btn btn-warning btn-launch btn-custom" style="float: none;color: #080707;"><?php echo __('External URL'); ?></button></div>
                    </div>
				
                </div>
                <div class="error-message"><?php echo $this->Form->error('image_url') ?></div>
            </div>

            <!-- end image input -->

            <!-- synopsis -->
            <div class="form-group" style="position: relative">
                <label for="description">
                    <?php echo __('Project Synopsis'); ?><br>
                    <small><?php echo __('Provide a short description of your project.') ?></small>
                </label>
                <?php echo $this->Form->textarea('description', array('maxLength' => 500, 'style'=>'resize: vertical', 'rows' => 5, 'div' => false, 'class' => 'form-control', 'label' => FALSE, 'onkeyup' => "countCharDescription(this)")); ?>
                <div class="charCount charDes" style="top:165px;"><?php echo isset($project['description']) ? 500 - strlen($project['description']) : 500; ?></div>
                <div class="error-message"><?php echo $this->Form->error('description') ?></div>
            </div>

            <!-- end synopsis -->

            <!-- category -->
            <div class="form-group">
                <label for="category_id">
                    <?php echo __('Category'); ?><br>
                    <small><?php echo __('Select a category that best represents your project. Talent will discover your project by exploring categories they are interrsted in.') ?></small>
                </label>
                <?php
                echo $this->Form->input('category_id', array(
                    'type' => 'select', 'class' => 'form-control', 'label' => FALSE,
                    'options' => $categories,
                    'id' => 'category_id',
                    'empty' => __('Category')
                        )
                );
                ?>
                <br>
                <label for="quantity_id">
                    <?php echo __('Quy mô lao động'); ?><br>
                    <small><?php echo __('') ?></small>
                </label>
                <?php
                echo $this->Form->input('quantity_id', array(
                    'type' => 'select', 'class' => 'form-control', 'label' => FALSE,
                    'options' => $quantities,
                    'id' => 'quantity_id',
                    'empty' => __('Quy mô lao động')
                        )
                );
                ?>
                <br>
                <label for="scale_id">
                    <?php echo __('Quy mô'); ?><br>
                    <small><?php echo __('Vốn đầu tư') ?></small>
                </label>
                <?php
                echo $this->Form->input('scale_id', array(
                    'type' => 'select', 'class' => 'form-control', 'label' => FALSE,
                    'options' => $scales,
                    'id' => 'scaley_id',
                    'empty' => __('Quy mô')
                        )
                );
                ?>
            </div>

            <!-- end category -->

            <!-- location -->
            <div class="form-group">
                <label for="title">
                    <?php echo __('Location'); ?><br>
                    <small><?php echo __('Chosse a location when you plan to complete your project. The location will be visible on your project page.') ?></small>
                </label>
                <div class="clearfix"></div>
                <div class="col-md-6 padd-left">
                    <?php
                    // $default = $countries->toArray();
                    // echo $default['235'];
                    // var_dump($default);
                    echo $this->Form->input('country_id', array(
                        'type' => 'select', 'class' => 'form-control', 'label' => FALSE,
                        'options' => $countries, 
                        'default' => 235,
                        'empty' => __('Country'), 'id' => 'countries'
                            )
                    );
                    ?>
                </div>
                <div class="col-md-6 padd-right">
                    <?php
                    echo $this->Form->input('state_id', array(
                        'type' => 'select', 'class' => 'form-control', 'label' => FALSE,
                        'options' => $states,
                        'default'=>2560,
                        'empty' => __('State/Province')
                            )
                    );
                    ?>
                </div>
                <div class="col-md-6 padd-left" style="padding-top: 15px;">
                    <?php
                    echo $this->Form->input('district_id', array(
                        'type' => 'select', 'class' => 'form-control', 'label' => FALSE,
                        'options' => $districts,
                        'empty' => __('City')
                            )
                    );
                    ?>
                </div>
            </div>


            <!-- end location -->

            <!-- project duration -->
            <!-- <div class="form-group">
                <label for="title">
                    <?php echo __('Project Start Date'); ?><br>
                    <small><?php echo __('When will you start looking for people to bring on to your project. Projects last 60 days.') ?></small>
                </label>
                <div class="clearfix"></div>
                <div class="col-md-6 padd-left">
                    <?php
                    $startDate = (isset($project['start_date']) && !empty($project['start_date'])) ? date('d-m-Y',  strtotime($project['start_date'])) : date('d-m-Y');
                    if (isset($project['start_date']) && !empty($project['start_date'])){
                        $maxEndDate = new DateTime(date('Y-m-d H:i:s',  strtotime($project['start_date'])));
                    }
                    else{
                        $maxEndDate = new DateTime(date('Y-m-d H:i:s'));
                    }
                    $maxEndDate->modify('+60 day');
                    echo $this->Form->input('start_date', array(
                        'class' => 'form-control', 'label' => FALSE, 'type' => 'text', 'placeholder' => __('Start date'), 'value' => $startDate, 'readonly' => 'true'
                            )
                    );
                    ?>
                    <div class="error-message" id="msgStartdate"></div>
                </div>
                <div class="col-md-6 padd-right">
                    <?php
                    $nowDate = new DateTime();
                    $nowDate->modify('+60 day');
                    $endDate = (isset($project['end_date']) && !empty($project['end_date'])) ? date('d-m-Y',  strtotime($project['end_date'])) : $nowDate->format('d-m-Y');
                    echo $this->Form->input('end_date', array(
                        'class' => 'form-control', 'label' => FALSE, 'type' => 'text', 'placeholder' => __('End date'),'value'=> $endDate, 'readonly' => 'true'
                            )
                    );
                    ?>
                    <div class="error-message" id="msgEnddate"><?php echo isset($end_date_errror) ? $end_date_errror : ''; ?></div>
                </div>
            </div> -->

            <!--end project duration -->

            <!-- tags -->
            <div class="form-group">
                <label for="tags">
                    <?php echo __('Project Tags'); ?><br>
                    <small><?php echo __('Enter up to 5 keywords that best describe your project. These will  not be displayed on your project page but will help collaborators find you.') ?></small>
                </label>
                <?php echo $this->Form->input('tags', array('div' => false, 'class' => 'form-control', 'label' => FALSE, 'data-role' => 'tagsinput', 'placeholder' => __('Tags'))); ?>
            </div>

            <!-- end tags -->
            <!-- project pitch -->
            <div class="form-group">
                <label for="pitch">
                    <?php echo __('Project Pitch'); ?><br>
                    <small><?php echo __('Present your project in more depth. Give collaborators more details about the story, direction, and obstacles you may face. ') ?></small>
                </label>
                <?php echo $this->Form->textarea('pitch', array('div' => false, 'id' => 'pitch', 'class' => 'form-control', 'label' => FALSE)); ?>
                <div class="error-message"><?php echo $this->Form->error('pitch') ?></div>
            </div>

            <!-- end pitch -->
            <br><br>
        </div>
        <div class="clearfix"></div>
        <button type="button" id="btn-submit" status="0" class="btn btn-warning btn-launch btn-custom pull-right" style="color: #080707;"><?php echo __('Save and Continue') ?></button>
        <?php echo $this->Form->end(); ?>
    </div>

    <!-- end form page 1 -->
</div>


<!-- modal for media external source -->
<div class="modal fade" id="externalVideoURLModal" tabindex="-1" role="dialog" aria-labelledby="externalVideoURLModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="externalURLModalLabel"><?= __('Media external links') ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="video-external-link"><?php echo __('Media external links'); ?></label>
                    <input type="text" maxlength="255" autofocus="" class="form-control" id="video-external-link">
                    <div class="error-message" id="msgVideo"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="color: #a5a5a5;"><?php echo __('Example: https://www.youtube.com/watch?v=cX0R3mXaod8'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="Users_ExternalURL_Type" value="">
            <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                <div class="row" style="margin-top: -25px;">
                    <div class="col-md-8 text-left"></div>
                    <div class="col-md-4">
                        <button id="Users_ExternalVideoURL_Post" type="button" class="btn btn-warning btn-launch" ><?= __('Post') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- end modal -->
<script type="text/javascript" src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
<script type="text/javascript">
    jQuery(function () {
        CKEDITOR.replace('pitch');
    });
</script>    
<script type="text/javascript">
    //Si.Nguyen: fix bug 27517
    var edit = <?php echo $edit ?>;
    if(edit!= 1){
        
    $('select[name="category_id"]').find('option').eq(1).prop('selected', true);
    $('select[name="quantity_id"]').find('option').eq(1).prop('selected', true);
    $('select[name="scale_id"]').find('option').eq(1).prop('selected', true);
    }
    // $('select[id="countries"]').find('option').eq(239).prop('selected', true);

    var today = '<?php echo date("d-m-Y"); ?>';
    $("#start-date").datetimepicker({
        format: 'd-m-Y',
        scrollInput: false,
        minDate: today
    });
    $("#end-date").datetimepicker({
        format: 'd-m-Y',
        scrollInput: false,
        minDate: today,
        maxDate: '<?php echo $maxEndDate->format('d-m-Y'); ?>'
    });
    $('#start-date').change(function () {
        var min_date = $(this).val();
        var min_date_arr = min_date.split('-');
        var maxDate = new Date(min_date_arr[2] + '-'+ min_date_arr[1]+ '-' + min_date_arr[0]);
        maxDate.setDate(maxDate.getDate() + 60);
        $("#end-date").datetimepicker({
            format: 'd-m-Y',
            scrollInput: false,
            minDate: min_date,
            maxDate: maxDate.dateFormat('d-m-Y')
        });
    });
    $('#browseImg').click(function (event) {
        event.preventDefault();
        $('#image[type=file]').trigger('click');
    });
    $("#image_project input[type=file]").change(function () {
        var fileName = this.value.split(/(\\|\/)/g).pop();
        if (fileName.length > 18) {
            fileName = fileName.substr(0, 8) + "..." + fileName.substr(fileName.length - 9, fileName.length - 1);
        }
        readURL(this);
        $("#fileNameUpload").text(fileName);
        $("input#image_project_url").attr('value', '');
    });
    function getFileExtension(filename) {
        return filename.split('.').pop();
    }
    function readURL(input) {
        var arr = ['jpg', 'png', 'gif', 'bmp', 'PNG'];
        var ext = getFileExtension(input.files[0].name);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onloadend = function (e) {  
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function () {
        var video_embed = $('#video-embed').val();
        $('#iframe-video').html(video_embed);
        $("#btn-submit").click(function () {
            var status = $(this).attr('status');
            $(this).attr('status','1');
            if (status == "0"){
                $("#creaetProject").submit();
            }
            else{
                return false;
            }
        });
        var edit = <?php echo $edit ?>;
        // console.log(edit);
        if(edit!= 1){

        // $('#countries').on('change', function () {
            var country_id = $('#countries').val();
            // var country_id = 235;
            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getStates"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#district-id');
                    var states = $('#state-id');
                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var newStates = {"<?= __('-- Select Province/State --') ?>": ""};

                    var stateList = data['data'];

                    if (stateList.length > 0) {
                        for (var idx = 0; idx < stateList.length; ++idx) {
                            newStates[stateList[idx]['States__name']] = stateList[idx]['States__id'];
                        }
                    }

                    states.empty();
                    $.each(newStates, function (key, value) {
                        states.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                     alertBox('<?= __('Session is timeout. Please, login again.') ?>');
                }
            });
        }
        // });
        $('#countries').on('change', function () {
            var country_id = $('#countries').val();
            // var country_id = 235;
            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getStates"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#district-id');
                    var states = $('#state-id');
                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var newStates = {"<?= __('-- Select Province/State --') ?>": ""};

                    var stateList = data['data'];

                    if (stateList.length > 0) {
                        for (var idx = 0; idx < stateList.length; ++idx) {
                            newStates[stateList[idx]['States__name']] = stateList[idx]['States__id'];
                        }
                    }

                    states.empty();
                    $.each(newStates, function (key, value) {
                        states.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                     alertBox('<?= __('Session is timeout. Please, login again.') ?>');
                }
            });
        });

        $('#state-id').on('change', function () {
            var country_id = $('#countries').val();
            var state_id = $(this).val();

            $.ajax({
                url: '<?= $this->Url->build([ "controller" => "Users", "action" => "getDistricts"]) ?>',
                type: 'POST',
                data: {
                    country_id: country_id,
                    state_id: state_id
                },
                dataType: 'json',
                success: function (data) {
                    var districts = $('#district-id');
//                    var postalCode = $('#users-postal-code');

                    var newDistricts = {"<?= __('-- Select City --') ?>": ""};
                    var districtList = data['data'];

                    if (districtList.length > 0) {
                        for (var idx = 0; idx < districtList.length; ++idx) {
                            newDistricts[districtList[idx]['Districts__name']] = districtList[idx]['Districts__id'];
                        }
                    }

                    districts.empty();
                    $.each(newDistricts, function (key, value) {
                        districts.append($("<option></option>")
                                .attr("value", value).text(key));
                    });

                    postalCode.val("");
                },
                error: function (e) {
                    alertBox('<?= __('Không thể hiển thị danh sách thành phố, vui lòng thử lại') ?>');
                }
            });
        });

        $("#Users_ExternalURL_Post").click(function () {
            var link_yt = $("#users-external-url").val();
            if (!(link_yt.indexOf('youtube.com') !== -1)) {
                $("#msgVideo").text('<?php echo __('Đường dẫn không hợp lệ'); ?>');
                return false;
            }
            if (!isUrlValid(link_yt)) {
                $("#msgVideo").text('<?php echo __('Đường dẫn không hợp lệ'); ?>');
                return false
            }
            var result = youtube_parser(link_yt);
            $("#video_project").css('background-image', 'url(https://i.ytimg.com/vi/' + result + '/hqdefault.jpg)');
            $("input#video").attr('value', result);
            $("#users-external-url").val('');
            $("#msgVideo").text('');
            $('#myModal').modal('hide');
        });
        $("#Users_ExternalImgURL_Post").click(function () {
			$('.cr-boundary').hide();
			$('.cr-slider-wrap').hide();
            var link_img = $("#users-external-imgurl").val();
            // alert(link_img);
            if (link_img.trim().length == 0) {
                $("#msgImage").text('<?php echo __('Invalid URL'); ?>');
                return false;
            }
            if (!isUrlValid(link_img)) {
                $("#msgImage").text('<?php echo __('Invalid URL'); ?>');
                return false;
            }
            // console.log(isUrlValid(link_img));

            // if (!isImage(link_img)) {
            //     $("#msgImage").text('<?php echo __('Invalid URL'); ?>');

            //     return false;
            // }
            $("#image_project").css('background-image', 'url(' + link_img + ')');
            $("#image_project .centered-text").css('visibility', 'hidden');
            $("input#image_project_url").attr('value', link_img);
            $("#users-external-imgurl").val('');
			$("input#image_url").val('');
            $("#image").val('');
            $("#msgImage").text('');
            $('#myModalImg').modal('hide');
        });

        $('#Users_ExternalVideoURL_Post').click(function () {
            var video_url = $('#video-external-link').val();
            if (!isYoutubeURL(video_url)) {
                $("#msgVideo").text('<?php echo __('Invalid URL'); ?>');
                return false;
            } else {
                var videoID = youtube_parser(video_url);
                $.ajax({
                    type: 'POST',
                    url: '/projects/checkVideoYoutubeExit',
                    data: {
                        'videoID': videoID
                    },
                    success: function (respone) {
                        if (respone == 'TRUE') {
                            $('#video-url').val(video_url);
                            $('#video-id').val(videoID);
                            $('#iframe-video').html('<iframe width="560" height="315" src="https://www.youtube.com/embed/' + videoID + '" frameborder="0" allowfullscreen></iframe>');
                            $('#externalVideoURLModal').modal('hide');
                            return true;
                        } else {
                            $("#msgVideo").text('<?php echo __('Invalid URL'); ?>');
                            return false;
                        }
                    }
                });
            }
        });

        $('#btn-show-video').click(function () {
            $('#video-embed-code').val('');
            $("#msgVideo").text('');
        });
        $(function () {
            $("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput({maxTags: 5, maxChars: 50});
        });
    });

    function show_guide_get_embed_code() {
        $('#guide-get-embeded').toggle();
    }
	
	$uploadCrop = $('#crop').croppie({
    enableExif: true,
    viewport: {
        width: 700,
        height: 280,
        type: 'rectangle'
    },
    boundary: {
        width: 800,
        height: 300,
    }
});

var _URL = window.URL || window.webkitURL;
$('.cr-boundary').hide();
$('.cr-slider-wrap').hide();
$('#image').on('change', function () { 
$('.cr-boundary').hide();
$('.cr-slider-wrap').hide();
$('#crop_images').hide();
var file, img;
if ((file = this.files[0])) {
	img = new Image();
	img.onload = function () {
		if( this.width < 720 || this.height < 720 || this.width > 2500 || this.height > 1500)
		{
			// alertBox("<?php echo __('Choose an image from your computer that is at least 720x720 pixels in size and a maximum of 2500x1500') ?>");
            alertBox("<?php echo __('Chọn ảnh từ máy tính với độ phân giải từ 720x720 đến 2500x1500') ?>");
            
			return false;
		}
		else
		{
			$('.cr-boundary').show();
			$('.cr-slider-wrap').show();
			$('#crop_images').show();
			$('.popup-w').show();
		}
	   
	};
	img.src = _URL.createObjectURL(file);
}

	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    	});
    	
    }
    reader.readAsDataURL(this.files[0]);
});
$('.upload-result').on('click', function (ev) {
	var img = document.getElementById('image'); 
	var width = img.clientWidth;
	var height = img.clientHeight;
	$uploadCrop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {
			$("#image_project").css('background-image', 'url(' + resp + ')');
            $("#image_project .centered-text").css('visibility', 'hidden');
            $("input#image_url").attr('value', resp);
			$('.cr-boundary').hide();
			$('.cr-slider-wrap').hide();
			$('#crop_images').hide();
			$('.popup-w').hide();
	});
});

	$('.item2').mouseover(function() {
		$('.item2').addClass('active2_item2');
		$('.item1').addClass('active1_item2');

	});
	$('.item2').mouseout(function() {
		$('.item2').removeClass('active2_item2');
		$('.item1').removeClass('active1_item2');
	});
		$('.item3').mouseover(function() {
		$('.item3').addClass('active3_item3');
		$('.item2').addClass('active2_item3');
	});
	$('.item3').mouseout(function() {
		$('.item3').removeClass('active3_item3');
		$('.item2').removeClass('active2_item3');
	});
		$('.item4').mouseover(function() {
		$('.item4').addClass('active4_item4');
		$('.item3').addClass('active3_item4');

	});
	$('.item4').mouseout(function() {
		$('.item4').removeClass('active4_item4');
		$('.item3').removeClass('active3_item4');
	});
	
</script>

<style type="text/css">
    a.cke_combo_button {
        border: none !important;box-shadow:none !important;background: transparent !important;background-image: none !important;
    }.cke_toolgroup {
        border: none !important;box-shadow: none !important; background: transparent !important;background-image: none !important;
    }
</style>
<!-- External Youtube URL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="externalURLModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="externalURLModalLabel"><?= __('Đường dẫn mở rộng') ?></h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="userswork-title"><?php echo __('Đường dẫn'); ?></label>
                    <input type="text" maxlength="255" class="form-control" id="users-external-url">
                    <div class="error-message" id="msgVideo"></div>
                </div>
            </div>

            <input type="hidden" id="Users_ExternalURL_Type" value="">
            <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                <button id="Users_ExternalURL_Post" type="button" class="btn btn-warning btn-launch btn-custom" style="width:20%;float: right;margin: 0px"><?= __('Post11111') ?></button>
            </div>

        </div>
    </div>
</div>
<!-- External project image URL -->
<div class="modal fade" id="myModalImg" tabindex="-1" role="dialog" aria-labelledby="externalURLModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="externalURLModalLabel"><?= __('Đường dẫn ảnh mở rộng') ?></h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="userswork-title"><?php echo __('Đường dẫn ảnh'); ?></label>
                    <input type="text" maxlength="255" class="form-control" id="users-external-imgurl">
                    <div class="error-message" id="msgImage"></div>
                </div>
            </div>

            <input type="hidden" id="Users_ExternalURL_Type" value="">
            <div class="modal-footer" style="border: none; padding: 0px 15px 15px 0px;">
                <button id="Users_ExternalImgURL_Post" type="button" class="btn btn-warning btn-launch btn-custom" style="width:20%;float: right;margin: 0px;"><?= __('Đăng ảnh') ?></button>
            </div>
        </div>
    </div>
</div>
<div class = "popup-w"></div>
<style>
h3 {
	background: #eb6500;
    padding: 15px;
    text-align: center;
    margin-top: 0px;
}
#crop_images{
background: #4a4848;
}
.upload-result {
    float: right;
    margin: 15px;
	background:#eb6500;
	color: #333;
}
.popup-w {
	display:none;
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(7,36,40, 0.5);
    top: 0;
    left: 0;
    z-index: 9;
}
#crop_images
{
	display:none;
    width: 800px;
    height: 460px;
    box-sizing: border-box;
    text-align: center;
    position: fixed;
    top: 40%;
    left: 40%;
    margin-top: -190px;
    margin-left: -285px;
    z-index: 999;
    color: black;
}
</style>