<?php
$userInfo = $this->AuthUser->_getUser();
if (!empty($data['start_date']) && !empty($data['end_date'])) {
    $actionDay = $this->AuthUser->_sub_datetime(date('Y-m-d', strtotime($data['end_date'])), date('Y-m-d', strtotime($data['start_date'])));
    $leftDay = $this->AuthUser->_sub_datetime(date('Y-m-d', strtotime($data['end_date'])), date('Y-m-d'));
} else {
    $leftDay = 0;
}
$uriShare = ROOT_URL . 'projectactions/share/' . $data['id'];
$linkShareFB = 'https://www.facebook.com/dialog/share?app_id=' . FB_APP_ID . '&display=popup&href=' . urlencode(ROOT_URL . $_SERVER["REQUEST_URI"]) . '&redirect_uri=' . urlencode(ROOT_URL . $_SERVER["REQUEST_URI"]);
$progessBar = 0;
if (count($projectRoles) > 0) {
    $listUserJoined = $this->AuthUser->_get_list_collaborator_by_status($usersProjects, 1);
    if (count($listUserJoined) > 0) {
        $projectRoleCounter = $this->AuthUser->_get_total_quantity_member_by_projects($projectRoles);
        $progessBar = (count($listUserJoined) / $projectRoleCounter) * 100;
        if ($progessBar > 100) {
            $progessBar = 100;
        }
    } else {
        $progessBar = 0;
    }
} else {
    $listUserJoined = [];
    $progessBar = 0;
}
?>
<div class="container box-project-info">
    <div class="row">
        <div class="col-md-12 box-project-name">
            <div class="col-md-8">
                <h1 class="project-name text-center"><?php echo isset($data['title']) ? $data['title'] : ''; ?></h1>
            </div>
            <div class="col-md-4">
                <?php if (!empty($userInfo) && $userInfo['id'] == $data['user_id']) : ?>
                    <a class="edit-panel" href="/projects/edit/<?php echo $data['id']; ?>"><img src="/img/icon_edit.png"
                                                                                                alt="<?php echo __('Edit project'); ?>"
                                                                                                title="<?php echo __('Edit project'); ?>"/></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="row">
                <div class="col-md-12 project-info">
                    <div class="row project-thumb">
                        <div class="video-container">
                            <!-- Video/Image Project -->
                            <?php if ($data['video_id'] == null): ?>
                                <img src="<?php echo $data['image_url'] ?>" onError="this.onerror=null;this.src='/img/project_default.jpg';">
                            <?php else: ?>
                                <?php echo !empty($data['video_id']) ? '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $data['video_id'] . '" frameborder="0" allowfullscreen></iframe>' : $data['video_url']; ?>
                            <?php endif; ?>
                            <!-- Video/Image Project -->
                        </div>
                    </div>
                    <div class="row project-info-detail">
                        <div class="col-xs-10"><span
                                    style="margin-right: 10px"><?php echo (isset($category['name']) && !empty($category['name'])) ? $this->AuthUser->_limit_by_length_string($category['name'], 400) : 'Feature'; ?></span><br><?php echo (isset($state['name']) && !empty($state['name'])) ? '<i class="fa fa-map-marker"></i>' . $state['name'] . ',' : ''; ?> <?php echo isset($country['country_name']) ? $country['country_name'] : ''; ?>
                        </div>
                        <!-- <div class="col-xs-6">
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?php echo $progessBar; ?>" aria-valuemin="0" aria-valuemax="<?php echo $progessBar; ?>" style="width: <?php echo $progessBar; ?>%">
                                    <span class="sr-only"><?php echo $percentLeftDay; ?>% <?php echo __('Complete (warning)'); ?></span>
                                </div>
                            </div>
                            <p class="text-right"><?php echo ($leftDay >= 0) ? $leftDay : 0; ?> <?php echo __('days left'); ?></p>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-12 project-summary">
                            <p><?php echo isset($data['description']) ? $data['description'] : ''; ?></p></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12 list-socials"><span><?php echo __('Share'); ?>:</span>
                            <a href="http://www.twitter.com/share?url=<?php echo ROOT_URL . $_SERVER["REQUEST_URI"]; ?>"><i
                                        class="fa fa-twitter"></i></a>
                            <a href="<?php echo $linkShareFB; ?>"><i class="fa fa-facebook-official"></i></a>
                            <a href="javascript:show_iframe_code();"><i class="fa fa-share-alt"></i></a>
                        </div>
                        <?php if (!empty($userInfo)): ?>
                            <div class="col-md-6 col-xs-12 list-follows">
                                <a href="javascript:add_action_to_system('1','<?php echo $data['id']; ?>');"
                                   class="btn-like">
                                    <?php if ($this->AuthUser->_check_user_project_action($userAction, 1)): ?>
                                        <img src="/img/heart-liked.png" alt="Like it!" title="Like it!"/>
                                    <?php else: ?>
                                        <img src="/img/heart-dislike.png" alt="Liked it!" title="Liked it!"/>
                                    <?php endif; ?>
                                </a>
                                <?php if ($is_blocked == TRUE): ?>
                                    <a href="javascript:alert_block_user();"
                                       class="btn btn-default"><?php echo __('Theo dõi +'); ?></a>
                                <?php else: ?>
                                    <?php if ($this->AuthUser->_check_user_project_action($userAction, 2)): ?>
                                        <a href="javascript:add_action_to_system('2','<?php echo $data['id']; ?>');"
                                           class="btn btn-default active"><?php echo __('Theo dõi +'); ?></a>
                                    <?php else: ?>
                                        <a href="javascript:add_action_to_system('2','<?php echo $data['id']; ?>');"
                                           class="btn btn-default"><?php echo __('Theo dõi +'); ?></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <div class="col-md-6 col-xs-12 list-follows">
                                <a href="javascript:void(0);" class="btn-like">
                                    <img src="/img/heart-dislike.png" alt="Liked it!" title="Liked it!"/>
                                </a>
                                <a href="javascript:void(0);"
                                   class="btn btn-default"><?php echo __('Theo dõi +'); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class='row' id='frm-copy-iframe'>
                        <div class='col-xs-10'>
                            <p><?php echo __('Nhúng mã HTML sau vào trang của bạn, ngay sau thẻ'); ?>
                                <code>&ltbody&gt</code><!-- --><?php /*echo __('tag'); */?>.</p>
                        </div>
                        <div class="col-xs-2 text-right">
                            <button class="btn btn-default btn-copy"
                                    onclick="copyToClipboard(document.getElementById('iframe-code-content').value);">
                                Copy
                            </button>
                        </div>
                        <div class="col-xs-12">
                            <textarea class="form-control" readonly="" id="iframe-code-content"><iframe
                                        src="<?php echo $uriShare; ?>" frameborder="0"
                                        style="height: 185px; overflow:scroll; width: 100%" marginheight="1"
                                        marginwidth="1" scrolling="no" frameborder="0"
                                        allowtransparency="true"></iframe></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 list-tab-project" id="list-frm-project-detail">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#detail" aria-controls="detail"
                                                                          role="tab"
                                                                          data-toggle="tab"><?php echo __('Detail'); ?></a>
                                </li>
                                <li role="presentation"><a href="#the-film" aria-controls="the-film" role="tab"
                                                           onclick="get_project_info_by_tab('the_film', 'the-film', '<?php echo $data['id']; ?>');"
                                                           data-toggle="tab"><?php echo __('The Team'); ?></a></li>
                                <li role="presentation"><a href="#updates" aria-controls="updates" role="tab"
                                                           onclick="get_project_info_by_tab('updates', 'updates', '<?php echo $data['id']; ?>');"
                                                           data-toggle="tab"><?php echo __('Thành tích'); ?></a></li>
                                <li role="presentation"><a href="#comments" aria-controls="comments" role="tab"
                                                           onclick="get_project_info_by_tab('comments', 'comments', '<?php echo $data['id']; ?>');"
                                                           data-toggle="tab"><?php echo __('Comments'); ?></a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="detail">
                                    <?php echo isset($data['pitch']) ? $data['pitch'] : ''; ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="the-film">
                                    <div class="row">
                                        <div class="col-xs-12 tab-info-content">
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="updates">
                                    <div class="row list-updates tab-info-content">
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="comments">
                                    <div class="row">
                                        <div class="col-xs-12 tab-info-content">
                                        </div>
                                    </div>
                                    <div class="row box-form-alert"></div>
                                    <?php
                                    if ((!empty($userInfo) && $this->AuthUser->_check_user_project_action($userAction, 2)) || (!empty($userInfo) && $userInfo['id'] == $data['user_id']) || $this->AuthUser->_check_is_member_of_project($listUserJoined, $userInfo['id'])) {
                                        $formCommentClass = '';
                                    } else {
                                        $formCommentClass = 'style="display:none"';
                                    }
                                    ?>
                                    <?php if (!empty($userInfo) && $is_blocked == FALSE): ?>
                                        <div class="row comment-all-form" <?php echo $formCommentClass; ?> >
                                            <form name="frm_comment" method="POST"
                                                  onsubmit="return add_comments_to_system('<?php echo $data['id']; ?>');">
                                                <div class="col-xs-12">
                                                    <p class="title-box-form"><?php echo __('Comment'); ?></p>
                                                    <textarea class="form-control" name="comment"></textarea>
                                                </div>
                                                <div class="col-xs-12 text-right">
                                                    <button class="btn btn-warning btn-launch"
                                                            type="submit"><?php echo __('Post'); ?></button>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="row project-author-info">
                <div class="col-xs-4"><a href="/applicant-profile-<?php echo $data['user']['id'] ?>"" class="img-circle"
                    ><img src="<?php echo !empty($data['user']['avatar']) ? $data['user']['avatar'] : '/img/avatars/avatar_default.jpg'; ?>"
                          alt="<?php echo isset($data['user']['first_name']) ? $data['user']['first_name'] . ' ' . $data['user']['last_name'] : ''; ?>"
                          title="<?php echo isset($data['user']['first_name']) ? $data['user']['first_name'] . ' ' . $data['user']['last_name'] : ''; ?>"/></a>
                </div>
                <div class="col-xs-6">
                    <p class="author-name"><b><?php echo __('Creator'); ?></b><br/>
                        <a href="/applicant-profile-<?php echo $data['user']['id'] ?>"><?php echo isset($data['user']['first_name']) ? $data['user']['first_name'] . ' ' . $data['user']['last_name'] : ''; ?></a>
                    </p>
                    <div class="row list-contacts"></div>
                </div>
                <div class="col-xs-2"></div>
            </div>
            <div class="row list-roles">
                <h3 class="box-title"><?php echo __('Open Roles'); ?></h3>
                <?php
                foreach ($projectRoles as $item):
                    $joinedCounter = $this->AuthUser->_get_role_open_in_list($usersProjects, $data['id'], $item['id']);
                    $status = ($joinedCounter >= $item['quantity']) ? '0' : '1';
                    if ($status == '1') {
                        $status = ($this->AuthUser->_check_user_request($usersProjects, $userInfo['id'], $item['id']) == TRUE) ? '0' : '1';
                    }
                    if ($is_blocked == TRUE) {
                        $status = '0';
                    }
                    $class = ($status == '0') ? 'active' : '';
                    ?>
                    <div class="role-item col-xs-12 <?php echo $class; ?>"
                         id="role-item-<?php echo $item['role']['id']; ?>" data-id="<?php echo $item['role']['id']; ?>"
                         data-project-role-id="<?php echo $item['id']; ?>"
                         data-role-name="<?php echo $item['role']['role']; ?>" data-status="<?php echo $status; ?>">
                        <p class="name-role"><?php echo $item['role']['role']; ?></p>
                        <?php if (!empty($item['charactor_name'])): ?>
                            <p class="name-role"><?php echo $item['charactor_name']; ?></p>
                        <?php endif; ?>
                        <p class="role-summary"><?php echo $item['description']; ?></p>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="alert-box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center alert-content"><p>
                            <strong><?php echo __('Are you sure you want to join project?'); ?></strong></p></div>
                    <input name="confirm_option" type="hidden" value="join_project" id="confirm-option"/>
                </div>
                <div class="row">
                    <div class="col-xs-6 text-center">
                        <button class="btn btn-launch" data-dismiss="modal"
                                aria-label="Close"><?php echo __('No'); ?></button>
                    </div>
                    <div class="col-xs-6 text-center">
                        <button class="btn btn-launch btn-accept"
                                onclick="process_confirm_box();"><?php echo __('Yes'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="form-request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Camera Guy'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 alert-content">
                        <p><?php echo __('You are applying for the Camera Guy role. Would you like to include a message?'); ?></p>
                    </div>
                    <div class="col-md-12">
                        <textarea class="form-control" name="message" id="message-confirm"></textarea>
                        <input name="role_id" type="hidden" value="0" id="role_id"/>
                        <input name="project_id" type="hidden" value="<?php echo $data['id']; ?>" id="project_id"/>
                        <input name="item_id" type="hidden" value="0" id="item_id"/>
                        <input name="project_role_id" type="hidden" value="0" id="project_role_id"/>
                        <input name="confirm_option" type="hidden" value="join_project" id="confirm-option"/>
                        <p class="alert-form text-danger" style="color: red"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button class="btn btn-cancel" data-dismiss="modal"
                                aria-label="Close"><?php echo __('Cancel'); ?></button>
                        <button class="btn btn-warning btn-launch" onclick="process_confirm_box();"
                                style="margin: 0; float: none; font-size: 19px;"><?php echo __('Apply'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    <?php if (!empty($userInfo) && $userInfo['id'] != $data['user_id']): ?>
    $(document).ready(function () {
        $('.list-roles .role-item').click(function () {
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            var project_role_id = $(this).attr('data-project-role-id');
            var role_name = $(this).attr('data-role-name');
            if (status == '1') {
                $('#message-confirm').val('');
                $('#role_id').val(id);
                $('#project_role_id').val(project_role_id);
                $('#confirm-option').val('join_project');
                $('#item_id').val(id);
                $('#form-request h4.modal-title').html(role_name);
                $('#form-request .alert-content').html('<p><?php echo __('You are applying for the'); ?> ' + role_name + ' <?php echo __('role. Would you like to include a message?'); ?></p>');
                $('#form-request').modal('show');
            }
        });
    });
    <?php endif; ?>
    $(function () {
        $("[data-toggle=popover]").popover({
            html: true,
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function () {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-heading").html();
            }
        });
    });
    function copyToClipboard(s) {
        var input = document.getElementById("iframe-code-content");
        input.focus();
        input.select();
        document.execCommand('Copy');

        if (document.selection) {
            document.selection.empty();
        } else if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
    }
    function alert_block_user() {
        alert('Could not follow this project.');
    }

</script>