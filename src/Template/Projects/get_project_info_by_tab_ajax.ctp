<?php if ($option == 'the_film'): ?>
    <?php $userInfo = $this->AuthUser->_getUser(); ?>
    <div class="row list-members">
        <?php if ((!empty($userInfo) && $this->AuthUser->_check_is_member_of_project($data, $userInfo['id'])) || $project->user_id == $userInfo['id']): ?>
            <?php foreach ($data as $item): ?>
                <div class="member-item col-md-3 col-xs-12">
                    <div class="row avatar">
                        <a href="/usersprojects/userChatDetail/<?php echo $item['project_id']; ?>/<?php echo $item['user_id']; ?>"><img class="img-circle" src="<?php echo!empty($item['user']['avatar']) ? $item['user']['avatar'] : '/img/avatars/avatar_default.jpg'; ?>" alt="<?php echo $item['name']; ?>" title="<?php echo $item['name']; ?>" /></a>
                    </div>
                    <div class="row member-name">
                        <p class="text-center"><?php echo $item['user']['first_name'].' '.$item['user']['last_name']; ?></p>
                        <p class="text-center"><?php echo $item['role']['role']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>   
        <?php else: ?>
            <?php foreach ($data as $item): ?>
                <div class="member-item col-md-3 col-xs-12">
                    <div class="row avatar">
                        <img class="img-circle" src="<?php echo!empty($item['user']['avatar']) ? $item['user']['avatar'] : '/img/avatars/avatar_default.jpg'; ?>" alt="<?php echo $item['name']; ?>" title="<?php echo $item['name']; ?>" />
                    </div>
                    <div class="row member-name">
                        <p class="text-center"><?php echo $item['name']; ?></p>
                        <p class="text-center"><?php echo $item['role']['role']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>  
        <?php endif; ?>
    </div>
<?php elseif ($option == 'updates'): ?>
    <div class="time-line" style="margin-top: 0px;">
        <div class="line"></div>
        <div class="wrapper-timeline">
            <div class="timeline-add dropdown">
                <a href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none; text-align: center; font-size: 50px; line-height: 20px; color: #fff;">...</a>

                <div class="form-add dropdown-menu" aria-labelledby="dLabel">
                    <div class="item update active" style="background: #ebebec;">
                        <div class="title"><a href="javascript:void(0);"><?php echo __('MileStones'); ?></a></div>
                        <?php if(isset($projectMileStonesInactive) && !empty($projectMileStonesInactive)): ?>
                            <ul class="template-list-milestones">
                                <?php foreach ($projectMileStonesInactive as $item): ?>
                                    <li><i class="fa fa-circle"></i><?php echo $item['title']; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <div class="no-item-message" style="padding: 50px 0; color: #a7a7a7;"><?php echo __('No content display'); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            
            <div class="wrapper-content">
                <ul class="clearfix">
                    <?php if(isset($data) && !empty($data)): ?>
                        <?php foreach ($data as $index => $row): ?>
                            <?php
                                echo $this->element('Mettings/item_update', array(
                                    'ProjectUpdate' => $row,
                                    'IsNotForm' => true,
                                ));
                            ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="timeline-start">
            <div class="milestone-circle"></div>
            <p class="title-timeline text-center" style="margin: 0;"><?php echo __('Project Start'); ?></p>
            <span><?php echo date('F d', strtotime($project['created'])); ?></span>
        </div>
    </div>
<?php else: ?>
    <?php $userInfo = $this->AuthUser->_getUser();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    ?>
    <input name="project_id" id="project_id" type="hidden" value="<?php echo $project['id']; ?>" />
    <ul class="media-list list-comments">
        <?php $listCommentParents = $this->AuthUser->_get_list_comment_by_parent_id($data, 0); ?>
        <?php foreach ($listCommentParents as $comment): ?>
            <li class="media comment-item" id="comment-item-<?php echo $comment['id']; ?>">
                <div class="media-left">
                    <div class="avatar-member">
                        <img class="media-object" src="<?php echo!empty($comment['user']['avatar']) ? $comment['user']['avatar'] : '/img/avatars/avatar_default.jpg'; ?>" alt="<?php echo $comment['user']['first_name'] . ' ' . $comment['user']['last_name']; ?>" title="<?php echo $comment['user']['first_name'] . ' ' . $comment['user']['last_name']; ?>" />
                    </div>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $comment['user']['first_name'] . ' ' . $comment['user']['last_name']; ?>
                        <?php if (!empty($userInfo) && ($project['user_id'] == $comment['user_id'])): ?>
                            <span class="member-regency"><?php echo __('Creator'); ?></span><span class="time-post"><?php echo $this->AuthUser->_get_time_ago($comment['created']); ?></span>
                        <?php else: ?>
                            <span class="time-post"><?php echo $this->AuthUser->_get_time_ago($comment['created']); ?></span>
                        <?php endif; ?>
                        <?php if (!empty($userInfo) && $userInfo['id'] == $project['user_id']): ?>
                            <a href="javascript:delete_comment('<?php echo $comment['id']; ?>');"><i class="fa fa-times"></i></a>
                        <?php endif; ?>
                    </h4>
                    <p><?php echo $comment['comment']; ?></p>
                    <?php $listCommentSubs = $this->AuthUser->_get_list_comment_by_parent_id($data, $comment['id']); ?>
                    <?php if (count($listCommentSubs) > 0): ?>
                        <?php foreach ($listCommentSubs as $row): ?>
                            <div class="media <?php echo ($row['user_id'] == $userInfo['id']) ? 'active' : ''; ?>">
                                <div class="media-left">
                                    <div class="avatar-member">
                                        <img class="media-object" src="<?php echo!empty($row['user']['avatar']) ? $row['user']['avatar'] : '/img/avatars/avatar_default.jpg'; ?>" alt="<?php echo $row['user']['first_name'] . ' ' . $row['user']['last_name']; ?>" title="<?php echo $row['user']['first_name'] . ' ' . $row['user']['last_name']; ?>" />
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <?php echo $row['user']['first_name'] . ' ' . $row['user']['last_name']; ?>
                                        <?php if (!empty($userInfo) && ($project['user_id'] == $row['user_id'])): ?>
                                            <span class="member-regency"><?php echo __('Creator'); ?></span><span class="time-post"><?php echo $this->AuthUser->_get_time_ago($row['created']); ?></span>
                                        <?php else: ?>
                                            <span class="time-post"><?php echo $this->AuthUser->_get_time_ago($row['created']); ?></span>
                                        <?php endif; ?>
                                        <?php if (!empty($userInfo) && $project['user_id'] == $userInfo['id']): ?>
                                            <a href="javascript:delete_comment('<?php echo $row['id']; ?>');"><i class="fa fa-times"></i></a>
                                        <?php endif; ?>
                                    </h4>
                                    <p><?php echo $row['comment']; ?></p> 
                                </div> 
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (!empty($userInfo) && $userInfo['id'] == $project['user_id']): ?>
                        <div class="col-xs-12 box-comment-reply"><a class="btn btn-warning btn-launch" href="javascript:void(0);" data="comment-item-<?php echo $comment['id']; ?>" data-comment-id="<?php echo $comment['id']; ?>"><?php echo __('Reply'); ?></a></div>
                        <div class="col-md-12 box-reply-form"></div>
                    <?php endif; ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <script>
        $(document).ready(function () {
            $('.comment-item .box-comment-reply .btn').click(function () {
                $(this).hide();
                var data_box = $(this).attr('data');
                var comment_id = $(this).attr('data-comment-id');
                var project_id = $('#project_id').val();
                $.ajax({
                    type: 'POST',
                    url: '/projectActions/getFormReplyComment',
                    data: {
                        'project_id': project_id,
                        'comment_id': comment_id
                    },
                    success: function (respone) {
                        $('#' + data_box + ' .box-reply-form').html(respone);
                        $('#' + data_box + ' .box-reply-form').toggle();
                    }
                });
            });
        });
    </script>
<?php endif; ?>
