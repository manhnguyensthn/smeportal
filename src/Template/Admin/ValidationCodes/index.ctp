<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>" class="btn btn-success"><?php echo __('Dashboard'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Codes'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 3%">#</th>
                        <th style="width: 30%"><?php echo __('Title'); ?></th>
                        <th><?php echo __('Status'); ?></th>
                        <!-- <th><?php echo __('Status'); ?></th> -->
                        <th style="width: 12%"><?php echo __('Action'); ?></th>
                    </tr>
                    <?php if(!empty($codes)):
                            foreach ($codes as $key => $value):?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->code; ?></td>
                        <td>
                            <?php if($value->status == 0){?>
                                <button class="btn btn-default btn-sm"><?php echo __('Un-Public'); ?></button>
                            <?php }elseif($value->status == 1){?>
                                <button class="btn btn-info btn-sm"><?php echo __('Waiting for activate'); ?></button>
                            <?php }else{?> 
                                <button class="btn btn-primary btn-sm"><?php echo __('Active'); ?></button>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if($value->status == 0){?>
                                <a href="<?= $this->Link->build(['action' => 'toBeFeature/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Activate'); ?></a>
                            <?php }if($value->status == 1){?> 
                                <a href="<?= $this->Link->build(['action' => 'toBeFeature/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Un-activate'); ?></a>
                            <?php } ?>
                            
                            <?= $this->Form->postLink(
                                'Delete',
                                ['action' => 'delete', $value->id],
                                ['confirm' => 'Are you sure?','class' => 'btn btn-danger btn-sm'])
                            ?>
                        </td>
                    </tr>
                        <?php endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>
        <?= $this->element('admin/pagination');?>
    </div>
</section>