<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        Nhập thông tin tìm kiếm
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Projects'); ?></li>
    </ol>
      <?php
        echo $this->Form->create('projects', array(
            'type' => 'get',
        ));
        ?>
    <div class="content-left" style="float: left;width: 45%">
        <?php
                 if(isset($this->request->query['title'])) {
                     $title= $this->request->query['title'];
                 } else {
                     $title = '';
                 }                    
                 echo $this->Form->input('title', array(
                     'type' => 'text',
                     'label' => 'Tiêu đề',
                     'class' => 'form-control',
                     'value' => $title,
                 ));
        ?>
      </div>  
    <div style="clear: both"></div>
        <?php
        echo $this->Form->submit('Tìm kiếm', array(
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-primary search'
        ));
        echo $this->Form->end();
        ?>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 3%">#</th>
                        <th style="width: 600px"><?php echo __('Title'); ?></th>
                        <th><?php echo __('Image'); ?></th>
                        <th><?php echo __('Status'); ?></th>
                        <th><?php echo __('Scales'); ?></th>
                        <th><?php echo __('Created'); ?></th>
                        <th><?php echo __('Ban chấp hành'); ?></th>
                        <th style="width: 12%"><?php echo __('Action'); ?></th>
                        <th>Hội phí</th>
                        <th style="width: 400px"><?php echo __('Ban chấp hành'); ?></th>
                    </tr>
                    <?php if(!empty($projects)):
                            foreach ($projects as $key => $value):?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><a target="_blank" href="<?= ROOT_URL. 'projects/' . $this->Tool->slug($value->title) . '-'. $value->id; ?>"><?= $value->title; ?></a></td>
                        <td>
                            <?php if(!empty($value->image_url)){?>
                                <img src="<?= $value->image_url; ?>" alt="<?= $value->image_url; ?>" id="image_path" width="100" height="100" />
                            <?php }else{
                                echo $this->Html->image('noimage/item.png', ['alt' => $value->image_url, 'id'=> 'image_path', 'width'=> 100, 'height'=>100]);
                            } ?>
                        </td>
                        <td>
                            <?php if($value->status == 0){?>
                                <button style="width: 90px" class="btn btn-default btn-sm"><?php echo __('Un-Public'); ?></button>
                            <?php }elseif($value->status == 1){?>
                                <button style="width: 90px" class="btn btn-info btn-sm"><?php echo __('Public'); ?></button>
                            <?php }else{?>
                                <button style="width: 90px" class="btn btn-primary btn-sm"><?php echo __('Feature'); ?></button>
                            <?php } ?>
                        </td>
                        <td><?= $value->scale_id; ?></td>
                        <td><?= $value->created; ?></td>
                        <td><?php echo $value->group['name'] ?></td>
                        <td>
                            <?php if($value->status == 1){?>
                                <a style="width: 90px" href="<?= $this->Link->build(['action' => 'toBeFeature/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Feature'); ?></a>
                            <?php }if($value->status == 2){?>
                                <a style="width: 90px" href="<?= $this->Link->build(['action' => 'toBeFeature/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Un-Feature'); ?></a>
                            <?php } ?>
                                  <a href="#" class="btn btn-danger btn-sm" onclick="xoa(<?= $value->id ?>)">Xóa</a>
                                
                        </td>
                        <td>
                            <?php if($value->fee_status == 0){?>
                                <a style="width:130px" href="<?= $this->Link->build(['action' => 'toBeFee/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('=> Đóng hội phí'); ?></a>
                            <?php }if($value->fee_status == 1){?>
                                <a style="width:130px" href="<?= $this->Link->build(['action' => 'toBeFee/'.$value->id]); ?>" class="btn btn-warning btn-sm"><?php echo __('=> Chưa đóng hội phí'); ?></a>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if($value->group_id != 2){?>
                                <a style="width:130px" href="<?= $this->Link->build(['action' => 'toBeStaff/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('=> Tạo phó chủ tịch'); ?></a>
                            <?php }if($value->group_id == 2){?>
                                <a style="width:130px" href="<?= $this->Link->build(['action' => 'toBeStaff/'.$value->id]); ?>" class="btn btn-warning btn-sm"><?php echo __('=> Hủy phó chủ tịch'); ?></a>
                            <?php } ?>
                        </td>
                    </tr>
                        <?php endforeach;
                        endif;
                    ?>
                </tbody>
                <div id="result">
                    
                </div>
            </table>
        </div>
        <?= $this->element('admin/pagination');?>
    </div>
</section>
<script>
    function xoa(id) {
        var r = confirm("Bạn có chắc chắn muốn xóa không?");
        if (r == true) {
          $.ajax({
             url: "/admin/projects/delete/"+id,
             type: "post",
             data: {
             },
             success: function (result) {
                  location.reload();
             }
         });
        } else {
            return false;
        }
    }
</script>
<style>
    .content-header>h1 {
    margin: 0;
    font-size: 24px;
    text-align: center;
    margin-bottom: 20px;
    margin-top: 20px;
}
.search {
    margin-top: 24px;
    margin-left: 40%;
    padding: 10px 100px 10px 100px;
}
</style>