<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'Sliders','action' => 'add']); ?>" class="btn btn-success"><?php echo __('Add sliders'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Home'); ?></a></li>
        <li class="active"><?php echo __('Sliders'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 3%">#</th>
                        <th style="width: 10%"><?php echo __('Image'); ?></th>
                        <th><?php echo __('Title'); ?></th>
                        <th><?php echo __('Link'); ?></th>
                        <th><?php echo __('Created'); ?></th>
                        <th><?php echo __('Updated'); ?></th>
                        <th style="width: 12%"><?php echo __('Action'); ?></th>
                    </tr>
                    <?php if(!empty($sliders)):
                            foreach ($sliders as $key => $value):?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td>
                            <?php if(!empty($value->image_path)){?>
                                <img src="<?= ROOT_URL.$value->image_path; ?>" alt="<?= $value->image_path; ?>" id="image_path" width="100" height="100" />
                            <?php }else{
                                echo $this->Html->image('phuquoc_logo.png', ['alt' => $value->image_path, 'id'=> 'image_path', 'width'=> 100, 'height'=>100]);
                            } ?>
                        </td>
                        <td><?= $value->title; ?></td>
                        <td><?= $value->url; ?></td>
                        <td><?= $value->created; ?></td>
                        <td><?= $value->updated; ?></td>
                        <td>
                            <a href="<?= $this->Link->build(['controller' => 'Sliders','action' => 'edit/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Edit'); ?></a>
                            <?= $this->Form->postLink(
                                'Delete',
                                ['action' => 'delete', $value->id],
                                ['confirm' => 'Are you sure?','class' => 'btn btn-danger btn-sm'])
                            ?>
                        </td>
                    </tr>
                        <?php endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>
        <?= $this->element('admin/pagination');?>
    </div>
</section>