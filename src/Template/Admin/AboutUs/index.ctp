<?php


?>
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Roles'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 3%">#</th>
                    <th><?php echo __('Title'); ?></th>
                    <th style="width: 12%"><?php echo __('Action'); ?></th>
                </tr>
                <?php if(!empty($aboutus)):
                    foreach ($aboutus as $key => $value):?>
                        <tr>
                            <td><?= $value->id; ?></td>
                            <td><?= $value->title; ?></td>
                            <td>
                                <a href="<?= $this->Link->build(['controller' => 'AboutUs','action' => 'edit/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Edit'); ?></a>
                            </td>
                        </tr>
                    <?php endforeach;
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>