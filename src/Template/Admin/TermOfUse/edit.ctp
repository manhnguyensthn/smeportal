<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript" src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
<script type="text/javascript">
    jQuery(function () {
        CKEDITOR.replace('pitch');
    });
</script>
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
        <!--<li><a href="<?/*= $this->Link->build(['controller' => 'roles','action' => 'index']); */?>"><?php /*echo __('roles'); */?></a></li>-->
        <!--<li class="active"><?php /*echo __('Add roles'); */?></li>-->
    </ol>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header with-border">
            <!--<h3 class="box-title">Tạo mới roles</h3>-->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Form->create($termofuse);?>
            <div class="form-group">
                <?= $this->Form->input('title',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Title', 'class'=>'form-control', 'placeholder'=>__('Please enter title ...')]); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->input('content',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Content', 'class'=>'ckeditor', 'placeholder'=>__('Please enter title ...')]); ?>
            </div>
            <div class="form-group">
                <div class="pull-right">
                    <?= $this->Form->button('Save',['class'=>'btn btn-primary']); ?>
                </div>
            </div>
            <?= $this->Form->end();?>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<script>
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_path')
                    .attr('src', e.target.result)
                    .width(750)
                    .height(350);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>