<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'Quantities','action' => 'index']); ?>" class="btn btn-info"><?php echo __('List Quantities'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
        <li><a href="<?= $this->Link->build(['controller' => 'Quantities','action' => 'index']); ?>"><?php echo __('Quantities'); ?></a></li>
        <li class="active"><?php echo __('Add Quantities'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header with-border">
            <!--<h3 class="box-title">Tạo mới Categories</h3>-->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Form->create($Quantities);?>
            <div class="form-group">
                <?= $this->Form->input('name',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Title', 'class'=>'form-control', 'placeholder'=>__('Please enter Quantities name ...')]); ?>
            </div>

            <div class="form-group">
                <div class="pull-right">
                    <?= $this->Form->button('Add',['class'=>'btn btn-primary']); ?>
                </div>
            </div>
            <?= $this->Form->end();?>
        </div>
        <!-- /.box-body -->
    </div>
</section>