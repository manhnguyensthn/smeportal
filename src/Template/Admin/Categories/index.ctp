<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'Categories','action' => 'add']); ?>" class="btn btn-success"><?php echo __('Add Categories'); ?></a>
    </h1>
    <h1 class="tt">
        Nhập thông tin tìm kiếm
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Categories'); ?></li>
    </ol>
     <?php
        echo $this->Form->create('projects', array(
            'type' => 'get',
        ));
        ?>
    <div class="content-left" style="float: left;width: 45%">
        <?php
                 if(isset($this->request->query['title'])) {
                     $title= $this->request->query['title'];
                 } else {
                     $title = '';
                 }                    
                 echo $this->Form->input('title', array(
                     'type' => 'text',
                     'label' => 'Tiêu đề',
                     'class' => 'form-control',
                     'value' => $title,
                 ));
        ?>
      </div>  
    <div style="clear: both"></div>
        <?php
        echo $this->Form->submit('Tìm kiếm', array(
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-primary search'
        ));
        echo $this->Form->end();
        ?>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 3%">#</th>
                        <th><?php echo __('Name'); ?></th>
                        <th><?php echo __('Đã tạo'); ?></th>
                        <th><?php echo __('Đã sửa'); ?></th>
                        <th style="width: 12%"><?php echo __('Action'); ?></th>
                    </tr>
                    <?php if(!empty($categories)):
                            foreach ($categories as $key => $value):?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->name; ?></td>
                        <td><?= $value->created; ?></td>
                        <td><?= $value->modified; ?></td>
                        <td>
                            <a href="<?= $this->Link->build(['controller' => 'Categories','action' => 'edit/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Edit'); ?></a>
                             <a href="#" class="btn btn-danger btn-sm" onclick="xoa(<?= $value->id ?>)">Xóa</a>
                        </td>
                    </tr>
                        <?php endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>
        <?= $this->element('admin/pagination');?>
    </div>
</section>
<script>
    function xoa(id) {
        var r = confirm("Bạn có chắc chắn muốn xóa không?");
        if (r == true) {
          $.ajax({
             url: "/admin/categories/delete/"+id,
             type: "post",
             data: {
             },
             success: function (result) {
                  location.reload();
             }
         });
        } else {
            return false;
        }
    }
</script>
<style>
    .content-header .tt{
    margin: 0;
    font-size: 24px;
    text-align: center;
    margin-bottom: 20px;
    margin-top: 20px;
}
.search {
    margin-top: 24px;
    margin-left: 40%;
    padding: 10px 100px 10px 100px;
}
</style>