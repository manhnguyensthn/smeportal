
<section class="content-header">
    <!-- <h1>
        <a href="<?= $this->Link->build(['controller' => 'UsersType','action' => 'add']); ?>" class="btn btn-success"><?php echo __('Add Groups'); ?></a>
    </h1> -->
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Groups'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 3%">Mã</th>
                        <th><?php echo __('Name'); ?></th>
                        <th style="width: 12%"><?php echo __('Action'); ?></th>
                    </tr>
                    <?php if(!empty($userstype)):
                            foreach ($userstype as $key => $value):?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->name; ?></td>
                        <!-- <td>
                            <a href="<?= $this->Link->build(['controller' => 'Groups','action' => 'edit/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Edit'); ?></a>
                            <?= $this->Form->postLink(
                                'Delete',
                                ['action' => 'delete', $value->id],
                                ['confirm' => 'Are you sure?','class' => 'btn btn-danger btn-sm'])
                            ?>
                        </td> -->
                    </tr>
                        <?php endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>
        <?= $this->element('admin/pagination');?>
    </div>
</section>
