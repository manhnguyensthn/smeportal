<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'Scales','action' => 'index']); ?>" class="btn btn-info"><?php echo __('List Scales'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
        <li><a href="<?= $this->Link->build(['controller' => 'Scales','action' => 'index']); ?>"><?php echo __('Scales'); ?></a></li>
        <li class="active"><?php echo __('Edit Scales   '); ?></li>

    </ol>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Form->create($Scales);?>
            <div class="form-group">
                <?= $this->Form->input('name',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Quy mô', 'class'=>'form-control', 'placeholder'=>__('Xin hãy nhập quy mô vốn')]); ?>
            </div>
            <div class="form-group">
                <div class="pull-right">
                    <?= $this->Form->button('Save',['class'=>'btn btn-primary']); ?>
                </div>
            </div>
            <?= $this->Form->end();?>
        </div>
        <!-- /.box-body -->
    </div> 
</section>
