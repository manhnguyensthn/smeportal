<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <?php echo __('Weekly Receive'); ?>
        <?php if($bEdit): ?>
            <a href="<?= $this->Link->build(['controller' => 'weekly-receive','action' => 'edit']); ?>" class="btn btn-success"><?php echo __('Add template'); ?></a>
        <?php endif; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
        <li><a href="<?= $this->Link->build(['controller' => 'WeeklyReceive','action' => 'index']); ?>"><?php echo __('Weekly Receive'); ?></a></li>
    </ol>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-body">
            <?= $this->Flash->render() ?>
            <div class="wrapper_form_mail">
                <?= $this->Form->create($weeklyReceive);?>
                <div class="form-group">
                    <?= $this->Form->input('title',['required' => true, 'autofocus' => true, 'id'=>'title', 'label'=>'Title', 'class'=>'form-control', 'placeholder'=>__('Please enter title ...')]); ?>
                </div>
                <div class="form-group" style="min-height: 329px;">
                    <label for="content">Content</label>
                    <?= $this->Form->textarea('content',['required' => true, 'id'=>'content', 'label'=>false, 'class'=>'form-control', 'placeholder'=>__('Please enter content ...')]); ?>
                </div>
                <div class="form-group text-center">
                    <?php
                        if($bEdit){
                            echo $this->Form->button(__('Send Mail'),['type'=>'button', 'class'=>'btn btn-danger', 'style'=>'margin-right: 5px;', 'onclick'=>'sendNewsletter(this, \'user\', 1)']);
                            echo $this->Form->button(__('Update Template Mail'),['class'=>'btn btn-primary']);
                        }else{
                            echo $this->Form->button(__('Add Template Mail'),['class'=>'btn btn-primary']);
                        }
                    ?>
                </div>
                <?= $this->Form->end();?>
            </div>

            <?php if($bEdit): ?>
                <table class="result_sent_mail table table-bordered" style="display: none;">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 250px"><?php echo __('Email'); ?></th>
                            <th class="text-center"><?php echo __('Name'); ?></th>
                            <th class="text-center"><?php echo __('Type'); ?></th>
                            <th class="text-center" style="width: 150px"><?php echo __('Status'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</section>

<script type="text/javascript" src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
<script type="text/javascript">
    jQuery(function () {
        CKEDITOR.replace('content');
    });
</script>
<script type="text/javascript">
function sendNewsletter(ele, TypeId, page){
    if(TypeId == 'user' && page == 1){
        $('.result_sent_mail tbody').html('');
    }

    var sTitle = $('#title').val();
    var sContent = $('#content').val();
    $.ajax({
        type: 'POST',
        url: '<?php echo ROOT_URL . 'users/sendNewsletter' ?>',
        data: {
            'title': sTitle,
            'content': sContent,
            'TypeId': TypeId,
            'page': page,
        },
        beforeSend: function(){
            $('.wrapper_form_mail').hide();
            $('.result_sent_mail').show();
        },
        success: function (respone) {
            var result = JSON.parse(respone);
            if(result.data.bContinue == 1){
                $('.result_sent_mail tbody').append(result.data.HtmlListMails);
                sendNewsletter(ele, result.data.TypeId, result.data.page);
            }else{
                $('.wrapper_form_mail').show();
            }
        }
    });
}
</script>