<section class="content-header">
    <h1><a href="<?= $this->Link->build(['controller' => 'weekly-receive','action' => 'edit']); ?>" class="btn btn-success"><?php echo __('Add template'); ?></a></h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
        <li><a href="<?= $this->Link->build(['controller' => 'WeeklyReceive','action' => 'index']); ?>"><?php echo __('Weekly Receive'); ?></a></li>
    </ol>
</section>

<section class="content">
    <div class="box box-warning">
        <div class="box-body">
        	<?php if(isset($WeeklyReceives) && !empty($WeeklyReceives)): ?>
        	<table class="table table-bordered">
                <tbody>
                    <tr>
                        <th class="text-center" style="width: 30px">#</th>
                        <th class="text-center" style="width: 250px"><?php echo __('Title'); ?></th>
                        <th class="text-center"><?php echo __('Content'); ?></th>
                        <th class="text-center" style="width: 150px"><?php echo __('Created'); ?></th>
                        <th class="text-center" style="width: 150px"><?php echo __('Modified'); ?></th>
                        <th class="text-center" style="width: 50px"><?php echo __('Action'); ?></th>
                    </tr>
                    <?php foreach ($WeeklyReceives as $key => $WeeklyReceive) : ?>
                    	<tr>
	                        <th class="text-center"><?php echo $key + 1; ?></th>
	                        <th><a href="<?= $this->Link->build(['controller' => 'weekly-receive','action' => 'edit/' . $WeeklyReceive->id]); ?>"><?php echo $WeeklyReceive->title; ?></a></th>
	                        <th><?php echo substr(strip_tags($WeeklyReceive->content), 0, 100); ?></th>
	                        <th class="text-center"><?= $WeeklyReceive->created; ?></th>
	                        <th class="text-center"><?= $WeeklyReceive->updated; ?></th>
	                        <th class="text-center">
	                        	<a href="<?= $this->Link->build(['controller' => 'weekly-receive','action' => 'index', 'delete' => $WeeklyReceive->id]); ?>" onclick="if(!confirm('Are you sure you want to delete?')) return false;" class="btn btn-danger btn-sm btn-block" style="margin-bottom: 5px;"><?php echo __('Delete'); ?></a>
	                        </th>
	                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
	        <?php endif; ?>
        </div>
    </div>
</section>