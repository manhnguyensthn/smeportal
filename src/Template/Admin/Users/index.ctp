
<section class="content-header">
    <h1>
        Nhập thông tin tìm kiếm
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard', 'action' => 'index']); ?>"><i
                        class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Users'); ?></li>
    </ol>
    
        <?php
        echo $this->Form->create('user', array(
            'type' => 'get',
        ));
        ?>
    <div class="content-left" style="float: left;width: 45%">
        <?php
                 if(isset($this->request->query['username'])) {
                     $username = $this->request->query['username'];
                 } else {
                     $username = '';
                 }                    
                 echo $this->Form->input('username', array(
                     'type' => 'text',
                     'label' => 'User Name',
                     'class' => 'form-control',
                     'value' => $username,
                 ));
        ?>
      </div>  
    <div class="content-right" style="float: right;width: 45%">
        <?php
                 if(isset($this->request->query['email'])) {
                     $email = $this->request->query['email'];
                 } else {
                     $email = '';
                 }  
                 echo $this->Form->input('email', array(
                     'type' => 'text',
                     'label' => 'Email',
                     'value' => $email,
                     'class' => 'form-control',
                 ));
        ?>
      </div>
    <div style="clear: both"></div>
        <?php
        echo $this->Form->submit('Tìm kiếm', array(
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-primary search'
        ));
        echo $this->Form->end();
        ?>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 3%">#</th>
                    <th><?php echo __('Name'); ?></th>
                    <th><?php echo __('Avatar'); ?></th>
                    <th><?php echo __('Email'); ?></th>
                    <th><?php echo __('Quyền'); ?></th>
                    <th><?php echo __('Created'); ?></th>
                    <th style="width: 5%"><?php echo __('Action'); ?></th>
                </tr>
                <?php if (!empty($users)):
                    foreach ($users as $key => $value):?>
                        <tr>
                            <td><?= $value->id; ?></td>
                            <td><?= $value->name; ?></td>
                            <td>
                                <?php if (!empty($value->avatar)) { ?>
                                    <img src="<?= $value->avatar; ?>" alt="<?= $value->name; ?>" id="image_path"
                                         width="50" height="50"/>
                                <?php } else {
                                    echo $this->Html->image('noimage/item.png', ['alt' => $value->name, 'id' => 'image_path', 'width' => 50, 'height' => 50]);
                                } ?>
                            </td>
                            <td><?= $value->email; ?></td>
                                <?php
                                switch ($value->admin) {
                                    case 0:
                                    $adminType = [0=>'USER'];
                                    $option = [1=>'QLHV',2 => 'PL', 3 => 'XTTM', 4 => 'VV', 5 => 'Mod'];
                                    break;
                                    case 1:
                                        $adminType = [1=>'QLHV'];
                                        $option = [0=>'USER',2 => 'PL', 3 => 'XTTM', 4 => 'VV', 5 => 'Mod'];
                                        break;
                                    case 2:
                                        $adminType = [2=>'PL'];
                                        $option = [0=>'USER',1 => 'QLHV', 3 => 'XTTM', 4 => 'VV', 5 => 'Mod'];
                                        break;
                                    case 3:
                                        $adminType = [3=>'XTTM'];
                                        $option = [0=>'USER',1 => 'QLHV', 2 => 'PL', 4 => 'VV', 5 => 'Mod'];
                                        break;
                                    case 4:
                                        $adminType = [4=>'VV'];
                                        $option = [0=>'USER',1 => 'QLHV', 2 => 'PL', 3 => 'XTTM', 5 => 'Mod'];
                                        break;
                                    case 5:
                                        $adminType = [5=>'Mod'];
                                        $option = [0=>'USER',1 => 'QLHV', 2 => 'PL', 3 => 'XTTM', 4 => 'VV'];
                                        break;
                                    default:
                                        $adminType = "-- Chọn BCH --";
                                        $option = [0=>'USER',1 => 'QLHV', 2 => 'PL', 3 => 'XTTM', 4 => 'VV', 5 => 'Mod'];
                                }
                                ?>
                            <td>
                                <input id="user_id" name="user_id" type="hidden" value="<?= $value->id ?>">
                                <?php echo $this->Form->input('groups', [
                                    'class' => 'form-control',
                                    'id' => 'group'.$value->id,
                                    'label' => false,
                                    'type' => 'select',
                                    'default' => $adminType,
                                    'options' => $option,
                                    'empty' => $adminType,
                                    'templates' => [
                                        'inputContainer' => '<div class="col-md-8 input padd-left {{type}}{{required}}">{{content}}</div>'
                                    ]
                                ]); ?>
                                <button id="submitAdmin" onclick = "thay_quyen(<?= $value->id ?>)" class="btn btn-warning">Chỉ định</button>
                            </td>
                            <td><?= $value->created; ?></td>
                           <td>
                            <a href="#" class="btn btn-danger btn-sm" onclick="xoa(<?= $value->id ?>)">Xóa</a>
                           </td>
                        </tr>
                    <?php endforeach;
                endif;
                ?>
                </tbody>
            </table>
        </div>
        <?= $this->element('admin/pagination'); ?>
    </div>
</section>
<script>
    function thay_quyen(user_id) {
         var group = $("#group"+user_id).val();
         $.ajax({
             url: "/admin/users/toBeAdmin",
             type: "post",
             data: {
                 user_id:user_id,
                 group:group
             },
             success: function (result) {
                  location.reload();
             }
         });
    }
    function xoa(id) {
        var r = confirm("Bạn có chắc chắn muốn xóa không?");
        if (r == true) {
          $.ajax({
             url: "/admin/users/delete/"+id,
             type: "post",
             data: {
             },
             success: function (result) {
                  location.reload();
             }
         });
        } else {
            return false;
        }
    }
</script>
<style>
    .content-header>h1 {
    margin: 0;
    font-size: 24px;
    text-align: center;
    margin-bottom: 20px;
    margin-top: 20px;
}
.search {
    margin-top: 24px;
    margin-left: 40%;
    padding: 10px 100px 10px 100px;
}
</style>
