<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="login-box">
    <div class="login-logo">
        <a href="<?= ROOT_URL; ?>"><b>SME</b>Control Panel</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo __('Please enter username and password to login!'); ?></p>

        <?= $this->Form->create(); ?>
            <div class="form-group has-feedback">
                <input type="text" autofocus="" name="username" class="form-control" placeholder="<?php echo __('Username'); ?>">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="<?php echo __('Password'); ?>">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Login'); ?></button>
                </div>
                <!-- /.col -->
            </div>
        <?= $this->Form->end(); ?>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
