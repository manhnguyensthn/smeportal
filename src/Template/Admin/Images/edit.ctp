<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'images','action' => 'index']); ?>" class="btn btn-info"><?php echo __('Danh sách ảnh'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Trang chủ'); ?></a></li>
        <li><a href="<?= $this->Link->build(['controller' => 'images','action' => 'index']); ?>"><?php echo __('images'); ?></a></li>
        <li class="active"><?php echo __('Sửa ảnh'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header with-border">
            <!--<h3 class="box-title">Tạo mới images</h3>-->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Form->create($images,['type'=>'file']);?>
            <div class="form-group">
                <div class="col-md-3" style="margin-top: 50px">
                    <?= $this->Form->input('url',[
                        'div'=>false, 'class'=>'form-control', 
                        'type' => 'file', 'accept' => 'image/jpeg, image/png',
                        'label' => 'Đường dẫn ảnh',
                        'required' => true,
                        'onchange' => 'readImage(this);'
                    ]); ?>            
                </div>
                <div class="col-md-9">
                    <?php if(!empty($images->url)){ ?>
                        <img style="zoom:70%;" src="<?= ROOT_URL.$images->url; ?>" alt="Icon" id="image_path" width="750" height="350" />
                    <?php }else{
                        echo $this->Html->image('phuquoc_logo.png', ['alt' => 'Icon', 'id'=> 'image_path', 'width'=> 750, 'height'=>350]);
                    }?>
                </div>
            </div>
            <div class="form-group"></div>
            <div class="form-group">
                <div class="pull-right">
                    <?= $this->Form->button('Save',['class'=>'btn btn-primary']); ?>
                </div>
            </div>
            <?= $this->Form->end();?>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<script>
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_path')
                        .attr('src', e.target.result)
                        .width(750)
                        .height(350);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>