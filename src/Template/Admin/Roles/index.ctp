<?php


?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'Roles','action' => 'add']); ?>" class="btn btn-success"><?php echo __('Add Role'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i><?php echo __('Dashboard'); ?></a></li>
        <li class="active"><?php echo __('Roles'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="row" style="margin-left: 0; margin-right: 0">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 3%">#</th>
                        <th><?php echo __('Role'); ?></th>
                        <!-- <th><?php echo __('Role Es'); ?></th> -->
                        <!-- <th><?php echo __('Role Ja'); ?></th> -->
                        <!-- <th><?php echo __('role type'); ?></th> -->
                        <!-- <th><?php echo __('Quantity'); ?></th> -->
                        <!-- <th><?php echo __('Lang_id'); ?></th> -->
                        <th><?php echo __('icon'); ?></th>
                        <!-- <th><?php echo __('Group'); ?></th> -->
                        <!-- <th><?php echo __('Created'); ?></th> -->
                        <th style="width: 12%"><?php echo __('Action'); ?></th>
                    </tr>
                    <?php if(!empty($roles)):
                            foreach ($roles as $key => $value):?>
                    <tr>
                        <td><?= $value->id; ?></td>
                        <td><?= $value->role; ?></td>
                        <!-- <td><?= $value->role_es; ?></td> -->
                        <!-- <td><?= $value->role_ja; ?></td> -->
                        <!-- <td><?= $value->role_type; ?></td> -->
                        <!-- <td><?= $value->quantity; ?></td> -->
                        <!-- <td><?= $value->lang_id; ?></td> -->
                        <td><img style="width:100px;" src="<?= $value->icon; ?>"/></td>
                        <!-- <td><?= $value->group; ?></td> -->
                        <!-- <td><?= $value->created; ?></td> -->
                        <td>
			  <a href="<?= $this->Link->build(['controller' => 'Roles','action' => 'edit/'.$value->id]); ?>" class="btn btn-primary btn-sm"><?php echo __('Edit'); ?></a>
                            <?= $this->Form->postLink(
                                'Delete',
                                ['action' => 'delete', $value->id],
                                ['confirm' => 'Are you sure?','class' => 'btn btn-danger btn-sm'])
                            ?>
                        </td>
                    </tr>
                        <?php endforeach;
                        endif;
                    ?>
                </tbody>
            </table>
        </div>
        <?= $this->element('admin/pagination');?>
    </div>
</section>