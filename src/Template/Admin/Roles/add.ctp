<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="content-header">
    <h1>
        <a href="<?= $this->Link->build(['controller' => 'roles','action' => 'index']); ?>" class="btn btn-info"><?php echo __('List roles'); ?></a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
        <li><a href="<?= $this->Link->build(['controller' => 'roles','action' => 'index']); ?>"><?php echo __('roles'); ?></a></li>
        <li class="active"><?php echo __('Add roles'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-warning">
        <div class="box-header with-border">
            <!--<h3 class="box-title">Tạo mới roles</h3>-->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Form->create($roles,['type'=>'file']);?>
            <div class="form-group">
                <?= $this->Form->input('role',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Role', 'class'=>'form-control', 'placeholder'=>__('Please enter title ...')]); ?>
            </div>
			<div class="form-group">
                <?= $this->Form->input('role_es',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Role ES', 'class'=>'form-control', 'placeholder'=>__('Please enter title ...')]); ?>
            </div>
			<div class="form-group">
                <?= $this->Form->input('role_ja',['required' => true, 'autofocus' => true, 'div'=>false, 'label'=>'Role JA', 'class'=>'form-control', 'placeholder'=>__('Please enter title ...')]); ?>
            </div>
            <div class="form-group">
                <div class="col-md-3" style="margin-top: 50px">
                    <?= $this->Form->input('icon',[
                        'div'=>false, 'class'=>'form-control', 
                        'type' => 'file', 'accept' => 'image/jpeg, image/png',
                        'label' => 'Icon',
                        'required' => true,
                        'onchange' => 'readImage(this);'
                    ]); ?>            
                </div>
                <div class="col-md-9">
                    <?php if(!empty($roles->image_path)){?>
                        <img src="<?= ROOT_URL.$roles->image_path; ?>" alt="Icon" id="image_path" width="750" height="350" />
                    <?php }else{
                        echo $this->Html->image('phuquoc_logo.png', ['alt' => 'Icon', 'id'=> 'image_path', 'width'=> 750, 'height'=>350]);
                    }?>
                </div>
            </div>
            <div class="form-group"></div>
            <div class="form-group">
                <div class="pull-right">
                    <?= $this->Form->button('Add',['class'=>'btn btn-primary']); ?>
                </div>
            </div>
            <?= $this->Form->end();?>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<script>
    function readImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_path')
                        .attr('src', e.target.result)
                        .width(750)
                        .height(350);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>