<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit District'), ['action' => 'edit', $district->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete District'), ['action' => 'delete', $district->id], ['confirm' => __('Are you sure you want to delete # {0}?', $district->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Districts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New District'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List States'), ['controller' => 'States', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New State'), ['controller' => 'States', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="districts view large-9 medium-8 columns content">
    <h3><?= h($district->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($district->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= $district->has('state') ? $this->Html->link($district->state->name, ['controller' => 'States', 'action' => 'view', $district->state->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($district->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country Id') ?></th>
            <td><?= $this->Number->format($district->country_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postal Code') ?></th>
            <td><?= $this->Number->format($district->postal_code) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Projects') ?></h4>
        <?php if (!empty($district->projects)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Image Url') ?></th>
                <th scope="col"><?= __('Video Url') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('District Id') ?></th>
                <th scope="col"><?= __('Country Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Process') ?></th>
                <th scope="col"><?= __('Tags') ?></th>
                <th scope="col"><?= __('Max Day') ?></th>
                <th scope="col"><?= __('Min Day') ?></th>
                <th scope="col"><?= __('Synopsis') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('Pitch') ?></th>
                <th scope="col"><?= __('Process Type') ?></th>
                <th scope="col"><?= __('Lang Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($district->projects as $projects): ?>
            <tr>
                <td><?= h($projects->id) ?></td>
                <td><?= h($projects->title) ?></td>
                <td><?= h($projects->image_url) ?></td>
                <td><?= h($projects->video_url) ?></td>
                <td><?= h($projects->description) ?></td>
                <td><?= h($projects->category_id) ?></td>
                <td><?= h($projects->user_id) ?></td>
                <td><?= h($projects->district_id) ?></td>
                <td><?= h($projects->country_id) ?></td>
                <td><?= h($projects->status) ?></td>
                <td><?= h($projects->type) ?></td>
                <td><?= h($projects->process) ?></td>
                <td><?= h($projects->tags) ?></td>
                <td><?= h($projects->max_day) ?></td>
                <td><?= h($projects->min_day) ?></td>
                <td><?= h($projects->synopsis) ?></td>
                <td><?= h($projects->end_date) ?></td>
                <td><?= h($projects->start_date) ?></td>
                <td><?= h($projects->pitch) ?></td>
                <td><?= h($projects->process_type) ?></td>
                <td><?= h($projects->lang_id) ?></td>
                <td><?= h($projects->created) ?></td>
                <td><?= h($projects->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Projects', 'action' => 'view', $projects->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Projects', 'action' => 'edit', $projects->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Projects', 'action' => 'delete', $projects->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projects->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($district->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Avatar') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Phone Cell') ?></th>
                <th scope="col"><?= __('Phone Work') ?></th>
                <th scope="col"><?= __('User Skill') ?></th>
                <th scope="col"><?= __('Birthday') ?></th>
                <th scope="col"><?= __('District Id') ?></th>
                <th scope="col"><?= __('State Id') ?></th>
                <th scope="col"><?= __('Country Id') ?></th>
                <th scope="col"><?= __('Postal Code') ?></th>
                <th scope="col"><?= __('About Me') ?></th>
                <th scope="col"><?= __('Biography') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Age') ?></th>
                <th scope="col"><?= __('Sex') ?></th>
                <th scope="col"><?= __('Fb Id') ?></th>
                <th scope="col"><?= __('Google Id') ?></th>
                <th scope="col"><?= __('Linked Id') ?></th>
                <th scope="col"><?= __('Token') ?></th>
                <th scope="col"><?= __('Time Limit') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($district->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->avatar) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->phone_cell) ?></td>
                <td><?= h($users->phone_work) ?></td>
                <td><?= h($users->user_skill) ?></td>
                <td><?= h($users->birthday) ?></td>
                <td><?= h($users->district_id) ?></td>
                <td><?= h($users->state_id) ?></td>
                <td><?= h($users->country_id) ?></td>
                <td><?= h($users->postal_code) ?></td>
                <td><?= h($users->about_me) ?></td>
                <td><?= h($users->biography) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->first_name) ?></td>
                <td><?= h($users->last_name) ?></td>
                <td><?= h($users->age) ?></td>
                <td><?= h($users->sex) ?></td>
                <td><?= h($users->fb_id) ?></td>
                <td><?= h($users->google_id) ?></td>
                <td><?= h($users->linked_id) ?></td>
                <td><?= h($users->token) ?></td>
                <td><?= h($users->time_limit) ?></td>
                <td><?= h($users->status) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
