<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Validation Codes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="validationCodes form large-9 medium-8 columns content">
    <?= $this->Form->create($validationCode) ?>
    <fieldset>
        <legend><?= __('Add Validation Code') ?></legend>
        <?php
            echo $this->Form->input('code');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
