<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Validation Code'), ['action' => 'edit', $validationCode->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Validation Code'), ['action' => 'delete', $validationCode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $validationCode->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Validation Codes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Validation Code'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="validationCodes view large-9 medium-8 columns content">
    <h3><?= h($validationCode->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($validationCode->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($validationCode->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($validationCode->status) ?></td>
        </tr>
    </table>
</div>
