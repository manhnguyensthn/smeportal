<div class="row">
    <div class="col-md-12">
        <ul class="list-findmore list-role-applicant">
            <?php foreach ($listFindmore as $index => $row): ?>
                <li <?php echo ($index % 2 == 1) ? 'class="odd"' : ''; ?> >
                    <div class="row role-item">
                        <a href="javascript:show_list_collaborator_findmore('<?php echo $row->role_id; ?>','<?php echo $project_id; ?>');"><div class="col-md-12 role-name"><img class="role-icon" src="<?php echo $row->icon; ?>" alt="<?php echo $row->title; ?>" title="<?php echo $row->title; ?>" /><strong><?php echo $row->title; ?></strong></div></a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-md-12 text-center applicant-loading" style="display: none"><img src="/img/loading.gif" alt="Loading" title="Loading" /><input name="current_page" type="hidden" value="1" /></div>
    <div class="col-md-12 text-center"><button class="btn btn-launch see-more-button"  onclick="load_more_applicants('<?php echo $project_id; ?>');" ><?php echo __('See more'); ?></button></div>
</div>