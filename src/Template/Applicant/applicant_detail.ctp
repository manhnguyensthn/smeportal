<div class="container">
    <h1 class="title-applicant"><strong><?php echo __('Applicants'); ?></strong> - <?php echo isset($project->title) ? $project->title : ''; ?></h1>
</div>
<div class="container-fluid" style="min-height: 500px" >
    <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="applicant-menu-tabs" role="tablist">
            <li role="presentation" class="active" id="applicant-tab"><a href="#applicant" aria-controls="applicant" role="tab" data-toggle="tab"><span>Đã tham gia</span></a></li>
            <li role="presentation"><a href="#myList" aria-controls="myList" role="tab" data-toggle="tab" onclick="get_application_tab_content('#myList',<?php echo $project->id; ?>,<?php echo $currentRole->id; ?>,'1',<?php echo $project_role_id; ?>);"><span><?php echo __('Danh sách lưu'); ?></span></a></li>
            <li role="presentation"><a href="#offers" aria-controls="offers" role="tab" data-toggle="tab" onclick="get_application_tab_content('#offers',<?php echo $project->id; ?>,<?php echo $currentRole->id; ?>,'2',<?php echo $project_role_id; ?>);"><span>Danh sách cấp</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="applicant">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-applicant-detail"><?php echo $currentRole->role; ?></h1>
                            <ul class="list-applicant-collaborators">
                                <?php foreach ($listUsers as $index => $row):
                                    if ($row->type == 1){
                                        $icon = '<img src="/img/icon_save.png" class="save-icon" alt="Save" title="Lưu" />';
                                    }
                                    else if($row->type == 2){
                                        $icon = '<img src="/img/icon_offer.png" class="offer-icon" alt="Offer" title="Chỉ định" />';
                                    }
                                    else{
                                        $icon = '';
                                    }

                                    ?>

                                <li <?php echo ($index % 2 == 1) ? 'class="odd"' : ''; ?> >
                                    <a href="/applicant-profile-<?php echo $row->user_id; ?>-<?php echo $row->project_id; ?>-<?php echo $row->role_id; ?>">
                                        <div class="row collaborator-item">
                                            <div class="col-md-10 collaborator-info">
                                                <div class="collaborator-avatar">
                                                    <img src="<?php echo (!empty($row->icon)) ? $row->icon : '/img/avatar_default.jpg'; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                                                </div>
                                                <strong class="collaborator-name"><?php echo $row->name; ?></strong>
                                            </div>
                                            <div class="col-md-2 collaborator-status"><?php echo $icon; ?></div>
                                        </div>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="myList">
                <div class="container">
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="offers">
                <div class="container">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var screen_width = $(window).width();
        var box_width = $('.title-applicant').width();
        var magin_value = (screen_width - box_width) / 2;
        $('#applicant-tab').css({'margin-left': magin_value + 'px'});
    });
</script>
