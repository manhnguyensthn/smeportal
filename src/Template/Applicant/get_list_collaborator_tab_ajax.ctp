<div class="row">
    <div class="col-md-12">
        <h1 class="title-applicant-detail"><?php echo $currentRole->role; ?></h1>
        <ul class="list-applicant-collaborators">
            <?php
            foreach ($listUsers as $index => $row):
                if ($type == 1) {
                    $icon = '<img src="/img/icon_save.png" class="save-icon" alt="Save" title="Save" />';
                } else{
                    $icon = '<img src="/img/icon_offer.png" class="offer-icon" alt="Offer" title="Offer" />';
                }
                ?>
                <li <?php echo ($index % 2 == 1) ? 'class="odd"' : ''; ?> >
                    <a href="/applicant-profile-<?php echo $row->user_id; ?>-<?php echo $row->project_id; ?>-<?php echo $row->role_id; ?>">
                        <div class="row collaborator-item">
                            <div class="col-md-10 collaborator-info">
                                <div class="collaborator-avatar">
                                    <img src="<?php echo (!empty($row->icon)) ? $row->icon : '/img/avatar_default.jpg'; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                                </div>
                                <strong class="collaborator-name"><?php echo $row->name; ?></strong>
                            </div>
                            <div class="col-md-2 collaborator-status"><?php echo $icon; ?></div>
                        </div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php if (count($listUsers) == 0): ?>
        <p><?php echo __('No data related.'); ?></p>
        <?php endif; ?>
    </div>
</div>