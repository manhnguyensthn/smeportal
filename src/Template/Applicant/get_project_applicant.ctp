<style type="text/css">
    .modal-open{ overflow: inherit; }
    body{ padding-right: 0 !important; }
</style>
<?php echo $this->element('Applicant/applicant_header'); ?>
<?php echo $this->element('Applicant/applicant_tab'); ?>
<div class="container">
    <div class="content_ajax">
        <?php echo $this->element('Applicant/content_project'); ?>
    </div>
</div>