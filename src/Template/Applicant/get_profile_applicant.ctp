<style type="text/css">
    .modal-open {
        overflow: inherit;
    }

    body {
        padding-right: 0 !important;
    }
</style>

<?php echo $this->element('Applicant/applicant_header'); ?>
<?php if (isset($message)) { ?>
<div class="row">
    <div class="col-md-3 col-xs-1"></div>
    <div class="col-md-8 col-xs-5">
        <div class="box_message">
            <textarea class="form-control message" name="message" id="message-confirm"
                      disabled><?php echo $message; ?></textarea>
            <a class="view_reply" href='<?php if (isset($reply)) echo $reply; ?>'>Trả lời</a>
        </div>
    </div>
    <div style="clear:both"></div>
    <?php } ?>
    <?php echo $this->element('Applicant/applicant_tab'); ?>
    <div class="container">
        <div class="content_ajax">
            <?php echo $this->element('Applicant/content_profile'); ?>
        </div>
    </div>
</div>

<style>
    .message {
        float: left;
        width: 450px;
        margin-left: 20px;
    }

    textarea.message {
        height: 70px;
    }

    .view_reply {
        font-family: "Avenir-Roman", sans-serif;
        padding: 20px 40px;
        color: #000;
        margin: 10px;
        float: left;
        font-size: 15px;
        font-weight: 500;
        background-color: transparent;
        background-repeat: repeat-x;
        background-size: 100% 100%;
        border: none;
        border-radius: 5px;
        background: #eb6500;
        margin-left: 20px;
        margin-top: 5px;
    }

    .view_reply:hover {
        background: #f0ad4e;
        text-decoration: none;
    }
</style>