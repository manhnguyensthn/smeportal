<div class="container">
    <h1 class="title-applicant"><strong><?php echo __('Find more'); ?></strong></h1>
</div>
<div class="container-fluid" style="min-height: 500px" >
    <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="applicant-menu-tabs" role="tablist">  
            <a href="#findMore" id = "findmore" aria-controls="findMore" role="tab" data-toggle="tab" onclick="get_list_findmore_tab_content('<?php echo $project->id; ?>');" ></a>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="applicant">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-role-applicant">
                                <?php foreach ($listApplicants as $index => $row): ?>
                                <li <?php echo ($index % 2 == 1) ? 'class="odd"' : ''; ?>>
                                    <a href="/applicant-detail-<?php echo $row->project_id; ?>-<?php echo $row->project_role_id; ?>">
                                        <div class="row role-item">
                                            <div class="col-md-9 role-name"><img class="role-icon" src="<?php echo $row->icon; ?>" alt="<?php echo $row->title; ?>" title="<?php echo $row->title; ?>" /><strong><?php echo $row->title; ?><?php echo !empty($row->charactor_name) ? ' ('.$row->charactor_name.')' : ''; ?></strong></div>
                                            <div class="col-md-1 role-joined-number">
                                                <strong class="role-number"><?php echo !empty($row->totalNumber) ? $row->totalNumber : ''; ?></strong><strong class="role-number-status"><?php echo (empty($row->checkNewsMember) && !empty($row->totalNumber)) ? '!' : ''; ?></strong>
                                            </div>
                                            <div class="col-md-2 list-role-actions">
                                                <span class="save-icon"><img src="/img/<?php echo !empty($row->numberSave) ? 'icon_save.png' : 'icon_star_inactive.png'; ?>" alt="Save" title="Save" /><?php echo !empty($row->numberSave) ? '<span class="save-status">'.$row->numberSave.'</span>' : ''; ?></span>
                                                <span class="offer-icon"><img src="/img/<?php echo !empty($row->numberOffer) ? 'icon_offer.png' : 'icon_offer_inactive.png'; ?>" alt="Offer" title="Chỉ Định" /><?php echo !empty($row->numberOffer) ? '<span class="offer-status">'.$row->numberOffer.'</span>' : ''; ?></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="findMore">
                <div class="container">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$( "#findmore" ).trigger( "click" );
    $(document).ready(function(){
        var screen_width = $(window).width();
        var box_width = $('.title-applicant').width();
        var magin_value = (screen_width - box_width)/2;
        $('#applicant-tab').css({'margin-left':magin_value+'px'});
    });
</script>