<?php if ($list_page == 'true'): ?>
   <?php foreach ($listUsers as $index => $row):
                    $class = ($index % 2 == 1) ? 'class="odd"' : '';
                    ?>
         <li <?php echo $class; ?> >
            <a href="/applicant-profile-<?php echo $row->id; ?>">
                <div class="row collaborator-item">
                    <div class="col-md-10 collaborator-info">
                        <div class="collaborator-avatar">
                            <img src="<?php echo!empty($row->avatar) ? $row->avatar : '/img/avatar_default.jpg'; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                        </div>
                        <strong class="collaborator-name"><?php echo $row->name; ?></strong>
                    </div>
                    <div class="col-md-2 collaborator-status"></div>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
<?php else: ?>
    <div class="row">
        <div class="col-md-12" style="padding: 20px">
            <div class="row">
                <div class="col-md-2"><a href="javascript:get_list_findmore_tab_content('<?php echo $project_id; ?>');" class="btn-back"><i class="fa fa-chevron-left"></i></a></div>
                <div class="col-md-8 box-search-applicant"><input name="keyword_search" type="text" class="form-control" value="" onkeyup="get_list_collaborattor_by_keyword();" /><input name="role_id" type="hidden" value="<?php echo $current_role_id; ?>" /><i class="fa fa-search"></i></div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <div class="col-md-12">
            <ul class="list-applicant-collaborators">
                <?php foreach ($listUsers as $index => $row):
                    $class = ($index % 2 == 1) ? 'class="odd"' : '';
                    ?>
                    <li <?php echo $class; ?> >
                        <a href="/findmore-profile-<?php echo $project_id; ?>-<?php echo $row->id; ?>">
                            <div class="row collaborator-item">
                                <div class="col-md-10 collaborator-info">
                                    <div class="collaborator-avatar">
                                        <img src="<?php echo!empty($row->avatar) ? $row->avatar : '/img/avatar_default.jpg'; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                                    </div>
                                    <strong class="collaborator-name"><?php echo $row->name; ?></strong>
                                </div>
                                <div class="col-md-2 collaborator-status"></div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-12 text-center applicant-loading" style="display: none"><img src="/img/loading.gif" alt="Loading" title="Loading" /><input name="current_page" type="hidden" value="1" /></div>
        <?php if (count($listUsers) == 10): ?>
            <div class="col-md-12 text-center"><button class="btn btn-launch see-more-button"  onclick="load_more_collaborator_by_keyword();" ><?php echo __('See more'); ?></button></div>
        <?php endif; ?>
		<input type = "hidden" value = "<?php echo $project_id; ?>" name = "project_id" id = "project_id">
    </div>
<?php endif; ?>