<?php

use App\Lib\ProjectsLib;

if (!empty($myLocation)) {
    $myLocationString = $myLocation['minLng'] . '|' . $myLocation['minLat'] . '|' . $myLocation['maxLng'] . '|' . $myLocation['maxLat'];
} else {
    $myLocationString = '';
}
if (!empty($dataLocation)) {
    $locationString = $dataLocation['minLng'] . '|' . $dataLocation['minLat'] . '|' . $dataLocation['maxLng'] . '|' . $dataLocation['maxLat'];
} else {
    $locationString = '';
}
$userInfo = $this->AuthUser->_getUser();
?>
<div class="container" >
    <div class="row discover">
        <div class="col-md-12">
            <div class="page-header" style="border-bottom: none;">
                <h1><?php echo __('Discover new projects'); ?></h1>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="sortArea col-md-12">
            <div class="row row-box-filter">
                <div class="col-md-4">
                    <select class="select" name="filterCategory">
                        <?php foreach ($listEverything as $item): ?>
                            <option <?php echo ($currentFilterCategory['object_id'] == $item->object_id && $currentFilterCategory['type'] == $item->type) ? 'selected' : ''; ?> value="<?php echo $item->object_id . '|' . $item->object_name . '|' . $item->group . '|' . $item->type; ?>"><?php echo$item->object_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-sm-3 control-label sortLabel text-right" for="formGroupInputLarge"><?php echo __('sort by'); ?>:</label>
                        <div class="col-sm-9">
                            <select class="select" name="sortBy">
                                <?php foreach ($listSortBy as $item): ?>
                                    <option <?php echo ($currentSortBy['object_id'] == $item->object_id && $currentSortBy['type'] == $item->type) ? 'selected' : ''; ?> value="<?php echo $item->object_id . '|' . $item->object_name . '|' . $item->group . '|' . $item->type; ?>"><?php echo $item->object_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-box-filter">
                <div class="col-md-5">
                    <select class="select" name="filterRole">
                        <option value="0|Role Available|3|4"><?php echo __('Role Available'); ?></option>
                        <?php foreach ($listRoles as $item): ?>
                            <option <?php echo ($currentFilterRole['object_id'] == $item->object_id && $currentFilterRole['type'] == $item->type) ? 'selected' : ''; ?> value="<?php echo $item->object_id . '|' . $item->object_name . '|' . $item->group . '|' . $item->type; ?>"><?php echo $item->object_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-4 searchedLocation">
                    <h2><?php echo __('Location'); ?></h2>
                    <div class="radio">
                        <label>
                            <input type="radio" name="filterLocation" id="optionsRadios1" value="1|Everywhere|4|5" <?php echo ($currentFilterLocation['object_id'] == 1) ? 'checked' : ''; ?> />
                            <span class="ip-label"><?php echo __('Everywhere'); ?></span>
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="filterLocation" id="optionsRadios2" value="2|Near me|4|5|<?php echo $myLocationString; ?>" <?php echo ($currentFilterLocation['object_id'] == 2) ? 'checked' : ''; ?> />
                            <span class="ip-label"><?php echo __('Near me'); ?></span>
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="filterLocation" id="optionsRadios3" value="3|Near me|4|5|<?php echo $locationString; ?>" <?php echo ($currentFilterLocation['object_id'] == 3) ? 'checked' : ''; ?> /><span class="ip-label"><?php echo __('Near'); ?></span> 
                            <input name="location_radius" id="location_radius" type="text" value="<?php echo $location_radius; ?>" />
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div>
                <div class="col-md-3 refine_search" style="text-align: right;padding-right: 30px">
                    <a href="javascript:search_project_by_options();"><?php echo __('refine search'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php if (count($listProjects) > 0): ?>
        <div class="row list-projects" id="list-projects" >
            <ul class="template-project-list wrapper-item slides clearfix">
                <?php
                foreach ($listProjects as $row):
                    $projectUri = '/projects/' . $this->Tool->slug($row->title) . '-' . $row->project_id;
                    $dayLeft = $this->AuthUser->_sub_datetime(date('Y-m-d', strtotime($row->end_date)), date('Y-m-d'));
                    $dayLeft = ($dayLeft > 0) ? $dayLeft : 0;
                    if ($row->total_role > 0) {
                        $progessBar = ($row->total_role_joined / $row->total_role) * 100;
                        $progessBar = ($progessBar <= 100) ? $progessBar : 100;
                    } else {
                        $progessBar = 0;
                    }
                    ?>
                    <li class="item slide">
                        <div class="container-item">
                            <div class="img">
                                <a href="<?php echo $projectUri; ?>" title="4 Dance in the star LP" class="image">
                                    <?php echo ProjectsLib::getImage($row->image_url, $row->title, '', true, false); ?>
                                </a>
                                <?php if (!empty($userInfo)): ?>
                                    <?php if ($this->AuthUser->_check_user_action_by_project_and_user($listUserActions, $row->project_id, $userInfo['id'], 1)): ?>
                                        <a href="javascript:void(0);" onclick="javascript:add_action_to_system('1', '<?php echo $row->project_id; ?>');" title="Liked it!" class="love_project_<?php echo $row->project_id; ?> template-love "></a>
                                    <?php else: ?>
                                        <a href="javascript:void(0);" onclick="javascript:add_action_to_system('1', '<?php echo $row->project_id; ?>');" title="Liked it!" class="love_project_<?php echo $row->project_id; ?> template-love  dislike "></a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a href="javascript:void(0);" title="Liked it!" class="love_project_<?php echo $row->project_id; ?> template-love  dislike "></a>
                                <?php endif; ?>
                            </div>
                            <div class="text-content">
                                <h2 class="title"><a href="<?php echo $projectUri; ?>"><?php echo $row->title; ?></a></h2>
                                <h3 class="name-user"><?php echo $row->owner; ?></h3>
                                <ul class="clearfix template-category-location font-md">
                                    <li><a href="/discover/index?filterCategory=<?php echo $row->category_id; ?>|<?php echo $row->category; ?>|1|2" title="<?php echo $row->category; ?>"><?php echo $row->category; ?></a></li>
                                    <?php if (!empty($row->address)): ?>
                                        <li><span class="icon map-marker"></span> <?php echo $this->AuthUser->_character_limiter($row->address, 15); ?></li>
                                    <?php endif; ?>
                                </ul>
                                <div class="text"><?php echo $this->AuthUser->_character_limiter($row->description, 400); ?></div>
                                
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="row text-center" id="box-loading" style="display: none;">
            <img src="/img/loading.gif" alt="Loading" title="Loading" />
        </div>
        <input name="current_page" id="current_page" value="1" type="hidden" />
        <?php if (count($listProjects) == 6): ?>
            <div class="row text-center" style="margin-top: 20px;">
                <a href="javascript:load_discover_see_more();" class="btn btn-warning btn-launch see-more-button" style="float: inherit; height: 60px; line-height: 60px"><?php echo __('See more'); ?></a>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12"><p><?php echo __('No data related. Please refine.'); ?></p></div>
        </div>
    <?php endif; ?>
</div>


<!-- Modal -->
<div class="modal fade" id="box-loading1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background: none;box-shadow: none;border: none;">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <img src="/img/ring.gif" />
                        <p style="color: white;"><?php echo __('System is loading. Please wait ...'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
