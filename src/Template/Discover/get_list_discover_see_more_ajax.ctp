<?php

use App\Lib\ProjectsLib;

$userInfo = $this->AuthUser->_getUser();
?>
<ul class="template-project-list wrapper-item slides clearfix">
    <?php
    foreach ($listProjects as $row):
        $projectUri = '/projects/' . $this->Tool->slug($row->title) . '-' . $row->project_id;
        $dayLeft = $this->AuthUser->_sub_datetime(date('Y-m-d', strtotime($row->end_date)), date('Y-m-d'));
        $dayLeft = ($dayLeft > 0) ? $dayLeft : 0;
        if ($row->total_role > 0) {
            $progessBar = ($row->total_role_joined / $row->total_role) * 100;
            $progessBar = ($progessBar <= 100) ? $progessBar : 100;
        } else {
            $progessBar = 0;
        }
        ?>
        <li class="item slide">
            <div class="container-item">
                <div class="img">
                    <a href="<?php echo $projectUri; ?>" title="4 Dance in the star LP" class="image">
                        <?php echo ProjectsLib::getImage($row->image_url, $row->title, '', true, false); ?>
                    </a>
                    <?php if (!empty($userInfo)): ?>
                        <?php if ($this->AuthUser->_check_user_action_by_project_and_user($listUserActions, $row->project_id, $userInfo['id'], 1)): ?>
                            <a href="javascript:void(0);" onclick="javascript:add_action_to_system('1', '<?php echo $row->project_id; ?>');" title="Liked it!" class="love_project_<?php echo $row->project_id; ?> template-love "></a>
                        <?php else: ?>
                            <a href="javascript:void(0);" onclick="javascript:add_action_to_system('1', '<?php echo $row->project_id; ?>');" title="Liked it!" class="love_project_<?php echo $row->project_id; ?> template-love  dislike "></a>
                        <?php endif; ?>
                    <?php else: ?>
                        <a href="javascript:void(0);" title="Liked it!" class="love_project_<?php echo $row->project_id; ?> template-love  dislike "></a>
                    <?php endif; ?>
                </div>
                <div class="text-content">
                    <h2 class="title"><a href="<?php echo $projectUri; ?>"><?php echo $row->title; ?></a></h2>
                    <h3 class="name-user"><?php echo $row->owner; ?></h3>
                    <ul class="clearfix template-category-location font-md">
                        <li><a href="/discover/index?filterCategory=<?php echo $row->category_id; ?>|<?php echo $row->category; ?>|1|2" title="<?php echo $row->category; ?>"><?php echo $row->category; ?></a></li>
                        <?php if (!empty($row->address)): ?>
                            <li><span class="icon map-marker"></span> <?php echo $this->AuthUser->_character_limiter($row->address, 15); ?></li>
                        <?php endif; ?>
                    </ul>
                    <div class="text"><?php echo $this->AuthUser->_character_limiter($row->description, 100); ?></div>
                    
                </div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>