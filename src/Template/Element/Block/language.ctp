<?php
	use App\Lib\CoreLib;
	$Languages = CoreLib::getLanguage();
	$LanguageCurrent = CoreLib::getLanguage(false);
?>
<div role="presentation" class="dropup"> 
	<a href="javascript:void(0)" class="dropdown-toggle" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		<?php echo (isset($LanguageCurrent['name']) ? $LanguageCurrent['name'] : __('Select')); ?>
		<i class="fa fa-chevron-down pull-right" aria-hidden="true"></i>
	</a> 
	<ul class="dropdown-menu" id="menu1" aria-labelledby="drop4"> 
		<?php foreach ($Languages as $key => $Language): ?>
			<li class="item">
				<a href="javascript:void(0)" onclick="setLanguageAjax(this); return false;" data-language="<?php echo $key; ?>">
					<?php echo $Language['name']; ?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul> 
</div>