<?php 
	$oTranslations = array(
		'are_you_sure_delete_this_update'              => __('Are you sure delete this update?'),
		'are_you_sure_you_want_to_delete_this_role'    => __('Are you sure you want to delete this role?'),
		'are_you_sure_you_want_to_delete_this_comment' => __('Are you sure you want to delete this comment?'),
		'are_you_sure_you_want_to_delete_this_work'    => __('Are you sure you want to delete this work?'),
		'are_you_sure_you_want_to_accept_this_request' => __('Are you sure you want to accept this request?'),
                'are_you_sure_delete_this_group'              => __('Are you sure delete this group?'),

		'title_is_required_max_length_of_chars_html_or_script_tags_is_not_allowed'     => sprintf(__('Title is required. Max length of %s chars.HTML or script tags is not allowed.'), 60),
		'content_is_required_max_length_of_chars_html_or_script_tags_is_not_allowed'   => sprintf(__('Content is required. Max length of %s chars.HTML or script tags is not allowed.'), 120),
		'file_upload_should_be_in_text_image_media_video_in_range_of_size_of_kb_to_mb' => sprintf(__('File upload should be in (in text, image, media, video), in range of size of %sKB to %sMB.'), 1, 100),
                
		'please_create_at_least_one_role' => __('Please create at least one role.'),
		'invalid_website_format_must_be'  => __('Định dạng website sai. Ví dụ: http://www.sme.com, https://www.amazon.co.jp'),
		'following'                       => __('Following'),
		'follow'                          => __('Theo dõi +'),
                'please_waitting' => __('Please waitting'),
                'please_select_one_member' => __('Please select one member'),

		'no'  => __('No'),
		'yes' => __('Yes'),
		'ok'  => __('OK'),
	);
?>
<script type="text/javascript">var oTranslations = <?php echo json_encode($oTranslations); ?></script>