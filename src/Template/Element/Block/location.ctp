<?php
	$aRequestSession = $this->request->session();
	$aDataLocation   = $aRequestSession->read('Config.location');
?>
<script type="text/javascript">
   $(window).load(function () {
   		<?php // GET LOCATION ?>
   		<?php if(!isset($aDataLocation['latitude']) || empty($aDataLocation['latitude']) || !isset($aDataLocation['longitude']) || empty($aDataLocation['longitude'])): ?>
	        if(navigator.geolocation){
		        navigator.geolocation.getCurrentPosition(updateLocation);
		    }else{
		        //console.log('Geolocation is not supported by this browser.');
		    }
	    <?php endif; ?>
	    <?php // END: GET LOCATION ?>
   });
</script>