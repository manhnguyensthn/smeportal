<?php
    $oController = new \App\Controller\HomeController();
    list($aJustForYous, $aTopPicks, $aNearYous) = $oController->blockFilterProject();
?>
<div class="home-tab-project">
    <div class="container">
        <div class="title-contents-list">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#just-for-you" aria-controls="just-for-you" role="tab" data-toggle="tab"><?php echo __('Just for you'); ?></a></li>
                <li role="presentation"><a href="#top-picks" aria-controls="top-picks" role="tab" data-toggle="tab"><?php echo __('Top picks'); ?></a></li>
                <li role="presentation"><a href="#near-you" aria-controls="near-you" role="tab" data-toggle="tab"><?php echo __('Near you'); ?></a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="just-for-you">
                    <?php if(isset($aJustForYous['projects']) && !empty($aJustForYous['projects'])): ?>
                        <div class="slider-tab-project">
                            <ul class="template-project-list wrapper-item clearfix">
                                <?php
                                    echo $this->element('Home/item-filter-project', array(
                                        'projects' => isset($aJustForYous['projects']) ? $aJustForYous['projects'] : array(),
                                        'ListUsersActions' => isset($aJustForYous['ListUsersActions']) ? $aJustForYous['ListUsersActions'] : array(),
                                        'ListUserProjects' => isset($aJustForYous['ListUserProjects']) ? $aJustForYous['ListUserProjects'] : array(),
                                    ));
                                ?>
                            </ul>
                        </div>

                        <?php if(isset($aJustForYous['paging']['pageCount']) && $aJustForYous['paging']['pageCount'] > 1): ?>
                            <div class="text-center button-see-more">
                                <center>
                                    <a href="javascript:void(0);" onclick="getHomeFilterProject(this, 'just-for-you', 2);" class="btn btn-warning btn-see-more" style="background-color:#009df7">
                                        <span><?php echo __('See more'); ?></span>
                                        <span class="loading"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> <?php echo __('Loading...'); ?></span>
                                    </a>
                                </center>
                            </div>
                        <?php endif; ?>

                    <?php else: ?>

                        <?php $this->General->getNoItemMessage(__('Join us and explore more projects.')); ?>

                    <?php endif; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="top-picks">
                    <?php if(isset($aTopPicks['projects']) && !empty($aTopPicks['projects'])): ?>

                        <div class="slider-tab-project">
                            <ul class="template-project-list wrapper-item clearfix">
                                <?php
                                    echo $this->element('Home/item-filter-project', array(
                                        'projects' => isset($aTopPicks['projects']) ? $aTopPicks['projects'] : array(),
                                        'ListUsersActions' => isset($aTopPicks['ListUsersActions']) ? $aTopPicks['ListUsersActions'] : array(),
                                        'ListUserProjects' => isset($aTopPicks['ListUserProjects']) ? $aTopPicks['ListUserProjects'] : array(),
                                    ));
                                ?>
                            </ul>
                        </div>

                        <?php if(isset($aTopPicks['paging']['pageCount']) && $aTopPicks['paging']['pageCount'] > 1): ?>
                            <div class="text-center button-see-more">
                                <center>
                                    <a href="javascript:void(0);" onclick="getHomeFilterProject(this, 'top-picks', 2);" class="btn btn-warning btn-see-more" >
                                        <span><?php echo __('See more'); ?></span>
                                        <span class="loading"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> <?php echo __('Loading...'); ?></span>
                                    </a>
                                </center>
                            </div>
                        <?php endif; ?>

                    <?php else: ?>

                        <?php $this->General->getNoItemMessage(__('Join us and explore more projects.')); ?>

                    <?php endif; ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="near-you">
                    <?php if(isset($aNearYous['projects']) && !empty($aNearYous['projects'])): ?>
                        <div class="slider-tab-project">
                            <input type="hidden" value="<?php echo (isset($aNearYous['DataId']) ? $aNearYous['DataId'] : ''); ?>" class="DataId">
                            <ul class="template-project-list wrapper-item clearfix">
                                <?php
                                    echo $this->element('Home/item-filter-project', array(
                                        'projects' => isset($aNearYous['projects']) ? $aNearYous['projects'] : array(),
                                        'ListUsersActions' => isset($aNearYous['ListUsersActions']) ? $aNearYous['ListUsersActions'] : array(),
                                        'ListUserProjects' => isset($aNearYous['ListUserProjects']) ? $aNearYous['ListUserProjects'] : array(),
                                    ));
                                ?>
                            </ul>
                        </div>

                        <?php if(isset($aNearYous['paging']['pageCount']) && $aNearYous['paging']['pageCount'] > 1): ?>
                            <div class="text-center button-see-more">
                                <center>
                                    <a href="javascript:void(0);" onclick="getHomeFilterProject(this, 'near-you', 2);" class="btn btn-warning btn-see-more" >
                                        <span><?php echo __('See more'); ?></span>
                                        <span class="loading"><i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> <?php echo __('Loading...'); ?></span>
                                    </a>
                                </center>
                            </div>
                        <?php endif; ?>

                    <?php else: ?>

                        <?php $this->General->getNoItemMessage(__('Join us and explore more projects.')); ?>
                        
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>