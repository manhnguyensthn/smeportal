<?php $oUsersProjectActions = new \App\Controller\UsersProjectActionsController(); ?>
<?php $HomeController = new \App\Controller\HomeController(); ?>

<?php
    if(isset($projects) && !empty($projects)){
        foreach ($projects as $key => $aData) {
            if(isset($aData->countries_left) && !empty($aData->countries_left)){
                $aData['country'] = $aData->countries_left;
            }
            if(isset($aData->states_left) && !empty($aData->states_left)){
                $aData['state'] = $aData->countries_left;
            }
            if(isset($aData->categories_left) && !empty($aData->categories_left)){
                $aData['category'] = $aData->categories_left;
            }
?>
            <li class="item slide">
                <div class="container-item">
                    <div class="img">
                        <a href="<?php $this->General->getLinkProject($aData); ?>" title="<?php echo $aData->title; ?>" class="image">
                            <?php echo $this->General->getImage($aData->image_url, $aData->title, '', false, true); ?>    
                        </a>

                        <?php  echo $this->element('Block/love', [
                            'item_id' => $aData->id,
                            'ListUsersActions' => (isset($ListUsersActions) ? $ListUsersActions : array()),
                        ]); // Love Project ?>
                    </div>
                    <div class="text-content">
                        <h2 class="title"><a href="<?php $this->General->getLinkProject($aData); ?>"><?php echo $aData->title; ?></a></h2>
                        <h3 class="name-user"><?php $this->General->getFullName($aData->user); ?></h3>
                        <ul class="clearfix template-category-location font-md">

                            <?php if(isset($aData->category) && !empty($aData->category)): ?>
                                <li><a href="<?php echo $this->General->getLinkCategory($aData->category); ?>" title="<?php echo (isset($aData->category->name) ? $aData->category->name : ''); ?>">
                                    <?php echo (isset($aData->category->name) ? $aData->category->name : ''); ?>
                                </a></li>
                            <?php endif; ?>

                            <?php if(isset($aData->country->country_name)): ?>
                                <li><span class="icon map-marker"></span> <?php echo $aData->country->country_name; ?></li>
                            <?php endif; ?>

                        </ul>
                        <div class="text"><?php echo $this->AuthUser->_character_limiter($aData->description, 390); ?></div>
                        <div class="wrapper-content-bootom">
                            <?php if(isset($aData['aRoles']) && !empty($aData['aRoles'])): ?>
                                <div class="row template-open-role">
                                    <!--<div class="item-role col-sm-4"><span><?php /*echo __('Open Roles'); */?></span></div>-->
                                    <div class="item-role col-sm-8">
<!--                                        <ul class="clearfix">-->
<!--                                            --><?php //$i = 0; foreach ($aData['aRoles'] as $keyRoles => $aRole): ?>
<!--                                                --><?php
//                                                    $joined    = $this->AuthUser->_get_role_open_in_list((isset($ListUserProjects) ? $ListUserProjects : array()), $aData->id, $aRole->id);
//                                                    $open_role = $aRole->quantity - $joined;
//                                                ?>
<!--                                                --><?php //if($open_role > 0): ?>
<!--                                                    --><?php //$i++; if($i > 3) break; ?>
<!--                                                    <li>-->
<!--                                                        <span class="role_text"><a href="--><?php //$this->General->getLinkProject($aData); ?><!--" title="--><?php //echo $aRole->role->role; ?><!--">--><?php //echo ucwords(strtolower($aRole->role->role)); ?><!--</a></span>-->
<!--                                                        <span class="open_role">(--><?php //echo $open_role; ?><!--)</span>-->
<!--                                                    </li>-->
<!--                                                --><?php //endif; ?>
<!--                                            --><?php //endforeach; ?>
<!--                                            --><?php //if(count($aData['aRoles']) > 3) : ?>
<!--                                                <li class="full-list"><a href="--><?php //$this->General->getLinkProject($aData); ?><!--" style="text-decoration: underline; ">--><?php //echo __('See full list+'); ?><!--</a></li>-->
<!--                                            --><?php //endif; ?>
<!--                                        </ul>-->
                                    </div>
                                </div>
                            <?php endif; ?>

                            <!-- <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: <?php echo (int)$aData->iPercentRoleJoined; ?>%">
                                    <span class="sr-only"><?php echo $aData->iPercentRoleJoined; ?><?php echo __('% Hoàn thành (warning)'); ?></span>
                                </div>
                            </div>

                            <div class="joined clearfix">
                                <span class="pull-left"><?php $this->General->getRoleJoin($aData); ?></span>
                                <span class="days pull-right"><?php $this->General->getDayLeft($aData); ?></span>
                            </div> -->

                        </div>
                    </div>
                </div>
            </li>
<?php
        }
    }
?>