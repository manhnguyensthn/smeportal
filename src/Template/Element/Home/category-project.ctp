<?php $controller = new \App\Controller\CategoriesController(); ?>
<?php $categories = $controller->getBlockCategoryHome(); ?>
<?php if(isset($categories) && !empty($categories)):?>
    <div class="container">
        <div class="template_home_category clearfix">
        <?php foreach ($categories as $key => $row): ?>
            <div class="item">
                <a href="<?php echo $this->General->getLinkCategory($row); ?>"><?php echo $row->name; ?></a>
            </div>
        <?php endforeach; ?> 
        </div>
    </div>
<?php else: ?>
    <?php $this->General->getNoItemMessage(); ?>
<?php endif; ?>