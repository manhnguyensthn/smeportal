<?php $oController = new \App\Controller\HomeController(); ?>
<?php $aDatas = $oController->blockFeaturedProject(); ?>
<div class="featured-project">
    <div class="container">
        <?php if(isset($aDatas->id) && $aDatas->id): ?>
        	<h2 class="page-title">
        		<a href="<?php $this->General->getLinkProject($aDatas); ?>" title="<?php echo $aDatas->title; ?>"><?php echo __('Featured Project'); ?></a>
        	</h2>
        	<div class="wrapper-item">
	            <div class="item clearfix">
	                <div class="img">
	                    <a href="<?php $this->General->getLinkProject($aDatas); ?>" title="<?php echo $aDatas->title; ?>" class="image">
		                    <?php echo $this->General->getImage($aDatas->image_url, $aDatas->title, '', true, false); ?>
	                    </a>
	                    <?php  echo $this->element('Block/love', ['item_id' => $aDatas->id, 'template' => 'lg']); // Love Project ?>
	                </div>
	                <div class="text-content">
	                    <h2 class="title"><a href="<?php $this->General->getLinkProject($aDatas); ?>" title="<?php echo $aDatas->title; ?>"><?php echo $aDatas->title; ?></a></h2>
	                    <h3 class="name-user"><?php $this->General->getFullName($aDatas->user); ?></h3>
	                    <ul class="clearfix template-category-location font-lg">
	                        <li><a href="<?php echo $this->General->getLinkCategory($aDatas->category); ?>" title="<?php echo $aDatas->category->name; ?>"><?php echo $aDatas->category->name; ?></a></li>
	                        <?php if(isset($aDatas->state->name) || isset($aDatas->country->country_name)): ?>
	                        <li>   
	                        	<span class="icon map-marker"></span>
	                        	<?php echo (isset($aDatas->state->name) ? $aDatas->state->name : ''); ?>
	                        	<?php echo (isset($aDatas->state->name) && isset($aDatas->country->country_name)) ? ', ' : ''; ?>
	                        	<?php echo (isset($aDatas->country->country_name) ? $aDatas->country->country_name : ''); ?></li>
		                    <?php endif; ?>
	                    </ul>
	                    <div class="text"><?php echo $aDatas->description; ?></div>
	                    <!-- <div class="btn-green-production"><a href="#" title="">Greenlit - In Production!</a></div> -->
	                </div>
	            </div>
	        </div>
        <?php else: ?>
        	<h2 class="page-title"><a><?php echo __('Featured Project'); ?></a></h2>
        	<?php $this->General->getNoItemMessage(); ?>
        <?php endif; ?>
    </div>
</div>