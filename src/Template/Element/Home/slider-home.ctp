<?php
$HomeController = new \App\Controller\HomeController();
$aSliders = $HomeController->getSliders();
?>
<div class="img-slide" style="width: 100% ;margin:auto; ">
    <div id="slideHome" class="flexslider">
        <?php if ($aSliders): ?>
            <ul class="slides">
                <?php foreach ($aSliders as $key => $aSlider): ?>
                    <li>
                        <div class="content-img">
                            <a href="<?php echo $aSlider->url; ?>">
                                <img src="/<?php echo $aSlider->image_path; ?>" style="zoom: 100%;">
                            </a>
                        </div>
                         <div class="slideInfo">
                            <h2><?php echo $aSlider->title; ?></h2>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <?php $this->General->getNoItemMessage(); ?>
        <?php endif; ?>
    </div>
    <div class="slideInfo create-button">
                     
        <a href="/projects/create" class="btn btn-warning btn-learn-more"><?php echo __('Đăng ký Doanh Nghiệp >>>'); ?></a>
    </div>
</div>