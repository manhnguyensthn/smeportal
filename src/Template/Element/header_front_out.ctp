<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
    $aRequestQuery  = $this->request->query;
    $keyword        = (isset($aRequestQuery['keyword']) ? $aRequestQuery['keyword'] : '');
    $filterCategory = (isset($aRequestQuery['filterCategory']) ? $aRequestQuery['filterCategory'] : '');
    $sortBy         = (isset($aRequestQuery['sortBy']) ? $aRequestQuery['sortBy'] : '');
    $filterRole     = (isset($aRequestQuery['filterRole']) ? $aRequestQuery['filterRole'] : '');
    $filterLocation = (isset($aRequestQuery['filterLocation']) ? $aRequestQuery['filterLocation'] : '');
?>

<div class="clearfix"></div>
<nav id="navBarWTP" class="navbar navbar-default navbar-custom">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-4 padd-all">
            <div class="row" style="margin: 10px; margin-bottom:-10px;">
               <!-- <div class="col-md-1" style="margin-right: -20px;">

                    <a href="javascript:void(0)" class="showSearch"><i class="fa fa-search" aria-hidden="true"></i></a>
                </div>-->
                <div class="col-md-11 purple search_box header_search">
                <form action="/discover/search" method="GET">
                    <div class="form-group">
                        <input type="hidden" name="filterCategory" value="<?php echo $filterCategory; ?>">
                        <input type="hidden" name="sortBy" value="<?php echo $sortBy; ?>">
                        <input type="hidden" name="filterRole" value="<?php echo $filterRole; ?>">
                        <input type="hidden" name="filterLocation" value="<?php echo $filterLocation; ?>">
                        <input type="text" class="form-control input-medium" placeholder="<?php echo __('Search'); ?>" name="keyword" value="<?php echo $keyword; ?>">
                    </div>
                </form>
                </div>
            </div>
        </div>
        <div class="navbar-header col-md-4 padd-all">
            <center>
                <a href="/"><img src="/img/logo.png" class="img-responsive logo header_logo" /></a>
            </center>
        </div>
        <div class="navbar-header col-md-4 padd-all">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse padd-all" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right ">
                    <li>
                        <a href="<?php echo $this->Curl->build(['controller' => 'users','action' => 'login']); ?>"  class="signup"><?= __('Login') ?></a>
                    </li>
                    <li><a href="<?php echo $this->Curl->build(['controller' => 'users','action' => 'register']); ?>"><?= __('Sign Up') ?></a></li>
                </ul>
                
            </div><!-- /.navbar-collapse -->
        </div>
    </div><!-- /.container-fluid -->
</nav>
<div class="clearfix"></div>
