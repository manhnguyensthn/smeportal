<?php if(isset($Follows) && !empty($Follows)): ?>
	<?php foreach ($Follows as $key => $Follow) : ?>
		<li class="item clearfix user_following_<?php echo $Follow->userid; ?>">
			<?php echo $this->General->getAvata($Follow, '/applicant-profile-' . $Follow->userid); ?>
			<div class="content_item">
				<div class="title"><a href="/applicant-profile-<?php echo $Follow->userid; ?>"><?php echo $Follow->name; ?></a></div>
				<div class="coutries"><?php echo $Follow->address; ?></div>
				<div class="roles"><?php echo $Follow->roles; ?></div>
				
				<?php if($user->id == $authUser['id']): ?>
					<?php echo $this->General->templateBlockUser($Follow->userid); ?>
					<?php if(isset($type) && $type == 'follower'): ?>
						<?php echo $this->General->templateFollowingCollaborator($Follow->userid, '', 'follower'); ?>
					<?php else: ?>
						<?php echo $this->General->templateFollowingCollaborator($Follow->userid); ?>
					<?php endif; ?>
				<?php else: ?>
					<?php echo $this->General->templateFollowingCollaborator(0, 'no_action'); ?>
				<?php endif; ?>
			</div>
		</li>
	<?php endforeach; ?>
<?php endif; ?>