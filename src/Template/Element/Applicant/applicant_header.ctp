<?php if (isset($user) && !empty($user)): ?>
    <div class="container">
        <div class="applicant_header">
            <?php echo $this->General->getAvata($user); ?>
            <div class="content_header">
                <div class="title">
                    <h1 class="profile-name" style="<?php if(isset($authUser['id'])) echo ($user['id'] == $authUser['id']) ? 'padding-right: 0px;' : ''; ?>">
                        <?php $this->General->getFullName($user); ?>
                    </h1>
                    <?php echo $this->General->templateFollowingCollaborator($user['id']); ?>
                </div>

                <div class="button_action">
                    <?php if (isset($user['roles']) && !empty($user['roles'])): ?>
                        <?php foreach ($user['roles'] as $key => $role): ?>
                            <a href="javascript:void(0);" class="action-item"><?php echo ucwords(strtolower($role->role)); ?></a>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <?php if (isset($authUser['id']) && $authUser['id'] != $user['id'] && !isset($is_userProfile)): ?>
                        <?php if ($bSave): ?>
                            <?php echo $this->General->templateSaveCollaborator($user['id'], $project_id, $role_id); ?>
                        <?php endif; ?>

                        <?php if ($bOffer): ?>
                            <?php echo $this->General->templateOfferCollaborator($user['id'], $project_id, $role_id); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if (isset($is_userProfile)): ?>
                            <a href="javascript:send_request_invite('<?php echo $user['id']; ?>','<?php echo $project_id; ?>');" style="background: #eb6500;"><?php echo __('Invite'); ?></a>
                    <?php endif; ?>
                </div>

                <p class="profile-info">
                    <?php if (isset($TotalProject) && !empty($TotalProject)): ?>
                        <!-- <span><?php echo sprintf(__('Completed %s projects'), $TotalProject['TotalCompletedProjects']); ?></span> -->  
                        <span><?php echo sprintf(__('Created %s projects'), $TotalProject['TotalCreatedProjects']); ?></span> - 
                    <?php endif; ?>

                    <?php if ((isset($user['States']) && !empty($user['States'])) || (isset($user['Countries']) && !empty($user['Countries']))): ?>
                        <span> 
                            <?php echo (isset($user['States']) && !empty($user['States'])) ? $user['States']['name'] : ''; ?>
                            <?php echo (isset($user['Countries']) && !empty($user['Countries'])) ? $user['Countries']['country_name'] : ''; ?>
                        </span> - 
                    <?php endif; ?>
                    <span> <?php echo __('Joined ') . date("F Y", strtotime($user['created'])); ?></span>
                </p>

                <div class="profile-description">
                    <div class="biography"><?php echo $user['biography']; ?></div>
                    <?php if (isset($user['biography']) && !empty($user['biography'])): ?>
                        <div class="pull-right"><a href="javascript:void(0)" class="view-bio" data-toggle="modal" data-target="#biography-extent" ><?php echo __('Xem thông tin chi tiết'); ?></a></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- Modal -->
    <style>
        #biography-extent{
            background: rgba(255,255,255,.5); font-family: "Comfortaa-Regular", sans-serif;
        }
        #biography-extent .modal-content{
            border: 1px solid #ffe3a5; -moz-box-shadow: none; -webkit-box-shadow: none; box-shadow: none;
        }
        #biography-extent h3{
            font-size: 50px; color: #4866b0; margin: 0;
        }
        #biography-extent .text{
            overflow-wrap: break-word; margin: 50px 0 30px; color: #000;
        }
        #biography-extent h5{
            margin-bottom: 30px;
        }

        #biography-extent ul{
            list-style: none; padding: 0;
        }
        #biography-extent ul a{
            color: #20419a;
        }

        #biography-extent .text,
        #biography-extent ul,
        #biography-extent h5{
            font-size: 20px;
        }
    </style>
    <div class="modal fade" id="biography-extent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 90%; max-width: 830px;">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 full-biography">
                            <h3 class="title"><?php echo __('Biography'); ?></h3>
                            <div class="text"><?php echo $user['biography']; ?></div>
                            <?php if (isset($user['UsersWebsites']) && !empty($user['UsersWebsites'])): ?>
                                <h5 style=""><?php echo __('Websites'); ?></h5>
                                <ul class="list-websites clearfix">
                                    <?php foreach ($user['UsersWebsites'] as $key => $UsersWebsite): ?>
                                        <li><a href="<?php echo $UsersWebsite->website->name; ?>" target="_blank"><?php echo $UsersWebsite->website->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>