<?php if(isset($projects) && !empty($projects)): ?>
	<ul class="template-project-list wrapper-item clearfix applicant_max_width">
		<?php
	        echo $this->element('Home/item-filter-project', array(
	            'projects' => isset($projects) ? $projects : array(),
	            'ListUsersActions' => isset($ListUsersActions) ? $ListUsersActions : array(),
	            'ListUserProjects' => isset($ListUserProjects) ? $ListUserProjects : array(),
	        ));
	    ?>
	</ul>
	
	<?php if($TotalProject['TotalProject'] > $PageSize): ?>
		<div class="text-center button-see-more">
	        <a href="javascript:void(0);" onclick="getApplicantProject(this, '2', '<?php echo $user->id; ?>');" class="template-see-more-v2" >
	            <span><?php echo __('See more'); ?></span>
	            <span class="loading">
	            	<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
	            	<?php echo __('Loading...'); ?>
	            </span>
	        </a>
	    </div>
	<?php endif; ?>    
    
<?php else: ?>
	<?php $this->General->getNoItemMessage(); ?>
<?php endif; ?>