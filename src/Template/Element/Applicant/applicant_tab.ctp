<?php if(isset($user) && !empty($user)): ?>
<?php
    $LinkExt = '';
    if(isset($project_id) && $project_id){
        $LinkExt .= '-' . (int)$project_id;
        $LinkExt .= '-' . (isset($role_id) ? (int)$role_id : 0);
    }
?>
<div class="template_tab_applicant_profile">
    <div class="container">
        <ul class="clearfix tab_left">
            <li class="item <?php echo ((isset($tab_active) && $tab_active == 'profile')) ? 'active' : ''; ?>">
                <a href="/applicant-profile-<?php echo $user->id . $LinkExt; ?>" data-action="getProfileApplicant"><?php echo __('Thành tích'); ?></a>
            </li>
            <li class="item <?php echo ((isset($tab_active) && $tab_active == 'project')) ? 'active' : ''; ?>">
                <a href="/applicant-project-<?php echo $user->id . $LinkExt; ?>" data-action="project"><?php echo __('Projects'); ?></a>
            </li>
        </ul>
        <ul class="clearfix tab_right">
            <li class="item <?php echo ((isset($tab_active) && $tab_active == 'following')) ? 'active' : ''; ?> total_following_<?php echo $user->id; ?>">
                <a href="/applicant-following-<?php echo $user->id . $LinkExt; ?>"><?php echo __('Following'); ?> <span class="number"><?php echo $user->TotalFollowing; ?></span></a>
            </li>
            <li class="item <?php echo ((isset($tab_active) && $tab_active == 'follower')) ? 'active' : ''; ?> total_follower_<?php echo $user->id; ?>">
                <a href="/applicant-follower-<?php echo $user->id . $LinkExt; ?>"><?php echo __('Follower'); ?> <span class="number"><?php echo $user->TotalFollower; ?></span></a>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
</div>

<script type="text/javascript">
    $('.template_tab_applicant_profile ul li a').click(function(event) {
        if(!$(this).closest('li').hasClass('active')){
            var sLink = $(this).attr('href');
            $.ajax({
                type: 'POST',
                url: sLink,
                data: {},
                beforeSend: function(){
                    $('.content_ajax').html('<div class="row text-center"><img src="/img/loading.gif" /></div>');
                },
                success: function (respone) {
                    $('.content_ajax').html(respone);
                }
            });
            window.history.replaceState({}, '', sLink);
            $('.template_tab_applicant_profile ul li').removeClass('active');
            $(this).closest('li').addClass('active');
            var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
            var is_safari = navigator.userAgent.indexOf("Safari") > -1;
            if(is_chrome && is_safari){
                is_safari = false;
            }
            if(is_safari){
                location.reload();
            }
        }
        return false;
    });
</script>

<?php endif; ?>
