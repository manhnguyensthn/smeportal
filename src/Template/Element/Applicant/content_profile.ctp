<?php if(isset($user->userWorks) && !empty($user->userWorks)): ?>
<div class="template-list-portfolios applicant_max_width">
    <ul class="clearfix">
    	<?php foreach ($user->userWorks as $key => $userWork): ?>
    		<?php if(isset($userWork->work_video_id) && !empty($userWork->work_video_id)): ?>
				<li class="item"><div class="iframe"><iframe src="https://www.youtube.com/embed/<?php echo $userWork->work_video_id; ?>" frameborder="0" allowfullscreen></iframe></div></li>
    		<?php endif; ?>
    	<?php endforeach; ?>
    </ul>
</div>
<?php else: ?>
	<?php $this->General->getNoItemMessage(); ?>
<?php endif; ?>