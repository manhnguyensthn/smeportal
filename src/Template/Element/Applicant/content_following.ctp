<?php if(isset($Followings) && !empty($Followings)): ?>
	<ul class="clearfix template_applicant_follow">
		<?php
	        echo $this->element('Applicant/item_follow', array(
				'Follows' => isset($Followings) ? $Followings : array(),
				'type'   => 'following',
	        ));
	    ?>
	</ul>

	<?php if($user->TotalFollowing >= $PageSize): ?>
	<div class="text-center button-see-more">
	    <a href="javascript:void(0);" onclick="getSeeMoreFollowing(this, '2','<?php echo $user->id; ?>');" class="template-see-more-v2" >
	        <span><?php echo __('See more'); ?></span>
	        <span class="loading">
	        	<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
	        	<?php echo __('Loading...'); ?>
	        </span>
	    </a>
	</div>
	<?php endif; ?>
<?php else: ?>
	<?php $this->General->getNoItemMessage(); ?>
<?php endif; ?>