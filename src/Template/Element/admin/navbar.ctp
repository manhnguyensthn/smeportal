<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active treeview">
                <a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard'); ?></span>
                </a>
            </li>
            <li class="active treeview">
                <a href="<?= $this->Link->build(['controller' => 'Projects','action' => 'index']); ?>">
                    <i class="fa fa-book"></i> <span><?php echo __('Projects'); ?></span>
                </a>
            </li>


            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Categories','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Lĩnh vực'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Scales','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Quy mô vốn'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Quantities','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Quy mô lao động'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Groups','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Ban chấp hành'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Roles','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Vai trò'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Validation_codes','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Validation_codes'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Users','action' => 'index']); ?>"><i class="fa fa-user"></i></i></i> <span><?php echo __('Users'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'AboutUs','action' => 'index']); ?>"><i class="fa fa-user"></i></i></i> <span><?php echo __('AboutUs'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'TermOfUse','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('Term of use'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Questions','action' => 'index']); ?>"><i class="fa fa-newspaper-o"></i> <span><?php echo __('FAQ & Guide'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>"><i class="fa fa-gears"></i> <span><?php echo __('Settings'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'Sliders','action' => 'index']); ?>"><i class="fa fa-sliders"></i> <span><?php echo __('Sliders'); ?></span></a></li>
            <li class="treeview"><a href="<?= $this->Link->build(['controller' => 'WeeklyReceive','action' => 'index']); ?>"><i class="fa fa-envelope"></i> <span><?php echo __('News Letters'); ?></span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
