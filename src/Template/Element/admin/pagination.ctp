<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row" style="margin-left: 0; margin-right: 0">
    <div class="col-sm-5">
        <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">
                    <?= 
                        $this->Paginator->counter(
                            'Show {{current}} in {{count}} total slider'
                        );
                    ?>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                    <?php if($this->Paginator->numbers()){ ?>
                        <ul class="pagination">
                            <?php echo $this->Paginator->first('<< ') ?>
                            <?php echo $this->Paginator->prev('< ') ?>
                            <?php echo $this->Paginator->numbers() ?>
                            <?php echo $this->Paginator->next(' >') ?>
                            <?php echo $this->Paginator->last('>> ') ?>       
                        </ul>
                    <?php } ?>
        </div>
    </div>
</div>