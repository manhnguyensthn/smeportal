<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?= $this->Link->build(['controller' => 'Dashboard','action' => 'index']); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SME</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b><?php echo __('We The Projects'); ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only"><?php echo __('Toggle navigation'); ?></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu" style="background-color: rgba(0,0,0,0.1);">
                    <a href="<?= $this->Link->build(['controller' => 'Users','action' => 'logout']); ?>">
                        <span class="hidden-xs"><?php echo __('Logout'); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>