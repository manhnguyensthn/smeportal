<?php if(isset($ProjectUpdate) && !empty($ProjectUpdate)): ?>
    <?php if($ProjectUpdate->type == UPDATE_TYPE): ?>
        <li class="update dropdown project_update_<?php echo $ProjectUpdate->id; ?>">
            <div class="content" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="date-timeline"><?php echo date("F j", strtotime($ProjectUpdate->created)); ?></div>
                <div class="title-timeline"><?php echo strip_tags($ProjectUpdate->title); ?></div>
                <div class="content-timeline"><?php echo strip_tags($ProjectUpdate->content); ?></div>
            </div>
            
            <?php if(!isset($IsNotForm) || !$IsNotForm): ?>
            <div class="form-update dropdown-menu" aria-labelledby="dLabel">
                <div class="item">
                    <div class="title"><a href="javascript:void(0);"><?php echo __('Updates'); ?></a></div>
                    <form action="javascript:void(0);" method="POST" onsubmit="updateProjectUpdate(this, '<?php echo $ProjectUpdate['id']; ?>', 'update'); return false;">
                        <div class="form-group">
                            <input type="text" name="title" placeholder="<?php echo __('Update Title'); ?>" class="form-control" value="<?php echo strip_tags($ProjectUpdate->title); ?>">
                        </div>
                        <div class="form-group">
                            <textarea name="description" id="" placeholder="<?php echo __('Description'); ?>" class="form-control"><?php echo strip_tags($ProjectUpdate->content); ?></textarea>
                        </div>

                        <div class="text-right">
                            <a href="javascript:void(0);" class="button" data-action="deleteProjectUpdate(this, '<?php echo $ProjectUpdate['id']; ?>', 'delete');" onclick="confirmBox(this, '<?php echo __('Are you sure you want to delete?'); ?>');"><?php echo __('Delete'); ?></a>
                            <button type="submit" class="button"><?php echo __('Update'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
            <?php endif; ?>
        </li>
    <?php elseif($ProjectUpdate->type == MILESTONE_TYPE): ?>
        <li class="milestone dropdown project_update_<?php echo $ProjectUpdate->id; ?>">
            <div class="content" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                <div class="date-timeline"><?php echo date("F j", strtotime($ProjectUpdate->created)); ?></div>
                <div class="title-timeline"><?php echo strip_tags($ProjectUpdate->title); ?></div>
                <div class="content-timeline"><?php echo strip_tags($ProjectUpdate->content); ?></div>
            </div>

            <?php if(!isset($IsNotForm) || !$IsNotForm): ?>
            <div class="form-update dropdown-menu" aria-labelledby="dLabel">
                <div class="item">
                    <div class="title"><a href="javascript:void(0);"><?php echo __('Update Milestone'); ?></a></div>
                    <form action="javascript:void(0);" method="POST" onsubmit="updateProjectMilestone(this, '<?php echo $ProjectUpdate['id']; ?>', 'update'); return false;">
                        <div class="form-group">
                            <input type="text" name="title" placeholder="<?php echo __('Update Title'); ?>" class="form-control" value="<?php echo strip_tags($ProjectUpdate->title); ?>">
                        </div>
                        <div class="form-group">
                            <textarea name="description" id="" placeholder="<?php echo __('Description'); ?>" class="form-control"><?php echo strip_tags($ProjectUpdate->content); ?></textarea>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="button"><?php echo __('Update'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
            <?php endif; ?>
        </li>
    <?php endif; ?>
<?php endif; ?>
