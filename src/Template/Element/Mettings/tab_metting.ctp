<?php
	$Tabs = array();
	$Tabs[] = array(
		'title'  => __('Messages'),
		'link'   => '/chat-room/'.$Project->id.'/0',
		'active' => false,
		'action' => 'chatRoomDetail',
	);


	$Tabs[] = array(
		'title'  => __('Updates'),
		'link'   => $this->Url->build(["controller" => "mettings","action" => "update", isset($Project->id) ? $Project->id : '']),
		'active' => false,
		'action' => 'update',
	);


	foreach ($Tabs as $key => $Tab) {
		if($Tab['action'] == $this->request->action){
			$Tabs[$key]['active'] = true;
		}
	}
?>


<div class="container">
    <ul class="nav nav-tabs cleafix" id="list-myfolder-tabs" role="tablist">
    	<?php foreach ($Tabs as $key => $Tab): ?>
			<li class="<?php echo ($Tab['active'] ? 'active' : ''); ?>">
				<a href="<?php echo ($Tab['link'] ? $Tab['link'] : '#'); ?>"><?php echo $Tab['title']; ?></a>
			</li>
    	<?php endforeach; ?>
    </ul>
</div>




<!--Tab isset and schedule-->	
<!-- 
	$Tabs[] = array(
		'title'  => __('Assets'),
		'link'   => $this->Url->build(["controller" => "mettings", "action" => "asset", isset($Project->id) ? $Project->id : '']),
		'active' => false,
		'action' => 'asset',
	);

		$Tabs[] = array(
		'title'  => __('Schedule'),
		'link'   => $this->Url->build(["controller" => "mettings", "action" => "schedule", isset($Project->id) ? $Project->id : '']),
		'active' => false,
		'action' => 'schedule',
	);
-->