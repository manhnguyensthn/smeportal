<?php if(isset($ListFolder) && !empty($ListFolder) && isset($ListFolder['.tag'])): ?>
	<?php if($ListFolder['.tag'] == 'folder') : ?>

		<div class="table_row">
			<div class="table_cell text-center"><a href="#"><span class="icon asset_icon folder"></span></a></div>
			<div class="table_cell name"><a href="#" title="<?php echo $ListFolder['name']; ?>"><?php echo $ListFolder['name']; ?></a></div>
			<div class="table_cell"><a href="#"><?php echo (isset($Project['user']['name']) ? $Project['user']['name'] : '-'); ?></a></div>
			<div class="table_cell"><a href="#"><?php echo (isset($ListFolder['server_modified']) ? $ListFolder['server_modified'] : '-'); ?></a></div>
			<div class="table_cell"><a href="#"><?php echo (isset($ListFolder['size']) ? $ListFolder['size'] : '-'); ?></a></div>
		</div>

	<?php elseif($ListFolder['.tag'] == 'file'): ?>
		<?php if(isset($ListFolder['type_file'])): ?>
			<div class="table_row <?php echo (isset($ListFolder['wtpid']) ? $ListFolder['wtpid'] : ''); ?>">
				<div class="table_cell text-center cell_icon">
					<a href="javascript:void(0)" onclick="downloadFileDropbox(this, '<?php echo $project_id; ?>', '<?php echo base64_encode($ListFolder['name']); ?>');">
						<span class="icon asset_icon <?php echo $ListFolder['type_file']; ?>"></span>
					</a>
					<?php if($bOwner): ?>
						<a href="javascript:void(0)" data-action="deleteFileDropbox(this, &#39;<?php echo $project_id; ?>&#39;, &#39;<?php echo base64_encode($ListFolder['name']); ?>&#39;);" onclick="confirmBox(this, '<?php echo __('Are you sure you want to delete?'); ?>');" class="delete_file" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
					<?php endif; ?>
				</div>
				<div class="table_cell name">
					<a href="javascript:void(0)" onclick="downloadFileDropbox(this, '<?php echo $project_id; ?>', '<?php echo base64_encode($ListFolder['name']); ?>');">
						<?php echo $ListFolder['name']; ?>
					</a>
				</div>
				<div class="table_cell">
					<a href="javascript:void(0)">
						<?php echo (isset($Project['user']['name']) ? $Project['user']['name'] : '-'); ?>
					</a>
				</div>
				<div class="table_cell">
					<a href="javascript:void(0)">
						<?php echo (isset($ListFolder['server_modified']) ? $ListFolder['server_modified'] : '-'); ?>
					</a>
				</div>
				<div class="table_cell">
					<a href="javascript:void(0)">
						<?php echo (isset($ListFolder['size']) ? $ListFolder['size'] : '-'); ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
