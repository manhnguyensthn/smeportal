<?php if(isset($Blockeds) && !empty($Blockeds)): ?>
	<?php foreach ($Blockeds as $key => $Blocked) : ?>
		<li class="item clearfix user_following_<?php echo $Blocked->userid; ?>">
			<?php echo $this->General->getAvata($Blocked, '/applicant-profile-' . $Blocked->userid); ?>
			<div class="content_item">
				<div class="title"><a href="/applicant-profile-<?php echo $Blocked->userid; ?>"><?php echo $Blocked->name; ?></a></div>
				<div class="coutries"><?php echo $Blocked->address; ?></div>
				<div class="roles"><?php echo $Blocked->roles; ?></div>
				<?php echo $this->General->templateBlockUser($Blocked->userid, 'unblock'); ?>
			</div>
		</li>
	<?php endforeach; ?>
<?php endif; ?>