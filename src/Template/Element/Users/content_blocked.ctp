<?php if(isset($Blockeds) && !empty($Blockeds)): ?>
	<ul class="clearfix template_applicant_follow">
		<?php
	        echo $this->element('Users/item_blocked', array(
				'Blockeds' => isset($Blockeds) ? $Blockeds : array(),
				'type'   => 'blocked',
	        ));
	    ?>
	</ul>

	<?php if(count($Blockeds) >= $PageSize): ?>
	<div class="text-center button-see-more">
	    <a href="javascript:void(0);" onclick="getSeeMoreBlocked(this, '2');" class="template-see-more-v2" >
	        <span><?php echo __('See more'); ?></span>
	        <span class="loading">
	        	<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>
	        	<?php echo __('Loading...'); ?>
	        </span>
	    </a>
	</div>
	<?php endif; ?>
<?php else: ?>
	<?php $this->General->getNoItemMessage(); ?>
<?php endif; ?>