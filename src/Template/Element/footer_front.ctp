<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
  .we-footer{
    margin-bottom: 0px;
  }
</style>
<div class="clearfix"></div>
<div class="we-footer overflow">
    <div style="width:100%"class="we-footer-content ">
        <div class="col-lg-9">
        <div class="col-lg-1">
            <h2>
                Hỗ trợ:
            </h2>
        </div>
        <div class="col-md-11">
            <ul>
                <li>
                    <?= $this->Html->link(__('Về chúng tôi >>'), ['controller' => 'pages', 'action' => 'about']); ?>
                </li>
                <li>
                    <?= $this->Html->link(__('Những câu hỏi thường gặp >>'), ['controller' => 'pages', 'action' => 'faq']); ?>
                </li>
                <li>
                    <?= $this->Html->link(__('Quy định tham gia và sử dụng tài nguyên >>'), ['controller' => 'pages', 'action' => 'TermsOfUser']); ?>
                </li>
                <li>
                    <?= $this->Html->link(__('Hướng dẫn sử dụng >>'), ['controller' => 'pages', 'action' => 'guide']); ?>
                </li>
            </ul>
        </div>
        <div class="row" style="margin-top: 150px;">
          
            <div class="logo footer-logo">
                <a href="/">
                    <img src="/img/logo.png">
                </a>

            </div>
            <div style="margin-top: 50px;">
            <p><strong>Hiệp hội doanh nghiệp nhỏ và vừa. Bản quyền 2017.</strong></p>
            </div>
        </div>
        </div>
        <div class="col-lg-3 pull-right receive-new" style="padding-right: 50px;">

          <div class="row">
            
            <form action="javascript:void(0)" onsubmit="actionSignupNewsletter(this); return false;">
                <input type="text" required="required" placeholder="<?php echo __('Your email address'); ?>" class="form-control email-fill" name="email"/>
                <button style="width: 215px;" type="submit" class="btn btn-warning btn-signup"><?php echo __('Đăng ký nhận tin'); ?></button>
            </form>
          </div>


            <div class="row" style="margin-top:0px; padding-top: 25px; display:block" >
              <!-- <div class="col-lg-4 " style="position:relative; margin-left: auto;"> -->

                <a href="http://vietis.com.vn/"><img style="zoom:50%" src="/img/vietis_logo.png" style="margin-top: -30px; zoom: 80%"></a>
              <!-- </div> -->
              <!-- <div class="col-lg-8" style="margin-left: -20px;"> -->
                <p><strong>VietIS hân hạnh tài trợ chương trình.</strong></p>
              <!-- </div>   -->
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <!-- <div class="we-footer-logo">

        <div class="col-lg-8 padd-all">
            <div class="logo footer-logo">
                <a href="/">
                    <img src="/img/logo.png">
                </a>

            </div>
            <div style="margin-top: 20px;">
            <p><strong>Hiệp hội doanh nghiệp nhỏ và vừa. Bản quyền 2017.</strong></p>
            </div>
        </div>
        <div class="col-lg-4 follows-icon vietis-right" style="margin-right: 0px;">
            <div class="" style="margin-top:20px; margin-right: -38px;" >
              <img src="/img/vietis.png">
              <p><strong>VietIS hân hạnh tài trợ chương trình.</strong></p>
            </div>
        </div>
    </div> -->
</div>
<div class="modal fade" id="box-loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div class="row">
              <div class="col-xs-12 text-center">
                  <img src="/img/ring.gif" />
                  <p><?php echo __('System is loading. Please wait ...'); ?></p>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
