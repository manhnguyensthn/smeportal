<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first(__('First'), array('escape' => false, 'tag' => 'a'), null, array('escape' => false, 'tag' => 'a', 'class' => '', 'disabledTag' => 'span')); ?>
        <?= $this->Paginator->prev('« ') ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next( ' »') ?>
        <?= $this->Paginator->last(__('Last'), array('escape' => false, 'tag' => 'a'), null, array('escape' => false, 'tag' => 'a', 'class' => '', 'disabledTag' => 'span')); ?>
    </ul>
</div>