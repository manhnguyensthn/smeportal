<?php
	$aTabs = array();
	$aTabs['profile.edit'] = array(
		'title' => __('Edit Profile'),
		'link' => '/profile/edit',
		'active' => false,
	);
	$aTabs['users.detail'] = array(
		'title' => __('Account Details'),
		'link' => '/users/detail',
		'active' => false,
	);
	$aTabs['users.notifications'] = array(
		'title' => __('Notifications'),
		'link' => '/users/notifications',
		'active' => false,
	);
	$aTabs['users.blocked'] = array(
		'title' => __('Blocked Users'),
		'link' => '/users/blocked',
		'active' => false,
	);
	$Action = strtolower($this->request->action);
	$Controller = strtolower($this->request->controller);
	$Active = $Controller . '.' . $Action;
?>

<div class="menu-child">
    <ul class="ulMenu clearfix">
    	<?php foreach ($aTabs as $key => $aTab): ?>
			<li class="menuLink <?php echo ($Active == $key) ? 'active' : ''; ?>"><a href="<?php echo $aTab['link']; ?>"><?php echo $aTab['title']; ?></a></li>
    	<?php endforeach; ?>
    </ul>
    <div class="clearfix"></div>
</div>