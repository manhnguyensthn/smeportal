<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$ProjectController = new App\Controller\ProjectsController();
$myProjects = $ProjectController->getMyProjects();
$myJoinProject = $ProjectController->getMyJoinProjects();
if (!empty($myJoinProject)) {
    $nameproject = $ProjectController->getNameProjects($myJoinProject[0]['project_id']);
}
?>
<?php
$aRequestQuery = $this->request->query;
$keyword = (isset($aRequestQuery['keyword']) ? $aRequestQuery['keyword'] : '');
$filterCategory = (isset($aRequestQuery['filterCategory']) ? $aRequestQuery['filterCategory'] : '');
$sortBy = (isset($aRequestQuery['sortBy']) ? $aRequestQuery['sortBy'] : '');
$filterRole = (isset($aRequestQuery['filterRole']) ? $aRequestQuery['filterRole'] : '');
$filterLocation = (isset($aRequestQuery['filterLocation']) ? $aRequestQuery['filterLocation'] : '');
?>
<style type="text/css">
    h4 {
        color: white;
    }
</style>
<div class="clearfix"></div>
<nav id="navBarWTP" class="navbar navbar-default navbar-custom">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header col-md-4 padd-all">
            <form action="/discover/search" method="GET">
                <div class="row" style="margin: 10px; margin-bottom:-10px;">
                    <!--<div class="col-md-1" style="margin-right: -20px;">
                        <button type="submit" class="showSearch"><i class="fa fa-search" aria-hidden="true"></i>
                        </button>

                    </div>-->
                    <div class="col-md-11 purple search_box header_search">
                        <div class="form-group">
                            <input type="hidden" name="filterCategory" value="<?php echo $filterCategory; ?>">
                            <input type="hidden" name="sortBy" value="<?php echo $sortBy; ?>">
                            <input type="hidden" name="filterRole" value="<?php echo $filterRole; ?>">
                            <input type="hidden" name="filterLocation" value="<?php echo $filterLocation; ?>">
                            <input type="text" class="form-control input-medium"
                                   placeholder="<?php echo __('Search'); ?>" name="keyword"
                                   value="<?php echo $keyword; ?>">
                        </div>

            </form>
        </div>
    </div>
    </div>
    <div class="navbar-header col-md-4 padd-all">
        <center>
            <a href="/"><img src="/img/logo.png" class="img-responsive logo header_logo"/></a>
        </center>
    </div>
    <div class="navbar-header col-md-4 padd-all">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="dropdown pull-right">
                <button id="userInfo" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $authUser['first_name'] . ' ' . $authUser['last_name']; ?>
                    <i style="color: #eb6500; font-size: 16px; padding-left: 10px; position: absolute; right: 0; top: 50%; margin-top: -8px;"
                       class="fa fa-chevron-down" aria-hidden="true"></i>
                </button>
                <ul id="menuDown" class="dropdown-menu" aria-labelledby="dLabel">
                    <li class="itemMenu">
                        <h4><?php echo __('My SME'); ?></h4>
                        <ul class="listChild">
                            <li>
                                <a href="/applicant-profile-<?php echo $authUser['id']; ?>"><?php echo __('Profile'); ?></a>
                            </li>
                            <li><a href="projects/create"><?php echo __('Tạo doanh nghiệp'); ?></a></li>
                            <li>
                                <a href="/discover/index?filterCategory=4|Following|1|1&sortBy=1|Popularity%20(viewed)|2|3&filterRole=0|Role%20Available|3|4&filterLocation=1|Everywhere|4|5#"><?php echo __('Follow'); ?></a>
                            </li>
                            <?php if ($authUser['admin'] != 0): ?>
                                <li><a href="/projects/list"><?php echo __('Tình trạng doanh nghiệp'); ?></a></li>
                            <?php endif; ?>
                            <li><a href="/requests/create"><?php echo __('Tạo yêu cầu/kiến nghị'); ?></a></li>
                            <li><a href="/request/list"><?php echo __('Yêu cầu/kiến nghị đã gửi'); ?></a></li>
                            <li><a href="/requests/index"><?php echo __('Yêu cầu/kiến nghị đã nhận'); ?></a></li>
                            <li><a href="/applicant-findmore-<?php if (isset($myProjects->id)) {
                                    echo $myProjects->id;
                                } else echo 0 ?>"><?php echo __('Tìm thêm người dùng'); ?></a></li>
                        </ul>
                    </li>
                    <?php if (!empty($myProjects)): ?>
                        <li class="itemMenu">
                            <h4><?php echo "Doanh nghiệp" ?></h4>
                            <ul class="listChild">
                                <li>
                                    <!--                                        <a href="/projects/-->
                                    <?php //echo $this->Tool->slug($listCreatedProjects[0]->title) . '-' . $listCreatedProjects[0]->id; ?><!--"><h5 class="project-title">-->
                                    <?php //echo $listCreatedProjects[0]->title; ?><!--</h5></a>-->
                                    <a href="/projects/<?php echo $this->Tool->slug($myProjects->title) . '-' . $myProjects->id; ?>"><?php echo $myProjects->title; ?></a>
                                    <ul class="clearfix" style="margin-left: -16px;">
                                        <li>
                                            <a href="/chat-room/<?php echo $myProjects->id; ?>/0">- <?php echo __('Chat với thành viên'); ?></a>
                                        </li>
                                        <li><?= $this->Html->link('- ' . __('Thành tích doanh nghiệp'), ['controller' => 'mettings', 'action' => 'update', $myProjects->id]); ?></li>

                                        <!-- <li><a href="applicant-findmore-<?php echo $myProjects->id ?>"><?php echo __('Vai trò'); ?></a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li class="itemMenu">
                            <h4><?php echo __('My folder'); ?></h4>
                        </li>
                    <?php endif; ?>
                    <div class="clearfix" style="margin-bottom: 0px;"></div>
                    <li class="itemMenu">
                        <h4><?php echo __('Settings'); ?></h4>
                        <ul class="listChild">
                            <li><?= $this->Html->link(__('Account Details'), ['controller' => 'Users', 'action' => 'detail']); ?></li>
                            <li><?= $this->Html->link(__('Edit Profile'), ['controller' => 'Profile', 'action' => 'edit']); ?></li>
                            <li><?= $this->Html->link(__('Cấu hình thông báo'), ['controller' => 'users', 'action' => 'notifications']); ?></li>
                            <li><?= $this->Html->link(__('Blocked Users'), ['controller' => 'users', 'action' => 'blocked']); ?></li>
                        </ul>
                    </li>
                    <?php if (!empty($myJoinProject)) { ?>
                        <li class="itemMenu" style="width: 50%;">
                            <ul class="listChild">
                                <li>
                                    <p class="menu_title_pro"><?php echo __('Doanh nghiệp đã tham gia') ?></p>
                                    <a href="/projects/<?php echo $this->Tool->slug($nameproject->title) . '-' . $nameproject->id; ?>">
                                        <p class="menu_title_pro"><?php echo $nameproject->title; ?></p></a>
                                    <ul class="clearfix" style="margin-left: -16px;">
                                        <li>
                                            <a href="/chat-room/<?php echo $myJoinProject[0]['project_id']; ?>/0">- <?php echo __('Chat với thành viên'); ?></a>
                                        </li>
                                        <!--                                            <li>-->
                                        <? //= $this->Html->link('- ' . __('Assets'), ['controller' => 'mettings', 'action' => 'asset', $myJoinProject[0]['project_id']]); ?><!--</li>-->
                                        <li><?= $this->Html->link('- ' . __('Thành tích doanh nghiệp'), ['controller' => 'mettings', 'action' => 'update', $myJoinProject[0]['project_id']]); ?></li>
                                        <!--                                            <li>-->
                                        <? //= $this->Html->link('- ' . __('Schedules'), ['controller' => 'mettings', 'action' => 'schedule', $myJoinProject[0]['project_id']]); ?><!--</a></li>-->
                                        <li>
                                            <a href="/applicant-findmore-<?php if (isset($myJoinProject[0]['project_id'])) {
                                                echo $myJoinProject[0]['project_id'];
                                            } else echo 0 ?>">- <?php echo __('Find more'); ?></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="itemBottom">
                        <div class="clearfix"></div>
                        <center><p><?php echo __('You are logged in as'); ?> <strong
                                        class="current-user"><?php echo $authUser['first_name'] . ' ' . $authUser['last_name']; ?></strong>
                                <a href="/users/logout"><strong><?php echo __('Log Out'); ?></strong></a></p></center>
                    </li>
                </ul>

            </div>
            <?php
            $notificationController = new \App\Controller\NotificationsController();
            $notificationString = $notificationController->getNotificationByCurrentUser();
            ?>
            <a href="/my-folder" class="icon-folder"><img src="/img/folder.png" title="My folder" alt="My folder"
                                                          class="img-responsive "/></a>
            <div class="dropdown pull-right" style="margin-top: ">
                <a href="" onclick="show_list_notification();" class="icon-alarm" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"><?php echo $notificationString; ?><img
                            src="/img/alarm.png" title="Notifications" alt="Notification"/></a>
                <div class="box-list-notifications dropdown-menu">
                </div>
            </div>
        </div><!-- /.navbar-collapse -->

    </div>
    </div><!-- /.container-fluid -->
</nav>
<div class="clearfix"></div>
