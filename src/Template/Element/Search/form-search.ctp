<?php
	$aRequestQuery  = $this->request->query;
	$keyword        = (isset($aRequestQuery['keyword']) ? $aRequestQuery['keyword'] : '');
	$filterCategory = (isset($aRequestQuery['filterCategory']) ? $aRequestQuery['filterCategory'] : '');
	$sortBy         = (isset($aRequestQuery['sortBy']) ? $aRequestQuery['sortBy'] : '');
	$filterRole     = (isset($aRequestQuery['filterRole']) ? $aRequestQuery['filterRole'] : '');
	$filterLocation = (isset($aRequestQuery['filterLocation']) ? $aRequestQuery['filterLocation'] : '');
?>
<nav id="searchArea" class="navbar navbar-default navbar-custom hidden">
    <div class="container-fluid">
        <a href="javascript:void(0)" class="showSearch"><i class="fa fa-search" aria-hidden="true"></i></a>
        <form class="navbar-form navbar-left form-custom" action="/discover/search" method="GET">
            <div class="form-group">
            	<input type="hidden" name="filterCategory" value="<?php echo $filterCategory; ?>">
            	<input type="hidden" name="sortBy" value="<?php echo $sortBy; ?>">
            	<input type="hidden" name="filterRole" value="<?php echo $filterRole; ?>">
            	<input type="hidden" name="filterLocation" value="<?php echo $filterLocation; ?>">
                <input type="text" class="form-control" placeholder="<?php echo __('Search'); ?>" name="keyword" value="<?php echo $keyword; ?>">
            </div>
        </form>
        <button id="closeSearch" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div><!-- /.container-fluid -->
</nav>