<style>
    table th{
        text-align: center;
    }
</style>
<div class="container-fluid">
  <div class="col-md-10">
    <h3>Danh sách yêu cầu kiến nghị đã nhận</h3>
    <table class="table table-bordered">
      <tr>
        <th>Mã</th>
        <th>Tiêu đề</th>
        <th>Doanh nghiệp gửi</th>
        <th>Loại yêu cầu</th>
        <th></th>
      </tr>
      <?php $stt = 1; ?>
      <?php foreach ($requests as $request): ?>
      <tr>
        <td><?php echo $request['id']; ?></td>
        <td><?php echo $request['title'] ?></td>
        <td><?php echo $project['title'] ?></td>
        <td>
          <?php if($request['type'] == 1): ?>
            <p>Quản lý hội viên</p>
          <?php elseif($request['type'] == 2): ?>
            <p>Pháp lý</p>
          <?php elseif($request['type'] == 3): ?>
            <p>Xúc tiến thương mại</p>
          <?php elseif($request['type'] == 4): ?>
            <p>Vay vốn</p>
          <?php endif; ?>
        </td>
        <td><?php echo $this->Html->link('Xem',['controller'=>'requests','action'=>'view',$request['id']]) ?></td>
      </tr>
      <?php $stt++;?>
      <?php endforeach; ?>
    </table>
      <div class="paginator">
          <ul class="pagination">
              <?= $this->Paginator->first('<< ' . __('Đầu')) ?>
              <?= $this->Paginator->prev('< ' . __('Trước')) ?>
              <?= $this->Paginator->numbers() ?>
              <?= $this->Paginator->next(__('Sau') . ' >') ?>
              <?= $this->Paginator->last(__('Cuối') . ' >>') ?>
          </ul>
      </div>
  </div>

  <div class="col-md-12">
      <h3>Bộ lọc</h3>
      <div class="">
      <?= $this->Form->create(null,['type'=>'get']) ?>
        <?php echo $this->Form->input('group', [
            'class' => 'form-control',
            'label' => __('Nhóm'),
            'type' => 'select',
            'options' => $groups,
            'empty' => __('-- Chọn nhóm --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <!-- <?php echo $this->Form->input('status', [
            'class' => 'form-control',
            'label' => __('Tình trạng'),
            'type' => 'select',
            'options' => [1=>'Chưa duyệt',2=>'Đang xử lý',3=>'Đã gửi'],
            // 'value' => 4,
            'empty' => __('-- Chọn tình trạng --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?> -->
        <?php echo $this->Form->input('type1', [
            'class' => 'form-control',
            'label' => __('Kiểu yêu cầu'),
            'type' => 'select',
            'options' => [1=>'Pháp lý',2=>'Vốn vay',3=>'Xúc tiến',4=>'Quản lý hội viên'],
            'empty' => __('-- Chọn kiểu yêu cầu --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('location', [
            'class' => 'form-control',
            'label' => __('Khu vực'),
            'type' => 'select',
            'options' => $locations,
            'empty' => __('-- Chọn khu vực --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('category', [
            'class' => 'form-control',
            'label' => __('Lĩnh vực'),
            'type' => 'select',
            'options' => $categories,
            'empty' => __('-- Chọn lĩnh vực --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
        <?php echo $this->Form->input('role', [
            'class' => 'form-control',
            'label' => __('Vai trò'),
            'type' => 'select',
            'options' => $usersType,
            'empty' => __('-- Vai tro--'),
            'templates' => [
                'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>
      </div>
      <div class>
          <?php echo $this->Form->input('projectReceive', [
              'class' => 'form-control request',
              'label' => __('Doanh nghiệp nhận'),
              'type' => 'text',
              'id' => 'projectOwner',
              'maxlength' => 100,
              'templates' => [
                  'inputContainer' => '<div class="col-md-2  input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>

          <?php echo $this->Form->input('projectSend', [
              'class' => 'form-control request',
              'label' => __('Doanh nghiệp gửi'),
              'type' => 'text',
              'id' => 'project',
              'maxlength' => 100,
              'templates' => [
                  'inputContainer' => '<div class="col-md-2  input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>
      <button type="submit" id="btn-submit" status="0" class="btn btn-warning btn-launch btn-custom pull-right" style="color: #080707;">Lọc dữ liệu</button>
      <!-- <?= $this->Form->button(__('Lọc'),["class"=>"btn btn-lg-6 pull-right","id"=>"confirm"]) ?> -->
      <?= $this->Form->end() ?>
      </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        var userID = <?php echo $authUser['id']; ?>;
        var availableTags = [];
        $.ajax({
            type: "POST",
            url: "../api/projects/getlistOwnerProject.json",
            data: {userID:userID},
            dataType: 'json',
            success: function (item) {
                var item = item['data'];
                for (var i = 0; i < item.length; i++) {
                    availableTags.push(item[i].title);
                }
            }
        });
        $("#projectOwner").autocomplete({
            source: availableTags
        });

        var availableTags1 = [];
        $.ajax({
            type: "POST",
            url: "../api/projects/getlistproject.json",
            dataType: 'json',
            success: function (item) {
                var item = item['data'];
                for (var i = 0; i < item.length; i++) {
                    availableTags1.push(item[i].title);
                }
            }
        });

        $("#project").autocomplete({
            source: availableTags1
        });


    });

</script>