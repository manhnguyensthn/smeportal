<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="requests form col-lg-12">
    <?= $this->Form->create($request) ?>
<div class="col-lg-6">
  <fieldset>
        <h3><?= __('Thêm yêu cầu/kiến nghị') ?></h3>
        <div class="input text col-md-6">
            <?php echo $this->Form->input('type', [
                'class' => 'form-control',
                'label' => __('Kiểu yêu cầu'),
                'type' => 'select',
                'options' => ['Pháp lý','Vốn vay','Xúc tiến','Quản lý hội viên'],
                'empty' => __('-- Chọn kiểu yêu cầu --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-4 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <?php echo $this->Form->input('project_id', [
                'class' => 'form-control',
                'label' => __('Từ doanh nghiệp'),
                'type' => 'select',
                'options' => $ownerProjects,
                'empty' => __('-- Chọn doanh nghiệp --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
        </div>
        <div class="input text col-md-6">

        </div>
    </fieldset>


    <fieldset style="margin-top: 20px">
      <div class="input texarea col-md-6">
          <?php
          echo '<label for="content">'.__('Nội dung').'</label>';
          echo $this->Form->textarea('content', array('maxlength' => 500, 'class' => 'form-control', 'rows' => 7, 'label' => __('Nội dung')));
          ?>
      </div>
     
    </fieldset>

<!-- End request -->

<!-- Begin reply -->
    <fieldset class="request">
        <legend><?= __('Phản hồi yêu cầu/kiến nghị') ?></legend>
        <div class="input text col-md-6">
            <label for="">Được duyệt bởi: </label>
            <label>Admin 1</label>
        </div>
    </fieldset>
    <fieldset>
      <div class="input texarea col-md-6">
          <?php
          echo '<label for="repcontent1">'.__('Nội dung').'</label>';
          echo $this->Form->textarea('repcontent1', array('maxlength' => 500, 'class' => 'form-control', 'rows' => 7, 'label' => __('Nội dung phản hồi')));
          ?>
      </div>
      <label>Đính kèm 1: </label><input type="file" name="attach1" class="custom_file" maxlength="100" id="attach1" value="">
      <label>Đính kèm 2: </label><input type="file" name="attach2" class="custom_file" maxlength="100" id="attach1" value="">
    </fieldset>
    <fieldset>
        <div class="input text col-md-6">
            <label for="">Được phản hồi bởi: </label>
            <label>Mod</label>

        </div>
    </fieldset>
    <fieldset>
      <div class="input texarea col-md-6">
          <?php
          echo '<label for="repcontent2">'.__('').'</label>';
          echo $this->Form->textarea('repcontent2', array('maxlength' => 500, 'class' => 'form-control', 'rows' => 7, 'label' => __('Nội dung phản hồi')));
          ?>
      </div>
    </fieldset>
<!-- End reply -->

    <?= $this->Form->button(__('Xác nhận, lưu chuyển'),["class"=>"btn btn-lg-6 pull-right"]) ?>
    <?= $this->Form->end() ?>
</div>
</div>
<div class="col-lg-6">
   <div class="col-md-3">
        <label>Đính kèm 1: </label><input type="file" name="attach1" class="custom_file" maxlength="100" id="attach1" value="">
        <label>Đính kèm 2: </label><input type="file" name="attach2" class="custom_file" maxlength="100" id="attach1" value="">
      </div>
      <div class="col-md-3">
        <div class="row">
        <?php echo $this->Form->input('receiver_id1', [
            'class' => 'form-control',
            'label' => __('Tới doanh nghiệp'),
            'type' => 'select',
            'options' => $projects,
            'empty' => __('-- Tới doanh nghiệp --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-10 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>

        </div>
        <div class="row">
        <?php echo $this->Form->input('receiver_id2', [
            'class' => 'form-control',
            'label' => __('Tới doanh nghiệp'),
            'type' => 'select',
            'options' => $projects,
            'empty' => __('-- Tới doanh nghiệp --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-10 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
      ]); ?>

      </div>
      <div class="row">
        <?php echo $this->Form->input('receiver_id3', [
            'class' => 'form-control',
            'label' => __('Tới doanh nghiệp'),
            'type' => 'select',
            'options' => $projects,
            'empty' => __('-- Tới doanh nghiệp --'),
            'templates' => [
                'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
      ]); ?>

      </div>
      </div>
</div>
<!-- Begin request -->

    


<style>
  .request{
    margin-top: 20px;
  }
  select option {
    margin: 20px;
    background: #ed6500;
    color: #ffffff;
    text-shadow: 0 1px 0 #ffffff;
}
button{
  background: #ed6500;
}

button:hover{
   background: #ef883b;
}
</style>
    <!-- echo $this->Form->control('title');
    echo $this->Form->control('content');
    echo $this->Form->control('type');
    echo $this->Form->control('project_id', ['options' => $projects]);
    echo $this->Form->control('user_id', ['options' => $users]);
    echo $this->Form->control('receiver_id1');
    echo $this->Form->control('receiver_id2');
    echo $this->Form->control('receiver_id3');
    echo $this->Form->control('attach1');
    echo $this->Form->control('attach2');
    echo $this->Form->control('repattach1');
    echo $this->Form->control('repattach2');
    echo $this->Form->control('repcontent1');
    echo $this->Form->control('repcontent2');
    echo $this->Form->control('status'); -->
