<!-- 

<div class="container" style="margin-top: 40px;">
<h3>Yêu cầu/ kiến nghị</h3>
	<div class="row">
		<div class="col-md-6">
			<ul class="nav nav-tabs">
			  <li class="active"><a href="#title" data-toggle="tab" aria-expanded="false">Tiêu đề</a></li>
			  <li class=""><a href="#type" data-toggle="tab" aria-expanded="false">Loại yêu cầu</a></li>
			  <li class=""><a href="#project" data-toggle="tab" aria-expanded="false">Doanh nghiệp gửi</a></li>
			  <li class=""><a href="#content1" data-toggle="tab" aria-expanded="false">Nội dung</a></li>
			  <li class=""><a href="#file1" data-toggle="tab" aria-expanded="false">Đính kèm</a></li>
			</ul>
			<div id="myTabContent" class="tab-content">
			  <div class="tab-pane fade active in" id="title">
			    <p><?= $request['title'] ?></p>
			  </div>
			  <div class="tab-pane fade" id="type">
			  	<?php if($request['type'] == 1): ?>
			    	<p>Quản lý hội viên</p>
			    <?php elseif($request['type'] == 2): ?>
			    	<p>Pháp lý</p>
			    <?php elseif($request['type'] == 3): ?>
			    	<p>Xúc tiến thương mại</p>
			    <?php elseif($request['type'] == 4): ?>
			    	<p>Vay vốn</p>
			    <?php endif; ?>
			  </div>
			  <div class="tab-pane fade" id="project">
			  <?= $project['title'] ?>
			  </div>
			  <div class="tab-pane fade" id="content1">
			  <?= $request['content'] ?>
			  </div>
			  <div class="tab-pane fade" id="file1">
				  <a href="<?php echo $request['attach1']  ?>" download>Đính kèm 1</a>
				  <a href="<?php echo $request['attach2']  ?>" download>Đính kèm 2</a>
			  </div>
			</div>
		</div>
		<div class="col-md-6">
			<ul class="nav nav-tabs">
			  <li class=""><a href="#replyContent" data-toggle="tab" aria-expanded="false">Admin duyệt</a></li>
			  <li class=""><a href="#replyContent2" data-toggle="tab" aria-expanded="false">Mod duyệt</a></li>
			  <li class=""><a href="#replyFile" data-toggle="tab" aria-expanded="false">Đính kèm duyệt</a></li>
			</ul>
			<div id="myTabContent" class="tab-content">
			  <div class="tab-pane fade" id="replyContent">
			    <p><?= $request['repcontent1'] ?></p>
			  </div>
			  <div class="tab-pane fade" id="replyContent2">
			    <p><?= $request['repcontent2'] ?></p>
			  </div>
			  <div class="tab-pane fade" id="replyFile">
				  <a href="<?php echo $request['repattach1']  ?>" download>Đính kèm 1</a>
				  <a href="<?php echo $request['repattach2']  ?>" download>Đính kèm 2</a>
			  </div>
			</div>
		</div>
	</div>

</div> -->

<div class="container" style="margin-top: 40px;">
	<div class="row">
		<div class="col-md-6">
		<h3 style="text-align: center">Nội dung yêu cầu/ kiến nghị</h3>
			<div class="list-group">
			  <a  class="list-group-item list-group-item-action flex-column align-items-start active">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Tiêu đề yêu cầu</b></h5>
			    </div>
			    <p class="mb-1"><?= substr($request['title'],0,65)."<br>".substr($request['title'],66,100) ?></p>
			  </a>
			  <a class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Loại yêu cầu</b></h5>
			    </div>
			    <p class="mb-1"><?php if($request['type'] == 1): ?>
							    	<p>Quản lý hội viên</p>
							    <?php elseif($request['type'] == 2): ?>
							    	<p>Pháp lý</p>
							    <?php elseif($request['type'] == 3): ?>
							    	<p>Xúc tiến thương mại</p>
							    <?php elseif($request['type'] == 4): ?>
							    	<p>Vay vốn</p>
							    <?php endif; ?></p>
			  </a>
			  <a  class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Doanh nghiệp gửi</b></h5>
			    </div>
			    <p class="mb-1"><?= $projectSend['title'] ?></p>
			  </a>
			  <a  class=" list-group-item list-group-item-action flex-column align-items-start">
			    <div data-toggle="collapse" data-target="#project" class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Doanh nghiệp nhận<small>(Click để xem danh sách)</small></b></h5>
			    </div>
			    <p id="project" class="mb-1 panel-collapse collapse">
			    	<?php foreach($projectReceiver as $items): ?>
			    		- <?php echo $items['title']."<br>" ?>
			    	<?php endforeach; ?>
			    </p>
			  </a>
			  <a class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Nội dung</b></h5>
			    </div>
			    <p class="mb-1"><?= $request['content'] ?></p>
			  </a>
			  <div>
			  <a class=" list-group-item list-group-item-action flex-column align-items-start">
			    <div data-toggle="collapse" data-target="#attach" class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Đính kèm<small>(Click để xem nội dung)</small></b></h5>
			    </div>
			  </a>
			    <?php if($request['attach1'] == null && $request['attach2'] == null): ?>
					<p id="attach" class="mb-1 panel-collapse">Không có tài liệu đính kèm</p>
				<?php else: ?>
				    <p id="attach" class="mb-1 panel-collapse collapse">
				    	<?php if($request['attach1'] != null): ?>
				    		<a class="" href="<?php echo $request['attach1']  ?>" download><b>Đính kèm 1</b></a><br>
				    	<?php endif; ?>
				    	<?php if($request['attach2'] != null): ?>
							<a class="" href="<?php echo $request['attach2']  ?>" download><b>Đính kèm 2</b><br></a>
						<?php endif; ?>
				    </p>
				<?php endif;?>
			  
			  </div>
			</div>
		</div>


		<!--              -->
		<div class="col-md-6">
		<h3 style="text-align: center">Duyệt từ quản trị</h3>
			<div class="list-group">
			  <a  class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Mod duyệt</b></h5>
			    </div>
			    <p class="mb-1">
			    <?php if($request['repcontent2']==null): ?>
			    	Không có thông điệp nào từ Mod
			    <?php else: ?>
			    	<?= $request['repcontent2'] ?>
				<?php endif; ?>
			    </p>
			  </a>
			  <a class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Admin duyệt</b></h5>
			    </div>
			    <p class="mb-1">
			    <?php if($request['repcontent1']==null): ?>
			    	Không có thông điệp nào từ Admin
			    <?php else: ?>
			    	<?= $request['repcontent1'] ?>
				<?php endif; ?>
			    </p>
			  </a>
			  <a class=" list-group-item list-group-item-action flex-column align-items-start">
			    <div data-toggle="collapse" data-target="#repattach" class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1"><b>Đính kèm<small>(Click để xem nội dung)</small></b></h5>
			    </div>
			  </a>
				<?php if($request['repattach2'] == null && $request['repattach2'] == null): ?>
					<p id="repattach" class="mb-1 panel-collapse">Không có tài liệu đính kèm</p>
				<?php else: ?>
				    <p id="repattach" class="mb-1 panel-collapse collapse">
				    	<?php if($request['repattach1'] != null): ?>
				    		<a class="" href="<?php echo $request['repattach1']  ?>" download><b>Đính kèm 1</b></a><br>
				    	<?php endif; ?>
				    	<?php if($request['repattach2'] != null): ?>
							<a class="" href="<?php echo $request['repattach2']  ?>" download><b>Đính kèm 2</b><br></a>
						<?php endif; ?>
				    </p>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>