<?php
/**
  * @var \App\View\AppView $this
  */
?>
<style>
  .request{
    margin-bottom: 10px;
  }
</style>
<div class="requests form col-lg-12">
    <?= $this->Form->create($request, array('type'=>'file')) ?>
<!-- Begin request -->
<div class="col-lg-6">
<div class="row">
    <fieldset>
        <h3 style="margin-left: 180px"><?= __('Phản hồi yêu cầu/kiến nghị') ?></h3>
        <div class="input text col-md-12">
            <?= $this->form->input('title',array('class'=>'form-control request','label'=>'Tiêu đề', 'readonly','required', 'value' => $request->title)); ?>
            
            <div class="row" style="padding-left: 15px">
            <?php echo $this->Form->input('type', [
                'type' => 'hidden',
                'class' => 'form-control request','readonly',
                'label' => __('Kiểu yêu cầu'),
                'value' => $request->type,
                'empty' => __('-- Chọn kiểu yêu cầu --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-8 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <?php echo $this->Form->input('type1', [
                'type' =>'text',
                'class' => 'form-control request','readonly',
                'label' => __('Kiểu yêu cầu'),
                'value' => $type,
                'default' => $type,
                'empty' => __('-- Chọn kiểu yêu cầu --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <?php echo $this->Form->input('project_title', [
                // 'type' => 'hidden',
                'class' => 'form-control request','readonly',
                'style' => 'margin-right: 20px;',
                'label' => __('Từ doanh nghiệp'),
                'value' => $title,
                // 'empty' => __('-- Chọn kiểu yêu cầu --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-6 input padd-left {{title}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
            <!-- <?php echo $this->Form->input('title1', [
                // 'type' => 'hidden',
                'class' => 'form-control request','readonly',
                'label' => __('Từ doanh nghiệp'),
                'value' => $title,
                // 'empty' => __('-- Chọn kiểu yêu cầu --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-6 input padd-left {{title}}{{required}}">{{content}}</div>'
                ]
            ]); ?> -->
            </div>
            <!-- <?php echo $this->Form->input('status',['type'=>'hidden']);  ?> -->
        </div>
        <div class="input text col-md-6">

        </div>
    </fieldset>
    </div>
    <fieldset style="margin-top: 20px">
    <div class="row">
      <div class="input texarea col-md-12">
          <?php
          echo '<label for="content">'.__('Nội dung').'</label>';
          echo $this->Form->textarea('content', array('maxlength' => 500, 'class' => 'form-control request', 'readonly', 'rows' => 7, 'label' => __('Nội dung')));
          ?>
      </div>
      </div>
    </fieldset>
<!-- End request -->

<!-- Begin reply -->
    <fieldset class="request" >
        <legend><?= __('Phản hồi yêu cầu/kiến nghị') ?></legend>

    </fieldset>
    <fieldset>
      <div class="input texarea col-md-12">
      <div class="row">
          <?php
          echo '<label for="repcontent1">'.__('Nội dung phản hồi - Admin').'</label>';
          echo $this->Form->textarea('repcontent1', array('maxlength' => 500, 'class' => 'form-control reply', 'id' => 'reply', 'readonly', 'rows' => 7, 'label' => __('Nội dung phản hồi')));
          ?>
      
      </div>
      </div>
    </fieldset>
    <fieldset>
        <div class="input text col-md-12 mod rep">
          <div class="row">
            <label for="">Nội dung phản hồi -  </label>
            <label>Mod</label>
          </div>
        </div>
    </fieldset>
    <fieldset>
      <div class="input texarea col-md-12">
        <div class="row">
          <?php
          echo '<label for="repcontent2">'.__('').'</label>';
          echo $this->Form->textarea('repcontent2', array('maxlength' => 500, 'class' => 'form-control replymod','readonly', 'rows' => 7, 'label' => __('Nội dung phản hồi')));
          ?>
        </div>  
      </div>
    </fieldset>
    </div>
    <div class="col-md-6">
    <h3 style="margin-left: 100px; margin-bottom: 20px"><?= __('Doanh nghiệp nhận yêu cầu/kiến nghị') ?></h3>  
    <div class="col-md-5">
    <div style="margin-top: 230px;">
      <?php if(!empty($request->attach1)){ ?>
      <a href="<?php echo $request->attach1 ?>" download> Đính kèm 1</a>
      <?php } ?>
      <?php if(!empty($request->attach2)){ ?>
      <a href="<?php echo $request->attach2 ?>" download> Đính kèm 2 </a>
      <?php } ?>

    </div>
     <?php if(!empty($request->attach1)){ ?>
      <div style="margin-top: 235px">
      <?php }  else {?>
        <div style='margin-top:465px'>
        <?php } ?>

      <?php if(!empty($request->repattach1)){ ?>
      <a href="<?php echo $request->repattach1 ?>" download> Đính kèm 1 </a>
      <?php }  else {?>
      <?= $this->form->input('repattach1',['type'=>'file','label'=>'Tài liệu đính kèm 1']) ?>
      <?php } ?>

      <?php if(!empty($request->repattach2)){ ?>
      <a href="<?php echo $request->repattach2 ?>" download> Đính kèm 2 </a>
      <?php }  else {?>
      <?= $this->form->input('repattach2',['type'=>'file','label'=>'Tài liệu đính kèm 2']) ?>
      <?php } ?>
      </div>
      <br>
      <p class="text-danger">Chấp nhận các định dạng PDF, DOCX, DOC, JPEG, PNG. Dung lượng nhỏ hơn 1MB.</p>
      </div>
      <div class="col-md-7">
      <div class="row">
          <?php echo $this->Form->input('receiver_id5', [
              'class' => 'form-control request','disabled',
              'label' => __('Doanh nghiệp'),
              'type' => 'text',
              'id' => 'project',
              'maxlength' => 100,
              // 'default'=> 'asdfsdfsdf',
              'value' => $proj_receiv,
              'templates' => [
                  'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
        ]); ?>

      </div>

      <div class="row">
        <?php echo $this->Form->input('receiver_id4', [
            'class' => 'form-control request', 'disabled',
            'label' => __('Tới vùng'),
            'type' => 'select',
            'options' => $locations,
            'empty' => __('-- Tới vùng --'),
            'default'=> '1',
            'templates' => [
                'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
      ]); ?>
      </div>
      
        <div class="row">
        <?php echo $this->Form->input('receiver_id1', [
            'class' => 'form-control request', 'disabled',
            'label' => __('Tới Lĩnh vực'),
            'type' => 'select',
            'options' => $categories,
            'empty' => __('-- Lĩnh vực --'),
            'default'=> $request->receiver_id1,
            'templates' => [
                'inputContainer' => '<div class="col-md-10 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
        ]); ?>

        </div>
        <div class="row">
        <?php echo $this->Form->input('receiver_id2', [
            'class' => 'form-control request','disabled',
            'label' => __('Tới BCH'),
            'type' => 'select',
            'options' => $groups,
            'empty' => __('-- Ban Chấp Hành --'),
            'default'=> $request->receiver_id2,
            'templates' => [
                'inputContainer' => '<div class="col-md-10 input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
      ]); ?>

      </div>
      <div class="row">
        <?php echo $this->Form->input('receiver_id3', [
            'class' => 'form-control request','disabled',
            'label' => __('Tới vai trò'),
            'type' => 'select',
            'options' => $quantities,
            'empty' => __('-- Tới vai trò --'),
            'default'=> $request->receiver_id3,
            'templates' => [
                'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
            ]
      ]); ?>

      </div>
      </div>
      </div>
    <!-- <button type="button" id="reject"> Reject </button> -->
</div>
  <div style="margin-right: 130px">
    <?= $this->form->input('accept',array( 'type' => 'hidden' ,'value' => 0,'id'=>'accept','default'=>0)); ?>
<!-- End reply -->
    <?= $this->Form->button(__('Xác nhận, lưu chuyển'),["class"=>"btn btn-lg-6 pull-right"]) ?>

    <?= $this->Form->button( __('Từ chối'),["class"=>"btn btn-lg-6 pull-right","style"=>"margin-right:10px",'id'=>'reject'])?>
    <?= $this->Form->end() ?>
</div>

<style>
  .request{
    margin-top: 20px;
  }
  select option {
    margin: 20px;
    background: #ed6500;
    color: #ffffff;
    text-shadow: 0 1px 0 #ffffff;
}
button{
  background: #ed6500;
}

button:hover{
   background: #ef883b;
}
</style>
<script>
<?php
  echo "var admin = '{$admin}';";

?>
$(document).ready(function(){
  $('select[name="receiver_id4"]').find('option').eq(1).prop('selected', true);
  // $('select[name="receiver_id5"]').find('option').eq(1).prop('selected', true);
  if(admin == 0){
    window.location.href = "https://smeportal.org";
  }
  console.log(admin);
  if(admin == 1 || admin == 2 || admin == 3 || admin == 4){
    $(".reply").removeAttr("readonly");
    $(".reply").attr("required","true");
  }
  if(admin == 5){
    $(".replymod").removeAttr("readonly");
    $(".replymod").attr("required","true");
    $("#repattach1").attr("disabled","true");
    $("#repattach2").attr("disabled","true");

  }

});
$(document).ready(function(){
  var value = $('#accept').val();
  $('#reject').click(function(){
    $('#accept').val(1);
});
});
</script>