<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!-- <script src="../../webroot/js/highcharts.js"></script> -->
<!-- <script src="http://code.highcharts.com/highcharts.js"></script> -->
<div class="row" style="margin-left: 0; margin-right: 0">
  <h2 style="text-align: center; margin-bottom: 30px" >Các yêu cầu/kiến nghị mới</h2>
  <div class="col-md-6">
    <table class="table table-bordered">
      <tr>
        <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('title') ?></th>
        <th scope="col"><?= $this->Paginator->sort('type') ?></th>
        <th scope="col"><?= $this->Paginator->sort('project_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('status') ?></th>
        <th scope="col" class="actions"><?= __('Actions') ?></th> -->
        <th scope="col">Mã</th>
        <th scope="col">Tiêu đề</th>
        <th scope="col">Loại yêu cầu</th>
        <th scope="col">Doanh nghiệp gửi</th>
        <th scope="col">Trạng thái</th>
        <th scope="col" class="actions">Xem</th>
        <th scope="col">Duyệt</th>
      </tr>
      <?php foreach ($requests as $request): ?>
      <tr>
          <td><?= $this->Number->format($request->id) ?></td>
          <?php if(strlen($request->title) >= 50): ?>
            <td><?= substr($request->title,0,30)."..." ?></td>
          <?php else: ?>
            <td><?= h($request->title) ?></td>
          <?php endif; ?>
          <td>
            <?php if(($this->Number->format($request->type))==1): ?>
              <?php echo "Quản lý hội viên" ?>
            <?php elseif(($this->Number->format($request->type))==2): ?>
              <?php echo "Pháp lý" ?>
            <?php elseif(($this->Number->format($request->type))==3): ?>
              <?php echo "XTTM" ?>
            <?php else: ?>
              <?php echo "Vốn vay" ?>
            <?php endif; ?>
          </td>
          <td><?= $request->project['title']?></td>
          <td>
            <?php if(($this->Number->format($request->status))==0): ?>
              <?php echo "Admin chưa duyệt" ?>
            <?php elseif(($this->Number->format($request->status))==1): ?>
              <?php echo "Đang xử lý" ?>
            <?php elseif(($this->Number->format($request->status))==2): ?>
              <?php echo "Đã gửi" ?>
            <?php else: ?>
              <?php echo "" ?>
            <?php endif; ?></td>
          <td class="actions">
              <?= $this->Html->link(__('Xem'), ['action' => 'view', $request->id]) ?>
          </td>
          <td>
          <?php if($adminType == 5): ?>
            <?php if($request->status == 1): ?>
              <?= $this->Html->link(__('Duyệt'), ['action' => 'respond', $request->id]) ?>
            <?php endif;?>
          <?php else: ?>
            <?php if($request->status == 0): ?>
              <?= $this->Html->link(__('Duyệt'), ['action' => 'respond', $request->id]) ?>
            <?php elseif($request->status == 2): ?>

            <?php endif; ?>
          <?php endif; ?>
          </td>
      </tr>
      <?php endforeach; ?>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Đầu')) ?>
            <?= $this->Paginator->prev('< ' . __('Trước')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Sau') . ' >') ?>
            <?= $this->Paginator->last(__('Cuối') . ' >>') ?>
        </ul>
    </div>
  </div>
  <div class="col-md-6 pull-left" style="margin-top: 0;">
    <div id="container" style="width:100%; height:300px;"></div>
  </div>
</div>
<div class="row" style="margin-left: 20px;">
    <div class="col-md-11">
      <h3>Bộ lọc</h3>
      <div class="row">
        <?= $this->Form->create(null,['type'=>'get']) ?>

          <?php echo $this->Form->input('group', [
              'class' => 'form-control',
              'label' => __('BCH'),
              'type' => 'select',
              'options' => $groups,
              'empty' => __('-- Chọn BCH --'),
              'templates' => [
                  'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>
          <?php echo $this->Form->input('status', [
              'class' => 'form-control',
              'label' => __('Tình trạng'),
              'type' => 'select',
              'options' => [1=>'Chưa duyệt',2=>'Đang xử lý',3=>'Đã gửi'],
              'empty' => __('-- Chọn tình trạng --'),
              'templates' => [
                  'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>
          <?php if($adminType == 5): ?>
            <?php echo $this->Form->input('type1', [
                'class' => 'form-control',
                'label' => __('Kiểu yêu cầu'),
                'type' => 'select',
                'options' => [1=>'Pháp lý',2=>'Vốn vay',3=>'Xúc tiến',4=>'Quản lý hội viên'],
                'empty' => __('-- Chọn kiểu yêu cầu --'),
                'templates' => [
                    'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
                ]
            ]); ?>
          <?php else: ?>
          <?php endif; ?>

            <?php echo $this->Form->input('location', [
              'class' => 'form-control',
              'label' => __('Khu vực'),
              'type' => 'select',
              'options' => $locations,
              'empty' => __('-- Chọn khu vực --'),
              'templates' => [
                  'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>
          <?php echo $this->Form->input('category', [
              'class' => 'form-control',
              'label' => __('Lĩnh vực'),
              'type' => 'select',
              'options' => $categories,
              'empty' => __('-- Chọn lĩnh vực --'),
              'templates' => [
                  'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>
          <?php echo $this->Form->input('role', [
              'class' => 'form-control',
              'label' => __('Vai trò'),
              'type' => 'select',
              'options' => $usersType,
              'empty' => __('-- Chọn vai trò --'),
              'templates' => [
                  'inputContainer' => '<div class="col-md-2 input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>

      </div>
      <div class="row">
          <?php echo $this->Form->input('projectReceive', [
              'class' => 'form-control request projectFilter',
              'label' => __('Doanh nghiệp nhận'),
              'type' => 'text',
              'maxlength' => 100,
              'templates' => [
                  'inputContainer' => '<div class="col-md-2  input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>

          <?php echo $this->Form->input('projectSend', [
              'class' => 'form-control request projectFilter',
              'label' => __('Doanh nghiệp gửi'),
              'type' => 'text',
              'maxlength' => 100,
              'templates' => [
                  'inputContainer' => '<div class="col-md-2  input padd-left {{type}}{{required}}">{{content}}</div>'
              ]
          ]); ?>

          <button type="submit" id="btn-submit" status="0" class="btn btn-warning btn-launch btn-custom pull-right" style="color: #080707;">Lọc dữ liệu</button>
      </div>
      <div class="row">
        
        <!-- <?= $this->Form->button(__('Lọc'),["class"=>"btn btn-lg-6 pull-right","id"=>"confirm"]) ?> -->
      </div>
      
      <?= $this->Form->end() ?>
    </div>
</div>

<?php if($adminType == 5): ?>
  <script type="text/javascript">
  Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Thống kê'
    },
    xAxis: {
        categories: ['Quản lý HV', 'Pháp lý', 'XTTM', 'Vốn vay']
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
            }
        }
    },
    series: [{
        name: 'Chưa xử lý',
        data: [<?= $manage ?>, <?= $juridical ?>, <?= $commerce ?>, <?= $loans ?>]
    }, {
        name: 'Đang xử lý',
        data: [<?= $manageProcessing ?>, <?= $juridicalProcessing ?>, <?= $commerceProcessing ?>, <?= $loansProcessing ?>]
    }, {
        name: 'Đã xử lý',
        data: [<?= $manageProcessed ?>, <?= $juridicalProcessed ?>, <?= $commerceProcessed ?>, <?= $loansProcessed ?>]
    }]
  });
  </script>
<?php endif; ?>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

    $(document).ready(function () {
        var availableTags = [];
        $.ajax({
            type: "POST",
            url: "../api/projects/getlistproject.json",
            dataType: 'json',
            success: function (item) {
                var item = item['data'];
                for (var i = 0; i < item.length; i++) {
                    availableTags.push(item[i].title);
                }
            }
        });
        $(".projectFilter").autocomplete({
            source: availableTags
        });


    });

</script>
