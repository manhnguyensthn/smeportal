<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>
    .request {
        margin-bottom: 10px;
    }

    .form-control{
        margin-bottom: 25px;
    }
    .star_require{
        color:red;
    }
</style><!--
<script src="jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="jquery-ui.css">-->
<div class="requests form col-lg-12">
    <?= $this->Form->create($request, array('type' => 'file','onsubmit'=>'return disableForm()')) ?>
    <div class="col-lg-6">
        <fieldset>
            <h3 style="margin-left: 100px"><?= __('Doanh nghiệp gửi yêu cầu/kiến nghị') ?></h3>
            <div class="input text">

                <label for="title">Tiêu đề yêu cầu <span class="star_require">*</span></label>
                <input type="text" name="title" class="form-control request" style="margin-top: -5px" maxlength="100" required="required" id="title">
<!--                --><?php //echo $this->Form->input('type', [
//                    'class' => 'form-control request',
//                    'style' => ['margin-top:-5px'],
//                    'label' => 'Kiểu yêu cầu *',
//                    'type' => 'select',
//                    'options' => ['Quản lý hội viên', 'Pháp lý', 'Xúc tiến', 'Vốn vay'],
//                    'empty' => __('-- Chọn kiểu yêu cầu --'),
//                    'templates' => [
//                        'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
//                    ]
//                ]); ?>
                <div class="col-md-6 input padd-left select require">
                    <label for="type">Kiểu yêu cầu <span class="star_require">*</span></label>
                    <select name="type" class="form-control request" style="margin-top: -5px" required="required" id="type">
                        <option value="">--Chọn kiểu yêu cầu--</option>
                        <option value="0">Quản lý hội viên</option>
                        <option value="1">Pháp lý</option>
                        <option value="2">Xúc tiến</option>
                        <option value="3">Vốn vay</option>
                    </select>
                </div>
<!--                --><?php //echo $this->Form->input('project_id', [
//                    'required',
//                    'class' => 'form-control request',
//                    'style' => ['margin-top:-5px'],
//                    'label' => __('Từ doanh nghiệp *'),
//                    'type' => 'select',
//                    'options' => $projects,
//                    'empty' => __('-- Chọn doanh nghiệp --'),
//                    'templates' => [
//                        'inputContainer' => '<div class="col-md-6 input padd-left {{type}}{{required}}">{{content}}</div>'
//                    ]
//                ]); ?>
                <div class="col-md-6 input padd-left select require">
                    <label for="type">Từ doanh nghiệp <span class="star_require">*</span></label>
                    <select name="project_id" class="form-control request" style="margin-top: -5px" required="required" id="project_id">
                        <?php foreach ($projects as $project): ?>
                            <option value="<?php echo $project['id'] ?>"><?php echo $project['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="input text">

            </div>
        </fieldset>

        <fieldset style="margin-top: 20px">
            <div class="input texarea">
                <?php
                echo '<label for="content">' . __('Nội dung') . '</label>';
                echo $this->Form->textarea('content', array('maxlength' => 500,
                    'style' => ['margin-top:-5px'], 'class' => 'form-control request', 'rows' => 7, 'label' => __('Nội dung')));
                ?>
            </div>

        </fieldset>
        <!-- End request -->

        <!-- Begin reply -->
        <fieldset class="request">
            <legend><?= __('Phản hồi yêu cầu/kiến nghị') ?></legend>
            <div class="input text">
                <label for="">Được duyệt bởi: </label>
                <!-- <label>Admin 1</label> -->
            </div>
        </fieldset>
        <fieldset>
            <div class="input texarea ">
                <?php
                echo '<label for="repcontent1">' . __('Nội dung') . '</label>';
                echo $this->Form->textarea('repcontent1', array('maxlength' => 500, 'class' => 'form-control reply','placeholder'=>'Hồi đáp của Admin', 'id' => 'reply', 'readonly', 'rows' => 7, 'label' => __('Nội dung phản hồi')));
                ?>
            </div>
        </fieldset>
        <fieldset>
            <div class="input texarea">
                <?php
                echo '<label for="repcontent2">' . __('') . '</label>';
                echo $this->Form->textarea('repcontent2', array('maxlength' => 500, 'class' => 'form-control reply','placeholder'=>'Hồi đáp của Mod', 'readonly', 'rows' => 7, 'label' => __('Nội dung phản hồi')));
                ?>
            </div>
        </fieldset>
        <!-- End reply -->

    </div>
    <div class="col-lg-6" style="">
        <h3 style="margin-left: 100px"><?= __('Doanh nghiệp nhận yêu cầu/kiến nghị') ?></h3>
        <div class="col-lg-4" style="margin-top: 230px">
            <?= $this->form->input('attach1', ['type' => 'file', 'label' => 'Tài liệu đính kèm 1','id'=>'FileUpload_1']) ?>
            <?= $this->form->input('attach2', ['type' => 'file', 'label' => 'Tài liệu đính kèm 2','id'=>'FileUpload_2']) ?><br>
            <p class="text-danger">Chấp nhận các định dạng PDF, DOCX, DOC, JPEG, PNG. Dung lượng nhỏ hơn 1MB.</p>
        </div>

        <div class="col-lg-7 pull-right">
            <small style="color: red"><i>Chọn ít nhất một trong những lựa chọn dưới *</i></small>
            <p></p>
            <div class="row">
                <?php echo $this->Form->input('receiver_id5', [
                    'class' => 'form-control request',
                    'style' => 'margin-top:-5px',
                    'label' => __('Tới doanh nghiệp'),
                    'type' => 'text',
                    'id' => 'project',
                    'maxlength' => 100,
                    'templates' => [
                        'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]); ?>
            </div>
            <div class="row">
                <?php echo $this->Form->input('receiver_id4', [
                    'class' => 'form-control request',
                    'label' => __('Tới vùng'),
                    'style' => 'margin-top:-5px',
                    'type' => 'select',
                    'options' => $locations,
                    'empty' => __('-- Vùng --'),
                    'templates' => [
                        'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]); ?>
            </div>
            <div class="row">
                <?php
                echo $this->Form->input('receiver_id1', [
                    'class' => 'form-control request',
                    'label' => __('Tới lĩnh vực'),
                    'style' => 'margin-top:-5px',
                    'type' => 'select',
                    'options' => $categories,
                    'empty' => __('-- Lĩnh vực --'),
                    'templates' => [
                        'inputContainer' => '<div class="col-md-10  input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]); ?>
            </div>
            <div class="row">
                <?php echo $this->Form->input('receiver_id2', [
                    'class' => 'form-control request',
                    'label' => __('Tới ban chấp hành'),
                    'style' => 'margin-top:-5px',
                    'type' => 'select',
                    'options' => $scales,
                    'empty' => __('-- Vị trí --'),
                    'templates' => [
                        'inputContainer' => '<div class="col-md-10 input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]); ?>

            </div>
            <div class="row">
                <?php echo $this->Form->input('receiver_id3', [
                    'class' => 'form-control request',
                    'label' => __('Tới vai trò'),
                    'style' => 'margin-top:-5px',
                    'type' => 'select',
                    'options' => $quantities,
                    'empty' => __('-- Vai trò --'),
                    'templates' => [
                        'inputContainer' => '<div class="col-md-10 input padd-left {{type}}{{required}}">{{content}}</div>'
                    ]
                ]); ?>

            </div>
        </div>
    </div>
    <!-- Begin request -->

</div>
<div class="button" style="margin-top: 5px">
    <?= $this->Form->button(__('Xác nhận, lưu chuyển'), ["class" => "btn btn-lg-6 pull-right", "id" => "confirm"]) ?>
    <?= $this->Form->end() ?>
</div>
<div id="dialog" title="Alert message" style="display: none; border-radius: 5px">
    <div class="ui-dialog-content ui-widget-content">
        <p>
            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0"></span>
            <label id="lblMessage">
            </label>
        </p>
    </div>
</div>
<style>
    .ui-widget-content{
        border: none;
    }
</style>
<script type="text/javascript">
    function disableForm(){
        $('#confirm').prop('disabled', true);
    }
    $('#FileUpload_1').bind('change', function() {
        var file1_size = this.files[0].size/(1024*1024);
        var file1_extension = this.files[0].name.substr(this.files[0].name.lastIndexOf('.'));
        var control = $("#FileUpload_1");
        var file_type = ['.jpg','.pdf','.docx','.doc','.jpeg','.png','.xls','.xlsx','csv'];
        var checkFileExtension = 2;
        for(i = 0; i < file_type.length;i++)
        {
            if(file_type[i] === file1_extension) {
                checkFileExtension = 1;
                break;
            } else {
                checkFileExtension = 0;
            }
        }
        var control = $("#FileUpload_1");

        if(file1_size > 1 || checkFileExtension == 0){
            var message = "Vui lòng chọn tập tin có dung lượng nhỏ hơn 4MB và có định dạng PDF | DOC | DOCX | JPEG | PNG | XLS | CSV !"
            ShowDialogBox("Cảnh báo",message,"OK","","");
            $("#FileUpload_1").val("");
        }
        //this.files[0].size gets the size of your file.\

    });
    $('#FileUpload_2').bind('change', function() {
        var file2_size = this.files[0].size/(1024*1024);
        var file2_extension = this.files[0].name.substr(this.files[0].name.lastIndexOf('.'));
        var control = $("#FileUpload_2");
        var file_type = ['.jpg','.pdf','.docx','.doc','.jpeg','.png','.xls','.xlsx','csv'];
        var checkFileExtension = 2;
        for(i = 0; i < file_type.length;i++)
        {
            if(file_type[i] === file2_extension) {
                checkFileExtension = 1;
                break;
            } else {
                checkFileExtension = 0;
            }
        }
        var control = $("#FileUpload_2");

        if(file2_size > 1 || checkFileExtension == 0){
            var message = "Vui lòng chọn tập tin có dung lượng nhỏ hơn 4MB và có định dạng PDF | DOC | DOCX | JPEG | PNG | XLS | CSV !"
            ShowDialogBox("Cảnh báo",message,"OK","","");
            $("#FileUpload_2").val("");
        }
        //this.files[0].size gets the size of your file.\

    });
    function ShowDialogBox(title,content, btn1text, btn2text,functionText, parameterList) {
        var btn1css;
        var btn2css;
        if (btn1text == '') {
            btn1css = "hidecss";
        } else {
            btn1css = "showcss btn btn-launch";
        }
        $("#lblMessage").html(content);

        $("#dialog").dialog({
            resizable: false,
            title: title,
            modal: true,
            width: '600px',
            height: 'auto',
            bgiframe: false,
            hide: {effect: 'scale', duration: 400},

            buttons: [
                {
                    text: btn1text,
                    "class": btn1css,
                    click: function () {

                        $("#dialog").dialog('close');

                    }
                }
            ]
        });
    }
</script>

<style>
    .request {
        margin-top: 20px;
    }

    select option {
        margin: 20px;
        background: #ed6500;
        color: #ffffff;
        text-shadow: 0 1px 0 #ffffff;
    }

    button {
        background: #ed6500;
    }

    button:hover {
        background: #ef883b;
    }
    .showcss{
        display:block;
        width: 130px;
        margin-top: -40px;

    }
    .hidecss{ display:none;}
    #dialog{
        height: 120px;
    }
    .ui-dialog{
        border: 10px solid #ed6500;
        border-radius: 10px;
    }
    .ui-dialog-titlebar{
        border-radius: 10px;
    }
    .ui-dialog-titlebar-close{
        display: none;
    }
</style>
<script>
    <?php
    echo "var admin = '{$admin}';";

    ?>
//    $("#confirm").click(function (e) {
//        // alert('hello');
//
//        $('#confirm').attr("disabled", "disabled");
//    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

    $(document).ready(function () {
// document.getElementById("project-id").setCustomValidity("Vui lòng chọn một lựa chọn");
// document.getElementById("type").setCustomValidity("Vui lòng chọn một lựa chọn");
        var availableTags = [];
        $.ajax({
            type: "POST",
            url: "../api/projects/getlistproject.json",
            dataType: 'json',
            success: function (item) {
                var item = item['data'];
                // console.log(item);
                // var item1 = JSON.parse(item);

                for (var i = 0; i < item.length; i++) {
                    // console.log(item1[i].title);
                    availableTags.push(item[i].title);
                }
            }
        });
// console.log(availableTags);
        $("#project").autocomplete({
            source: availableTags
        });


    });

</script>
