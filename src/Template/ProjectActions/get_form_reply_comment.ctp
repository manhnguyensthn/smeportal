<div class="row box-author-comment-info">
    <div class="col-md-4 avatar-member"><img src="<?php echo !empty($user['avatar']) ? $user['avatar'] : '/img/avatars/avatar_default.jpg'; ?>" class="media-object"  title="<?php echo $user['first_name'].' '.$user['last_name']; ?>" alt="<?php echo $user['first_name'].' '.$user['last_name']; ?>" /></div>
    <div class="col-md-8 author-comment-name">
        <strong><?php echo $user['first_name'].' '.$user['last_name']; ?></strong>
    </div>
</div>
<div class="row box-form-alert"></div>
<div class="row comment-form">
    <div class="col-xs-12">
        <textarea class="form-control" name="comment" id="comment-id-<?php echo isset($comment_id) ? $comment_id : 0; ?>"></textarea>
    </div>
    <div class="col-xs-12 text-right"><a class="btn btn-warning btn-launch" href="javascript:post_comment_to_system('<?php echo isset($project_id) ? $project_id : 0; ?>','<?php echo isset($comment_id) ? $comment_id : 0; ?>');"><?php echo __('Post'); ?></a></div>
</div>