<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= isset($title) ? $title : $this->fetch("title") ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css(['bootstrap.min', 'font-awesome.min', 'flexslider','reset' ,'style']) ?>
        <?= $this->Html->script(['jquery.min', 'bootstrap-min','jquery.flexslider','app']) ?>
        <style>
            .project-thumbnail img{width: 100%;margin-top: 25px;}
            .project-info a{color: black}
            .project-info a:hover{text-decoration: none;}
            .project-author-info span{margin-right: 20px;}
            .project-author-info span i{margin-right: 5px}
        </style>
    </head>
    <?php $uri = \Cake\Utility\Inflector::slug($project['title']); ?>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3 project-thumbnail">
                    <a href="<?php echo ROOT_URL.$uri; ?>"><img src="<?php echo !empty($project['image_url']) ? $project['image_url'] : ROOT_URL.'img/logo.png'; ?>" title="<?php echo $project['title']; ?>" alt="<?php echo $project['title']; ?>" /></a>
                </div>
                <div class="col-xs-9 project-info">
                    <a href="<?php echo ROOT_URL.$uri; ?>" style="color: black"><h3><?php echo $project['title']; ?></h3></a>
                    <p class="project-author-info">
                        <?php if (!empty($category)): ?>
                        <span ><i class="fa fa-folder-open" ></i><?php echo $category['name']; ?></span>
                        <?php else: ?>
                        <span ><i class="fa fa-folder-open" ></i><?php echo __('Feature'); ?></span>
                        <?php endif; ?>
                        <?php if (!empty($state) || !empty($country)): ?>
                        <span ><i class="fa fa-map-marker"></i><?php echo isset($state['name']) ? $state['name'].', ' : ''; ?><?php echo !empty($country['country_name']) ? $country['country_name'] : ''; ?></span>
                        <?php endif; ?>
                        <span><i class="fa fa-user"></i><?php echo isset($project['user']['first_name']) ? $project['user']['first_name'] . ' ' . $project['user']['last_name'] : ''; ?></span>
                    </p>
                    <p class="project-summary"><?php echo isset($project['description']) ? $project['description'] : ''; ?></p>
                </div>
            </div>
        </div>
    </body>
</html>
