<?php
$notificationCotroller = new App\Controller\NotificationsController;
$userController = new \App\Controller\UsersController();
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 header-box">
            <h1><?php echo __('Your Notifications'); ?></h1>
        </div>
        <div class="col-md-12" id="box-notifications-content">
            <input name="current_page" type="hidden" id="current-page" value="1" />
            <ul class="list-notifications">
                <?php
                foreach ($listNotifications as $notification):
                    $userAction = $userController->getUserInfoById($notification->action_user_id);
                    $class = ($notification->read_flg == 0) ? ' not-read' : '';
                    $timeAgo = $this->AuthUser->_get_time_ago($notification->created);
                    $javascriptAction = ($notification->action_type == 1 && $notification->read_flg == 0) ? 'onclick="process_accept_join_project(' . $notification->id . ');"' : 'onclick="process_read_notification(' . $notification->id . ');"';
                    if ($notification->read_flg == 0) {
                        $javascriptAction = "process_notifications('" . $notification->id . "');";
                    } else {
                        $javascriptAction = 'javascript:void();';
                    }
                    ?>
                    <li class="notification-item<?php echo $class; ?>" onclick="<?php echo $javascriptAction; ?>" >
                        <p class="notification-title"><?php echo $notificationCotroller->getTypeNotificationContent($userAction['first_name'] . ' ' . $userAction['last_name'], $notification->action_type, $notification->option_data,$notification->role_title); ?></p>
                        <p class="notification-time"><?php echo $timeAgo; ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php if (count($listNotifications) == 12): ?>
            <div class="col-md-12 text-center">
                <button class="btn btn-launch see-more-button" onclick="load_more_content_by_page('/notifications/loadNotificationByPage', '.list-notifications');"><?php echo __('See more'); ?></button>
            </div>
        <?php endif; ?>
    </div>
</div>