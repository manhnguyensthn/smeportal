<?php
$notificationCotroller = new App\Controller\NotificationsController;
$userController = new \App\Controller\UsersController();
?>
<?php if (count($listNotifications) > 0): ?>
    <?php
    foreach ($listNotifications as $notification):
        $userAction = $userController->getUserInfoById($notification->action_user_id);
        $class = ($notification->read_flg == 0) ? ' not-read' : '';
        $timeAgo = $this->AuthUser->_get_time_ago($notification->created);
        $javascriptAction = ($notification->action_type == 1 && $notification->read_flg == 0) ? 'onclick="process_accept_join_project(' . $notification->id . ');"' : 'onclick="process_read_notification(' . $notification->id . ');"';
        if ($notification->read_flg == 1) {
            $javascriptAction = '';
        }
        ?>
        <li class="notification-item<?php echo $class; ?>">
            <p class="notification-title"><?php echo $notificationCotroller->getTypeNotificationContent($userAction['first_name'] . ' ' . $userAction['last_name'], $notification->action_type, $notification->option_data, $notification->role_title); ?></p>
            <p class="notification-time"><?php echo $timeAgo; ?></p>
        </li>
    <?php endforeach; ?>
<?php endif; ?>