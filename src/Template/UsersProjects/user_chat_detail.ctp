<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
    .we-footer{
        margin-top: 0;
    }
</style>

<div class="metting page_messages">
    <?php echo $this->element('Mettings/title_metting'); ?>
    <?php echo $this->element('Mettings/tab_metting'); ?>
</div>
<div class="container">
    <div class="wrapper_chat clearfix">
        <div class="list-chat">
            <div class="title"><?php echo __('Group Chats'); ?></div>
            <ul class="template-list-chat group clearfix">
                <?php foreach ($listRooms as $room): ?>
                        <li class="item">
                            <a href="#" class="clearfix">
                                <div class="avata avata_group">
                                    <div class="avatar-item">
                                        <?php foreach ($room->users as $row): ?>
                                            <span><img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->member_name; ?>" title="<?php echo $row->member_name; ?>" /></span>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="content_item">
                                    <div class="name"><?php echo $room->room_name; ?></div>
                                </div>
                            </a>
                        </li>
                <?php endforeach; ?>
            </ul>
            <div class="title"><?php echo __('Contacts'); ?></div>
            <ul class="template-list-chat clearfix">
                <?php foreach ($contacts as $row): ?>
                        <li class="item is_online">
                            <a href="#">
                                <div class="avata avata_group">
                                    <?php if (!empty($row->number_not_read)): ?>
                                        <span class="number_message"><?php echo $row->number_not_read; ?></span>
                                    <?php endif; ?>
                                    <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                    <div class="avatar-item">
                                        <img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                                    </div>
                                </div>
                                <div class="content_item">
                                    <div class="name"><?php echo $row->name; ?></div>
                                    <div class="mgs"><?php echo $this->AuthUser->_limit_by_length_string($row->member_status,50); ?></div>
                                </div>
                            </a>
                        </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="template_messages">
            <div class="messages_header">
                <div class="info_message is_online">
                    <div class="avata avata_group">
                        <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                        <div class="avatar-item">
                            <img src="/img/john_avatar.jpg" alt="John" title="John" />
                        </div>
                    </div>
                    <div class="content_item">
                        <div class="name">Mike Lafuente</div>
                        <div class="mgs">Sometimes even when you brake you can’t Sometimes even when you brake you can’t ...</div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="add_to_group">
                    <div class="wrapper_group dropdown">
                        <a href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="horizontal"></span>
                            <span class="vertical"></span>
                        </a>

                        <div class="wrapper_to_group dropdown-menu" aria-labelledby="dLabel">
                            <form action="javascript:void(0);" method="POST">
                                <div class="user_accept_form">
                                    <div class="list_user_accept" contenteditable>
                                        <span>Nancy Wheeler</span>
                                        <span>Mike Lafuente</span>
                                        <span>Nancy</span>
                                        <span>Nancy Wheeler</span>
                                        <span>Mike Lafuente</span>
                                    </div>
                                </div>
                                <div class="list_contacts">
                                    <div class="title">Contacts</div>
                                    <ul class="template-list-chat clearfix">
                                        <?php for ($i = 0; $i < 10; $i++): ?>
                                            <li class="item is_online is_active">
                                                <a href="#">
                                                    <div class="avata avata_group">
                                                        <span class="number_message">3</span>
                                                        <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                                        <div class="avatar-item">
                                                            <img src="/img/john_avatar.jpg" alt="John" title="John" />
                                                        </div>
                                                    </div>
                                                    <div class="content_item">
                                                        <div class="name">Mike Lafuente</div>
                                                        <div class="mgs">Sometimes even when you brake you can’t Sometimes even when you brake you can’t ...</div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php endfor; ?>
                                    </ul>
                                </div>
                                <div class="button_create">
                                    <button type="submit">Create group</button>
                                    <button>Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="messages_body">
                <div class="wrapper_messages_body">
                    <ul class="clearfix template_list_message">
                        <?php for ($i = 0; $i < 3; $i++): ?>
                            <li class="item">
                                <div class="avata"><img src="/img/john_avatar.jpg" alt="John" title="John"></div>
                                <div class="mesage_text">
                                    <div class="wrap_text">
                                        <div class="text">Well, the way they make shows is, they make one show.</div>
                                        <div class="time_is_view">
                                            <span>1:23 PM</span>
                                            <span class="is_view"><img src="/img/deliver_icon.png" class="icon-read"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item is_curent">
                                <div class="avata"><img src="/img/john_avatar.jpg" alt="John" title="John"></div>
                                <div class="mesage_text">
                                    <div class="wrap_text">
                                        <div class="text">Well, the way they make shows is, they make one show.</div>
                                        <div class="time_is_view">
                                            <span>1:23 PM</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="line"><span>56min ago</span></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>

            <div class="messages_footer">
                <div class="wrapper_messages_footer">
                    <input type="text" name="chat_content" autofocus class="ip-chat" value=""/>
                    <button class="btn btn-warning">SEND</button>
                </div>
            </div>
        </div>
    </div>
</div>