<?php
$currentDate = count($listConversations) ? date('Y-m-d', strtotime($listConversations[0]->created)) : date('Y-m-d');
foreach ($listConversations as $row):
    ?>
    <?php if ($current_user['id'] != $row->user->id): ?>
        <li class="item">
            <div class="avata"><img src="<?php echo $row->user->avatar; ?>" alt="<?php echo $row->user->name; ?>" title="<?php echo $row->user->name; ?>"></div>
            <div class="mesage_text">
                <div class="wrap_text">
                    <div class="text"><?php echo $row->message; ?></div>
                    <div class="time_is_view">
                        <span><?php echo date('g:i A', strtotime($row->created)); ?></span>
                        <span class="is_view"><img src="/img/deliver_icon.png" class="icon-read"></span>
                    </div>
                </div>
            </div>
        </li>
    <?php else: ?>
        <li class="item is_curent">
            <div class="avata"><img src="<?php echo $row->user->avatar; ?>" alt="<?php echo $row->user->name; ?>" title="<?php echo $row->user->name; ?>"></div>
            <div class="mesage_text">
                <div class="wrap_text">
                    <div class="text"><?php echo $row->message; ?></div>
                    <div class="time_is_view">
                        <span><?php echo date('g:i A', strtotime($row->created)); ?></span>
                    </div>
                </div>
            </div>
        </li>
    <?php endif; ?>
    <?php if (date('Y-m-d', strtotime($row->created)) != $currentDate): ?>
        <li class="line"><span><?php echo $this->AuthUser->_get_time_ago($currentDate); ?></span></li>
        <?php $currentDate = date('Y-m-d', strtotime($row->created)); ?>
    <?php endif; ?>
<?php endforeach; ?>