<?php foreach ($contacts as $user): ?>
<li id="member-<?php echo $user->id; ?>" class="item is_online is_active" onclick="select_member_add_to_group('<?php echo $user->id; ?>')" data-id="<?php echo $user->id; ?>" data-selected="0" data-name="<?php echo $user->name; ?>" >
        <a href="javascript:void(0);"  >
            <div class="avata avata_group">
                <?php if (!empty($user->number_not_read)): ?>
                    <span class="number_message"><?php echo $user->number_not_read; ?></span>
                <?php endif; ?>
                <?php if ($user->chat_status == 1): ?>
                    <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                <?php else: ?>
                    <span class="status not-online"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                <?php endif; ?>
                <div class="avatar-item">
                    <img src="<?php echo $user->avatar; ?>" alt="<?php echo $user->name; ?>" title="<?php echo $user->name; ?>" />
                </div>
            </div>
            <div class="content_item">
                <div class="name"><?php echo $user->name; ?></div>
                <div class="mgs"><?php echo $user->member_status; ?></div>
            </div>
        </a>
    </li>
<?php endforeach; ?>