<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users Project'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersProjects index large-9 medium-8 columns content">
    <h3><?= __('Users Projects') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('project_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersProjects as $usersProject): ?>
            <tr>
                <td><?= $usersProject->has('user') ? $this->Html->link($usersProject->user->name, ['controller' => 'Users', 'action' => 'view', $usersProject->user->id]) : '' ?></td>
                <td><?= $usersProject->has('project') ? $this->Html->link($usersProject->project->title, ['controller' => 'Projects', 'action' => 'view', $usersProject->project->id]) : '' ?></td>
                <td><?= h($usersProject->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersProject->user_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersProject->user_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersProject->user_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersProject->user_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
