<style>
    .we-footer{
        margin-top: 0;
    }
</style>
<div class="metting page_messages">
    <?php echo $this->element('Mettings/title_metting'); ?>
    <?php echo $this->element('Mettings/tab_metting'); ?>
</div>
<div class="chat-group"></div>
<div class="container">
    <div class="wrapper_chat clearfix">
        <div class="list-chat">
            <div class="title"><?php echo __('Group Chats'); ?></div>
            <ul class="template-list-chat group clearfix">
                <?php foreach ($listRooms as $room): ?>
                    <li class="item">
                        <a href="/chat-room/<?php echo $Project->id; ?>/<?php echo $room->room_id; ?>" class="clearfix">
                            <div class="avata avata_group">
                                <div class="avatar-item">
                                    <?php foreach ($room->users as $index => $row): ?>
                                        <?php if ($index < 3): ?>
                                            <span><img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->member_name; ?>" title="<?php echo $row->member_name; ?>" /></span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="content_item">
                                <div class="name"><?php echo $room->room_name; ?></div>
                            </div>
                        </a>
                        <span class="btn-delete-group" onclick="delete_group_chat('<?php echo $room->room_id; ?>');" >x</span>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="title"><?php echo __('Contacts'); ?></div>
            <ul class="template-list-chat clearfix"> 
                <?php foreach ($contacts as $row): ?>
                    <li class="item is_online">
                     
                            <div class="avata avata_group">
                                <?php if (!empty($row->number_not_read)): ?>
                                    <span class="number_message"><?php echo $row->number_not_read; ?></span>
                                <?php endif; ?>
                                <?php if ($row->chat_status == 1): ?>
                                    <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                <?php else: ?>
                                    <span class="status not-online"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                <?php endif; ?>
								<a href="/applicant-profile-<?php echo $row->id; ?>">
                                <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                    <div class="avatar-item">
                                        <img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                                    </div>
								</a>
                            </div>
							   <a href="/usersprojects/userChatDetail/<?php echo $Project->id; ?>/<?php echo $row->id; ?>">
                            <div class="content_item">
                                <div class="name"><?php echo $row->name; ?></div>
                                <div class="mgs"><?php echo $this->AuthUser->_limit_by_length_string($row->member_status, 50); ?></div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="template_messages">
            <?php if (!empty($roomId)): ?>
                <div class="messages_header">
                    <div class="info_message is_online">
                        <?php if (!empty($roomInfo)): ?>
                            <?php if ($roomInfo->room_type == 1): ?>
                                <div class="avata">
                                    <?php if ($roomInfo->chat_status == 1): ?>
                                        <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                    <?php endif; ?>
										<a href = "/applicant-profile-<?php echo $roomInfo->users[1]->member_id; ?>">                            
                                    <span class="status"><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                    <div class="avatar-item">
                                        <img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>" />
                                    </div>
								   </a>
                                </div>
                            <?php else: ?>
                                <div class="avata avata_group">
                                    <div class="avatar-item">
                                        <?php foreach ($roomInfo->users as $index => $row): ?>
                                            <?php if ($index < 3): ?>
                                                <span><img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->member_name; ?>" title="<?php echo $row->member_name; ?>" /></span>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="content_item">
                                <a href = "/applicant-profile-<?php echo $roomInfo->users[1]->member_id; ?>"><div class="name"><?php echo substr($roomInfo->room_name,0,strlen ( $roomInfo->room_name )-1); ?></div></a>
                                <div class="mgs"><?php echo $roomInfo->room_status; ?></div>
                            </div>
                            <div class="clear"></div>
                        <?php endif; ?>
                    </div>

                    <div class="add_to_group">
                        <div class="wrapper_group dropdown">
                            <a href="javascript:void(0);" id="btn-add-group" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="horizontal"></span>
                                <span class="vertical"></span>
                            </a>
                            <div class="wrapper_to_group dropdown-menu" id="box-add-group" aria-labelledby="dLabel">
                                <div class="user_accept_form">
                                    <div class="list_user_accept">
                                    </div>
                                </div>
                                <div class="list_contacts">
                                    <div class="title"><?php echo __('Contacts'); ?></div>
                                    <div class="row" id="content-loading" style="margin-left: 0px; margin-right: 0px">
                                    </div>
                                    <ul class="template-list-chat clearfix">

                                    </ul>
                                </div>
                                <div class="button_create">
                                    <button id="btn-creat-group" onclick="add_member_to_group('<?php echo isset($roomId) ? $roomId : 0; ?>', '<?php echo isset($Project->id) ? $Project->id : 0; ?>');" data-active="0" ><?php echo __('Create group'); ?></button>
                                    <button onclick="cancel_dropdown();"><?php echo __('Cancel'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="messages_body"  id="list-messages">
                    <div class="wrapper_messages_body">
                        <div class="row" id="box-old-message-loading">
                        </div>
                       
                            <ul class="clearfix template_list_message" id = "template_list_message">
							 <?php if (!empty($listConversations)) : ?>
                                <li class="line" id="line-see-old"><a href="javascript:get_old_message('<?php echo isset($roomId) ? $roomId : 0; ?>');" data-page="0" ><?php echo __('See old messages'); ?></a></li>
                                <?php
                                $currentDate = count($listConversations) ? date('Y-m-d', strtotime($listConversations[0]->created)) : date('Y-m-d');
                                foreach ($listConversations as $row):
                                    ?>
                                    <?php if (date('Y-m-d', strtotime($row->created)) != $currentDate): ?>
                                        <li class="line"><span><?php echo $this->AuthUser->_get_time_ago($currentDate); ?></span></li>
                                        <?php $currentDate = date('Y-m-d', strtotime($row->created)); ?>
                                    <?php endif; ?>
                                    <?php if ($current_user['id'] != $row->user->id): ?>
                                        <li class="item">
                                            <div class="avata"><img src="<?php echo $row->user->avatar; ?>" alt="<?php echo $row->user->name; ?>" title="<?php echo $row->user->name; ?>"></div>
                                            <div class="mesage_text">
                                                <div class="wrap_text">
                                                    <div class="text"><?php echo $row->message; ?></div>
                                                    <div class="time_is_view">
                                                        <span><?php echo date('g:i A', strtotime($row->created)); ?></span>
                                                        <span class="is_view"><img src="/img/deliver_icon.png" class="icon-read"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php else: ?>
                                        <li class="item is_curent">
                                            <div class="avata"><img src="<?php echo $row->user->avatar; ?>" alt="<?php echo $row->user->name; ?>" title="<?php echo $row->user->name; ?>"></div>
                                            <div class="mesage_text">
                                                <div class="wrap_text">
                                                    <div class="text"><?php echo $row->message; ?></div>
                                                    <div class="time_is_view">
                                                        <span><?php echo date('g:i A', strtotime($row->created)); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endif; ?>

                                <?php endforeach; ?>
								   <?php endif; ?>
                            </ul>
                     
                    </div>
                </div>

                <div class="messages_footer">
                    <div class="wrapper_messages_footer">
                        <form name="chat_form" method="POST" action="" >
                            <input type="text" name="chat_content" autofocus class="ip-chat" value=""/>
                            <button type="submit" onclick = "return send_chat_message('<?php echo $roomId; ?>');"  class="btn btn-warning"><?php echo strtoupper(__('Send')); ?></button>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="check_chat" style = "display:none">
<div class="modal fade alert-box alert in" tabindex="-1" role="dialog" style="display: block; padding-right: 17px;">
<div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button onclick="close_p();" type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">×</span></button>
<h4 class="modal-title" id="myModalLabel">
</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-xs-12 text-center alert-content">
<p>
<strong><?php echo __('Message content is required to send. Max length of 120 chars. HTML or script tags is not allowed.'); ?></strong>
</p>
</div>
</div>
<div class="row">
<div class="col-xs-12 text-center">
<button class="btn btn-launch" data-dismiss="modal" onclick = "close_p();" aria-label="Close" style="float: none;"><?php echo __('OK'); ?></button>
</div>
</div></div></div></div></div></div>
<div class="modal-backdrop fade in check_chat" style = "display:none"></div>
<script>
var current_counter = <?php echo $current_counter; ?>;
  var height = $(window).scrollTop();
    $(document).ready(function () {
        $('#list-messages').scrollTop($('#list-messages').height() + 500);
        setInterval(function () {	
		var element = document.getElementById('list-messages');
            $.ajax({
                type: 'POST',
                url: '/UsersProjects/checkRoomStatusAjax',
                data: {
                    'room_id': <?php echo isset($roomId) ? $roomId : 0; ?>,
                    'current_counter': current_counter
                },
                success: function (respone) {
					
                  if (respone !== '') {
				  	current_counter++;
                    var div = document.createElement('div');
					div.className = 'template_list_message';
					div.innerHTML = respone;
					document.getElementById('template_list_message').appendChild(div);
					if (element.scrollHeight - element.scrollTop - 300 <= element.clientHeight)
					{
						$("#list-messages").scrollTop($("#list-messages")[0].scrollHeight);
					}
                    }
                }
            });
        }, 1000);
        $('#btn-add-group').click(function () {
            var expanded = $(this).attr('aria-expanded');
            if (expanded == 'false') {
                $('.list_user_accept').html('');
                $.ajax({
                    type: 'POST',
                    url: '/UsersProjects/getListContactAjax',
                    data: {
                        'room_id': <?php echo isset($roomId) ? $roomId : 0; ?>,
                        'project_id': <?php echo isset($Project->id) ? $Project->id : 0; ?>
                    },
                    beforeSend: function () {
                        $('#content-loading').html('<div class="col-md-12 text-center applicant-loading"><img src="/img/loading.gif" alt="Loading" title="Loading" /></div>');
                    },
                    success: function (respone) {
                        $('#content-loading').html('');
                        $('#box-add-group .template-list-chat').html(respone);
                    }
                });
            } else {
                $('.add_to_group .dropdown').on({
                    "shown.bs.dropdown": function () {
                        this.closable = false;
                    },
                    "click": function () {
                        this.closable = true;
                    },
                    "hide.bs.dropdown": function () {
                        return this.closable;
                    }
                });
            }
        });

    });
function close_popup()
{
	$(".chat-group").hide();
}
function alertBoxChat(Message){
    $(".chat-group").show();
    var sHtml = '';
    sHtml += '<div class="modal fade alert-box alert in" tabindex="-1" role="dialog" style="display: block;"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button onclick="close_popup();" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title" id="myModalLabel"></h4></div><div class="modal-body"><div class="row"><div class="col-xs-12 text-center alert-content"><p><strong>'+Message+'</strong></p></div></div><div class="row"><div class="col-xs-12 text-center"><button class="btn btn-launch" data-dismiss="modal" aria-label="Close" style="float: none;" onclick="close_popup();">OK</button></div></div></div></div></div></div>';
    sHtml += '<div class="modal-backdrop fade in"></div>';
    $('.chat-group').html(sHtml);
}
</script>