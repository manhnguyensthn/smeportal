<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Project'), ['action' => 'edit', $usersProject->user_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Project'), ['action' => 'delete', $usersProject->user_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersProject->user_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Projects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Project'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersProjects view large-9 medium-8 columns content">
    <h3><?= h($usersProject->user_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $usersProject->has('user') ? $this->Html->link($usersProject->user->name, ['controller' => 'Users', 'action' => 'view', $usersProject->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Project') ?></th>
            <td><?= $usersProject->has('project') ? $this->Html->link($usersProject->project->title, ['controller' => 'Projects', 'action' => 'view', $usersProject->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($usersProject->created) ?></td>
        </tr>
    </table>
</div>
